if (!Function.prototype.bind) {

    Function.prototype.bind = function(oThis) {

        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            EmptyFn = function() {},
            fBound = function() {
                return fToBind.apply(this instanceof EmptyFn ? this : oThis || window,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        EmptyFn.prototype = this.prototype;
        fBound.prototype = new EmptyFn();

        return fBound;
    };

}
/*!

 handlebars v1.3.0

Copyright (C) 2011 by Yehuda Katz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@license
*/
var Handlebars = (function() {
// handlebars/safe-string.js
var __module3__ = (function() {
  "use strict";
  var __exports__;
  // Build out our basic SafeString type
  function SafeString(string) {
    this.string = string;
  }

  SafeString.prototype.toString = function() {
    return "" + this.string;
  };

  __exports__ = SafeString;
  return __exports__;
})();

// handlebars/utils.js
var __module2__ = (function(__dependency1__) {
  "use strict";
  var __exports__ = {};
  /*jshint -W004 */
  var SafeString = __dependency1__;

  var escape = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#x27;",
    "`": "&#x60;"
  };

  var badChars = /[&<>"'`]/g;
  var possible = /[&<>"'`]/;

  function escapeChar(chr) {
    return escape[chr] || "&amp;";
  }

  function extend(obj, value) {
    for(var key in value) {
      if(Object.prototype.hasOwnProperty.call(value, key)) {
        obj[key] = value[key];
      }
    }
  }

  __exports__.extend = extend;var toString = Object.prototype.toString;
  __exports__.toString = toString;
  // Sourced from lodash
  // https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
  var isFunction = function(value) {
    return typeof value === 'function';
  };
  // fallback for older versions of Chrome and Safari
  if (isFunction(/x/)) {
    isFunction = function(value) {
      return typeof value === 'function' && toString.call(value) === '[object Function]';
    };
  }
  var isFunction;
  __exports__.isFunction = isFunction;
  var isArray = Array.isArray || function(value) {
    return (value && typeof value === 'object') ? toString.call(value) === '[object Array]' : false;
  };
  __exports__.isArray = isArray;

  function escapeExpression(string) {
    // don't escape SafeStrings, since they're already safe
    if (string instanceof SafeString) {
      return string.toString();
    } else if (!string && string !== 0) {
      return "";
    }

    // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.
    string = "" + string;

    if(!possible.test(string)) { return string; }
    return string.replace(badChars, escapeChar);
  }

  __exports__.escapeExpression = escapeExpression;function isEmpty(value) {
    if (!value && value !== 0) {
      return true;
    } else if (isArray(value) && value.length === 0) {
      return true;
    } else {
      return false;
    }
  }

  __exports__.isEmpty = isEmpty;
  return __exports__;
})(__module3__);

// handlebars/exception.js
var __module4__ = (function() {
  "use strict";
  var __exports__;

  var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

  function Exception(message, node) {
    var line;
    if (node && node.firstLine) {
      line = node.firstLine;

      message += ' - ' + line + ':' + node.firstColumn;
    }

    var tmp = Error.prototype.constructor.call(this, message);

    // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
    for (var idx = 0; idx < errorProps.length; idx++) {
      this[errorProps[idx]] = tmp[errorProps[idx]];
    }

    if (line) {
      this.lineNumber = line;
      this.column = node.firstColumn;
    }
  }

  Exception.prototype = new Error();

  __exports__ = Exception;
  return __exports__;
})();

// handlebars/base.js
var __module1__ = (function(__dependency1__, __dependency2__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;

  var VERSION = "1.3.0";
  __exports__.VERSION = VERSION;var COMPILER_REVISION = 4;
  __exports__.COMPILER_REVISION = COMPILER_REVISION;
  var REVISION_CHANGES = {
    1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
    2: '== 1.0.0-rc.3',
    3: '== 1.0.0-rc.4',
    4: '>= 1.0.0'
  };
  __exports__.REVISION_CHANGES = REVISION_CHANGES;
  var isArray = Utils.isArray,
      isFunction = Utils.isFunction,
      toString = Utils.toString,
      objectType = '[object Object]';

  function HandlebarsEnvironment(helpers, partials) {
    this.helpers = helpers || {};
    this.partials = partials || {};

    registerDefaultHelpers(this);
  }

  __exports__.HandlebarsEnvironment = HandlebarsEnvironment;HandlebarsEnvironment.prototype = {
    constructor: HandlebarsEnvironment,

    logger: logger,
    log: log,

    registerHelper: function(name, fn, inverse) {
      if (toString.call(name) === objectType) {
        if (inverse || fn) { throw new Exception('Arg not supported with multiple helpers'); }
        Utils.extend(this.helpers, name);
      } else {
        if (inverse) { fn.not = inverse; }
        this.helpers[name] = fn;
      }
    },

    registerPartial: function(name, str) {
      if (toString.call(name) === objectType) {
        Utils.extend(this.partials,  name);
      } else {
        this.partials[name] = str;
      }
    }
  };

  function registerDefaultHelpers(instance) {
    instance.registerHelper('helperMissing', function(arg) {
      if(arguments.length === 2) {
        return undefined;
      } else {
        throw new Exception("Missing helper: '" + arg + "'");
      }
    });

    instance.registerHelper('blockHelperMissing', function(context, options) {
      var inverse = options.inverse || function() {}, fn = options.fn;

      if (isFunction(context)) { context = context.call(this); }

      if(context === true) {
        return fn(this);
      } else if(context === false || context == null) {
        return inverse(this);
      } else if (isArray(context)) {
        if(context.length > 0) {
          return instance.helpers.each(context, options);
        } else {
          return inverse(this);
        }
      } else {
        return fn(context);
      }
    });

    instance.registerHelper('each', function(context, options) {
      var fn = options.fn, inverse = options.inverse;
      var i = 0, ret = "", data;

      if (isFunction(context)) { context = context.call(this); }

      if (options.data) {
        data = createFrame(options.data);
      }

      if(context && typeof context === 'object') {
        if (isArray(context)) {
          for(var j = context.length; i<j; i++) {
            if (data) {
              data.index = i;
              data.first = (i === 0);
              data.last  = (i === (context.length-1));
            }
            ret = ret + fn(context[i], { data: data });
          }
        } else {
          for(var key in context) {
            if(context.hasOwnProperty(key)) {
              if(data) { 
                data.key = key; 
                data.index = i;
                data.first = (i === 0);
              }
              ret = ret + fn(context[key], {data: data});
              i++;
            }
          }
        }
      }

      if(i === 0){
        ret = inverse(this);
      }

      return ret;
    });

    instance.registerHelper('if', function(conditional, options) {
      if (isFunction(conditional)) { conditional = conditional.call(this); }

      // Default behavior is to render the positive path if the value is truthy and not empty.
      // The `includeZero` option may be set to treat the condtional as purely not empty based on the
      // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
      if ((!options.hash.includeZero && !conditional) || Utils.isEmpty(conditional)) {
        return options.inverse(this);
      } else {
        return options.fn(this);
      }
    });

    instance.registerHelper('unless', function(conditional, options) {
      return instance.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn, hash: options.hash});
    });

    instance.registerHelper('with', function(context, options) {
      if (isFunction(context)) { context = context.call(this); }

      if (!Utils.isEmpty(context)) return options.fn(context);
    });

    instance.registerHelper('log', function(context, options) {
      var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
      instance.log(level, context);
    });
  }

  var logger = {
    methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },

    // State enum
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3,
    level: 3,

    // can be overridden in the host environment
    log: function(level, obj) {
      if (logger.level <= level) {
        var method = logger.methodMap[level];
        if (typeof console !== 'undefined' && console[method]) {
          console[method].call(console, obj);
        }
      }
    }
  };
  __exports__.logger = logger;
  function log(level, obj) { logger.log(level, obj); }

  __exports__.log = log;var createFrame = function(object) {
    var obj = {};
    Utils.extend(obj, object);
    return obj;
  };
  __exports__.createFrame = createFrame;
  return __exports__;
})(__module2__, __module4__);

// handlebars/runtime.js
var __module5__ = (function(__dependency1__, __dependency2__, __dependency3__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;
  var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
  var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;

  function checkRevision(compilerInfo) {
    var compilerRevision = compilerInfo && compilerInfo[0] || 1,
        currentRevision = COMPILER_REVISION;

    if (compilerRevision !== currentRevision) {
      if (compilerRevision < currentRevision) {
        var runtimeVersions = REVISION_CHANGES[currentRevision],
            compilerVersions = REVISION_CHANGES[compilerRevision];
        throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. "+
              "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").");
      } else {
        // Use the embedded version info since the runtime doesn't know about this revision yet
        throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. "+
              "Please update your runtime to a newer version ("+compilerInfo[1]+").");
      }
    }
  }

  __exports__.checkRevision = checkRevision;// TODO: Remove this line and break up compilePartial

  function template(templateSpec, env) {
    if (!env) {
      throw new Exception("No environment passed to template");
    }

    // Note: Using env.VM references rather than local var references throughout this section to allow
    // for external users to override these as psuedo-supported APIs.
    var invokePartialWrapper = function(partial, name, context, helpers, partials, data) {
      var result = env.VM.invokePartial.apply(this, arguments);
      if (result != null) { return result; }

      if (env.compile) {
        var options = { helpers: helpers, partials: partials, data: data };
        partials[name] = env.compile(partial, { data: data !== undefined }, env);
        return partials[name](context, options);
      } else {
        throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
      }
    };

    // Just add water
    var container = {
      escapeExpression: Utils.escapeExpression,
      invokePartial: invokePartialWrapper,
      programs: [],
      program: function(i, fn, data) {
        var programWrapper = this.programs[i];
        if(data) {
          programWrapper = program(i, fn, data);
        } else if (!programWrapper) {
          programWrapper = this.programs[i] = program(i, fn);
        }
        return programWrapper;
      },
      merge: function(param, common) {
        var ret = param || common;

        if (param && common && (param !== common)) {
          ret = {};
          Utils.extend(ret, common);
          Utils.extend(ret, param);
        }
        return ret;
      },
      programWithDepth: env.VM.programWithDepth,
      noop: env.VM.noop,
      compilerInfo: null
    };

    return function(context, options) {
      options = options || {};
      var namespace = options.partial ? options : env,
          helpers,
          partials;

      if (!options.partial) {
        helpers = options.helpers;
        partials = options.partials;
      }
      var result = templateSpec.call(
            container,
            namespace, context,
            helpers,
            partials,
            options.data);

      if (!options.partial) {
        env.VM.checkRevision(container.compilerInfo);
      }

      return result;
    };
  }

  __exports__.template = template;function programWithDepth(i, fn, data /*, $depth */) {
    var args = Array.prototype.slice.call(arguments, 3);

    var prog = function(context, options) {
      options = options || {};

      return fn.apply(this, [context, options.data || data].concat(args));
    };
    prog.program = i;
    prog.depth = args.length;
    return prog;
  }

  __exports__.programWithDepth = programWithDepth;function program(i, fn, data) {
    var prog = function(context, options) {
      options = options || {};

      return fn(context, options.data || data);
    };
    prog.program = i;
    prog.depth = 0;
    return prog;
  }

  __exports__.program = program;function invokePartial(partial, name, context, helpers, partials, data) {
    var options = { partial: true, helpers: helpers, partials: partials, data: data };

    if(partial === undefined) {
      throw new Exception("The partial " + name + " could not be found");
    } else if(partial instanceof Function) {
      return partial(context, options);
    }
  }

  __exports__.invokePartial = invokePartial;function noop() { return ""; }

  __exports__.noop = noop;
  return __exports__;
})(__module2__, __module4__, __module1__);

// handlebars.runtime.js
var __module0__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
  "use strict";
  var __exports__;
  /*globals Handlebars: true */
  var base = __dependency1__;

  // Each of these augment the Handlebars object. No need to setup here.
  // (This is done to easily share code between commonjs and browse envs)
  var SafeString = __dependency2__;
  var Exception = __dependency3__;
  var Utils = __dependency4__;
  var runtime = __dependency5__;

  // For compatibility and usage outside of module systems, make the Handlebars object a namespace
  var create = function() {
    var hb = new base.HandlebarsEnvironment();

    Utils.extend(hb, base);
    hb.SafeString = SafeString;
    hb.Exception = Exception;
    hb.Utils = Utils;

    hb.VM = runtime;
    hb.template = function(spec) {
      return runtime.template(spec, hb);
    };

    return hb;
  };

  var Handlebars = create();
  Handlebars.create = create;

  __exports__ = Handlebars;
  return __exports__;
})(__module1__, __module3__, __module4__, __module2__, __module5__);

  return __module0__;
})();

this.Handlebars = this.Handlebars || {};
this.Handlebars.templates = this.Handlebars.templates || {};
(function() {

    var win = window;

    // Custom event polyfill
    if(!win.CustomEvent) {

        var CustomEvent = function(event, params) {

            var evt;

            params = params || {
                bubbles: false,
                cancelable: false,
                detail: undefined
            };

            try {
                evt = document.createEvent("CustomEvent");
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            } catch (error) {
                evt = document.createEvent("Event");
                for (var param in params) {
                    evt[param] = params[param];
                }
                evt.initEvent(event, params.bubbles, params.cancelable);
            }

            return evt;
        };

        CustomEvent.prototype = win.Event.prototype;

        win.CustomEvent = CustomEvent;

    }

}());
(function() {

    /**
     * @ignore
     * FastClick is an override shim which maps event pairs of
     *   'touchstart' and 'touchend' which differ by less than a certain
     *   threshold to the 'click' event.
     *   This is used to speed up clicks on some browsers.
     */
    var clickThreshold = 300;
    var clickWindow = 500;
    var potentialClicks = {};
    var recentlyDispatched = {};
    var win = window;
    var inputs = ['INPUT', 'TEXTAREA', 'SELECT'];
    var android = (win.navigator.userAgent.indexOf('Android') >= 0);
    var mustBlur = false;

    win.addEventListener('touchstart', function(event) {

        if (mustBlur) {
            //if (document.activeElement) document.activeElement.blur();
            if (typeof mustBlur.blur === 'function') {
                mustBlur.blur();
            }
            mustBlur = false;
        }

        var timestamp = Date.now(),
            len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            potentialClicks[touch.identifier] = timestamp;
        }
    });
    win.addEventListener('touchmove', function(event) {

        var len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            delete potentialClicks[touch.identifier];
        }
    });
    win.addEventListener('touchend', function(event) {

        var currTime = Date.now(),
            len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            var startTime = potentialClicks[touch.identifier];
            if (startTime && currTime - startTime < clickThreshold) {
                var clickEvt = new win.CustomEvent('click', {
                    'bubbles': true,
                    'details': touch
                });
                recentlyDispatched[currTime] = event;
                event.target.dispatchEvent(clickEvt);
                // we prevent here to avoid ghost click on ios 7 with input click (2nd time)
                // tested on ios
                event.preventDefault(); // if (ios) ? must test on android
                if (inputs.indexOf(event.target.tagName) !== -1) {
                    if (!android && event.target.focus) event.target.focus();
                    else mustBlur = event.target;
                }
            }
            delete potentialClicks[touch.identifier];
        }
    });
    win.addEventListener('click', function(event) {

        var currTime = Date.now();
        for (var i in recentlyDispatched) {
            var previousEvent = recentlyDispatched[i];
            if (currTime - i < clickWindow) {
                if (event instanceof win.MouseEvent) {
                    event.stopPropagation();
                    // prevent ghost click
                    if (event.target !== previousEvent.target) event.preventDefault();
                }
                // if (event instanceof win.MouseEvent && event.target === previousEvent.target) event.stopPropagation();
                // else if (event instanceof win.MouseEvent && event.target !== previousEvent.target) {
                //     // @TODO @PATCH prevent ghost click
                //     event.stopPropagation();
                //     event.preventDefault();
                // }
            }
            else delete recentlyDispatched[i];
        }
    }, true);

}());

/**
 * @class Roc
 * @singleton
 *
 * Framework global namespace.
 *
 * ### Class definition example

    App.MyClass = (function() {

        // private instances shared variable declaration
        var _parent = Roc;

        return _parent.subclass({

            constructor: function(config) {
                // Class constructor
                _parent.prototype.constructor.call(this, config);
            }

        });

    }());
 */

(function() {

    //'use strict';

    var slice = Array.prototype.slice,
        _xtypes = {};

    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {
            return (_xtypes[xtype] ||
                console.warn('[core] xtype "' + xtype +
                    '" does not exists.'));
        }
    };

    var _createSubclass = function(props) {

        props = props || {};

        var key,
            realConstructor,
            superclass = props.superclass.prototype;

        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }

        function constructor() {

            var self = this;

            if (!(self instanceof constructor)) {
                throw new Error('[core] Please use "new" when initializing Roc classes');
            }
            realConstructor.apply(self, arguments);
        }

        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;

        _extend(constructor, {

            parent: superclass,

            subclass: function(obj) {

                var sclass;

                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);

                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });

        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }

        return constructor;
    };

    var _extend = function() {

        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();

        while (++i < l) {

            var key,
                props = args[i];

            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };

    var core = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        /**
         * @readonly
         * @property {String} version
         * Current framework version.
         */
        version: '2.0.0',

        /**
         * Create instance of object from given xtype
         * and configuration.
         * @param {String} xtype Xtype.
         * @param {Object} [config] Configuration.
         * @return {Mixed} Instanciated object.
         */
        create: function(xtype, config) {

            var obj = _xtype(xtype);

            return (new obj(config));

        },

        /**
         * Extend given xtype or class with given configuration and return the new class.
         * This method will override constructor method of subclass, so please use `subclass` if you wanna override constructor instead.
         * @param  {Mixed} xtype  Xtype string or Parent object class to extend.
         * @param  {Object} config Object to extend.
         * @return {Mixed} New generated extend class.
         *
         * For example:
         *
         *      // this:
         *      var mypage = Roc.extend('page', {
         *
         *          xtype: 'mypage',
         *
         *          config: {
         *              items: [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }]
         *          }
         *
         *      });
         *
         *      // is same as:
         *      var mypage2 = Roc.views.Page.subclass({
         *
         *          xtype: 'mypage',
         *
         *          constructor: function(opts) {
         *
         *              opts = opts || {};
         *
         *              opts.items = [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }];
         *
         *              Roc.views.Page.prototype.constructor.call(this, opts);
         *
         *          }
         *      });
         *
         *      // and same as:
         *      var mypage3 = Roc.extend(Roc.views.Page, {
         *
         *          xtype: 'mypage',
         *
         *          config: {
         *              items: [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }]
         *          }
         *
         *      });
         */
        extend: function(xtype, config) {

            if (typeof xtype === 'string') xtype = _xtype(xtype);

            if (config.config) {
                config.constructor = function(opts) {

                    //opts = opts || {};
                    opts = config.config;

                    xtype.prototype.constructor.call(this, config.config);
                };
            }

            return xtype.subclass(config);
        },

        /**
         * Allows to extend from class.
         * @method
         * @param {Object} obj
         * @return {Object}
         */
        subclass: function(obj) {
            var sclass;

            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);

            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = core;
        }
        exports.Roc = core;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return core;
        });
    } else {
        window.Roc = core;
    }

})();

Roc.debug = new (function() {

    var _profiles = {},
        _debug = false,
        _whitelist = [/*
            'router',
            'news',
            'store',
            'request',
            'newsdetail',
            'button',
            'list',
            //'click',
            //'clickbuster',
            'listbuffered',
            'loadmask',
            'scroll',
            'request',
            'bounce'
        */];

    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };

    return Roc.subclass({

        /*profile: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.time(key);
            //_profiles[key] = this.memory();
            return this;
        },*/

        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },

        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },

        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },

        /*profileEnd: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.timeEnd(key);
            //console.debug(_profiles[key], '->', this.memory());
            //console.debug(key, 'Before: ', _profiles[key]);
            //console.debug(key, 'Now: ', (window.performance.memory.usedJSHeapSize / 1024.0) + 'ko');
            return this;
        },

        memory: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            if (window.performance && window.performance.memory) {
                return ((window.performance.memory.totalJSHeapSize / 1024.0) + 'ko');
            }
            return 0;
        },*/

        set: function(debug) {
            _debug = (debug === true);
        }
    });

}())();

/**
 * @class Roc.utils
 * @singleton
 */
Roc.utils = (function() {

    'use strict';

    var _table = '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';

    return {
        /**
         * Apply base[key] to obj[key] if obj[key] is undefined.
         *
         * ### Sample usage
         *
         *     var obj = { a: true, b: "toto", c: undefined },
         *         base = { a: false, b: "tata", c: [1, 2], d: 1 };
         *
         *     var results = Roc.utils.applyIf(obj, base, ['a', 'c', 'd']);
         *
         *     // results = { a: true, b: "toto", c: [1, 2], d: 1 };
         *
         * @param {Object} obj The receiver of the properties.
         * @param {Object} base The source of the properties.
         * @param {Array} keys List of keys to copy.
         * @return {Object} returns obj
         */
        applyIf: function(obj, base, keys) {

            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof obj[key] === 'undefined' &&
                    typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of base to obj.
         * @param {Object} obj The receiver of properties.
         * @param {Object} base The source of properties.
         * @return {Object} returns obj
         */
        applyIfAuto: function(obj, base) {

            var key;

            for (key in base) {
                if (typeof obj[key] === 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of base to obj for given keys.
         * @param {Object} obj The receiver of properties.
         * @param {Object} base The source of properties.
         * @param {Array} keys Keys to apply.
         * @return {Object} Updated obj.
         */
        apply: function(obj, base, keys) {

            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of both objects to new object.
         * It means we merge obj1 with obj2 into new object.
         * If same key is in both objects, obj2 values will be taken.
         * @param {Object} obj1
         * @param {Object} obj2
         * @return {Object} Merged obj1 and obj2 objects.
         */
        applyAuto: function(obj1, obj2) {

            var attrname, obj3 = {};

            for (attrname in obj1) {
                obj3[attrname] = obj1[attrname];
            }
            for (attrname in obj2) {
                obj3[attrname] = obj2[attrname];
            }
            return obj3;
        },

        /**
         * Copy all array values from given start index to given end index.
         * @param {Array} array Array to copy.
         * @param {Number} from (optional) Index to copy data from. Defaults to start.
         * @param {Number} to (optional) Index to copy data to. Defaults to end.
         * @return {Array} Copied array.
         */
        arraySlice: function(array, from, to) {

            var result = [],
                i = (from ? (from - 1) : 0),
                length = (to ? to : array.length);

            while (++i < length) {
                result.push(array[i]);
            }
            return result;
        },

        /**
         * Capitalize given string and return it.
         * @param {String} str String to capitalize.
         * @return {String} Capitalized string.
         */
        capitalize: function(str) {
            return (str.charAt(0).toUpperCase() + str.slice(1));
        },

        /**
         * Returns the crc32 for the given string.
         * @param {String} str String.
         * @param {Number} crc (optional) CRC, defaults to 0.
         * @return {Number} CRC32 for given string.
         */
        crc32: function(str, crc) {
            if (crc == window.undefined) {
                crc = 0;
            }

            var i = 0,
                iTop = str.length,
                n = 0, //a number between 0 and 255
                x = 0; //an hex number

            crc = crc ^ (-1);
            for (; i < iTop; i++ ) {
                n = ( crc ^ str.charCodeAt(i)) & 0xFF;
                x = '0x' + _table.substr(n * 9, 8);
                crc = (crc >>> 8) ^ x;
            }
            return crc ^ (-1);
        }
    };

}());

/**
 * @class Roc.Storage
 * @extends Roc
 * @singleton
 *
 * ### Example usage
 *
    var s = Roc.Storage;

    // (optional) set a prefix for our application
    s.setPrefix('myapp');

    // you can store item by name
    s.setItem('news', [{
        id: 1,
        title: 'News 1',
        content: '...'
    }, {
        id: 2,
        title: 'News 2',
        content: '...'
    }]);

    // you can retrieve those items by name too
    s.getItem('news');

    // or remove them by name
    s.removeItem('news');

    // or completly erase all data for given prefix
    s.empty('myapp');
 *
 */
Roc.Storage = new (function() {

    'use strict';

    var _store = localStorage,
        _prefix = '';

    var _privateStore = {};
    var _backupStore = {
        setItem: function(name, value) {
            _privateStore[name] = value;
        },
        getItem: function(name) {
            return _privateStore[name];
        },
        removeItem: function(name) {
            delete _privateStore[name];
        }
    };

    /**
     * Set a default prefix for all store items key.
     * @method setPrefix
     * @param {String} prefix Prefix.
     * @return {Boolean} true if success else false.
     */
    function _setPrefix(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    }

    /**
     * Get the default prefix.
     * @method getPrefix
     * @param {String} prefix Prefix.
     * @return {String} Return given prefix if correct else default prefix.
     */
    function _getPrefix(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    }

    /**
     * Store value for given name.
     * @method setItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _setItem(name, value, prefix, retry) {

        var varname = _getPrefix(prefix) + name,
            varvalue = JSON.stringify(value);

        try {
            _store.setItem(varname, varvalue);
        } catch (e) {
            if (e.name === 'QUOTA_EXCEEDED_ERR') {
                _reset(!retry);
                if (retry !== false) {
                    setTimeout(function() {
                        _setItem(name, value, prefix, false);
                    }, 1);
                } else {
                    _store = _backupStore;
                    _store.setItem(varname, varvalue);
                }
            }
        }
    }

    function _reset(keep_with_prefix) {

        var prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (keep_with_prefix !== true ||
                (prefix && name.indexOf(prefix))) {
                _removeItem(name, '');
            }
        }

    }

    /**
     * Get value stored for given name.
     * @method getItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _getItem(name, prefix) {

        var value = _store.getItem(_getPrefix(prefix) + name);

        // @bugfix for android 2.3 when JSON.parse null
        if (typeof value === 'string') {
            return JSON.parse(value);
        }
        return false;
    }

    /**
     * Remove value at given name.
     * @method removeItem
     * @param {String} name Name.
     * @param {String} prefix (optional) Prefix.
     * @return {Boolean} True if success else false.
     */
    function _removeItem(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    }

    /**
     * Empty all stored data for given prefix or all if no prefix.
     * @method empty
     * @param {String} prefix (optional) Prefix.
     */
    function _empty(prefix) {
        prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    }

    return Roc.subclass({

        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty,
        reset: _reset

    });

}())();

/**
 * @singleton
 * @class Roc.Selector
 * @extends Roc
 *
 * DOM manipulation and utilities.
 */
Roc.Selector = new (function() {

    //'use strict';

    // @private
    var _ns = Roc,
        _scope,
        _elUid = 0,
        _win = window,
        _prefix,
        _utils = _ns.utils;

    _prefix = (function () {

        var styles = _win.getComputedStyle(document.documentElement, ''),
            pre = (Array.prototype.slice
                .call(styles)
                .join('')
                .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
            )[1],
            dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1],
            cssStyle = _utils.capitalize(pre);

        var events = {
            'moz': undefined,
            'webkit': 'webkit',
            'ms': undefined,
            'o': 'o'
        };

        if (cssStyle === 'Webkit') {
            cssStyle = 'webkit';
        }

        return {
            dom: dom,
            lowercase: pre,
            css: '-' + pre + '-',
            cssStyle: cssStyle,
            jsEvent: function(eventName) {

                var before = events[pre];

                if (!before) {
                    return eventName.toLowerCase();
                }

                return (before + _utils.capitalize(eventName));
            },
            js: pre[0].toUpperCase() + pre.substr(1)
        };

    })();

    // public
    return Roc.subclass({

        prefix: _prefix,

        /**
         * Create new selector.
         * @param {Object} gscope The global scope. Defaults to `window`.
         */
        constructor: function(gscope) {
            _scope = gscope;
        },

        /**
         * Generate new DOM uniq identifier.
         * @param {String} prefix (optional) Prefix.
         * @return {String} id.
         */
        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'fs';
            return (prefix + _elUid);
        },

        /**
         * Get a DOM Node from query selector.
         *
         * @param {String} selector Selector
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {HTMLElement} result.
         */
        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },

        /**
         * Get all DOM nodes from query selector.
         * @param {String} selector Selector.
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {Array} Array of HTMLElement matches.
         */
        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },

        /**
         * Execute callback recursivly on element parent until
         * callback returns false.
         * @param {HTMLElement} el DOM node.
         * @param {Function} callback Callback.
         * @return {Mixed}
         */
        findParent: function(el, callback) {
            if (callback(el) === false && el.parentNode) {
                return this.findParent(el.parentNode, callback);
            }
        },

        /**
         * Apply given css properties to element.
         * This method will automatically check for prefix properties.
         * So it is highly recommended to use for example:
         *  `transform` instead of `webkitTransform`.
         *
         * @method css
         * @param {HTMLElement} el DOM node.
         * @param {Object} props CSS properties.
         * @return {Boolean} success
         */
        css: function(el, props) {

            var self = this,
                styles = document.body.style;

            Object.keys(props).forEach(function(name) {

                var compliant = self.prefix.cssStyle + _utils.capitalize(name);

                if (compliant in styles) {
                    el.style[compliant] = props[name];
                }
                el.style[name] = props[name];
            });
            return true;
        },

        /**
         * Checks if element has CSS class.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean}
         */
        hasClass: function(el, cls) {
            // <debug>
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el);
                console.trace('Roc.Selector.hasClass');
                return false;
            }
            // </debug>
            //return (el.classList && el.classList.contains(cls));
            return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') !== -1);
        },

        /**
         * Checks if element has given CSS classes.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        hasClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                if (!this.hasClass(el, c[len])) {
                    return false;
                }
            }
            return true;

        },

        /**
         * Adds CSS class to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                //console.log('addClass ====== ', el.id, cls);
                el.className += ' ' + cls;
                el.className = el.className.replace(/^\s+|\s+$/g, '');
                //el.classList.add(cls);
                return true;
            }
            return false;
        },

        /**
         * Add CSS classes from given element.
         * Use this method instead of {@link #addClass} when you are not sure if one or several of those classes are already applied on element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        addClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.addClass(el, c[len]);
            }
            return true;

        },

        /**
         * Removes CSS class from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                //el.classList.remove(cls);
                return true;
            }
            return false;
        },

        /**
         * Remove CSS classes from given element.
         * Use this method instead of {@link #removeClass} when you are not sure if one or several of those classes are already applied or removed from element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        removeClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.removeClass(el, c[len]);
            }
            return true;
        },

        /**
         * Force browser to redraw given element.
         * @param {HTMLElement} el DOM node.
         */
        redraw: function(el) {
            el.style.display = 'none';
            var h = el.offsetHeight;
            el.style.display = 'block';
        },

        /**
         * Removes given element from DOM.
         * @param {HTMLElement} el DOM node.
         */
        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },

        /**
         * Removes all HTML from given element.
         * @param {HTMLElement} el DOM node.
         */
        removeHtml: function(el) {
            //el.innerHTML = '';
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },

        // !TODO doc
        findParentByTag: function(el, tagName) {
            while (el) {
                if (el.tagName === tagName) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        // !TODO doc
        findParentByClass: function(el, className) {
            while (el) {
                if (this.hasClass(el, className)) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        /**
         * Adds HTML to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },

        /**
         * Updates HTML from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },

        /**
         * Scrolls to given offsets for given element.
         * @param {HTMLElement} el DOM node.
         * @param {Number} offsetX (optional) Offset x. Defaults to 0.
         * @param {Number} offsetY (optional) Offset y. Defaults to 0.
         */
        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });

}())(document);

/**
 * @class Roc.Request
 * @extends Roc
 * @singleton
 *
 * Request class to perform remote request.
 * Used by {@link Roc.data.Store#proxy store proxy}.
 *
 * ### Example
 *
 *      Roc.Request.ajax('/login', {
 *          jsonParams: {
 *              username: 'mrsmith',
 *              password: 'wanna die?'
 *          },
 *          scope: this,
 *          callback: function(requestSuccess, data) {
 *
 *              if (requestSuccess === true) {
 *
 *                  if (typeof data === 'object' &&
 *                      data.success === true) {
 *                      // welcome !
 *                  } else
 *                      // login error
 *                  }
 *
 *              } else {
 *                  // server error
 *              }
 *
 *           }
 *      });
 *
 */
Roc.Request = new (function() {

    'use strict';

    // @private
    var jsonp_id = 0;

    var _jsonToString = function(json, prefix) {

        var key,
            val,
            result = '',
            first = true;

        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }

        return (prefix ? prefix : '') + result;
    };

    function url_delimiter(url) {
        return (url.indexOf('?') === -1 ? '?': '&');
    }

    // public
    return Roc.subclass({

        /**
         * Makes an AJAX request.
         *
         * ## Example
         *
         *      Roc.Request.ajax('/getdata', {
         *          callback: function(success, data) {},
         *          scope: this,
         *          method: 'PUT', // GET, POST, PUT, DELETE
         *          data: 'name1=value1&name2=value2',
         *          params: 'name1=value1&name2=value2', // (for GET)
         *          jsonParams: { name1: 'value1', name2: 'value2' } // (for GET)
         *      });
         *
         * @param {String} url URL.
         * @param {Object} options Options.
         * @method
         */
        ajax: function(url, options) {

            var request,
                callback = (typeof options.callback === 'function' ?
                    options.callback.bind(options.scope || window) : null);
                options.method = options.method || 'GET';
                options.data = options.data || '';

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }

            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },

        /**
         * Makes a JSONP request.
         *
         * ## Example
         *
         *     Roc.Request.jsonp('/getdata?callback={callback}', {
         *         callback: function(success, data) {},
         *         scope: this,
         *         params: 'sortby=date&dir=desc',
         *         jsonParams: {
         *             sortby: 'date',
         *             dir: 'desc'
         *         }
         *     });
         *
         * @param {String} url URL.
         * @param {Object} options An object which may contain the following properties. Note that options will
         * take priority over any defaults that are specified in the class.
         * <ul>
         * <li><b>params</b> : String (Optional)<div class="sub-desc">A string containing additional parameters.</div></li>
         * <li><b>jsonParams</b> : Object (Optional)<div class="sub-desc">An object containing a series of
         * key value pairs that will be sent along with the request.</div></li>
         * <li><b>callback</b> : Function (Optional) <div class="sub-desc">A function to execute when the request
         * completes, whether it is a success or failure.</div></li>
         * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope in
         * which to execute the callbacks: The "this" object for the callback function. Defaults to the browser window.</div></li>
         * </ul>
         * @method
         */
        jsonp: function(url, options) {

            var callback_name = ['jsonp', ++jsonp_id].join(''),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    //_debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams, url_delimiter(url));
            }
            if (options.params) {
                url += (url_delimiter(url) + options.params);
            }

            if (url.indexOf('{callback}') === -1) {
                url += (url_delimiter(url) + 'callback={callback}');
            }

            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        }
    });
}())();

/**
 * @class Roc.History
 * @extends Roc
 * @singleton
 * @requires Roc.Storage
 *
 * History support class. Use with {@link Roc.Router router}.
 *
 * ### Example
 *
 *      var history = Roc.History;
 *
 *      history.start();
 *      history.navigate('/home');
 *
 *      var currentUrl = history.here();
 *
 */
Roc.History = new (function() {

    'use strict';

    // @private
    var _ns = Roc,
        _win = window,
        scope = _win,
        _storage = _ns.Storage,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [],
        _history = [],
        _maxLength = 20,
        _curLength = 0;

    function runCallbacks(self, location) {
        /*if (self.curhash === location) {
            return false;
        }*/
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }

        var regexp,
            route,
            i = -1,
            length = routes.length;

        self.curhash = location;

        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                if (route.callback(location) === false) {
                    return false;
                }
            }
        }

        return true;
    }

    var onHashChange = function(event) {

        var here = _win.location.hash.substr(1);

        _curLength = _history.push(here);

        if (_curLength > _maxLength) {
            _history.shift();
        }
        runCallbacks(this, here);
    };

    // public
    return _ns.subclass({

        /**
         * Create new history.
         * @param {Object} gscope The global scope. Defaults to `window`.
         */
        constructor: function(gscope) {
            /**
             * @property {String} defaultRoute The default route.
             */
            this.defaultRoute = '';
            scope = gscope || _win;
            onHashChange = onHashChange.bind(this);
            scope.addEventListener('hashchange', onHashChange, false);
        },

        /**
         * Set the default route.
         * @param {String} path Path.
         */
        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },

        /**
         * Start history.
         * If browser is standalone it will recover to last visited url or {@link #defaultRoute defaultRoute}.
         */
        start: function() {

            var hash = curhash;

            curhash = '';

            // if added to homescreen, try to restore previous location
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks(this, hash);
        },

        /**
         * Stop history.
         */
        stop: function() {
            scope.removeEventListener('hashchange', onHashChange, false);
        },

        /**
         * Get the current location.
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Current location.
         */
        here: function(encoded) {

            var hash = _win.location.hash.substr(1);

            return (encoded ? encodeURIComponent(hash) : hash);
        },

        /**
         * Get history item from given index.
         * @param {Number} index
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Location for given index.
         */
        get: function(index, encoded) {

            var pos = _curLength - 1 + index;

            if (pos >= _curLength) {
                return this.here(encoded);
            } else if (pos < 0) {
                pos = 0;
            }
            return (encoded ? encodeURIComponent(_history[pos]) : _history[pos]);
        },

        /**
         * Go back in history.
         * @param {Number} index (optional)
         */
        back: function(index) {
            index = (typeof index !== 'number' ? -1 : index);
            _win.history.go(index);
        },

        // @TODO backToLastRefused
        /*
        Stocker lors du go avec genre {pending: true}
        et lors de l'appel du callback on delete history.pending
        */

        /**
         * Navigate to given location.
         * @param {String} location.
         */
        navigate: function() {
            return this.go.apply(this, arguments);
        },

        go: function(location) {
            _win.location.hash = '#' + location;
        },

        /**
         * Add given route.
         * @param {String} route Route
         * @param {Function} callback Callback.
         */
        route: function(base, route, callback) {
            routes.push({
                base: base,
                regexp: route,
                callback: callback
            });
        }
    });
}())(window);

/**
 * @class Roc.Router
 * @extends Roc
 *
 * ### Example
 *

 *
 */
Roc.Router = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _history = _ns.History,
        optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;

    function _extractParameters(route, fragment) {
        return route.exec(fragment).slice(1);
    }

    function _routeToRegExp(route) {
        route = route.replace(escapeRegExp, '\\$&')
            .replace(optionalParam, '(?:$1)?')
            .replace(namedParam, function(match, optional) {
                return optional ? match : '([^\/]+)';
            })
            .replace(splatParam, '(.*?)');

        return new RegExp('^' + route + '$');
    }

    function _prepareBefore(self) {

        if (self.before) {

            var pattern;

            self.beforeRegexp = {};
            for (pattern in self.before) {
                /* @ignore !TODO permettre de prendre en compte un array de callback
                if (typeof self.before[i] === 'string') {
                    self.before[i] = [self.before[i]];
                }
                */
                self.beforeRegexp[pattern] = new RegExp(pattern);
            }
        }
    }

    function _getBefore(self, funcname) {

        if (self.before) {

            var pattern;

            for (pattern in self.before) {
                if (self.beforeRegexp[pattern].test(funcname) === true) {
                //if (funcname.match(self.beforeRegexp[i]) !== null) {
                    var func = self[self.before[pattern]];

                    return (func ? func : false);
                }
            }
        }
        return false;
    }

    // public
    return _ns.subclass({
        /**
         * @cfg {Object} routes
         */
        /**
         * @cfg {Object} before
         */
        /**
         * @cfg {String} routeNoMatch
         */
        /**
         * Create new router.
         */
        constructor: function() {

            var self = this;

            _prepareBefore(self);
            if (self.routes) {

                var pattern;

                for (pattern in self.routes) {
                    self.route(pattern, self.routes[pattern]);
                }
            }

            if (self.routeNoMatch) {
                self.matched = false;
                _history.route('', /^(.*?)+$/ig, function() {
                    if (self.matched === false) {
                        self[self.routeNoMatch]();
                    } else {
                        self.matched = false;
                    }
                });
            }
        },

        /**
         * Calls given path with callback.
         *
         * @param {String} path Path.
         * @param {Function} foo Route callback.
         * @method
         */
        route: function(base, foo) {

            var path,
                before,
                self = this,
                callback = self[foo],
                history = _history;

            if (callback) {
                if (self.routeEscape !== false) {
                    path = _routeToRegExp(base);
                } else {
                    path = new RegExp(base);
                }
                before = _getBefore(self, foo);

                var cb = function(fragment) {

                    var args = _extractParameters(path, fragment);

                    callback.apply(self, args);
                    self.matched = true;
                },
                beforeCb = function(fragment) {
                    var canContinue = before.call(self, function() {
                        return cb(fragment);
                    });
                    if (canContinue === false) return false;
                };

                if (before) {
                    history.route(base, path, beforeCb);
                } else {
                    history.route(base, path, cb);
                }
            }

            return self;
        }
    });
}());

/**
 * @ignore !TODO for debugging: Global event handler like Roc.EventHandler.listenAllEvents('afterrender');
 */
/**
 * @class Roc.Event
 * @extends Roc
 * @requires Roc.utils
 *
 * Event manager class.
 *
 * ## Example

    var event = new Roc.Event({
        listeners: {
            scope: this,
            afterload: function(success, data) {
                // do some stuff here...
            },
            beforeload: function(request) {
                if (typeof request.data === 'undefined') {
                    return false; // stop all next events from being firing.
                }
            }
        }
    });

    // Registering listener for `error` event.
    var eid = event.on('error', function(code, message) {
        alert('Error (' + code + ') occured: \n' + message);
    }, this);

    // Firing `error` event.
    event.fire('error', 401, 'Unauthorized !');

    // Unregistering listener.
    event.off(eid);

 */
Roc.Event = (function() {

    'use strict';

    var _ns = Roc,
        _utils = _ns.utils,
        _parent = _ns,
        _priorities = {
            BEFORECORE: 1025,
            CORE: 1000,
            AFTERCORE: 975,
            BEFOREVIEWS: 925,
            VIEWS: 900,
            AFTERVIEWS: 875,
            DEFAULT: 800
        };

    function _setupEvents(self) {
        if (typeof self._events !== 'object') {
            // events queue
            /*

            e1 = event_id

            {
                "eventname1": {
                    "998": [e1, e2],
                    "1000": [e3]
                },
                ...
            }

            */
            self._events = {};
            self.uids = 0;
            // priority queue @TODO
            /*

            {
                "eventname1": [998, 1000],
                "eventname2": [1000],
                "eventname3": [100, 1000, 9999],
                ...
            }

            */
            self.pqueue = {};
            // callbacks queue
            /*

            {
                "event_id": [event_name, priority, callback, scope],
                ...
            }

            */
            self.equeue = {};
        }
    }

    function _numSort(a, b) {
        return (b - a);
    }

    function _count_listeners(self, name) {

        var e = self._events[name];

        if (e) {
            var total = 0,
                keys = Object.keys(self._events[name]),
                i = keys.length;

            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    }

    function _register_listeners(self, listeners) {

        var key, priority,
            scope = self,
            l = self._listeners = self._listeners || {},
            keys = Object.keys(listeners),
            len = keys.length;

        var scopeIndex = keys.indexOf('scope');
        if (scopeIndex !== -1) {
            keys.splice(scopeIndex, 1);
            scope = listeners.scope;
        }

        var priorityIndex = keys.indexOf('priority');
        if (priorityIndex !== -1) {
            keys.splice(priorityIndex, 1);
            priority = listeners.priority;
        }

        if (typeof priority === 'string' &&
            typeof _priorities[priority] !== 'undefined') {
            priority = _priorities[priority];
        } else {
            priority = _priorities.DEFAULT;
        }

        while (len--) {
            key = keys[len];

            if (!self._listeners[key]) {
                self._listeners[key] = [];
            }
            self._listeners[key].push([listeners[key], scope, priority]);
        }

    }

    /**
     * @cfg {Object} listeners Listeners.
     */
    return _parent.subclass({

        /**
         * @property {Number} priority. Prior to higher.
         * <p><ul>
            <li><b>CORE:</b> Core event priority (high)</li>
            <li><b>VIEWS:</b> Views event priority (medium)</li>
            <li><b>DEFAULT:</b> Default event priority (low)</li>
         * </ul></p>
         */
        priority: _priorities,

        /**
         * @property {Number} defaultPriority Default priority.
         */
        defaultPriority: 800,

        /**
         * Create new event manager.
         * @constructor
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {
            // <inline>

            var self = this;

            opts = opts || {};

            if (opts && opts.listeners) {
                //self.listeners = opts.listeners;
                _register_listeners(self, opts.listeners);
            }
            if (self.listeners) {
                _register_listeners(self, self.listeners);
            }
            self.wasListened = {};
            _setupEvents(self);

            if (opts.hasListeners) {
                self.hasListeners = (self.hasListeners || [])
                    .concat(opts.hasListeners);
            }

            if (self.hasListeners) {
                self.setupListeners(self.hasListeners, self);
            }
            // </inline>
        },

        addListeners: function(listeners) {
            _register_listeners(this, listeners);
        },

        
        /**
         * Initialize listeners for given event and renderer.
         *
         * @method setupListeners
         * @param {Array} eventNames List of event names.
         * @param { Roc.views.Template } renderer (optional) Renderer. Defaults to current class instance.
         */
        
        setupListeners: function(eventNames, renderer) {

            var self = this;

            if (self._listeners) {

                var name,
                    scope = self._listeners.scope || self,
                    i = eventNames.length;

                renderer = renderer || self;

                while (i--) {
                    name = eventNames[i];

                    if (self._listeners[name] &&
                        !self.wasListened[name]) {

                        self.wasListened[name] = true;
                        self._listeners[name].forEach(function(listener) {
                            renderer.on(name, listener[0],
                                (listener[1] || scope), listener[2]);
                        });

                    }

                }
            }
        },

        /**
         * Checks to see if this object has any listeners for a specified event.
         * @param {String} name Event name.
         * @return {Boolean} True if the event is being listened for, else false.
         */
        hasListener: function(name) {
            if (typeof this._listeners === 'object') {
                return (this._listeners[name] instanceof Array);
            }
            return false;
        },

        /**
         * Return listeners for given event name.
         *
         * @param {String} name Event name.
         * @return {Function} Listener callback for given event name.
         */
        getListener: function(name) {
            return this._listeners[name];
        },

        /**
         * Fires given event with given extra parameters.
         *
         * ## Example
         *
         *     // Registering event listener
         *     var eid = this.on('afterload', function(success, data) {
         *         console.log('Received data !', success, data);
         *     }, this);
         *
         *     // Fireing event
         *     this.fire('afterload', true, 'Some data');
         *
         *     // Unregistering event
         *     this.off(eid);
         *
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fire: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1),
                    toOff = [];

                while (++i < plen) {
                    events = self._events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function !');//, arguments.callee.callee, e);
                            toOff.push(events[j]);
                        } else if (e[2].apply(e[3], args) === false) {
                            return false;
                        }
                    }

                }

                toOff.forEach(function(uid) {
                    self.off(uid);
                });
                return true;
            }
        },

        /**
         * Same as fire but put `scope` as first arguments instead of scoping it when calling listeners.
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fireScope: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1),
                    toOff = [];

                while (++i < plen) {
                    events = self._events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function !');//, arguments.callee.callee, e);
                            toOff.push(events[j]);
                        } else if (e[2].apply(e[3], [e[3]].concat(args)) === false) {
                            return false;
                        }
                    }

                }

                toOff.forEach(function(uid) {
                    self.off(uid);
                });
                return true;
            }
        },

        /**
         * Register listener for given eventname.
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this, this.priority.VIEWS);
         *
         * @param {String} name Event name.
         * @param {Function} cb Listener callback.
         * @param {Object} scope (optional) Callback scope. Defaults to `window`.
         * @param {Number} priority (optional) Callback priority. Defaults to `Roc.Event.priority.DEFAULT`.
         * @return {Number} Returns listener uniq identifier `uid`.
         */
        on: function(name, cb, scope, priority) {

            var self = this;

            priority = priority || self.defaultPriority;

            if (typeof self._events[name] === 'undefined') {
                self._events[name] = {};
            }

            if (typeof self._events[name]['p' + priority] === 'undefined') {
                self._events[name]['p' + priority] = [];
            }

            if (typeof self.pqueue[name] === 'undefined') {
                self.pqueue[name] = [];
            }

            if (self.pqueue[name].indexOf(priority) === -1) {
                self.pqueue[name].push(priority);
                self.pqueue[name].sort(_numSort);
            }
            scope = scope || window;
            self.equeue['u' + (++self.uids)] = [name, priority, cb, scope];
            self._events[name]['p' + priority].push(self.uids);
            return self.uids;
        },

        has: function(name) {
            return _count_listeners(this, name);
        },

        /**
         * Unregister listener for given listener uniq identifier (returns by `on` method).
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this);
         *      // ...
         *      this.off(eid);
         *
         * @param {Number} uid Event listener uniq identifier (returns by `on` method).
         * @return {Mixed} Returns false if error else return event listener count.
         */
        off: function(uid) {

            var self = this,
                e = self.equeue['u' + uid];

            if (e) {

                var cb,
                    length,
                    name = e[0],
                    priority = e[1],
                    listeners = self._events[name]['p' + priority];

                listeners.splice(listeners.indexOf(uid), 1);
                length = listeners.length;

                delete self.equeue['u' + uid];

                if (!length) {
                    // cleaning hash tables
                    delete self._events[name]['p' + priority];
                    self.pqueue[name].splice(self.pqueue[name].indexOf(priority), 1);
                    if (!self.pqueue[name].length) {
                        delete self.pqueue[name];
                        delete self._events[name];
                        return 0;
                    }
                }
                return _count_listeners(self, name);
            }
            return false;
        }
    });

}());

/**
 * @class Roc.Templates
 * @extends Roc
 * @requires Handlebars
 */
Roc.Templates = new (function() {

    'use strict';

    // private
    var _ns = Roc,
        _handlebars = Handlebars,
        _request = _ns.Request;

    _handlebars.currentParent = [];

    /**
     * @ignore
     * Render given configuration element.
     *
     * ### Sample usage
     * 
     * Where items are for example:
     * items: [{ xtype: 'button', config: { text: 'Hello' }}]
     */
    // <unstable> Use it carefully. Still testing performance memory/cpu.
    _handlebars.registerHelper('render', function(config) {

        var html,
            parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = new (_ns.xtype(config.xtype))(config),
            renderer = ((parent.renderer && parent.renderer.el) ? parent.abstractRenderer : parent.renderer);

        /*
            abstractRenderer is used for calling afterrender event for item compile after
            the parent renderer has been rendered.
            For example: the list renders her children after render itself so we need to use
            the abstract renderer.
            !TODO We should store the item inside parents collections to be able to delete it or retrieve it.
        */
        if (!renderer) {
            renderer = parent.abstractRenderer = new _ns.Event();
        }

        html = item.compile(renderer.config, renderer);
        return new _handlebars.SafeString(html);
    });
    // </unstable>

    /**
     * @ignore
     * Get current config from given item children index.
     *
     * ### Sample usage
     * 
     */
    _handlebars.registerHelper('config', function(context, options) {

        var parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = (parent.items ? parent.items[context] : undefined);

        if (typeof item === 'object' && item.config) {
            return options.fn(item.config);
        }
        return options.fn(this);

    });

    // @ignore !TODO permettre d'avoir la config d'un item a partir de son index et/ou id
    /*_handlebars.registerHelper('getconfig', function() {

    });*/

    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });

    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });

    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });

    return _ns.subclass({

        /**
         * Create new templates class.
         */
        constructor: function() {
            this.path = '';
        },

        /**
         * Set path for loading external template files.
         * Used by {@link Roc.App#require application} class.
         * @param {String} path Path.
         */
        setPath: function(path) {
            this.path = path;
        },

        /**
         * Get template by given name.
         * @param {String} name Template name.
         * @return {Function} Template function.
         */
        get: function(name) {
            return _handlebars.templates[name];
        },

        /**
         * Compile given source or retrieve the template.
         * @param {Mixed} source The source.
         * @return {Function} Function corresponding to the given source.
         */
        compile: function(source) {

            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }

            return function() {
                return source;
            };
        }
    });
}())();

/**
 * @class Roc.Settings
 * @extends Roc.Event
 * @singleton
 *
 * ### Usage example

    var session = new Roc.Settings({
        username: 'mrsmith',
        logged: true
    });

    session.on('loggedchanged', function(s, n, value, oldValue) {
        if (value === false) {
            // show login
        } else {
            // hide login
        }
    }, this);

    session.set('logged', false);

 *
 */
Roc.Settings = (function() {

    'use strict';

    var _ns = Roc,
        _parent = _ns.Event;

    function with_storage(self, name) {
        if (self.storage) {
            if (self.storageKeys) {
                return (self.storageKeys.indexOf(name) !== -1);
            }
            return true;
        }
        return false;
    }

    return _parent.subclass({

        /**
         * New Settings.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(config, opts) {

            var self = this;

            self.vars = (config ? config : {});

            if (opts && opts.storage) {
                self.storage = opts.storage;
                if (opts.storageKeys) {
                    self.storageKeys = opts.storageKeys;
                    self.storageKeys.forEach(function(key) {
                        if (typeof self.vars[key] !== 'undefined') {
                            self.storage.setItem(key, self.vars[key]);
                            delete self.vars[key];
                        }
                    });
                } else {
                    delete self.vars;
                }
            }

            _parent.prototype.constructor.apply(self, arguments);
        },

        /**
         * Set value for given name.
         * @param {String} name Name.
         * @param {Mixed} value Value.
         * @param {Boolean} silent True to disable fire events. Defaults to false.
         * @fires settingschanged
         * @fires `name`changed
         */
        set: function(name, value, silent) {

            var self = this,
                oldValue = this.get(name);

            if (with_storage(self, name)) {
                self.storage.setItem(name, value);
            } else {
                self.vars[name] = value;
            }

            if (silent !== true) {
                self.fire('settingchanged', self, name, value, oldValue);
                self.fire(name + 'changed', self, name, value, oldValue);
            }
        },

        /**
         * Get value from given name.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        get: function(name, defaultValue) {

            var self = this;

            if (with_storage(self, name)) {
                return self.storage.getItem(name);
            } else if (self.vars[name]) {
                return self.vars[name];
            }

            return defaultValue;
        },

        /**
         * Get value from given name and delete it if exists.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        pop: function(name, defaultValue) {

            var self = this,
                value = self.get(name, defaultValue);

            self.empty(name);

            return value;

        },

        /**
         * Gets value from given names.
         *
         * ## Sample usage
         *
         *      var loginUrl = settings.gets('serverUrl', 'loginPath').join('');
         *
         * @param {String...} names Variable names.
         * @return {Array} Values for given names put into array.
         */
        gets: function() {

            var result = [],
                i = -1,
                length = arguments.length;

            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        },

        /**
         * @fires nameempty
         * Empty given variable name.
         * @param {String} name Variable name.
         * @param {Boolean} silent True to disable fire events. Defaults to false.
         */
        empty: function(name, silent) {

            var self = this;

            if (with_storage(self, name)) {
                self.storage.removeItem(name);
            }

            if (typeof self.vars[name] !== 'undefined') {
                delete self.vars[name];
                if (silent !== true) {
                    self.fire(name + 'empty', self, name);
                    self.fire(name + 'changed', self, name);
                }
            }
        }
    });

}());

/**
 * @class Roc.App
 * @extends Roc
 *
 * ### Sample usage:
 *
    var app = new Roc.App({
        onready: function() {
            var main = new App.controllers.Main();
        },
        defaultRoute: '/home',
        engine: 'JqueryMobile',
        ui: 'c'
    });
 *
 */
Roc.App = (function() {

    'use strict';

    var _ns = Roc,
        _debug = _ns.debug,
        _storage = _ns.Storage,
        _engines = _ns.engines;

    _ns.global = new _ns.Event();

    _ns.global.on = function(eventName, callback, scope) {

        if (['beforeready', 'ready'].indexOf(eventName) !== -1 &&
            ['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
            scope = scope || this;
            setTimeout(function() {
                callback.call(scope);
            }, 1);
        } else {
            return _ns.Event.prototype.on.apply(this, arguments);
        }

    };

    function _get_engine(name, defaultEngine) {

        var engine = _engines[name];

        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    }

    function _register_listener(self) {

        function ready() {
            if (!self.domReady) {
                document.removeEventListener('DOMContentLoaded', ready, false);
            }

            self.domReady = true;
            _ns.global.fire('beforeready');
            if (self.config && self.config.onready) self.config.onready();
            _ns.global.fire('ready');
            _ns.History.start();
        }

        if (['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
            self.domReady = true;
            setTimeout(ready, 10);
        } else {
            document.addEventListener('DOMContentLoaded', ready, false);
        }
    }

    /**
     * @cfg {Function} onready Callback when application ready to start.
     */

    /**
     * @cfg {String} engine Engine name. Defaults to 'Homemade'.
     */

    /**
     * @cfg {String} defaultRoute (optional) Defaults application route.
     */

    /**
     * @cfg {Boolean} debug (optional) Enables or disables debug mode.
     */

    /**
     * @cfg {Object} require (optional) Required components.
     *
     * ## Sample usage:
     *
     *      require: {
     *          templates: [
     *              'home',
     *              'menu',
     *              ...
     *          ]
     *      }
     *
     */

    /**
     * @cfg {String} ui (optional) Defaults ui for all components (can be override by component configuration).
     */
    return _ns.subclass({

        defaultEngine: 'Homemade',
        domReady: false,

        /**
         * Create new application.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _ns.History.setDefaultRoute(self.config.defaultRoute);
            }
            _register_listener(self);
            // Setting default engine to global configuration object.
            _ns.config.engine = _get_engine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _ns.config.ui = self.config.ui;
            }
        }
    });

})();

/**
 * @private
 * @class Roc.events.Abstract
 * @extends Roc
 * @xtype events.abstract
 *
 * Abstract event class. Allows DOM events to be merged together or standalone.
 *
 * ## Extending abstract class
 *
 *        Roc.events.Keypress = (function() {
 *
 *            var _parent = Roc.events.Abstract;
 *
 *            return _parent.subclass({
 *
 *                xtype: 'events.keypress',
 *                eventName: 'keypress',
 *                globalFwd: window,
 *
 *                constructor: _parent.prototype.constructor
 *
 *            });
 *
 *        }());
 *
 */
Roc.events.Abstract = (function() {

    'use strict';

    var _ns = Roc,
        _debug = _ns.debug,
        _eventsAttached = {},
        _events = {},
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    function _global_handler(name) {
        return function(e) {

            var handler = _events[name][e.target.id];

            if (handler) {
                handler(e);
            }
        };
    }

    function _global_attach(self) {

        if (!_eventsAttached[self.eventName]) {
            _eventsAttached[self.eventName] = true;
            self.globalFwd.addEventListener(self.eventName,
                _global_handler(self.eventName), self.useCapture);
        }
        if (!self.el.id || !self.el.id.length) {
            self.el.id = _ns.Selector.generateId();
        }
        if (!_events[self.eventName]) {
            _events[self.eventName] = {};
        }
        _events[self.eventName][self.el.id] = self.handler;

    }

    function _global_detach(self) {

        delete _events[self.eventName][self.el.id];
        if (_eventsAttached[self.eventName] &&
            !_events[self.eventName].length) {
            _eventsAttached[self.eventName] = false;
            self.globalFwd.removeEventListener(self.eventName,
                _global_handler, self.useCapture);
        }

    }

    return _ns.subclass({

        xtype: 'events.abstract',

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        useCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {String} eventName Browser event name.
         * @property {String} eventName Browser event name.
         * Should be override by super class or when constructor called.
         */
        eventName: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * @property {Object} globalFwd Global forward scope.
         * Used when we want to listen event on window, and
         * forward to target.
         */
        globalFwd: false,

        /**
         * New abstract event instance.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach',
                'globalFwd'
            ]);
            if (!self.eventName && opts.eventName) {
                self.eventName = opts.eventName;
            }
            self.defaultEl = opts.el || window;
            self.defaultScope = (opts.scope ? opts.scope : self);
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_attach(self);
                } else {
                    self.el.addEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_detach(self);
                } else {
                    self.el.removeEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        }
    });

})();

/**
 * @class Roc.events.Click
 * @extends Roc
 * @xtype events.click
 *
 * Click event class. Also remove 300ms delay for touch device.
 *
 * ## Example
 *
 *       var clickEvent = new Roc.events.Click({
 *           autoAttach: true,
 *           el: document.getElementById('mybutton'),
 *           scope: this,
 *           handler: showPopup
 *       });
 *
 */
Roc.events.Click = (function() {

    'use strict';

    // private
    var _ns = Roc;
        //_isMobile = ('ontouchstart' in document.documentElement)


    function _no_handler() {
        console.warn('click', 'No handler is define !');
    }

    function _stop_scroll(e) {
        e.preventDefault();
    }

    // public
    return _ns.subclass({

        xtype: 'events.click',

        /**
         * @cfg {Boolean} disableScrolling (optional) Enable or disable scrolling on attached element.
         * Defaults to `false`.
         */
        defaultDisableScrolling: false,

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        defaultUseCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * New click event.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.disableScrolling = opts.disableScrolling || self.defaultDisableScrolling;
            self.useCapture = opts.useCapture || self.defaultUseCapture;
            _ns.utils.apply(self, opts, [
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);

            /**
             * @property {Boolean} attached True if event is attached.
             */
            self.attached = false;

            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);

            self.el.addEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.addEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }

            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;

            self.el.removeEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.removeEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        }
    });
}());

/**
 * @class Roc.events.Scroll
 * @extends Roc
 * @xtype events.scroll
 *
 * Scroll event class.
 *
 * ## Example
 *
 *       var scrollEvent = new Roc.events.Scroll({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: loadMoreItem
 *       });
 *
 * @deprecated If you use Roc.physics.Scroll, this event won't be fire.
 */
Roc.events.Scroll = (function() {

    'use strict';

    // private
    var _lastEvent,
        _intervalId,
        _ns = Roc,
        _debug = _ns.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,//_window.screen.availHeight,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _ns.Event();

    function _noHandler() {
        _debug.error('scroll', '[Roc.events.Scroll] No handler is define !');
    }

    function _poolHandler() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    }

    function _windowScrollListener() {
        _scrolled = true;
        _lastEvent = event;
    }

    function _windowResizeListener() {
        _screenHeight = _doc.height;//_window.screen.availHeight;
    }



    // public
    return _ns.subclass({

        xtype: 'events.scroll',
        defaultInterval: 167, // 60fps
        useCapture: false,
        autoAttach: false,
        // defaults
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.interval = opts.interval || self.defaultInterval;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _intervalId = _window.setInterval(_poolHandler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('scroll', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;

            var length = _events.off(self.euid);

            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();

/**
 * @class Roc.events.Resize
 * @extends Roc
 * @xtype events.resize
 *
 * Resize event class.
 *
 * ## Example
 *
 *       var resizeEvent = new Roc.events.Resize({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: updateLayout
 *       });
 *
 */
Roc.events.Resize = (function() {

    'use strict';

    // private
    var //_lastEvent,
        //_intervalId,
        _window = window,
        _eventsAttached = false,
        //_resized = false,
        _ns = Roc,
        _debug = _ns.debug,
        _events = new _ns.Event();

    function _no_handler() {
        _debug.warn('events.resize', 'No handler is define !');
    }

    // function _pool_handler() {
    //     if (_resized === true) {
    //         _resized = false;
    //         _events.fire('resize', _lastEvent);
    //     }
    // }

    function _window_resize_listener(e) {
        // _resized = true;
        // _lastEvent = e;
        _events.fire('resize', e);
    }

    // public
    return _ns.subclass({

        xtype: 'events.resize',
        //defaultInterval: 500,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            //self.interval = opts.interval || self.defaultInterval;
            self.attached = false;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _window_resize_listener, false);
                //_intervalId = _window.setInterval(_pool_handler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ? handler : self.defaultHandler);
            self.euid = _events.on('resize', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;

            var length = _events.off(self.euid);

            delete self.handler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _window_resize_listener, false);
            //_window.clearInterval(_intervalId);
        }
    });
})();

/**
 * var nobounce = new Roc.events.Nobounce({
     className: 'nobounce', // optional
     autoAttach: true, // optional
     el: document // optional
   });
 * nobounce.attach(document);
 * nobounce.detach(document);
 */
Roc.events.Nobounce = (function() {

    // private
    var _lastEvent,
        _ns = Roc,
        $ = _ns.Selector,
        _doc = document,
        _parent = _ns;

    var _isParentNoBounce = function(el, className) {
        while (el) {
            if ($.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _disableBounce = function(e) {
        if (_isParentNoBounce(e.target, this.className)) {
            e.preventDefault();
        }
    };

    // public
    return _parent.subclass({

        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.autoAttach = opts.autoAttach || self.defaultAutoAttach;
            self.className = opts.className || self.defaultClassName;
            self.el = opts.el || self.defaultEl;
            self.attached = false;
            self.handler = _disableBounce.bind(self);
            if (self.autoAttach === true) {
                self.attach(self.el);
            }
        },

        attach: function(cmp) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.el;
            self.el.addEventListener('touchmove', self.handler, false);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;
            self.el.removeEventListener('touchmove', self.handler, false);
        }
    });
})();

/**
 * @class Roc.events.Focus
 * @extends Roc.events.Abstract
 * @xtype events.focus
 *
 * Input focus event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var focusEvent = new Roc.events.Focus({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on focus called
 *             Roc.Selector.addClass(focusEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
Roc.events.Focus = (function() {

    'use strict';

    // private
    var _ns = Roc,
        //isAndroid = (navigator.userAgent.indexOf('Android ') !== -1),
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.focus',

        /**
         * @readonly
         * @property {String} eventName Browser event name.
         */
        eventName: 'focusin',//(isAndroid === true ? 'focus' : 'focusin'),

        /**
         * @readonly
         * @property {Object} globalFwd Global forward scope.
         */
        globalFwd: window,//(isAndroid === true ? undefined : window),

        /**
         * New focus event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class Roc.events.Blur
 * @extends Roc.events.Abstract
 * @xtype events.blur
 *
 * Input blur event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var blurEvent = new Roc.events.Blur({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on blur called
 *             Roc.Selector.removeClass(blurEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
Roc.events.Blur = (function() {

    // private
    var _ns = Roc,
        //isAndroid = (navigator.userAgent.indexOf('Android ') !== -1),
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.blur',

        /**
         * @readonly
         */
        eventName: 'focusout',//(isAndroid === true ? 'blur' : 'focusout'),

        /**
         * @readonly
         */
        globalFwd: window,//(isAndroid === true ? undefined : window),

        /**
         * New blur event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class Roc.events.Keypress
 * @extends Roc.events.Abstract
 * @xtype events.keypress
 *
 * Input keypress event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var keypressEvent = new Roc.events.Keypress({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on keypress called
 *             if (e.keyCode === 13) {
 *                 this.form.submit();
 *             }
 *         }
 *     });
 *
 */
Roc.events.Keypress = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.keypress',

        /**
         * @readonly
         */
        eventName: 'keypress',

        /**
         * @readonly
         */
        globalFwd: window,

        /**
         * New keypress event.
         */
        constructor: function(opts) {

            var self = this;


            if (opts.el) {

                var tagname = opts.el.tagName;

                if (tagname !== 'INPUT' && tagname !== 'TEXTAREA') {
                    opts.el.setAttribute('tabindex', '1');
                }
            }

            _parent.prototype.constructor.call(self, opts);

        }
    });
}());

window.requestAnimationFrame || (window.requestAnimationFrame =
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(callback, element) {
        return window.setTimeout(function() {
            callback(+new Date());
        }, 1000 / 60);
});
/*
 * Scroller
 * http://github.com/zynga/scroller
 *
 * Copyright 2011, Zynga Inc.
 * Licensed under the MIT License.
 * https://raw.github.com/zynga/scroller/master/MIT-LICENSE.txt
 *
 * Based on the work of: Unify Project (unify-project.org)
 * http://unify-project.org
 * Copyright 2011, Deutsche Telekom AG
 * License: MIT + Apache (V2)
 */

/**
 * @ignore
 * Generic animation class with support for dropped frames both optional easing and duration.
 *
 * Optional duration is useful when the lifetime is defined by another condition than time
 * e.g. speed of an animating object, etc.
 *
 * Dropped frame logic allows to keep using the same updater logic independent from the actual
 * rendering. This eases a lot of cases where it might be pretty complex to break down a state
 * based on the pure time difference.
 */
/**
 * @private
 * @class Animate
 */
(function(global) {
	var time = Date.now || function() {
		return +new Date();
	};
	var desiredFrames = 60;
	var millisecondsPerSecond = 1000;
	var running = {};
	var counter = 1;

	// Create namespaces
	if (!global.core) {
		global.core = { effect : {} };

	} else if (!core.effect) {
		core.effect = {};
	}

	core.effect.Animate = {

		/**
		 * @ignore
		 * Stops the given animation.
		 *
		 * @param id {Integer} Unique animation ID
		 * @return {Boolean} Whether the animation was stopped (aka, was running before)
		 */
		stop: function(id) {
			var cleared = running[id] != null;
			if (cleared) {
				running[id] = null;
			}

			return cleared;
		},


		/**
		 * @ignore
		 * Whether the given animation is still running.
		 *
		 * @param id {Integer} Unique animation ID
		 * @return {Boolean} Whether the animation is still running
		 */
		isRunning: function(id) {
			return running[id] != null;
		},


		/**
		 * @ignore
		 * Start the animation.
		 *
		 * @param stepCallback {Function} Pointer to function which is executed on every step.
		 *   Signature of the method should be `function(percent, now, virtual) { return continueWithAnimation; }`
		 * @param verifyCallback {Function} Executed before every animation step.
		 *   Signature of the method should be `function() { return continueWithAnimation; }`
		 * @param completedCallback {Function}
		 *   Signature of the method should be `function(droppedFrames, finishedAnimation) {}`
		 * @param duration {Integer} Milliseconds to run the animation
		 * @param easingMethod {Function} Pointer to easing function
		 *   Signature of the method should be `function(percent) { return modifiedValue; }`
		 * @param root {Element ? document.body} Render root, when available. Used for internal
		 *   usage of requestAnimationFrame.
		 * @return {Integer} Identifier of animation. Can be used to stop it any time.
		 */
		start: function(stepCallback, verifyCallback, completedCallback, duration, easingMethod, root) {

			var start = time();
			var lastFrame = start;
			var percent = 0;
			var dropCounter = 0;
			var id = counter++;
			var win = window;

			if (!root) {
				root = document.body;
			}

			// Compacting running db automatically every few new animations
			if (id % 20 === 0) {
				var newRunning = {};
				for (var usedId in running) {
					newRunning[usedId] = true;
				}
				running = newRunning;
			}

			// This is the internal step method which is called every few milliseconds
			var step = function(virtual) {

				// Normalize virtual value
				var render = virtual !== true;

				// Get current time
				var now = time();

				// Verification is executed before next animation step
				if (!running[id] || (verifyCallback && !verifyCallback(id))) {

					running[id] = null;
					completedCallback && completedCallback(desiredFrames - (dropCounter / ((now - start) / millisecondsPerSecond)), id, false);
					return;

				}

				// For the current rendering to apply let's update omitted steps in memory.
				// This is important to bring internal state variables up-to-date with progress in time.
				if (render) {

					var droppedFrames = Math.round((now - lastFrame) / (millisecondsPerSecond / desiredFrames)) - 1;
					for (var j = 0; j < Math.min(droppedFrames, 4); j++) {
						step(true);
						dropCounter++;
					}

				}

				// Compute percent value
				if (duration) {
					percent = (now - start) / duration;
					if (percent > 1) {
						percent = 1;
					}
				}

				// Execute step callback, then...
				var value = easingMethod ? easingMethod(percent) : percent;
				if ((stepCallback(value, now, render) === false || percent === 1) && render) {
					running[id] = null;
					completedCallback && completedCallback(desiredFrames - (dropCounter / ((now - start) / millisecondsPerSecond)), id, percent === 1 || duration == null);
				} else if (render) {
					lastFrame = now;
					//core.effect.Animate.requestAnimationFrame(step, root);
					win.requestAnimationFrame(step, root);
				}
			};

			// Mark as running
			running[id] = true;

			// Init first step
			//core.effect.Animate.requestAnimationFrame(step, root);
			win.requestAnimationFrame(step, root);

			// Return unique animation ID
			return id;
		}
	};
})(this);


/*
 * Scroller
 * http://github.com/zynga/scroller
 *
 * Copyright 2011, Zynga Inc.
 * Licensed under the MIT License.
 * https://raw.github.com/zynga/scroller/master/MIT-LICENSE.txt
 *
 * Based on the work of: Unify Project (unify-project.org)
 * http://unify-project.org
 * Copyright 2011, Deutsche Telekom AG
 * License: MIT + Apache (V2)
 */

/**
 * @private
 * @class Scroller
 */
var Scroller;

(function() {
	var NOOP = function(){};

	/**
	 * @ignore
	 * A pure logic 'component' for 'virtual' scrolling/zooming.
	 */
	Scroller = function(callback, options) {

		this.__callback = callback;

		this.options = {

			/** Enable scrolling on x-axis */
			scrollingX: true,

			/** Enable scrolling on y-axis */
			scrollingY: true,

			/** Enable animations for deceleration, snap back, zooming and scrolling */
			animating: true,

			/** duration for animations triggered by scrollTo/zoomTo */
			animationDuration: 250,

			/** Enable bouncing (content can be slowly moved outside and jumps back after releasing) */
			bouncing: true,

			/** Enable locking to the main axis if user moves only slightly on one of them at start */
			locking: true,

			/** Enable pagination mode (switching between full page content panes) */
			paging: false,

			/** Enable snapping of content to a configured pixel grid */
			snapping: false,

			/** Enable zooming of content via API, fingers and mouse wheel */
			zooming: false,

			/** Minimum zoom level */
			minZoom: 0.5,

			/** Maximum zoom level */
			maxZoom: 3,

			/** Multiply or decrease scrolling speed **/
			speedMultiplier: 1,

			/** Callback that is fired on the later of touch end or deceleration end,
				provided that another scrolling action has not begun. Used to know
				when to fade out a scrollbar. */
			scrollingComplete: NOOP,

			/** This configures the amount of change applied to deceleration when reaching boundaries  **/
            penetrationDeceleration : 0.03,

            /** This configures the amount of change applied to acceleration when reaching boundaries  **/
            penetrationAcceleration : 0.08

		};

		for (var key in options) {
			this.options[key] = options[key];
		}

	};


	// Easing Equations (c) 2003 Robert Penner, all rights reserved.
	// Open source under the BSD License.

	/**
	 * @param pos {Number} position between 0 (start of effect) and 1 (end of effect)
	**/
	var easeOutCubic = function(pos) {
		return (Math.pow((pos - 1), 3) + 1);
	};

	/**
	 * @param pos {Number} position between 0 (start of effect) and 1 (end of effect)
	**/
	var easeInOutCubic = function(pos) {
		if ((pos /= 0.5) < 1) {
			return 0.5 * Math.pow(pos, 3);
		}

		return 0.5 * (Math.pow((pos - 2), 3) + 2);
	};


	var members = {

		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: STATUS
		---------------------------------------------------------------------------
		*/

		/** {Boolean} Whether only a single finger is used in touch handling */
		__isSingleTouch: false,

		/** {Boolean} Whether a touch event sequence is in progress */
		__isTracking: false,

		/** {Boolean} Whether a deceleration animation went to completion. */
		__didDecelerationComplete: false,

		/**
		 * {Boolean} Whether a gesture zoom/rotate event is in progress. Activates when
		 * a gesturestart event happens. This has higher priority than dragging.
		 */
		__isGesturing: false,

		/**
		 * {Boolean} Whether the user has moved by such a distance that we have enabled
		 * dragging mode. Hint: It's only enabled after some pixels of movement to
		 * not interrupt with clicks etc.
		 */
		__isDragging: false,

		/**
		 * {Boolean} Not touching and dragging anymore, and smoothly animating the
		 * touch sequence using deceleration.
		 */
		__isDecelerating: false,

		/**
		 * {Boolean} Smoothly animating the currently configured change
		 */
		__isAnimating: false,



		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: DIMENSIONS
		---------------------------------------------------------------------------
		*/

		/** {Integer} Available outer left position (from document perspective) */
		__clientLeft: 0,

		/** {Integer} Available outer top position (from document perspective) */
		__clientTop: 0,

		/** {Integer} Available outer width */
		__clientWidth: 0,

		/** {Integer} Available outer height */
		__clientHeight: 0,

		/** {Integer} Outer width of content */
		__contentWidth: 0,

		/** {Integer} Outer height of content */
		__contentHeight: 0,

		/** {Integer} Snapping width for content */
		__snapWidth: 100,

		/** {Integer} Snapping height for content */
		__snapHeight: 100,

		/** {Integer} Height to assign to refresh area */
		__refreshHeight: null,

		/** {Boolean} Whether the refresh process is enabled when the event is released now */
		__refreshActive: false,

		/** {Function} Callback to execute on activation. This is for signalling the user about a refresh is about to happen when he release */
		__refreshActivate: null,

		/** {Function} Callback to execute on deactivation. This is for signalling the user about the refresh being cancelled */
		__refreshDeactivate: null,

		/** @ignore {Function} Callback to execute to start the actual refresh. Call {@link #refreshFinish} when done */
		__refreshStart: null,

		/** {Number} Zoom level */
		__zoomLevel: 1,

		/** {Number} Scroll position on x-axis */
		__scrollLeft: 0,

		/** {Number} Scroll position on y-axis */
		__scrollTop: 0,

		/** {Integer} Maximum allowed scroll position on x-axis */
		__maxScrollLeft: 0,

		/** {Integer} Maximum allowed scroll position on y-axis */
		__maxScrollTop: 0,

		/* {Number} Scheduled left position (final position when animating) */
		__scheduledLeft: 0,

		/* {Number} Scheduled top position (final position when animating) */
		__scheduledTop: 0,

		/* {Number} Scheduled zoom level (final scale when animating) */
		__scheduledZoom: 0,



		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: LAST POSITIONS
		---------------------------------------------------------------------------
		*/

		/** {Number} Left position of finger at start */
		__lastTouchLeft: null,

		/** {Number} Top position of finger at start */
		__lastTouchTop: null,

		/** {Date} Timestamp of last move of finger. Used to limit tracking range for deceleration speed. */
		__lastTouchMove: null,

		/** {Array} List of positions, uses three indexes for each state: left, top, timestamp */
		__positions: null,



		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: DECELERATION SUPPORT
		---------------------------------------------------------------------------
		*/

		/** {Integer} Minimum left scroll position during deceleration */
		__minDecelerationScrollLeft: null,

		/** {Integer} Minimum top scroll position during deceleration */
		__minDecelerationScrollTop: null,

		/** {Integer} Maximum left scroll position during deceleration */
		__maxDecelerationScrollLeft: null,

		/** {Integer} Maximum top scroll position during deceleration */
		__maxDecelerationScrollTop: null,

		/** {Number} Current factor to modify horizontal scroll position with on every step */
		__decelerationVelocityX: null,

		/** {Number} Current factor to modify vertical scroll position with on every step */
		__decelerationVelocityY: null,



		/*
		---------------------------------------------------------------------------
			PUBLIC API
		---------------------------------------------------------------------------
		*/

		/**
		 * Configures the dimensions of the client (outer) and content (inner) elements.
		 * Requires the available space for the outer element and the outer size of the inner element.
		 * All values which are falsy (null or zero etc.) are ignored and the old value is kept.
		 *
		 * @param clientWidth {Integer ? null} Inner width of outer element
		 * @param clientHeight {Integer ? null} Inner height of outer element
		 * @param contentWidth {Integer ? null} Outer width of inner element
		 * @param contentHeight {Integer ? null} Outer height of inner element
		 */
		setDimensions: function(clientWidth, clientHeight, contentWidth, contentHeight) {

			var self = this;

			// Only update values which are defined
			if (clientWidth === +clientWidth) {
				self.__clientWidth = clientWidth;
			}

			if (clientHeight === +clientHeight) {
				self.__clientHeight = clientHeight;
			}

			if (contentWidth === +contentWidth) {
				self.__contentWidth = contentWidth;
			}

			if (contentHeight === +contentHeight) {
				self.__contentHeight = contentHeight;
			}

			// Refresh maximums
			self.__computeScrollMax();

			// Refresh scroll position
			self.scrollTo(self.__scrollLeft, self.__scrollTop, true);

		},


		/**
		 * Sets the client coordinates in relation to the document.
		 *
		 * @param left {Integer ? 0} Left position of outer element
		 * @param top {Integer ? 0} Top position of outer element
		 */
		setPosition: function(left, top) {

			var self = this;

			self.__clientLeft = left || 0;
			self.__clientTop = top || 0;

		},


		/**
		 * Configures the snapping (when snapping is active)
		 *
		 * @param width {Integer} Snapping width
		 * @param height {Integer} Snapping height
		 */
		setSnapSize: function(width, height) {

			var self = this;

			self.__snapWidth = width;
			self.__snapHeight = height;

		},


		/**
		 * Activates pull-to-refresh. A special zone on the top of the list to start a list refresh whenever
		 * the user event is released during visibility of this zone. This was introduced by some apps on iOS like
		 * the official Twitter client.
		 *
		 * @param height {Integer} Height of pull-to-refresh zone on top of rendered list
		 * @param activateCallback {Function} Callback to execute on activation. This is for signalling the user about a refresh is about to happen when he release.
		 * @param deactivateCallback {Function} Callback to execute on deactivation. This is for signalling the user about the refresh being cancelled.
		 * @param startCallback {Function} Callback to execute to start the real async refresh action. Call {@link #finishPullToRefresh} after finish of refresh.
		 */
		activatePullToRefresh: function(height, activateCallback, deactivateCallback, startCallback) {

			var self = this;

			self.__refreshHeight = height;
			self.__refreshActivate = activateCallback;
			self.__refreshDeactivate = deactivateCallback;
			self.__refreshStart = startCallback;

		},


		/**
		 * Starts pull-to-refresh manually.
		 */
		triggerPullToRefresh: function() {
			// Use publish instead of scrollTo to allow scrolling to out of boundary position
			// We don't need to normalize scrollLeft, zoomLevel, etc. here because we only y-scrolling when pull-to-refresh is enabled
			this.__publish(this.__scrollLeft, -this.__refreshHeight, this.__zoomLevel, true);

			if (this.__refreshStart) {
				this.__refreshStart();
			}
		},


		/**
		 * Signalizes that pull-to-refresh is finished.
		 */
		finishPullToRefresh: function() {

			var self = this;

			self.__refreshActive = false;
			if (self.__refreshDeactivate) {
				self.__refreshDeactivate();
			}

			self.scrollTo(self.__scrollLeft, self.__scrollTop, true);

		},


		/**
		 * Returns the scroll position and zooming values
		 * @ignore
		 * @return {Map} `left` and `top` scroll position and `zoom` level
		 */
		getValues: function() {

			var self = this;

			return {
				left: self.__scrollLeft,
				top: self.__scrollTop,
				zoom: self.__zoomLevel
			};

		},


		/**
		 * Returns the maximum scroll values
		 * @ignore
		 * @return {Map} `left` and `top` maximum scroll values
		 */
		getScrollMax: function() {

			var self = this;

			return {
				left: self.__maxScrollLeft,
				top: self.__maxScrollTop
			};

		},


		/**
		 * Zooms to the given level. Supports optional animation. Zooms
		 * the center when no coordinates are given.
		 * @ignore
		 * @param level {Number} Level to zoom to
		 * @param animate {Boolean ? false} Whether to use animation
		 * @param originLeft {Number ? null} Zoom in at given left coordinate
		 * @param originTop {Number ? null} Zoom in at given top coordinate
		 * @param callback {Function ? null} A callback that gets fired when the zoom is complete.
		 */
		zoomTo: function(level, animate, originLeft, originTop, callback) {

			var self = this;

			if (!self.options.zooming) {
				throw new Error("Zooming is not enabled!");
			}

			// Add callback if exists
			if(callback) {
				self.__zoomComplete = callback;
			}

			// Stop deceleration
			if (self.__isDecelerating) {
				core.effect.Animate.stop(self.__isDecelerating);
				self.__isDecelerating = false;
			}

			var oldLevel = self.__zoomLevel;

			// Normalize input origin to center of viewport if not defined
			if (originLeft == null) {
				originLeft = self.__clientWidth / 2;
			}

			if (originTop == null) {
				originTop = self.__clientHeight / 2;
			}

			// Limit level according to configuration
			level = Math.max(Math.min(level, self.options.maxZoom), self.options.minZoom);

			// Recompute maximum values while temporary tweaking maximum scroll ranges
			self.__computeScrollMax(level);

			// Recompute left and top coordinates based on new zoom level
			var left = ((originLeft + self.__scrollLeft) * level / oldLevel) - originLeft;
			var top = ((originTop + self.__scrollTop) * level / oldLevel) - originTop;

			// Limit x-axis
			if (left > self.__maxScrollLeft) {
				left = self.__maxScrollLeft;
			} else if (left < 0) {
				left = 0;
			}

			// Limit y-axis
			if (top > self.__maxScrollTop) {
				top = self.__maxScrollTop;
			} else if (top < 0) {
				top = 0;
			}

			// Push values out
			self.__publish(left, top, level, animate);

		},


		/**
		 * Zooms the content by the given factor.
		 *
		 * @param factor {Number} Zoom by given factor
		 * @param animate {Boolean ? false} Whether to use animation
		 * @param originLeft {Number ? 0} Zoom in at given left coordinate
		 * @param originTop {Number ? 0} Zoom in at given top coordinate
		 * @param callback {Function ? null} A callback that gets fired when the zoom is complete.
		 */
		zoomBy: function(factor, animate, originLeft, originTop, callback) {

			var self = this;

			self.zoomTo(self.__zoomLevel * factor, animate, originLeft, originTop, callback);

		},


		/**
		 * Scrolls to the given position. Respect limitations and snapping automatically.
		 *
		 * @param left {Number?null} Horizontal scroll position, keeps current if value is <code>null</code>
		 * @param top {Number?null} Vertical scroll position, keeps current if value is <code>null</code>
		 * @param animate {Boolean?false} Whether the scrolling should happen using an animation
		 * @param zoom {Number?null} Zoom level to go to
		 */
		scrollTo: function(left, top, animate, zoom) {

			var self = this;

			// Stop deceleration
			if (self.__isDecelerating) {
				core.effect.Animate.stop(self.__isDecelerating);
				self.__isDecelerating = false;
			}

			// Correct coordinates based on new zoom level
			if (zoom != null && zoom !== self.__zoomLevel) {

				if (!self.options.zooming) {
					throw new Error("Zooming is not enabled!");
				}

				left *= zoom;
				top *= zoom;

				// Recompute maximum values while temporary tweaking maximum scroll ranges
				self.__computeScrollMax(zoom);

			} else {

				// Keep zoom when not defined
				zoom = self.__zoomLevel;

			}

			if (!self.options.scrollingX) {

				left = self.__scrollLeft;

			} else {

				if (self.options.paging) {
					left = Math.round(left / self.__clientWidth) * self.__clientWidth;
				} else if (self.options.snapping) {
					left = Math.round(left / self.__snapWidth) * self.__snapWidth;
				}

			}

			if (!self.options.scrollingY) {

				top = self.__scrollTop;

			} else {

				if (self.options.paging) {
					top = Math.round(top / self.__clientHeight) * self.__clientHeight;
				} else if (self.options.snapping) {
					top = Math.round(top / self.__snapHeight) * self.__snapHeight;
				}

			}

			// Limit for allowed ranges
			left = Math.max(Math.min(self.__maxScrollLeft, left), 0);
			top = Math.max(Math.min(self.__maxScrollTop, top), 0);

			// Don't animate when no change detected, still call publish to make sure
			// that rendered position is really in-sync with internal data
			if (left === self.__scrollLeft && top === self.__scrollTop) {
				animate = false;
			}

			// Publish new values
			self.__publish(left, top, zoom, animate);

		},


		/**
		 * Scroll by the given offset
		 *
		 * @param left {Number ? 0} Scroll x-axis by given offset
		 * @param top {Number ? 0} Scroll x-axis by given offset
		 * @param animate {Boolean ? false} Whether to animate the given change
		 */
		scrollBy: function(left, top, animate) {

			var self = this;

			var startLeft = self.__isAnimating ? self.__scheduledLeft : self.__scrollLeft;
			var startTop = self.__isAnimating ? self.__scheduledTop : self.__scrollTop;

			self.scrollTo(startLeft + (left || 0), startTop + (top || 0), animate);

		},



		/*
		---------------------------------------------------------------------------
			EVENT CALLBACKS
		---------------------------------------------------------------------------
		*/

		/**
		 * Mouse wheel handler for zooming support
		 */
		doMouseZoom: function(wheelDelta, timeStamp, pageX, pageY) {

			var self = this;
			var change = wheelDelta > 0 ? 0.97 : 1.03;

			return self.zoomTo(self.__zoomLevel * change, false, pageX - self.__clientLeft, pageY - self.__clientTop);

		},


		/**
		 * Touch start handler for scrolling support
		 */
		doTouchStart: function(touches, timeStamp) {

			// Array-like check is enough here
			if (touches.length == null) {
				throw new Error("Invalid touch list: " + touches);
			}

			if (timeStamp instanceof Date) {
				timeStamp = timeStamp.valueOf();
			}
			if (typeof timeStamp !== "number") {
				throw new Error("Invalid timestamp value: " + timeStamp);
			}

			var self = this;

			// Reset interruptedAnimation flag
			self.__interruptedAnimation = true;

			// Stop deceleration
			if (self.__isDecelerating) {
				core.effect.Animate.stop(self.__isDecelerating);
				self.__isDecelerating = false;
				self.__interruptedAnimation = true;
			}

			// Stop animation
			if (self.__isAnimating) {
				core.effect.Animate.stop(self.__isAnimating);
				self.__isAnimating = false;
				self.__interruptedAnimation = true;
			}

			// Use center point when dealing with two fingers
			var currentTouchLeft, currentTouchTop;
			var isSingleTouch = touches.length === 1;
			if (isSingleTouch) {
				currentTouchLeft = touches[0].pageX;
				currentTouchTop = touches[0].pageY;
			} else {
				currentTouchLeft = Math.abs(touches[0].pageX + touches[1].pageX) / 2;
				currentTouchTop = Math.abs(touches[0].pageY + touches[1].pageY) / 2;
			}

			// Store initial positions
			self.__initialTouchLeft = currentTouchLeft;
			self.__initialTouchTop = currentTouchTop;

			// Store current zoom level
			self.__zoomLevelStart = self.__zoomLevel;

			// Store initial touch positions
			self.__lastTouchLeft = currentTouchLeft;
			self.__lastTouchTop = currentTouchTop;

			// Store initial move time stamp
			self.__lastTouchMove = timeStamp;

			// Reset initial scale
			self.__lastScale = 1;

			// Reset locking flags
			self.__enableScrollX = !isSingleTouch && self.options.scrollingX;
			self.__enableScrollY = !isSingleTouch && self.options.scrollingY;

			// Reset tracking flag
			self.__isTracking = true;

			// Reset deceleration complete flag
			self.__didDecelerationComplete = false;

			// Dragging starts directly with two fingers, otherwise lazy with an offset
			self.__isDragging = !isSingleTouch;

			// Some features are disabled in multi touch scenarios
			self.__isSingleTouch = isSingleTouch;

			// Clearing data structure
			self.__positions = [];

		},


		/**
		 * Touch move handler for scrolling support
		 */
		doTouchMove: function(touches, timeStamp, scale) {

			// Array-like check is enough here
			if (touches.length == null) {
				throw new Error("Invalid touch list: " + touches);
			}

			if (timeStamp instanceof Date) {
				timeStamp = timeStamp.valueOf();
			}
			if (typeof timeStamp !== "number") {
				throw new Error("Invalid timestamp value: " + timeStamp);
			}

			var self = this;

			// Ignore event when tracking is not enabled (event might be outside of element)
			if (!self.__isTracking) {
				return;
			}


			var currentTouchLeft, currentTouchTop;

			// Compute move based around of center of fingers
			if (touches.length === 2) {
				currentTouchLeft = Math.abs(touches[0].pageX + touches[1].pageX) / 2;
				currentTouchTop = Math.abs(touches[0].pageY + touches[1].pageY) / 2;
			} else {
				currentTouchLeft = touches[0].pageX;
				currentTouchTop = touches[0].pageY;
			}

			var positions = self.__positions;

			// Are we already is dragging mode?
			if (self.__isDragging) {

				// Compute move distance
				var moveX = currentTouchLeft - self.__lastTouchLeft;
				var moveY = currentTouchTop - self.__lastTouchTop;

				// Read previous scroll position and zooming
				var scrollLeft = self.__scrollLeft;
				var scrollTop = self.__scrollTop;
				var level = self.__zoomLevel;

				// Work with scaling
				if (scale != null && self.options.zooming) {

					var oldLevel = level;

					// Recompute level based on previous scale and new scale
					level = level / self.__lastScale * scale;

					// Limit level according to configuration
					level = Math.max(Math.min(level, self.options.maxZoom), self.options.minZoom);

					// Only do further compution when change happened
					if (oldLevel !== level) {

						// Compute relative event position to container
						var currentTouchLeftRel = currentTouchLeft - self.__clientLeft;
						var currentTouchTopRel = currentTouchTop - self.__clientTop;

						// Recompute left and top coordinates based on new zoom level
						scrollLeft = ((currentTouchLeftRel + scrollLeft) * level / oldLevel) - currentTouchLeftRel;
						scrollTop = ((currentTouchTopRel + scrollTop) * level / oldLevel) - currentTouchTopRel;

						// Recompute max scroll values
						self.__computeScrollMax(level);

					}
				}

				if (self.__enableScrollX) {

					scrollLeft -= moveX * this.options.speedMultiplier;
					var maxScrollLeft = self.__maxScrollLeft;

					if (scrollLeft > maxScrollLeft || scrollLeft < 0) {

						// Slow down on the edges
						if (self.options.bouncing) {

							scrollLeft += (moveX / 2  * this.options.speedMultiplier);

						} else if (scrollLeft > maxScrollLeft) {

							scrollLeft = maxScrollLeft;

						} else {

							scrollLeft = 0;

						}
					}
				}

				// Compute new vertical scroll position
				if (self.__enableScrollY) {

					scrollTop -= moveY * this.options.speedMultiplier;
					var maxScrollTop = self.__maxScrollTop;

					if (scrollTop > maxScrollTop || scrollTop < 0) {

						// Slow down on the edges
						if (self.options.bouncing) {

							scrollTop += (moveY / 2 * this.options.speedMultiplier);

							// Support pull-to-refresh (only when only y is scrollable)
							if (!self.__enableScrollX && self.__refreshHeight != null) {

								if (!self.__refreshActive && scrollTop <= -self.__refreshHeight) {

									self.__refreshActive = true;
									if (self.__refreshActivate) {
										self.__refreshActivate();
									}

								} else if (self.__refreshActive && scrollTop > -self.__refreshHeight) {

									self.__refreshActive = false;
									if (self.__refreshDeactivate) {
										self.__refreshDeactivate();
									}

								}
							}

						} else if (scrollTop > maxScrollTop) {

							scrollTop = maxScrollTop;

						} else {

							scrollTop = 0;

						}
					}
				}

				// Keep list from growing infinitely (holding min 10, max 20 measure points)
				if (positions.length > 60) {
					positions.splice(0, 30);
				}

				// Track scroll movement for decleration
				positions.push(scrollLeft, scrollTop, timeStamp);

				// Sync scroll position
				self.__publish(scrollLeft, scrollTop, level);

			// Otherwise figure out whether we are switching into dragging mode now.
			} else {

				var minimumTrackingForScroll = self.options.locking ? 3 : 0;
				var minimumTrackingForDrag = 5;

				var distanceX = Math.abs(currentTouchLeft - self.__initialTouchLeft);
				var distanceY = Math.abs(currentTouchTop - self.__initialTouchTop);

				self.__enableScrollX = self.options.scrollingX && distanceX >= minimumTrackingForScroll;
				self.__enableScrollY = self.options.scrollingY && distanceY >= minimumTrackingForScroll;

				positions.push(self.__scrollLeft, self.__scrollTop, timeStamp);

				self.__isDragging = (self.__enableScrollX || self.__enableScrollY) && (distanceX >= minimumTrackingForDrag || distanceY >= minimumTrackingForDrag);
				if (self.__isDragging) {
					self.__interruptedAnimation = false;
				}

			}

			// Update last touch positions and time stamp for next event
			self.__lastTouchLeft = currentTouchLeft;
			self.__lastTouchTop = currentTouchTop;
			self.__lastTouchMove = timeStamp;
			self.__lastScale = scale;

		},


		/**
		 * Touch end handler for scrolling support
		 */
		doTouchEnd: function(timeStamp) {

			if (timeStamp instanceof Date) {
				timeStamp = timeStamp.valueOf();
			}
			if (typeof timeStamp !== "number") {
				throw new Error("Invalid timestamp value: " + timeStamp);
			}

			var self = this;

			// Ignore event when tracking is not enabled (no touchstart event on element)
			// This is required as this listener ('touchmove') sits on the document and not on the element itself.
			if (!self.__isTracking) {
				return;
			}

			// Not touching anymore (when two finger hit the screen there are two touch end events)
			self.__isTracking = false;

			// Be sure to reset the dragging flag now. Here we also detect whether
			// the finger has moved fast enough to switch into a deceleration animation.
			if (self.__isDragging) {

				// Reset dragging flag
				self.__isDragging = false;

				// Start deceleration
				// Verify that the last move detected was in some relevant time frame
				if (self.__isSingleTouch && self.options.animating && (timeStamp - self.__lastTouchMove) <= 100) {

					// Then figure out what the scroll position was about 100ms ago
					var positions = self.__positions;
					var endPos = positions.length - 1;
					var startPos = endPos;

					// Move pointer to position measured 100ms ago
					for (var i = endPos; i > 0 && positions[i] > (self.__lastTouchMove - 100); i -= 3) {
						startPos = i;
					}

					// If start and stop position is identical in a 100ms timeframe,
					// we cannot compute any useful deceleration.
					if (startPos !== endPos) {

						// Compute relative movement between these two points
						var timeOffset = positions[endPos] - positions[startPos];
						var movedLeft = self.__scrollLeft - positions[startPos - 2];
						var movedTop = self.__scrollTop - positions[startPos - 1];

						// Based on 50ms compute the movement to apply for each render step
						self.__decelerationVelocityX = movedLeft / timeOffset * (1000 / 60);
						self.__decelerationVelocityY = movedTop / timeOffset * (1000 / 60);

						// How much velocity is required to start the deceleration
						var minVelocityToStartDeceleration = self.options.paging || self.options.snapping ? 4 : 1;

						// Verify that we have enough velocity to start deceleration
						if (Math.abs(self.__decelerationVelocityX) > minVelocityToStartDeceleration || Math.abs(self.__decelerationVelocityY) > minVelocityToStartDeceleration) {

							// Deactivate pull-to-refresh when decelerating
							if (!self.__refreshActive) {
								self.__startDeceleration(timeStamp);
							}
						} else {
							self.options.scrollingComplete();
						}
					} else {
						self.options.scrollingComplete();
					}
				} else if ((timeStamp - self.__lastTouchMove) > 100) {
					self.options.scrollingComplete();
	 			}
			}

			// If this was a slower move it is per default non decelerated, but this
			// still means that we want snap back to the bounds which is done here.
			// This is placed outside the condition above to improve edge case stability
			// e.g. touchend fired without enabled dragging. This should normally do not
			// have modified the scroll positions or even showed the scrollbars though.
			if (!self.__isDecelerating) {

				if (self.__refreshActive && self.__refreshStart) {

					// Use publish instead of scrollTo to allow scrolling to out of boundary position
					// We don't need to normalize scrollLeft, zoomLevel, etc. here because we only y-scrolling when pull-to-refresh is enabled
					self.__publish(self.__scrollLeft, -self.__refreshHeight, self.__zoomLevel, true);

					if (self.__refreshStart) {
						self.__refreshStart();
					}

				} else {

					if (self.__interruptedAnimation || self.__isDragging) {
						self.options.scrollingComplete();
					}
					self.scrollTo(self.__scrollLeft, self.__scrollTop, true, self.__zoomLevel);

					// Directly signalize deactivation (nothing todo on refresh?)
					if (self.__refreshActive) {

						self.__refreshActive = false;
						if (self.__refreshDeactivate) {
							self.__refreshDeactivate();
						}

					}
				}
			}

			// Fully cleanup list
			self.__positions.length = 0;

		},



		/*
		---------------------------------------------------------------------------
			PRIVATE API
		---------------------------------------------------------------------------
		*/

		/**
		 * Applies the scroll position to the content element
		 *
		 * @param left {Number} Left scroll position
		 * @param top {Number} Top scroll position
		 * @param animate {Boolean?false} Whether animation should be used to move to the new coordinates
		 */
		__publish: function(left, top, zoom, animate) {

			var self = this;

			// Remember whether we had an animation, then we try to continue based on the current "drive" of the animation
			var wasAnimating = self.__isAnimating;
			if (wasAnimating) {
				core.effect.Animate.stop(wasAnimating);
				self.__isAnimating = false;
			}

			if (animate && self.options.animating) {

				// Keep scheduled positions for scrollBy/zoomBy functionality
				self.__scheduledLeft = left;
				self.__scheduledTop = top;
				self.__scheduledZoom = zoom;

				var oldLeft = self.__scrollLeft;
				var oldTop = self.__scrollTop;
				var oldZoom = self.__zoomLevel;

				var diffLeft = left - oldLeft;
				var diffTop = top - oldTop;
				var diffZoom = zoom - oldZoom;

				var step = function(percent, now, render) {

					if (render) {

						self.__scrollLeft = oldLeft + (diffLeft * percent);
						self.__scrollTop = oldTop + (diffTop * percent);
						self.__zoomLevel = oldZoom + (diffZoom * percent);

						// Push values out
						if (self.__callback) {
							self.__callback(self.__scrollLeft, self.__scrollTop, self.__zoomLevel);
						}

					}
				};

				var verify = function(id) {
					return self.__isAnimating === id;
				};

				var completed = function(renderedFramesPerSecond, animationId, wasFinished) {
					if (animationId === self.__isAnimating) {
						self.__isAnimating = false;
					}
					if (self.__didDecelerationComplete || wasFinished) {
						self.options.scrollingComplete();
					}

					if (self.options.zooming) {
						self.__computeScrollMax();
						if(self.__zoomComplete) {
							self.__zoomComplete();
							self.__zoomComplete = null;
						}
					}
				};

				// When continuing based on previous animation we choose an ease-out animation instead of ease-in-out
				self.__isAnimating = core.effect.Animate.start(step, verify, completed, self.options.animationDuration, wasAnimating ? easeOutCubic : easeInOutCubic);

			} else {

				self.__scheduledLeft = self.__scrollLeft = left;
				self.__scheduledTop = self.__scrollTop = top;
				self.__scheduledZoom = self.__zoomLevel = zoom;

				// Push values out
				if (self.__callback) {
					self.__callback(left, top, zoom);
				}

				// Fix max scroll ranges
				if (self.options.zooming) {
					self.__computeScrollMax();
					if(self.__zoomComplete) {
						self.__zoomComplete();
						self.__zoomComplete = null;
					}
				}
			}
		},


		/**
		 * Recomputes scroll minimum values based on client dimensions and content dimensions.
		 */
		__computeScrollMax: function(zoomLevel) {

			var self = this;

			if (zoomLevel == null) {
				zoomLevel = self.__zoomLevel;
			}

			self.__maxScrollLeft = Math.max((self.__contentWidth * zoomLevel) - self.__clientWidth, 0);
			self.__maxScrollTop = Math.max((self.__contentHeight * zoomLevel) - self.__clientHeight, 0);

		},



		/*
		---------------------------------------------------------------------------
			ANIMATION (DECELERATION) SUPPORT
		---------------------------------------------------------------------------
		*/

		/**
		 * Called when a touch sequence end and the speed of the finger was high enough
		 * to switch into deceleration mode.
		 */
		__startDeceleration: function(timeStamp) {

			var self = this;

			if (self.options.paging) {

				var scrollLeft = Math.max(Math.min(self.__scrollLeft, self.__maxScrollLeft), 0);
				var scrollTop = Math.max(Math.min(self.__scrollTop, self.__maxScrollTop), 0);
				var clientWidth = self.__clientWidth;
				var clientHeight = self.__clientHeight;

				// We limit deceleration not to the min/max values of the allowed range, but to the size of the visible client area.
				// Each page should have exactly the size of the client area.
				self.__minDecelerationScrollLeft = Math.floor(scrollLeft / clientWidth) * clientWidth;
				self.__minDecelerationScrollTop = Math.floor(scrollTop / clientHeight) * clientHeight;
				self.__maxDecelerationScrollLeft = Math.ceil(scrollLeft / clientWidth) * clientWidth;
				self.__maxDecelerationScrollTop = Math.ceil(scrollTop / clientHeight) * clientHeight;

			} else {

				self.__minDecelerationScrollLeft = 0;
				self.__minDecelerationScrollTop = 0;
				self.__maxDecelerationScrollLeft = self.__maxScrollLeft;
				self.__maxDecelerationScrollTop = self.__maxScrollTop;

			}

			// Wrap class method
			var step = function(percent, now, render) {
				self.__stepThroughDeceleration(render);
			};

			// How much velocity is required to keep the deceleration running
			var minVelocityToKeepDecelerating = self.options.snapping ? 4 : 0.1;

			// Detect whether it's still worth to continue animating steps
			// If we are already slow enough to not being user perceivable anymore, we stop the whole process here.
			var verify = function() {
				var shouldContinue = Math.abs(self.__decelerationVelocityX) >= minVelocityToKeepDecelerating || Math.abs(self.__decelerationVelocityY) >= minVelocityToKeepDecelerating;
				if (!shouldContinue) {
					self.__didDecelerationComplete = true;
				}
				return shouldContinue;
			};

			var completed = function(renderedFramesPerSecond, animationId, wasFinished) {
				self.__isDecelerating = false;
				// if (self.__didDecelerationComplete) {
				// 	self.options.scrollingComplete();
				// }
				self.options.scrollingComplete();

				// Animate to grid when snapping is active, otherwise just fix out-of-boundary positions
				self.scrollTo(self.__scrollLeft, self.__scrollTop, self.options.snapping);
			};

			// Start animation and switch on flag
			self.__isDecelerating = core.effect.Animate.start(step, verify, completed);

		},


		/**
		 * Called on every step of the animation
		 *
		 * @param inMemory {Boolean?false} Whether to not render the current step, but keep it in memory only. Used internally only!
		 */
		__stepThroughDeceleration: function(render) {

			var self = this;


			//
			// COMPUTE NEXT SCROLL POSITION
			//

			// Add deceleration to scroll position
			var scrollLeft = self.__scrollLeft + self.__decelerationVelocityX;
			var scrollTop = self.__scrollTop + self.__decelerationVelocityY;


			//
			// HARD LIMIT SCROLL POSITION FOR NON BOUNCING MODE
			//

			if (!self.options.bouncing) {

				var scrollLeftFixed = Math.max(Math.min(self.__maxDecelerationScrollLeft, scrollLeft), self.__minDecelerationScrollLeft);
				if (scrollLeftFixed !== scrollLeft) {
					scrollLeft = scrollLeftFixed;
					self.__decelerationVelocityX = 0;
				}

				var scrollTopFixed = Math.max(Math.min(self.__maxDecelerationScrollTop, scrollTop), self.__minDecelerationScrollTop);
				if (scrollTopFixed !== scrollTop) {
					scrollTop = scrollTopFixed;
					self.__decelerationVelocityY = 0;
				}

			}


			//
			// UPDATE SCROLL POSITION
			//

			if (render) {

				self.__publish(scrollLeft, scrollTop, self.__zoomLevel);

			} else {

				self.__scrollLeft = scrollLeft;
				self.__scrollTop = scrollTop;

			}


			//
			// SLOW DOWN
			//

			// Slow down velocity on every iteration
			if (!self.options.paging) {

				// This is the factor applied to every iteration of the animation
				// to slow down the process. This should emulate natural behavior where
				// objects slow down when the initiator of the movement is removed
				var frictionFactor = 0.95;

				self.__decelerationVelocityX *= frictionFactor;
				self.__decelerationVelocityY *= frictionFactor;

			}


			//
			// BOUNCING SUPPORT
			//

			if (self.options.bouncing) {

				var scrollOutsideX = 0;
				var scrollOutsideY = 0;

				// This configures the amount of change applied to deceleration/acceleration when reaching boundaries
				var penetrationDeceleration = self.options.penetrationDeceleration;
				var penetrationAcceleration = self.options.penetrationAcceleration;

				// Check limits
				if (scrollLeft < self.__minDecelerationScrollLeft) {
					scrollOutsideX = self.__minDecelerationScrollLeft - scrollLeft;
				} else if (scrollLeft > self.__maxDecelerationScrollLeft) {
					scrollOutsideX = self.__maxDecelerationScrollLeft - scrollLeft;
				}

				if (scrollTop < self.__minDecelerationScrollTop) {
					scrollOutsideY = self.__minDecelerationScrollTop - scrollTop;
				} else if (scrollTop > self.__maxDecelerationScrollTop) {
					scrollOutsideY = self.__maxDecelerationScrollTop - scrollTop;
				}

				// Slow down until slow enough, then flip back to snap position
				if (scrollOutsideX !== 0) {
					if (scrollOutsideX * self.__decelerationVelocityX <= 0) {
						self.__decelerationVelocityX += scrollOutsideX * penetrationDeceleration;
					} else {
						self.__decelerationVelocityX = scrollOutsideX * penetrationAcceleration;
					}
				}

				if (scrollOutsideY !== 0) {
					if (scrollOutsideY * self.__decelerationVelocityY <= 0) {
						self.__decelerationVelocityY += scrollOutsideY * penetrationDeceleration;
					} else {
						self.__decelerationVelocityY = scrollOutsideY * penetrationAcceleration;
					}
				}
			}
		}
	};

	// Copy over members to prototype
	for (var key in members) {
		Scroller.prototype[key] = members[key];
	}

})();

/**
 * @class Roc.physics.Scroll
 * @extends Roc.Event
 * @xtype physics.scroll
 *
 * Javascript scrolling engine.
 *
 * ### Sample usage:
 *
    afterrender: function(self) {

        self.events.add('scroller', {
            xtype: 'physics.scroll',
            el: self.content.el,
            cmp: self,
            scrollable: 'y'
        });

        self.events.get('scroller').resize();

    }
 *
 */
if (!Roc.physics) Roc.physics = {};
Roc.physics.Scroll = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _win = window,
        _doc = document,
        isTouch = ('ontouchstart' in _win),
        isAndroid = (_win.navigator.userAgent.indexOf('Android') >= 0),
        _parent = _ns.Event,
        $ = _ns.Selector;

    var docStyle = _doc.documentElement.style;

    var engine;

    if (_win.opera &&
        Object.prototype.toString.call(opera) === '[object Opera]') {
        engine = 'presto';
    } else if ('MozAppearance' in docStyle) {
        engine = 'gecko';
    } else if ('WebkitAppearance' in docStyle) {
        engine = 'webkit';
    } else if (typeof navigator.cpuClass === 'string') {
        engine = 'trident';
    }

    var vendorPrefix = {
        trident: 'ms',
        gecko: 'Moz',
        webkit: 'Webkit',
        presto: 'O'
    }[engine];

    var vendorStylePropertyPrefix = {
        trident: 'ms',
        gecko: 'Moz',
        webkit: 'webkit',
        presto: 'O'
    }[engine];

    var helperElem = _doc.createElement("div");
    var undef;

    var perspectiveProperty = vendorPrefix + "Perspective";
    var transformProperty = vendorPrefix + "Transform";

    function render(self) {

        if (helperElem.style[perspectiveProperty] !== undef) {

            return function(left, top, zoom) {
                if (self.callback && self.callback(left, top, zoom) === false) return false;
                self.content.style[transformProperty] = 'translate3d(' + (-left) + 'px,' + (-top) + 'px,0) scale(' + zoom + ')';
            };

        } else if (helperElem.style[transformProperty] !== undef) {

            return function(left, top, zoom) {
                if (self.callback && self.callback(left, top, zoom) === false) return false;
                self.content.style[transformProperty] = 'translate(' + (-left) + 'px,' + (-top) + 'px) scale(' + zoom + ')';
            };

        } else {

            return function(left, top, zoom) {
                if (self.callback && self.callback(left, top, zoom) === false) return false;
                self.content.style.marginLeft = left ? (-left/zoom) + 'px' : '';
                self.content.style.marginTop = top ? (-top/zoom) + 'px' : '';
                self.content.style.zoom = zoom || '';
            };

        }
    }

    function init_events(self) {

        if (!self.events) {

            var is_keyboard = false;
            var is_landscape = false;
            var initial_screen_size = window.innerHeight;
            var inputs = ['INPUT', 'TEXTAREA', 'SELECT'];
            var android = (window.navigator.userAgent.indexOf('Android') >= 0);

            self.events = {};

            if (isTouch) {

                var isFocusable = function(target) {
                    if (android && inputs.indexOf(target.tagName) !== -1) {
                        return true;
                    }
                    return false;
                };

                // fix bug for android when focus input we must scroll
                // to focused input
                if (android === true) {

                    var mustFocus = false;

                    self.events.click = function(evt) {

                        var target = evt.target;

                        if (['INPUT', 'TEXTAREA'].indexOf(target.tagName) !== -1) {
                            mustFocus = target;
                        } else {
                            mustFocus = false;
                        }

                    };

                    self.events.resize = function() {

                        self.resize();

                        if (mustFocus !== false) {

                            var sizes = mustFocus.getBoundingClientRect(),
                                pos = self.getPositions(),
                                offsetTop = (self.options.offsetTop ?
                                    self.options.offsetTop : 0),
                                total = (sizes.top - sizes.height - offsetTop + pos.y);

                            self.scrollTo(0, total, true);
                            mustFocus = false;
                        }

                    };

                } else {
                    self.events.focusin = function() {
                        self.disabled = true;
                    };
                    self.events.focusout = function() {
                        self.disabled = false;
                    };
                    self.events.resize = function() {
                        self.resize();
                    };
                }

                self.events.touchstart = function(e) {

                    if (self.disabled === true) {
                        return;
                    } else if (self.options.beforestart &&
                        self.options.beforestart(e, e.touches[0].target) === false) {
                        return;
                    }

                    // android inputs twice focus bug resolve
                    if (isFocusable(e.target)) {
                        self.__hasStarted = false;
                        return;
                    }

                    self.__hasStarted = true;

                    self.api.doTouchStart(e.touches, e.timeStamp);
                    e.preventDefault();

                };

                self.events.touchmove = function(e) {

                    if (!self.__hasStarted && isFocusable(e.target)) {
                        self.__hasStarted = true;
                        self.api.doTouchStart(e.touches, e.timeStamp);
                        e.preventDefault();
                        return;
                    } else if (!self.__hasStarted) return;

                    self.api.doTouchMove(e.touches, e.timeStamp, e.scale);
                };

                self.events.touchend = function(e) {
                    if (!self.__hasStarted && isFocusable(e.target)) {
                        return;
                    }
                    self.__hasStarted = false;
                    self.api.doTouchEnd(e.timeStamp);
                };

            } else {

                var mousedown = false;

                self.events.mousedown = function(e) {

                    if (e.target.tagName.match(/input|textarea|select/i)) {
                        return;
                    }

                    self.api.doTouchStart([{
                        pageX: e.pageX,
                        pageY: e.pageY
                    }], e.timeStamp);

                    mousedown = true;
                    e.preventDefault();

                };

                self.events.mousemove = function(e) {

                    if (!mousedown) {
                        return;
                    }

                    self.api.doTouchMove([{
                        pageX: e.pageX,
                        pageY: e.pageY
                    }], e.timeStamp);

                    mousedown = true;

                };

                self.events.mouseup = function(e) {

                    if (!mousedown) {
                        return;
                    }

                    self.api.doTouchEnd(e.timeStamp);

                    mousedown = false;

                };

                self.events.mousewheel = function(e) {
                    if (self.options.zooming) {
                        self.api.doMouseZoom(e.wheelDelta, e.timeStamp, e.pageX, e.pageY);
                        e.preventDefault();
                    }
                };
            }

        }
    }

    function auto_resize(self) {

        self.__autoResize = function() {
            self.resize();
        };

        self.__autoResizeAttached = false;

        // Try and reuse the old, disconnected observer instance if available
        // Otherwise, check for support before proceeding
        if (!self.mutationObserver) {
            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window[vendorStylePropertyPrefix + 'MutationObserver'];
            if (MutationObserver) {
                self.mutationObserver = new MutationObserver(self.__autoResize);
            } else {
                self.__autoResizeDOM = function (e) {
                    // Ignore changes to nested FT Scrollers - even updating a transform style
                    // can trigger a DOMSubtreeModified in IE, causing nested scrollers to always
                    // favour the deepest scroller as parent scrollers 'resize'/end scrolling.
                    if (e && (e.srcElement === self.container)) {
                        return;
                    }

                    self.resize();
                };
            }
        }

    }

    function auto_resize_attach(self) {

        if (self.__autoResizeAttached !== false) {
            return false;
        }

        self.__autoResizeAttached = true;

        if (self.mutationObserver) {
            self.mutationObserver.observe(self.container, {
                childList: true,
                characterData: true,
                subtree: true
            });
        } else {
            self.container.addEventListener('DOMSubtreeModified', self.__autoResizeDOM, true);
        }
        self.container.addEventListener('load', self.__autoResize, true);
    }

    function auto_resize_detach(self) {

        if (self.__autoResizeAttached !== true) {
            return false;
        }

        self.__autoResizeAttached = false;

        if (self.mutationObserver) {
            self.mutationObserver.disconnect();
        } else {
            self.container.removeEventListener('DOMSubtreeModified', self.__autoResizeDOM, true);
        }
        self.container.removeEventListener('load', self.__autoResize, true);
    }

    // public
    return _parent.subclass({

        className: 'Roc.physics.Scroll',
        xtype: 'physics.scroll',

        /**
         * @cfg {Object} [api={}] Passed to low level API for advanced custom configuration.
         */

        /**
         * @cfg {Mixed} [parentClass='scroll-content'] Container CSS class string or `false` to disable it.
         */

        /**
         * @cfg {Function} [callback=undefined] Scroll callback. Called each time scrolling is happening.
         */

        /**
         * @cfg {Mixed} scrollable True to activate x and y scrolling else 'x' or 'y' strings only. Defaults to undefined.
         */

        /**
         * @cfg {Boolean} [pullToRefresh=false] True to activate pull to refresh feature.
         */

        /**
         * @cfg {HTMLElement} el Given HTML element to activate scroll on.
         */

        /**
         * @cfg {HTMLElement} [container=el.parentNode] Given HTML element container.
         */

        /**
         * @cfg {Object} cmp Component (view) instance.
         */

        /**
         * @cfg {Boolean} [zoomable=false] Defines if scroll area is zoomable or not.
         */

        /**
         * @cfg {Number} [minZoom=undefined] Defines minimum zoom float. (0 to 100+). `zoomable` must be `true`.
         */

        /**
         * @cfg {Number} [maxZoom=undefined] Defines maximum zoom float. (0 to 100+). `zoomable` must be `true`.
         */

        /**
         * @cfg {Function} [beforestart=undefined] Before start callback.
         * You can return false to avoid scrolling continue.
         *
         *      function(event, target) { event.preventDefault(); return false; }
         */

        /**
         * @cfg {Function} [afterscroll=undefined] After scroll callback.
         */

        /**
         * @cfg {Function} [onrefresh=undefined] On refresh callback. `pullToRefresh` must be `true`.
         */

        /**
         * @cfg {Number} [offsetHeight=0] Add an offset height for container height.
         */

        /**
         * @cfg {Number} [refreshHeight=50] Set refresh height area. `pullToRefresh` must be `true`.
         */

        constructor: function(options) {

            var element,
                cmp,
                self = this;

            /**
             * @property {Object} options Scroller options given from constructor.
             */
            self.options = options || {};

            /**
             * @property {Boolean} attached Is scroller attached or not.
             */
            self.attached = false;

            /**
             * @property {HTMLElement} content Scroller content.
             */
            self.content = element = options.el;

            /**
             * @property {HTMLElement} container Scroller container. Defaults to content parent node.
             */
            self.container = (options.container ? options.container : element.parentNode);

            /**
             * @property {Object} cmp Roc component (view) instance.
             */
            self.cmp = cmp = options.cmp;
            // self.selector @TODO si pas de el alors on afterrender et on
            // self.cmp.querySelector(self.selector)

            if (typeof options.parentClass === 'string') {
                $.addClass(self.container, options.parentClass);
            } else if (options.parentClass !== false) {
                $.addClass(self.container, 'scroll-content');
            }
            $.addClass(element, 'scroll');

            if (options.callback) {
                self.callback = options.callback;
            }

            var config = (options.api ? options.api : {});

            config.scrollingX = (options.scrollable === true ||
                options.scrollable === 'x');
            config.scrollingY = (options.scrollable === true ||
                options.scrollable === 'y');

            if (options.zoomable === true) {
                config.zooming = true;
                config.minZoom = parseFloat(options.minZoom);
                config.maxZoom = parseFloat(options.maxZoom);
            }

            if (options.paging === true) {
                config.paging = true;
            }

            if (options.snapping === true) {
                config.snapping = true;
            }

            if (typeof options.beforestart === 'function') {
                config.beforestart = options.beforestart;
            }

            if (typeof options.afterscroll === 'function') {
                config.scrollingComplete = options.afterscroll;
            }

            // self.api = new EasyScroller(element, config);
            // self.engine = self.api.scroller;
            /**
             * @property {Scroller} api Scroller API instance. You should avoid to use it.
             * @private
             */
            self.api = new Scroller(render(self), config);

            if (options.pullToRefresh === true) {
                self.refreshHeight = options.refreshHeight || 50;
                self.content.insertAdjacentHTML('afterbegin',
                    '<div class="refresh"><i class="ion ion-ios7-arrow-thin-down"></i><span></span></div>');//<span>' + t('Pull to Refresh') + '</span></div>');
                self.refreshEl = self.content.querySelector('.refresh');
                self.refreshIconEl = self.refreshEl.querySelector('.ion');
                //self.refreshTextEl = self.refreshEl.querySelector('span');
                self.api.activatePullToRefresh(self.refreshHeight, function() {
                    $.addClass(self.refreshEl, 'active');
                }, function() {
                    $.removeClass(self.refreshEl, 'active');
                }, function() {
                    $.removeClass(self.refreshIconEl, 'ion-ios7-arrow-thin-down');
                    $.addClass(self.refreshIconEl, 'ion-loading-d');
                    $.addClass(self.refreshEl, 'running');
                    if (typeof options.onrefresh === 'function') {
                        options.onrefresh(function() {
                            $.removeClass(self.refreshEl, 'running');
                            $.removeClass(self.refreshIconEl, 'ion-loading-d');
                            $.addClass(self.refreshIconEl, 'ion-ios7-arrow-thin-down');
                            var rect = self.container.getBoundingClientRect();
                            self.api.setPosition(rect.left + self.container.clientLeft,
                                rect.top + self.container.clientTop - self.refreshHeight);
                            self.api.finishPullToRefresh();
                        });
                    }
                });
            }

            _parent.prototype.constructor.call(self, options);

            self.setupListeners(['apiready']);

            self.fire('apiready', self, self.api);

            self.seID = cmp.renderer.on('show', self.attach, self);
            self.heID = cmp.renderer.on('hide', self.detach, self);

            init_events(self);

            self.content.style[vendorPrefix + 'TransformOrigin'] = 'left top';

            if (options.autoResize === true) {
                auto_resize(self);
            }

            self.attach();

        },

        /**
         * Get width, height, top and left from scrolling area.
         * @return {Object} `{ width: 100, height: 100, top: 10, left: 10 }`
         */
        getSizes: function() {

            var api = this.api;

            return ({
                width: api.__maxScrollLeft,
                height: api.__maxScrollTop,
                top: api.__scrollTop,
                left: api.__scrollLeft
            });

        },

        /**
         * Get current scrolling positions.
         * @return {Object} `{ x: 10, y: 0 }`
         */
        getPositions: function() {

            var api = this.api;

            return ({
                y: api.__scrollTop,
                x: api.__scrollLeft
            });
        },

        /**
         * Get scrolling dimensions.
         * @return {Object} `{ x: 100, y: 400 }`
         */
        getDimensions: function() {

            var api = this.api;

            return {
                x: api.__maxScrollTop,
                y: api.__maxScrollLeft
            };

        },

        /**
         * Scroll to given positions with given options.
         * @param  {Number} left    Left position.
         * @param  {Number} top     Top position.
         * @param  {Boolean} [animate] True to animate scrolling. Defaults to false.
         * @param  {Number} [zoom]    Float zoom. Defaults to 1.
         */
        scrollTo: function(left, top, animate, zoom) {
            this.api.scrollTo(left, top, animate, zoom);
        },

        /**
         * Attach scroller listeners.
         * @param  {Object} [self] Current instance.
         */
        attach: function(self) {

            self = self || this;

            var container = self.container,
                events = self.events;

            if (self.attached !== false) {
                return false;
            }

            //console.log('scroll ++ ATtach from ', self.cmp.el.id, new Date());

            self.attached = true;

            if (self.options.autoResize === true) {
                auto_resize_attach(self);
            }

            if (isTouch) {

                if (isAndroid) {
                    container.addEventListener('click', events.click, false);
                } else {
                    _win.addEventListener('focusin', events.focusin, false);
                    _win.addEventListener('focusout', events.focusout, false);
                }

                _win.addEventListener('resize', events.resize, false);

                container.addEventListener('touchstart', events.touchstart, false);

                // all of those were document. based
                container.addEventListener('touchmove', events.touchmove, false);
                container.addEventListener('touchend', events.touchend, false);
                container.addEventListener('touchcancel', events.touchend, false);
            } else {
                container.addEventListener('mousedown', events.mousedown, false);
                container.addEventListener('mousewheel', events.mousewheel, false);

                // all of those were document. based
                container.addEventListener('mousemove', events.mousemove, false);
                container.addEventListener('mouseup', events.mouseup, false);
            }
        },

        /**
         * Detach scroller listeners.
         * @param  {Object} [self] Current instance.
         */
        detach: function(self) {

            self = self || this;

            var container = self.container,
                events = self.events;

            if (self.attached !== true) {
                return false;
            }

            //console.log('scroll -- DEtach from ', self.cmp.el.id, new Date());

            self.attached = false;

            if (self.options.autoResize === true) {
                auto_resize_detach(self);
            }

            // reflow handling

            // touch devices bind touch events
            if (isTouch) {

                _win.removeEventListener("resize", events.resize, false);

                if (isAndroid) {
                    container.removeEventListener('click', events.click, false);
                } else {
                    _win.removeEventListener('focusin', events.focusin, false);
                    _win.removeEventListener('focusout', events.focusout, false);
                }

                container.removeEventListener("touchstart", events.touchstart, false);

                // all of those were document. based
                container.removeEventListener("touchmove", events.touchmove, false);
                container.removeEventListener("touchend", events.touchend, false);
                container.removeEventListener("touchcancel", events.touchend, false);

            // non-touch bind mouse events
            } else {

                container.removeEventListener("mousedown", events.mousedown, false);
                container.removeEventListener("mousewheel", events.mousewheel, false);

                // all of those were document. based
                container.removeEventListener("mousemove", events.mousemove, false);
                container.removeEventListener("mouseup", events.mouseup, false);

            }
        },

        /**
         * Force scroller to resize.
         */
        resize: function() {

            var self = this,
                // refresh the position for zooming purposes
                rect = self.container.getBoundingClientRect();

            self.api.setDimensions(self.container.clientWidth,
                self.container.clientHeight,
                self.content.offsetWidth,
                (self.content.offsetHeight + 95 + // TODO bugfix with ionic
                    (self.options.offsetHeight ? self.options.offsetHeight : 0)
                )
            );

            self.api.setPosition(rect.left + self.container.clientLeft, rect.top + self.container.clientTop);
        }

    });
})();

/**
 * @class Roc.implements.events
 * @extends {Object}
 *
 * Usage:
 *
 * 		var example = {
 * 			xtpl: 'container',
 * 			implements: ['events'],
 * 			hasListeners: ['afterrender'],
 * 			listeners: {
 * 				afterrender: function(self) {
 * 					self.events.add('scroll', {
 * 						xtype: 'physics.scroll',
 * 						cmp: this,
 * 						el: this.el,
 * 						scrollable: 'y'
 * 					});
 * 					self.events.get('scroll').resize();
 * 				}
 * 			}
 * 		};
 *
 */

/**
 * Attach all event listeners.
 * @method listen
 */

/**
 * Detach all event listeners.
 * @method unlisten
 */

/**
 * Add new event to component.
 * Example:
 *
 *      this.events.add('scroll', {
 *          xtype: 'physics.scroll',
 *          cmp: this,
 *          el: this.el,
 *          scrollable: 'y'
 *      });
 *
 * @method add
 * @param {String} name Event name. Must be unique else it will not override.
 * @param {Object} opts Event constructor options.
 * @param {String} opts.xtype Event xtype.
 */

/**
 * Remove event from component.
 * Example:
 *
 *     this.events.remove('scroll');
 *
 * @method remove
 * @param  {String} name Event name to remove.
 * @return {Boolean} True if success else false.
 */

/**
 * Get event corresponding to given name.
 * @method get
 * @param  {String} name Event name.
 * @return {Object} Event instance.
 */

/**
 * Attach all events.
 * @method attach
 */

/**
 * Detach all events.
 * @method detach
 */

/**
 * @class Roc.implements.update
 * @extends {Object}
 *
 * Usage:
 *
 * 		var example = {
 * 			xtpl: 'container',
 * 			implements: ['update'],
 * 			hasListeners: ['afterrender'],
 * 			listeners: {
 * 				afterrender: function(self) {
 * 					self.update({}, 'New container items content.');
 * 				}
 * 			}
 * 		};
 *
 */

/**
 * Update component with given configuration and items.
 * Example:
 *
 *      this.update({
 *      	data: data
 *      }, [{
 *      	xtype: 'button',
 *      	config: {
 *      		text: 'OK'
 *      	}
 *      }, '-', {
 *      	xtype: 'button',
 *      	config: {
 *      		text: 'Cancel'
 *      	}
 *      }]);
 *
 * @method update
 * @param {Object} config New component configuration.
 * @param {Mixed} [items] New component items.
 */

/**
 * @class Roc.views.Template
 * @extends Roc.Event
 * @xtype template
 *
 * Base view template class.
 *
 *      var tpl = new Roc.views.Template({
 *          xtpl: 'my-compiled-handlebars-template',
 *          config: {
 *              title: 'Hi there !'
 *          }
 *      });
 *
 *      tpl.compile();
 *      tpl.render('body');
 */
Roc.views.Template = (function() {

    'use strict';

    var _ns = Roc,
        _gconf = _ns.config,
        _parent = _ns.Event,
        _selector = _ns.Selector,
        _handlebars = Handlebars;
        //_observer = _win.MutationObserver || _win.WebKitMutationObserver

    function _registerEngineEvents(self) {

        if (!self.renderer ||
            self.engineEventsRegistered) {
            return false;
        }
        self.engineEventsRegistered = true;

        var parent = self;

        while (parent) {
            if (parent.xtype) {
                _registerEngineListeners(self, parent.xtype);
                parent = _ns.xtype(parent.xtype).parent;
            } else {
                parent = false;
            }
        }
        return true;
    }

    function _registerEngineListeners(self, xtype) {

        var i, e, events = self.engine.getEvents(xtype);

        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    self.renderer.on(i, events[i], self, self.priority.VIEWS);
                } else {
                    self.on(i, events[i], self, self.priority.VIEWS);
                }
            }
        }
    }

    var events = function(parent) {

        return {

            listen: function() {

                var self = this;

                if (!self._eshowid) {
                    self._eshowid = parent.renderer.on('show', self.attach, self);
                    self._ehideid = parent.renderer.on('hide', self.detach, self);
                    self._eremoveid = parent.renderer.on('remove', self.remove, self);
                }
            },

            unlisten: function() {

                var self = this;

                if (self._eshowid) {
                    self.renderer.off(self._eshowid);
                    self.renderer.off(self._ehideid);
                    self.renderer.off(self._eremoveid);

                    delete self._eshowid;
                    delete self._ehideid;
                    delete self._eremoveid;
                }
            },

            add: function(name, opts) {

                var self = this;

                if (!self._events) {
                    self._events = {};
                    self._eventKeys = [];
                    self._elen = 0;
                }
                if (!self._events[name]) {
                    self._events[name] = new (_ns.xtype(opts.xtype))(opts);
                    self._eventKeys.push(name);
                    ++self._elen;
                }
                if (self._elen === 1) {
                    self.listen();
                }
                return self._events[name];
            },

            remove: function(name) {

                var self = this;

                if (self._events && self._events[name]) {
                    self._events[name].detach();
                    delete self._events[name];
                    --self._elen;

                    if (!self._elen) {
                        self.unlisten();
                    }

                    return true;
                }
                return false;
            },

            get: function(name) {
                return this._events[name];
            },

            attach: function() {

                var key,
                    self = this,
                    len = self._elen;

                while (len--) {
                    key = self._eventKeys[len];
                    self._events[key].attach();
                }

                return self;

            },

            detach: function() {

                var key,
                    self = this,
                    len = self._elen;

                while (len--) {
                    key = self._eventKeys[len];
                    self._events[key].detach();
                }

                return self;

            }

        };

    };

    /*
    function _registerEngineOverrides() {
        if (this.engineOverridesRegistered) {
            return false;
        }
        this.engineOverridesRegistered = true;

        var i, o, overrides = this.engine.getOverrides(this.xtype);

        for (i in overrides) {
            o = overrides[i];

            if (typeof o === 'function') {
                this[i] = o;
            }
        }
        return true;
    };
    */

    function tpl_update(self) {

        return function(config, items) {

            if (!self.el) {
                return false;
            }

            self.parentEl = self.el.parentNode;
            self.nextEl = self.parentEl.querySelector('#' + self.el.id + ' + *');
            self.remove();

            if (!config.id) {
                config.id = self.config.id;
            }

            self.data = config;
            self.data.items = [];
            if (items) {
                self.items = items;
            } else {
                self.items = [];
            }

            self.compile();

            if (self.nextEl) {
                self.render(self.nextEl, 'beforebegin');
            } else {
                self.render(self.parentEl, 'beforeend');
            }
            self.fireScope('update', self);
        };

    }

    return _parent.subclass({

        className: 'Roc.views.Template',
        xtype: 'template',

        /**
         * @cfg {String} [xtpl=undefined] Abstract template name.
         */

        /**
         * @cfg {Boolean} [hidden=false] Hidden component.
         */

        /**
         * New template.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {
                template: '',
                config: {},
                items: []
            };

            if (opts.implements) {
                self.implements = self.implements || [];
                self.implements = self.implements.concat(opts.implements);
            }

            /**
             * @cfg {Array} [implements=[]] Additional features implementations.
             * You can implement:
             * <ul>
             *  <li>{@link Roc.implements.events events}: Allows to use event API to manage component events.</li>
             *  <li>{@link Roc.implements.update update}: Add update method to component to update template data and items without creating new component.</li>
             * </ul>
             */
            if (self.implements) {
                if (self.implements.indexOf('events') !== -1) {
                    self.events = events(self);
                }
                if (self.implements.indexOf('update') !== -1) {
                    self.update = tpl_update(self);
                }
            }

            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtpl = opts.xtpl;
            }

            /**
             * @cfg {Object} [config={}] Template configuration.
             * This variable will be the scope while executing template.
             */
            self.config = opts.config || {};
            if (!self.config.id) {
                self.config.id = _selector.generateId();
            }

            if (self.config.hidden === true) {
                self.hidden = true;
            } else {
                self.hidden = false;
            }

            // <testing>
            //delete opts.config;
            //self.config = opts;
            // </testing>

            /**
             * @cfg {Object} [defaults={}] Defaults values for first child level items.
             */
            if (opts.defaults) {
                self.defaults = opts.defaults;
            }

            /**
             * @cfg {Mixed} items (optional) Template children items.
             *
             * ## Different possibilities:
             *
             *     var mypage = new Roc.views.Page({
             *         items: [{
             *             xtype: 'header',
             *             config: {
             *                 title: 'My page'
             *             }
             *         }, {
             *             xtype: 'content',
             *             items: '<h1>My content</h1>'
             *         }, {
             *             xtype: 'footer',
             *             items: [new Roc.views.TabButton({
             *                 ...
             *             }), {
             *                 xtype: 'tabbutton',
             *                 ...
             *             }, 'Raw HTML', ...]
             *         }]
             *     })
             *
             */
            self.items = opts.items || [];
            if (!self.items.indexOf) {
                self.items = [self.items];
            }
            self.data = self.config;
            self.data.items = [];
            //self.data.items = self.items;
            if (!opts.template) {
                opts.template = self.setEngine(opts, _gconf).getTpl(self.xtpl || self.xtype);
            } else {
                self.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }

            /**
             * @cfg {String} ref Reference name to given `refScope`.
             */
            if (opts.ref) {
                self.ref = opts.ref;
            }

            /**
             * @cfg {Object} refScope Reference scope.
             */
            if (opts.refScope) {
                self.refScope = opts.refScope;
            }

            /**
             * @property {Function} tpl Template function.
             * @readonly
             */
            self.tpl = opts.template;

            /**
             * @property {String} html Compiled HTML.
             * @readonly
             */
            self.html = '';

            self.renderToEl = '';

            /**
             * @property {HTMLElement} el HTML element corresponding to this component.
             * @readonly
             */
            self.el = '';
            self.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(self, arguments);
        },

        getEngine: function(config, gconfig) {

            var self = this;

            if (config && config.engine) {
                return config.engine;
            } else if (self.engine) {
                return self.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }

            return self.engine;
        },

        setEngine: function(config, gconfig) {

            var self = this;

            self.engine = self.getEngine(config, gconfig);

            if (typeof self.engine === 'string') {
                self.engine = new (_ns.xtype(self.engine))();
            } else if (self.engine) {
                self.engine = new self.engine();
            } else {
                console.warn('No engine specified. Use default engine.', self.engine);
                self.engine = {
                    getEvents: function() {},
                    getTpl: function(tplname) {
                        var tpl = _handlebars.templates[tplname];
                        if (!tpl) console.warn('Template "' + tplname + '" does not exists.');
                        return tpl;
                    }
                };
            }

            return self.engine;
        },

        /**
         * Compile template and generate HTML.
         * @fires aftercompile
         */
        compile: function(parentConfig, renderer) {

            var item, tpl,
                self = this,
                i = -1,
                items = self.items,//.slice(0),
                length = self.items.length;

            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _ns.utils.applyIf(self.config, parentConfig, ['ui']);
            }

            while (++i < length) {

                item = items[i];

                if (typeof item === 'object') {
                    if (self.defaults) {
                        _ns.utils.applyIfAuto(item, self.defaults);
                    }
                    if (!item.engine) {
                        item.engine = self.engine.xtype;
                    }
                }

                if (typeof item === 'object' &&
                    (typeof item.xtype === 'string' ||
                    typeof item.xtpl === 'string') &&
                    !(item instanceof _parent)) {
                    if (!item.xtype) {
                        item.xtype = 'template';
                    }
                    //typeof item.__proto__.xtype === 'undefined') {
                    //typeof Object.getPrototypeOf(item).xtype === 'undefined') {
                    item = new (_ns.xtype(item.xtype))(item);
                    item.parent = self;
                }
                if (typeof item === 'string') {
                    tpl = _ns.Templates.compile(item);
                    self.data.items[i] = tpl(self.config);
                } else if (typeof item.compile === 'function') {
                    item.parent = self;
                    self.data.items[i] = item.compile(self.config, renderer || self);
                } else {
                    self.data.items[i] = item;
                }
            }

            _handlebars.currentParent.push(self);

            self.html = self.tpl(self.data);

            _handlebars.currentParent.pop();

            if (!self.renderer) {
                self.renderer = renderer || self;
                if (self.ref && !self.refScope) {
                    self.refScope = self.renderer;
                }
            }
            // @todo Check if this kind of thing is not too much perf killer
            // and double check memory leak.
            // <unstable>
            if (self.ref &&
                !self.refScope[self.ref]) {
                self.refScope[self.ref] = self;
            }
            // </unstable>

            self.__arID = self.renderer.on('afterrender', function() {
                self.el = document.getElementById(self.config.id);
                //self.parentEl = self.el.parentNode;
                self.renderer.off(self.__arID);
                delete self.__arID;
            }, self, self.priority.CORE);

            self.setupListeners([

                /**
                 * @event aftercompile
                 * Fire after component has been compiled.
                 * @param { Roc.views.Template } self The component instance
                 * @param {Mixed} renderer The renderer component instance.
                 */
                'aftercompile',

                /**
                 * @event afterrender
                 * Fire after component has been rendered.
                 * @param {Mixed} scope Given scope
                 * @param { Roc.views.Template } self The component instance
                 * @param {Mixed} renderer The renderer component instance.
                 * This is the instance of the component from which the `render` method has been called.
                 */
                'afterrender'
            ], self.renderer);

            self.fire('aftercompile', self, self.renderer);
            _registerEngineEvents(self);

            return self.html;
        },

        /**
         * Render template to given element and position.
         * position: <!-- beforebegin --><p><!-- afterbegin -->foo<!-- beforeend --></p><!-- afterend -->
         * @fires afterrender
         * @param {Mixed} renderToEl Selector string or HTML element.
         * @param {String} [position='beforeend'] Position to render element at.
         * @chainable
         */
        render: function(renderToEl, position) {

            var self = this;

            position = position || 'beforeend';

            if (typeof renderToEl === 'string') {
                //renderToEl = _ns.Selector.get(renderToEl);
                self.renderToEl = _ns.Selector.get(renderToEl);
            } else {
                self.renderToEl = renderToEl;
            }

            self.renderToEl.insertAdjacentHTML(position, self.html);
            self.fireScope('afterrender', self);

            if (!self.el) {
                self.el = document.getElementById(self.config.id);
            }

            return self;
        },

        /**
         * Remove element from DOM if rendered.
         * @fires remove
         * @chainable
         */
        remove: function() {

            var self = this;

            if (self.el) {
                self.fireScope('remove', self);
                self.el.parentNode.removeChild(self.el);
                delete self.el;
                // @ignore !TODO remove all events automatically self.fire('remove', self);
                // should store all events (like click, resize etc.) inside instance array var
            } else {
                console.warn('Element cannot been removed because it has not been rendered !', this);
            }

            return self;
        },

        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        },

        /**
         * Show element.
         * @param  {Mixed} args Arguments for listeners.
         * @fires show
         * @chainable
         */
        show: function(args) {

            var self = this;

            self.hidden = false;
            /**
             * @event show
             * Fire after component has been showed.
             * @param {Mixed} scope Given scope from listener
             * @param { Roc.views.Template } self Component instance.
             * @param {Mixed} args Given arguments to show (from router for example)
             */
            self.fireScope('show', self, args);
            return self;
        },

        /**
         * Hide element.
         * @param  {Mixed} args Arguments for listeners.
         * @fires hide
         * @chainable
         */
        hide: function() {

            var self = this;

            self.hidden = true;
            /**
             * @event hide
             * Fire after component has been hidden.
             * @param {Mixed} scope Given scope from listener
             * @param { Roc.views.Template } self Component instance.
             */
            self.fireScope('hide', self);
            return self;
        }
    });

}());

/**
 * @class  Roc.RouterView
 * @extends Roc.Router
 * @xtype routerview
 *
 * Router helper to easily manage view by xtype.
 *
 * ### Example
 *
 *      var history = Roc.History,
 *          session = App.Session,
 *          parent = Roc.RouterView;
 *
 *      var routerview = new (parent.subclass({
 *
 *          xtype: 'user.router',
 *
 *          before: {
 *              'routeProfile': 'isAuthenticated'
 *          },
 *
 *          routes: {
 *              {{ route "/user/login" as="user.login" method="routeLogin" }},
 *              {{ route "/user/profile" as="user.profile" method="routeProfile" }}
 *          },
 *
 *          isAuthenticated: function(done) {
 *
 *              if (session.logged === false) {
 *                  history.go({{ url "user.login" }});
 *                  return false;
 *              }
 *
 *              // user is logged already so we continue
 *              return done();
 *          },
 *
 *          routeLogin: function() {
 *              this.setCurrentView('user.login');
 *          },
 *
 *          routeProfile: function() {
 *              this.setCurrentView('user.profile');
 *          }
 *      }));
 *
 */
Roc.RouterView = (function() {

    var _ns = Roc,
        _history = _ns.History,
        _parent = _ns.Router,
        _events = new _ns.Event(),
        _animating = false,
        _queue = [],
        _vqueue = [];

    _events.on('queuePop', function() {

        if (!_queue.length) {
            return false;
        }

        var item = _queue.pop();

        item.router.setCurrentView(item.xtype, item.config, item.args);
    });

    function has_fx(self, xtype) {
        if (self.fx && self.fx[xtype]) {
            return true;
        }
        return false;
    }

    function get_fx(self, xtype) {
        return (self.fx[xtype]);
    }

    function fx(self, xtype, done) {

        var config = get_fx(self, xtype),
            show = config.show,
            effect = new (_ns.xtype(config.xtype))(config),
            previousView = self.getPreviousView();

        if (previousView && config[previousView.xtype]) {
            show = config[previousView.xtype];
        }

        var opts = {
            speed: show.speed,
            effect: show.me,
            el: self.currentView.el,
            callback: function() {
                self.currentView.fireScope('afterfx', self.currentView);
                done();
            }
        };

        if (previousView) {
            if (show.other) {
                opts.fromEl = previousView.el;
                opts.fromEffect = show.other;
            } else {
                previousView.hide();
            }
        }

        effect.run(opts);

    }

    return _parent.subclass({

        xtype: 'routerview',

        /**
         * Create a new router view.
         *
         * @constructor
         */
        constructor: function() {

            var self = this;

            self.views = {};
            _parent.prototype.constructor.apply(self, arguments);

        },

        /**
         * Remove view from given xtype.
         * It will simply hide the view and keep the instance.
         *
         * @param  {String} xtype View xtype
         */
        removeView: function(xtype) {

            var self = this;

            if (self.views[xtype]) {
                // self.views[xtype].remove();
                // delete self.views[xtype];
                self.views[xtype].hide();
            }

        },

        /**
         * Remove current view.
         * @fires removeCurrentView if global is not false.
         * @param  {Boolean} [global=true] Global flag to let other router view catch this event.
         */
        removeCurrentView: function(global) {

            var self = this;

            if (self.currentView) {
                self.removeView(self.currentView.xtype);
                if (self.rvid) {
                    _events.off(self.rvid);
                    self.rvid = false;
                }
            }
            if (global !== false) {
                _events.fire('removeCurrentView', false);
            }

        },

        /**
         * Add previous view to queue.
         * @param {Object} view View instance.
         */
        addPreviousView: function(view) {
            _vqueue.push(view);
        },

        /**
         * Get previous view.
         * @return {Mixed} View instance if found else false.
         */
        getPreviousView: function() {
            if (_vqueue.length > 0) {
                return _vqueue[_vqueue.length - 1];
            }
            return false;
        },

        /**
         * Remove previous view. It will hide it.
         * If current view is same as previous view, it will do nothing.
         * @param  {Object} currentView Current view.
         */
        removePreviousView: function(currentView) {

            var self = this;

            if (_vqueue.length) {
                var view = _vqueue.pop();
                console.log('<<< Remove previous view: ' + view.xtype);
                if (view.config.id !== currentView.config.id) {
                    view.hide();
                }
            }

        },

        /**
         * Add given parameter to queue.
         * It is called by setCurrentView when animating is already running.
         * @param  {String} xtype  View xtype
         * @param  {Object} [config] View configuration
         * @param  {Mixed} [args]   View arguments
         */
        queue: function(xtype, config, args) {
            _queue = [];
            _queue.push({
                router: this,
                xtype: xtype,
                config: config,
                args: args
            });
        },

        /**
         * Set current view.
         * @param {String} xtype  View xtype
         * @param {Object} config Configuration for view constructor.
         * @param {Mixed} args   Arguments passed to view show event listeners.
         */
        setCurrentView: function(xtype, config, args) {

            var self = this;

            if (_animating === true) {
                return self.queue(xtype, config, args);
            }

            self.currentView = self.getView(xtype);

            var previous = self.getPreviousView();

            if (previous !== false &&
                previous.config.id === self.currentView.config.id) {
                console.log('=== Current view is same as previous view: ' + xtype);
                return self.currentView;
            }

            console.log('>>> Set current view to: ' + xtype);

            // fx stuff
            if (has_fx(self, xtype)) {
                _animating = true;
                self.currentView.show(args);
                fx(self, xtype, function() {
                    self.removePreviousView(self.currentView);
                    self.addPreviousView(self.currentView);
                    _animating = false;
                    _events.fire('queuePop', self);
                });
            } else {
                _animating = false;
                self.currentView.show(args);
                self.removePreviousView(self.currentView);
                self.addPreviousView(self.currentView);
            }

            return self.currentView;
        },

        /**
         * Get view from given xtype.
         * If the view does not exists, it will create new instance.
         * @param  {String} xtype View xtype
         * @return {Object}       View instance.
         */
        getView: function(xtype) {

            var self = this;

            if (!self.views[xtype]) {

                var view = self.views[xtype] = new (_ns.xtype(xtype))();

                view.compile();
                view.render('body', 'beforeend');
            }
            return self.views[xtype];
        }

    });

}());
/**
 * @class  Roc.RouterViewSandbox
 * @extends Roc.Router
 * @xtype routerviewsandbox
 *
 * Sandbox router helper to easily manage view by xtype.
 *
 * ### Example
 *
 *      var history = Roc.History,
 *          session = App.Session,
 *          parent = Roc.RouterViewSandbox;
 *
 *      var routerview = new (parent.subclass({
 *
 *          xtype: 'user.router',
 *
 *          before: {
 *              'routeProfile': 'isAuthenticated'
 *          },
 *
 *          routes: {
 *              {{ route "/user/login" as="user.login" method="routeLogin" }},
 *              {{ route "/user/profile" as="user.profile" method="routeProfile" }}
 *          },
 *
 *          isAuthenticated: function(done) {
 *
 *              if (session.logged === false) {
 *                  history.go({{ url "user.login" }});
 *                  return false;
 *              }
 *
 *              // user is logged already so we continue
 *              return done();
 *          },
 *
 *          routeLogin: function() {
 *              this.setCurrentView('user.login');
 *          },
 *
 *          routeProfile: function() {
 *              this.setCurrentView('user.profile');
 *          }
 *      }));
 *
 */
Roc.RouterViewSandbox = (function() {

    var _ns = Roc,
        _history = _ns.History,
        _parent = _ns.Router;

    function has_fx(self, xtype) {
        if (self.fx && self.fx[xtype]) {
            return true;
        }
        return false;
    }

    function get_fx(self, xtype) {
        return (self.fx[xtype]);
    }

    function fx(self, xtype, done) {

        var config = get_fx(self, xtype),
            show = config.show,
            effect = new (_ns.xtype(config.xtype))(config),
            previousView = self.getPreviousView();

        if (previousView && config[previousView.xtype]) {
            show = config[previousView.xtype];
        }

        var opts = {
            speed: show.speed,
            effect: show.me,
            el: self.currentView.el,
            callback: done
        };

        if (previousView) {
            if (show.other) {
                opts.fromEl = previousView.el;
                opts.fromEffect = show.other;
            } else {
                previousView.hide();
            }
        }

        effect.run(opts);

    }

    return _parent.subclass({

        xtype: 'routerviewsandbox',
        hasListeners: ['queuePop'],
        listeners: {
            priority: 'VIEWS',
            queuePop: function(self) {

                if (!self.queue.length) {
                    return false;
                }

                var item = self.queue.pop();

                self.setCurrentView(item.xtype);
            }
        },

        /**
         * Create a new router view.
         *
         * @constructor
         */
        constructor: function() {

            var self = this;

            self.views = {};
            self.animating = false;
            self.vqueue = [];
            self.queue = [];

            _parent.prototype.constructor.apply(self, arguments);

        },

        /**
         * Remove view from given xtype.
         * It will simply hide the view and keep the instance.
         *
         * @param  {String} xtype View xtype
         */
        removeView: function(xtype) {

            var self = this;

            if (self.views[xtype]) {
                // self.views[xtype].remove();
                // delete self.views[xtype];
                self.views[xtype].hide();
            }

        },

        /**
         * Remove current view.
         * @fires removeCurrentView if global is not false.
         * @param  {Boolean} [global=true] Global flag to let other router view catch this event.
         */
        removeCurrentView: function(global) {

            var self = this;

            if (self.currentView) {
                self.removeView(self.currentView.xtype);
                if (self.rvid) {
                    self.off(self.rvid);
                    self.rvid = false;
                }
            }
            if (global !== false) {
                self.fire('removeCurrentView', false);
            }

        },

        /**
         * Add previous view to queue.
         * @param {Object} view View instance.
         */
        addPreviousView: function(view) {
            this.vqueue.push(view);
        },

        /**
         * Get previous view.
         * @return {Mixed} View instance if found else false.
         */
        getPreviousView: function() {

            var self = this;

            if (self.vqueue.length > 0) {
                return self.vqueue[self.vqueue.length - 1];
            }
            return false;
        },

        /**
         * Remove previous view. It will hide it.
         * If current view is same as previous view, it will do nothing.
         * @param  {Object} currentView Current view.
         */
        removePreviousView: function(currentView) {

            var self = this;

            if (self.vqueue.length) {

                var view = self.vqueue.pop();
                console.log('<<< Remove previous sandbox view: ' + view.xtype);
                if (view.config.id !== currentView.config.id) {
                    view.hide();
                }
            }

        },

        /**
         * Add given parameter to queue.
         * It is called by setCurrentView when animating is already running.
         * @param  {String} xtype  View xtype
         * @param  {Object} [config] View configuration
         * @param  {Mixed} [args]   View arguments
         */
        queue: function(xtype, config, args) {

            var self = this;

            self.queue = [];
            self.queue.push({
                router: this,
                xtype: xtype,
                config: config,
                args: args
            });
        },

        /**
         * Set current view.
         * @param {String} xtype  View xtype
         * @param {Object} config Configuration for view constructor.
         * @param {Mixed} args   Arguments passed to view show event listeners.
         */
        setCurrentView: function(xtype, config, args) {

            var self = this;

            if (self.animating === true) {
                return self.queue(xtype, config, args);
            }

            self.currentView = self.getView(xtype, config);

            var previous = self.getPreviousView();

            if (previous !== false &&
                previous.config.id === self.currentView.config.id) {
                console.log('=== Current sandbox view is same as previous view: ' + xtype);
                return self.currentView;
            }

            console.log('>>> Set current sandbox view to: ' + xtype);

            // fx stuff
            if (has_fx(self, xtype)) {
                self.animating = true;
                self.currentView.show(args);
                fx(self, xtype, function() {
                    self.removePreviousView(self.currentView);
                    self.addPreviousView(self.currentView);
                    self.animating = false;
                    _events.fire('queuePop', self);
                });
            } else {
                self.animating = false;
                self.currentView.show(args);
                self.removePreviousView(self.currentView);
                self.addPreviousView(self.currentView);
            }

            return self.currentView;
        },

        /**
         * Get view from given xtype.
         * If the view does not exists, it will create new instance.
         * @param  {String} xtype View xtype
         * @return {Object}       View instance.
         */
        getView: function(xtype, config) {

            var self = this;

            if (!self.views[xtype]) {

                var view = self.views[xtype] = new (_ns.xtype(xtype))(config);

                view.compile();
                if (!self.view) {
                    view.render('body', 'beforeend');
                } else {
                    if (self.selector) {
                        view.render(self.view.el.querySelector(self.selector), 'beforeend');
                    } else {
                        view.render(self.view.el, 'beforeend');
                    }
                }
            }
            return self.views[xtype];
        }

    });

}());
/**
 * @class Roc.helpers.i18n
 * @extends Roc
 *
 * I18N helper.
 *
 * ### Example
 *
 *      var i18n = new Roc.helpers.i18n();
 *
 *      i18n.addLang('cn', {
 *          'Highlights': '推荐',
 *          'Discover': '分类',
 *          //...
 *      });
 *
 *      i18n.setLang('cn');
 *
 *      // Then you can use global `t` function
 *      var title = t('Highlights');
 *
 *      // And `t` helper in handlebars templates:
 *      {{ t "Highlights" }}
 */
Roc.helpers.i18n = (function(gscope) {

    'use strict';

    var _ns = Roc,
        // stores all languages available (after load)
        _langs = {},
        // stores current language
        _currentLang = {},
        // activated language
        _lang = 'en',
        _defaultScope = gscope,
        _handlebars = Handlebars,
        _parent = _ns;

    /**
     * Translate function.
     * @private
     * @param  {String} str String to translate with current language.
     * @return {String} Translated string.
     */
    function _t(str) {
        return (_currentLang[str] || str);
    }

    if (_handlebars) {
        _handlebars.registerHelper('t', _t);
    }

    _defaultScope.t = _t;

    return _parent.subclass({

        /**
         * Set language to given name.
         * @param {String} name Language name.
         */
        setLang: function(name) {
            _lang = name;
            _currentLang = _langs[_lang];
        },

        /**
         * Add language.
         * @param {String} name       Language name
         * @param {Object} values     Translation dictionnary.
         * @param {Boolean} [setDefault=false] True to set this language as default.
         */
        addLang: function(name, values, setDefault) {
            _langs[name] = values;
            if (setDefault) {
                this.setLang(name);
            }
        }
    });

}(this));

/**
 * Direction event.
 * @class Roc.events.Direction
 * @extends Roc
 * @xtype events.direction
 *
 * ### Example
 *
 *      afterrender: function(self) {
 *
 *          self.events.add('hideonscroll', {
 *              xtype: 'events.direction',
 *              el: self.el,
 *              handler: function(direction) {
 *                  if (direction === 'up') {
 *                      self.setVisible(true);
 *                  } else {
 *                      self.setVisible(false);
 *                  }
 *              }
 *          });
 *
 *      }
 */
Roc.events.Direction = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns,
        _events = new _ns.Event(),
        _attached = false,
        _tid = false,
        _timerInterval = 300, // 300ms
        _previous = false,
        _current = false,
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    function timer() {
        _events.fire('move', ((_previous - _current) > 0 ? 'down' : 'up'));
    }

    function touchstart(event) {
        if (event.touches.length === 1) {
            _previous = event.touches[0].clientY;
            _tid = setInterval(timer, _timerInterval);
        }
    }

    function touchmove(event) {
        if (event.touches.length === 1) {

            if (_current !== false) {
                _previous = _current;
            }
            _current = event.touches[0].clientY;
        }
    }

    function touchend(event) {
        if (_tid !== false) {
            if ((_previous !== _current) &&
                _previous !== false &&
                _current !== false) {
                timer();
            }
            _previous = false;
            _current = false;
            clearInterval(_tid);
            _tid = false;
        }
    }

    function touchcancel(event) {
        if (_tid !== false) {
            clearInterval(_tid);
            _tid = false;
        }
    }

    function mousewheel(event) {
        if (event.wheelDelta >= 0) {
            _events.fire('move', 'up');
        } else {
            _events.fire('move', 'down');
        }
    }

    function _attach() {

        _attached = true;

        if ('ontouchstart' in window) {
            document.addEventListener('touchstart', touchstart, false);
            document.addEventListener('touchmove', touchmove, false);
            document.addEventListener('touchend', touchend, false);
            document.addEventListener('touchcancel', touchcancel, false);
        } else {
            document.addEventListener('mousewheel', mousewheel, false);
        }

    }

    function _detach() {

        _attached = false;

        if ('ontouchstart' in window) {
            document.removeEventListener('touchstart', touchstart, false);
            document.removeEventListener('touchmove', touchmove, false);
            document.removeEventListener('touchend', touchend, false);
            document.removeEventListener('touchcancel', touchcancel, false);
        } else {
            document.removeEventListener('mousewheel', mousewheel, false);
        }

    }

    // public
    return _parent.subclass({

        className: 'Roc.events.Direction',
        xtype: 'events.direction',

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        useCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = (opts.scope ? opts.scope : self);
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }

        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler : self.defaultHandler);
            if (_attached === false) {
                _attach();
            }
            self.eid = _events.on('move', self.handler, scope);
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;

            var count = _events.off(self.eid);

            if (!count) {
                _detach();
            }
        }

    });
})();

/**
 * Simple CRUD form.
 * @class Roc.forms.Simple
 * @extends Roc.views.Template
 * @xtype forms.simple
 *
 * ### Example
 *
 *      var parent = Roc.forms.Simple;
 *
 *      var my_form = parent.subclass({
 *          proxy: 'jsonp',
 *          listeners: {
 *              aftersubmit: function(form, success, data) {
 *                  if (!success) return alert('Server unreachable');
 *                  else if (!data.success) return alert('Data not correct');
 *              }
 *          },
 *          read_my_field_name: function(value) {
 *              if (value === false) return 'NO';
 *              else return 'YES';
 *          },
 *
 *          constructor: function() {
 *
 *              var self = this;
 *
 *              var form = [{
 *                  xtype: 'textfield',
 *                  config: {
 *                      label: 'Name',
 *                      name: 'name',
 *                      required: true,
 *                      placeHolder: 'Your name'
 *                  }
 *              }, {
 *                  xtype: 'button',
 *                  config: {
 *                      text: 'Submit'
 *                  },
 *                  handler: function() {
 *                      self.submit();
 *                  }
 *              }];
 *
 *              var options = {
 *                  items: form
 *              };
 *
 *              parent.prototype.constructor.call(self, options);
 *          }
 *      });
 */
if (!Roc.forms) Roc.forms = {};
Roc.forms.Simple = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _request = _ns.Request,
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    var reader = function(self, field_name, field_value) {

        var method_name = 'read_' + field_name;

        if (typeof self[method_name] === 'function') {
            return self[method_name](field_value);
        }
        return field_value;
    };

    var _fieldsToJson = function(self, selector, root) {

        var field,
            result = {},
            fields = $.gets(selector, root),
            i = fields.length;

        while (i--) {
            field = fields[i];
            if (field.getAttribute('required') !== null &&
                !field.value.length) {
                return false;
            } else if (field.validity.valid === false) {
                return false;
            }

            var name = field.getAttribute('name');

            result[name] = reader(self, name, field.value);
        }
        return result;
    };

    var _validateFields = function(selector, root) {

        var field,
            result = true,
            fields = $.gets(selector, root),
            i = fields.length;

        while (i--) {
            field = fields[i];
            if (field.getAttribute('required') !== null &&
                !field.value.length) {
                $.addClass(field.parentNode, 'fielderror');
                result = false;
            } else if (field.validity.valid === false) {
                $.addClass(field.parentNode, 'fielderror');
                result = false;
            } else {
                $.removeClass(field.parentNode, 'fielderror');
            }
        }

        return result;

    };

    var _jsonToFields = function(self, json, fields) {
        if (json) {
            var field, name, i = fields.length;

            while (i--) {
                field = fields[i];
                name = field.getAttribute('name');
                if (typeof json[name] !== 'undefined') {
                    field.value = reader(self, name, json[name]);
                } else {
                    field.value = '';
                }
            }
        }
    };

    var _replaceByValues = function(str, fields, encode) {

        var i;

        for (i in fields) {

            var value = fields[i];

            if (typeof encode === 'function') {
                value = encode(value);
            }
            str = str.replace('{' + i + '}', value);
        }
        return str;
    };

    // public
    return _parent.subclass({

        className: 'Roc.forms.Simple',
        xtype: 'forms.simple',
        hasListeners: ['aftersubmit'],
        listeners: {
            priority: 'VIEWS',
            afterrender: function(self) {
                [].forEach.call(self.el.querySelectorAll('input[name]'), function(el) {
                    self.events.add('submit-on-enter-' + el.getAttribute('name'), {
                        xtype: 'events.keypress',
                        el: el,
                        handler: function(evt) {
                            if (evt.keyCode === 13) {
                                evt.target.blur();
                                self.submit();
                            }
                        }
                    });
                });
            }
        },
        implements: ['events'],

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            /**
             * @cfg {Object} [cmp=this] Component containing fields.
             */
            if (opts.cmp) {
                self.cmp = opts.cmp;
            } else {
                self.cmp = self;
            }

            /**
             * @cfg {String} [url] Submit form URL
             */
            if (opts.url) {
                self.url = opts.url;
            }

            /**
             * @cfg {Object} [urls] Form URLs
             * @cfg {String} [urls.read] Read URL
             * @cfg {String} [urls.update] Update URL
             * @cfg {String} [urls.remove] Remove URL
             */
            if (opts.urls) {
                self.urls = opts.urls;
            }

            /**
             * @cfg {Object} [loader] Form loader component.
             */
            if (opts.loader) {
                self.loader = opts.loader;
            }

            /**
             * @cfg {String} [proxy="jsonp"] Proxy type:
             * <ul>
             *  <li>ajax</li>
             *  <li>jsonp</li>
             *  <li>See Roc.Request for other proxy type.</li>
             * </ul>
             */
            if (opts.proxy) {
                self.proxy = opts.proxy;
            }

            if (typeof self.proxy === 'string') {
                if (!_request[self.proxy]) {
                    console.warn('Roc.forms.Simple: Proxy "', self.proxy, '" does not exists.');
                }
                self.proxy = _request[self.proxy];
            }

            if (typeof self.proxy !== 'function') {
                self.proxy = _request.jsonp;
            }

            _parent.prototype.constructor.call(self, opts);

            self.setupListeners([
                'afterupdate',
                'afterremove',
                'aftersubmit',
                'afterread'
            ]);
        },

        /**
         * Check if form fields are valid or not.
         * @return {Boolean} True if all fields valid, else false.
         */
        isValid: function() {
            if (_validateFields('[name]', this.cmp.el) === false) {
                return false;
            }
            return true;
        },

        /**
         * Reset form by removing all values from fields.
         */
        reset: function() {

            var field,
                fields = $.gets('[name]', this.cmp.el),
                i = fields.length;

            while (i--) {
                field = fields[i];

                if (typeof field.value !== 'undefined') {
                    field.value = '';
                }
            }

        },

        /**
         * Read form data from server with given options.
         * @fires afterread
         * @param  {Object} [opts] Options
         * @param {Object} [opts.args] URL arguments dictionnary. Will add those values to URL.
         * @param {Function} [opts.callback] Callback function.
         */
        read: function(opts) {

            var self = this,
                url = self.urls.read;

            if (opts.args) {
                url = _replaceByValues(url, opts.args, encodeURIComponent);
            }

            if (self.loader) {
                self.loader.show();
            }

            self.proxy(url, {
                scope: self,
                callback: function(success, data) {

                    if (self.loader) {
                        self.loader.hide();
                    }

                    if (success && typeof data === 'string') {
                        data = JSON.parse(data);
                    }

                    if (success && data.success) {
                        self.json(data.data);
                    }

                    if (opts.callback) {
                        opts.callback(success, data);
                    }

                    self.fire('afterread', self, success, data);

                }
            });

        },

        /**
         * Submit updated field values to server.
         * @fires afterupdate
         * @param  {Object} [opts] Options
         * @param {Function} [opts.callback] After submit callback
         */
        update: function(opts) {

            var self = this;

            opts = opts || {};

            opts.url = self.urls.update;
            opts.operation = 'update';
            self.submit(opts);

        },

        /**
         * Remove values from server.
         * @fires afterremove
         * @param  {Object} opts Options
         */
        remove: function(opts) {

            var self = this;

            opts = opts || {};

            opts.url = self.urls.remove;
            opts.operation = 'remove';
            self.submit(opts);

        },

        /**
         * Apply given data to fields.
         * @param  {Object} data Dictionnary values
         */
        json: function(data) {

            var self = this;

            if (data) {
                self.data = data;
                return (_jsonToFields(self, data,
                    self.cmp.el.querySelectorAll('[name]')));
            }
            return _fieldsToJson(self, '[name]', self.cmp.el);
        },

        /**
         * Submit data to server if form is valid.
         * @fires aftersubmit
         * @param  {Object} [opts] Options
         * @param {String} [opts.operation="submit"] Operation name.
         * <ul>
         *  <li>submit</li>
         *  <li>remove</li>
         *  <li>update</li>
         *  <li>read</li>
         * </ul>
         * @param {Function} [opts.callback] Finish callback.
         * @param {Object} [opts.args] URL dictionnary arguments.
         * @return {Mixed} False if form is not valid.
         */
        submit: function(opts) {

            opts = opts || {};

            var self = this,
                url = (opts.url ? opts.url : self.url),
                operation = (opts.operation ? opts.operation : 'submit'),
                callback = opts.callback;

            if (self.isValid() === false) {
                if (callback) {
                    callback(false);
                }
                return false;
            }

            var fields = self.json();

            if (opts.args) {
                url = _replaceByValues(url, opts.args, encodeURIComponent);
            }

            if (fields !== false) {
                url = _replaceByValues(url, fields, encodeURIComponent);

                if (self.loader) {
                    self.loader.show();
                }

                self.proxy(url, {
                    scope: self,
                    callback: function(success, data) {

                        if (self.loader) {
                            self.loader.hide();
                        }

                        if (callback) {
                            callback(success, data);
                        }
                        self.fire(('after' + operation), self, success, data);
                    }
                });
            }
        }

    });

})();

/**
 * @class Roc.data.Store
 * @extends Roc.Event
 * @requires Roc.Storage
 * @requires Roc.Request
 * @xtype store
 *
 * Store class. Used for retrieve data from given proxy configuration and link them to
 * view.
 *
 * ## Example
 *
 *     this.newsStore = new Roc.data.Store({
 *         enableHashTable: true,
 *         storageCache: true,
 *         reader: {
 *             idProperty: 'id',
 *             rootProperty: 'data',
 *             successProperty: 'success',
 *             totalProperty: 'total'
 *         },
 *         proxy: {
 *             type: 'jsonp',
 *             cache: false,
 *             url: 'http://example.com/news/list.json?callback={callback}'
 *         },
 *         listeners: {
 *             scope: this,
 *             loaderror: onStoreError
 *         }
 *     });
 *
 *     this.newsStore.load({
 *         jsonParams: { page: 1, dir: 'ASC', name: 'test%' },
 *         scope: this,
 *         callback: onDataLoaded
 *     });
 *
 */
(function() {

    'use strict';

    var _ns = Roc,
        _storage = _ns.Storage,
        _debug = _ns.debug,
        _request = _ns.Request,
        _parent = _ns.Event,
        _win = window,
        _online = _win.navigator.onLine;

    _win.addEventListener('online', function() {
        _online = true;
    }, false);

    _win.addEventListener('offline', function() {
        _online = false;
    }, false);

    /**
     * @cfg {Object} proxy Proxy configuration.
     *
     * ## Example
     *
     *     proxy: {
     *         url: 'http://example.com/data/list.json',
     *         type: 'ajax' // or `json` or `memory`
     *     }
     *
     */

    /**
     * @cfg {Object} reader Store reader configuration.
     *
     * ## Example (with default values)
     *
     *     reader {
     *         idProperty: 'id',
     *         rootProperty: 'data',
     *         successProperty: 'success',
     *         totalProperty: 'total'
     *     }
     *
     */
    _ns.data.Store = _parent.subclass({

        xtype: 'store',
        defaultEnableHashTable: false,
        defaultReaderRoot: 'data',
        defaultReaderTotal: 'total',
        defaultReaderSuccess: 'success',
        defaultReaderId: 'id',
        defaultStorageCache: false,

        /**
         * Create new store.
         * @param {Object} config Configuration.
         */
        constructor: function(config) {

            var self = this;

            self.config = config || {};
            self.totalRecords = config.totalRecords || 0;
            self.loadedRecords = 0;
            self.records = [];

            /**
             * @property {Boolean} isLoading Current loading status.
             * @readonly
             */
            self.isLoading = false;

            /**
             * @cfg {Boolean} enableHashTable Enable hash table for store records.
             * Allows developer to use {@link #getRecord getRecord} method to quickly find record.
             * Defaults to `false`.
             */
            self.enableHashTable = config.enableHashTable || self.defaultEnableHashTable;

            /**
             * @cfg {Boolean} storageCache Enable or disable local storage cache for store remote results.
             * Defaults to `false`.
             */
            self.storageCache = config.storageCache || self.defaultStorageCache;

            if (self.enableHashTable) {
                self.hashRecords = {};
            }

            self.rootProperty = self.getReaderRoot(self.defaultReaderRoot);
            self.totalProperty = self.getReaderTotal(self.defaultReaderTotal);
            self.successProperty = self.getReaderSuccess(self.defaultReaderSuccess);
            self.idProperty = self.getReaderId(self.defaultReaderId);

            _parent.prototype.constructor.apply(self, arguments);

            self.setupListeners([
                /**
                 * @event load
                 * Fired after data from load request from {@link #method-load load} method call but before `callback` call.
                 * @param {Boolean} success Request success.
                 * @param {@link Roc.data.Store} store Store.
                 * @param {Array} newRecords New records.
                 */
                'load',

                /**
                 * @event afterload
                 * Fired after data from load request from {@link #method-load load} method call and after `callback` call.
                 * @param {Boolean} success Request success.
                 * @param {@link Roc.data.Store} store Store.
                 * @param {Array} newRecords New records.
                 */
                'afterload',

                /**
                 * @event beforeload
                 * Fired before load request from {@link #method-load load} method called.
                 * @param {@link Roc.data.Store} store Store.
                 * @param {Object} options Load request options.
                 */
                'beforeload',

                /**
                 * @event loaderror
                 * Fired when error occured from {@link #method-load load} method call.
                 * @param {Boolean} success Request success.
                 * @param {@link Roc.data.Store} store Store.
                 * @param {String} data Data from request result.
                 */
                'loaderror'
            ]);

        },

        /**
         * Load data from proxy configuration to store.
         *
         * ## Example
         *
         *     store.load({
         *         url: 'http://example.com/news/list.json',
         *         // ... other proxy request configuration goes here.
         *         scope: store,
         *         callback: onLoadComplete
         *     });
         *
         * @method
         * @fires beforeload
         * @fires load
         * @fires afterload
         * @fires loaderror
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        load: function(opts) {
            opts = _ns.utils.applyIfAuto(opts || {}, this.getProxyOpts());
            this.isLoading = true;

            var request,
                self = this,
                type = self.getProxyType(),
                finalCb = opts.callback;

            self.fire('beforeload', self, opts);
            var cb = function(success, data) {
                var total,
                    newRecords = [];

                if (typeof data === 'string') {
                    data = JSON.parse(data);
                }

                if (success && data[self.successProperty]) {
                    total = data[self.totalProperty];
                    if (typeof total !== 'undefined') {
                        self.totalRecords = total;
                    }
                    newRecords = data[self.rootProperty];
                    if (self.enableHashTable) {
                        var recordId, length = newRecords.length;
                        while (length--) {
                            recordId = newRecords[length][self.idProperty];
                            self.hashRecords['r' + recordId] = self.loadedRecords + length;
                        }
                    }
                    self.loadedRecords += newRecords.length;
                    self.records = self.records.concat(newRecords);
                } else {
                    self.fire('loaderror', success, self, data);
                }
                self.fire('load', success, self, newRecords);
                if (finalCb) {
                    finalCb(success, self, newRecords);
                }
                self.fire('afterload', success, self, newRecords);
                self.isLoading = false;
            };

            opts.url = opts.url || self.getProxyUrl();

            if (self.storageCache) {
                var key = JSON.stringify(_ns.utils.crc32(JSON.stringify(opts))),
                    results = _storage.getItem(key);
                if (_online === false && results) {
                    setTimeout(function() {
                        cb(true, results);
                    }, 10);
                    return true;
                } else {
                    var tmp = cb;
                    cb = function(success, data) {
                        if (success && data[self.successProperty]) {
                            _storage.setItem(key, data);
                        }
                        tmp(success, data);
                    };
                }
            }

            // if (self.getProxyCache() === false) {
            //     self.addUrlTimestamp(opts.url);
            // }
            opts.callback = cb;
            if (typeof _request[type] === 'function') {
                request = _request[type];
            } else {
                request = _request.ajax;
            }
            return request(opts.url, opts, self);
        },

        /**
         * Add given record to the end of list or start of list.
         *
         * @method
         * @param {Object} record Record to add.
         * @param {Boolean} fromStart Add record from start of current records list.
         * Defaults to `false`.
         */
        addRecord: function(record, fromStart) {

            var self = this,
                recordId = record[self.idProperty];

            if (self.enableHashTable) {
                self.hashRecords['r' + recordId] = self.loadedRecords;
            }
            if (fromStart === true) {
                self.records = [record].concat(self.records);
                if (self.enableHashTable) {
                    var rec,
                        i = self.loadedRecords + 1;
                    while (i--) {
                        rec = self.records[i];
                        self.hashRecords['r' + rec[self.idProperty]] = i;
                    }
                }
            } else {
                self.records = self.records.concat([record]);
            }
            self.loadedRecords += 1;
            self.totalRecords += 1;
        },

        /**
         * Reset/Empty all data. TODO: Need to rename to `empty` ?
         *
         * @method
         */
        reset: function() {

            var self = this;

            self.records = [];
            self.totalRecords = 0;
            self.loadedRecords = 0;
            if (self.enableHashTable) {
                self.hashRecords = {};
            }
        },

        // addUrlTimestamp: function(url) {
        //     // @ignore TODO!
        //     //var date = new Date();
        // },

        /**
         * Get record from its `idProperty` value.
         * Configuration option {@link #enableHashTable enableHashTable}
         * needs to be set to `true`.
         *
         * @method
         * @param {Mixed} recordId Record id.
         * @return {Mixed} Return record if found else `undefined`.
         */
        getRecord: function(recId) {
            // <debug>
            if (!this.enableHashTable) {
                _debug.error('store', 'You must enable hash table for using store.getRecord by setting enableHashTable: true in store config.');
                return;
            }
            // </debug>
            return this.records[this.hashRecords['r' + recId]];
        },

        /**
         * Get record at given index.
         *
         * @method
         * @param {Number} index Index.
         * @return {Mixed} Return record if found else `undefined`.
         */
        getAt: function(index) {
            return this.records[index];
        },

        setProxyOpts: function(opts) {
            if (!this.config.proxy) {
                this.config.proxy = {};
            }
            this.config.proxy.opts = opts;
            return true;
        },

        getProxyOpts: function(defaultValue) {
            defaultValue = defaultValue || {};
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.opts || defaultValue;
            }
            return defaultValue;
        },

        getProxyUrl: function(defaultValue) {
            defaultValue = defaultValue || '';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.url || defaultValue;
            }
            return defaultValue;
        },

        getProxyCache: function(defaultValue) {
            defaultValue = defaultValue || true;
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return (p.cache === false ? false : defaultValue);
            }
            return defaultValue;
        },

        getProxyType: function(defaultValue) {
            defaultValue = defaultValue || 'ajax';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.type || defaultValue;
            }
            return defaultValue;
        },

        getReaderRoot: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderRoot;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.rootProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderTotal: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderTotal;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.totalProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderSuccess: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderSuccess;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.successProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderId: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderId;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.idProperty || defaultValue;
            }
            return defaultValue;
        }
    });
}());

/**
 * @class Roc.data.Pagingstore
 * @extends Roc.data.Store
 * @xtype pagingstore
 *
 * Paging store. Contains easy setup methods for retrieve records with paginator.
 *
 * ## Example
 *
 *     this.newsStore = new Roc.data.Pagingstore({
 *         enableHashTable: true,
 *         recordPerPage: 10,
 *         storageCache: true,
 *         reader: {
 *             idProperty: 'id',
 *             rootProperty: 'data',
 *             successProperty: 'success',
 *             totalProperty: 'total'
 *         },
 *         proxy: {
 *             type: 'jsonp',
 *             cache: false,
 *             url: 'http://example.com/news/list.json?callback={callback}'
 *         },
 *         listeners: {
 *             scope: this,
 *             loaderror: onStoreError
 *         }
 *     });
 *
 *     this.newsStore.loadPage(1, {
 *         scope: this,
 *         callback: onDataLoaded
 *     });
 *
 *     this.newsStore.nextPage();
 *
 */
Roc.data.Pagingstore = (function() {

    'use strict';

    var _ns = Roc,
        _parent = _ns.data.Store;

    return _parent.subclass({

        xtype: 'pagingstore',

        /**
         * Create new paging store.
         * @param {Object} config Configuration.
         */
        constructor: function(config) {
            config = config || {};

            /**
             * @cfg {Number} currentPage (optional) Current page.
             */
            this.currentPage = config.currentPage || 0;

            /**
             * @cfg {Number} recordPerPage (optional) Number of record per page.
             */
            this.recordPerPage = config.recordPerPage || 10;
            _parent.prototype.constructor.call(this, config);
        },

        /**
         * Load the given page.
         *
         * @param {Number} page Page number.
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        loadPage: function(page, opts) {

            opts = opts || {};
            opts.url = opts.url || this.getProxyUrl();

            var nb = this.recordPerPage,
                paging = 'start=' + (page * nb) +
                    '&limit=' + nb;

            if (opts.url.indexOf('?') !== -1) {
                opts.url += '&' + paging;
            } else {
                opts.url += '?' + paging;
            }
            return this.load(opts);
        },

        /**
         * Load the next page.
         *
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        nextPage: function(opts) {
            return this.loadPage(this.currentPage++, opts);
        },

        /**
         * Load the previous page.
         *
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        previousPage: function(opts) {
            return this.loadPage(this.currentPage--, opts);
        },

        /**
         * Reset/Empty all data. TODO: Need to rename to `empty` ?
         */
        reset: function() {
            this.currentPage = 0;
            _parent.prototype.reset.apply(this, arguments);
        }
    });

}());

Roc.engines.ionic = (function() {

    //'use strict';

    /* jshint -W030 */
    var _ns = Roc,
        _views = _ns.views,
        _event = _ns.events,
        _parent = _ns,
        $ = _ns.Selector,
        _templates = _ns.Templates,
        _handlebars = Handlebars,
        _tpls = _handlebars.templates,
        _events = {};
    /* jshint +W030 */

    _ns.global.on('ready', function() {

        var win = window,
            nav = win.navigator,
            body = document.body,
            ua = nav.userAgent;

        if (ua.indexOf('CPU iPhone OS 7') !== -1 ||
            ua.indexOf('iPhone OS 7') !== -1 ||
            ua.indexOf('CPU OS 7') !== -1) {

            $.addClass(body, 'platform-ios7');

        } else if (ua.indexOf('Android ') !== -1) {

            var android = ua.match(/Android [\d\.]+/ig),
                version = parseFloat(android[0].split(' ')[1]);

            if (version < 4.4) {
                $.addClass(body, 'grade-' + (version < 4 ? 'c' : 'b'));
            }

            $.addClass(body, 'platform-android');
        }
        if (nav.standalone === true ||
            typeof win.cordova !== 'undefined' ||
            typeof win.phonegap !== 'undefined') {
            $.addClass(body, 'platform-cordova');
        } else {
            win.addEventListener('deviceready', function() {
                $.addClass(body, 'platform-cordova');
            }, false);
        }

        var focusin = function() {
            $.addClass(body, 'keyboard-open');
        };

        var focusout = function() {
            $.removeClass(body, 'keyboard-open');
        };

        if (ua.indexOf('Android ') !== -1) {

            var mustHide = false;

            win.addEventListener('click', function(evt) {

                if (['SELECT', 'INPUT', 'TEXTAREA'].indexOf(evt.target.tagName) > -1) {
                    mustHide = true;
                    focusin();
                } else if (mustHide === true) {
                    mustHide = false;
                    focusout();
                }

            }, false);
        } else {
            win.addEventListener('focusin', focusin, false);
            win.addEventListener('focusout', focusout, false);
        }

    });

    function findParent(el, callback) {
        if (callback(el) === false) {
            if (el.parentNode) {
                return findParent(el.parentNode, callback);
            }
            return false;
        }
        return el;
    }

    function activateButton(self, event) {

        var activeEl = self.el.querySelector('.active');

        if (activeEl) {
            $.removeClass(activeEl, 'active');
        }

        findParent(event.target, function(el) {
            if (el.tagName === 'BUTTON') {
                $.addClass(el, 'active');
                return true;
            }
            return false;
        });
    }

    function activateTab(self, event) {

        var activeEl = self.el.querySelector('.active');

        var parent = findParent(event.target, function(el) {
            if ($.hasClass(el, 'tab-item')) {
                return true;
            }
            return false;
        });

        if (parent && parent !== activeEl) {
            if (activeEl) {
                $.removeClass(activeEl, 'active');
            }
            $.addClass(parent, 'active');
        }
    }

    var activeListItem = function(self, event) {

        findParent(event.target, function(el) {
            if ($.hasClass(el, 'item')) {
                $.addClass(el, 'active');

                var activeEl = self.el.querySelector('.active');

                if (activeEl) {
                    $.removeClass(activeEl, 'active');
                }

                var sid = setTimeout(function() {
                    $.removeClass(el, 'active');
                    clearTimeout(sid);
                }, 500);

                return true;
            }
            return false;
        });

    };

    _events.list = _events.listbuffered = {
        itemselect: activeListItem
    };

    _events.controlgroup = {
        select: activateButton
    };

    _events.toggle = {
        change: function(scope, component, checked) {

            var el = component.el.querySelector('input[type]');

            if (checked === true) {
                el.setAttribute('checked', '');
            } else {
                el.removeAttribute('checked');
            }
        }
    };

    _events.tabs = {
        select: activateTab,
        activate: function(self, name) {

            var el = self.el.querySelector('a[data-name="' + name + '"]');

            if (el) {
                activateTab(self, {
                    target: el
                });
            }

        }
    };

    _events.button = {
        seticon: function(self, icon) {
            if (icon === self.config.icon) {
                return false;
            }
            $.removeClass(self.el, 'ion-' + self.config.icon);
            $.addClass(self.el, 'ion-' + icon);
            self.config.icon = icon;
        }
    };

    _events.template = {
        show: function(self) {
            if (self.el) {
                $.removeClass(self.el, 'hidden');
            }
        },
        hide: function(self) {
            if (self.el) {
                $.addClass(self.el, 'hidden');
            }
        }
    };

    // helpers for setUI methods
    function _removeUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            $.removeClass(el, pattern.replace('#{ui}', ui[len]));
        }
    }

    function _addUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            $.addClass(el, pattern.replace('#{ui}', ui[len]));
        }
    }

    var _setUIBtn = function(ui) {

        var self = this;

        _removeUI('button-#{ui}', self.config.ui, self.el);
        _addUI('button-#{ui}', ui, self.el);

        self.config.ui = ui;
    };

    _ns.global.on('button_ready', function() {

        if (_views.Button) {
            _views.Button.prototype.setUI = _setUIBtn;
        }

    });

    return _parent.subclass({

        xtype: 'ionic',
        className: 'Roc.engines.ionic',
        version: '0.9.14',

        getTpl: function(name) {
            if (_tpls[name]) {
                return _tpls[name];
            }
            console.warn(this.className +
                ' engine: Template "%s" does not exists.', name);
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

/**
 * @class Roc.handlebars.helpers.ifeq
 *
 * Handlebars `if_eq` (if equals) helper.
 *
 * _Note_: Given `default` value is only used when `value` is undefined.
 *
 * ## Sample usage:
 *
 *      {{#if_eq value compare=true default=false}}
 *         Value is true
 *      {{else}}
 *         Value is false or undefined (default)
 *      {{/if_eq}}
 */
Handlebars.registerHelper('if_eq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context == options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});

/**
 * @class Roc.handlebars.helpers.foreach
 *
 * Handlebars `foreach` helper.
 *
 * ## Sample usage:
 *
 *      {{#foreach people}}
 *         Name: {{name}} {{! This show the name of each person inside people }}
 *         Index: {{$index}} {{! This is current index (Number) }}
 *         First: {{$first}} {{! true when first iteration else false }}
 *         Last: {{$last}} {{! true when last iteration else false }}
 *      {{/foreach}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 *      var config = {
 *          people = [{
 *              name: 'John Smith'
 *          }, {
 *              name: 'Hello World'
 *          }, {
 *              name: 'Jessica Alba'
 *          }]
 *      };
 *
 * #### Handlebars template
 *
 *      {{#foreach people}}
 *          My name is {{name}}.
 *          I am the index: {{$index}}
 *          {{#$first}}
 *              I am the first item !
 *          {{else}}
 *              {{#$last}}I am the last item !{{/$last}}
 *          {{/$first}}
 *      {{/foreach}}
 *
 * You can also access to `{{original}}` object,
 * which contains `people[$index]` data without the `$index`, `$first`, `$last` of foreach
 * (instead of `{{.}}`).
 *
 * #### Output
 *
 *      My name is John Smith.
 *      I am the index: 0
 *      I am the first item !
 *
 *      My name is Hello World
 *      I am the index: 1
 *
 *      My name is Jessica Alba
 *      I am the index: 2
 *      I am the last item !
 *
 */
Handlebars.registerHelper('foreach', function(arr, options) {

    var length = arr.length;

    if (options.inverse && !length) {
        return options.inverse(this);
    }

    return arr.map(function(item, index) {
        if (typeof item !== 'object') {
            item = {
                original: item
            };
        }
        item.$index = index;
        item.$first = index === 0;
        item.$last  = index === (length - 1);
        return options.fn(item);
    }).join('');

});

/**
 * @class Roc.handlebars.helpers.get
 *
 * Handlebars `get` helper.
 *
 * ## Sample usage:
 *
 *      {{get position default="left" choices="left|right"}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 *      var config = {
 *          hidden: true,
 *          required: undefined,
 *          disabled: false,
 *          iconPos: "right"
 *      };
 *
 * #### Handlebars template
 *
 *      {{get hidden default=false}}
 *      {{get required default=false}}
 *      {{get disabled default=true}}
 *      {{get iconPos default="left" choices="left|right"}}
 *
 * #### Output
 *
 *      true
 *      false
 *      false
 *      right
 *
 */
(function() {

    var _handlebars = Handlebars;

    var is_choice = function(choices, value) {

        if (typeof choices === 'string') {
            choices = choices.split('|');
            return (choices.indexOf(value) !== -1);
        }
        return false;

    };

    _handlebars.registerHelper('get', function(context, options) {

        var hash = options.hash;

        if (typeof context === 'undefined' ||
            is_choice(hash.choices, context)) {
            return hash['default'];
        }
        return context;
    });

}());

Handlebars.templates.listitemfull = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': ("divider")
  },inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.type), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.type), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += "<div class=\"item item-divider\">";
  if (helper = helpers.label) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</div>";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.arrow), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.arrow), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"list-item item";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.button), {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  options={hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data}
  if (helper = helpers.note) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.note); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.note) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.programWithDepth(19, program19, data, depth0),data:data}
  if (helper = helpers.bubble) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.bubble); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.bubble) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.programWithDepth(19, program19, data, depth0),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data}
  if (helper = helpers.button) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.button); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.button) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.arrow), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.arrow), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += "a";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  buffer += "div";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += " item-icon-"
    + escapeExpression((helper = helpers.get || (depth0 && depth0.get),options={hash:{
    'default': ("left")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.iconPos), options) : helperMissing.call(depth0, "get", (depth0 && depth0.iconPos), options)));
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += " item-button-"
    + escapeExpression((helper = helpers.get || (depth0 && depth0.get),options={hash:{
    'default': ("right")
  },data:data},helper ? helper.call(depth0, ((stack1 = (depth0 && depth0.button)),stack1 == null || stack1 === false ? stack1 : stack1.position), options) : helperMissing.call(depth0, "get", ((stack1 = (depth0 && depth0.button)),stack1 == null || stack1 === false ? stack1 : stack1.position), options)));
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon ion-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"></i>";
  return buffer;
  }

function program15(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

function program17(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"item-note\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }

function program19(depth0,data,depth1) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"badge badge-";
  stack1 = helpers['if'].call(depth0, (depth1 && depth1.ui), {hash:{},inverse:self.program(20, program20, data),fn:self.program(15, program15, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }
function program20(depth0,data) {
  
  
  return "assertive";
  }

function program22(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<button class=\"button";
  options={hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><i class=\"icon ion-";
  if (helper = helpers.icon) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"></i></button>";
  return buffer;
  }
function program23(depth0,data) {
  
  var buffer = "";
  buffer += " button-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
Handlebars.registerPartial("css_class", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.css_class) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.css_class); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.css_class) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
Handlebars.registerPartial("cssClass", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.cssClass) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.cssClass); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.css_class
 *
 * Handlebars `css_class` partial.
 *
 * This partial allows you to pass any CSS classes as string from `cssClass` or `css_class` variable inside component configuration.
 *
 * ## Sample usage
 *
 * 		{{> css_class}}
 * 		{{! or }}
 * 		{{> cssClass}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			css_class: "raw css classes"
 *		};
 *
 * #### Handlebars template
 *
 *		<div class="{{> css_class}}"></div>
 *
 * #### Output
 *
 * 		<div class="raw css classes"></div>
 *
 */
Handlebars.registerPartial("style", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.style) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.style); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.style
 *
 * Handlebars `style` partial.
 *
 * This partial is used to pass inline CSS style for HTMLElement.
 *
 * ## Sample usage
 *
 * 		{{> style}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			config: {
 *				title: 'Profile',
 *				style: 'background: #000; color: #fff;'
 *			}
 *		};
 *
 * #### Handlebars template
 *
 *		<div{{> id}}{{> style}}>
 *			{{#title}}<h2>{{.}}</h2>{{/title}}
 *		</div>
 *
 * #### Output
 *
 * 		<div id="fs1" style="background: #000; color: #fff;">
 * 			<h2>Profile</h2>
 * 		</div>
 *
 */

Handlebars.registerPartial("id", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.id
 *
 * Handlebars `id` partial.
 *
 * This partial allows you to pass any id as string from `id` variable inside component configuration.
 *
 * _Note_: You must use this partial in any templates which is link to a component to allow Roc retreive the HTMLElement by its own.
 *
 * ## Sample usage
 *
 *		{{> id}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			id: "html-element-id"
 *		};
 *
 * #### Handlebars template
 *
 *		<div{{> id}}></div>
 *
 * #### Output
 *
 * 		<div id="html-element-id"></div>
 *
 */
/**
 * @class Roc.handlebars.helpers.safe
 * @alternateClassName Roc.handlebars.helpers.raw
 *
 * Handlebars `safe`, `raw` helper.
 * By default Handlebars will sanetize HTML from all output.
 *
 * This helper allows you to force Handlebars to output this string as raw.
 *
 * ## Sample usage:
 *
 *		{{#safe items}}
 *         {{.}}
 *      {{/safe}}
 *      {{! OR }}
 *      {{safe item}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 * 		var config = {
 * 			html: '<p>test</p>'
 * 		};
 *
 * #### Handlebars template
 *
 * 		{{html}}
 * 		{{raw html}}
 * 		{{safe html}}
 *
 * #### Output
 *
 *      &lt;p&gt;test&lt;/p&gt;
 *      <p>test</p>
 *      <p>test</p>
 *
 */
(function() {

    var handlebars = Handlebars;

    handlebars.registerHelper('safe', function(str) {
        return new handlebars.SafeString(str);
    });

    handlebars.registerHelper('raw', function(str) {
        return new handlebars.SafeString(str);
    });

}());

Handlebars.registerPartial("items", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, self=this, helperMissing=helpers.helperMissing, functionType="function", blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var stack1, helper, options;
  stack1 = (helper = helpers.safe || (depth0 && depth0.safe),options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program2(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.items) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.items); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.items) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.items
 *
 * Handlebars `items` partial.
 *
 * This partial is used to push given items from components configuration into your template as HTML.
 *
 * ## Sample usage
 *
 * 		{{> items}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			config: {
 *				title: 'Profile'
 *			},
 *			items: [{
 *				xtype: 'button',
 *				config: {
 *					text: 'Back',
 *					route: '/home'
 *				}
 *			}]
 *		};
 *
 * #### Handlebars template
 *
 *		<div{{> id}}>
 *			{{> items}}
 *			{{#title}}<h2>{{.}}</h2>{{/title}}
 *		</div>
 *
 * #### Output
 *
 * 		<div id="fs1">
 * 			<button id="fs2" href="#/home">Back</button>
 * 			<h2>Profile</h2>
 * 		</div>
 *
 */

Handlebars.templates.listitembasic = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<a class=\"item list-item\">";
  options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
/**
 * @class Roc.views.Alert
 * @xtype alert
 * @extends Roc.views.Template
 *
 * Alert message box component.
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new Roc.views.Alert({
 *          config: {
 *              ui: 'assertive',
 *              title: 'Error',
 *              message: 'Something went wrong'
 *          }
 *      });
 */
Roc.views.Alert = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    function _afterrender(self) {

        var id = self.config.id;

        self.overlayEl = document.getElementById(id + '-overlay');
        self.closeEl = document.getElementById(id + '-close');

        if (self.config.overlayHide !== false) {
            self.events.add('overlay-click-hide', {
                xtype: 'events.click',
                el: self.overlayEl,
                scope: self,
                handler: self.hide
            });
        }

        if (self.closeEl) {
            self.events.add('close-click', {
                xtype: 'events.click',
                disabledScrolling: true,
                el: self.closeEl,
                scope: self,
                handler: self.hide
            });
        }
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.Alert',
        xtype: 'alert',
        hasListeners: [
            'show',
            'hide'
        ],
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        /**
         * Show alert.
         * @fires show
         */
        show: function() {

            var self = this;

            if (self.el) {
                _selector.removeClass(self.el, 'hidden');
                _selector.removeClass(self.overlayEl, 'hidden');
            }
            self.fire('show', self);
        },

        /**
         * Hide alert.
         * @fires hide
         */
        hide: function() {

            var self = this;

            if (self.el) {
                _selector.addClass(self.el, 'hidden');
                _selector.addClass(self.overlayEl, 'hidden');
            }
            self.fire('hide', self);
        }

    });
})();

Handlebars.templates.alert = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += "<div class=\"popup-head\"><h3 class=\"popup-title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h3></div>";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<div class=\"popup-body\"><span>"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span></div>";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += " popup-backdrop-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-overlay\"";
  return buffer;
  }

  buffer += "<div class=\"popup popup-showing active";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.title) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.message) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.message); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.message) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<div class=\"popup-buttons row\">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div></div><div class=\"popup-backdrop";
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.overlay) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.overlay); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.overlay) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options={hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></div>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Alert
 * @extends Roc.views.Alert
 * @xtype alert
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new Roc.views.Alert({
 *          config: {
 *              ui: 'assertive',
 *              title: 'Error',
 *              message: 'Something went wrong'
 *          }
 *      });
 */
(function() {

    var _ns = Roc,
        $ = _ns.Selector,
        _alert = _ns.views.Alert.prototype;

    /**
     * @cfg {Object} [config] Template configuration
     * @cfg {String} [config.cssClass] Additional CSS classes
     * @cfg {String} [config.style] Inline CSS style
     * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
     * @cfg {String} [config.title] Alert header title
     * @cfg {String} [config.message] Alert message.
     * @cfg {Mixed} [config.items] Alert body items.
     * @cfg {String} [config.overlay] Alert backdrop overlay
     * @cfg {Boolean} [config.overlayHide] Hide alert when clicking/tapping on overlay layer.
     */

    _alert.show = function() {

        var self = this;

        $.removeClass(self.overlayEl, 'hidden');
        $.removeClass(self.el, 'hidden');

        var top = (self.el.offsetHeight / 2),
            left = (self.el.offsetWidth / 2);

        self.el.setAttribute('style', ('margin-top: -' + top + 'px;' +
            'margin-left: -' + left + 'px;'));

        $.addClasses(self.el, 'popup-showing active');

        self.fire('show', self);
    };

    _alert.hide = function() {

        var self = this;

        $.removeClasses(self.el, 'active');
        $.addClass(self.el, 'popup-hidden');
        $.addClass(self.overlayEl, 'hidden');

        var tid = setTimeout(function() {
            $.addClass(self.el, 'hidden');
            $.removeClass(self.el, 'popup-hidden');
            clearTimeout(tid);

            self.fire('hide', self);
        }, 150);
    };

}());
/**
 * @class Roc.views.Button
 * @xtype button
 * @extends Roc.views.Template
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new Roc.views.Button({
 *          config: {
 *              label: 'Submit'
 *          },
 *          handler: function() {
 *              // form.submit();
 *          }
 *      });
 */
(function() {

    'use strict';

    // private
    var _ns = Roc,
        _views = _ns.views,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.events.add('click', {
            xtype: 'events.click',
            autoAttach: true,
            el: self.el,
            scope: self,
            handler: self.handler
        });
    }

    // public
    _views.Button = _parent.subclass({

        className: 'Roc.views.Button',
        xtype: 'button',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            /**
             * @cfg {Function} [handler] Click handler function.
             */
            /**
             * @cfg {Object} [scope] Handler function scope.
             */
            /**
             * @cfg {String} [route] Button default route. If handler has been override, this will not work by default.
             */

            if (opts.handler) {
                self.handler = (opts.scope ?
                    opts.handler.bind(opts.scope) : opts.handler);
            }

            _parent.prototype.constructor.call(self, opts);

        },

        /**
         * Default button handler.
         * If route has been defined, it will go to route when clicked.
         * @fires click
         * @param  {Event} evt Click or tap event
         */
        handler: function(evt) {

            var self = this,
                route = self.config.route;

            self.fire('click', self, evt);

            if (route) {
                _ns.History.go(route);
            }
        },

        /**
         * Set disabled state to button
         * @fires disabled
         * @param {Boolean} [disabled=false] Disabled state
         */
        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        },

        /**
         * Set icon
         * @fires seticon
         * @param {Mixed} icon Icon value, depends of which UI used.
         */
        setIcon: function(icon) {

            var self = this;

            self.fire('seticon', self, icon);
        }
    });

    _ns.global.fire('button_ready', true);

})();

Handlebars.templates.button = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " button-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, helper;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.iconPos), {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ion-";
  if (helper = helpers.icon) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " icon-";
  if (helper = helpers.iconPos) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.iconPos); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

function program6(depth0,data) {
  
  
  return " icon";
  }

function program8(depth0,data) {
  
  
  return " active";
  }

function program10(depth0,data) {
  
  
  return " hidden";
  }

function program12(depth0,data) {
  
  
  return " disabled=\"disabled\"";
  }

function program14(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

  buffer += "<button class=\"button";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(8, program8, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.active), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.active), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(10, program10, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hidden), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hidden), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(12, program12, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data}
  if (helper = helpers.text) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.text); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.text) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</button>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Button
 * @xtype button
 * @extends Roc.views.Button
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = new Roc.views.Button({
 *          config: {
 *              ui: 'calm',
 *              icon: 'loop',
 *              iconPos: 'right',
 *              text: 'Reload'
 *          },
 *          handler: function() {
 *              // form.submit();
 *          }
 *      });
 *
 * ### Different buttons
 *
 * 		@example ionic_page
 *      var example = [{
 *      	xtype: 'button',
 *      	config: {
 *      		ui: ['block', 'balanced'],
 *      		text: 'Block button'
 *      	}
 *      }, {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: ['small', 'stable'],
 *      		icon: 'ios7-gear-outline'
 *      	}
 *      }, '&nbsp;&nbsp;', {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: 'assertive',
 *      		active: true,
 *      		icon: 'ios7-cart',
 *      		text: 'Buy it'
 *      	}
 *      }, '&nbsp;&nbsp;', {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: ['outline', 'large'],
 *      		text: 'Outline & large button'
 *      	}
 *      }, '&nbsp;&nbsp;', {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: 'clear',
 *      		text: 'Clear button'
 *      	}
 *      }];
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String|Array} [config.ui] UI theme:
 * <ul>
 *  <li>light</li>
 *  <li>stable</li>
 *  <li>positive</li>
 *  <li>calm</li>
 *  <li>balanced</li>
 *  <li>energized</li>
 *  <li>assertive</li>
 *  <li>royal</li>
 *  <li>dark</li>
 * </ul>
 * But also:
 * <ul>
 *  <li>small (for small size button)</li>
 *  <li>large (for large size button)</li>
 *  <li>outline</li>
 *  <li>clear</li>
 * </ul>
 * @cfg {String} [config.icon] Icon name (without ion-) (from http://ionicons.com)
 * @cfg {String} [config.iconPos="left"] Icon position:
 * <ul>
 *  <li>left</li>
 *  <li>right</li>
 * </ul>
 * @cfg {Boolean} [config.active=false] Set button as active
 * @cfg {Boolean} [config.hidden=false] Hide the button
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {String} [config.text] Button text
 */
/**
 * Container component
 * @class Roc.views.Container
 * @extends Roc.views.Template
 * @xtype container
 */
Roc.views.Container = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Container',
        xtype: 'container'

    });
})();

Handlebars.templates.container = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, self=this, functionType="function", blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  
  return " hidden";
  }

  buffer += "<div class=\"container";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.hidden) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.hidden); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Container
 * @xtype container
 * @extends Roc.views.Container
 *
 * ### Example
 *
 *      var container = new Roc.views.Container({
 *          config: {
 *          	style: 'background-color: black;'
 *          },
 *          items: '<p>Hello</p>'
 *      });
 *
 * 		// should better use this way when just for template:
 * 		items: [{
 * 			xtpl: 'container'
 * 		}]
 * 		// or
 * 		var container = new Roc.views.Template({
 * 			xtpl: 'container',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: '...'
 * 		});
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 */
Handlebars.templates.content = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  return " has-header";
  }

function program3(depth0,data) {
  
  
  return " has-subheader";
  }

function program5(depth0,data) {
  
  
  return " has-footer";
  }

function program7(depth0,data) {
  
  
  return " padding";
  }

function program9(depth0,data) {
  
  
  return " hidden";
  }

  buffer += "<div class=\"content";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hasHeader), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hasHeader), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(3, program3, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hasSubHeader), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hasSubHeader), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(5, program5, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hasFooter), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hasFooter), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(7, program7, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.padding), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.padding), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(9, program9, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hidden), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hidden), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Content
 * @extends Roc.views.Template
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = {
 *      	xtpl: 'content',
 *          config: {
 *          	style: 'background-color: black;'
 *          },
 *          items: '<p style="color: white;">Hello</p>'
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 * @cfg {Boolean} [config.padding=true] Add padding to content.
 * @cfg {Boolean} [config.hasHeader=false] Set to true if page has header.
 * @cfg {Boolean} [config.hasFooter=false] Set to true if page has footer.
 * @cfg {Boolean} [config.hasSubHeader=false] Set to true if page has sub-header.
 */

/**
 * @class Roc.views.Footer
 * @xtype footer
 * @extends Roc.views.Template
 */
Roc.views.Footer = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Footer',
        xtype: 'footer'

    });
})();

Handlebars.templates.footer = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " bar-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<h1 class=\"title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h1>";
  return buffer;
  }

  buffer += "<div class=\"bar bar-footer";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.title) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Footer
 * @xtype footer
 * @extends Roc.views.Footer
 *
 * ### Example
 *
 * 		@example ionic_container
 *      var example = {
 *      	xtype: 'footer',
 *          config: {
 *          	ui: 'positive',
 *          	title: 'Hello'
 *          }
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.ui] Bar UI theme:
 * <ul>
 *  <li>positive</li>
 *  <li>calm</li>
 *  <li>balanced</li>
 *  <li>energized</li>
 *  <li>assertive</li>
 *  <li>royal</li>
 *  <li>dark</li>
 * </ul>
 * @cfg {String} [config.title] Bar title.
 */

Handlebars.templates.footertitle = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<h1";
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"title";
  stack1 = self.invokePartial(partials.css_class, 'css_class', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h1>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.FooterTitle
 * @extends Roc.views.Template
 *
 * Footer title template.
 * Used throught `xtpl`.
 *
 * ### Example
 *
 *      var component = new Roc.views.Footer({
 *          items: [{
 *          	xtype: 'button',
 *          	config: {
 *          		text: 'Just a button'
 *          	}
 *          }, {
 *          	xtpl: 'footertitle',
 *          	items: 'My title or other components'
 *          }]
 *      });
 *
 * 		// or
 * 		items: [{
 * 			xtype: 'footer',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: [{
 * 				xtpl: 'footertitle',
 *     			items: [{
 *     				xtpl: 'button',
 *     				config: {
 *     					text: 'Title is a button'
 *     				}
 *     			}]
 * 			}]
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

/**
 * @class Roc.views.Header
 * @xtype header
 * @extends Roc.views.Template
 */
Roc.views.Header = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Header',
        xtype: 'header'

    });
})();

Handlebars.templates.header = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " bar-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<h1 class=\"title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h1>";
  return buffer;
  }

  buffer += "<div class=\"bar bar-header";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.title) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Header
 * @xtype header
 * @extends Roc.views.Header
 *
 * ### Example
 *
 *      var component = new Roc.views.Header({
 *          config: {
 *          	style: 'background-color: black;'
 *          },
 *          items: '<p>Hello</p>'
 *      });
 *
 * 		// or
 * 		items: [{
 * 			xtype: 'header',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: [
 * 				// ...
 * 			]
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.ui] Bar UI theme:
 * <ul>
 *  <li>positive</li>
 *  <li>calm</li>
 *  <li>balanced</li>
 *  <li>energized</li>
 *  <li>assertive</li>
 *  <li>royal</li>
 *  <li>dark</li>
 * </ul>
 * @cfg {String} [config.title] Bar title.
 */

Handlebars.templates.headertitle = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<h1";
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"title";
  stack1 = self.invokePartial(partials.css_class, 'css_class', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</h1>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.HeaderTitle
 * @extends Roc.views.Template
 *
 * Header title template.
 * Used throught `xtpl`.
 *
 * ### Example
 *
 *      var component = new Roc.views.Header({
 *          items: [{
 *          	xtype: 'button',
 *          	config: {
 *          		text: 'Just a button'
 *          	}
 *          }, {
 *          	xtpl: 'headertitle',
 *          	items: 'My title or other components'
 *          }]
 *      });
 *
 * 		// or
 * 		items: [{
 * 			xtype: 'header',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: [{
 * 				xtpl: 'headertitle',
 *     			items: [{
 *     				xtpl: 'button',
 *     				config: {
 *     					text: 'Title is a button'
 *     				}
 *     			}]
 * 			}]
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

/**
 * @class Roc.views.List
 * @xtype list
 * @extends Roc.views.Template
 *
 * List component.
 * Can be use with or without store.
 *
 * This class implements {@link Roc.implements.events events}.
 *
 * ### Most simple example
 *
 *      @example ionic_page
 *      var example = new Roc.views.List({
 *          items: [{
 *              label: 'One'
 *          }, {
 *              label: 'Two'
 *          }, {
 *              label: 'Three'
 *          }]
 *      });
 *
 * ### With listitemfull itemTpl
 *
 *      @example ionic_page
 *      var example = new Roc.views.List({
 *          itemTpl: 'listitemfull',
 *          items: [{
 *              label: 'Zero',
 *              type: 'divider'
 *          }, {
 *              label: 'One',
 *              icon: 'gear-a',
 *              iconPos: 'right'
 *          }, {
 *              label: 'Two',
 *              type: 'divider'
 *          }, {
 *              label: 'Three',
 *              bubble: '1'
 *          }, {
 *              label: 'Four',
 *              icon: 'chatbubble-working'
 *          }, {
 *              label: 'Five',
 *              note: 'This is a note'
 *          }, {
 *              label: 'Six',
 *              arrow: false,
 *              button: {
 *                  position: 'left',
 *                  ui: 'positive',
 *                  icon: 'ios7-telephone',
 *                  text: 'Salut'
 *              }
 *          }]
 *      });
 *
 * ### With store
 *
 *      var store = new Roc.data.Store({
 *          enableHashTable: true,
 *          proxy: {
 *              type: 'jsonp',
 *              cache: false,
 *              url: 'http://...'
 *          }
 *      });
 *      var example = new Roc.views.List({
 *          itemTpl: 'test',
 *          store: store
 *      });
 *
 * ### Example of custom itemTpl handlebars template
 *
    {{#foreach items}}

        <a class="item list-item">
            {{#label}}{{.}}{{/label}}
        </a>

    {{/foreach}}
 *
 */
Roc.views.List = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _handlebars = Handlebars,
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    /**
     * @ignore
     * Meilleur moyen pour loader les images:
     *
     * SetInterval(50, loadImagesAfterImageLoad);
     * On detecte quand le user arrete de scroller. La on
    */

    function _afterrender(self) {

        var id = self.config.id;

        self.loadingEl = document.getElementById(id + '-loader');

        if (self.hasListener('itemselect') || self.has('itemselect')) {
            self.events.add('itemselect', {
                xtype: 'events.click',
                autoAttach: false,
                el: self.el,
                handler: function(e) {
                    // handler is called twice :( but registered just once
                    // e.stopImmediatePropagation();
                    // e.stopPropagation();
                    // e.preventDefault();
                    self.fire('itemselect', self, e);
                }
            });
        }

        if (self.autoload) {
            self.load();
        }
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.List',
        xtype: 'list',
        implements: ['events'],
        hasListeners: ['itemselect', 'updated'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        defaultAutoload: true,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            if (!self.store) {
                /**
                 * @cfg {Object} [store=undefined] Store instance to load data through.
                 */
                self.store = opts.store;
            }

            /**
             * @cfg {Boolean} [autoload=true] Automaticaly load list after render.
             */
            self.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : self.defaultAutoload);
            if (opts.config) {
                opts.config.isLoading = self.autoload;
            }

            /**
             * @cfg {String} [emptyTpl=false] Empty data HTML markup. This will replace the list content in case data is empty (on load or after).
             */
            self.emptyTpl = opts.emptyTpl || false;

            _parent.prototype.constructor.call(self, opts);

            if (!self.itemTpl) {
                /**
                 * @cfg {Mixed} [itemTpl="itembasic"] List item template name or function directly. You can see availables `itemTpl` in `Roc.*.ui.ListItem*`.
                 */
                self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            }

            if (typeof self.itemTpl === 'string') {

                var engineItemTpl = self.engine.getTpl(self.itemTpl);

                if (typeof engineItemTpl !== 'undefined') {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        /**
         * Retreive item HTMLElement from given event.
         * @param  {Event} evt Click or Touch event.
         * @return {HTMLElement} Item element if found.
         */
        getItemFromEvent: function(evt) {
            return _selector.findParentByClass(evt.target, 'list-item');
        },

        /**
         * Empty the list and then reload it.
         * If store is bind to list, use store and not given items.
         * If no items given, then just empty the list.
         * If list is already loading, then just return false.
         * @param  {Array} [items] List items.
         */
        reload: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            self.el.innerHTML = '';

            /*while (self.el.firstChild) {
                self.el.removeChild(self.el.firstChild);
            }*/

            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },

        /**
         * Show loading.
         */
        showLoading: function() {

            var self = this;

            self.isLoading = true;
            _selector.removeClass(self.loadingEl, 'hidden');
        },

        /**
         * Hide loading.
         */
        hideLoading: function() {

            var self = this;

            self.isLoading = false;
            _selector.addClass(self.loadingEl, 'hidden');
        },

        /**
         * Load list with given items.
         * If store is bind to the list, then just use store instead of items.
         * @param  {Array} [items] List items to load.
         */
        load: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },

        /**
         * Append given items to list.
         * @param  {Array} items Items to append.
         */
        loadItems: function(items) {

            var self = this,
                itemTpl = self.itemTpl,
                config = _ns.utils.applyAuto(self.config, {});

            config.items = items;

            _handlebars.currentParent.push(self);
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            _handlebars.currentParent.pop();

            if (self.abstractRenderer) {
                self.abstractRenderer.fireScope('afterrender', self.renderer);
            }

            self.hideLoading();
        },

        /**
         * Callback called by store after it has been loaded.
         * @param  {Boolean} success    Success flag.
         * @param  {Object} store Store.
         * @param  {Array} newRecords Records
         */
        storeLoaded: function(success, store, newRecords) {

            var self = this;

            self.loadItems(newRecords);

            if (!store.totalRecords && self.emptyTpl) {
                _selector.updateHtml(self.el, self.emptyTpl);
            }
            self.fire('updated', self, success, store, newRecords);
        }
    });
})();

Handlebars.templates.list = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-loader\"";
  return buffer;
  }

function program3(depth0,data) {
  
  
  return " list-loader-hide";
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += " list-loader-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  buffer += "<div class=\"list";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></div><div ";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"list-loader";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (false),
    'defaults': (true)
  },inverse:self.noop,fn:self.program(3, program3, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.isLoading), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.isLoading), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><a class=\"list-loader-text\">Loading...</a></div>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.List
 * @xtype list
 * @extends Roc.views.List
 *
 * ### Example
 *
 * 		items: [{
 * 			xtpl: 'list'
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 */
/**
 * @class Roc.views.Listbuffered
 * @xtype listbuffered
 * @extends Roc.views.Template
 *
 * List buffered component.
 * Must be use with PagingStore instead of Store.
 *
 * This class implements {@link Roc.implements.events events}.
 *
 * ### Example
 *
 *      var store = new _ns.data.Pagingstore({
            enableHashTable: true,
            recordPerPage: 10,
            proxy: {
                type: 'jsonp',
                cache: false,
                url: 'http://...'
            }
        });

        var example = {
            xtype: 'listbuffered',
            xtpl: 'tickets-list',
            itemTpl: 'user-orderitem',
            emptyTpl: [
                '<div class="empty-list">',
                    '<i class="icon ion-pricetags"></i>',
                    '<span class="message">',
                        t('No orders.'),
                    '</span>',
                '</div>'
            ].join(''),
            ref: 'list',
            refScope: this,
            store: store
        };
 */
Roc.views.Listbuffered = (function() {

    'use strict';

    // private
    var _window = window,
        _ns = Roc,
        _events = _ns.events,
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    /**
    Meilleur moyen pour loader les images:

    SetInterval(50, loadImagesAfterImageLoad);
    On detecte quand le user arrete de scroller. La on
    */

    function _afterrender(self) {

        self.loadingEl = document.getElementById(self.config.id + '-loader');

        if (self.hasListener('itemselect') || self.has('itemselect')) {
            self.setupListeners(['itemselect'], self);
            self.events.add('itemselect', {
                xtype: 'events.click',
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function(e) {
                    self.fire('itemselect', self, e);
                }
            });
        }

        if (self.disableScrollEvent !== true) {
            self.events.add('scroll', {
                xtype: 'events.scroll',
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: self.scrollHandler
            });
        }

        self.loadNext();

    }

    // public
    return _parent.subclass({

        // What range of pixel to load before scrolling hit the reload point
        deltaPixels: 100,
        className: 'Roc.views.Listbuffered',
        xtype: 'listbuffered',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            /**
             * @cfg { Roc.data.PagingStore } store Store.
             */
            self.store = opts.store;
            /**
             * @cfg {String} [emptyTpl=false] Empty data HTML markup. This will replace the list content in case data is empty (on load or after).
             */
            self.emptyTpl = opts.emptyTpl || false;
            /**
             * @property {Boolean} [isLoading=false] True if list is loading, else false.
             */
            self.isLoading = false;
            /**
             * @cfg {Boolean} [disableScrollEvent=false] True to disable default scroll event, else false.
             */
            self.disableScrollEvent = opts.disableScrollEvent || false;

            /**
             * @cfg {Boolean} [loading=true] Loading indicator.
             */
            if (typeof opts.loading === 'boolean') {
                self.loading = opts.loading;
            } else {
                self.loading = true;
            }

            /**
             * @cfg {Boolean} [emptyBeforeLoad=true] Empty the list before store load.
             * Only occurs on store load, not on store next page.
             */
            if (typeof opts.emptyBeforeLoad === 'boolean') {
                self.emptyBeforeLoad = opts.emptyBeforeLoad;
            } else {
                self.emptyBeforeLoad = true;
            }

            _parent.prototype.constructor.call(self, opts);

            self.setupListeners(['showloading', 'hideloading', 'afterload']);

            if (!self.itemTpl) {
                /**
                 * @cfg {Mixed} [itemTpl="itembasic"] List item template name or function directly. You can see availables `itemTpl` in `Roc.*.ui.ListItem*`.
                 */
                self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            }

            if (typeof self.itemTpl === 'string') {

                var engineItemTpl = self.engine.getTpl(self.itemTpl);

                if (typeof engineItemTpl !== 'undefined') {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        /**
         * Retreive item HTMLElement from given event.
         * @param  {Event} evt Click or Touch event.
         * @return {HTMLElement} Item element if found.
         */
        getItemFromEvent: function(evt) {
            return $.findParentByClass(evt.target, 'list-item');
        },

        scrollHandler: function() {

            var p = document.body,
                //scrollTop = p.scrollTop,
                screenHeight = _window.screen.availHeight,
                scrollHeight = p.scrollHeight - this.deltaPixels,
                totalScroll = p.scrollTop + screenHeight;

            if (totalScroll >= scrollHeight) {
                this.loadNext();
            }
        },

        /**
         * Empty the list and then reload it.
         * If list is already loading, then just return false.
         */
        reload: function() {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            self.isLoading = true;
            if (self.emptyBeforeLoad === true) {
                self.el.innerHTML = '';
            } else {
                self.needEmpty = true;
            }
            if (self.loading === true) {
                self.showLoading();
            } else {
                self.hideLoading();
            }
            self.store.reset();
            self.store.nextPage({
                callback: self.getNext.bind(self)
            });
        },

        /**
         * Load next items from paging store.
         * If store already loading, returns false.
         */
        loadNext: function() {

            var self = this;

            if (self.isLoading === true) {
                return false;
            } else if (self.store.loadedRecords &&
                self.store.loadedRecords === self.store.totalRecords) {
                self.hideLoading();
                return false;
            }
            self.isLoading = true;
            self.showLoading();
            self.store.nextPage({
                callback: self.getNext.bind(self)
            });
        },

        /**
         * @private
         * Store nextPage callback.
         * @param  {Boolean} success Success flag
         * @param  { Roc.data.PagingStore } store Paging store
         * @param  {Array} newRecords New records
         * @fires afterload
         */
        getNext: function(success, store, newRecords) {

            var conf,
                self = this,
                itemTpl = this.itemTpl || _itemTpl;

            if (self.needEmpty === true) {
                self.el.innerHTML = '';
                delete self.needEmpty;
            }
            conf = self.config;
            conf.items = newRecords;
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(conf));/*{
                items: newRecords
            }));*/
            // <forcing redraw> BUG on iOS 6.1.3 Safari mobile
            // when reloading list from 4 elems to 1 elem.
            // The 1 elem not shown if don't force redraw
            $.redraw(self.el);
            // </forcing redraw>
            if (store.loadedRecords === store.totalRecords) {
                self.hideLoading();
            } else if (store.totalRecords && self.loading === false) {
                self.showLoading();
            }

            self.isLoading = false;

            if (!store.totalRecords && self.emptyTpl) {
                $.updateHtml(self.el, self.emptyTpl);
            }

            self.fireScope('afterload', self);
        },

        /**
         * Show loading
         * @fires showloading
         */
        showLoading: function() {

            var self = this;

            $.removeClass(self.loadingEl, 'list-loader-hide');
            self.fireScope('showloading', self);
        },

        /**
         * Hide loading
         * @fires hideloading
         */
        hideLoading: function() {

            var self = this;

            $.addClass(this.loadingEl, 'list-loader-hide');
            self.fireScope('hideloading', self);
        }
    });
})();

/**
 * @class Roc.views.Tabbutton
 * @xtype tabbutton
 * @extends Roc.views.Button
 *
 * Tab button component.
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */
Roc.views.Tabbutton = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Button;

    // public
    return _parent.subclass({

        className: 'Roc.views.Tabbutton',
        xtype: 'tabbutton',

        /**
         * Click event handler.
         * Can be override.
         */
        handler: _parent.prototype.handler,

        /**
         * Active this tab button.
         * @fires activate
         */
        activate: function() {
            this.active = true;
            this.fire('activate', this);
        },

        /**
         * Deactivate this tab button.
         * @fires deactivate
         */
        deactivate: function() {
            this.active = false;
            this.fire('deactivate', this);
        }

    });
})();

Handlebars.templates.tabbutton = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  
  return " active";
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += " href=\"#"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  buffer += " data-name=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"></i>";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "";
  buffer += "<span>"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }

  buffer += "<a class=\"tab-item";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(3, program3, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.active), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.active), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.route) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.route); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.route) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}
  if (helper = helpers.name) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.name) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data}
  if (helper = helpers.text) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.text); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.text) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Tabbutton
 * @xtype tabbutton
 * @extends Roc.views.Tabbuton
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    active: true,
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {Boolean} [config.active=false] True to set button as active.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 * @cfg {String} [config.name] Tab button name. Usefull for activate tab from name.
 * @cfg {String} [config.route] Tab route without the "#".
 * @cfg {String} [config.text] Tab button text.
 */
/**
 * @class Roc.views.Tabs
 * @xtype tabs
 * @extends Roc.views.Template
 *
 * Tabs component.
 *
 * This class implements {@link Roc.implements.events events}.
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */
Roc.views.Tabs = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        if (typeof self.handler === 'function') {
            self.events.add('click', {
                xtype: 'events.click',
                el: self.el,
                scope: self,
                handler: self.handler
            });
        }
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.Tabs',
        xtype: 'tabs',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            if (typeof opts.handler !== 'undefined') {
                /**
                 * @cfg {Function} [handler=this.handler] Click event handler.
                 */
                self.handler = opts.handler;
            }
            _parent.prototype.constructor.call(self, opts);
        },

        /**
         * Handler attached to click event.
         * @param  {Event} event Click event.
         * @fires select
         */
        handler: function(event) {

            var self = this;

            self.fire('select', self, event);
        },

        /**
         * Set given tabbutton name as active.
         * @param {String} name   Tab button name.
         * @param {Boolean} [silent=false] If true it will not fire activate event.
         */
        setActive: function(name, silent) {

            var self = this;

            if (silent === true) return;

            self.fire('activate', self, name);
        }

    });
})();

Handlebars.templates.tabs = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " tabs-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += " tabs-icon-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  buffer += "<div class=\"tabs";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.iconPos) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.iconPos); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.iconPos) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Tabs
 * @xtype tabs
 * @extends Roc.views.Tabs
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {String} [config.iconPos] Icon position:
 * <ul><li>left: icons on left of text.</li><li>top: icons on top of text.</li><li>only: only icons.</li></ul>
 */
/**
 * @class Roc.handlebars.helpers.key_value
 *
 * Handlebars `key_value` helper.
 *
 * ## Sample usage:
 *
 *      {{#key_value people}}
 *         Key: {{key}} {{! This is the key of current iteration }}
 *         Value: {{value}} {{! This is the value of current iteration }}
 *      {{/key_value}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 *      var config = {
 *          attrs = {
 *              required: true,
 *              disabled: false,
 *              value: 'test'
 *          }
 *      };
 *
 * #### Handlebars template
 *
 *      {{#key_value attrs}}
 *          {{key}}="{{value}}"
 *      {{/key_value}}
 *
 * #### Output
 *
 *      required="true"
 *      disabled="false"
 *      value="test"
 *
 */
Handlebars.registerHelper('key_value', function(context, options) {

    var buffer = '',
        key;

    for (key in context) {
        if (context.hasOwnProperty(key)) {
            buffer += options.fn({
                key: key,
                value: context[key]
            });
        }
    }

    return buffer;
});
(function() {

    var _handlebars = Handlebars;

    /*
     * {{!-- {{style inputStyle}} --}}
     */
    _handlebars.registerHelper('style', function(context, options) {

        if (typeof context !== 'string') {
            if (typeof this.style === 'string') {
                return new _handlebars.SafeString(' style="' + this.style + '"');
            }
            return '';
        }
        return new _handlebars.SafeString(' style="' + context + '"');
    });

}());

/**
 * @class Roc.views.Textfield
 * @xtype textfield
 * @extends Roc.views.Template
 *
 * Textfield for form.
 * Examples are done with ionic UI.
 *
 * ### Simple textfield
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'textfield',
 *          config: {
 *              label: 'Username',
 *              icon: 'ion-user',
 *              placeHolder: 'remi'
 *          }
 *      };
 *
 * ### Multiple fields type
 *
 *      @example ionic_page
 *      var example = [{
 *          xtype: 'textfield',
 *          config: {
 *              labelInline: false,
 *              required: true,
 *              label: 'Username',
 *              placeHolder: 'Your name...'
 *          }
 *      }, {
 *          xtype: 'textfield',
 *          config: {
 *              required: true,
 *              type: 'password',
 *              labelInline: false,
 *              label: 'Password',
 *              placeHolder: 'Password...'
 *          }
 *      }, {
 *          xtype: 'textfield',
 *          config: {
 *              type: 'date',
 *              labelInline: false,
 *              label: 'Birthday date',
 *              placeHolder: 'DD-MM-YYYY...'
 *          }
 *      }];
 */
Roc.views.Textfield = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Textfield',
        xtype: 'textfield',

        /**
         * Disable or enable textfield.
         * @param {Boolean} disabled Disable flag. True to disable, false to enable.
         * @fires disabled
         */
        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        }
    });
})();

Handlebars.templates.textfield = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " item-stacked-label";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"input-label\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + " icon-big\" style=\"margin: 0px 10px;\"></i>";
  return buffer;
  }

function program7(depth0,data) {
  
  var stack1, helper;
  if (helper = helpers.type) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.type); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  return escapeExpression(stack1);
  }

function program9(depth0,data) {
  
  
  return "text";
  }

function program11(depth0,data) {
  
  var buffer = "";
  buffer += " autocomplete=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "";
  buffer += " name=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program15(depth0,data) {
  
  var buffer = "";
  buffer += " pattern=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program17(depth0,data) {
  
  var buffer = "";
  buffer += " value=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = "";
  buffer += " placeholder=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program21(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  stack1 = (helper = helpers.key_value || (depth0 && depth0.key_value),options={hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.attrs), options) : helperMissing.call(depth0, "key_value", (depth0 && depth0.attrs), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program22(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " ";
  if (helper = helpers.key) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.key); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "=\"";
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program24(depth0,data) {
  
  
  return " disabled=\"disabled\"";
  }

function program26(depth0,data) {
  
  
  return " required";
  }

function program28(depth0,data) {
  
  
  return " readonly";
  }

  buffer += "<label class=\"item item-input";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (false),
    'default': (true)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.labelInline), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.labelInline), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<input type=\"";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.type), {hash:{},inverse:self.program(9, program9, data),fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options={hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data}
  if (helper = helpers.autocomplete) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.autocomplete); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.autocomplete) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}
  if (helper = helpers.name) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.name) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data}
  if (helper = helpers.pattern) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.pattern); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.pattern) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data}
  if (helper = helpers.value) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.value) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data}
  if (helper = helpers.placeHolder) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.placeHolder); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.placeHolder) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.attrs), {hash:{},inverse:self.noop,fn:self.program(21, program21, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(24, program24, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(26, program26, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.required), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.required), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(28, program28, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.readonly), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.readonly), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += escapeExpression((helper = helpers.style || (depth0 && depth0.style),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.inputStyle), options) : helperMissing.call(depth0, "style", (depth0 && depth0.inputStyle), options)));
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></label>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Textfield
 * @xtype textfield
 * @extends Roc.views.Textfield
 *
 * ### Example
 *
 *      @example ionic_page
        var example = {
            xtype: 'textfield',
            config: {
                label: 'Username',
                icon: 'ion-user',
                placeHolder: 'remi'
            }
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {String} [config.type="text"] Input type:
 *  <ul>
 *   <li>text</li>
 *   <li>password</li>
 *   <li>color</li>
 *   <li>date</li>
 *   <li>datetime</li>
 *   <li>datetime-local</li>
 *   <li>email</li>
 *   <li>month</li>
 *   <li>number</li>
 *   <li>range</li>
 *   <li>search</li>
 *   <li>tel</li>
 *   <li>time</li>
 *   <li>url</li>
 *   <li>week</li>
 *  </ul>
 * @cfg {String} [config.label] Field label.
 * @cfg {Boolean} [config.labelInline=true] Put label inline. False to disable it.
 * @cfg {Boolean} [config.disabled=false] Set field as disabled.
 * @cfg {Boolean} [config.required=false] Set field as required.
 * @cfg {Boolean} [config.readonly=false] Set field as readonly.
 * @cfg {String} [config.inputStyle] Input inline style.
 * @cfg {String} [config.autocomplete] Autocomplete attribute value.
 * @cfg {String} [config.name] Input name attribute value.
 * @cfg {String} [config.pattern] Input pattern attribute value.
 * @cfg {String} [config.value] Input value attribute value.
 * @cfg {String} [config.placeHolder] Input placeHolder attribute value.
 * @cfg {Object} [config.attrs] Input attributes object.
 * Example:
 *
 * 		attrs: {
 * 			required: "required",
 * 			onclick: "javascript:alert(1);"
 * 		}
 *
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 */
Handlebars.templates.selectitembasic = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<option value=\"";
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"";
  stack1 = (helper = helpers.if_eq || (depth1 && depth1.if_eq),options={hash:{
    'compare': ((depth1 && depth1.value))
  },inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.value), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.value), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(4, program4, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</option>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  buffer += " selected=\"selected\"";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "";
  buffer += " disabled=\"disabled\"";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "";
  buffer += escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1, helper;
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

  stack1 = helpers.each.call(depth0, (depth0 && depth0.options), {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  });
/**
 * @class Roc.views.Select
 * @xtype select
 * @extends Roc.views.Template
 *
 * Select input for form.
 * Examples are done with ionic UI.
 *
 * This class implements {@link Roc.implements.events events}.
 *
 * ### Sample usage
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'select',
 *          config: {
 *              label: 'Gender',
 *              value: 'm',
 *              options: [{
 *                  label: 'Male',
 *                  value: 'm'
 *              }, {
 *                  label: 'Female',
 *                  value: 'f'
 *              }]
 *          }
 *      };
 *
 * ### Link to Roc.data.Store
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'select',
 *          config: {
 *              label: 'Countries',
 *              name: 'country',
 *              required: true,
 *              placeHolder: 'Select your country'
 *          },
 *          store: countries_store,
 *          listeners: {
 *              change: function(select, item) {
 *                  // do something with item.value
 *              }
 *          }
 *      };
 */
Roc.views.Select = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.uiEl = document.getElementById(self.config.id + '-ui');
        self.setupListeners(['change']);
        self.events.add('change', {
            xtype: 'events.abstract',
            autoAttach: true,
            eventName: 'change',
            el: self.el,
            scope: self,
            handler: function() {

                var item;

                if (self.store) {
                    item = self.store.getAt(self.el.selectedIndex);
                } else {
                    item = self.config.options[self.el.selectedIndex];
                }
                self.fire('change', self, item);
            }
        });

        if (self.store && self.autoload) {
            self.load();
        }
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.Select',
        xtype: 'select',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        defaultAutoload: true,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            /**
             * @cfg { Roc.data.Store } [store] Data store.
             */
            self.store = opts.store;
            opts.config = opts.config || {};

            /**
             * @cfg {Boolean} [autoload=true] Autoload the data.
             */
            self.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : self.defaultAutoload);

            _parent.prototype.constructor.call(self, opts);

            if (!self.itemTpl) {
                /**
                 * @cfg {Mixed} [itemTpl="itembasic"] List item template name or function directly. You can see availables `itemTpl` in `Roc.*.ui.Selectitem*`.
                 */
                self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            }

            if (typeof self.itemTpl === 'string') {

                var engineItemTpl = self.engine.getTpl(self.itemTpl);

                if (typeof engineItemTpl !== 'undefined') {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        /**
         * Set loading to given value.
         * @param {Boolean} [loading=false] Loading flag. True is for loading, false is for hide loading.
         * @fires loading
         */
        setLoading: function(loading) {

            var self = this;

            loading = (loading === true);
            self.isLoading = loading;
            self.fire('loading', self, loading);
        },

        /**
         * Set disabled to given value.
         * @param {Boolean} disabled True to disable this component, else false.
         * @fires disabled
         */
        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        },

        /**
         * Empty the select items and reload data from reloading store, or given items.
         * @param  {Array} [items] Items for itemTpl.
         */
        reload: function(items) {

            var self = this;

            while (self.el.firstChild) {
                self.el.removeChild(self.el.firstChild);
            }
            if (self.store) {
                self.setLoading(true);
                self.store.reset();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.config.options);
            }
        },

        /**
         * Clear selected item.
         * @fires change
         */
        clearSelection: function() {

            var self = this;

            if (self.el) {

                var emptyItem = {};

                if (self.config.emptyText) {
                    emptyItem.label = self.config.emptyText;
                }
                self.el.selectedIndex = 0;
                self.fire('change', self, emptyItem);
            }
        },

        /**
         * Load store or given items.
         * It will append to existing items.
         * @param  {Array} [items] Items to load.
         */
        load: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            if (self.store) {
                self.setLoading(true);
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.config.options);
            }
        },

        /**
         * Load given items to select.
         * @param  {Array} items Items to load.
         */
        loadItems: function(items) {

            var self = this,
                itemTpl = self.itemTpl,
                config = _ns.utils.applyAuto(self.config, {});

            config.options = items;
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            self.setLoading(false);
        },

        /**
         * @private
         * Callback after store has been loaded.
         */
        storeLoaded: function() {

            var self = this;

            if (self.config.emptyText) {
                self.store.addRecord({
                    label: self.config.emptyText
                }, true);
            }
            self.loadItems(self.store.records);
        }
    });
})();

Handlebars.templates.select = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " item-stacked-label";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"input-label\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + " icon-big\" style=\"margin: 0px 10px;\"></i>";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  buffer += " autocomplete=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "";
  buffer += " name=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "";
  buffer += " pattern=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "";
  buffer += " placeholder=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program15(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  stack1 = (helper = helpers.key_value || (depth0 && depth0.key_value),options={hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.attrs), options) : helperMissing.call(depth0, "key_value", (depth0 && depth0.attrs), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program16(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " ";
  if (helper = helpers.key) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.key); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "=\"";
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"";
  return buffer;
  }

function program18(depth0,data) {
  
  
  return " disabled=\"disabled\"";
  }

function program20(depth0,data) {
  
  
  return " required";
  }

function program22(depth0,data) {
  
  
  return " readonly";
  }

function program24(depth0,data,depth1) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<option value=\"";
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"";
  stack1 = (helper = helpers.if_eq || (depth1 && depth1.if_eq),options={hash:{
    'compare': ((depth1 && depth1.value))
  },inverse:self.noop,fn:self.program(25, program25, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.value), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.value), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(18, program18, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.program(29, program29, data),fn:self.program(27, program27, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.program(29, program29, data),fn:self.program(27, program27, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</option>";
  return buffer;
  }
function program25(depth0,data) {
  
  var buffer = "";
  buffer += " selected=\"selected\"";
  return buffer;
  }

function program27(depth0,data) {
  
  var buffer = "";
  buffer += escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program29(depth0,data) {
  
  var buffer = "", stack1, helper;
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

  buffer += "<label class=\"item item-input item-select";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (false),
    'default': (true)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.labelInline), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.labelInline), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<select ";
  options={hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}
  if (helper = helpers.autocomplete) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.autocomplete); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.autocomplete) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data}
  if (helper = helpers.name) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.name) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data}
  if (helper = helpers.pattern) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.pattern); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.pattern) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}
  if (helper = helpers.placeHolder) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.placeHolder); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.placeHolder) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.attrs), {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(18, program18, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(20, program20, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.required), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.required), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(22, program22, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.readonly), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.readonly), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += escapeExpression((helper = helpers.style || (depth0 && depth0.style),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.inputStyle), options) : helperMissing.call(depth0, "style", (depth0 && depth0.inputStyle), options)));
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.options), {hash:{},inverse:self.noop,fn:self.programWithDepth(24, program24, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</select></label>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Select
 * @xtype select
 * @extends Roc.views.Select
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'select',
 *          config: {
 *              label: 'Gender',
 *              value: 'm',
 *              options: [{
 *                  label: 'Male',
 *                  value: 'm'
 *              }, {
 *                  label: 'Female',
 *                  value: 'f'
 *              }]
 *          }
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {String} [config.label] Field label.
 * @cfg {Boolean} [config.labelInline=true] Put label inline. False to disable it.
 * @cfg {Boolean} [config.disabled=false] Set field as disabled.
 * @cfg {Boolean} [config.required=false] Set field as required.
 * @cfg {Boolean} [config.readonly=false] Set field as readonly.
 * @cfg {String} [config.inputStyle] Textarea inline style.
 * @cfg {String} [config.autocomplete] Autocomplete attribute value.
 * @cfg {String} [config.name] Textarea name attribute value.
 * @cfg {String} [config.pattern] Textarea pattern attribute value.
 * @cfg {String} [config.value] Textarea value.
 * @cfg {String} [config.placeHolder] Textarea placeHolder attribute value.
 * @cfg { Roc.data.Store } [config.store] Data store.
 * @cfg {Object} [config.attrs] Textarea attributes object.
 * Example:
 *
 * 		attrs: {
 * 			required: "required",
 * 			onclick: "javascript:alert(1);"
 * 		}
 *
 * @cfg {Array} [config.options] Options for select. Example:
 *
 *      [{
 *          label: 'Option one',
 *          value: 1
 *      }, {
 *          label: 'Option two',
 *          value: 2
 *      }, ...]
 *
 * or only values (in this case, label will be the value)
 *
 *      [1, 2]
 *
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 */
Handlebars.templates.searchfield = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, self=this, helperMissing=helpers.helperMissing, functionType="function", escapeExpression=this.escapeExpression, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += "item-input-wrapper";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "item item-input";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (false),
    'default': (true)
  },inverse:self.noop,fn:self.program(4, program4, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.labelInline), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.labelInline), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "";
  buffer += " item-stacked-label";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"input-label\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + " placeholder-icon\"></i>";
  return buffer;
  }

function program10(depth0,data) {
  
  var stack1, helper;
  if (helper = helpers.type) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.type); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  return escapeExpression(stack1);
  }

function program12(depth0,data) {
  
  
  return "text";
  }

function program14(depth0,data) {
  
  var buffer = "";
  buffer += " autocomplete=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program16(depth0,data) {
  
  var buffer = "";
  buffer += " name=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program18(depth0,data) {
  
  var buffer = "";
  buffer += " pattern=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program20(depth0,data) {
  
  var buffer = "";
  buffer += " value=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program22(depth0,data) {
  
  var buffer = "";
  buffer += " placeholder=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program24(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  stack1 = (helper = helpers.key_value || (depth0 && depth0.key_value),options={hash:{},inverse:self.noop,fn:self.program(25, program25, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.attrs), options) : helperMissing.call(depth0, "key_value", (depth0 && depth0.attrs), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program25(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " ";
  if (helper = helpers.key) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.key); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "=";
  if (helper = helpers.value) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

function program27(depth0,data) {
  
  
  return " disabled=\"disabled\"";
  }

function program29(depth0,data) {
  
  
  return " required";
  }

function program31(depth0,data) {
  
  
  return " readonly";
  }

  buffer += "<label class=\"";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.wrapper), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.wrapper), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<input type=\"";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.type), {hash:{},inverse:self.program(12, program12, data),fn:self.program(10, program10, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options={hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data}
  if (helper = helpers.autocomplete) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.autocomplete); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.autocomplete) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data}
  if (helper = helpers.name) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.name) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data}
  if (helper = helpers.pattern) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.pattern); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.pattern) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(18, program18, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data}
  if (helper = helpers.value) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.value); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.value) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data}
  if (helper = helpers.placeHolder) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.placeHolder); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.placeHolder) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.attrs), {hash:{},inverse:self.noop,fn:self.program(24, program24, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(27, program27, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.disabled), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.disabled), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(29, program29, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.required), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.required), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(31, program31, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.readonly), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.readonly), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += escapeExpression((helper = helpers.style || (depth0 && depth0.style),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.inputStyle), options) : helperMissing.call(depth0, "style", (depth0 && depth0.inputStyle), options)));
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></label>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Searchfield
 * @xtype textfield
 * @extends Roc.views.Textfield
 *
 * ### Sample usage
 *
 *      @example ionic_page
        var example = ['<div class="list list-inset">', {
            xtype: 'textfield',
            xtpl: 'searchfield',
            config: {
            	icon: 'ion-search',
                placeHolder: 'Search something...'
            }
        }, '</div>'];

 * ### Inside header
 *
 * 		@example ionic_container
 *      var example = [{
 *      	xtype: 'header',
 *      	config: {
 *      		cssClass: 'item-input-inset'
 *      	},
 *      	items: [{
 *      		xtype: 'textfield',
 *      		xtpl: 'searchfield',
 *      		config: {
 *      			wrapper: true,
 *      			icon: 'ion-ios7-search',
 *      			placeHolder: 'Search...'
 *      		}
 *      	}, {
 *      		xtype: 'button',
 *        		config: {
 *        			text: 'Cancel',
 *        			ui: 'clear'
 *        		}
 *      	}]
 *      }];
 *
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {String} [config.type="text"] Input type:
 *  <ul>
 *   <li>text</li>
 *   <li>password</li>
 *   <li>color</li>
 *   <li>date</li>
 *   <li>datetime</li>
 *   <li>datetime-local</li>
 *   <li>email</li>
 *   <li>month</li>
 *   <li>number</li>
 *   <li>range</li>
 *   <li>search</li>
 *   <li>tel</li>
 *   <li>time</li>
 *   <li>url</li>
 *   <li>week</li>
 *  </ul>
 * @cfg {String} [config.label] Field label.
 * @cfg {Boolean} [config.labelInline=true] Put label inline. False to disable it. Only works with `wrapper` to false (default).
 * @cfg {Boolean} [config.wrapper=false] Set to true when you want to wrap the input inside bar.
 * @cfg {Boolean} [config.disabled=false] Set field as disabled.
 * @cfg {Boolean} [config.required=false] Set field as required.
 * @cfg {Boolean} [config.readonly=false] Set field as readonly.
 * @cfg {String} [config.inputStyle] Input inline style.
 * @cfg {String} [config.autocomplete] Autocomplete attribute value.
 * @cfg {String} [config.name] Input name attribute value.
 * @cfg {String} [config.pattern] Input pattern attribute value.
 * @cfg {String} [config.value] Input value attribute value.
 * @cfg {String} [config.placeHolder] Input placeHolder attribute value.
 * @cfg {Object} [config.attrs] Input attributes object.
 * Example:
 *
 * 		attrs: {
 * 			required: "required",
 * 			onclick: "javascript:alert(1);"
 * 		}
 *
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 */
Handlebars.templates.card = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  
  return " hidden";
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += " card-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += "<div class=\"item item-divider\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</div>";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  stack1 = (helper = helpers.config || (depth0 && depth0.config),options={hash:{},inverse:self.programWithDepth(13, program13, data, depth0),fn:self.programWithDepth(8, program8, data, depth0),data:data},helper ? helper.call(depth0, (depth0 && depth0.$index), options) : helperMissing.call(depth0, "config", (depth0 && depth0.$index), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program8(depth0,data,depth1) {
  
  var buffer = "", stack1, helper, options;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.title), {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<div class=\"item item-text-wrap";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (false),
    'default': (true)
  },inverse:self.noop,fn:self.program(11, program11, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.padding), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.padding), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">"
    + escapeExpression((helper = helpers.safe || (depth1 && depth1.safe),options={hash:{},data:data},helper ? helper.call(depth0, (depth1 && depth1.original), options) : helperMissing.call(depth0, "safe", (depth1 && depth1.original), options)))
    + "</div>";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += "<div class=\"item item-divider\">";
  if (helper = helpers.title) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</div>";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "";
  buffer += " item-nopadding";
  return buffer;
  }

function program13(depth0,data,depth1) {
  
  var buffer = "", helper, options;
  buffer += "<div class=\"item item-text-wrap\">"
    + escapeExpression((helper = helpers.safe || (depth1 && depth1.safe),options={hash:{},data:data},helper ? helper.call(depth0, (depth1 && depth1.original), options) : helperMissing.call(depth0, "safe", (depth1 && depth1.original), options)))
    + "</div>";
  return buffer;
  }

  buffer += "<div class=\"card";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.hidden) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.hidden); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.header) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.header); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.header) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.footer) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.footer); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.footer) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Card
 * @extends Roc.views.Template
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = {
 *          xtpl: 'card',
 *          config: {
 *          	header: 'Header',
 *          	footer: 'Footer'
 *          },
 *          items: [{
 *          	xtpl: 'container',
 *          	config: {
 *          		title: 'Card 1'
 *          	},
 *          	items: '<p>Card 1 content</p>'
 *          }, {
 *          	xtpl: 'container',
 *          	config: {
 *          		title: 'Card 2'
 *          	},
 *          	items: '<p>Card 2 content</p>'
 *          }]
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {String} [config.header] Card header.
 * @cfg {String} [config.footer] Card footer.
 */

/**
 * @class Roc.views.Loadmask
 * @xtype loadmask
 * @extends Roc.views.Template
 *
 * Loadmask component.
 *
 * This class implements {@link Roc.implements.events events}.
 *
 * ### Sample usage
 *
 *      @example
 *      var loader = new Roc.views.Loadmask({
 *          lock: true
 *      });
 *      loader.compile();
 *      loader.render('body');
 *
 */
Roc.views.Loadmask = (function() {

    'use strict';

    var _ns = Roc,
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        if (self.lock === true) {
            self.events.add('lock-screen', {
                xtype: 'events.abstract',
                eventName: 'touchstart',
                el: window,
                autoAttach: true,
                handler: function(evt) {
                    evt.stopPropagation();
                    evt.preventDefault();
                    return false;
                }
            });
        }
    }

    return _parent.subclass({

        className: 'Roc.views.Loadmask',
        xtype: 'loadmask',
        implements: ['events'],
        hasListeners: ['afterrender'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            if (opts.lock === true) {
                /**
                 * @cfg {Boolean} [lock=false] True to lock click/touch events while loading is active.
                 */
                self.lock = true;
                self.bodyEl = document.body;
            }

            _parent.prototype.constructor.call(self, opts);
        },

        /**
         * Show the loader.
         * @fires show
         */
        show: function() {

            var self = this;

            if (self.lock) {
                self.bodyEl.style.pointerEvents = 'none';
            }

            $.removeClass(self.el, 'hidden');
            self.fireScope('show', self);
        },

        /**
         * Hide the loader.
         * @fires hide
         */
        hide: function() {

            var self = this;

            if (self.lock) {
                self.bodyEl.style.pointerEvents = '';
            }

            $.addClass(this.el, 'hidden');
            self.fireScope('hide', self);
        }
    });

}());

Handlebars.templates.loadmask = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<div class=\"loadmask";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "><i class=\"ion-ios7-reloading\"></i>";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Loadmask
 * @extends Roc.views.Loadmask
 *
 * Loadmask component.
 *
 * This class implements {@link Roc.implements.events events}.
 *
 * ### Sample usage
 *
 *      @example
 *      var loader = new Roc.views.Loadmask({
 *          lock: true
 *      });
 *      loader.compile();
 *      loader.render('body');
 *
 */

/**
 * @class Roc.views.Page
 * @xtype page
 * @extends Roc.views.Template
 *
 * ### Example
 *
 *		@example ionic_container
 *      var example = {
 *      	xtype: 'page',
 *          items: [{
 *          	xtype: 'header',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Header title'
 *          	}
 *          }, {
 *          	xtype: 'footer',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Footer title'
 *          	}
 *          }]
 *      };
 */
Roc.views.Page = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Page',
        xtype: 'page'

    });
})();

Handlebars.templates.page = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<div class=\"view";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Page
 * @extends Roc.views.Page
 *
 * ### Example
 *
 * 		@example ionic_container
 *      var example = {
 *      	xtype: 'page',
 *          items: [{
 *          	xtype: 'header',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Header title'
 *          	}
 *          }, {
 *          	xtype: 'footer',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Footer title'
 *          	}
 *          }]
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

Handlebars.templates.row = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<div class=\"col";
  stack1 = (helper = helpers.config || (depth0 && depth0.config),options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.$index), options) : helperMissing.call(depth0, "config", (depth0 && depth0.$index), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  stack1 = (helper = helpers.safe || (depth0 && depth0.safe),options={hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.original), options) : helperMissing.call(depth0, "safe", (depth0 && depth0.original), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.col), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.colCssClass), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " col-";
  if (helper = helpers.col) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.col); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " ";
  if (helper = helpers.colCssClass) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.colCssClass); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  buffer += "<div class=\"row";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Row
 * @extends Roc.views.Template
 *
 * Ionic row template.
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = {
 *      	xtpl: 'row',
 *          items: [{
 *          	xtpl: 'container',
 *          	config: {
 *           		col: 75
 *          	},
 *          	items: 'This is a 75% width template'
 *          }, {
 *          	xtpl: 'container',
 *          	items: 'This is a 25% width container'
 *          }]
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

Handlebars.templates.bubble = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " badge-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  buffer += "<span class=\"badge";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  if (helper = helpers.label) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Bubble
 * @extends Roc.views.Template
 *
 * Ionic bubble template.
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = [{
 *      	xtpl: 'bubble',
 *      	config: {
 *      		ui: 'assertive',
 *      		label: '99+'
 *      	}
 *      }, '<br><br>', {
 *      	xtpl: 'bubble',
 *      	config: {
 *      		ui: 'positive',
 *      		label: 'important'
 *      	}
 *      }];
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Mixed} [config.ui] UI theme for bubble. Can be string or array of string.
 * @cfg {String} [config.label] Label for bubble.
 */

/**
 * @class Roc.handlebars.helpers.if_neq
 *
 * Handlebars `if_neq` (if not equals) helper.
 *
 * _Note_: Given `default` value is only used when `value` is undefined.
 *
 * ## Sample usage:
 *
 *      {{#if_neq value compare=true default=false}}
 *         Value is not true
 *      {{else}}
 *         Value is true
 *      {{/if_neq}}
 */
Handlebars.registerHelper('if_neq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context != options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});
/**
 * @class Roc.handlebars.helpers.if_gteq
 *
 * Handlebars `if_gteq` (if greater than or equals) helper.
 *
 * _Note_: Given `default` value is only used when `value` is undefined.
 *
 * ## Sample usage:
 *
 *      {{#if_gteq value compare=10 default=1}}
 *         Value >= 10
 *      {{else}}
 *         Value < 10
 *      {{/if_gteq}}
 */
Handlebars.registerHelper('if_gteq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context >= options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});
//
// strftime
// github.com/samsonjs/strftime
// @_sjs
//
// Copyright 2010 - 2013 Sami Samhuri <sami@samhuri.net>
//
// MIT License
// http://sjs.mit-license.org
//

;(function() {

  //// Where to export the API
  var namespace;

  // CommonJS / Node module
  if (typeof module !== 'undefined') {
    namespace = module.exports = strftime;
  }

  // Browsers and other environments
  else {
    // Get the global object. Works in ES3, ES5, and ES5 strict mode.
    namespace = (function(){ return this || (1,eval)('this') }());
  }

  function words(s) { return (s || '').split(' '); }

  var DefaultLocale =
  { days: words('Sunday Monday Tuesday Wednesday Thursday Friday Saturday')
  , shortDays: words('Sun Mon Tue Wed Thu Fri Sat')
  , months: words('January February March April May June July August September October November December')
  , shortMonths: words('Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec')
  , AM: 'AM'
  , PM: 'PM'
  , am: 'am'
  , pm: 'pm'
  };

  namespace.strftime = strftime;
  function strftime(fmt, d, locale) {
    return _strftime(fmt, d, locale);
  }

  // locale is optional
  namespace.strftimeTZ = strftime.strftimeTZ = strftimeTZ;
  function strftimeTZ(fmt, d, locale, timezone) {
    if ((typeof locale == 'number' || typeof locale == 'string') && timezone == null) {
      timezone = locale;
      locale = undefined;
    }
    return _strftime(fmt, d, locale, { timezone: timezone });
  }

  namespace.strftimeUTC = strftime.strftimeUTC = strftimeUTC;
  function strftimeUTC(fmt, d, locale) {
    return _strftime(fmt, d, locale, { utc: true });
  }

  namespace.localizedStrftime = strftime.localizedStrftime = localizedStrftime;
  function localizedStrftime(locale) {
    return function(fmt, d, options) {
      return strftime(fmt, d, locale, options);
    };
  }

  // d, locale, and options are optional, but you can't leave
  // holes in the argument list. If you pass options you have to pass
  // in all the preceding args as well.
  //
  // options:
  //   - locale   [object] an object with the same structure as DefaultLocale
  //   - timezone [number] timezone offset in minutes from GMT
  function _strftime(fmt, d, locale, options) {
    options = options || {};

    // d and locale are optional so check if d is really the locale
    if (d && !quacksLikeDate(d)) {
      locale = d;
      d = undefined;
    }
    d = d || new Date();

    locale = locale || DefaultLocale;
    locale.formats = locale.formats || {};

    // Hang on to this Unix timestamp because we might mess with it directly below.
    var timestamp = d.getTime();

    var tz = options.timezone;
    var tzType = typeof tz;

    if (options.utc || tzType == 'number' || tzType == 'string') {
      d = dateToUTC(d);
    }

    if (tz) {
      // ISO 8601 format timezone string, [-+]HHMM
      //
      // Convert to the number of minutes and it'll be applied to the date below.
      if (tzType == 'string') {
        var sign = tz[0] == '-' ? -1 : 1;
        var hours = parseInt(tz.slice(1, 3), 10);
        var mins = parseInt(tz.slice(3, 5), 10);
        tz = sign * ((60 * hours) + mins);
      }

      if (tzType) {
        d = new Date(d.getTime() + (tz * 60000));
      }
    }

    // Most of the specifiers supported by C's strftime, and some from Ruby.
    // Some other syntax extensions from Ruby are supported: %-, %_, and %0
    // to pad with nothing, space, or zero (respectively).
    return fmt.replace(/%([-_0]?.)/g, function(_, c) {
      var mod, padding;

      if (c.length == 2) {
        mod = c[0];
        // omit padding
        if (mod == '-') {
          padding = '';
        }
        // pad with space
        else if (mod == '_') {
          padding = ' ';
        }
        // pad with zero
        else if (mod == '0') {
          padding = '0';
        }
        else {
          // unrecognized, return the format
          return _;
        }
        c = c[1];
      }

      switch (c) {

        // Examples for new Date(0) in GMT

        // 'Thursday'
        case 'A': return locale.days[d.getDay()];

        // 'Thu'
        case 'a': return locale.shortDays[d.getDay()];

        // 'January'
        case 'B': return locale.months[d.getMonth()];

        // 'Jan'
        case 'b': return locale.shortMonths[d.getMonth()];

        // '19'
        case 'C': return pad(Math.floor(d.getFullYear() / 100), padding);

        // '01/01/70'
        case 'D': return _strftime(locale.formats.D || '%m/%d/%y', d, locale);

        // '01'
        case 'd': return pad(d.getDate(), padding);

        // '01'
        case 'e': return d.getDate();

        // '1970-01-01'
        case 'F': return _strftime(locale.formats.F || '%Y-%m-%d', d, locale);

        // '00'
        case 'H': return pad(d.getHours(), padding);

        // 'Jan'
        case 'h': return locale.shortMonths[d.getMonth()];

        // '12'
        case 'I': return pad(hours12(d), padding);

        // '000'
        case 'j':
          var y = new Date(d.getFullYear(), 0, 1);
          var day = Math.ceil((d.getTime() - y.getTime()) / (1000 * 60 * 60 * 24));
          return pad(day, 3);

        // ' 0'
        case 'k': return pad(d.getHours(), padding == null ? ' ' : padding);

        // '000'
        case 'L': return pad(Math.floor(timestamp % 1000), 3);

        // '12'
        case 'l': return pad(hours12(d), padding == null ? ' ' : padding);

        // '00'
        case 'M': return pad(d.getMinutes(), padding);

        // '01'
        case 'm': return pad(d.getMonth() + 1, padding);

        // '\n'
        case 'n': return '\n';

        // '1st'
        case 'o': return String(d.getDate()) + ordinal(d.getDate());

        // 'am'
        case 'P': return d.getHours() < 12 ? locale.am : locale.pm;

        // 'AM'
        case 'p': return d.getHours() < 12 ? locale.AM : locale.PM;

        // '00:00'
        case 'R': return _strftime(locale.formats.R || '%H:%M', d, locale);

        // '12:00:00 AM'
        case 'r': return _strftime(locale.formats.r || '%I:%M:%S %p', d, locale);

        // '00'
        case 'S': return pad(d.getSeconds(), padding);

        // '0'
        case 's': return Math.floor(timestamp / 1000);

        // '00:00:00'
        case 'T': return _strftime(locale.formats.T || '%H:%M:%S', d, locale);

        // '\t'
        case 't': return '\t';

        // '00'
        case 'U': return pad(weekNumber(d, 'sunday'), padding);

        // '4'
        case 'u':
          var day = d.getDay();
          return day == 0 ? 7 : day; // 1 - 7, Monday is first day of the week

        // '1-Jan-1970'
        case 'v': return _strftime(locale.formats.v || '%e-%b-%Y', d, locale);

        // '00'
        case 'W': return pad(weekNumber(d, 'monday'), padding);

        // '4'
        case 'w': return d.getDay(); // 0 - 6, Sunday is first day of the week

        // '1970'
        case 'Y': return d.getFullYear();

        // '70'
        case 'y':
          var y = String(d.getFullYear());
          return y.slice(y.length - 2);

        // 'GMT'
        case 'Z':
          if (options.utc) {
            return "GMT";
          }
          else {
            var tzString = d.toString().match(/\(([\w\s]+)\)/);
            return tzString && tzString[1] || '';
          }

        // '+0000'
        case 'z':
          if (options.utc) {
            return "+0000";
          }
          else {
            var off = typeof tz == 'number' ? tz : -d.getTimezoneOffset();
            return (off < 0 ? '-' : '+') + pad(Math.floor(Math.abs(off) / 60)) + pad(Math.abs(off) % 60);
          }

        default: return c;
      }
    });
  }

  function dateToUTC(d) {
    var msDelta = (d.getTimezoneOffset() || 0) * 60000;
    return new Date(d.getTime() + msDelta);
  }

  var RequiredDateMethods = ['getTime', 'getTimezoneOffset', 'getDay', 'getDate', 'getMonth', 'getFullYear', 'getYear', 'getHours', 'getMinutes', 'getSeconds'];
  function quacksLikeDate(x) {
    var i = 0
      , n = RequiredDateMethods.length
      ;
    for (i = 0; i < n; ++i) {
      if (typeof x[RequiredDateMethods[i]] != 'function') {
        return false;
      }
    }
    return true;
  }

  // Default padding is '0' and default length is 2, both are optional.
  function pad(n, padding, length) {
    // pad(n, <length>)
    if (typeof padding === 'number') {
      length = padding;
      padding = '0';
    }

    // Defaults handle pad(n) and pad(n, <padding>)
    if (padding == null) {
      padding = '0';
    }
    length = length || 2;

    var s = String(n);
    // padding may be an empty string, don't loop forever if it is
    if (padding) {
      while (s.length < length) s = padding + s;
    }
    return s;
  }

  function hours12(d) {
    var hour = d.getHours();
    if (hour == 0) hour = 12;
    else if (hour > 12) hour -= 12;
    return hour;
  }

  // Get the ordinal suffix for a number: st, nd, rd, or th
  function ordinal(n) {
    var i = n % 10
      , ii = n % 100
      ;
    if ((ii >= 11 && ii <= 13) || i === 0 || i >= 4) {
      return 'th';
    }
    switch (i) {
      case 1: return 'st';
      case 2: return 'nd';
      case 3: return 'rd';
    }
  }

  // firstWeekday: 'sunday' or 'monday', default is 'sunday'
  //
  // Pilfered & ported from Ruby's strftime implementation.
  function weekNumber(d, firstWeekday) {
    firstWeekday = firstWeekday || 'sunday';

    // This works by shifting the weekday back by one day if we
    // are treating Monday as the first day of the week.
    var wday = d.getDay();
    if (firstWeekday == 'monday') {
      if (wday == 0) // Sunday
        wday = 6;
      else
        wday--;
    }
    var firstDayOfYear = new Date(d.getFullYear(), 0, 1)
      , yday = (d - firstDayOfYear) / 86400000
      , weekNum = (yday + 7 - wday) / 7
      ;
    return Math.floor(weekNum);
  }

}());

/**
 * @class Roc.handlebars.helpers.strftime
 *
 * Handlebars `strftime` helper.
 * Allows to easily format date and time.
 *
 * ## Sample usage:
 *
 *      {{strftime format="%d-%m-%Y" date=date_string_or_javascript_date}}
 *      {{strftime tformat="short_date" date=date_string_or_javascript_date}}
 *
 * _Note_: `tformat` option requires `Roc.helpers.i18n` package.
 *
 * ### With i18n package:
 *
 *      App.i18n.addLang('cn', {
 *          //...
 *          // date format for strftime
 *          strftime: {
 *
 *              formats: {
 *                  short_date: '%Y年%b%e%a',
 *                  short_datetime: '%Y年%b%e%a %I:%M%p'
 *              },
 *
 *              days: [ '星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六' ],
 *              shortDays: [ '日', '一', '二', '三', '四', '五', '六' ],
 *
 *              months: [ '一月', '二月', '三月', '四月', '五月', '六月', '七月',
 *                '八月', '九月', '十月', '十一月', '十二月' ],
 *
 *              shortMonths: [ '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月',
 *                '9月', '10月', '11月', '12月' ],
 *              AM: '上午',
 *              PM: '下午'
 *          }
 *      });
 *
 *      And then you can use:
 *
 *      {{strftime date=my_date tformat="short_date"}}
 *
 */
Handlebars.registerHelper('strftime', function(context, options) {

    if (!options) options = context;

    var date = options.hash.date,
        format = options.hash.format;

    if (typeof format === 'undefined' &&
        options.hash.tformat) {
        format = t('strftime').formats[options.hash.tformat];
    }

    if (typeof date === 'string' ||
        typeof date === 'number') {
        date = new Date(date);
    }

    if (typeof t === 'function' &&
        typeof t('strftime') === 'object') {

        var timezone = ((new Date()).getTimezoneOffset()).toString();

        return strftimeTZ(format, date, t('strftime'), timezone);
    } else {
        return strftime(format, date);
    }

});

Handlebars.registerPartial("for_id", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " for=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.for_id
 *
 * Handlebars `for_id` partial.
 *
 * This partial is used while `label` HTML tag needs to be link to an `id`.
 *
 * ## Sample usage
 *
 * 		{{> for_id}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *				config: {
 *					label: 'Username',
 *					id: 'username'
 *				}
 *			}
 *		};
 *
 * #### Handlebars template
 *
 *		<label{{> for_id}}>
 *			{{label}}:
 *			<input type="text"{{> id}} />
 *		</label>
 *
 * #### Output
 *
 * 		<label for="username">
 * 			Username:
 * 			<input type="text" id="username" />
 * 		</label>
 *
 */

/**
 * @class Roc.handlebars.server.xtype
 *
 * Handlebars `xtype` server-side helper.
 *
 * You can use this helper directly in Roc source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      {{xtype}}
 *      {{xtype "alert"}}
 *      {{xtype type=false}}
 *      {{xtype type="views"}}
 *
 * ### Example:
 *
 * #### Given ROC JSON configuration (from package `.roc.json` file)
 *
 *      {
 *          "name": "alert",
 *          "version": "0.1.0",
 *          "roc_type": "components"
 *      }
 *
 * #### Javascript source file
 *
 *      {{xtype}}
 *      {{xtype "alert"}}
 *      {{xtype type=false}}
 *      {{xtype type="views"}}
 *
 * #### Output
 *
 *      components.alert
 *      alert
 *      alert
 *      views.alert
 *
 */

/**
 * @class Roc.handlebars.server.classname
 *
 * Handlebars `classname` server-side helper.
 *
 * You can use this helper directly in Roc source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      {{classname}}
 *      {{classname type=false}}
 *      {{classname type="views"}}
 *      {{classname type="views" base="App"}}
 *      {{classname type="views" base="App" initialize=true}}
 *      {{classname "App.views.Home"}}
 *
 * ### Example:
 *
 * #### Given ROC JSON configuration (from package `.roc.json` file)
 *
 *      {
 *          "name": "home",
 *          "version": "0.1.0",
 *          "roc_type": "app"
 *      }
 *
 * #### Javascript source file
 *
 *      {{classname}}
 *      {{classname type=false}}
 *      {{classname type="views"}}
 *      {{classname type="views" base="App"}}
 *      {{classname type="views" base="App" initialize=true}}
 *      {{classname "App.views.Home"}}
 *
 * #### Output
 *
 *      Roc.app.Home
 *      Roc.Home
 *      Roc.views.Home
 *      App.views.Home
 *      if (!window.App) window.App = {}; if (!window.App.views) window.App.views = {}; App.views.Home
 *      App.views.Home
 *
 */

/**
 * @class Roc.handlebars.server.lowercase
 *
 * Handlebars `lowercase` server-side helper.
 *
 * You can use this helper directly in Roc source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      {{lowercase "Hello World"}}
 *
 * ### Example:
 *
 * #### Javascript source file
 *
 *      {{lowercase "Hello World"}}
 *
 * #### Output
 *
 *      hello world
 *
 */

/**
 * @class Roc.handlebars.server.ns
 *
 * Handlebars `ns` (namespace) server-side helper.
 *
 * The namespace is based on the Roc `package.json` file (`name` configuration key value).
 *
 * You can use this helper directly in Roc source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      {{ns}}
 *      {{ns lowercase=true}}
 *      {{ns uppercase=true}}
 *
 * ### Example:
 *
 * #### Javascript source file
 *
 *      {{ns}}
 *      {{ns lowercase=true}}
 *      {{ns uppercase=true}}
 *
 * #### Output
 *
 *      Roc
 *      roc
 *      ROC
 *
 */

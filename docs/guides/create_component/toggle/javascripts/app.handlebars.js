(function() {

    var app = new {{ns}}.App({
        engine: 'ionic',
        onready: function() {

            var page = new {{ns}}.views.Page({
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'positive',
                        title: 'Toggle component'
                    }
                }, {
                    xtpl: 'content',
                    config: {
                        hasHeader: true,
                        padding: false
                    },
                    items: ['<ul class="list"><li class="item item-toggle">New toggle', {
                        xtype: 'toggle',
                        config: {
                            ui: 'positive'
                        }
                    }, '</li></ul>']
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());

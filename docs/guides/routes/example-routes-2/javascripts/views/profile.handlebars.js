{{ns}}.extend('page', {
    xtype: 'user.profile',
    xtpl: 'page',
    config: {
        items: [{
            xtype: 'header',
            config: {
                ui: 'assertive',
                title: 'My profile'
            }
        }, {
            xtpl: 'content',
            config: {
                hasHeader: true
            },
            items: 'This is my profile page'
        }]
    }
});

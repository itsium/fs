var router = new ({{ns}}.extend('routerview', {
    routeNoMatch: 'routeHome',
    routes: {
        '/home': 'routeHome',
        '/user/profile': 'routeProfile',
        '/say/hello/to/:name': 'routeHello'
    },
    routeHome: function() {
        this.setCurrentView('home');
    },
    routeProfile: function() {
        this.setCurrentView('user.profile');
    },
    routeHello: function(name) {
        this.setCurrentView('hello', {}, {
            name: name
        });
    }
}))();

{{ns}}.extend('template', {
    xtype: 'home.card',
    xtpl: 'card',
    config: {
        config: {
            header: 'Header',
            footer: 'Footer'
        },
        items: [{
            xtpl: 'container',
            config: {
                title: 'Card 1'
            },
            items: '<p>Card 1 content</p>'
        }, {
            xtpl: 'container',
            config: {
                title: 'Card 2'
            },
            items: '<p>Card 2 content</p>'
        }]
    }
});
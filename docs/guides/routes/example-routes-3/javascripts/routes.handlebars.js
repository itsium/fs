// for testing only
var session = {
    logged: false // or false
};

var router = new ({{ns}}.extend('routerview', {
    routeNoMatch: 'routeHome',
    before: {
        'routeProfile': 'isAuthenticated'
    },
    routes: {
        '/home': 'routeHome',
        '/user/profile': 'routeProfile',
        '/say/hello/to/:name': 'routeHello'
    },
    isAuthenticated: function(done) {

        // we are assuming you have created a session object
        // which stores user information
        if (session.logged === true) {
            // if user is logged in then we can call the done callback here to
            // go to the next before, or to the route if there is no next before.
            return done();
        }

        alert('Sorry you need to be logged in order to access to this page.');
        // else we redirect the user to the home view
        // or we can show an alert box here and then redirect
        Roc.History.go('/home');
        return false;

    },
    routeHome: function() {
        this.setCurrentView('home');
    },
    routeProfile: function() {
        this.setCurrentView('user.profile');
    },
    routeHello: function(name) {
        this.setCurrentView('hello', {}, {
            name: name
        });
    }
}))();

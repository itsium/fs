/* selector benchmarks */

(function() {

    'use strict';

    var _win = window,
        _fs = _win.Fs,
        _selector = _fs.Selector,
        _el = _selector.get('#testel');

    var s = _win.bench = new _win.Benchmark.Suite('Selector');

    s.add('hasClass', function() {
        _selector.hasClass(_el, 'testcls1 testcls2 testcls3');
    });

    s.add('addClass', function() {
        _selector.addClass(_el, 'testcls1 testcls2 testcls3');
    });

    s.add('addClasses', function() {
        _selector.addClasses(_el, 'testcls1 testcls2 testcls3');
    });

    s.add('removeClass', function() {
        _selector.removeClass(_el, 'testcls1 testcls2 testcls3');
    });

    s.add('removeClasses', function() {
        _selector.removeClasses(_el, 'testcls1 testcls2 testcls3');
    });

    s.add('get', function() {
        _selector.get('#testcls');
    });

    s.add('removeHtml', function() {
        _selector.removeHtml(_el);
    });

    s.add('updateHtml', function() {
        _selector.updateHtml(_el, 'lolilol');
    });

    s.add('update#innerHTML', function() {
        _el.innerHTML = 'lolilol';
    });

    s.on('cycle', function(event) {
        console.log(String(event.target));
    })
    .on('complete', function() {
        console.log('Fastest is ' + this.filter('fastest').pluck('name'));
    });


}());
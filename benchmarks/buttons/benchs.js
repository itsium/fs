/* buttons benchmarks */

(function() {

    'use strict';

    var _win = window,
        _fs = _win.Fs,
        _button = _fs.views.Button;

    _win.bench_compile = new _win.Benchmark('Button compile (empty config)', function() {

        this.btn.compile();

    }, {
        onStart: function() {
        },
        setup: function() {
            this.btn = new Fs.views.Button({
                engine: Fs.engines.JqueryMobile
            });
        },
        teardown: function() {
            delete this.btn;
        },
        onComplete: function() {
            console.log(this.toString());
        },
        onError: function(event) {
            console.log('error', event, event.message);
        }
    });

    _win.bench_compile_full = new _win.Benchmark('Button compile (full config)', function() {

        this.btn.compile();

    }, {
        onStart: function() {
        },
        setup: function() {
            this.btn = new Fs.views.Button({
                config: {
                    text: 'testcase',
                    route: '/test/case',
                    size: 'large',
                    icon: 'vcard',
                    iconPos: 'left',
                    inline: true,
                    roundCorners: true
                },
                engine: Fs.engines.JqueryMobile
            });
        },
        teardown: function() {
            delete this.btn;
        },
        onComplete: function() {
            console.log(this.toString());
        },
        onError: function(event) {
            console.log('error', event, event.message);
        }
    });

    _win.bench_render = new _win.Benchmark('Button render', function() {

        this.btn.render('body');
        this.btn.remove();

    }, {
        onStart: function() {
        },
        setup: function() {
            this.btn = new Fs.views.Button({
                config: {
                    text: 'salut !'
                },
                engine: Fs.engines.JqueryMobile
            });
            this.btn.compile();
        },
        teardown: function() {
            delete this.btn;
        },
        onComplete: function() {
            console.log(this.toString());
        },
        onError: function(event) {
            console.log('error', event, event.message);
        }
    });

    _win.bench_disabled = new _win.Benchmark('Button setDisabled', function() {

        this.btn.setDisabled(!this.btn.disabled);

    }, {
        onStart: function() {
        },
        setup: function() {
            this.btn = new Fs.views.Button({
                config: {
                    text: 'salut !'
                },
                engine: Fs.engines.JqueryMobile
            });
            this.btn.compile();
            this.btn.render('body');
        },
        teardown: function() {
            this.btn.remove();
            delete this.btn;
        },
        onComplete: function() {
            console.log(this.toString());
        },
        onError: function(event) {
            console.log('error', event, event.message);
        }
    });

    _win.dom_count = {
        run: function() {

            var bb = _win.BB;

            bb.start();

            this.btn = new Fs.views.Button({
                config: {
                    text: 'Test case dom count',
                    route: '/test/case',
                    icon: 'vcard',
                    iconPos: 'left'
                },
                handler: function() {},
                engine: Fs.engines.JqueryMobile,
                listeners: {
                    scope: this,
                    afterrender: function() {
                        console.log(bb.report(this.btn.el));
                    }
                }
            });
            this.btn.compile();
            this.btn.render('#playground');
        }
    };

}());

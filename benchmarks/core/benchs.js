/* core benchmarks */

(function() {

    'use strict';

    var _win = window,
        _fs = _win.Fs,
        _itemXtype = {
            xtype: 'button'
        },
        _item = new _fs.Event(),
        _parent = _fs.Event;

    var s = _win.bench = new _win.Benchmark.Suite('Core');

    s.add('instanceof success', function() {

        var result;

        if (!(_itemXtype instanceof _parent)) {
            result = true;
        } else {
            result = false;
        }

    });

    s.add('__proto__ success', function() {

        var result;

        if (typeof _itemXtype.__proto__.xtype === 'undefined') {
            result = true;
        } else {
            result = false;
        }

    });

    s.add('instanceof fail', function() {

        var result;

        if (!(_item instanceof _parent)) {
            result = true;
        } else {
            result = false;
        }

    });

    s.add('__proto__ fail', function() {

        var result;

        if (typeof _item.__proto__.xtype === 'undefined') {
            result = true;
        } else {
            result = false;
        }

    });

    s.on('cycle', function(event) {
        console.log(String(event.target));
    })
    .on('complete', function() {
        console.log('Fastest is ' + this.filter('fastest').pluck('name'));
    });


}());
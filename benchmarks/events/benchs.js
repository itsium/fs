/* events benchmarks */

(function() {

    'use strict';

    var _event,
        _win = window;

    var s = _win.bench = new _win.Benchmark.Suite('Event', {

        onStart: function() {
            _event = new _win.Fs.Event();
        }

    });

    s.add('on#off', function() {

        var e = _event;

        var uid = e.on('testcase1', function(a, b) {});

        e.fire('testcase1', 0, true);
        var length = e.off(uid);
    });

    s.add('off#inside callback', function() {

        var e = _event;

        var uids = [], counter = 0, lengths = [];

        uids[0] = e.on('testcase2', function(a, b) {
            ++counter;
        });

        uids[1] = e.on('testcase2', function(a, b) {
            ++counter;
            lengths[0] = e.off(uids[1]);
        });

        uids[2] = e.on('testcase2', function(a, b) {
            lengths[1] = e.off(uids[0]);
        });

        e.fire('testcase2', 1, false);

        lengths[2] = e.off(uids[2]);

    });

    s.add('callback#return false', function() {

        var e = _event;

        var uids = [], counter = 0, lengths = [];

        uids[0] = e.on('testcase3', function(a, b) {
            ++counter;
            lengths[0] = e.off(uids[0]);
            return false;
        });

        uids[1] = e.on('testcase3', function(a, b) {
            ++counter;
        });

        uids[2] = e.on('testcase3', function(a, b) {
            ++counter;
        });

        e.fire('testcase3', 2, null);

        lengths[1] = e.off(uids[1]);
        lengths[2] = e.off(uids[2]);

    });

    s.add('priority', function() {

        var e = _event;

        var uids = [], counter = 0, lengths = [];

        uids[0] = e.on('testcase4', function(a, b) {
            ++counter;
        }, this, e.priority.DEFAULT);

        uids[1] = e.on('testcase4', function(a, b) {
            ++counter;
        }, this, e.priority.VIEWS);

        uids[2] = e.on('testcase4', function(a, b) {
            ++counter;
        }, this, e.priority.CORE);

        e.fire('testcase4', 2, null);

        lengths[0] = e.off(uids[0]);
        lengths[1] = e.off(uids[1]);
        lengths[2] = e.off(uids[2]);

    });

    s.on('cycle', function(event) {
        console.log(String(event.target));
    })
    .on('complete', function() {
        console.log('Fastest is ' + this.filter('fastest').pluck('name'));
    });


}());
/*
Copyright (c) 2008-2014 Pivotal Labs

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
function getJasmineRequireObj() {
  if (typeof module !== 'undefined' && module.exports) {
    return exports;
  } else {
    window.jasmineRequire = window.jasmineRequire || {};
    return window.jasmineRequire;
  }
}

getJasmineRequireObj().core = function(jRequire) {
  var j$ = {};

  jRequire.base(j$);
  j$.util = jRequire.util();
  j$.Any = jRequire.Any();
  j$.CallTracker = jRequire.CallTracker();
  j$.MockDate = jRequire.MockDate();
  j$.Clock = jRequire.Clock();
  j$.DelayedFunctionScheduler = jRequire.DelayedFunctionScheduler();
  j$.Env = jRequire.Env(j$);
  j$.ExceptionFormatter = jRequire.ExceptionFormatter();
  j$.Expectation = jRequire.Expectation();
  j$.buildExpectationResult = jRequire.buildExpectationResult();
  j$.JsApiReporter = jRequire.JsApiReporter();
  j$.matchersUtil = jRequire.matchersUtil(j$);
  j$.ObjectContaining = jRequire.ObjectContaining(j$);
  j$.pp = jRequire.pp(j$);
  j$.QueueRunner = jRequire.QueueRunner(j$);
  j$.ReportDispatcher = jRequire.ReportDispatcher();
  j$.Spec = jRequire.Spec(j$);
  j$.SpyStrategy = jRequire.SpyStrategy();
  j$.Suite = jRequire.Suite();
  j$.Timer = jRequire.Timer();
  j$.version = jRequire.version();

  j$.matchers = jRequire.requireMatchers(jRequire, j$);

  return j$;
};

getJasmineRequireObj().requireMatchers = function(jRequire, j$) {
  var availableMatchers = [
      'toBe',
      'toBeCloseTo',
      'toBeDefined',
      'toBeFalsy',
      'toBeGreaterThan',
      'toBeLessThan',
      'toBeNaN',
      'toBeNull',
      'toBeTruthy',
      'toBeUndefined',
      'toContain',
      'toEqual',
      'toHaveBeenCalled',
      'toHaveBeenCalledWith',
      'toMatch',
      'toThrow',
      'toThrowError'
    ],
    matchers = {};

  for (var i = 0; i < availableMatchers.length; i++) {
    var name = availableMatchers[i];
    matchers[name] = jRequire[name](j$);
  }

  return matchers;
};

getJasmineRequireObj().base = (function (jasmineGlobal) {
  if (typeof module !== 'undefined' && module.exports) {
    jasmineGlobal = global;
  }

  return function(j$) {
    j$.unimplementedMethod_ = function() {
      throw new Error('unimplemented method');
    };

    j$.MAX_PRETTY_PRINT_DEPTH = 40;
    j$.MAX_PRETTY_PRINT_ARRAY_LENGTH = 100;
    j$.DEFAULT_TIMEOUT_INTERVAL = 5000;

    j$.getGlobal = function() {
      return jasmineGlobal;
    };

    j$.getEnv = function(options) {
      var env = j$.currentEnv_ = j$.currentEnv_ || new j$.Env(options);
      //jasmine. singletons in here (setTimeout blah blah).
      return env;
    };

    j$.isArray_ = function(value) {
      return j$.isA_('Array', value);
    };

    j$.isString_ = function(value) {
      return j$.isA_('String', value);
    };

    j$.isNumber_ = function(value) {
      return j$.isA_('Number', value);
    };

    j$.isA_ = function(typeName, value) {
      return Object.prototype.toString.apply(value) === '[object ' + typeName + ']';
    };

    j$.isDomNode = function(obj) {
      return obj.nodeType > 0;
    };

    j$.any = function(clazz) {
      return new j$.Any(clazz);
    };

    j$.objectContaining = function(sample) {
      return new j$.ObjectContaining(sample);
    };

    j$.createSpy = function(name, originalFn) {

      var spyStrategy = new j$.SpyStrategy({
          name: name,
          fn: originalFn,
          getSpy: function() { return spy; }
        }),
        callTracker = new j$.CallTracker(),
        spy = function() {
          callTracker.track({
            object: this,
            args: Array.prototype.slice.apply(arguments)
          });
          return spyStrategy.exec.apply(this, arguments);
        };

      for (var prop in originalFn) {
        if (prop === 'and' || prop === 'calls') {
          throw new Error('Jasmine spies would overwrite the \'and\' and \'calls\' properties on the object being spied upon');
        }

        spy[prop] = originalFn[prop];
      }

      spy.and = spyStrategy;
      spy.calls = callTracker;

      return spy;
    };

    j$.isSpy = function(putativeSpy) {
      if (!putativeSpy) {
        return false;
      }
      return putativeSpy.and instanceof j$.SpyStrategy &&
        putativeSpy.calls instanceof j$.CallTracker;
    };

    j$.createSpyObj = function(baseName, methodNames) {
      if (!j$.isArray_(methodNames) || methodNames.length === 0) {
        throw 'createSpyObj requires a non-empty array of method names to create spies for';
      }
      var obj = {};
      for (var i = 0; i < methodNames.length; i++) {
        obj[methodNames[i]] = j$.createSpy(baseName + '.' + methodNames[i]);
      }
      return obj;
    };
  };
})(this);

getJasmineRequireObj().util = function() {

  var util = {};

  util.inherit = function(childClass, parentClass) {
    var Subclass = function() {
    };
    Subclass.prototype = parentClass.prototype;
    childClass.prototype = new Subclass();
  };

  util.htmlEscape = function(str) {
    if (!str) {
      return str;
    }
    return str.replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
  };

  util.argsToArray = function(args) {
    var arrayOfArgs = [];
    for (var i = 0; i < args.length; i++) {
      arrayOfArgs.push(args[i]);
    }
    return arrayOfArgs;
  };

  util.isUndefined = function(obj) {
    return obj === void 0;
  };

  util.arrayContains = function(array, search) {
    var i = array.length;
    while (i--) {
      if (array[i] == search) {
        return true;
      }
    }
    return false;
  };

  return util;
};

getJasmineRequireObj().Spec = function(j$) {
  function Spec(attrs) {
    this.expectationFactory = attrs.expectationFactory;
    this.resultCallback = attrs.resultCallback || function() {};
    this.id = attrs.id;
    this.description = attrs.description || '';
    this.fn = attrs.fn;
    this.beforeFns = attrs.beforeFns || function() { return []; };
    this.afterFns = attrs.afterFns || function() { return []; };
    this.onStart = attrs.onStart || function() {};
    this.exceptionFormatter = attrs.exceptionFormatter || function() {};
    this.getSpecName = attrs.getSpecName || function() { return ''; };
    this.expectationResultFactory = attrs.expectationResultFactory || function() { };
    this.queueRunnerFactory = attrs.queueRunnerFactory || function() {};
    this.catchingExceptions = attrs.catchingExceptions || function() { return true; };

    if (!this.fn) {
      this.pend();
    }

    this.result = {
      id: this.id,
      description: this.description,
      fullName: this.getFullName(),
      failedExpectations: [],
      passedExpectations: []
    };
  }

  Spec.prototype.addExpectationResult = function(passed, data) {
    var expectationResult = this.expectationResultFactory(data);
    if (passed) {
      this.result.passedExpectations.push(expectationResult);
    } else {
      this.result.failedExpectations.push(expectationResult);
    }
  };

  Spec.prototype.expect = function(actual) {
    return this.expectationFactory(actual, this);
  };

  Spec.prototype.execute = function(onComplete) {
    var self = this;

    this.onStart(this);

    if (this.markedPending || this.disabled) {
      complete();
      return;
    }

    var allFns = this.beforeFns().concat(this.fn).concat(this.afterFns());

    this.queueRunnerFactory({
      fns: allFns,
      onException: onException,
      onComplete: complete,
      enforceTimeout: function() { return true; }
    });

    function onException(e) {
      if (Spec.isPendingSpecException(e)) {
        self.pend();
        return;
      }

      self.addExpectationResult(false, {
        matcherName: '',
        passed: false,
        expected: '',
        actual: '',
        error: e
      });
    }

    function complete() {
      self.result.status = self.status();
      self.resultCallback(self.result);

      if (onComplete) {
        onComplete();
      }
    }
  };

  Spec.prototype.disable = function() {
    this.disabled = true;
  };

  Spec.prototype.pend = function() {
    this.markedPending = true;
  };

  Spec.prototype.status = function() {
    if (this.disabled) {
      return 'disabled';
    }

    if (this.markedPending) {
      return 'pending';
    }

    if (this.result.failedExpectations.length > 0) {
      return 'failed';
    } else {
      return 'passed';
    }
  };

  Spec.prototype.getFullName = function() {
    return this.getSpecName(this);
  };

  Spec.pendingSpecExceptionMessage = '=> marked Pending';

  Spec.isPendingSpecException = function(e) {
    return !!(e && e.toString && e.toString().indexOf(Spec.pendingSpecExceptionMessage) !== -1);
  };

  return Spec;
};

if (typeof window == void 0 && typeof exports == 'object') {
  exports.Spec = jasmineRequire.Spec;
}

getJasmineRequireObj().Env = function(j$) {
  function Env(options) {
    options = options || {};

    var self = this;
    var global = options.global || j$.getGlobal();

    var totalSpecsDefined = 0;

    var catchExceptions = true;

    var realSetTimeout = j$.getGlobal().setTimeout;
    var realClearTimeout = j$.getGlobal().clearTimeout;
    this.clock = new j$.Clock(global, new j$.DelayedFunctionScheduler(), new j$.MockDate(global));

    var runnableLookupTable = {};

    var spies = [];

    var currentSpec = null;
    var currentSuite = null;

    var reporter = new j$.ReportDispatcher([
      'jasmineStarted',
      'jasmineDone',
      'suiteStarted',
      'suiteDone',
      'specStarted',
      'specDone'
    ]);

    this.specFilter = function() {
      return true;
    };

    var equalityTesters = [];

    var customEqualityTesters = [];
    this.addCustomEqualityTester = function(tester) {
      customEqualityTesters.push(tester);
    };

    j$.Expectation.addCoreMatchers(j$.matchers);

    var nextSpecId = 0;
    var getNextSpecId = function() {
      return 'spec' + nextSpecId++;
    };

    var nextSuiteId = 0;
    var getNextSuiteId = function() {
      return 'suite' + nextSuiteId++;
    };

    var expectationFactory = function(actual, spec) {
      return j$.Expectation.Factory({
        util: j$.matchersUtil,
        customEqualityTesters: customEqualityTesters,
        actual: actual,
        addExpectationResult: addExpectationResult
      });

      function addExpectationResult(passed, result) {
        return spec.addExpectationResult(passed, result);
      }
    };

    var specStarted = function(spec) {
      currentSpec = spec;
      reporter.specStarted(spec.result);
    };

    var beforeFns = function(suite) {
      return function() {
        var befores = [];
        while(suite) {
          befores = befores.concat(suite.beforeFns);
          suite = suite.parentSuite;
        }
        return befores.reverse();
      };
    };

    var afterFns = function(suite) {
      return function() {
        var afters = [];
        while(suite) {
          afters = afters.concat(suite.afterFns);
          suite = suite.parentSuite;
        }
        return afters;
      };
    };

    var getSpecName = function(spec, suite) {
      return suite.getFullName() + ' ' + spec.description;
    };

    // TODO: we may just be able to pass in the fn instead of wrapping here
    var buildExpectationResult = j$.buildExpectationResult,
        exceptionFormatter = new j$.ExceptionFormatter(),
        expectationResultFactory = function(attrs) {
          attrs.messageFormatter = exceptionFormatter.message;
          attrs.stackFormatter = exceptionFormatter.stack;

          return buildExpectationResult(attrs);
        };

    // TODO: fix this naming, and here's where the value comes in
    this.catchExceptions = function(value) {
      catchExceptions = !!value;
      return catchExceptions;
    };

    this.catchingExceptions = function() {
      return catchExceptions;
    };

    var maximumSpecCallbackDepth = 20;
    var currentSpecCallbackDepth = 0;

    function clearStack(fn) {
      currentSpecCallbackDepth++;
      if (currentSpecCallbackDepth >= maximumSpecCallbackDepth) {
        currentSpecCallbackDepth = 0;
        realSetTimeout(fn, 0);
      } else {
        fn();
      }
    }

    var catchException = function(e) {
      return j$.Spec.isPendingSpecException(e) || catchExceptions;
    };

    var queueRunnerFactory = function(options) {
      options.catchException = catchException;
      options.clearStack = options.clearStack || clearStack;
      options.timer = {setTimeout: realSetTimeout, clearTimeout: realClearTimeout};

      new j$.QueueRunner(options).execute();
    };

    var topSuite = new j$.Suite({
      env: this,
      id: getNextSuiteId(),
      description: 'Jasmine__TopLevel__Suite',
      queueRunner: queueRunnerFactory,
      resultCallback: function() {} // TODO - hook this up
    });
    runnableLookupTable[topSuite.id] = topSuite;
    currentSuite = topSuite;

    this.topSuite = function() {
      return topSuite;
    };

    this.execute = function(runnablesToRun) {
      runnablesToRun = runnablesToRun || [topSuite.id];

      var allFns = [];
      for(var i = 0; i < runnablesToRun.length; i++) {
        var runnable = runnableLookupTable[runnablesToRun[i]];
        allFns.push((function(runnable) { return function(done) { runnable.execute(done); }; })(runnable));
      }

      reporter.jasmineStarted({
        totalSpecsDefined: totalSpecsDefined
      });

      queueRunnerFactory({fns: allFns, onComplete: reporter.jasmineDone});
    };

    this.addReporter = function(reporterToAdd) {
      reporter.addReporter(reporterToAdd);
    };

    this.addMatchers = function(matchersToAdd) {
      j$.Expectation.addMatchers(matchersToAdd);
    };

    this.spyOn = function(obj, methodName) {
      if (j$.util.isUndefined(obj)) {
        throw new Error('spyOn could not find an object to spy upon for ' + methodName + '()');
      }

      if (j$.util.isUndefined(obj[methodName])) {
        throw new Error(methodName + '() method does not exist');
      }

      if (obj[methodName] && j$.isSpy(obj[methodName])) {
        //TODO?: should this return the current spy? Downside: may cause user confusion about spy state
        throw new Error(methodName + ' has already been spied upon');
      }

      var spy = j$.createSpy(methodName, obj[methodName]);

      spies.push({
        spy: spy,
        baseObj: obj,
        methodName: methodName,
        originalValue: obj[methodName]
      });

      obj[methodName] = spy;

      return spy;
    };

    var suiteFactory = function(description) {
      var suite = new j$.Suite({
        env: self,
        id: getNextSuiteId(),
        description: description,
        parentSuite: currentSuite,
        queueRunner: queueRunnerFactory,
        onStart: suiteStarted,
        resultCallback: function(attrs) {
          reporter.suiteDone(attrs);
        }
      });

      runnableLookupTable[suite.id] = suite;
      return suite;
    };

    this.describe = function(description, specDefinitions) {
      var suite = suiteFactory(description);

      var parentSuite = currentSuite;
      parentSuite.addChild(suite);
      currentSuite = suite;

      var declarationError = null;
      try {
        specDefinitions.call(suite);
      } catch (e) {
        declarationError = e;
      }

      if (declarationError) {
        this.it('encountered a declaration exception', function() {
          throw declarationError;
        });
      }

      currentSuite = parentSuite;

      return suite;
    };

    this.xdescribe = function(description, specDefinitions) {
      var suite = this.describe(description, specDefinitions);
      suite.disable();
      return suite;
    };

    var specFactory = function(description, fn, suite) {
      totalSpecsDefined++;

      var spec = new j$.Spec({
        id: getNextSpecId(),
        beforeFns: beforeFns(suite),
        afterFns: afterFns(suite),
        expectationFactory: expectationFactory,
        exceptionFormatter: exceptionFormatter,
        resultCallback: specResultCallback,
        getSpecName: function(spec) {
          return getSpecName(spec, suite);
        },
        onStart: specStarted,
        description: description,
        expectationResultFactory: expectationResultFactory,
        queueRunnerFactory: queueRunnerFactory,
        fn: fn
      });

      runnableLookupTable[spec.id] = spec;

      if (!self.specFilter(spec)) {
        spec.disable();
      }

      return spec;

      function removeAllSpies() {
        for (var i = 0; i < spies.length; i++) {
          var spyEntry = spies[i];
          spyEntry.baseObj[spyEntry.methodName] = spyEntry.originalValue;
        }
        spies = [];
      }

      function specResultCallback(result) {
        removeAllSpies();
        j$.Expectation.resetMatchers();
        customEqualityTesters = [];
        currentSpec = null;
        reporter.specDone(result);
      }
    };

    var suiteStarted = function(suite) {
      reporter.suiteStarted(suite.result);
    };

    this.it = function(description, fn) {
      var spec = specFactory(description, fn, currentSuite);
      currentSuite.addChild(spec);
      return spec;
    };

    this.xit = function(description, fn) {
      var spec = this.it(description, fn);
      spec.pend();
      return spec;
    };

    this.expect = function(actual) {
      if (!currentSpec) {
        throw new Error('\'expect\' was used when there was no current spec, this could be because an asynchronous test timed out');
      }

      return currentSpec.expect(actual);
    };

    this.beforeEach = function(beforeEachFunction) {
      currentSuite.beforeEach(beforeEachFunction);
    };

    this.afterEach = function(afterEachFunction) {
      currentSuite.afterEach(afterEachFunction);
    };

    this.pending = function() {
      throw j$.Spec.pendingSpecExceptionMessage;
    };
  }

  return Env;
};

getJasmineRequireObj().JsApiReporter = function() {

  var noopTimer = {
    start: function(){},
    elapsed: function(){ return 0; }
  };

  function JsApiReporter(options) {
    var timer = options.timer || noopTimer,
        status = 'loaded';

    this.started = false;
    this.finished = false;

    this.jasmineStarted = function() {
      this.started = true;
      status = 'started';
      timer.start();
    };

    var executionTime;

    this.jasmineDone = function() {
      this.finished = true;
      executionTime = timer.elapsed();
      status = 'done';
    };

    this.status = function() {
      return status;
    };

    var suites = {};

    this.suiteStarted = function(result) {
      storeSuite(result);
    };

    this.suiteDone = function(result) {
      storeSuite(result);
    };

    function storeSuite(result) {
      suites[result.id] = result;
    }

    this.suites = function() {
      return suites;
    };

    var specs = [];
    this.specStarted = function(result) { };

    this.specDone = function(result) {
      specs.push(result);
    };

    this.specResults = function(index, length) {
      return specs.slice(index, index + length);
    };

    this.specs = function() {
      return specs;
    };

    this.executionTime = function() {
      return executionTime;
    };

  }

  return JsApiReporter;
};

getJasmineRequireObj().Any = function() {

  function Any(expectedObject) {
    this.expectedObject = expectedObject;
  }

  Any.prototype.jasmineMatches = function(other) {
    if (this.expectedObject == String) {
      return typeof other == 'string' || other instanceof String;
    }

    if (this.expectedObject == Number) {
      return typeof other == 'number' || other instanceof Number;
    }

    if (this.expectedObject == Function) {
      return typeof other == 'function' || other instanceof Function;
    }

    if (this.expectedObject == Object) {
      return typeof other == 'object';
    }
    
    if (this.expectedObject == Boolean) {
      return typeof other == 'boolean';
    }

    return other instanceof this.expectedObject;
  };

  Any.prototype.jasmineToString = function() {
    return '<jasmine.any(' + this.expectedObject + ')>';
  };

  return Any;
};

getJasmineRequireObj().CallTracker = function() {

  function CallTracker() {
    var calls = [];

    this.track = function(context) {
      calls.push(context);
    };

    this.any = function() {
      return !!calls.length;
    };

    this.count = function() {
      return calls.length;
    };

    this.argsFor = function(index) {
      var call = calls[index];
      return call ? call.args : [];
    };

    this.all = function() {
      return calls;
    };

    this.allArgs = function() {
      var callArgs = [];
      for(var i = 0; i < calls.length; i++){
        callArgs.push(calls[i].args);
      }

      return callArgs;
    };

    this.first = function() {
      return calls[0];
    };

    this.mostRecent = function() {
      return calls[calls.length - 1];
    };

    this.reset = function() {
      calls = [];
    };
  }

  return CallTracker;
};

getJasmineRequireObj().Clock = function() {
  function Clock(global, delayedFunctionScheduler, mockDate) {
    var self = this,
      realTimingFunctions = {
        setTimeout: global.setTimeout,
        clearTimeout: global.clearTimeout,
        setInterval: global.setInterval,
        clearInterval: global.clearInterval
      },
      fakeTimingFunctions = {
        setTimeout: setTimeout,
        clearTimeout: clearTimeout,
        setInterval: setInterval,
        clearInterval: clearInterval
      },
      installed = false,
      timer;


    self.install = function() {
      replace(global, fakeTimingFunctions);
      timer = fakeTimingFunctions;
      installed = true;

      return self;
    };

    self.uninstall = function() {
      delayedFunctionScheduler.reset();
      mockDate.uninstall();
      replace(global, realTimingFunctions);

      timer = realTimingFunctions;
      installed = false;
    };

    self.mockDate = function(initialDate) {
      mockDate.install(initialDate);
    };

    self.setTimeout = function(fn, delay, params) {
      if (legacyIE()) {
        if (arguments.length > 2) {
          throw new Error('IE < 9 cannot support extra params to setTimeout without a polyfill');
        }
        return timer.setTimeout(fn, delay);
      }
      return Function.prototype.apply.apply(timer.setTimeout, [global, arguments]);
    };

    self.setInterval = function(fn, delay, params) {
      if (legacyIE()) {
        if (arguments.length > 2) {
          throw new Error('IE < 9 cannot support extra params to setInterval without a polyfill');
        }
        return timer.setInterval(fn, delay);
      }
      return Function.prototype.apply.apply(timer.setInterval, [global, arguments]);
    };

    self.clearTimeout = function(id) {
      return Function.prototype.call.apply(timer.clearTimeout, [global, id]);
    };

    self.clearInterval = function(id) {
      return Function.prototype.call.apply(timer.clearInterval, [global, id]);
    };

    self.tick = function(millis) {
      if (installed) {
        mockDate.tick(millis);
        delayedFunctionScheduler.tick(millis);
      } else {
        throw new Error('Mock clock is not installed, use jasmine.clock().install()');
      }
    };

    return self;

    function legacyIE() {
      //if these methods are polyfilled, apply will be present
      return !(realTimingFunctions.setTimeout || realTimingFunctions.setInterval).apply;
    }

    function replace(dest, source) {
      for (var prop in source) {
        dest[prop] = source[prop];
      }
    }

    function setTimeout(fn, delay) {
      return delayedFunctionScheduler.scheduleFunction(fn, delay, argSlice(arguments, 2));
    }

    function clearTimeout(id) {
      return delayedFunctionScheduler.removeFunctionWithId(id);
    }

    function setInterval(fn, interval) {
      return delayedFunctionScheduler.scheduleFunction(fn, interval, argSlice(arguments, 2), true);
    }

    function clearInterval(id) {
      return delayedFunctionScheduler.removeFunctionWithId(id);
    }

    function argSlice(argsObj, n) {
      return Array.prototype.slice.call(argsObj, n);
    }
  }

  return Clock;
};

getJasmineRequireObj().DelayedFunctionScheduler = function() {
  function DelayedFunctionScheduler() {
    var self = this;
    var scheduledLookup = [];
    var scheduledFunctions = {};
    var currentTime = 0;
    var delayedFnCount = 0;

    self.tick = function(millis) {
      millis = millis || 0;
      var endTime = currentTime + millis;

      runScheduledFunctions(endTime);
      currentTime = endTime;
    };

    self.scheduleFunction = function(funcToCall, millis, params, recurring, timeoutKey, runAtMillis) {
      var f;
      if (typeof(funcToCall) === 'string') {
        /* jshint evil: true */
        f = function() { return eval(funcToCall); };
        /* jshint evil: false */
      } else {
        f = funcToCall;
      }

      millis = millis || 0;
      timeoutKey = timeoutKey || ++delayedFnCount;
      runAtMillis = runAtMillis || (currentTime + millis);

      var funcToSchedule = {
        runAtMillis: runAtMillis,
        funcToCall: f,
        recurring: recurring,
        params: params,
        timeoutKey: timeoutKey,
        millis: millis
      };

      if (runAtMillis in scheduledFunctions) {
        scheduledFunctions[runAtMillis].push(funcToSchedule);
      } else {
        scheduledFunctions[runAtMillis] = [funcToSchedule];
        scheduledLookup.push(runAtMillis);
        scheduledLookup.sort(function (a, b) {
          return a - b;
        });
      }

      return timeoutKey;
    };

    self.removeFunctionWithId = function(timeoutKey) {
      for (var runAtMillis in scheduledFunctions) {
        var funcs = scheduledFunctions[runAtMillis];
        var i = indexOfFirstToPass(funcs, function (func) {
          return func.timeoutKey === timeoutKey;
        });

        if (i > -1) {
          if (funcs.length === 1) {
            delete scheduledFunctions[runAtMillis];
            deleteFromLookup(runAtMillis);
          } else {
            funcs.splice(i, 1);
          }

          // intervals get rescheduled when executed, so there's never more
          // than a single scheduled function with a given timeoutKey
          break;
        }
      }
    };

    self.reset = function() {
      currentTime = 0;
      scheduledLookup = [];
      scheduledFunctions = {};
      delayedFnCount = 0;
    };

    return self;

    function indexOfFirstToPass(array, testFn) {
      var index = -1;

      for (var i = 0; i < array.length; ++i) {
        if (testFn(array[i])) {
          index = i;
          break;
        }
      }

      return index;
    }

    function deleteFromLookup(key) {
      var value = Number(key);
      var i = indexOfFirstToPass(scheduledLookup, function (millis) {
        return millis === value;
      });

      if (i > -1) {
        scheduledLookup.splice(i, 1);
      }
    }

    function reschedule(scheduledFn) {
      self.scheduleFunction(scheduledFn.funcToCall,
        scheduledFn.millis,
        scheduledFn.params,
        true,
        scheduledFn.timeoutKey,
        scheduledFn.runAtMillis + scheduledFn.millis);
    }

    function runScheduledFunctions(endTime) {
      if (scheduledLookup.length === 0 || scheduledLookup[0] > endTime) {
        return;
      }

      do {
        currentTime = scheduledLookup.shift();

        var funcsToRun = scheduledFunctions[currentTime];
        delete scheduledFunctions[currentTime];

        for (var i = 0; i < funcsToRun.length; ++i) {
          var funcToRun = funcsToRun[i];
          funcToRun.funcToCall.apply(null, funcToRun.params || []);

          if (funcToRun.recurring) {
            reschedule(funcToRun);
          }
        }
      } while (scheduledLookup.length > 0 &&
              // checking first if we're out of time prevents setTimeout(0)
              // scheduled in a funcToRun from forcing an extra iteration
                 currentTime !== endTime  &&
                 scheduledLookup[0] <= endTime);
    }
  }

  return DelayedFunctionScheduler;
};

getJasmineRequireObj().ExceptionFormatter = function() {
  function ExceptionFormatter() {
    this.message = function(error) {
      var message = '';

      if (error.name && error.message) {
        message += error.name + ': ' + error.message;
      } else {
        message += error.toString() + ' thrown';
      }

      if (error.fileName || error.sourceURL) {
        message += ' in ' + (error.fileName || error.sourceURL);
      }

      if (error.line || error.lineNumber) {
        message += ' (line ' + (error.line || error.lineNumber) + ')';
      }

      return message;
    };

    this.stack = function(error) {
      return error ? error.stack : null;
    };
  }

  return ExceptionFormatter;
};

getJasmineRequireObj().Expectation = function() {

  var matchers = {};

  function Expectation(options) {
    this.util = options.util || { buildFailureMessage: function() {} };
    this.customEqualityTesters = options.customEqualityTesters || [];
    this.actual = options.actual;
    this.addExpectationResult = options.addExpectationResult || function(){};
    this.isNot = options.isNot;

    for (var matcherName in matchers) {
      this[matcherName] = matchers[matcherName];
    }
  }

  Expectation.prototype.wrapCompare = function(name, matcherFactory) {
    return function() {
      var args = Array.prototype.slice.call(arguments, 0),
        expected = args.slice(0),
        message = '';

      args.unshift(this.actual);

      var matcher = matcherFactory(this.util, this.customEqualityTesters),
          matcherCompare = matcher.compare;

      function defaultNegativeCompare() {
        var result = matcher.compare.apply(null, args);
        result.pass = !result.pass;
        return result;
      }

      if (this.isNot) {
        matcherCompare = matcher.negativeCompare || defaultNegativeCompare;
      }

      var result = matcherCompare.apply(null, args);

      if (!result.pass) {
        if (!result.message) {
          args.unshift(this.isNot);
          args.unshift(name);
          message = this.util.buildFailureMessage.apply(null, args);
        } else {
          if (Object.prototype.toString.apply(result.message) === '[object Function]') {
            message = result.message();
          } else {
            message = result.message;
          }
        }
      }

      if (expected.length == 1) {
        expected = expected[0];
      }

      // TODO: how many of these params are needed?
      this.addExpectationResult(
        result.pass,
        {
          matcherName: name,
          passed: result.pass,
          message: message,
          actual: this.actual,
          expected: expected // TODO: this may need to be arrayified/sliced
        }
      );
    };
  };

  Expectation.addCoreMatchers = function(matchers) {
    var prototype = Expectation.prototype;
    for (var matcherName in matchers) {
      var matcher = matchers[matcherName];
      prototype[matcherName] = prototype.wrapCompare(matcherName, matcher);
    }
  };

  Expectation.addMatchers = function(matchersToAdd) {
    for (var name in matchersToAdd) {
      var matcher = matchersToAdd[name];
      matchers[name] = Expectation.prototype.wrapCompare(name, matcher);
    }
  };

  Expectation.resetMatchers = function() {
    for (var name in matchers) {
      delete matchers[name];
    }
  };

  Expectation.Factory = function(options) {
    options = options || {};

    var expect = new Expectation(options);

    // TODO: this would be nice as its own Object - NegativeExpectation
    // TODO: copy instead of mutate options
    options.isNot = true;
    expect.not = new Expectation(options);

    return expect;
  };

  return Expectation;
};

//TODO: expectation result may make more sense as a presentation of an expectation.
getJasmineRequireObj().buildExpectationResult = function() {
  function buildExpectationResult(options) {
    var messageFormatter = options.messageFormatter || function() {},
      stackFormatter = options.stackFormatter || function() {};

    return {
      matcherName: options.matcherName,
      expected: options.expected,
      actual: options.actual,
      message: message(),
      stack: stack(),
      passed: options.passed
    };

    function message() {
      if (options.passed) {
        return 'Passed.';
      } else if (options.message) {
        return options.message;
      } else if (options.error) {
        return messageFormatter(options.error);
      }
      return '';
    }

    function stack() {
      if (options.passed) {
        return '';
      }

      var error = options.error;
      if (!error) {
        try {
          throw new Error(message());
        } catch (e) {
          error = e;
        }
      }
      return stackFormatter(error);
    }
  }

  return buildExpectationResult;
};

getJasmineRequireObj().MockDate = function() {
  function MockDate(global) {
    var self = this;
    var currentTime = 0;

    if (!global || !global.Date) {
      self.install = function() {};
      self.tick = function() {};
      self.uninstall = function() {};
      return self;
    }

    var GlobalDate = global.Date;

    self.install = function(mockDate) {
      if (mockDate instanceof GlobalDate) {
        currentTime = mockDate.getTime();
      } else {
        currentTime = new GlobalDate().getTime();
      }

      global.Date = FakeDate;
    };

    self.tick = function(millis) {
      millis = millis || 0;
      currentTime = currentTime + millis;
    };

    self.uninstall = function() {
      currentTime = 0;
      global.Date = GlobalDate;
    };

    createDateProperties();

    return self;

    function FakeDate() {
      switch(arguments.length) {
        case 0:
          return new GlobalDate(currentTime);
        case 1:
          return new GlobalDate(arguments[0]);
        case 2:
          return new GlobalDate(arguments[0], arguments[1]);
        case 3:
          return new GlobalDate(arguments[0], arguments[1], arguments[2]);
        case 4:
          return new GlobalDate(arguments[0], arguments[1], arguments[2], arguments[3]);
        case 5:
          return new GlobalDate(arguments[0], arguments[1], arguments[2], arguments[3],
                                arguments[4]);
        case 6:
          return new GlobalDate(arguments[0], arguments[1], arguments[2], arguments[3],
                                arguments[4], arguments[5]);
        case 7:
          return new GlobalDate(arguments[0], arguments[1], arguments[2], arguments[3],
                                arguments[4], arguments[5], arguments[6]);
      }
    }

    function createDateProperties() {

      FakeDate.now = function() {
        if (GlobalDate.now) {
          return currentTime;
        } else {
          throw new Error('Browser does not support Date.now()');
        }
      };

      FakeDate.toSource = GlobalDate.toSource;
      FakeDate.toString = GlobalDate.toString;
      FakeDate.parse = GlobalDate.parse;
      FakeDate.UTC = GlobalDate.UTC;
    }
	}

  return MockDate;
};

getJasmineRequireObj().ObjectContaining = function(j$) {

  function ObjectContaining(sample) {
    this.sample = sample;
  }

  ObjectContaining.prototype.jasmineMatches = function(other, mismatchKeys, mismatchValues) {
    if (typeof(this.sample) !== 'object') { throw new Error('You must provide an object to objectContaining, not \''+this.sample+'\'.'); }

    mismatchKeys = mismatchKeys || [];
    mismatchValues = mismatchValues || [];

    var hasKey = function(obj, keyName) {
      return obj !== null && !j$.util.isUndefined(obj[keyName]);
    };

    for (var property in this.sample) {
      if (!hasKey(other, property) && hasKey(this.sample, property)) {
        mismatchKeys.push('expected has key \'' + property + '\', but missing from actual.');
      }
      else if (!j$.matchersUtil.equals(other[property], this.sample[property])) {
        mismatchValues.push('\'' + property + '\' was \'' + (other[property] ? j$.util.htmlEscape(other[property].toString()) : other[property]) + '\' in actual, but was \'' + (this.sample[property] ? j$.util.htmlEscape(this.sample[property].toString()) : this.sample[property]) + '\' in expected.');
      }
    }

    return (mismatchKeys.length === 0 && mismatchValues.length === 0);
  };

  ObjectContaining.prototype.jasmineToString = function() {
    return '<jasmine.objectContaining(' + j$.pp(this.sample) + ')>';
  };

  return ObjectContaining;
};

getJasmineRequireObj().pp = function(j$) {

  function PrettyPrinter() {
    this.ppNestLevel_ = 0;
    this.seen = [];
  }

  PrettyPrinter.prototype.format = function(value) {
    this.ppNestLevel_++;
    try {
      if (j$.util.isUndefined(value)) {
        this.emitScalar('undefined');
      } else if (value === null) {
        this.emitScalar('null');
      } else if (value === 0 && 1/value === -Infinity) {
        this.emitScalar('-0');
      } else if (value === j$.getGlobal()) {
        this.emitScalar('<global>');
      } else if (value.jasmineToString) {
        this.emitScalar(value.jasmineToString());
      } else if (typeof value === 'string') {
        this.emitString(value);
      } else if (j$.isSpy(value)) {
        this.emitScalar('spy on ' + value.and.identity());
      } else if (value instanceof RegExp) {
        this.emitScalar(value.toString());
      } else if (typeof value === 'function') {
        this.emitScalar('Function');
      } else if (typeof value.nodeType === 'number') {
        this.emitScalar('HTMLNode');
      } else if (value instanceof Date) {
        this.emitScalar('Date(' + value + ')');
      } else if (j$.util.arrayContains(this.seen, value)) {
        this.emitScalar('<circular reference: ' + (j$.isArray_(value) ? 'Array' : 'Object') + '>');
      } else if (j$.isArray_(value) || j$.isA_('Object', value)) {
        this.seen.push(value);
        if (j$.isArray_(value)) {
          this.emitArray(value);
        } else {
          this.emitObject(value);
        }
        this.seen.pop();
      } else {
        this.emitScalar(value.toString());
      }
    } finally {
      this.ppNestLevel_--;
    }
  };

  PrettyPrinter.prototype.iterateObject = function(obj, fn) {
    for (var property in obj) {
      if (!Object.prototype.hasOwnProperty.call(obj, property)) { continue; }
      fn(property, obj.__lookupGetter__ ? (!j$.util.isUndefined(obj.__lookupGetter__(property)) &&
          obj.__lookupGetter__(property) !== null) : false);
    }
  };

  PrettyPrinter.prototype.emitArray = j$.unimplementedMethod_;
  PrettyPrinter.prototype.emitObject = j$.unimplementedMethod_;
  PrettyPrinter.prototype.emitScalar = j$.unimplementedMethod_;
  PrettyPrinter.prototype.emitString = j$.unimplementedMethod_;

  function StringPrettyPrinter() {
    PrettyPrinter.call(this);

    this.string = '';
  }

  j$.util.inherit(StringPrettyPrinter, PrettyPrinter);

  StringPrettyPrinter.prototype.emitScalar = function(value) {
    this.append(value);
  };

  StringPrettyPrinter.prototype.emitString = function(value) {
    this.append('\'' + value + '\'');
  };

  StringPrettyPrinter.prototype.emitArray = function(array) {
    if (this.ppNestLevel_ > j$.MAX_PRETTY_PRINT_DEPTH) {
      this.append('Array');
      return;
    }
    var length = Math.min(array.length, j$.MAX_PRETTY_PRINT_ARRAY_LENGTH);
    this.append('[ ');
    for (var i = 0; i < length; i++) {
      if (i > 0) {
        this.append(', ');
      }
      this.format(array[i]);
    }
    if(array.length > length){
      this.append(', ...');
    }
    this.append(' ]');
  };

  StringPrettyPrinter.prototype.emitObject = function(obj) {
    if (this.ppNestLevel_ > j$.MAX_PRETTY_PRINT_DEPTH) {
      this.append('Object');
      return;
    }

    var self = this;
    this.append('{ ');
    var first = true;

    this.iterateObject(obj, function(property, isGetter) {
      if (first) {
        first = false;
      } else {
        self.append(', ');
      }

      self.append(property);
      self.append(': ');
      if (isGetter) {
        self.append('<getter>');
      } else {
        self.format(obj[property]);
      }
    });

    this.append(' }');
  };

  StringPrettyPrinter.prototype.append = function(value) {
    this.string += value;
  };

  return function(value) {
    var stringPrettyPrinter = new StringPrettyPrinter();
    stringPrettyPrinter.format(value);
    return stringPrettyPrinter.string;
  };
};

getJasmineRequireObj().QueueRunner = function(j$) {

  function once(fn) {
    var called = false;
    return function() {
      if (!called) {
        called = true;
        fn();
      }
    };
  }

  function QueueRunner(attrs) {
    this.fns = attrs.fns || [];
    this.onComplete = attrs.onComplete || function() {};
    this.clearStack = attrs.clearStack || function(fn) {fn();};
    this.onException = attrs.onException || function() {};
    this.catchException = attrs.catchException || function() { return true; };
    this.enforceTimeout = attrs.enforceTimeout || function() { return false; };
    this.userContext = {};
    this.timer = attrs.timeout || {setTimeout: setTimeout, clearTimeout: clearTimeout};
  }

  QueueRunner.prototype.execute = function() {
    this.run(this.fns, 0);
  };

  QueueRunner.prototype.run = function(fns, recursiveIndex) {
    var length = fns.length,
        self = this,
        iterativeIndex;

    for(iterativeIndex = recursiveIndex; iterativeIndex < length; iterativeIndex++) {
      var fn = fns[iterativeIndex];
      if (fn.length > 0) {
        return attemptAsync(fn);
      } else {
        attemptSync(fn);
      }
    }

    var runnerDone = iterativeIndex >= length;

    if (runnerDone) {
      this.clearStack(this.onComplete);
    }

    function attemptSync(fn) {
      try {
        fn.call(self.userContext);
      } catch (e) {
        handleException(e);
      }
    }

    function attemptAsync(fn) {
      var clearTimeout = function () {
          Function.prototype.apply.apply(self.timer.clearTimeout, [j$.getGlobal(), [timeoutId]]);
        },
        next = once(function () {
          clearTimeout(timeoutId);
          self.run(fns, iterativeIndex + 1);
        }),
        timeoutId;

      if (self.enforceTimeout()) {
        timeoutId = Function.prototype.apply.apply(self.timer.setTimeout, [j$.getGlobal(), [function() {
          self.onException(new Error('Timeout - Async callback was not invoked within timeout specified by jasmine.DEFAULT_TIMEOUT_INTERVAL.'));
          next();
        }, j$.DEFAULT_TIMEOUT_INTERVAL]]);
      }

      try {
        fn.call(self.userContext, next);
      } catch (e) {
        handleException(e);
        next();
      }
    }

    function handleException(e) {
      self.onException(e);
      if (!self.catchException(e)) {
        //TODO: set a var when we catch an exception and
        //use a finally block to close the loop in a nice way..
        throw e;
      }
    }
  };

  return QueueRunner;
};

getJasmineRequireObj().ReportDispatcher = function() {
  function ReportDispatcher(methods) {

    var dispatchedMethods = methods || [];

    for (var i = 0; i < dispatchedMethods.length; i++) {
      var method = dispatchedMethods[i];
      this[method] = (function(m) {
        return function() {
          dispatch(m, arguments);
        };
      }(method));
    }

    var reporters = [];

    this.addReporter = function(reporter) {
      reporters.push(reporter);
    };

    return this;

    function dispatch(method, args) {
      for (var i = 0; i < reporters.length; i++) {
        var reporter = reporters[i];
        if (reporter[method]) {
          reporter[method].apply(reporter, args);
        }
      }
    }
  }

  return ReportDispatcher;
};


getJasmineRequireObj().SpyStrategy = function() {

  function SpyStrategy(options) {
    options = options || {};

    var identity = options.name || 'unknown',
        originalFn = options.fn || function() {},
        getSpy = options.getSpy || function() {},
        plan = function() {};

    this.identity = function() {
      return identity;
    };

    this.exec = function() {
      return plan.apply(this, arguments);
    };

    this.callThrough = function() {
      plan = originalFn;
      return getSpy();
    };

    this.returnValue = function(value) {
      plan = function() {
        return value;
      };
      return getSpy();
    };

    this.throwError = function(something) {
      var error = (something instanceof Error) ? something : new Error(something);
      plan = function() {
        throw error;
      };
      return getSpy();
    };

    this.callFake = function(fn) {
      plan = fn;
      return getSpy();
    };

    this.stub = function(fn) {
      plan = function() {};
      return getSpy();
    };
  }

  return SpyStrategy;
};

getJasmineRequireObj().Suite = function() {
  function Suite(attrs) {
    this.env = attrs.env;
    this.id = attrs.id;
    this.parentSuite = attrs.parentSuite;
    this.description = attrs.description;
    this.onStart = attrs.onStart || function() {};
    this.resultCallback = attrs.resultCallback || function() {};
    this.clearStack = attrs.clearStack || function(fn) {fn();};

    this.beforeFns = [];
    this.afterFns = [];
    this.queueRunner = attrs.queueRunner || function() {};
    this.disabled = false;

    this.children = [];

    this.result = {
      id: this.id,
      status: this.disabled ? 'disabled' : '',
      description: this.description,
      fullName: this.getFullName()
    };
  }

  Suite.prototype.getFullName = function() {
    var fullName = this.description;
    for (var parentSuite = this.parentSuite; parentSuite; parentSuite = parentSuite.parentSuite) {
      if (parentSuite.parentSuite) {
        fullName = parentSuite.description + ' ' + fullName;
      }
    }
    return fullName;
  };

  Suite.prototype.disable = function() {
    this.disabled = true;
    this.result.status = 'disabled';
  };

  Suite.prototype.beforeEach = function(fn) {
    this.beforeFns.unshift(fn);
  };

  Suite.prototype.afterEach = function(fn) {
    this.afterFns.unshift(fn);
  };

  Suite.prototype.addChild = function(child) {
    this.children.push(child);
  };

  Suite.prototype.execute = function(onComplete) {
    var self = this;

    this.onStart(this);

    if (this.disabled) {
      complete();
      return;
    }

    var allFns = [];

    for (var i = 0; i < this.children.length; i++) {
      allFns.push(wrapChildAsAsync(this.children[i]));
    }

    this.queueRunner({
      fns: allFns,
      onComplete: complete
    });

    function complete() {
      self.resultCallback(self.result);

      if (onComplete) {
        onComplete();
      }
    }

    function wrapChildAsAsync(child) {
      return function(done) { child.execute(done); };
    }
  };

  return Suite;
};

if (typeof window == void 0 && typeof exports == 'object') {
  exports.Suite = jasmineRequire.Suite;
}

getJasmineRequireObj().Timer = function() {
  var defaultNow = (function(Date) {
    return function() { return new Date().getTime(); };
  })(Date);

  function Timer(options) {
    options = options || {};

    var now = options.now || defaultNow,
      startTime;

    this.start = function() {
      startTime = now();
    };

    this.elapsed = function() {
      return now() - startTime;
    };
  }

  return Timer;
};

getJasmineRequireObj().matchersUtil = function(j$) {
  // TODO: what to do about jasmine.pp not being inject? move to JSON.stringify? gut PrettyPrinter?

  return {
    equals: function(a, b, customTesters) {
      customTesters = customTesters || [];

      return eq(a, b, [], [], customTesters);
    },

    contains: function(haystack, needle, customTesters) {
      customTesters = customTesters || [];

      if (Object.prototype.toString.apply(haystack) === '[object Array]') {
        for (var i = 0; i < haystack.length; i++) {
          if (eq(haystack[i], needle, [], [], customTesters)) {
            return true;
          }
        }
        return false;
      }
      return !!haystack && haystack.indexOf(needle) >= 0;
    },

    buildFailureMessage: function() {
      var args = Array.prototype.slice.call(arguments, 0),
        matcherName = args[0],
        isNot = args[1],
        actual = args[2],
        expected = args.slice(3),
        englishyPredicate = matcherName.replace(/[A-Z]/g, function(s) { return ' ' + s.toLowerCase(); });

      var message = 'Expected ' +
        j$.pp(actual) +
        (isNot ? ' not ' : ' ') +
        englishyPredicate;

      if (expected.length > 0) {
        for (var i = 0; i < expected.length; i++) {
          if (i > 0) {
            message += ',';
          }
          message += ' ' + j$.pp(expected[i]);
        }
      }

      return message + '.';
    }
  };

  // Equality function lovingly adapted from isEqual in
  //   [Underscore](http://underscorejs.org)
  function eq(a, b, aStack, bStack, customTesters) {
    var result = true;

    for (var i = 0; i < customTesters.length; i++) {
      var customTesterResult = customTesters[i](a, b);
      if (!j$.util.isUndefined(customTesterResult)) {
        return customTesterResult;
      }
    }

    if (a instanceof j$.Any) {
      result = a.jasmineMatches(b);
      if (result) {
        return true;
      }
    }

    if (b instanceof j$.Any) {
      result = b.jasmineMatches(a);
      if (result) {
        return true;
      }
    }

    if (b instanceof j$.ObjectContaining) {
      result = b.jasmineMatches(a);
      if (result) {
        return true;
      }
    }

    if (a instanceof Error && b instanceof Error) {
      return a.message == b.message;
    }

    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) { return a !== 0 || 1 / a == 1 / b; }
    // A strict comparison is necessary because `null == undefined`.
    if (a === null || b === null) { return a === b; }
    var className = Object.prototype.toString.call(a);
    if (className != Object.prototype.toString.call(b)) { return false; }
    switch (className) {
      // Strings, numbers, dates, and booleans are compared by value.
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return a == String(b);
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive. An `egal` comparison is performed for
        // other numeric values.
        return a != +a ? b != +b : (a === 0 ? 1 / a == 1 / b : a == +b);
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a == +b;
      // RegExps are compared by their source patterns and flags.
      case '[object RegExp]':
        return a.source == b.source &&
          a.global == b.global &&
          a.multiline == b.multiline &&
          a.ignoreCase == b.ignoreCase;
    }
    if (typeof a != 'object' || typeof b != 'object') { return false; }
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] == a) { return bStack[length] == b; }
    }
    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);
    var size = 0;
    // Recursively compare objects and arrays.
    if (className == '[object Array]') {
      // Compare array lengths to determine if a deep comparison is necessary.
      size = a.length;
      result = size == b.length;
      if (result) {
        // Deep compare the contents, ignoring non-numeric properties.
        while (size--) {
          if (!(result = eq(a[size], b[size], aStack, bStack, customTesters))) { break; }
        }
      }
    } else {
      // Objects with different constructors are not equivalent, but `Object`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(isFunction(aCtor) && (aCtor instanceof aCtor) &&
        isFunction(bCtor) && (bCtor instanceof bCtor))) {
        return false;
      }
      // Deep compare objects.
      for (var key in a) {
        if (has(a, key)) {
          // Count the expected number of properties.
          size++;
          // Deep compare each member.
          if (!(result = has(b, key) && eq(a[key], b[key], aStack, bStack, customTesters))) { break; }
        }
      }
      // Ensure that both objects contain the same number of properties.
      if (result) {
        for (key in b) {
          if (has(b, key) && !(size--)) { break; }
        }
        result = !size;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();

    return result;

    function has(obj, key) {
      return obj.hasOwnProperty(key);
    }

    function isFunction(obj) {
      return typeof obj === 'function';
    }
  }
};

getJasmineRequireObj().toBe = function() {
  function toBe() {
    return {
      compare: function(actual, expected) {
        return {
          pass: actual === expected
        };
      }
    };
  }

  return toBe;
};

getJasmineRequireObj().toBeCloseTo = function() {

  function toBeCloseTo() {
    return {
      compare: function(actual, expected, precision) {
        if (precision !== 0) {
          precision = precision || 2;
        }

        return {
          pass: Math.abs(expected - actual) < (Math.pow(10, -precision) / 2)
        };
      }
    };
  }

  return toBeCloseTo;
};

getJasmineRequireObj().toBeDefined = function() {
  function toBeDefined() {
    return {
      compare: function(actual) {
        return {
          pass: (void 0 !== actual)
        };
      }
    };
  }

  return toBeDefined;
};

getJasmineRequireObj().toBeFalsy = function() {
  function toBeFalsy() {
    return {
      compare: function(actual) {
        return {
          pass: !!!actual
        };
      }
    };
  }

  return toBeFalsy;
};

getJasmineRequireObj().toBeGreaterThan = function() {

  function toBeGreaterThan() {
    return {
      compare: function(actual, expected) {
        return {
          pass: actual > expected
        };
      }
    };
  }

  return toBeGreaterThan;
};


getJasmineRequireObj().toBeLessThan = function() {
  function toBeLessThan() {
    return {

      compare: function(actual, expected) {
        return {
          pass: actual < expected
        };
      }
    };
  }

  return toBeLessThan;
};
getJasmineRequireObj().toBeNaN = function(j$) {

  function toBeNaN() {
    return {
      compare: function(actual) {
        var result = {
          pass: (actual !== actual)
        };

        if (result.pass) {
          result.message = 'Expected actual not to be NaN.';
        } else {
          result.message = function() { return 'Expected ' + j$.pp(actual) + ' to be NaN.'; };
        }

        return result;
      }
    };
  }

  return toBeNaN;
};

getJasmineRequireObj().toBeNull = function() {

  function toBeNull() {
    return {
      compare: function(actual) {
        return {
          pass: actual === null
        };
      }
    };
  }

  return toBeNull;
};

getJasmineRequireObj().toBeTruthy = function() {

  function toBeTruthy() {
    return {
      compare: function(actual) {
        return {
          pass: !!actual
        };
      }
    };
  }

  return toBeTruthy;
};

getJasmineRequireObj().toBeUndefined = function() {

  function toBeUndefined() {
    return {
      compare: function(actual) {
        return {
          pass: void 0 === actual
        };
      }
    };
  }

  return toBeUndefined;
};

getJasmineRequireObj().toContain = function() {
  function toContain(util, customEqualityTesters) {
    customEqualityTesters = customEqualityTesters || [];

    return {
      compare: function(actual, expected) {

        return {
          pass: util.contains(actual, expected, customEqualityTesters)
        };
      }
    };
  }

  return toContain;
};

getJasmineRequireObj().toEqual = function() {

  function toEqual(util, customEqualityTesters) {
    customEqualityTesters = customEqualityTesters || [];

    return {
      compare: function(actual, expected) {
        var result = {
          pass: false
        };

        result.pass = util.equals(actual, expected, customEqualityTesters);

        return result;
      }
    };
  }

  return toEqual;
};

getJasmineRequireObj().toHaveBeenCalled = function(j$) {

  function toHaveBeenCalled() {
    return {
      compare: function(actual) {
        var result = {};

        if (!j$.isSpy(actual)) {
          throw new Error('Expected a spy, but got ' + j$.pp(actual) + '.');
        }

        if (arguments.length > 1) {
          throw new Error('toHaveBeenCalled does not take arguments, use toHaveBeenCalledWith');
        }

        result.pass = actual.calls.any();

        result.message = result.pass ?
          'Expected spy ' + actual.and.identity() + ' not to have been called.' :
          'Expected spy ' + actual.and.identity() + ' to have been called.';

        return result;
      }
    };
  }

  return toHaveBeenCalled;
};

getJasmineRequireObj().toHaveBeenCalledWith = function(j$) {

  function toHaveBeenCalledWith(util, customEqualityTesters) {
    return {
      compare: function() {
        var args = Array.prototype.slice.call(arguments, 0),
          actual = args[0],
          expectedArgs = args.slice(1),
          result = { pass: false };

        if (!j$.isSpy(actual)) {
          throw new Error('Expected a spy, but got ' + j$.pp(actual) + '.');
        }

        if (!actual.calls.any()) {
          result.message = function() { return 'Expected spy ' + actual.and.identity() + ' to have been called with ' + j$.pp(expectedArgs) + ' but it was never called.'; };
          return result;
        }

        if (util.contains(actual.calls.allArgs(), expectedArgs, customEqualityTesters)) {
          result.pass = true;
          result.message = function() { return 'Expected spy ' + actual.and.identity() + ' not to have been called with ' + j$.pp(expectedArgs) + ' but it was.'; };
        } else {
          result.message = function() { return 'Expected spy ' + actual.and.identity() + ' to have been called with ' + j$.pp(expectedArgs) + ' but actual calls were ' + j$.pp(actual.calls.allArgs()).replace(/^\[ | \]$/g, '') + '.'; };
        }

        return result;
      }
    };
  }

  return toHaveBeenCalledWith;
};

getJasmineRequireObj().toMatch = function() {

  function toMatch() {
    return {
      compare: function(actual, expected) {
        var regexp = new RegExp(expected);

        return {
          pass: regexp.test(actual)
        };
      }
    };
  }

  return toMatch;
};

getJasmineRequireObj().toThrow = function(j$) {

  function toThrow(util) {
    return {
      compare: function(actual, expected) {
        var result = { pass: false },
          threw = false,
          thrown;

        if (typeof actual != 'function') {
          throw new Error('Actual is not a Function');
        }

        try {
          actual();
        } catch (e) {
          threw = true;
          thrown = e;
        }

        if (!threw) {
          result.message = 'Expected function to throw an exception.';
          return result;
        }

        if (arguments.length == 1) {
          result.pass = true;
          result.message = function() { return 'Expected function not to throw, but it threw ' + j$.pp(thrown) + '.'; };

          return result;
        }

        if (util.equals(thrown, expected)) {
          result.pass = true;
          result.message = function() { return 'Expected function not to throw ' + j$.pp(expected) + '.'; };
        } else {
          result.message = function() { return 'Expected function to throw ' + j$.pp(expected) + ', but it threw ' +  j$.pp(thrown) + '.'; };
        }

        return result;
      }
    };
  }

  return toThrow;
};

getJasmineRequireObj().toThrowError = function(j$) {
  function toThrowError (util) {
    return {
      compare: function(actual) {
        var threw = false,
          pass = {pass: true},
          fail = {pass: false},
          thrown,
          errorType,
          message,
          regexp,
          name,
          constructorName;

        if (typeof actual != 'function') {
          throw new Error('Actual is not a Function');
        }

        extractExpectedParams.apply(null, arguments);

        try {
          actual();
        } catch (e) {
          threw = true;
          thrown = e;
        }

        if (!threw) {
          fail.message = 'Expected function to throw an Error.';
          return fail;
        }

        if (!(thrown instanceof Error)) {
          fail.message = function() { return 'Expected function to throw an Error, but it threw ' + j$.pp(thrown) + '.'; };
          return fail;
        }

        if (arguments.length == 1) {
          pass.message = 'Expected function not to throw an Error, but it threw ' + fnNameFor(thrown) + '.';
          return pass;
        }

        if (errorType) {
          name = fnNameFor(errorType);
          constructorName = fnNameFor(thrown.constructor);
        }

        if (errorType && message) {
          if (thrown.constructor == errorType && util.equals(thrown.message, message)) {
            pass.message = function() { return 'Expected function not to throw ' + name + ' with message ' + j$.pp(message) + '.'; };
            return pass;
          } else {
            fail.message = function() { return 'Expected function to throw ' + name + ' with message ' + j$.pp(message) +
              ', but it threw ' + constructorName + ' with message ' + j$.pp(thrown.message) + '.'; };
            return fail;
          }
        }

        if (errorType && regexp) {
          if (thrown.constructor == errorType && regexp.test(thrown.message)) {
            pass.message = function() { return 'Expected function not to throw ' + name + ' with message matching ' + j$.pp(regexp) + '.'; };
            return pass;
          } else {
            fail.message = function() { return 'Expected function to throw ' + name + ' with message matching ' + j$.pp(regexp) +
              ', but it threw ' + constructorName + ' with message ' + j$.pp(thrown.message) + '.'; };
            return fail;
          }
        }

        if (errorType) {
          if (thrown.constructor == errorType) {
            pass.message = 'Expected function not to throw ' + name + '.';
            return pass;
          } else {
            fail.message = 'Expected function to throw ' + name + ', but it threw ' + constructorName + '.';
            return fail;
          }
        }

        if (message) {
          if (thrown.message == message) {
            pass.message = function() { return 'Expected function not to throw an exception with message ' + j$.pp(message) + '.'; };
            return pass;
          } else {
            fail.message = function() { return 'Expected function to throw an exception with message ' + j$.pp(message) +
              ', but it threw an exception with message ' + j$.pp(thrown.message) + '.'; };
            return fail;
          }
        }

        if (regexp) {
          if (regexp.test(thrown.message)) {
            pass.message = function() { return 'Expected function not to throw an exception with a message matching ' + j$.pp(regexp) + '.'; };
            return pass;
          } else {
            fail.message = function() { return 'Expected function to throw an exception with a message matching ' + j$.pp(regexp) +
              ', but it threw an exception with message ' + j$.pp(thrown.message) + '.'; };
            return fail;
          }
        }

        function fnNameFor(func) {
            return func.name || func.toString().match(/^\s*function\s*(\w*)\s*\(/)[1];
        }

        function extractExpectedParams() {
          if (arguments.length == 1) {
            return;
          }

          if (arguments.length == 2) {
            var expected = arguments[1];

            if (expected instanceof RegExp) {
              regexp = expected;
            } else if (typeof expected == 'string') {
              message = expected;
            } else if (checkForAnErrorType(expected)) {
              errorType = expected;
            }

            if (!(errorType || message || regexp)) {
              throw new Error('Expected is not an Error, string, or RegExp.');
            }
          } else {
            if (checkForAnErrorType(arguments[1])) {
              errorType = arguments[1];
            } else {
              throw new Error('Expected error type is not an Error.');
            }

            if (arguments[2] instanceof RegExp) {
              regexp = arguments[2];
            } else if (typeof arguments[2] == 'string') {
              message = arguments[2];
            } else {
              throw new Error('Expected error message is not a string or RegExp.');
            }
          }
        }

        function checkForAnErrorType(type) {
          if (typeof type !== 'function') {
            return false;
          }

          var Surrogate = function() {};
          Surrogate.prototype = type.prototype;
          return (new Surrogate()) instanceof Error;
        }
      }
    };
  }

  return toThrowError;
};

getJasmineRequireObj().interface = function(jasmine, env) {
  var jasmineInterface = {
    describe: function(description, specDefinitions) {
      return env.describe(description, specDefinitions);
    },

    xdescribe: function(description, specDefinitions) {
      return env.xdescribe(description, specDefinitions);
    },

    it: function(desc, func) {
      return env.it(desc, func);
    },

    xit: function(desc, func) {
      return env.xit(desc, func);
    },

    beforeEach: function(beforeEachFunction) {
      return env.beforeEach(beforeEachFunction);
    },

    afterEach: function(afterEachFunction) {
      return env.afterEach(afterEachFunction);
    },

    expect: function(actual) {
      return env.expect(actual);
    },

    pending: function() {
      return env.pending();
    },

    spyOn: function(obj, methodName) {
      return env.spyOn(obj, methodName);
    },

    jsApiReporter: new jasmine.JsApiReporter({
      timer: new jasmine.Timer()
    }),

    jasmine: jasmine
  };

  jasmine.addCustomEqualityTester = function(tester) {
    env.addCustomEqualityTester(tester);
  };

  jasmine.addMatchers = function(matchers) {
    return env.addMatchers(matchers);
  };

  jasmine.clock = function() {
    return env.clock;
  };

  return jasmineInterface;
};

getJasmineRequireObj().version = function() {
  return '2.0.3';
};

/*
Copyright (c) 2008-2014 Pivotal Labs

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
jasmineRequire.html = function(j$) {
  j$.ResultsNode = jasmineRequire.ResultsNode();
  j$.HtmlReporter = jasmineRequire.HtmlReporter(j$);
  j$.QueryString = jasmineRequire.QueryString();
  j$.HtmlSpecFilter = jasmineRequire.HtmlSpecFilter();
};

jasmineRequire.HtmlReporter = function(j$) {

  var noopTimer = {
    start: function() {},
    elapsed: function() { return 0; }
  };

  function HtmlReporter(options) {
    var env = options.env || {},
      getContainer = options.getContainer,
      createElement = options.createElement,
      createTextNode = options.createTextNode,
      onRaiseExceptionsClick = options.onRaiseExceptionsClick || function() {},
      timer = options.timer || noopTimer,
      results = [],
      specsExecuted = 0,
      failureCount = 0,
      pendingSpecCount = 0,
      htmlReporterMain,
      symbols;

    this.initialize = function() {
      clearPrior();
      htmlReporterMain = createDom('div', {className: 'jasmine_html-reporter'},
        createDom('div', {className: 'banner'},
          createDom('a', {className: 'title', href: 'http://jasmine.github.io/', target: '_blank'}),
          createDom('span', {className: 'version'}, j$.version)
        ),
        createDom('ul', {className: 'symbol-summary'}),
        createDom('div', {className: 'alert'}),
        createDom('div', {className: 'results'},
          createDom('div', {className: 'failures'})
        )
      );
      getContainer().appendChild(htmlReporterMain);

      symbols = find('.symbol-summary');
    };

    var totalSpecsDefined;
    this.jasmineStarted = function(options) {
      totalSpecsDefined = options.totalSpecsDefined || 0;
      timer.start();
    };

    var summary = createDom('div', {className: 'summary'});

    var topResults = new j$.ResultsNode({}, '', null),
      currentParent = topResults;

    this.suiteStarted = function(result) {
      currentParent.addChild(result, 'suite');
      currentParent = currentParent.last();
    };

    this.suiteDone = function(result) {
      if (currentParent == topResults) {
        return;
      }

      currentParent = currentParent.parent;
    };

    this.specStarted = function(result) {
      currentParent.addChild(result, 'spec');
    };

    var failures = [];
    this.specDone = function(result) {
      if(noExpectations(result) && console && console.error) {
        console.error('Spec \'' + result.fullName + '\' has no expectations.');
      }

      if (result.status != 'disabled') {
        specsExecuted++;
      }

      symbols.appendChild(createDom('li', {
          className: noExpectations(result) ? 'empty' : result.status,
          id: 'spec_' + result.id,
          title: result.fullName
        }
      ));

      if (result.status == 'failed') {
        failureCount++;

        var failure =
          createDom('div', {className: 'spec-detail failed'},
            createDom('div', {className: 'description'},
              createDom('a', {title: result.fullName, href: specHref(result)}, result.fullName)
            ),
            createDom('div', {className: 'messages'})
          );
        var messages = failure.childNodes[1];

        for (var i = 0; i < result.failedExpectations.length; i++) {
          var expectation = result.failedExpectations[i];
          messages.appendChild(createDom('div', {className: 'result-message'}, expectation.message));
          messages.appendChild(createDom('div', {className: 'stack-trace'}, expectation.stack));
        }

        failures.push(failure);
      }

      if (result.status == 'pending') {
        pendingSpecCount++;
      }
    };

    this.jasmineDone = function() {
      var banner = find('.banner');
      banner.appendChild(createDom('span', {className: 'duration'}, 'finished in ' + timer.elapsed() / 1000 + 's'));

      var alert = find('.alert');

      alert.appendChild(createDom('span', { className: 'exceptions' },
        createDom('label', { className: 'label', 'for': 'raise-exceptions' }, 'raise exceptions'),
        createDom('input', {
          className: 'raise',
          id: 'raise-exceptions',
          type: 'checkbox'
        })
      ));
      var checkbox = find('#raise-exceptions');

      checkbox.checked = !env.catchingExceptions();
      checkbox.onclick = onRaiseExceptionsClick;

      if (specsExecuted < totalSpecsDefined) {
        var skippedMessage = 'Ran ' + specsExecuted + ' of ' + totalSpecsDefined + ' specs - run all';
        alert.appendChild(
          createDom('span', {className: 'bar skipped'},
            createDom('a', {href: '?', title: 'Run all specs'}, skippedMessage)
          )
        );
      }
      var statusBarMessage = '';
      var statusBarClassName = 'bar ';

      if (totalSpecsDefined > 0) {
        statusBarMessage += pluralize('spec', specsExecuted) + ', ' + pluralize('failure', failureCount);
        if (pendingSpecCount) { statusBarMessage += ', ' + pluralize('pending spec', pendingSpecCount); }
        statusBarClassName += (failureCount > 0) ? 'failed' : 'passed';
      } else {
        statusBarClassName += 'skipped';
        statusBarMessage += 'No specs found';
      }

      alert.appendChild(createDom('span', {className: statusBarClassName}, statusBarMessage));

      var results = find('.results');
      results.appendChild(summary);

      summaryList(topResults, summary);

      function summaryList(resultsTree, domParent) {
        var specListNode;
        for (var i = 0; i < resultsTree.children.length; i++) {
          var resultNode = resultsTree.children[i];
          if (resultNode.type == 'suite') {
            var suiteListNode = createDom('ul', {className: 'suite', id: 'suite-' + resultNode.result.id},
              createDom('li', {className: 'suite-detail'},
                createDom('a', {href: specHref(resultNode.result)}, resultNode.result.description)
              )
            );

            summaryList(resultNode, suiteListNode);
            domParent.appendChild(suiteListNode);
          }
          if (resultNode.type == 'spec') {
            if (domParent.getAttribute('class') != 'specs') {
              specListNode = createDom('ul', {className: 'specs'});
              domParent.appendChild(specListNode);
            }
            var specDescription = resultNode.result.description;
            if(noExpectations(resultNode.result)) {
              specDescription = 'SPEC HAS NO EXPECTATIONS ' + specDescription;
            }
            specListNode.appendChild(
              createDom('li', {
                  className: resultNode.result.status,
                  id: 'spec-' + resultNode.result.id
                },
                createDom('a', {href: specHref(resultNode.result)}, specDescription)
              )
            );
          }
        }
      }

      if (failures.length) {
        alert.appendChild(
          createDom('span', {className: 'menu bar spec-list'},
            createDom('span', {}, 'Spec List | '),
            createDom('a', {className: 'failures-menu', href: '#'}, 'Failures')));
        alert.appendChild(
          createDom('span', {className: 'menu bar failure-list'},
            createDom('a', {className: 'spec-list-menu', href: '#'}, 'Spec List'),
            createDom('span', {}, ' | Failures ')));

        find('.failures-menu').onclick = function() {
          setMenuModeTo('failure-list');
        };
        find('.spec-list-menu').onclick = function() {
          setMenuModeTo('spec-list');
        };

        setMenuModeTo('failure-list');

        var failureNode = find('.failures');
        for (var i = 0; i < failures.length; i++) {
          failureNode.appendChild(failures[i]);
        }
      }
    };

    return this;

    function find(selector) {
      return getContainer().querySelector('.jasmine_html-reporter ' + selector);
    }

    function clearPrior() {
      // return the reporter
      var oldReporter = find('');
      
      if(oldReporter) {
        getContainer().removeChild(oldReporter);
      }
    }

    function createDom(type, attrs, childrenVarArgs) {
      var el = createElement(type);

      for (var i = 2; i < arguments.length; i++) {
        var child = arguments[i];

        if (typeof child === 'string') {
          el.appendChild(createTextNode(child));
        } else {
          if (child) {
            el.appendChild(child);
          }
        }
      }

      for (var attr in attrs) {
        if (attr == 'className') {
          el[attr] = attrs[attr];
        } else {
          el.setAttribute(attr, attrs[attr]);
        }
      }

      return el;
    }

    function pluralize(singular, count) {
      var word = (count == 1 ? singular : singular + 's');

      return '' + count + ' ' + word;
    }

    function specHref(result) {
      return '?spec=' + encodeURIComponent(result.fullName);
    }

    function setMenuModeTo(mode) {
      htmlReporterMain.setAttribute('class', 'jasmine_html-reporter ' + mode);
    }

    function noExpectations(result) {
      return (result.failedExpectations.length + result.passedExpectations.length) === 0 &&
        result.status === 'passed';
    }
  }

  return HtmlReporter;
};

jasmineRequire.HtmlSpecFilter = function() {
  function HtmlSpecFilter(options) {
    var filterString = options && options.filterString() && options.filterString().replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    var filterPattern = new RegExp(filterString);

    this.matches = function(specName) {
      return filterPattern.test(specName);
    };
  }

  return HtmlSpecFilter;
};

jasmineRequire.ResultsNode = function() {
  function ResultsNode(result, type, parent) {
    this.result = result;
    this.type = type;
    this.parent = parent;

    this.children = [];

    this.addChild = function(result, type) {
      this.children.push(new ResultsNode(result, type, this));
    };

    this.last = function() {
      return this.children[this.children.length - 1];
    };
  }

  return ResultsNode;
};

jasmineRequire.QueryString = function() {
  function QueryString(options) {

    this.setParam = function(key, value) {
      var paramMap = queryStringToParamMap();
      paramMap[key] = value;
      options.getWindowLocation().search = toQueryString(paramMap);
    };

    this.getParam = function(key) {
      return queryStringToParamMap()[key];
    };

    return this;

    function toQueryString(paramMap) {
      var qStrPairs = [];
      for (var prop in paramMap) {
        qStrPairs.push(encodeURIComponent(prop) + '=' + encodeURIComponent(paramMap[prop]));
      }
      return '?' + qStrPairs.join('&');
    }

    function queryStringToParamMap() {
      var paramStr = options.getWindowLocation().search.substring(1),
        params = [],
        paramMap = {};

      if (paramStr.length > 0) {
        params = paramStr.split('&');
        for (var i = 0; i < params.length; i++) {
          var p = params[i].split('=');
          var value = decodeURIComponent(p[1]);
          if (value === 'true' || value === 'false') {
            value = JSON.parse(value);
          }
          paramMap[decodeURIComponent(p[0])] = value;
        }
      }

      return paramMap;
    }

  }

  return QueryString;
};

/**
 Starting with version 2.0, this file "boots" Jasmine, performing all of the necessary initialization before executing the loaded environment and all of a project's specs. This file should be loaded after `jasmine.js`, but before any project source files or spec files are loaded. Thus this file can also be used to customize Jasmine for a project.

 If a project is using Jasmine via the standalone distribution, this file can be customized directly. If a project is using Jasmine via the [Ruby gem][jasmine-gem], this file can be copied into the support directory via `jasmine copy_boot_js`. Other environments (e.g., Python) will have different mechanisms.

 The location of `boot.js` can be specified and/or overridden in `jasmine.yml`.

 [jasmine-gem]: http://github.com/pivotal/jasmine-gem
 */

(function() {

  /**
   * ## Require &amp; Instantiate
   *
   * Require Jasmine's core files. Specifically, this requires and attaches all of Jasmine's code to the `jasmine` reference.
   */
  window.jasmine = jasmineRequire.core(jasmineRequire);

  /**
   * Since this is being run in a browser and the results should populate to an HTML page, require the HTML-specific Jasmine code, injecting the same reference.
   */
  jasmineRequire.html(jasmine);

  /**
   * Create the Jasmine environment. This is used to run all specs in a project.
   */
  var env = jasmine.getEnv();

  /**
   * ## The Global Interface
   *
   * Build up the functions that will be exposed as the Jasmine public interface. A project can customize, rename or alias any of these functions as desired, provided the implementation remains unchanged.
   */
  var jasmineInterface = jasmineRequire.interface(jasmine, env);

  /**
   * Add all of the Jasmine global/public interface to the proper global, so a project can use the public interface directly. For example, calling `describe` in specs instead of `jasmine.getEnv().describe`.
   */
  if (typeof window == "undefined" && typeof exports == "object") {
    extend(exports, jasmineInterface);
  } else {
    extend(window, jasmineInterface);
  }

  /**
   * ## Runner Parameters
   *
   * More browser specific code - wrap the query string in an object and to allow for getting/setting parameters from the runner user interface.
   */

  var queryString = new jasmine.QueryString({
    getWindowLocation: function() { return window.location; }
  });

  var catchingExceptions = queryString.getParam("catch");
  env.catchExceptions(typeof catchingExceptions === "undefined" ? true : catchingExceptions);

  /**
   * ## Reporters
   * The `HtmlReporter` builds all of the HTML UI for the runner page. This reporter paints the dots, stars, and x's for specs, as well as all spec names and all failures (if any).
   */
  var htmlReporter = new jasmine.HtmlReporter({
    env: env,
    onRaiseExceptionsClick: function() { queryString.setParam("catch", !env.catchingExceptions()); },
    getContainer: function() { return document.body; },
    createElement: function() { return document.createElement.apply(document, arguments); },
    createTextNode: function() { return document.createTextNode.apply(document, arguments); },
    timer: new jasmine.Timer()
  });

  /**
   * The `jsApiReporter` also receives spec results, and is used by any environment that needs to extract the results  from JavaScript.
   */
  env.addReporter(jasmineInterface.jsApiReporter);
  env.addReporter(htmlReporter);

  /**
   * Filter which specs will be run by matching the start of the full name against the `spec` query param.
   */
  var specFilter = new jasmine.HtmlSpecFilter({
    filterString: function() { return queryString.getParam("spec"); }
  });

  env.specFilter = function(spec) {
    return specFilter.matches(spec.getFullName());
  };

  /**
   * Setting up timing functions to be able to be overridden. Certain browsers (Safari, IE 8, phantomjs) require this hack.
   */
  window.setTimeout = window.setTimeout;
  window.setInterval = window.setInterval;
  window.clearTimeout = window.clearTimeout;
  window.clearInterval = window.clearInterval;

  /**
   * ## Execution
   *
   * Replace the browser window's `onload`, ensure it's called, and then run all of the loaded specs. This includes initializing the `HtmlReporter` instance and then executing the loaded Jasmine environment. All of this will happen after all of the specs are loaded.
   */
  var currentWindowOnload = window.onload;

  window.onload = function() {
    if (currentWindowOnload) {
      currentWindowOnload();
    }
    htmlReporter.initialize();
    env.execute();
  };

  /**
   * Helper function for readability above.
   */
  function extend(destination, source) {
    for (var property in source) destination[property] = source[property];
    return destination;
  }

}());

if (!Function.prototype.bind) {

    Function.prototype.bind = function(oThis) {

        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            EmptyFn = function() {},
            fBound = function() {
                return fToBind.apply(this instanceof EmptyFn ? this : oThis || window,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        EmptyFn.prototype = this.prototype;
        fBound.prototype = new EmptyFn();

        return fBound;
    };

}
/*!

 handlebars v1.3.0

Copyright (C) 2011 by Yehuda Katz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@license
*/
var Handlebars = (function() {
// handlebars/safe-string.js
var __module3__ = (function() {
  "use strict";
  var __exports__;
  // Build out our basic SafeString type
  function SafeString(string) {
    this.string = string;
  }

  SafeString.prototype.toString = function() {
    return "" + this.string;
  };

  __exports__ = SafeString;
  return __exports__;
})();

// handlebars/utils.js
var __module2__ = (function(__dependency1__) {
  "use strict";
  var __exports__ = {};
  /*jshint -W004 */
  var SafeString = __dependency1__;

  var escape = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#x27;",
    "`": "&#x60;"
  };

  var badChars = /[&<>"'`]/g;
  var possible = /[&<>"'`]/;

  function escapeChar(chr) {
    return escape[chr] || "&amp;";
  }

  function extend(obj, value) {
    for(var key in value) {
      if(Object.prototype.hasOwnProperty.call(value, key)) {
        obj[key] = value[key];
      }
    }
  }

  __exports__.extend = extend;var toString = Object.prototype.toString;
  __exports__.toString = toString;
  // Sourced from lodash
  // https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
  var isFunction = function(value) {
    return typeof value === 'function';
  };
  // fallback for older versions of Chrome and Safari
  if (isFunction(/x/)) {
    isFunction = function(value) {
      return typeof value === 'function' && toString.call(value) === '[object Function]';
    };
  }
  var isFunction;
  __exports__.isFunction = isFunction;
  var isArray = Array.isArray || function(value) {
    return (value && typeof value === 'object') ? toString.call(value) === '[object Array]' : false;
  };
  __exports__.isArray = isArray;

  function escapeExpression(string) {
    // don't escape SafeStrings, since they're already safe
    if (string instanceof SafeString) {
      return string.toString();
    } else if (!string && string !== 0) {
      return "";
    }

    // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.
    string = "" + string;

    if(!possible.test(string)) { return string; }
    return string.replace(badChars, escapeChar);
  }

  __exports__.escapeExpression = escapeExpression;function isEmpty(value) {
    if (!value && value !== 0) {
      return true;
    } else if (isArray(value) && value.length === 0) {
      return true;
    } else {
      return false;
    }
  }

  __exports__.isEmpty = isEmpty;
  return __exports__;
})(__module3__);

// handlebars/exception.js
var __module4__ = (function() {
  "use strict";
  var __exports__;

  var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

  function Exception(message, node) {
    var line;
    if (node && node.firstLine) {
      line = node.firstLine;

      message += ' - ' + line + ':' + node.firstColumn;
    }

    var tmp = Error.prototype.constructor.call(this, message);

    // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
    for (var idx = 0; idx < errorProps.length; idx++) {
      this[errorProps[idx]] = tmp[errorProps[idx]];
    }

    if (line) {
      this.lineNumber = line;
      this.column = node.firstColumn;
    }
  }

  Exception.prototype = new Error();

  __exports__ = Exception;
  return __exports__;
})();

// handlebars/base.js
var __module1__ = (function(__dependency1__, __dependency2__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;

  var VERSION = "1.3.0";
  __exports__.VERSION = VERSION;var COMPILER_REVISION = 4;
  __exports__.COMPILER_REVISION = COMPILER_REVISION;
  var REVISION_CHANGES = {
    1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
    2: '== 1.0.0-rc.3',
    3: '== 1.0.0-rc.4',
    4: '>= 1.0.0'
  };
  __exports__.REVISION_CHANGES = REVISION_CHANGES;
  var isArray = Utils.isArray,
      isFunction = Utils.isFunction,
      toString = Utils.toString,
      objectType = '[object Object]';

  function HandlebarsEnvironment(helpers, partials) {
    this.helpers = helpers || {};
    this.partials = partials || {};

    registerDefaultHelpers(this);
  }

  __exports__.HandlebarsEnvironment = HandlebarsEnvironment;HandlebarsEnvironment.prototype = {
    constructor: HandlebarsEnvironment,

    logger: logger,
    log: log,

    registerHelper: function(name, fn, inverse) {
      if (toString.call(name) === objectType) {
        if (inverse || fn) { throw new Exception('Arg not supported with multiple helpers'); }
        Utils.extend(this.helpers, name);
      } else {
        if (inverse) { fn.not = inverse; }
        this.helpers[name] = fn;
      }
    },

    registerPartial: function(name, str) {
      if (toString.call(name) === objectType) {
        Utils.extend(this.partials,  name);
      } else {
        this.partials[name] = str;
      }
    }
  };

  function registerDefaultHelpers(instance) {
    instance.registerHelper('helperMissing', function(arg) {
      if(arguments.length === 2) {
        return undefined;
      } else {
        throw new Exception("Missing helper: '" + arg + "'");
      }
    });

    instance.registerHelper('blockHelperMissing', function(context, options) {
      var inverse = options.inverse || function() {}, fn = options.fn;

      if (isFunction(context)) { context = context.call(this); }

      if(context === true) {
        return fn(this);
      } else if(context === false || context == null) {
        return inverse(this);
      } else if (isArray(context)) {
        if(context.length > 0) {
          return instance.helpers.each(context, options);
        } else {
          return inverse(this);
        }
      } else {
        return fn(context);
      }
    });

    instance.registerHelper('each', function(context, options) {
      var fn = options.fn, inverse = options.inverse;
      var i = 0, ret = "", data;

      if (isFunction(context)) { context = context.call(this); }

      if (options.data) {
        data = createFrame(options.data);
      }

      if(context && typeof context === 'object') {
        if (isArray(context)) {
          for(var j = context.length; i<j; i++) {
            if (data) {
              data.index = i;
              data.first = (i === 0);
              data.last  = (i === (context.length-1));
            }
            ret = ret + fn(context[i], { data: data });
          }
        } else {
          for(var key in context) {
            if(context.hasOwnProperty(key)) {
              if(data) { 
                data.key = key; 
                data.index = i;
                data.first = (i === 0);
              }
              ret = ret + fn(context[key], {data: data});
              i++;
            }
          }
        }
      }

      if(i === 0){
        ret = inverse(this);
      }

      return ret;
    });

    instance.registerHelper('if', function(conditional, options) {
      if (isFunction(conditional)) { conditional = conditional.call(this); }

      // Default behavior is to render the positive path if the value is truthy and not empty.
      // The `includeZero` option may be set to treat the condtional as purely not empty based on the
      // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
      if ((!options.hash.includeZero && !conditional) || Utils.isEmpty(conditional)) {
        return options.inverse(this);
      } else {
        return options.fn(this);
      }
    });

    instance.registerHelper('unless', function(conditional, options) {
      return instance.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn, hash: options.hash});
    });

    instance.registerHelper('with', function(context, options) {
      if (isFunction(context)) { context = context.call(this); }

      if (!Utils.isEmpty(context)) return options.fn(context);
    });

    instance.registerHelper('log', function(context, options) {
      var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
      instance.log(level, context);
    });
  }

  var logger = {
    methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },

    // State enum
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3,
    level: 3,

    // can be overridden in the host environment
    log: function(level, obj) {
      if (logger.level <= level) {
        var method = logger.methodMap[level];
        if (typeof console !== 'undefined' && console[method]) {
          console[method].call(console, obj);
        }
      }
    }
  };
  __exports__.logger = logger;
  function log(level, obj) { logger.log(level, obj); }

  __exports__.log = log;var createFrame = function(object) {
    var obj = {};
    Utils.extend(obj, object);
    return obj;
  };
  __exports__.createFrame = createFrame;
  return __exports__;
})(__module2__, __module4__);

// handlebars/runtime.js
var __module5__ = (function(__dependency1__, __dependency2__, __dependency3__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;
  var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
  var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;

  function checkRevision(compilerInfo) {
    var compilerRevision = compilerInfo && compilerInfo[0] || 1,
        currentRevision = COMPILER_REVISION;

    if (compilerRevision !== currentRevision) {
      if (compilerRevision < currentRevision) {
        var runtimeVersions = REVISION_CHANGES[currentRevision],
            compilerVersions = REVISION_CHANGES[compilerRevision];
        throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. "+
              "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").");
      } else {
        // Use the embedded version info since the runtime doesn't know about this revision yet
        throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. "+
              "Please update your runtime to a newer version ("+compilerInfo[1]+").");
      }
    }
  }

  __exports__.checkRevision = checkRevision;// TODO: Remove this line and break up compilePartial

  function template(templateSpec, env) {
    if (!env) {
      throw new Exception("No environment passed to template");
    }

    // Note: Using env.VM references rather than local var references throughout this section to allow
    // for external users to override these as psuedo-supported APIs.
    var invokePartialWrapper = function(partial, name, context, helpers, partials, data) {
      var result = env.VM.invokePartial.apply(this, arguments);
      if (result != null) { return result; }

      if (env.compile) {
        var options = { helpers: helpers, partials: partials, data: data };
        partials[name] = env.compile(partial, { data: data !== undefined }, env);
        return partials[name](context, options);
      } else {
        throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
      }
    };

    // Just add water
    var container = {
      escapeExpression: Utils.escapeExpression,
      invokePartial: invokePartialWrapper,
      programs: [],
      program: function(i, fn, data) {
        var programWrapper = this.programs[i];
        if(data) {
          programWrapper = program(i, fn, data);
        } else if (!programWrapper) {
          programWrapper = this.programs[i] = program(i, fn);
        }
        return programWrapper;
      },
      merge: function(param, common) {
        var ret = param || common;

        if (param && common && (param !== common)) {
          ret = {};
          Utils.extend(ret, common);
          Utils.extend(ret, param);
        }
        return ret;
      },
      programWithDepth: env.VM.programWithDepth,
      noop: env.VM.noop,
      compilerInfo: null
    };

    return function(context, options) {
      options = options || {};
      var namespace = options.partial ? options : env,
          helpers,
          partials;

      if (!options.partial) {
        helpers = options.helpers;
        partials = options.partials;
      }
      var result = templateSpec.call(
            container,
            namespace, context,
            helpers,
            partials,
            options.data);

      if (!options.partial) {
        env.VM.checkRevision(container.compilerInfo);
      }

      return result;
    };
  }

  __exports__.template = template;function programWithDepth(i, fn, data /*, $depth */) {
    var args = Array.prototype.slice.call(arguments, 3);

    var prog = function(context, options) {
      options = options || {};

      return fn.apply(this, [context, options.data || data].concat(args));
    };
    prog.program = i;
    prog.depth = args.length;
    return prog;
  }

  __exports__.programWithDepth = programWithDepth;function program(i, fn, data) {
    var prog = function(context, options) {
      options = options || {};

      return fn(context, options.data || data);
    };
    prog.program = i;
    prog.depth = 0;
    return prog;
  }

  __exports__.program = program;function invokePartial(partial, name, context, helpers, partials, data) {
    var options = { partial: true, helpers: helpers, partials: partials, data: data };

    if(partial === undefined) {
      throw new Exception("The partial " + name + " could not be found");
    } else if(partial instanceof Function) {
      return partial(context, options);
    }
  }

  __exports__.invokePartial = invokePartial;function noop() { return ""; }

  __exports__.noop = noop;
  return __exports__;
})(__module2__, __module4__, __module1__);

// handlebars.runtime.js
var __module0__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
  "use strict";
  var __exports__;
  /*globals Handlebars: true */
  var base = __dependency1__;

  // Each of these augment the Handlebars object. No need to setup here.
  // (This is done to easily share code between commonjs and browse envs)
  var SafeString = __dependency2__;
  var Exception = __dependency3__;
  var Utils = __dependency4__;
  var runtime = __dependency5__;

  // For compatibility and usage outside of module systems, make the Handlebars object a namespace
  var create = function() {
    var hb = new base.HandlebarsEnvironment();

    Utils.extend(hb, base);
    hb.SafeString = SafeString;
    hb.Exception = Exception;
    hb.Utils = Utils;

    hb.VM = runtime;
    hb.template = function(spec) {
      return runtime.template(spec, hb);
    };

    return hb;
  };

  var Handlebars = create();
  Handlebars.create = create;

  __exports__ = Handlebars;
  return __exports__;
})(__module1__, __module3__, __module4__, __module2__, __module5__);

  return __module0__;
})();

this.Handlebars = this.Handlebars || {};
this.Handlebars.templates = this.Handlebars.templates || {};
(function() {

    var win = window;

    // Custom event polyfill
    if(!win.CustomEvent) {

        var CustomEvent = function(event, params) {

            var evt;

            params = params || {
                bubbles: false,
                cancelable: false,
                detail: undefined
            };

            try {
                evt = document.createEvent("CustomEvent");
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            } catch (error) {
                evt = document.createEvent("Event");
                for (var param in params) {
                    evt[param] = params[param];
                }
                evt.initEvent(event, params.bubbles, params.cancelable);
            }

            return evt;
        };

        CustomEvent.prototype = win.Event.prototype;

        win.CustomEvent = CustomEvent;

    }

}());
(function() {

    /**
     * @ignore
     * FastClick is an override shim which maps event pairs of
     *   'touchstart' and 'touchend' which differ by less than a certain
     *   threshold to the 'click' event.
     *   This is used to speed up clicks on some browsers.
     */
    var clickThreshold = 300;
    var clickWindow = 500;
    var potentialClicks = {};
    var recentlyDispatched = {};
    var win = window;
    var inputs = ['INPUT', 'TEXTAREA', 'SELECT'];
    var android = (win.navigator.userAgent.indexOf('Android') >= 0);
    var mustBlur = false;

    win.addEventListener('touchstart', function(event) {

        if (mustBlur) {
            //if (document.activeElement) document.activeElement.blur();
            if (typeof mustBlur.blur === 'function') {
                mustBlur.blur();
            }
            mustBlur = false;
        }

        var timestamp = Date.now(),
            len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            potentialClicks[touch.identifier] = timestamp;
        }
    });
    win.addEventListener('touchmove', function(event) {

        var len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            delete potentialClicks[touch.identifier];
        }
    });
    win.addEventListener('touchend', function(event) {

        var currTime = Date.now(),
            len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            var startTime = potentialClicks[touch.identifier];
            if (startTime && currTime - startTime < clickThreshold) {
                var clickEvt = new win.CustomEvent('click', {
                    'bubbles': true,
                    'details': touch
                });
                recentlyDispatched[currTime] = event;
                event.target.dispatchEvent(clickEvt);
                // we prevent here to avoid ghost click on ios 7 with input click (2nd time)
                // tested on ios
                event.preventDefault(); // if (ios) ? must test on android
                if (inputs.indexOf(event.target.tagName) !== -1) {
                    if (!android && event.target.focus) event.target.focus();
                    else mustBlur = event.target;
                }
            }
            delete potentialClicks[touch.identifier];
        }
    });
    win.addEventListener('click', function(event) {

        var currTime = Date.now();
        for (var i in recentlyDispatched) {
            var previousEvent = recentlyDispatched[i];
            if (currTime - i < clickWindow) {
                if (event instanceof win.MouseEvent) {
                    event.stopPropagation();
                    // prevent ghost click
                    if (event.target !== previousEvent.target) event.preventDefault();
                }
                // if (event instanceof win.MouseEvent && event.target === previousEvent.target) event.stopPropagation();
                // else if (event instanceof win.MouseEvent && event.target !== previousEvent.target) {
                //     // @TODO @PATCH prevent ghost click
                //     event.stopPropagation();
                //     event.preventDefault();
                // }
            }
            else delete recentlyDispatched[i];
        }
    }, true);

}());

/**
 * @class Roc
 * @singleton
 *
 * Framework global namespace.
 *
 * ### Class definition example

    App.MyClass = (function() {

        // private instances shared variable declaration
        var _parent = Roc;

        return _parent.subclass({

            constructor: function(config) {
                // Class constructor
                _parent.prototype.constructor.call(this, config);
            }

        });

    }());
 */

(function() {

    //'use strict';

    var slice = Array.prototype.slice,
        _xtypes = {};

    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {

            if (_xtypes[xtype]) return _xtypes[xtype];
            console.warn('[core] xtype "' + xtype + '" does not exists.');
            return false;

        }
    };

    var _createSubclass = function(props) {

        props = props || {};

        var key,
            realConstructor,
            superclass = props.superclass.prototype;

        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }

        function constructor() {

            var self = this;

            if (!(self instanceof constructor)) {
                throw new Error('[core] Please use "new" when initializing Roc classes');
            }
            realConstructor.apply(self, arguments);
        }

        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;

        _extend(constructor, {

            parent: superclass,

            subclass: function(obj) {

                var sclass;

                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);

                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });

        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }

        return constructor;
    };

    var _extend = function() {

        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();

        while (++i < l) {

            var key,
                props = args[i];

            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };

    var core = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        /**
         * @readonly
         * @property {String} version
         * Current framework version.
         */
        version: '2.0.0',

        /**
         * Create instance of object from given xtype
         * and configuration.
         * @param {String} xtype Xtype.
         * @param {Object} [config] Configuration.
         * @return {Mixed} Instanciated object.
         */
        create: function(xtype, config) {

            var obj = _xtype(xtype);

            return (new obj(config));

        },

        /**
         * Extend given xtype or class with given configuration and return the new class.
         * This method will override constructor method of subclass, so please use `subclass` if you wanna override constructor instead.
         * @param  {Mixed} xtype  Xtype string or Parent object class to extend.
         * @param  {Object} config Object to extend.
         * @return {Mixed} New generated extend class.
         *
         * For example:
         *
         *      // this:
         *      var mypage = Roc.extend('page', {
         *
         *          xtype: 'mypage',
         *
         *          config: {
         *              items: [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }]
         *          }
         *
         *      });
         *
         *      // is same as:
         *      var mypage2 = Roc.views.Page.subclass({
         *
         *          xtype: 'mypage',
         *
         *          constructor: function(opts) {
         *
         *              opts = opts || {};
         *
         *              opts.items = [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }];
         *
         *              Roc.views.Page.prototype.constructor.call(this, opts);
         *
         *          }
         *      });
         *
         *      // and same as:
         *      var mypage3 = Roc.extend(Roc.views.Page, {
         *
         *          xtype: 'mypage',
         *
         *          config: {
         *              items: [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }]
         *          }
         *
         *      });
         */
        extend: function(xtype, config) {

            if (typeof xtype === 'string') xtype = _xtype(xtype);

            if (!xtype || typeof xtype.subclass !== 'function') {
                throw new Error('Class or xtype does not exists.');
                return false;
            }

            if (config.config) {
                config.constructor = function(opts) {

                    //opts = opts || {};
                    opts = config.config;

                    xtype.prototype.constructor.call(this, config.config);
                };
            }

            return xtype.subclass(config);
        },

        /**
         * Allows to extend from class.
         * @method
         * @param {Object} obj
         * @return {Object}
         */
        subclass: function(obj) {
            var sclass;

            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);

            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = core;
        }
        exports.Roc = core;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return core;
        });
    } else {
        window.Roc = core;
    }

})();

Roc.debug = new (function() {

    var _profiles = {},
        _debug = false,
        _whitelist = [/*
            'router',
            'news',
            'store',
            'request',
            'newsdetail',
            'button',
            'list',
            //'click',
            //'clickbuster',
            'listbuffered',
            'loadmask',
            'scroll',
            'request',
            'bounce'
        */];

    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };

    return Roc.subclass({

        /*profile: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.time(key);
            //_profiles[key] = this.memory();
            return this;
        },*/

        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },

        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },

        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },

        /*profileEnd: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.timeEnd(key);
            //console.debug(_profiles[key], '->', this.memory());
            //console.debug(key, 'Before: ', _profiles[key]);
            //console.debug(key, 'Now: ', (window.performance.memory.usedJSHeapSize / 1024.0) + 'ko');
            return this;
        },

        memory: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            if (window.performance && window.performance.memory) {
                return ((window.performance.memory.totalJSHeapSize / 1024.0) + 'ko');
            }
            return 0;
        },*/

        set: function(debug) {
            _debug = (debug === true);
        }
    });

}())();

/**
 * @class Roc.utils
 * @singleton
 */
Roc.utils = (function() {

    'use strict';

    var _table = '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';

    return {
        /**
         * Apply base[key] to obj[key] if obj[key] is undefined.
         *
         * ### Sample usage
         *
         *     var obj = { a: true, b: "toto", c: undefined },
         *         base = { a: false, b: "tata", c: [1, 2], d: 1 };
         *
         *     var results = Roc.utils.applyIf(obj, base, ['a', 'c', 'd']);
         *
         *     // results = { a: true, b: "toto", c: [1, 2], d: 1 };
         *
         * @param {Object} obj The receiver of the properties.
         * @param {Object} base The source of the properties.
         * @param {Array} keys List of keys to copy.
         * @return {Object} returns obj
         */
        applyIf: function(obj, base, keys) {

            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof obj[key] === 'undefined' &&
                    typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of base to obj.
         * @param {Object} obj The receiver of properties.
         * @param {Object} base The source of properties.
         * @return {Object} returns obj
         */
        applyIfAuto: function(obj, base) {

            var key;

            for (key in base) {
                if (typeof obj[key] === 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of base to obj for given keys.
         * @param {Object} to_override The receiver of properties.
         * @param {Object} values The source of properties.
         * @param {Array} keys Keys to apply.
         * @return {Object} Updated obj.
         */
        apply: function(to_override, values, keys) {

            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof values[key] !== 'undefined') {
                    to_override[key] = values[key];
                }
            }
            return to_override;
        },

        /**
         * Apply every values of both objects to new object.
         * It means we merge obj1 with obj2 into new object.
         * If same key is in both objects, obj2 values will be taken.
         * @param {Object} obj1
         * @param {Object} obj2
         * @return {Object} Merged obj1 and obj2 objects.
         */
        applyAuto: function(obj1, obj2) {

            var attrname, obj3 = {};

            for (attrname in obj1) {
                obj3[attrname] = obj1[attrname];
            }
            for (attrname in obj2) {
                obj3[attrname] = obj2[attrname];
            }
            return obj3;
        },

        /**
         * Copy all array values from given start index to given end index.
         * @param {Array} array Array to copy.
         * @param {Number} [from=0] Index to copy data from.
         * @param {Number} [to=array.length] Index to copy data to.
         * @return {Array} Copied array.
         */
        arraySlice: function(array, from, to) {

            var result = [],
                len = array.length,
                i = (typeof from === 'number' ? from : 0) - 1,
                length = (typeof to === 'number' ? to : len);

            if (i < -1) {
                i = -1;
            } else if (i >= len) {
                i = len;
            }

            if (length < 0) {
                length = -1;
            } else if (length >= len) {
                length = len - 1;
            }

            if (i > length) {
                i = length;
            }

            while (++i <= length) {
                result.push(array[i]);
            }
            return result;
        },

        /**
         * Capitalize given string and return it.
         * @param {String} str String to capitalize.
         * @return {String} Capitalized string.
         */
        capitalize: function(str) {
            if (typeof str !== 'string') return str;
            return (str.charAt(0).toUpperCase() + str.slice(1));
        },

        /**
         * Returns the crc32 for the given string.
         * @param {String} str String.
         * @param {Number} [crc=0] CRC.
         * @return {Number} CRC32 for given string.
         */
        crc32: function(str, crc) {

            if (typeof str !== 'string') return false;

            if (typeof crc !== 'number') {
                crc = 0;
            }

            var i = 0,
                iTop = str.length,
                n = 0, //a number between 0 and 255
                x = 0; //an hex number

            crc = crc ^ (-1);
            for (; i < iTop; i++ ) {
                n = ( crc ^ str.charCodeAt(i)) & 0xFF;
                x = '0x' + _table.substr(n * 9, 8);
                crc = (crc >>> 8) ^ x;
            }
            return crc ^ (-1);
        }
    };

}());

/**
 * @class Roc.Storage
 * @extends Roc
 * @singleton
 *
 * ### Example usage
 *
    var s = Roc.Storage;

    // (optional) set a prefix for our application
    s.setPrefix('myapp');

    // you can store item by name
    s.setItem('news', [{
        id: 1,
        title: 'News 1',
        content: '...'
    }, {
        id: 2,
        title: 'News 2',
        content: '...'
    }]);

    // you can retrieve those items by name too
    s.getItem('news');

    // or remove them by name
    s.removeItem('news');

    // or completly erase all data for given prefix
    s.empty('myapp');
 *
 */
Roc.Storage = new (function() {

    'use strict';

    var _store = localStorage,
        _prefix = '';

    var _privateStore = {};
    var _backupStore = {
        setItem: function(name, value) {
            _privateStore[name] = value;
        },
        getItem: function(name) {
            return _privateStore[name];
        },
        removeItem: function(name) {
            delete _privateStore[name];
        }
    };

    /**
     * Set a default prefix for all store items key.
     * @method setPrefix
     * @param {String} prefix Prefix.
     * @return {Boolean} true if success else false.
     */
    function _setPrefix(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    }

    /**
     * Get the default prefix.
     * @method getPrefix
     * @param {String} prefix Prefix.
     * @return {String} Return given prefix if correct else default prefix.
     */
    function _getPrefix(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    }

    /**
     * Store value for given name.
     * @method setItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _setItem(name, value, prefix, retry) {

        var varname = _getPrefix(prefix) + name,
            varvalue = JSON.stringify(value);

        try {
            _store.setItem(varname, varvalue);
        } catch (e) {
            if (e.name === 'QUOTA_EXCEEDED_ERR') {
                _reset(!retry);
                if (retry !== false) {
                    setTimeout(function() {
                        _setItem(name, value, prefix, false);
                    }, 1);
                } else {
                    _store = _backupStore;
                    _store.setItem(varname, varvalue);
                }
            }
        }
    }

    function _reset(keep_with_prefix) {

        var prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (keep_with_prefix !== true ||
                (prefix && name.indexOf(prefix))) {
                _removeItem(name, '');
            }
        }

    }

    /**
     * Get value stored for given name.
     * @method getItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _getItem(name, prefix) {

        var value = _store.getItem(_getPrefix(prefix) + name);

        // @bugfix for android 2.3 when JSON.parse null
        if (typeof value === 'string') {
            return JSON.parse(value);
        }
        return false;
    }

    /**
     * Remove value at given name.
     * @method removeItem
     * @param {String} name Name.
     * @param {String} prefix (optional) Prefix.
     * @return {Boolean} True if success else false.
     */
    function _removeItem(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    }

    /**
     * Empty all stored data for given prefix or all if no prefix.
     * @method empty
     * @param {String} prefix (optional) Prefix.
     */
    function _empty(prefix) {
        prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    }

    return Roc.subclass({

        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty,
        reset: _reset

    });

}())();

/**
 * @singleton
 * @class Roc.Selector
 * @extends Roc
 *
 * DOM manipulation and utilities.
 */
Roc.Selector = new (function() {

    //'use strict';

    // @private
    var _ns = Roc,
        _scope,
        _elUid = 0,
        _win = window,
        _prefix,
        _utils = _ns.utils;

    _prefix = (function () {

        var styles = _win.getComputedStyle(document.documentElement, ''),
            pre = (Array.prototype.slice
                .call(styles)
                .join('')
                .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
            )[1],
            dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1],
            cssStyle = _utils.capitalize(pre);

        var events = {
            'moz': undefined,
            'webkit': 'webkit',
            'ms': undefined,
            'o': 'o'
        };

        if (cssStyle === 'Webkit') {
            cssStyle = 'webkit';
        }

        return {
            dom: dom,
            lowercase: pre,
            css: '-' + pre + '-',
            cssStyle: cssStyle,
            jsEvent: function(eventName) {

                var before = events[pre];

                if (!before) {
                    return eventName.toLowerCase();
                }

                return (before + _utils.capitalize(eventName));
            },
            js: pre[0].toUpperCase() + pre.substr(1)
        };

    })();

    // public
    return Roc.subclass({

        prefix: _prefix,

        /**
         * Create new selector.
         * @param {Object} gscope The global scope. Defaults to `window`.
         */
        constructor: function(gscope) {
            _scope = gscope;
        },

        /**
         * Generate new DOM uniq identifier.
         * @param {String} prefix (optional) Prefix.
         * @return {String} id.
         */
        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'r';
            return (prefix + _elUid);
        },

        /**
         * Get a DOM Node from query selector.
         *
         * @param {String} selector Selector
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {HTMLElement} result.
         */
        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },

        /**
         * Get all DOM nodes from query selector.
         * @param {String} selector Selector.
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {Array} Array of HTMLElement matches.
         */
        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },

        /**
         * Execute callback recursivly on element parent until
         * callback returns false.
         * @param {HTMLElement} el DOM node.
         * @param {Function} callback Callback.
         * @return {Mixed}
         */
        findParent: function(el, callback) {

            var parentEl = callback(el);

            if (parentEl === false && el.parentNode) {
                return this.findParent(el.parentNode, callback);
            }
            return parentEl;
        },

        /**
         * Apply given css properties to element.
         * This method will automatically check for prefix properties.
         * So it is highly recommended to use for example:
         *  `transform` instead of `webkitTransform`.
         *
         * @method css
         * @param {HTMLElement} el DOM node.
         * @param {Object} props CSS properties.
         * @return {Boolean} success
         */
        css: function(el, props) {

            var self = this,
                styles = document.body.style;

            Object.keys(props).forEach(function(name) {

                var compliant = self.prefix.cssStyle + _utils.capitalize(name);

                if (compliant in styles) {
                    el.style[compliant] = props[name];
                }
                el.style[name] = props[name];
            });
            return true;
        },

        /**
         * Checks if element has CSS class.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean}
         */
        hasClass: function(el, cls) {
            // <debug>
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el);
                console.trace('Roc.Selector.hasClass');
                return false;
            }
            // </debug>
            //return (el.classList && el.classList.contains(cls));
            return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') !== -1);
        },

        /**
         * Checks if element has given CSS classes.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        hasClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                if (!this.hasClass(el, c[len])) {
                    return false;
                }
            }
            return true;

        },

        /**
         * Adds CSS class to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                //console.log('addClass ====== ', el.id, cls);
                el.className += ' ' + cls;
                el.className = el.className.replace(/^\s+|\s+$/g, '');
                //el.classList.add(cls);
                return true;
            }
            return false;
        },

        /**
         * Add CSS classes from given element.
         * Use this method instead of {@link #addClass} when you are not sure if one or several of those classes are already applied on element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        addClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.addClass(el, c[len]);
            }
            return true;

        },

        /**
         * Removes CSS class from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                //el.classList.remove(cls);
                return true;
            }
            return false;
        },

        /**
         * Remove CSS classes from given element.
         * Use this method instead of {@link #removeClass} when you are not sure if one or several of those classes are already applied or removed from element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        removeClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.removeClass(el, c[len]);
            }
            return true;
        },

        /**
         * Force browser to redraw given element.
         * @param {HTMLElement} el DOM node.
         */
        redraw: function(el) {
            el.style.display = 'none';
            var h = el.offsetHeight;
            el.style.display = 'block';
        },

        /**
         * Removes given element from DOM.
         * @param {HTMLElement} el DOM node.
         */
        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },

        /**
         * Removes all HTML from given element.
         * @param {HTMLElement} el DOM node.
         */
        removeHtml: function(el) {
            //el.innerHTML = '';
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },

        // !TODO doc
        findParentByTag: function(el, tagName) {
            while (el) {
                if (el.tagName.toLowerCase() === tagName.toLowerCase()) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        // !TODO doc
        findParentByClass: function(el, className) {
            while (el) {
                if (this.hasClass(el, className)) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        /**
         * Adds HTML to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },

        /**
         * Updates HTML from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },

        /**
         * Scrolls to given offsets for given element.
         * @param {HTMLElement} el DOM node.
         * @param {Number} offsetX (optional) Offset x. Defaults to 0.
         * @param {Number} offsetY (optional) Offset y. Defaults to 0.
         */
        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });

}())(document);

/**
 * @class Roc.Request
 * @extends Roc
 * @singleton
 *
 * Request class to perform remote request.
 * Used by {@link Roc.data.Store#proxy store proxy}.
 *
 * ### Example
 *
 *      Roc.Request.ajax('/login', {
 *          jsonParams: {
 *              username: 'mrsmith',
 *              password: 'wanna die?'
 *          },
 *          scope: this,
 *          callback: function(requestSuccess, data) {
 *
 *              if (requestSuccess === true) {
 *
 *                  if (typeof data === 'object' &&
 *                      data.success === true) {
 *                      // welcome !
 *                  } else
 *                      // login error
 *                  }
 *
 *              } else {
 *                  // server error
 *              }
 *
 *           }
 *      });
 *
 */
Roc.Request = new (function() {

    'use strict';

    // @private
    var jsonp_id = 0;

    var _jsonToString = function(json, prefix) {

        var key,
            val,
            result = '',
            first = true;

        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }

        return (prefix ? prefix : '') + result;
    };

    function url_delimiter(url) {
        return (url.indexOf('?') === -1 ? '?': '&');
    }

    // public
    return Roc.subclass({

        /**
         * Makes an AJAX request.
         *
         * ## Example
         *
         *      Roc.Request.ajax('/getdata', {
         *          callback: function(success, data) {},
         *          scope: this,
         *          method: 'PUT', // GET, POST, PUT, DELETE
         *          data: 'name1=value1&name2=value2',
         *          params: 'name1=value1&name2=value2', // (for GET)
         *          jsonParams: { name1: 'value1', name2: 'value2' } // (for GET)
         *      });
         *
         * @param {String} url URL.
         * @param {Object} options Options.
         */
        ajax: function(url, options) {

            var request,
                callback = (typeof options.callback === 'function' ?
                    options.callback.bind(options.scope || window) : null);
                options.method = options.method || 'GET';
                options.data = options.data || '';

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }

            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },

        /**
         * Makes a JSONP request.
         *
         * ## Example
         *
         *     Roc.Request.jsonp('/getdata?callback={callback}', {
         *         callback: function(success, data) {},
         *         scope: this,
         *         params: 'sortby=date&dir=desc',
         *         jsonParams: {
         *             sortby: 'date',
         *             dir: 'desc'
         *         }
         *     });
         *
         * @param {String} url URL.
         * @param {Object} options An object which may contain the following properties. Note that options will
         * take priority over any defaults that are specified in the class.
         * <ul>
         * <li><b>params</b> : String (Optional)<div class="sub-desc">A string containing additional parameters.</div></li>
         * <li><b>jsonParams</b> : Object (Optional)<div class="sub-desc">An object containing a series of
         * key value pairs that will be sent along with the request.</div></li>
         * <li><b>callback</b> : Function (Optional) <div class="sub-desc">A function to execute when the request
         * completes, whether it is a success or failure.</div></li>
         * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope in
         * which to execute the callbacks: The "this" object for the callback function. Defaults to the browser window.</div></li>
         * </ul>
         */
        jsonp: function(url, options) {

            options = (typeof options === 'object' ? options : {});

            var callback_name = (options.callbackName ? options.callbackName : ['jsonp', ++jsonp_id].join('')),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    //_debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams, url_delimiter(url));
            }
            if (options.params) {
                url += (url_delimiter(url) + options.params);
            }

            if (url.indexOf('{callback}') === -1) {
                url += (url_delimiter(url) + 'callback={callback}');
            }

            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        }
    });
}())();

/**
 * @class Roc.History
 * @extends Roc
 * @singleton
 * @requires Roc.Storage
 *
 * History support class. Use with {@link Roc.Router router}.
 *
 * ### Example
 *
 *      var history = Roc.History;
 *
 *      history.start();
 *      history.navigate('/home');
 *
 *      var currentUrl = history.here();
 *
 */
Roc.History = new (function() {

    'use strict';

    // @private
    var _ns = Roc,
        _win = window,
        _storage = _ns.Storage,
        _self,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [],
        _history = [],
        _callbacks = {},
        _maxLength = 20,
        _curLength = 0;

    function runCallbacks(self, location) {
        /*if (self.curhash === location) {
            return false;
        }*/
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }

        var regexp,
            route,
            i = -1,
            length = routes.length;

        self.curhash = location;

        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                if (route.callback(location) === false) {
                    return false;
                }
            }
        }

        if (typeof _callbacks[location] !== 'undefined' &&
            _callbacks[location].length > 0) {

            var c = _callbacks[location],
                len = c.length;

            while (len--) {
                (c.pop())();
            }

        }

        return true;
    }

    var onHashChange = function(event) {

        var here = _win.location.hash.substr(1);

        if (_history[_curLength - 1] !== here) {
            _curLength = _history.push(here);
        }

        if (_curLength > _maxLength) {
            _history.shift();
        }
        runCallbacks(_self, here);
    };

    // public
    return _ns.subclass({

        /**
         * Create new history.
         */
        constructor: function() {
            /**
             * @property {Boolean} started True if history has started else false.
             * @readOnly
             */
            this.started = false;
            /**
             * @property {String} defaultRoute The default route.
             * @readOnly
             */
            this.defaultRoute = '';
            _self = this;
        },

        /**
         * Set the default route.
         * @param {String} path Path.
         */
        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },

        /**
         * Start history.
         * If browser is standalone it will recover to last visited url or {@link #defaultRoute defaultRoute}.
         */
        start: function() {

            if (this.started === true) return false;

            var hash = curhash;

            this.started = true;
            curhash = '';
            _win.addEventListener('hashchange', onHashChange, false);

            // if added to homescreen, try to restore previous location
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks(this, hash);

            return true;
        },

        /**
         * Reset history
         * @param {Boolean} [restart=false] True to restart listener.
         */
        reset: function(restart) {
            _history = [];
            _curLength = 0;

            if (restart === true) {
                this.stop();
                _win.location.hash = '';
                this.start();
            }
        },

        /**
         * Stop history.
         */
        stop: function() {

            if (this.started === false) return false;

            this.started = false;
            _win.removeEventListener('hashchange', onHashChange, false);
        },

        /**
         * Get the current location.
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Current location.
         */
        here: function(encoded) {

            var hash = _win.location.hash.substr(1);

            return (encoded ? encodeURIComponent(hash) : hash);
        },

        /**
         * Get history item from given index.
         * @param {Number} index
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Location for given index.
         */
        get: function(index, encoded) {

            var pos = _curLength - 1 + index;

            if (pos >= _curLength) {
                return this.here(encoded);
            } else if (pos < 0) {
                pos = 0;
            }
            return (encoded ? encodeURIComponent(_history[pos]) : _history[pos]);
        },

        /**
         * Go back in history.
         * @param {Number} index (optional)
         */
        back: function(index) {
            index = (typeof index !== 'number' ? -1 : index);
            this.go(this.get(index));
        },

        // @TODO backToLastRefused
        /*
        Stocker lors du go avec genre {pending: true}
        et lors de l'appel du callback on delete history.pending
        */

        /**
         * Navigate to given location.
         * @param {String} location.
         */
        navigate: function() {
            return this.go.apply(this, arguments);
        },

        go: function(location, callback) {
            _win.location.hash = '#' + location;

            if (typeof callback === 'function') {
                if (typeof _callbacks[location] === 'undefined') {
                    _callbacks[location] = [];
                }
                _callbacks[location].push(callback);
            }
        },

        /**
         * Add given route.
         * @param {String} route Route
         * @param {Function} callback Callback.
         */
        route: function(base, route, callback) {
            routes.push({
                base: base,
                regexp: route,
                callback: callback
            });
        }
    });
}())();

/**
 * @class Roc.Router
 * @extends Roc
 * @xtype router
 *
 * ### Example
 *

 *
 */
Roc.Router = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _history = _ns.History,
        optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;

    function _extractParameters(route, fragment) {
        return route.exec(fragment).slice(1);
    }

    function _routeToRegExp(route) {
        route = route.replace(escapeRegExp, '\\$&')
            .replace(optionalParam, '(?:$1)?')
            .replace(namedParam, function(match, optional) {
                return optional ? match : '([^\/]+)';
            })
            .replace(splatParam, '(.*?)');

        return new RegExp('^' + route + '$');
    }

    function _prepareBefore(self) {

        if (self.before) {

            var pattern;

            self.beforeRegexp = {};
            for (pattern in self.before) {
                /* @ignore !TODO permettre de prendre en compte un array de callback
                if (typeof self.before[i] === 'string') {
                    self.before[i] = [self.before[i]];
                }
                */
                self.beforeRegexp[pattern] = new RegExp(pattern);
            }
        }
    }

    function _getBefore(self, funcname) {

        if (self.before) {

            var pattern;

            for (pattern in self.before) {
                if (self.beforeRegexp[pattern].test(funcname) === true) {
                //if (funcname.match(self.beforeRegexp[i]) !== null) {
                    var func = self[self.before[pattern]];

                    return (func ? func : false);
                }
            }
        }
        return false;
    }

    // public
    return _ns.subclass({
        xtype: 'router',
        /**
         * @cfg {Object} routes
         */
        /**
         * @cfg {Object} before
         */
        /**
         * @cfg {String} routeNoMatch
         */
        /**
         * Create new router.
         */
        constructor: function() {

            var self = this;

            _prepareBefore(self);
            if (self.routes) {

                var pattern;

                for (pattern in self.routes) {
                    self.route(pattern, self.routes[pattern]);
                }
            }

            if (self.routeNoMatch) {
                self.matched = false;
                _history.route('', /^(.*?)+$/ig, function() {
                    if (self.matched === false) {
                        self[self.routeNoMatch]();
                    } else {
                        self.matched = false;
                    }
                });
            }
        },

        /**
         * Calls given path with callback.
         *
         * @param {String} path Path.
         * @param {Function} foo Route callback.
         * @method
         */
        route: function(base, foo) {

            var path,
                before,
                self = this,
                callback = self[foo],
                history = _history;

            if (callback) {
                if (self.routeEscape !== false) {
                    path = _routeToRegExp(base);
                } else {
                    path = new RegExp(base);
                }
                before = _getBefore(self, foo);

                var cb = function(fragment) {

                    var args = _extractParameters(path, fragment);

                    callback.apply(self, args);
                    self.matched = true;
                },
                beforeCb = function(fragment) {
                    var canContinue = before.call(self, function() {
                        return cb(fragment);
                    });
                    if (canContinue === false) return false;
                };

                if (before) {
                    history.route(base, path, beforeCb);
                } else {
                    history.route(base, path, cb);
                }
            }

            return self;
        }
    });
}());

/**
 * @ignore !TODO for debugging: Global event handler like Roc.EventHandler.listenAllEvents('afterrender');
 */
/**
 * @class Roc.Event
 * @extends Roc
 * @requires Roc.utils
 *
 * Event manager class.
 *
 * ## Example

    var event = new Roc.Event({
        listeners: {
            scope: this,
            afterload: function(success, data) {
                // do some stuff here...
            },
            beforeload: function(request) {
                if (typeof request.data === 'undefined') {
                    return false; // stop all next events from being firing.
                }
            }
        }
    });

    // Registering listener for `error` event.
    var eid = event.on('error', function(code, message) {
        alert('Error (' + code + ') occured: \n' + message);
    }, this);

    // Firing `error` event.
    event.fire('error', 401, 'Unauthorized !');

    // Unregistering listener.
    event.off(eid);

 */
Roc.Event = (function() {

    'use strict';

    var _ns = Roc,
        _utils = _ns.utils,
        _parent = _ns,
        _priorities = {
            BEFORECORE: 1025,
            CORE: 1000,
            AFTERCORE: 975,
            BEFOREVIEWS: 925,
            VIEWS: 900,
            AFTERVIEWS: 875,
            DEFAULT: 800
        };

    function _setupEvents(self) {
        if (typeof self._events !== 'object') {
            // events queue
            /*

            e1 = event_id

            {
                "eventname1": {
                    "998": [e1, e2],
                    "1000": [e3]
                },
                ...
            }

            */
            self._events = {};
            self.uids = 0;
            // priority queue @TODO
            /*

            {
                "eventname1": [998, 1000],
                "eventname2": [1000],
                "eventname3": [100, 1000, 9999],
                ...
            }

            */
            self.pqueue = {};
            // callbacks queue
            /*

            {
                "event_id": [event_name, priority, callback, scope],
                ...
            }

            */
            self.equeue = {};
        }
    }

    function _numSort(a, b) {
        return (b - a);
    }

    function _count_listeners(self, name) {

        var e = self._events[name];

        if (e) {
            var total = 0,
                keys = Object.keys(self._events[name]),
                i = keys.length;

            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    }

    function _register_listeners(self, listeners) {

        var key, priority,
            scope = self,
            l = self._listeners = self._listeners || {},
            keys = Object.keys(listeners),
            len = keys.length;

        var scopeIndex = keys.indexOf('scope');
        if (scopeIndex !== -1) {
            keys.splice(scopeIndex, 1);
            scope = listeners.scope;
        }

        var priorityIndex = keys.indexOf('priority');
        if (priorityIndex !== -1) {
            keys.splice(priorityIndex, 1);
            priority = listeners.priority;
        }

        if (typeof priority === 'string' &&
            typeof _priorities[priority] !== 'undefined') {
            priority = _priorities[priority];
        } else {
            priority = _priorities.DEFAULT;
        }

        while (len--) {
            key = keys[len];

            if (!self._listeners[key]) {
                self._listeners[key] = [];
            }
            self._listeners[key].push([listeners[key], scope, priority]);
        }

    }

    /**
     * @cfg {Object} listeners Listeners.
     */
    return _parent.subclass({

        /**
         * @property {Number} priority. Prior to higher.
         * <p><ul>
            <li><b>CORE:</b> Core event priority (high)</li>
            <li><b>VIEWS:</b> Views event priority (medium)</li>
            <li><b>DEFAULT:</b> Default event priority (low)</li>
         * </ul></p>
         */
        priority: _priorities,

        /**
         * @property {Number} defaultPriority Default priority.
         */
        defaultPriority: 800,

        /**
         * Create new event manager.
         * @constructor
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {
            // <inline>

            var self = this;

            opts = opts || {};

            if (opts && opts.listeners) {
                //self.listeners = opts.listeners;
                _register_listeners(self, opts.listeners);
            }
            if (self.listeners) {
                _register_listeners(self, self.listeners);
            }
            self.wasListened = {};
            _setupEvents(self);

            if (opts.hasListeners) {
                self.hasListeners = (self.hasListeners || [])
                    .concat(opts.hasListeners);
            }

            if (self.hasListeners) {
                self.setupListeners(self.hasListeners, self);
            }
            // </inline>
        },

        addListeners: function(listeners) {
            _register_listeners(this, listeners);
        },

        
        /**
         * Initialize listeners for given event and renderer.
         *
         * @method setupListeners
         * @param {Array} eventNames List of event names.
         * @param { Roc.views.Template } renderer (optional) Renderer. Defaults to current class instance.
         */
        
        setupListeners: function(eventNames, renderer) {

            var self = this;

            if (self._listeners) {

                var name,
                    scope = self._listeners.scope || self,
                    i = eventNames.length;

                renderer = renderer || self;

                while (i--) {
                    name = eventNames[i];

                    if (self._listeners[name] &&
                        !self.wasListened[name]) {

                        self.wasListened[name] = true;
                        self._listeners[name].forEach(function(listener) {
                            renderer.on(name, listener[0],
                                (listener[1] || scope), listener[2]);
                        });

                    }

                }
            }
        },

        /**
         * Checks to see if this object has any listeners for a specified event.
         * @param {String} name Event name.
         * @return {Boolean} True if the event is being listened for, else false.
         */
        hasListener: function(name) {
            if (typeof this._listeners === 'object') {
                return (this._listeners[name] instanceof Array);
            }
            return false;
        },

        /**
         * Return listeners for given event name.
         *
         * @param {String} name Event name.
         * @return {Function} Listener callback for given event name.
         */
        getListener: function(name) {
            return this._listeners[name];
        },

        /**
         * Fires given event with given extra parameters.
         *
         * ## Example
         *
         *     // Registering event listener
         *     var eid = this.on('afterload', function(success, data) {
         *         console.log('Received data !', success, data);
         *     }, this);
         *
         *     // Fireing event
         *     this.fire('afterload', true, 'Some data');
         *
         *     // Unregistering event
         *     this.off(eid);
         *
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fire: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1),
                    toOff = [];

                while (++i < plen) {
                    events = self._events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function !');//, arguments.callee.callee, e);
                            toOff.push(events[j]);
                        } else if (e[2].apply(e[3], args) === false) {
                            return false;
                        }
                    }

                }

                toOff.forEach(function(uid) {
                    self.off(uid);
                });
                return true;
            }
        },

        /**
         * Same as fire but put `scope` as first arguments instead of scoping it when calling listeners.
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fireScope: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1),
                    toOff = [];

                while (++i < plen) {
                    events = self._events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function !');//, arguments.callee.callee, e);
                            toOff.push(events[j]);
                        } else if (e[2].apply(e[3], [e[3]].concat(args)) === false) {
                            return false;
                        }
                    }

                }

                toOff.forEach(function(uid) {
                    self.off(uid);
                });
                return true;
            }
        },

        /**
         * Register listener for given eventname.
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this, this.priority.VIEWS);
         *
         * @param {String} name Event name.
         * @param {Function} cb Listener callback.
         * @param {Object} [scope=null] Callback scope.
         * @param {Number} [priority=Roc.Event.DEFAULT] Callback priority.
         * @return {Number} Returns listener uniq identifier `uid`.
         */
        on: function(name, cb, scope, priority) {

            var self = this;

            if (typeof priority === 'string') {
                priority = _priorities[priority];
            }

            priority = priority || self.defaultPriority;

            if (typeof self._events[name] === 'undefined') {
                self._events[name] = {};
            }

            if (typeof self._events[name]['p' + priority] === 'undefined') {
                self._events[name]['p' + priority] = [];
            }

            if (typeof self.pqueue[name] === 'undefined') {
                self.pqueue[name] = [];
            }

            if (self.pqueue[name].indexOf(priority) === -1) {
                self.pqueue[name].push(priority);
                self.pqueue[name].sort(_numSort);
            }
            scope = scope || window;
            self.equeue['u' + (++self.uids)] = [name, priority, cb, scope];
            self._events[name]['p' + priority].push(self.uids);
            return self.uids;
        },

        has: function(name) {
            return _count_listeners(this, name);
        },

        /**
         * Unregister listener for given listener uniq identifier (returns by `on` method).
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this);
         *      // ...
         *      this.off(eid);
         *
         * @param {Number} uid Event listener uniq identifier (returns by `on` method).
         * @return {Mixed} Returns false if error else return event listener count.
         */
        off: function(uid) {

            var self = this,
                e = self.equeue['u' + uid];

            if (e) {

                var cb,
                    length,
                    name = e[0],
                    priority = e[1],
                    listeners = self._events[name]['p' + priority];

                listeners.splice(listeners.indexOf(uid), 1);
                length = listeners.length;

                delete self.equeue['u' + uid];

                if (!length) {
                    // cleaning hash tables
                    delete self._events[name]['p' + priority];
                    self.pqueue[name].splice(self.pqueue[name].indexOf(priority), 1);
                    if (!self.pqueue[name].length) {
                        delete self.pqueue[name];
                        delete self._events[name];
                        return 0;
                    }
                }
                return _count_listeners(self, name);
            }
            return false;
        }
    });

}());

/**
 * @class Roc.Templates
 * @extends Roc
 * @requires Handlebars
 */
Roc.Templates = new (function() {

    'use strict';

    // private
    var _ns = Roc,
        _handlebars = Handlebars,
        _request = _ns.Request;

    _handlebars.currentParent = [];

    /**
     * @ignore
     * Render given configuration element.
     *
     * ### Sample usage
     * 
     * Where items are for example:
     * items: [{ xtype: 'button', config: { text: 'Hello' }}]
     */
    // <unstable> Use it carefully. Still testing performance memory/cpu.
    _handlebars.registerHelper('render', function(config) {

        var html,
            parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = new (_ns.xtype(config.xtype))(config),
            renderer = ((parent.renderer && parent.renderer.el) ? parent.abstractRenderer : parent.renderer);

        /*
            abstractRenderer is used for calling afterrender event for item compile after
            the parent renderer has been rendered.
            For example: the list renders her children after render itself so we need to use
            the abstract renderer.
            !TODO We should store the item inside parents collections to be able to delete it or retrieve it.
        */
        if (!renderer) {
            renderer = parent.abstractRenderer = new _ns.Event();
        }

        html = item.compile(renderer.config, renderer);
        return new _handlebars.SafeString(html);
    });
    // </unstable>

    /**
     * @ignore
     * Get current config from given item children index.
     *
     * ### Sample usage
     * 
     */
    _handlebars.registerHelper('config', function(context, options) {

        var parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = (parent.items ? parent.items[context] : undefined);

        if (typeof item === 'object' && item.config) {
            return options.fn(item.config);
        }
        return options.fn(this);

    });

    // @ignore !TODO permettre d'avoir la config d'un item a partir de son index et/ou id
    /*_handlebars.registerHelper('getconfig', function() {

    });*/

    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });

    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });

    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });

    return _ns.subclass({

        /**
         * Create new templates class.
         */
        constructor: function() {
            this.path = '';
        },

        /**
         * Set path for loading external template files.
         * Used by {@link Roc.App#require application} class.
         * @param {String} path Path.
         */
        setPath: function(path) {
            this.path = path;
        },

        /**
         * Get template by given name.
         * @param {String} name Template name.
         * @return {Function} Template function.
         */
        get: function(name) {
            return _handlebars.templates[name];
        },

        /**
         * Compile given source or retrieve the template.
         * @param {Mixed} source The source.
         * @return {Function} Function corresponding to the given source.
         */
        compile: function(source) {

            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }

            return function() {
                return source;
            };
        }
    });
}())();

/**
 * @class Roc.Settings
 * @extends Roc.Event
 * @singleton
 *
 * ### Usage example

    var session = new Roc.Settings({
        username: 'mrsmith',
        logged: true,
        city: 'Paris',
        country: 'France'
    }, {
        storage: Roc.Storage,
        storageKeys: ['city', 'country']
    });

    session.on('loggedchanged', function(s, n, value, oldValue) {
        if (value === false) {
            // show login
        } else {
            // hide login
        }
    }, this);

    session.set('logged', false);

 *
 */
Roc.Settings = (function() {

    'use strict';

    var _ns = Roc,
        _parent = _ns.Event;

    function with_storage(self, name) {
        if (self.storage) {
            if (self.storageKeys) {
                return (self.storageKeys.indexOf(name) !== -1);
            }
            return true;
        }
        return false;
    }

    return _parent.subclass({

        /**
         * New Settings.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(config, opts) {

            var self = this;

            self.vars = (config ? config : {});

            if (opts && opts.storage) {
                self.storage = opts.storage;
                if (opts.storageKeys) {
                    self.storageKeys = opts.storageKeys;
                    self.storageKeys.forEach(function(key) {
                        if (typeof self.vars[key] !== 'undefined') {
                            self.storage.setItem(key, self.vars[key]);
                            delete self.vars[key];
                        }
                    });
                } else {
                    delete self.vars;
                }
            }

            _parent.prototype.constructor.apply(self, arguments);
        },

        /**
         * Set value for given name.
         * @param {String} name Name.
         * @param {Mixed} value Value.
         * @param {Boolean} silent True to disable fire events. Defaults to false.
         * @fires settingschanged
         * @fires `name`changed
         */
        set: function(name, value, silent) {

            var self = this,
                oldValue = this.get(name);

            if (with_storage(self, name)) {
                self.storage.setItem(name, value);
            } else {
                self.vars[name] = value;
            }

            if (silent !== true) {
                self.fire('settingchanged', self, name, value, oldValue);
                self.fire(name + 'changed', self, name, value, oldValue);
            }
        },

        /**
         * Get value from given name.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        get: function(name, defaultValue) {

            var self = this;

            if (with_storage(self, name)) {
                return self.storage.getItem(name, defaultValue);
            } else if (typeof self.vars[name] !== 'undefined') {
                return self.vars[name];
            }

            return defaultValue;
        },

        /**
         * Get value from given name and delete it if exists.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        pop: function(name, defaultValue) {

            var self = this,
                value = self.get(name, defaultValue);

            self.empty(name);

            return value;

        },

        /**
         * Gets value from given names.
         *
         * ## Sample usage
         *
         *      var loginUrl = settings.gets('serverUrl', 'loginPath').join('');
         *
         * @param {String...} names Variable names.
         * @return {Array} Values for given names put into array.
         */
        gets: function() {

            var result = [],
                i = -1,
                length = arguments.length;

            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        },

        /**
         * @fires nameempty
         * Empty given variable name.
         * @param {String} name Variable name.
         * @param {Boolean} silent True to disable fire events. Defaults to false.
         */
        empty: function(name, silent) {

            var self = this;

            if (with_storage(self, name)) {
                self.storage.removeItem(name);
            }

            if (typeof self.vars[name] !== 'undefined') {

                var oldValue = self.vars[name];

                delete self.vars[name];
                if (silent !== true) {
                    self.fire(name + 'empty', self, name, oldValue);
                    self.fire(name + 'changed', self, name, undefined, oldValue);
                }
            }
        }
    });

}());

/**
 * @class Roc.App
 * @extends Roc
 *
 * ### Sample usage:
 *
    var app = new Roc.App({
        onready: function() {
            var main = new App.controllers.Main();
        },
        defaultRoute: '/home',
        engine: 'JqueryMobile',
        ui: 'c'
    });
 *
 */
Roc.App = (function() {

    'use strict';

    var _ns = Roc,
        _debug = _ns.debug,
        _storage = _ns.Storage,
        _engines = _ns.engines;

    _ns.global = new _ns.Event();

    _ns.global.on = function(eventName, callback, scope) {

        if (['beforeready', 'ready'].indexOf(eventName) !== -1 &&
            ['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
            scope = scope || this;
            setTimeout(function() {
                callback.call(scope);
            }, 1);
        } else {
            return _ns.Event.prototype.on.apply(this, arguments);
        }

    };

    function _get_engine(name, defaultEngine) {

        var engine = _engines[name];

        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    }

    function _register_listener(self) {

        function ready() {
            if (!self.domReady) {
                document.removeEventListener('DOMContentLoaded', ready, false);
            }

            self.domReady = true;
            _ns.global.fire('beforeready');
            if (self.config && self.config.onready) self.config.onready();
            _ns.global.fire('ready');
            _ns.History.start();
        }

        if (['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
            self.domReady = true;
            setTimeout(ready, 10);
        } else {
            document.addEventListener('DOMContentLoaded', ready, false);
        }
    }

    /**
     * @cfg {Function} onready Callback when application ready to start.
     */

    /**
     * @cfg {String} engine Engine name. Defaults to 'Homemade'.
     */

    /**
     * @cfg {String} defaultRoute (optional) Defaults application route.
     */

    /**
     * @cfg {Boolean} debug (optional) Enables or disables debug mode.
     */

    /**
     * @cfg {Object} require (optional) Required components.
     *
     * ## Sample usage:
     *
     *      require: {
     *          templates: [
     *              'home',
     *              'menu',
     *              ...
     *          ]
     *      }
     *
     */

    /**
     * @cfg {String} ui (optional) Defaults ui for all components (can be override by component configuration).
     */
    return _ns.subclass({

        defaultEngine: 'Homemade',
        domReady: false,

        /**
         * Create new application.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _ns.History.setDefaultRoute(self.config.defaultRoute);
            }
            _register_listener(self);
            // Setting default engine to global configuration object.
            _ns.config.engine = _get_engine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _ns.config.ui = self.config.ui;
            }
        }
    });

})();

/**
 * @private
 * @class Roc.events.Abstract
 * @extends Roc
 * @xtype events.abstract
 *
 * Abstract event class. Allows DOM events to be merged together or standalone.
 *
 * ## Extending abstract class
 *
 *        Roc.events.Keypress = (function() {
 *
 *            var _parent = Roc.events.Abstract;
 *
 *            return _parent.subclass({
 *
 *                xtype: 'events.keypress',
 *                eventName: 'keypress',
 *                globalFwd: window,
 *
 *                constructor: _parent.prototype.constructor
 *
 *            });
 *
 *        }());
 *
 */
Roc.events.Abstract = (function() {

    'use strict';

    var _ns = Roc,
        _debug = _ns.debug,
        _eventsAttached = {},
        _events = {},
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    function _global_handler(name) {
        return function(e) {

            var handler = _events[name][e.target.id];

            if (handler) {
                handler(e);
            }
        };
    }

    function _global_attach(self) {

        if (!_eventsAttached[self.eventName]) {
            _eventsAttached[self.eventName] = true;
            self.globalFwd.addEventListener(self.eventName,
                _global_handler(self.eventName), self.useCapture);
        }
        if (!self.el.id || !self.el.id.length) {
            self.el.id = _ns.Selector.generateId();
        }
        if (!_events[self.eventName]) {
            _events[self.eventName] = {};
        }
        _events[self.eventName][self.el.id] = self.handler;

    }

    function _global_detach(self) {

        delete _events[self.eventName][self.el.id];
        if (_eventsAttached[self.eventName] &&
            !_events[self.eventName].length) {
            _eventsAttached[self.eventName] = false;
            self.globalFwd.removeEventListener(self.eventName,
                _global_handler, self.useCapture);
        }

    }

    return _ns.subclass({

        xtype: 'events.abstract',

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        useCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {String} eventName Browser event name.
         * @property {String} eventName Browser event name.
         * Should be override by super class or when constructor called.
         */
        eventName: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * @property {Object} globalFwd Global forward scope.
         * Used when we want to listen event on window, and
         * forward to target.
         */
        globalFwd: false,

        /**
         * New abstract event instance.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach',
                'globalFwd'
            ]);
            if (!self.eventName && opts.eventName) {
                self.eventName = opts.eventName;
            }
            self.defaultEl = opts.el || window;
            self.defaultScope = (opts.scope ? opts.scope : self);
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_attach(self);
                } else {
                    self.el.addEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_detach(self);
                } else {
                    self.el.removeEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        }
    });

})();

/**
 * @class Roc.events.Click
 * @extends Roc
 * @xtype events.click
 *
 * Click event class. Also remove 300ms delay for touch device.
 *
 * ## Example
 *
 *       var clickEvent = new Roc.events.Click({
 *           autoAttach: true,
 *           el: document.getElementById('mybutton'),
 *           scope: this,
 *           handler: showPopup
 *       });
 *
 */
Roc.events.Click = (function() {

    'use strict';

    // private
    var _ns = Roc;
        //_isMobile = ('ontouchstart' in document.documentElement)


    function _no_handler() {
        console.warn('click', 'No handler is define !');
    }

    function _stop_scroll(e) {
        e.preventDefault();
    }

    // public
    return _ns.subclass({

        xtype: 'events.click',

        /**
         * @cfg {Boolean} disableScrolling (optional) Enable or disable scrolling on attached element.
         * Defaults to `false`.
         */
        defaultDisableScrolling: false,

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        defaultUseCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * New click event.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.disableScrolling = opts.disableScrolling || self.defaultDisableScrolling;
            self.useCapture = opts.useCapture || self.defaultUseCapture;
            _ns.utils.apply(self, opts, [
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);

            /**
             * @property {Boolean} attached True if event is attached.
             */
            self.attached = false;

            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);

            self.el.addEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.addEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }

            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;

            self.el.removeEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.removeEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        }
    });
}());

/**
 * @class Roc.events.Scroll
 * @extends Roc
 * @xtype events.scroll
 *
 * Scroll event class.
 *
 * ## Example
 *
 *       var scrollEvent = new Roc.events.Scroll({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: loadMoreItem
 *       });
 *
 * @deprecated If you use Roc.physics.Scroll, this event won't be fire.
 */
Roc.events.Scroll = (function() {

    'use strict';

    // private
    var _lastEvent,
        _intervalId,
        _ns = Roc,
        _debug = _ns.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,//_window.screen.availHeight,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _ns.Event();

    function _noHandler() {
        _debug.error('scroll', '[Roc.events.Scroll] No handler is define !');
    }

    function _poolHandler() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    }

    function _windowScrollListener() {
        _scrolled = true;
        _lastEvent = event;
    }

    function _windowResizeListener() {
        _screenHeight = _doc.height;//_window.screen.availHeight;
    }



    // public
    return _ns.subclass({

        xtype: 'events.scroll',
        defaultInterval: 167, // 60fps
        useCapture: false,
        autoAttach: false,
        // defaults
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.interval = opts.interval || self.defaultInterval;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _intervalId = _window.setInterval(_poolHandler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('scroll', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;

            var length = _events.off(self.euid);

            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();

/**
 * @class Roc.events.Resize
 * @extends Roc
 * @xtype events.resize
 *
 * Resize event class.
 *
 * ## Example
 *
 *       var resizeEvent = new Roc.events.Resize({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: updateLayout
 *       });
 *
 */
Roc.events.Resize = (function() {

    'use strict';

    // private
    var //_lastEvent,
        //_intervalId,
        _window = window,
        _eventsAttached = false,
        //_resized = false,
        _ns = Roc,
        _debug = _ns.debug,
        _events = new _ns.Event();

    function _no_handler() {
        _debug.warn('events.resize', 'No handler is define !');
    }

    // function _pool_handler() {
    //     if (_resized === true) {
    //         _resized = false;
    //         _events.fire('resize', _lastEvent);
    //     }
    // }

    function _window_resize_listener(e) {
        // _resized = true;
        // _lastEvent = e;
        _events.fire('resize', e);
    }

    // public
    return _ns.subclass({

        xtype: 'events.resize',
        //defaultInterval: 500,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            //self.interval = opts.interval || self.defaultInterval;
            self.attached = false;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _window_resize_listener, false);
                //_intervalId = _window.setInterval(_pool_handler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ? handler : self.defaultHandler);
            self.euid = _events.on('resize', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;

            var length = _events.off(self.euid);

            delete self.handler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _window_resize_listener, false);
            //_window.clearInterval(_intervalId);
        }
    });
})();

/**
 * var nobounce = new Roc.events.Nobounce({
     className: 'nobounce', // optional
     autoAttach: true, // optional
     el: document // optional
   });
 * nobounce.attach(document);
 * nobounce.detach(document);
 */
Roc.events.Nobounce = (function() {

    // private
    var _lastEvent,
        _ns = Roc,
        $ = _ns.Selector,
        _doc = document,
        _parent = _ns;

    var _isParentNoBounce = function(el, className) {
        while (el) {
            if ($.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _disableBounce = function(e) {
        if (_isParentNoBounce(e.target, this.className)) {
            e.preventDefault();
        }
    };

    // public
    return _parent.subclass({

        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.autoAttach = opts.autoAttach || self.defaultAutoAttach;
            self.className = opts.className || self.defaultClassName;
            self.el = opts.el || self.defaultEl;
            self.attached = false;
            self.handler = _disableBounce.bind(self);
            if (self.autoAttach === true) {
                self.attach(self.el);
            }
        },

        attach: function(cmp) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.el;
            self.el.addEventListener('touchmove', self.handler, false);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;
            self.el.removeEventListener('touchmove', self.handler, false);
        }
    });
})();

/**
 * @class Roc.events.Focus
 * @extends Roc.events.Abstract
 * @xtype events.focus
 *
 * Input focus event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var focusEvent = new Roc.events.Focus({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on focus called
 *             Roc.Selector.addClass(focusEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
Roc.events.Focus = (function() {

    'use strict';

    // private
    var _ns = Roc,
        //isAndroid = (navigator.userAgent.indexOf('Android ') !== -1),
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.focus',

        /**
         * @readonly
         * @property {String} eventName Browser event name.
         */
        eventName: 'focusin',//(isAndroid === true ? 'focus' : 'focusin'),

        /**
         * @readonly
         * @property {Object} globalFwd Global forward scope.
         */
        globalFwd: window,//(isAndroid === true ? undefined : window),

        /**
         * New focus event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class Roc.events.Blur
 * @extends Roc.events.Abstract
 * @xtype events.blur
 *
 * Input blur event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var blurEvent = new Roc.events.Blur({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on blur called
 *             Roc.Selector.removeClass(blurEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
Roc.events.Blur = (function() {

    // private
    var _ns = Roc,
        //isAndroid = (navigator.userAgent.indexOf('Android ') !== -1),
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.blur',

        /**
         * @readonly
         */
        eventName: 'focusout',//(isAndroid === true ? 'blur' : 'focusout'),

        /**
         * @readonly
         */
        globalFwd: window,//(isAndroid === true ? undefined : window),

        /**
         * New blur event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class Roc.events.Keypress
 * @extends Roc.events.Abstract
 * @xtype events.keypress
 *
 * Input keypress event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var keypressEvent = new Roc.events.Keypress({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on keypress called
 *             if (e.keyCode === 13) {
 *                 this.form.submit();
 *             }
 *         }
 *     });
 *
 */
Roc.events.Keypress = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.keypress',

        /**
         * @readonly
         */
        eventName: 'keypress',

        /**
         * @readonly
         */
        globalFwd: window,

        /**
         * New keypress event.
         */
        constructor: function(opts) {

            var self = this;


            if (opts.el) {

                var tagname = opts.el.tagName;

                if (tagname !== 'INPUT' && tagname !== 'TEXTAREA') {
                    opts.el.setAttribute('tabindex', '1');
                }
            }

            _parent.prototype.constructor.call(self, opts);

        }
    });
}());

describe("Roc", function() {

	var coreClass = Roc;
	var cls;

	beforeEach(function() {
		cls = undefined;
	});

	describe("subclass", function() {

		it("should has subclass method", function() {

			var classA = coreClass.subclass();

			expect(typeof classA.subclass).toBe('function');
		});

		it("should herits from attributes", function() {

			var classA = coreClass.subclass({
				exists: true,
				will_override: false,
				not_override: true
			});
			var classB = classA.subclass({
				will_override: true
			});
			var instance = new classB();

			expect(classA.prototype.will_override).toBe(false);
			expect(classB.prototype.will_override).toBe(true);
			expect(instance.will_override).toBe(true);
			expect(instance.exists).toBe(true);
			expect(instance.not_override).toBe(true);

		});

		it("should call parent constructor", function() {

			var classA = coreClass.subclass({

				parent_called: false,

				constructor: function() {
					this.parent_called = true;
				}
			});
			var classB = classA.subclass({
				constructor: function() {
					classA.prototype.constructor.apply(this, arguments);
				}
			});
			var instance = new classB();

			expect(instance.parent_called).toBe(true);

		});

		it("should be able to have multiple herits", function() {

			var classA = coreClass.subclass({
				still_there: true,
				from_who: function() {
					return 'A';
				}
			});
			var classB = classA.subclass({
				from_who: function() {
					return 'B';
				}
			});
			var classC = classB.subclass({
				from_who: function() {
					return 'C';
				}
			});
			var instance = new classC();

			expect(instance.still_there).toBe(true);
			expect(instance.from_who()).toBe('C');

		});

		it("should register with xtype", function() {

			var classA = coreClass.subclass({
				xtype: 'test-xtype',
				my_super_class: true
			});
			var _classA = coreClass.xtype('test-xtype');

			expect(classA).toBe(_classA);
			expect(_classA.prototype.my_super_class).toBe(true);

		});

		it("should create from xtype", function() {

			var classA = coreClass.subclass({
				xtype: 'test-xtype2',
				my_super_class: true
			});
			var instance = coreClass.create('test-xtype2');

			expect(instance.my_super_class).toBe(true);

		});

		it("should herits with extend", function() {

			var classA = coreClass.extend(coreClass, {
				xtype: 'herits-with-extend',
				has_extend: false
			});
			var classB = coreClass.extend('herits-with-extend', {
				xtype: 'herits-with-extend2',
				has_extend: true
			});
			var instance = coreClass.create('herits-with-extend2');

			expect(instance.has_extend).toBe(true);
		});

		it("should not extend from unknown xtype/class", function() {

			expect(function() {
				coreClass.extend('unknown');
			}).toThrowError('Class or xtype does not exists.');

			expect(function() {
				coreClass.extend({});
			}).toThrowError('Class or xtype does not exists.');

		});

		it("should have parent attribute", function() {

			var classA = coreClass.extend(coreClass, {
				is_base_class: true
			});

			var classB = coreClass.extend(classA, {
				is_base_class: false
			});

			expect(classB.parent.is_base_class).toBe(true);

		});

	});

});

describe("Roc.Event", function() {

	var eventClass = Roc.Event;
	var evt;

	beforeEach(function() {
		evt = undefined;
	});

	describe("events", function() {

		it("should listen event", function() {

			var called = false;

			evt = new eventClass();
			evt.on('test', function() {
				called = true;
			});
			evt.fire('test');
			expect(called).toBe(true);
		});

		it("should unlisten event", function() {

			var nb_call = 0;

			evt = new eventClass();

			var eid = evt.on('test', function() {
				++nb_call;
			});

			evt.fire('test');
			evt.off(eid);
			evt.fire('test');
			expect(nb_call).toEqual(1);

		});

		it("should count listeners", function() {
			evt = new eventClass();
			expect(evt.has('test')).toEqual(0);
			evt.on('test', function() {
				called = true;
			});
			expect(evt.has('test')).toBe(1);
		});

		it("should prevent next listener to be fired", function() {

			var nb_call = 0;

			evt = new eventClass();
			evt.on('test', function() {
				++nb_call;
				return false;
			});
			evt.on('test', function() {
				++nb_call;
			});
			evt.fire('test');
			expect(nb_call).toEqual(1);
		});

		it("should remove listener inside listener", function() {

			var nb_call = 0;

			evt = new eventClass();
			var uid = evt.on('test', function() {
				++nb_call;
				evt.off(uid);
			});
			evt.fire('test');
			evt.fire('test');
			expect(nb_call).toEqual(1);
		});

		it("should fire with priority", function() {

			var first = true;

			evt = new eventClass();
			evt.on('test', function() {
				expect(first).toBe(false);
			});
			evt.on('test', function() {
				expect(first).toBe(true);
				first = false;
			}, this, 'CORE');
			evt.fire('test');
		});

	});

	describe("listeners", function() {

		it("should fire event", function() {

			var nb_call = 0;

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					test: function() {
						++nb_call;
					}
				}
			});
			evt.fire('test');
			expect(nb_call).toEqual(1);
		});

		it("should not listen event without hasListeners", function(done) {

			var called = false;

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					test2: function() {
						called = true;
					},
					test: function() {
						expect(called).toBe(false);
						done();
					}
				}
			});
			evt.fire('test2');
			evt.fire('test');
		});

		it("should fire with scope as first argument", function(done) {

			var test_scope = {
				test: true
			};

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					scope: test_scope,
					test: function(scope, value) {
						expect(scope.test).toBe(true);
						done();
					}
				}
			});
			evt.fireScope('test', true);
		});

		it("should fire with given scope", function(done) {

			var test_scope = {
				test: true
			};

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					scope: test_scope,
					test: function(scope, value) {
						expect(this.test).toBe(true);
						done();
					}
				}
			});
			evt.fireScope('test', true);
		});

		it("should fire with priority", function(done) {

			var first = true;

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					priority: 'VIEWS',
					test: function() {
						expect(first).toBe(false);
						done();
					}
				}
			});
			evt.on('test', function() {
				expect(first).toBe(true);
				first = false;
			}, this, 'CORE');
			evt.fire('test');
		});

		it("should has or not listener", function() {
			evt = new eventClass({
				hasListeners: ['exists'],
				listeners: {
					exists: function() {}
				}
			});
			expect(evt.hasListener('exists')).toBe(true);
			expect(evt.hasListener('does_not_exist')).toBe(false);
		});

		it("should return listener", function() {
			evt = new eventClass({
				hasListeners: ['exists'],
				listeners: {
					exists: function() {}
				}
			});
			expect(evt.getListener('exists')).toBeDefined();
		});

	});

});
describe("Roc.Selector", function() {

	var $ = Roc.Selector;

	describe("DOM", function() {

		var el;

		beforeEach(function() {
			el = document.createElement('div');
			el.id = 'tests-selector-el';
			document.body.appendChild(el);
		});

		afterEach(function() {
			el.parentNode.removeChild(el);
		});

		it("should add class", function() {
			$.addClass(el, 'test');
			expect(el.className).toContain('test');
		});

		it("should remove class", function() {
			$.addClass(el, 'test');
			expect(el.className).toContain('test');
			$.removeClass(el, 'test');
			expect(el.className).not.toContain('test');
		});

		it("should have css class", function() {
			$.addClass(el, 'test');
			expect($.hasClass(el, 'test')).toBe(true);
		});

		it("should add multiple css classes", function() {
			$.addClasses(el, 'test1 test2 test3');
			expect(el.className).toContain('test1');
			expect(el.className).toContain('test2');
			expect(el.className).toContain('test3');
		});

		it("should remove multiple css classes", function() {
			$.addClasses(el, 'test1 test2 test3');
			expect(el.className).toContain('test1');
			expect(el.className).toContain('test2');
			expect(el.className).toContain('test3');
			$.removeClasses(el, 'test1 test2 test3');
			expect(el.className).not.toContain('test1');
			expect(el.className).not.toContain('test2');
			expect(el.className).not.toContain('test3');
		});

		it("should have multiple css classes", function() {
			$.addClasses(el, 'test1 test2 test3');
			expect($.hasClasses(el, 'test1 test2 test3')).toBe(true);
		});

		it("should get element from selector", function() {
			expect($.get('#tests-selector-el')).toBe(el);
		});

		it("should get element from selector with root", function() {
			expect($.get('#tests-selector-el', document.body)).toBe(el);
		});

		it("should set css properties to element", function() {
			$.css(el, {
				'background-image': 'none',
				backgroundColor: 'white',
				color: 'black',
				transform: 'rotateX(40deg)'
			});
			expect(el.style.color).toEqual('black');
			expect(el.style.backgroundColor).toEqual('white');
			expect(el.style.backgroundImage).toEqual('none');
			expect(el.style[$.prefix.cssStyle + 'Transform']).toEqual('rotateX(40deg)');
		});

		it("should remove element", function() {

			var el2 = document.createElement('div');

			el2.id = 'tests-selector-el2';
			document.body.appendChild(el2);
			expect($.get('#tests-selector-el2')).not.toBe(null);
			$.removeEl(el2);
			expect($.get('#tests-selector-el2')).toBe(null);
		});

		it("should add HTML to element", function() {
			expect(el.innerText).toBe('');
			$.addHtml(el, '<i>test</i>');
			expect(el.innerText).toBe('test');
			expect(el.innerHTML).toBe('<i>test</i>');
		});

		it("should update HTML from element", function() {
			expect(el.innerText).toBe('');
			$.updateHtml(el, '<i>test</i>');
			expect(el.innerText).toBe('test');
			expect(el.innerHTML).toBe('<i>test</i>');
		});

		it("should remove HTML from element", function() {
			el.innerHTML = 'test';
			expect(el.innerText).toBe('test');
			$.removeHtml(el);
			expect(el.innerText).toBe('');
		});

		it("should find parent from callback", function() {

			el.innerHTML = '<div class="parent-we-wanna-find">' +
				'<ul><li></li><li><a href="" class="start-from-here">test</a></li></ul></div>';

			var startEl = $.get('.start-from-here', el);
			var parentEl = $.findParent(startEl, function(el) {
				if ($.hasClass(el, 'parent-we-wanna-find')) return el;
				return false;
			});

			expect($.hasClass(parentEl, 'parent-we-wanna-find')).toBe(true);
		});

		it("should find parent from tag", function() {

			el.innerHTML = '<div class="parent-we-wanna-find">' +
				'<ul><li></li><li><a href="" class="start-from-here">test</a></li></ul></div>';

			var startEl = $.get('.start-from-here', el);
			var parentEl = $.findParentByTag(startEl, 'div');

			expect($.hasClass(parentEl, 'parent-we-wanna-find')).toBe(true);
		});

		it("should find parent from css class", function() {

			el.innerHTML = '<div class="parent-we-wanna-find">' +
				'<ul><li></li><li><a href="" class="start-from-here">test</a></li></ul></div>';

			var startEl = $.get('.start-from-here', el);
			var parentEl = $.findParentByClass(startEl, 'parent-we-wanna-find');

			expect($.hasClass(parentEl, 'parent-we-wanna-find')).toBe(true);
		});

	});

});

describe("Roc.Settings", function() {

	var settingsClass = Roc.Settings;

	describe("without storage", function() {

		var settings;

		beforeEach(function() {
			settings = undefined;
		});

		it("should get value from known name", function() {
			settings = new settingsClass({
				is_correct: true
			});
			expect(settings.get('is_correct')).toBe(true);
			expect(settings.get('is_correct', 'default')).toBe(true);
		});

		it("should get default value from unknown name", function() {
			settings = new settingsClass({
				is_not_correct: false
			});
			expect(settings.get('is_correct', 'default')).toBe('default');
			expect(settings.get('is_correct', true)).toBe(true);
		});

		it("should get undefined from unknown name without default value", function() {
			settings = new settingsClass({
				is_not_correct: false
			});
			expect(settings.get('is_correct')).not.toBeDefined();
		});

		it("should get multiple known values", function() {
			settings = new settingsClass({
				one: 'One',
				two: 'Two',
				three: 'Three'
			});
			expect(settings.gets('one', 'two', 'three').join(', ')).toBe('One, Two, Three');
		});

		it("should pop value from name", function() {
			settings = new settingsClass({
				is_still_here: true
			});
			expect(settings.pop('is_still_here')).toBe(true);
			expect(settings.pop('is_still_here')).not.toBe(true);
			expect(settings.pop('is_still_here')).not.toBeDefined();
		});

		it("should pop value from name with default value", function() {
			settings = new settingsClass({
				is_still_here: true
			});
			expect(settings.pop('is_still_here', 'default')).toBe(true);
			expect(settings.pop('is_still_here', 'default')).toBe('default');
		});

		it("should empty given name value", function() {
			settings = new settingsClass({
				one: true,
				two: true
			});
			expect(settings.get('one')).toBe(true);
			expect(settings.get('two')).toBe(true);
			settings.empty('one');
			expect(settings.get('one')).not.toBe(true);
			expect(settings.get('one')).not.toBeDefined();
			expect(settings.get('two')).toBe(true);
		});

		it("should fire empty events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('oneempty', function(_, name, oldValue) {
				console.log('empty: ', arguments);
				expect(name).toBe('one');
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.on('onechanged', function(_, name, value, oldValue) {
				console.log('changed: ', arguments);
				expect(name).toBe('one');
				expect(value).not.toBeDefined();
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.empty('one');
			expect(calls).toEqual(2);
		});

		it("should not fire empty events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('oneempty', function(_, name, oldValue) {
				console.log('empty: ', arguments);
				++calls;
			});
			settings.on('onechanged', function(_, name, value, oldValue) {
				console.log('changed: ', arguments);
				++calls;
			});
			settings.empty('one', true);
			expect(calls).toEqual(0);
		});

		it("should set value to given name", function() {
			settings = new settingsClass({
				one: true
			});
			expect(settings.get('one')).toBe(true);
			settings.set('one', false);
			expect(settings.get('one')).toBe(false);
		});

		it("should fire changed events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('settingchanged', function(_, name, value, oldValue) {
				expect(name).toBe('one');
				expect(value).toBe(false);
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.on('onechanged', function(_, name, value, oldValue) {
				expect(name).toBe('one');
				expect(value).toBe(false);
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.set('one', false);
			expect(calls).toEqual(2);
		});

		it("should not fire changed events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('settingchanged', function() {
				++calls;
			});
			settings.on('onechanged', function() {
				++calls;
			});
			settings.set('one', false, true);
			expect(calls).toEqual(0);
		});

	});

});

Handlebars.registerPartial("id", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.id
 *
 * Handlebars `id` partial.
 *
 * This partial allows you to pass any id as string from `id` variable inside component configuration.
 *
 * _Note_: You must use this partial in any templates which is link to a component to allow Roc retreive the HTMLElement by its own.
 *
 * ## Sample usage
 *
 *		{{> id}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			id: "html-element-id"
 *		};
 *
 * #### Handlebars template
 *
 *		<div{{> id}}></div>
 *
 * #### Output
 *
 * 		<div id="html-element-id"></div>
 *
 */
Handlebars.registerPartial("css_class", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.css_class) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.css_class); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.css_class) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
Handlebars.registerPartial("cssClass", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.cssClass) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.cssClass); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.cssClass) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.css_class
 *
 * Handlebars `css_class` partial.
 *
 * This partial allows you to pass any CSS classes as string from `cssClass` or `css_class` variable inside component configuration.
 *
 * ## Sample usage
 *
 * 		{{> css_class}}
 * 		{{! or }}
 * 		{{> cssClass}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			css_class: "raw css classes"
 *		};
 *
 * #### Handlebars template
 *
 *		<div class="{{> css_class}}"></div>
 *
 * #### Output
 *
 * 		<div class="raw css classes"></div>
 *
 */
Handlebars.registerPartial("style", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.style) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.style); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.style
 *
 * Handlebars `style` partial.
 *
 * This partial is used to pass inline CSS style for HTMLElement.
 *
 * ## Sample usage
 *
 * 		{{> style}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			config: {
 *				title: 'Profile',
 *				style: 'background: #000; color: #fff;'
 *			}
 *		};
 *
 * #### Handlebars template
 *
 *		<div{{> id}}{{> style}}>
 *			{{#title}}<h2>{{.}}</h2>{{/title}}
 *		</div>
 *
 * #### Output
 *
 * 		<div id="fs1" style="background: #000; color: #fff;">
 * 			<h2>Profile</h2>
 * 		</div>
 *
 */

/**
 * @class Roc.handlebars.helpers.safe
 * @alternateClassName Roc.handlebars.helpers.raw
 *
 * Handlebars `safe`, `raw` helper.
 * By default Handlebars will sanetize HTML from all output.
 *
 * This helper allows you to force Handlebars to output this string as raw.
 *
 * ## Sample usage:
 *
 *		{{#safe items}}
 *         {{.}}
 *      {{/safe}}
 *      {{! OR }}
 *      {{safe item}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 * 		var config = {
 * 			html: '<p>test</p>'
 * 		};
 *
 * #### Handlebars template
 *
 * 		{{html}}
 * 		{{raw html}}
 * 		{{safe html}}
 *
 * #### Output
 *
 *      &lt;p&gt;test&lt;/p&gt;
 *      <p>test</p>
 *      <p>test</p>
 *
 */
(function() {

    var handlebars = Handlebars;

    handlebars.registerHelper('safe', function(str) {
        return new handlebars.SafeString(str);
    });

    handlebars.registerHelper('raw', function(str) {
        return new handlebars.SafeString(str);
    });

}());

Handlebars.registerPartial("items", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, self=this, helperMissing=helpers.helperMissing, functionType="function", blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var stack1, helper, options;
  stack1 = (helper = helpers.safe || (depth0 && depth0.safe),options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program2(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.items) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.items); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.items) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
/**
 * @class Roc.handlebars.partials.items
 *
 * Handlebars `items` partial.
 *
 * This partial is used to push given items from components configuration into your template as HTML.
 *
 * ## Sample usage
 *
 * 		{{> items}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			config: {
 *				title: 'Profile'
 *			},
 *			items: [{
 *				xtype: 'button',
 *				config: {
 *					text: 'Back',
 *					route: '/home'
 *				}
 *			}]
 *		};
 *
 * #### Handlebars template
 *
 *		<div{{> id}}>
 *			{{> items}}
 *			{{#title}}<h2>{{.}}</h2>{{/title}}
 *		</div>
 *
 * #### Output
 *
 * 		<div id="fs1">
 * 			<button id="fs2" href="#/home">Back</button>
 * 			<h2>Profile</h2>
 * 		</div>
 *
 */

/**
 * @class Roc.implements.events
 * @extends {Object}
 *
 * Usage:
 *
 * 		var example = {
 * 			xtpl: 'container',
 * 			implements: ['events'],
 * 			hasListeners: ['afterrender'],
 * 			listeners: {
 * 				afterrender: function(self) {
 * 					self.events.add('scroll', {
 * 						xtype: 'physics.scroll',
 * 						cmp: this,
 * 						el: this.el,
 * 						scrollable: 'y'
 * 					});
 * 					self.events.get('scroll').resize();
 * 				}
 * 			}
 * 		};
 *
 */

/**
 * Attach all event listeners.
 * @method listen
 */

/**
 * Detach all event listeners.
 * @method unlisten
 */

/**
 * Add new event to component.
 * Example:
 *
 *      this.events.add('scroll', {
 *          xtype: 'physics.scroll',
 *          cmp: this,
 *          el: this.el,
 *          scrollable: 'y'
 *      });
 *
 * @method add
 * @param {String} name Event name. Must be unique else it will not override.
 * @param {Object} opts Event constructor options.
 * @param {String} opts.xtype Event xtype.
 */

/**
 * Remove event from component.
 * Example:
 *
 *     this.events.remove('scroll');
 *
 * @method remove
 * @param  {String} name Event name to remove.
 * @return {Boolean} True if success else false.
 */

/**
 * Get event corresponding to given name.
 * @method get
 * @param  {String} name Event name.
 * @return {Object} Event instance.
 */

/**
 * Attach all events.
 * @method attach
 */

/**
 * Detach all events.
 * @method detach
 */

/**
 * @class Roc.implements.update
 * @extends {Object}
 *
 * Usage:
 *
 * 		var example = {
 * 			xtpl: 'container',
 * 			implements: ['update'],
 * 			hasListeners: ['afterrender'],
 * 			listeners: {
 * 				afterrender: function(self) {
 * 					self.update({}, 'New container items content.');
 * 				}
 * 			}
 * 		};
 *
 */

/**
 * Update component with given configuration and items.
 * Example:
 *
 *      this.update({
 *      	data: data
 *      }, [{
 *      	xtype: 'button',
 *      	config: {
 *      		text: 'OK'
 *      	}
 *      }, '-', {
 *      	xtype: 'button',
 *      	config: {
 *      		text: 'Cancel'
 *      	}
 *      }]);
 *
 * @method update
 * @param {Object} config New component configuration.
 * @param {Mixed} [items] New component items.
 */

/**
 * @class Roc.views.Template
 * @extends Roc.Event
 * @xtype template
 *
 * Base view template class.
 *
 *      var tpl = new Roc.views.Template({
 *          xtpl: 'my-compiled-handlebars-template',
 *          config: {
 *              title: 'Hi there !'
 *          }
 *      });
 *
 *      tpl.compile();
 *      tpl.render('body');
 */
Roc.views.Template = (function() {

    'use strict';

    var _ns = Roc,
        _gconf = _ns.config,
        _parent = _ns.Event,
        _selector = _ns.Selector,
        _handlebars = Handlebars;
        //_observer = _win.MutationObserver || _win.WebKitMutationObserver

    function _registerEngineEvents(self) {

        if (!self.renderer ||
            self.engineEventsRegistered) {
            return false;
        }
        self.engineEventsRegistered = true;

        var parent = self;

        while (parent) {
            if (parent.xtype) {
                _registerEngineListeners(self, parent.xtype);
                parent = _ns.xtype(parent.xtype).parent;
            } else {
                parent = false;
            }
        }
        return true;
    }

    function _registerEngineListeners(self, xtype) {

        var i, e, events = self.engine.getEvents(xtype);

        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    self.renderer.on(i, events[i], self, self.priority.VIEWS);
                } else {
                    self.on(i, events[i], self, self.priority.VIEWS);
                }
            }
        }
    }

    var events = function(parent) {

        return {

            listen: function() {

                var self = this;

                if (!self._eshowid) {
                    self._eshowid = parent.renderer.on('show', self.attach, self);
                    self._ehideid = parent.renderer.on('hide', self.detach, self);
                    self._eremoveid = parent.renderer.on('remove', self.remove, self);
                }
            },

            unlisten: function() {

                var self = this;

                if (self._eshowid) {
                    parent.renderer.off(self._eshowid);
                    parent.renderer.off(self._ehideid);
                    parent.renderer.off(self._eremoveid);

                    delete self._eshowid;
                    delete self._ehideid;
                    delete self._eremoveid;
                }
            },

            add: function(name, opts) {

                var self = this;

                if (!self._events) {
                    self._events = {};
                    self._eventKeys = [];
                    self._elen = 0;
                }
                if (!self._events[name]) {
                    self._events[name] = new (_ns.xtype(opts.xtype))(opts);
                    self._eventKeys.push(name);
                    ++self._elen;
                }
                if (self._elen === 1) {
                    self.listen();
                }
                return self._events[name];
            },

            remove: function(name) {

                var self = this;

                if (self._events && self._events[name]) {
                    self._events[name].detach();
                    delete self._events[name];
                    --self._elen;

                    if (!self._elen) {
                        self.unlisten();
                    }

                    return true;
                }
                return false;
            },

            get: function(name) {
                return this._events[name];
            },

            attach: function() {

                var key,
                    self = this,
                    len = self._elen;

                while (len--) {
                    key = self._eventKeys[len];
                    self._events[key].attach();
                }

                return self;

            },

            detach: function() {

                var key,
                    self = this,
                    len = self._elen;

                while (len--) {
                    key = self._eventKeys[len];
                    self._events[key].detach();
                }

                return self;

            }

        };

    };

    /*
    function _registerEngineOverrides() {
        if (this.engineOverridesRegistered) {
            return false;
        }
        this.engineOverridesRegistered = true;

        var i, o, overrides = this.engine.getOverrides(this.xtype);

        for (i in overrides) {
            o = overrides[i];

            if (typeof o === 'function') {
                this[i] = o;
            }
        }
        return true;
    };
    */

    function tpl_update(self) {

        return function(config, items) {

            if (!self.el) {
                return false;
            }

            self.parentEl = self.el.parentNode;
            self.nextEl = self.parentEl.querySelector('#' + self.el.id + ' + *');
            self.remove();

            if (!config.id) {
                config.id = self.config.id;
            }

            self.data = config;
            self.data.items = [];
            if (items) {
                self.items = items;
            } else {
                self.items = [];
            }

            self.compile();

            if (self.nextEl) {
                self.render(self.nextEl, 'beforebegin');
            } else {
                self.render(self.parentEl, 'beforeend');
            }
            self.fireScope('update', self);
        };

    }

    return _parent.subclass({

        className: 'Roc.views.Template',
        xtype: 'template',

        /**
         * @cfg {String} [xtpl=undefined] Abstract template name.
         */

        /**
         * @cfg {Boolean} [hidden=false] Hidden component.
         */

        /**
         * New template.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {
                template: '',
                config: {},
                items: []
            };

            if (opts.renderTo) {
                /**
                 * @cfg {Mixed} [renderTo] Render to element.
                 * Can be a selector string or a HTMLElement.
                 *
                 *     @example
                 *     renderTo: 'body'
                 *     renderTo: '#myelementid'
                 *     renderTo: document.body
                 */
                self.renderTo = opts.renderTo;
            }

            if (opts.implements) {
                self.implements = self.implements || [];
                self.implements = self.implements.concat(opts.implements);
            }

            /**
             * @cfg {Array} [implements=[]] Additional features implementations.
             * You can implement:
             * <ul>
             *  <li>{@link Roc.implements.events events}: Allows to use event API to manage component events.</li>
             *  <li>{@link Roc.implements.update update}: Add update method to component to update template data and items without creating new component.</li>
             * </ul>
             */
            if (self.implements) {
                if (self.implements.indexOf('events') !== -1) {
                    self.events = events(self);
                }
                if (self.implements.indexOf('update') !== -1) {
                    self.update = tpl_update(self);
                }
            }

            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtpl = opts.xtpl;
            }

            /**
             * @cfg {Object} [config={}] Template configuration.
             * This variable will be the scope while executing template.
             */
            self.config = opts.config || {};
            if (!self.config.id) {
                self.config.id = _selector.generateId();
            }

            if (self.config.hidden === true) {
                self.hidden = true;
            } else {
                self.hidden = false;
            }

            // <testing>
            //delete opts.config;
            //self.config = opts;
            // </testing>

            /**
             * @cfg {Object} [defaults={}] Defaults values for first child level items.
             */
            if (opts.defaults) {
                self.defaults = opts.defaults;
            }

            /**
             * @cfg {Mixed} items (optional) Template children items.
             *
             * ## Different possibilities:
             *
             *     var mypage = new Roc.views.Page({
             *         items: [{
             *             xtype: 'header',
             *             config: {
             *                 title: 'My page'
             *             }
             *         }, {
             *             xtype: 'content',
             *             items: '<h1>My content</h1>'
             *         }, {
             *             xtype: 'footer',
             *             items: [new Roc.views.TabButton({
             *                 ...
             *             }), {
             *                 xtype: 'tabbutton',
             *                 ...
             *             }, 'Raw HTML', ...]
             *         }]
             *     })
             *
             */
            self.items = opts.items || [];
            if (!self.items.indexOf) {
                self.items = [self.items];
            }
            self.data = self.config;
            self.data.items = [];
            //self.data.items = self.items;
            if (!opts.template) {
                opts.template = self.setEngine(opts, _gconf).getTpl(self.xtpl || self.xtype);
            } else {
                self.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }

            /**
             * @cfg {String} ref Reference name to given `refScope`.
             */
            if (opts.ref) {
                self.ref = opts.ref;
            }

            /**
             * @cfg {Object} refScope Reference scope.
             */
            if (opts.refScope) {
                self.refScope = opts.refScope;
            }

            /**
             * @property {Function} tpl Template function.
             * @readonly
             */
            self.tpl = opts.template;

            /**
             * @property {String} html Compiled HTML.
             * @readonly
             */
            self.html = '';
            /**
             * @property {String} renderToEl Render to element
             * @readonly
             */
            self.renderToEl = '';

            /**
             * @property {HTMLElement} el HTML element corresponding to this component.
             * @readonly
             */
            self.el = '';
            self.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(self, arguments);

            if (typeof self.renderTo !== 'undefined') {
                self.compile();
                self.render(self.renderTo);
            }
        },

        getEngine: function(config, gconfig) {

            var self = this;

            if (config && config.engine) {
                return config.engine;
            } else if (self.engine) {
                return self.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }

            return self.engine;
        },

        setEngine: function(config, gconfig) {

            var self = this;

            self.engine = self.getEngine(config, gconfig);

            if (typeof self.engine === 'string') {
                self.engine = new (_ns.xtype(self.engine))();
            } else if (self.engine) {
                self.engine = new self.engine();
            } else {
                console.warn('No engine specified. Use default engine.', self.engine);
                self.engine = {
                    getEvents: function() {},
                    getTpl: function(tplname) {
                        var tpl = _handlebars.templates[tplname];
                        if (!tpl) console.warn('Template "' + tplname + '" does not exists.');
                        return tpl;
                    }
                };
            }

            return self.engine;
        },

        /**
         * Compile template and generate HTML.
         * @fires aftercompile
         */
        compile: function(parentConfig, renderer) {

            var item, tpl,
                self = this,
                i = -1,
                items = self.items,//.slice(0),
                length = self.items.length;

            if (typeof self.tpl !== 'function') {
                throw new Error('no template given');
                return false;
            }

            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _ns.utils.applyIf(self.config, parentConfig, ['ui']);
            }

            while (++i < length) {

                item = items[i];

                if (typeof item === 'object') {
                    if (self.defaults) {
                        _ns.utils.applyIfAuto(item, self.defaults);
                    }
                    if (!item.engine) {
                        item.engine = self.engine.xtype;
                    }
                }

                if (typeof item === 'object' &&
                    (typeof item.xtype === 'string' ||
                    typeof item.xtpl === 'string') &&
                    !(item instanceof _parent)) {
                    if (!item.xtype) {
                        item.xtype = 'template';
                    }
                    //typeof item.__proto__.xtype === 'undefined') {
                    //typeof Object.getPrototypeOf(item).xtype === 'undefined') {
                    item = new (_ns.xtype(item.xtype))(item);
                    item.parent = self;
                }
                if (typeof item === 'string') {
                    tpl = _ns.Templates.compile(item);
                    self.data.items[i] = tpl(self.config);
                } else if (typeof item.compile === 'function') {
                    item.parent = self;
                    self.data.items[i] = item.compile(self.config, renderer || self);
                } else {
                    self.data.items[i] = item;
                }
            }

            _handlebars.currentParent.push(self);

            self.html = self.tpl(self.data);

            _handlebars.currentParent.pop();

            if (!self.renderer) {
                self.renderer = renderer || self;
                if (self.ref && !self.refScope) {
                    self.refScope = self.renderer;
                }
            }
            // @todo Check if this kind of thing is not too much perf killer
            // and double check memory leak.
            // <unstable>
            if (self.ref &&
                !self.refScope[self.ref]) {
                self.refScope[self.ref] = self;
            }
            // </unstable>

            self.__arID = self.renderer.on('afterrender', function() {
                self.el = document.getElementById(self.config.id);
                //self.parentEl = self.el.parentNode;
                self.renderer.off(self.__arID);
                delete self.__arID;
            }, self, self.priority.CORE);

            self.setupListeners([

                /**
                 * @event aftercompile
                 * Fire after component has been compiled.
                 * @param { Roc.views.Template } self The component instance
                 * @param {Mixed} renderer The renderer component instance.
                 */
                'aftercompile',

                /**
                 * @event afterrender
                 * Fire after component has been rendered.
                 * @param {Mixed} scope Given scope
                 * @param { Roc.views.Template } self The component instance
                 * @param {Mixed} renderer The renderer component instance.
                 * This is the instance of the component from which the `render` method has been called.
                 */
                'afterrender'
            ], self.renderer);

            self.fire('aftercompile', self, self.renderer);
            _registerEngineEvents(self);

            return self.html;
        },

        /**
         * Render template to given element and position.
         * position: <!-- beforebegin --><p><!-- afterbegin -->foo<!-- beforeend --></p><!-- afterend -->
         * @fires afterrender
         * @param {Mixed} renderToEl Selector string or HTML element.
         * @param {String} [position='beforeend'] Position to render element at.
         * @chainable
         */
        render: function(renderToEl, position) {

            var self = this;

            position = position || 'beforeend';

            if (typeof renderToEl === 'string') {
                //renderToEl = _ns.Selector.get(renderToEl);
                self.renderToEl = _ns.Selector.get(renderToEl);
            } else {
                self.renderToEl = renderToEl;
            }

            self.renderToEl.insertAdjacentHTML(position, self.html);
            self.fireScope('afterrender', self);

            if (!self.el) {
                self.el = document.getElementById(self.config.id);
            }

            return self;
        },

        /**
         * Remove element from DOM if rendered.
         * @fires remove
         * @chainable
         */
        remove: function() {

            var self = this;

            if (self.el) {
                self.fireScope('remove', self);
                self.el.parentNode.removeChild(self.el);
                delete self.el;
                // @ignore !TODO remove all events automatically self.fire('remove', self);
                // should store all events (like click, resize etc.) inside instance array var
            } else {
                console.warn('Element cannot been removed because it has not been rendered !', this);
            }

            return self;
        },

        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        },

        /**
         * Show element.
         * @param  {Mixed} args Arguments for listeners.
         * @fires show
         * @chainable
         */
        show: function(args) {

            var self = this;

            self.hidden = false;
            /**
             * @event show
             * Fire after component has been showed.
             * @param {Mixed} scope Given scope from listener
             * @param { Roc.views.Template } self Component instance.
             * @param {Mixed} args Given arguments to show (from router for example)
             */
            self.fireScope('show', self, args);
            return self;
        },

        /**
         * Hide element.
         * @param  {Mixed} args Arguments for listeners.
         * @fires hide
         * @chainable
         */
        hide: function() {

            var self = this;

            self.hidden = true;
            /**
             * @event hide
             * Fire after component has been hidden.
             * @param {Mixed} scope Given scope from listener
             * @param { Roc.views.Template } self Component instance.
             */
            self.fireScope('hide', self);
            return self;
        }
    });

}());

Handlebars.templates.test = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<div style=\"display:none;\"";
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
describe("Roc.views.Template", function() {

	var templateClass = Roc.views.Template;
	var template;

	beforeEach(function() {
		template = undefined;
	});

	afterEach(function() {
		if (template.el) template.remove();
	});

	describe("events implements", function() {

		it("should implements events", function() {
			template = new templateClass({
				implements: ['events']
			});
			expect(template.events).toBeDefined();
		});

		it("should add event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});
			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			expect(evt === template.events.get('test')).toBe(true);
		});

		it("should remove event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});
			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			expect(evt === template.events.get('test')).toBe(true);
			template.events.remove('test');
			expect(template.events.get('test')).toBe(undefined);
		});

		it("should listen event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			spyOn(template.events, 'listen').and.callThrough();

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			expect(template.events.listen).toHaveBeenCalled();
		});

		it("should unlisten event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			spyOn(template.events, 'unlisten').and.callThrough();

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			template.events.remove('test');
			expect(template.events.unlisten).toHaveBeenCalled();
		});

		it("should attach event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			spyOn(evt, 'attach').and.callThrough();
			template.events.attach();
			expect(evt.attach).toHaveBeenCalled();
		});

		it("should detach event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			spyOn(evt, 'detach').and.callThrough();
			template.events.attach();
			template.events.detach();
			expect(evt.detach).toHaveBeenCalled();
		});

	});

	describe("update implements", function() {

		it("should implements update", function() {
			template = new templateClass({
				implements: ['update']
			});
			expect(typeof template.update).toBe('function');
		});

		it("should update HTML", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['update'],
				items: 'toto'
			});

			template.compile();
			template.render('body');
			expect(template.html).toContain('toto');

			template.update({}, ['tata']);
			expect(template.html).toContain('tata');
			expect(template.html).not.toContain('toto');
		});

	});

	describe("templating", function() {

		it("should compile template", function() {

			template = new templateClass({
				xtpl: 'test'
			});

			var html = template.compile();

			expect(typeof html).toBe('string');
			expect(html.length).toBeGreaterThan(25);
			expect(html).toEqual(template.html);
		});

		it("should not compile if no template given", function() {
			template = new templateClass();

			var compile = function() {
				template.compile();
			};

			expect(compile).toThrowError("no template given");
		});

		it("should render given template", function() {
			template = new templateClass({
				xtpl: 'test',
				config: {
					id: 'test-body-rendering'
				},
				renderTo: 'body'
			});
			expect(document.getElementById('test-body-rendering')).toBe(template.el);
		});

		it("should remove element", function() {
			template = new templateClass({
				xtpl: 'test',
				config: {
					id: 'test-remove-element'
				},
				renderTo: 'body'
			});
			expect(document.getElementById('test-remove-element')).toBe(template.el);
			template.remove();
			expect(document.getElementById('test-remove-element')).toBe(null);
			expect(template.el).toBe(undefined);
		});

	});

	describe("configuration", function() {

		it("items should contains default", function() {

			template = new templateClass({
				xtpl: 'test',
				defaults: {
					items: 'default items value'
				},
				items: [{
					xtpl: 'test'
				}]
			});

			var html = template.compile();

			expect(html).toContain('default items value');

		});

		it("should have access to reference", function() {

			template = new templateClass({
				xtpl: 'test',
				items: {
					xtpl: 'test',
					ref: 'referenceTest'
				}
			});

			template.compile();

			expect(template.referenceTest).toBeDefined();

		});

		it("should have reference in scope", function() {

			var scope = {};

			template = new templateClass({
				xtpl: 'test',
				items: {
					xtpl: 'test',
					refScope: scope,
					ref: 'referenceTest'
				}
			});

			template.compile();

			expect(scope.referenceTest).toBeDefined();
			expect(template.referenceTest).not.toBeDefined();

		});

		it("should render to given (string) element", function() {
			template = new templateClass({
				xtpl: 'test',
				renderTo: 'body'
			});
			expect(template.el).toBeDefined();
		});

		it("should render to given (HTML) element", function() {
			template = new templateClass({
				xtpl: 'test',
				renderTo: document.body
			});
			expect(template.el).toBeDefined();
		});

	});

});
describe("Roc.History", function() {

	var history = Roc.History;

	beforeEach(function() {
		window.location.hash = '';
		history.reset(true);
	});

	it("should set default route", function() {
		history.setDefaultRoute('/test/default/route');
		expect(history.defaultRoute).toBe('/test/default/route');
	});

	it("should stop history", function() {
		history.stop();
		expect(history.started).toBe(false);
	});

	it("should start history", function() {
		history.start();
		expect(history.started).toBe(true);
	});

	it("should go to location", function() {
		history.go('/test/route');
		expect(window.location.hash).toContain('/test/route');
	});

	it("should get current location", function() {
		history.go('/test/route');
		expect(history.here()).toContain('/test/route');
	});

	it("should get urlencoded current location", function() {
		history.go('/test/route');
		expect(history.here(true)).toContain('%2Ftest%2Froute');
	});

	it("should get given index url", function(done) {
		history.go('/another/url');
		setTimeout(function() {
			history.go('/again/route');
			setTimeout(function() {
				expect(history.get(-1)).toContain('/another/url');
				expect(history.get(0)).toContain('/again/route');
				done();
			}, 10);
		}, 10);
	});

	it("should back to previous url", function(done) {
		history.go('/back1');
		setTimeout(function() {
			expect(history.here()).toContain('/back1');
			history.go('/back2');
			setTimeout(function() {
				expect(history.here()).toContain('/back2');
				history.back(-1);
				setTimeout(function() {
					expect(history.here()).toContain('/back1');
					done();
				}, 10);
			}, 10);
		}, 10);
	});

	it("should call route callback", function(done) {

		var nb_call = 0;

		history.route('', new RegExp('^/the/route$'), function() {
			++nb_call;
		});
		history.go('/the/route');
		setTimeout(function() {
			expect(nb_call).toBeGreaterThan(0);
			done();
		}, 10);

	});

});

describe("Roc.utils", function() {

	var utils = Roc.utils;

	describe("apply if", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should not apply if key exists but value is defined", function() {

			var result = utils.applyIf(to_override, values, ['a']);

			expect(result.a).toBe(true);
		});

		it("should apply if key exists and value undefined", function() {

			var result = utils.applyIf(to_override, values, ['c']);

			expect(result.c).toEqual('ok');
		});

		it("should keep untouched values", function() {

			var result = utils.applyIf(to_override, values, ['a', 'b']);

			expect(result.not_in_keys).toEqual(true);
		});

		it("should not apply if key not given", function() {

			var result = utils.applyIf(to_override, values, ['a']);

			expect(result.c).not.toBeDefined();
		});

	});

	describe("apply if auto", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should not apply if value is defined", function() {

			var result = utils.applyIfAuto(to_override, values);

			expect(result.a).toBe(true);
		});

		it("should apply if new value", function() {

			var result = utils.applyIfAuto(to_override, values);

			expect(result.b).toEqual(false);
		});

		it("should keep untouched values", function() {

			var result = utils.applyIfAuto(to_override, values);

			expect(result.not_in_keys).toEqual(true);
		});

	});

	describe("apply", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should apply even if value is defined", function() {

			var result = utils.apply(to_override, values, ['a']);

			expect(result.a).toBe(false);
		});

		it("should apply if new value", function() {

			var result = utils.apply(to_override, values, ['c']);

			expect(result.c).toEqual('ok');
		});

		it("should keep untouched values", function() {

			var result = utils.apply(to_override, values, ['a', 'b']);

			expect(result.not_in_keys).toEqual(true);
		});

		it("should not apply if keys not here", function() {

			var result = utils.apply(to_override, values, ['a']);

			expect(result.c).not.toBeDefined();
		});

	});

	describe("applyAuto", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should apply even if value is defined", function() {

			var result = utils.applyAuto(to_override, values);

			expect(result.a).toBe(false);
		});

		it("should apply if new value", function() {

			var result = utils.applyAuto(to_override, values);

			expect(result.c).toEqual('ok');
		});

		it("should keep untouched values", function() {

			var result = utils.applyAuto(to_override, values);

			expect(result.not_in_keys).toEqual(true);
		});

	});

	describe("arraySlice", function() {

		var array;

		beforeEach(function() {
			array = [1, 2, 3, 4, 5];
		});

		it("should copy array", function() {

			var result = utils.arraySlice(array);

			array[0] = 1000;
			expect(result[0]).toEqual(1);
			expect(result[1]).toEqual(2);
			expect(result[2]).toEqual(3);
			expect(result[3]).toEqual(4);
			expect(result[4]).toEqual(5);
		});

		it("should copy array from second element", function() {

			var result = utils.arraySlice(array, 1);

			array[0] = 1000;
			expect(result[0]).toEqual(2);
			expect(result[1]).toEqual(3);
			expect(result[2]).toEqual(4);
			expect(result[3]).toEqual(5);
		});

		it("should copy array from third element to fourth element", function() {

			var result = utils.arraySlice(array, 2, 3);

			array[0] = 1000;
			expect(result[0]).toEqual(3);
			expect(result[1]).toEqual(4);
		});

		it("should copy from 0 with < 0 start", function() {

			var result = utils.arraySlice(array, -10);

			expect(result[0]).toEqual(1);
			expect(result[1]).toEqual(2);
			expect(result[2]).toEqual(3);
			expect(result[3]).toEqual(4);
			expect(result[4]).toEqual(5);
		});

		it("should copy from length with > length start", function() {

			var result = utils.arraySlice(array, 10);

			expect(result.length).toEqual(0);
		});

		it("should copy to 0 with < 0 end", function() {

			var result = utils.arraySlice(array, 0, -10);

			expect(result.length).toEqual(0);
		});

		it("should copy until length with > length end", function() {

			var result = utils.arraySlice(array, 0, 10);

			expect(result[0]).toEqual(1);
			expect(result[1]).toEqual(2);
			expect(result[2]).toEqual(3);
			expect(result[3]).toEqual(4);
			expect(result[4]).toEqual(5);
		});

	});

	it("should capitalize", function() {
		expect(utils.capitalize('test')).toEqual('Test');
		expect(utils.capitalize('0123')).toEqual('0123');
	});

	it("should not capitalize non string", function() {

		var test = false;

		expect(utils.capitalize(test)).toBe(false);
	});

	it("should crc32", function() {
		expect(utils.crc32('test')).toEqual(-662733300);
		expect(utils.crc32('test', 1000)).toEqual(-906843774);
	});

});

describe("Roc.Request", function() {

	var request = Roc.Request;

	describe("ajax", function() {

		it("should fetch given url", function(done) {
			request.ajax('resources/ajax.json', {
				callback: function(success, response) {
					expect(success).toBe(true);
					expect(typeof response).toEqual('string');

					var data = JSON.parse(response);

					expect(data.success).toEqual(true);
					done();
				}
			});
		});

		it("should return success false", function(done) {
			request.ajax('resources/unknown', {
				callback: function(success) {
					expect(success).toBe(false);
					done();
				}
			});
		});

	});

	describe("jsonp", function() {

		it("should fetch given url", function(done) {
			request.jsonp('resources/jsonp.json', {
				callbackName: 'jsonpTest',
				callback: function(success, data) {
					expect(success).toBe(true);
					expect(typeof data).toEqual('object');
					expect(data.success).toEqual(true);
					done();
				}
			});
		});

		it("should return success false", function(done) {
			request.jsonp('resources/unknown', {
				callback: function(success) {
					expect(success).toBe(false);
					done();
				}
			});
		});

	});

});

describe("Roc.Router", function() {

	var j = jasmine,
		routerClass,
		withArgs,
		withoutArgs,
		noMatchDefault,
		withBefore,
		isBefore,
		mustFailBefore,
		withBeforeButFail,
		//default_interval = jasmine.DEFAULT_TIMEOUT_INTERVAL,
		history = Roc.History;

	beforeEach(function() {

		history.reset(true);

		withArgs = j.createSpy('withArgs');
		withoutArgs = j.createSpy('withoutArgs');
		noMatchDefault = j.createSpy('noMatchDefault');
		withBefore = j.createSpy('withBefore');
		isBefore = j.createSpy('isBefore');
		mustFailBefore = j.createSpy('mustFailBefore');
		withBeforeButFail = j.createSpy('withBeforeButFail');

		routerClass = Roc.extend('router', {
			routeNoMatch: 'routeNoMatchDefault',
			before: {
				'^routeWithBefore$': 'mustCalledBefore',
				'^routeWithBeforeButFail$': 'mustFailInThisBefore'
			},
			routes: {
				'/without_args': 'routeWithoutArgs',
				'/with_args/:first/:second': 'routeWithArgs',
				'/before': 'routeWithBefore',
				'/fail_before': 'routeWithBeforeButFail'
			},
			routeWithBeforeButFail: function() {
				withBeforeButFail();
			},
			mustFailInThisBefore: function(ok) {
				mustFailBefore();
				return false;
			},
			mustCalledBefore: function(ok) {
				isBefore();
				ok();
			},
			routeWithBefore: function() {
				withBefore();
			},
			routeNoMatchDefault: function() {
				noMatchDefault();
			},
			routeWithoutArgs: function() {
				withoutArgs();
			},
			routeWithArgs: function(first, second) {
				withArgs(first, second);
			}
		});

	});

	it("should call no match default route", function(done) {

		var router = new routerClass();

		history.go('/unknown', function() {
			expect(noMatchDefault).toHaveBeenCalled();
			done();
		});

	});

	it("should call route without arguments", function(done) {

		var router = new routerClass();

		history.go('/without_args', function() {
			expect(withoutArgs).toHaveBeenCalled();
			done();
		});
	});

	it("should call route with arguments", function(done) {

		var router = new routerClass();

		history.go('/with_args/abcd/1234', function() {
			expect(withArgs).toHaveBeenCalledWith('abcd', '1234');
			done();
		});
	});

	it("should call before with route", function(done) {

		var router = new routerClass();

		history.go('/before', function() {
			expect(withBefore).toHaveBeenCalled();
			expect(isBefore).toHaveBeenCalled();
			done();
		});

	});

	xit("should call before without route", function(done) {

		// test fail i dont know why

		var router = new routerClass();

		history.go('/fail_before', function() {
			expect(mustFailBefore).toHaveBeenCalled();
			expect(withBeforeButFail).not.toHaveBeenCalled();
			done();
		});

	});

});

/**
 * @class Roc.helpers.i18n
 * @extends Roc
 *
 * I18N helper.
 *
 * ### Example
 *
 *      var i18n = new Roc.helpers.i18n();
 *
 *      i18n.addLang('cn', {
 *          'Highlights': '推荐',
 *          'Discover': '分类',
 *          //...
 *      });
 *
 *      i18n.setLang('cn');
 *
 *      // Then you can use global `t` function
 *      var title = t('Highlights');
 *
 *      // And `t` helper in handlebars templates:
 *      {{ t "Highlights" }}
 */
Roc.helpers.i18n = (function(gscope) {

    'use strict';

    var _ns = Roc,
        // stores all languages available (after load)
        _langs = {},
        // stores current language
        _currentLang = {},
        // activated language
        _lang = 'en',
        _defaultScope = gscope,
        _handlebars = Handlebars,
        _parent = _ns;

    /**
     * Translate function.
     * @private
     * @param  {String} str String to translate with current language.
     * @return {String} Translated string.
     */
    function _t(str) {
        return (_currentLang[str] || str);
    }

    if (_handlebars) {
        _handlebars.registerHelper('t', _t);
    }

    _defaultScope.t = _t;

    return _parent.subclass({

        /**
         * Set language to given name.
         * @param {String} name Language name.
         */
        setLang: function(name) {
            _lang = name;
            _currentLang = _langs[_lang];
        },

        /**
         * Add language.
         * @param {String} name       Language name
         * @param {Object} values     Translation dictionnary.
         * @param {Boolean} [setDefault=false] True to set this language as default.
         */
        addLang: function(name, values, setDefault) {
            _langs[name] = values;
            if (setDefault) {
                this.setLang(name);
            }
        }
    });

}(this));

describe("Roc.helpers.i18n", function() {

	var i18nClass = Roc.helpers.i18n,
		i18n;

	beforeEach(function() {
		i18n = undefined;
	});

	it("should translate given words", function() {
		i18n = new i18nClass();
		i18n.addLang('test', {
			'OK': 'Alright',
			'1': true
		}, true);
		expect(t('OK')).toEqual('Alright');
		expect(t('1')).toBe(true);
	});

	it("should change current language", function() {
		i18n = new i18nClass();
		i18n.addLang('test2', {
			'OK': 'Alright'
		});
		i18n.addLang('test3', {
			'OK': 'Good'
		});
		i18n.setLang('test2');
		expect(t('OK')).toEqual('Alright');
		i18n.setLang('test3');
		expect(t('OK')).toEqual('Good');
	});

});

/**
 * Container component
 * @class Roc.views.Container
 * @extends Roc.views.Template
 * @xtype container
 */
Roc.views.Container = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Container',
        xtype: 'container'

    });
})();

Handlebars.templates.container = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, self=this, functionType="function", blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  
  return " hidden";
  }

  buffer += "<div class=\"container";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.hidden) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.hidden); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
 * @class Roc.ionic.ui.Container
 * @xtype container
 * @extends Roc.views.Container
 *
 * ### Example
 *
 *      var container = new Roc.views.Container({
 *          config: {
 *          	style: 'background-color: black;'
 *          },
 *          items: '<p>Hello</p>'
 *      });
 *
 * 		// should better use this way when just for template:
 * 		items: [{
 * 			xtpl: 'container'
 * 		}]
 * 		// or
 * 		var container = new Roc.views.Template({
 * 			xtpl: 'container',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: '...'
 * 		});
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, Roc will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 */
/**
 * @class  Roc.RouterView
 * @extends Roc.Router
 * @xtype routerview
 *
 * Router helper to easily manage view by xtype.
 *
 * ### Example
 *
 *      var history = Roc.History,
 *          session = App.Session,
 *          parent = Roc.RouterView;
 *
 *      var routerview = new (parent.subclass({
 *
 *          xtype: 'user.router',
 *
 *          before: {
 *              'routeProfile': 'isAuthenticated'
 *          },
 *
 *          routes: {
 *              {{ route "/user/login" as="user.login" method="routeLogin" }},
 *              {{ route "/user/profile" as="user.profile" method="routeProfile" }}
 *          },
 *
 *          isAuthenticated: function(done) {
 *
 *              if (session.logged === false) {
 *                  history.go({{ url "user.login" }});
 *                  return false;
 *              }
 *
 *              // user is logged already so we continue
 *              return done();
 *          },
 *
 *          routeLogin: function() {
 *              this.setCurrentView('user.login');
 *          },
 *
 *          routeProfile: function() {
 *              this.setCurrentView('user.profile');
 *          }
 *      }));
 *
 */
Roc.RouterView = (function() {

    var _ns = Roc,
        _history = _ns.History,
        _parent = _ns.Router,
        _events = new _ns.Event(),
        _animating = false,
        _queue = [],
        _vqueue = [];

    _events.on('queuePop', function() {

        if (!_queue.length) {
            return false;
        }

        var item = _queue.pop();

        item.router.setCurrentView(item.xtype, item.config, item.args);
    });

    function has_fx(self, xtype) {
        if (self.fx && self.fx[xtype]) {
            return true;
        }
        return false;
    }

    function get_fx(self, xtype) {
        return (self.fx[xtype]);
    }

    function fx(self, xtype, done) {

        var config = get_fx(self, xtype),
            show = config.show,
            effect = new (_ns.xtype(config.xtype))(config),
            previousView = self.getPreviousView();

        if (previousView && config[previousView.xtype]) {
            show = config[previousView.xtype];
        }

        var opts = {
            speed: show.speed,
            effect: show.me,
            el: self.currentView.el,
            callback: function() {
                self.currentView.fireScope('afterfx', self.currentView);
                done();
            }
        };

        if (previousView) {
            if (show.other) {
                opts.fromEl = previousView.el;
                opts.fromEffect = show.other;
            } else {
                previousView.hide();
            }
        }

        effect.run(opts);

    }

    return _parent.subclass({

        xtype: 'routerview',

        /**
         * Create a new router view.
         *
         * @constructor
         */
        constructor: function() {

            var self = this;

            self.views = {};
            _parent.prototype.constructor.apply(self, arguments);

        },

        /**
         * Remove view from given xtype.
         * It will simply hide the view and keep the instance.
         *
         * @param  {String} xtype View xtype
         */
        removeView: function(xtype) {

            var self = this;

            if (self.views[xtype]) {
                // self.views[xtype].remove();
                // delete self.views[xtype];
                self.views[xtype].hide();
            }

        },

        /**
         * Remove current view.
         * @fires hideCurrentView if global is not false.
         * @param  {Boolean} [global=true] Global flag to let other router view catch this event.
         */
        hideCurrentView: function(global) {

            var self = this;

            if (self.currentView) {
                self.removeView(self.currentView.xtype);
                if (self.rvid) {
                    _events.off(self.rvid);
                    self.rvid = false;
                }
            }
            if (global !== false) {
                _events.fire('hideCurrentView', false);
            }

        },

        /**
         * Add previous view to queue.
         * @param {Object} view View instance.
         */
        addPreviousView: function(view) {
            _vqueue.push(view);
        },

        /**
         * Get previous view.
         * @return {Mixed} View instance if found else false.
         */
        getPreviousView: function() {
            if (_vqueue.length > 0) {
                return _vqueue[_vqueue.length - 1];
            }
            return false;
        },

        /**
         * Hide previous view.
         * If current view is same as previous view, it will do nothing.
         * @param  {Object} currentView Current view.
         */
        hidePreviousView: function(currentView) {

            var self = this;

            currentView = currentView || self.currentView;

            if (_vqueue.length) {
                var view = _vqueue.pop();
                console.log('<<< Remove previous view: ' + view.xtype);
                if (view.config.id !== currentView.config.id) {
                    view.hide();
                }
            }

        },

        /**
         * Add given parameter to queue.
         * It is called by setCurrentView when animating is already running.
         * @param  {String} xtype  View xtype
         * @param  {Object} [config] View configuration
         * @param  {Mixed} [args]   View arguments
         */
        queue: function(xtype, config, args) {
            _queue = [];
            _queue.push({
                router: this,
                xtype: xtype,
                config: config,
                args: args
            });
        },

        /**
         * Set current view.
         * @param {String} xtype  View xtype
         * @param {Object} config Configuration for view constructor.
         * @param {Mixed} args   Arguments passed to view show event listeners.
         */
        setCurrentView: function(xtype, config, args) {

            var self = this;

            if (_animating === true) {
                return self.queue(xtype, config, args);
            }

            self.currentView = self.getView(xtype);

            var previous = self.getPreviousView();

            if (previous !== false &&
                previous.config.id === self.currentView.config.id) {
                console.log('=== Current view is same as previous view: ' + xtype);
                return self.currentView;
            }

            console.log('>>> Set current view to: ' + xtype);

            // fx stuff
            if (has_fx(self, xtype)) {
                _animating = true;
                self.currentView.show(args);
                fx(self, xtype, function() {
                    self.hidePreviousView(self.currentView);
                    self.addPreviousView(self.currentView);
                    _animating = false;
                    _events.fire('queuePop', self);
                });
            } else {
                _animating = false;
                self.currentView.show(args);
                self.hidePreviousView(self.currentView);
                self.addPreviousView(self.currentView);
            }

            return self.currentView;
        },

        /**
         * Get view from given xtype.
         * If the view does not exists, it will create new instance.
         * @param  {String} xtype View xtype
         * @return {Object}       View instance.
         */
        getView: function(xtype) {

            var self = this;

            if (!self.views[xtype]) {

                var view = self.views[xtype] = new (_ns.xtype(xtype))();

                view.compile();
                view.render('body', 'beforeend');
            }
            return self.views[xtype];
        }

    });

}());
/**
 * @class  Roc.RouterViewSandbox
 * @extends Roc.Router
 * @xtype routerviewsandbox
 *
 * Sandbox router helper to easily manage view by xtype.
 *
 * ### Example
 *
 *      var history = Roc.History,
 *          session = App.Session,
 *          parent = Roc.RouterViewSandbox;
 *
 *      var routerview = new (parent.subclass({
 *
 *          xtype: 'user.router',
 *
 *          before: {
 *              'routeProfile': 'isAuthenticated'
 *          },
 *
 *          routes: {
 *              {{ route "/user/login" as="user.login" method="routeLogin" }},
 *              {{ route "/user/profile" as="user.profile" method="routeProfile" }}
 *          },
 *
 *          isAuthenticated: function(done) {
 *
 *              if (session.logged === false) {
 *                  history.go({{ url "user.login" }});
 *                  return false;
 *              }
 *
 *              // user is logged already so we continue
 *              return done();
 *          },
 *
 *          routeLogin: function() {
 *              this.setCurrentView('user.login');
 *          },
 *
 *          routeProfile: function() {
 *              this.setCurrentView('user.profile');
 *          }
 *      }));
 *
 */
Roc.RouterViewSandbox = (function() {

    var _ns = Roc,
        _history = _ns.History,
        _parent = _ns.Router;

    function has_fx(self, xtype) {
        if (self.fx && self.fx[xtype]) {
            return true;
        }
        return false;
    }

    function get_fx(self, xtype) {
        return (self.fx[xtype]);
    }

    function fx(self, xtype, done) {

        var config = get_fx(self, xtype),
            show = config.show,
            effect = new (_ns.xtype(config.xtype))(config),
            previousView = self.getPreviousView();

        if (previousView && config[previousView.xtype]) {
            show = config[previousView.xtype];
        }

        var opts = {
            speed: show.speed,
            effect: show.me,
            el: self.currentView.el,
            callback: done
        };

        if (previousView) {
            if (show.other) {
                opts.fromEl = previousView.el;
                opts.fromEffect = show.other;
            } else {
                previousView.hide();
            }
        }

        effect.run(opts);

    }

    return _parent.subclass({

        xtype: 'routerviewsandbox',
        hasListeners: ['queuePop'],
        listeners: {
            priority: 'VIEWS',
            queuePop: function(self) {

                if (!self.queue.length) {
                    return false;
                }

                var item = self.queue.pop();

                self.setCurrentView(item.xtype);
            }
        },

        /**
         * Create a new router view.
         *
         * @constructor
         */
        constructor: function() {

            var self = this;

            self.views = {};
            self.animating = false;
            self.vqueue = [];
            self.queue = [];

            _parent.prototype.constructor.apply(self, arguments);

        },

        /**
         * Remove view from given xtype.
         * It will simply hide the view and keep the instance.
         *
         * @param  {String} xtype View xtype
         */
        removeView: function(xtype) {

            var self = this;

            if (self.views[xtype]) {
                // self.views[xtype].remove();
                // delete self.views[xtype];
                self.views[xtype].hide();
            }

        },

        /**
         * Remove current view.
         * @fires removeCurrentView if global is not false.
         * @param  {Boolean} [global=true] Global flag to let other router view catch this event.
         */
        removeCurrentView: function(global) {

            var self = this;

            if (self.currentView) {
                self.removeView(self.currentView.xtype);
                if (self.rvid) {
                    self.off(self.rvid);
                    self.rvid = false;
                }
            }
            if (global !== false) {
                self.fire('removeCurrentView', false);
            }

        },

        /**
         * Add previous view to queue.
         * @param {Object} view View instance.
         */
        addPreviousView: function(view) {
            this.vqueue.push(view);
        },

        /**
         * Get previous view.
         * @return {Mixed} View instance if found else false.
         */
        getPreviousView: function() {

            var self = this;

            if (self.vqueue.length > 0) {
                return self.vqueue[self.vqueue.length - 1];
            }
            return false;
        },

        /**
         * Remove previous view. It will hide it.
         * If current view is same as previous view, it will do nothing.
         * @param  {Object} currentView Current view.
         */
        removePreviousView: function(currentView) {

            var self = this;

            if (self.vqueue.length) {

                var view = self.vqueue.pop();
                console.log('<<< Remove previous sandbox view: ' + view.xtype);
                if (view.config.id !== currentView.config.id) {
                    view.hide();
                }
            }

        },

        /**
         * Add given parameter to queue.
         * It is called by setCurrentView when animating is already running.
         * @param  {String} xtype  View xtype
         * @param  {Object} [config] View configuration
         * @param  {Mixed} [args]   View arguments
         */
        queue: function(xtype, config, args) {

            var self = this;

            self.queue = [];
            self.queue.push({
                router: this,
                xtype: xtype,
                config: config,
                args: args
            });
        },

        /**
         * Set current view.
         * @param {String} xtype  View xtype
         * @param {Object} config Configuration for view constructor.
         * @param {Mixed} args   Arguments passed to view show event listeners.
         */
        setCurrentView: function(xtype, config, args) {

            var self = this;

            if (self.animating === true) {
                return self.queue(xtype, config, args);
            }

            self.currentView = self.getView(xtype, config);

            var previous = self.getPreviousView();

            if (previous !== false &&
                previous.config.id === self.currentView.config.id) {
                console.log('=== Current sandbox view is same as previous view: ' + xtype);
                return self.currentView;
            }

            console.log('>>> Set current sandbox view to: ' + xtype);

            // fx stuff
            if (has_fx(self, xtype)) {
                self.animating = true;
                self.currentView.show(args);
                fx(self, xtype, function() {
                    self.removePreviousView(self.currentView);
                    self.addPreviousView(self.currentView);
                    self.animating = false;
                    _events.fire('queuePop', self);
                });
            } else {
                self.animating = false;
                self.currentView.show(args);
                self.removePreviousView(self.currentView);
                self.addPreviousView(self.currentView);
            }

            return self.currentView;
        },

        /**
         * Get view from given xtype.
         * If the view does not exists, it will create new instance.
         * @param  {String} xtype View xtype
         * @return {Object}       View instance.
         */
        getView: function(xtype, config) {

            var self = this;

            if (!self.views[xtype]) {

                var view = self.views[xtype] = new (_ns.xtype(xtype))(config);

                view.compile();
                if (!self.view) {
                    view.render('body', 'beforeend');
                } else {
                    if (self.selector) {
                        view.render(self.view.el.querySelector(self.selector), 'beforeend');
                    } else {
                        view.render(self.view.el, 'beforeend');
                    }
                }
            }
            return self.views[xtype];
        }

    });

}());
describe("Roc.RouterView", function() {

	var j = jasmine,
		routerClass,
		router,
		view1,
		view2,
		history = Roc.History;

	Roc.extend('template', {
		xtype: 'view1',
		config: {
			xtpl: 'container',
			config: {
				style: 'display: none'
			},
			items: 'view 1'
		}
	});

	Roc.extend('template', {
		xtype: 'view2',
		config: {
			xtpl: 'container',
			config: {
				style: 'display: none'
			},
			items: 'view 2'
		}
	});

	beforeEach(function() {

		history.reset(true);

		view1 = j.createSpy('view1');
		view2 = j.createSpy('view2');

		routerClass = Roc.extend('routerview', {
			routes: {
				'/set_current_view/view1': 'routeView1',
				'/set_current_view/view2': 'routeView2'
			},
			routeView1: function() {
				view1();
				this.setCurrentView('view1');
			},
			routeView2: function() {
				view2();
				this.setCurrentView('view2');
			}
		});

		router = new routerClass();

	});

	afterEach(function() {
		history.reset(true);
	});

	it("should set view", function() {
		router.setCurrentView('view1');
		expect(router.currentView.xtype).toEqual('view1');
	});

	it("should hide current view", function() {
		router.setCurrentView('view1');
		expect(router.currentView.xtype).toEqual('view1');
		router.hideCurrentView();
		expect(router.currentView.hidden).toBe(true);
	});

	it("should hide previous view", function() {
		router.setCurrentView('view1');
		expect(router.currentView.xtype).toEqual('view1');

		var previousView = router.getPreviousView();

		router.setCurrentView('view2');
		expect(router.currentView.xtype).toEqual('view2');
		router.hidePreviousView();
		expect(router.currentView.hidden).toBe(false);
		expect(previousView.hidden).toBe(true);
	});

	it("should get view from given xtype", function() {
		expect(router.getView('view1').xtype).toEqual('view1');
		expect(router.getView('view2').xtype).toEqual('view2');
	});

});

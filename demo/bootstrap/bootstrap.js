(function() {

    var app = new Fs.App({
        engine: 'Bootstrap',
        onready: function() {

            var page = new Fs.views.Page({
                config: {
                    style: 'margin-top: 20px; margin-bottom: 20px;'
                },
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'default',
                        //position: 'static',
                        title: 'DEMO'
                    },
                    items: [{
                        xtype: 'template',
                        xtpl: 'menu',
                        config: {
                            align: 'right'
                        },
                        defaults: {
                            xtype: 'template',
                            xtpl: 'menuitem'
                        },
                        items: [{
                            config: {
                                title: 'Link 1',
                                active: true
                            }
                        }, {
                            config: {
                                title: 'Link 2'
                            }
                        }, {
                            xtype: 'button',
                            config: {
                                title: 'Link 3'
                            },
                            items: [{
                                xtype: 'template',
                                xtpl: 'menu',
                                defaults: {
                                    xtype: 'template',
                                    xtpl: 'menuitem'
                                },
                                items: [{
                                    config: {
                                        title: 'Link 3.1'
                                    }
                                }, {
                                    config: {
                                        title: 'Link 3.2'
                                    }
                                }, {
                                    xtpl: 'menuitemdivider'
                                }, {
                                    xtpl: 'menuitemheader',
                                    config: {
                                        title: 'SALUT'
                                    }
                                }, {
                                    config: {
                                        title: 'Link 3.3'
                                    }
                                }]
                            }]
                        }]
                    }]
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());
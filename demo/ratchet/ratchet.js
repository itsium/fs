(function() {

    var app = new Fs.App({
        engine: 'Ratchet',
        onready: function() {
            var page = new Fs.views.Page({
                items: [{
                    xtype: 'header',
                    config: {
                        title: 'DEMO'
                    }
                }, {
                    xtype: 'toolbar',
                    config: {
                        cssClass: 'bar-header-secondary'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            text: 'Button block',
                            ui: 'block'
                        }
                    }]
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    config: {
                        ui: 'padded',
                        style: 'margin-bottom: 50px;'
                    },
                    items: [{
                        xtype: 'textfield',
                        config: {
                            type: 'search',
                            placeHolder: 'Search !'
                        }
                    }, {
                        xtype: 'textfield',
                        config: {
                            placeHolder: 'Username'
                        }
                    }, {
                        xtype: 'textarea'
                    }, {
                        xtype: 'button',
                        config: {
                            ui: ['block','main'],
                            text: 'Submit',
                            bubble: 'bubble'
                        }
                    }, {
                        xtype: 'controlgroup',
                        config: {
                            buttons: [{
                                text: 'One'
                            }, {
                                text: 'Two',
                                active: true
                            }, {
                                text: 'Three'
                            }]
                        }
                    }, {
                        xtype: 'flipswitch',
                        config: {
                            value: true
                        }
                    }, {
                        xtype: 'flipswitch'
                    }, {
                        xtype: 'abstract',
                        xtpl: 'bubble',
                        config: {
                            label: 'default'
                        }
                    }, {
                        xtype: 'abstract',
                        xtpl: 'bubble',
                        config: {
                            ui: 'main',
                            label: 'main'
                        }
                    }, {
                        xtype: 'abstract',
                        xtpl: 'bubble',
                        config: {
                            ui: 'positive',
                            label: 'positive'
                        }
                    }, {
                        xtype: 'abstract',
                        xtpl: 'bubble',
                        config: {
                            ui: 'negative',
                            label: 'negative'
                        }
                    }, {
                        xtype: 'list',
                        config: {
                            inset: true
                        },
                        items: [{
                            label: 'One'
                        }, {
                            label: 'Two'
                        }, {
                            label: 'Three'
                        }, {
                            label: 'Four'
                        }, {
                            label: 'Five'
                        }]
                    }, {
                        xtype: 'list',
                        itemTpl: 'full',
                        config: {
                            style: 'margin: 10px -10px;'
                        },
                        items: [{
                            label: 'One',
                            bubble: 'bubble !'
                        }, {
                            label: 'Two',
                            type: 'divider'
                        }, {
                            label: 'Three',
                            icon: 'arrow'
                        }, {
                            label: 'Four',
                            icon: 'arrow',
                            bubble: '3'
                        }, {
                            label: 'Five',
                            items: [{
                                xtype: 'button',
                                config: {
                                    ui: 'positive',
                                    text: 'Add'
                                }
                            }]
                        }, {
                            label: 'Six',
                            items: [{
                                xtype: 'flipswitch',
                                ref: 'testouz'
                            }]
                        }]
                    }, {
                        xtype: 'abstract',
                        xtpl: 'inputgroup',
                        items: [{
                            xtype: 'textfield',
                            config: {
                                placeHolder: 'Full name'
                            }
                        }, {
                            xtype: 'textfield',
                            config: {
                                type: 'email',
                                placeHolder: 'Email'
                            }
                        }, {
                            xtype: 'textfield',
                            config: {
                                placeHolder: 'Username'
                            }
                        }]
                    }, {
                        xtype: 'abstract',
                        xtpl: 'inputgroup',
                        items: [{
                            xtype: 'textfield',
                            config: {
                                label: 'Full name',
                                placeHolder: 'Mister Ratchet'
                            }
                        }, {
                            xtype: 'textfield',
                            config: {
                                label: 'Email',
                                type: 'email',
                                placeHolder: 'ratchetframework@gmail.com'
                            }
                        }, {
                            xtype: 'textfield',
                            config: {
                                label: 'Username',
                                placeHolder: 'goRatchet'
                            }
                        }]
                    }]
                }/*, {
                    xtype: 'floatingpanel',
                    config: {
                        style: 'display: block; position: relative; opacity: 1;',
                        header: {
                            title: 'Blabla',
                            button: {
                                text: 'Close',
                                route: '/popclose'
                            }
                        }
                    },
                    items: '<ul class="list"><li>Item</li><li>Item</li><li>Item</li><li>Item</li></ul>'
                }*/, {
                    xtype: 'tabs',
                    items: [{
                        xtype: 'tabbutton',
                        config: {
                            active: true,
                            text: 'Button 1'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Button 2'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Button 3'
                        }
                    }]
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());
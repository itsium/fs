(function() {

    var app = new Fs.App({
        engine: 'Topcoat',
        onready: function() {
            var page = new Fs.views.Page({
                items: [{
                    xtype: 'header',
                    config: {
                        title: 'DEMO'
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    items: [{
                        xtype: 'header',
                        config: {
                            title: 'Toolbar'
                        }
                    }, {
                        xtype: 'toolbar',
                        items: {
                            xtype: 'controlgroup',
                            config: {
                                //size: 'large',
                                cssClass: 'full',
                                buttons: [{
                                    cssClass: 'full',
                                    text: 'DEMO 1'
                                }, {
                                    cssClass: 'full',
                                    text: 'DEMO 2'
                                }, {
                                    cssClass: 'full',
                                    text: 'DEMO 3'
                                }]
                            }
                        }
                    }, {
                        xtype: 'header',
                        config: {
                            title: 'Buttons'
                        }
                    }, {
                        xtype: 'button',
                        config: {
                            text: 'Button'
                        }
                    }, {
                        xtype: 'button',
                        config: {
                            disabled: true,
                            text: 'Disabled'
                        }
                    }, {
                        xtype: 'button',
                        config: {
                            ui: 'cta',
                            text: 'Action button'
                        }
                    }, {
                        xtype: 'button',
                        config: {
                            ui: 'quiet',
                            text: 'Quiet button'
                        }
                    }, {
                        xtype: 'header',
                        config: {
                            title: 'Textfields'
                        }
                    }, {
                        xtype: 'textfield',
                        config: {
                            type: 'search',
                            placeHolder: 'Search...'
                        }
                    }, {
                        xtype: 'textfield',
                        config: {
                            type: 'text',
                            value: 'Username',
                            pattern: 'Mrsmith',
                            placeHolder: 'Username...'
                        }
                    }, {
                        xtype: 'textfield',
                        config: {
                            disabled: true,
                            type: 'password',
                            placeHolder: 'Password...'
                        }
                    }, {
                        xtype: 'header',
                        config: {
                            title: 'Form misc'
                        }
                    }, {
                        xtype: 'checkbox',
                        config: {
                            label: 'Simple checkbox'
                        }
                    }, {
                        xtype: 'checkbox',
                        config: {
                            disabled: true,
                            label: 'Disabled checkbox'
                        }
                    }, '<br><br>', {
                        xtype: 'flipswitch',
                        config: {
                            value: true
                        }
                    }, '<br><br>', {
                        xtype: 'flipswitch',
                        config: {
                            disabled: true
                        }
                    }, {
                        xtype: 'header',
                        config: {
                            title: 'Textarea'
                        }
                    }, {
                        xtype: 'textarea',
                        config: {
                            placeHolder: 'Long long text',
                            style: 'width: 100%'
                        }
                    }, {
                        xtype: 'list',
                        items: [{
                            label: 'One'
                        }, {
                            label: 'Two'
                        }, {
                            label: 'Three'
                        }, {
                            label: 'Four'
                        }, {
                            label: 'Five'
                        }]
                    }, {
                        xtype: 'abstract',
                        xtpl: 'bubble',
                        config: {
                            label: '1'
                        }
                    }, {
                        xtype: 'list',
                        itemTpl: 'full',
                        items: [{
                            type: 'divider',
                            label: 'Category'
                        }, {
                            label: 'One'
                        }, {
                            label: 'Contacts',
                            type: 'divider'
                        }, {
                            label: 'Two'
                        }, {
                            label: 'Three'
                        }, {
                            label: 'Four'
                        }]
                    }]
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());
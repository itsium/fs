(function() {

    var _ns = {{framework.barename}},
        $ = _ns.Selector,
        scroll;

    function onafterrender(self) {
        scroll = new _ns.physics.Scroll($.get('#contentview'), {
            scrollable: 'y'
            // zoomable: true,
            // minZoom: 0.5,
            // maxZoom: 1.5
        });
    }

    var items = [{
        xtype: 'button',
        config: {
            text: 'ui positive and block',
            ui: ['positive', 'block']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'ui energized and full',
            ui: ['energized', 'full']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'ui balanced and outline',
            ui: ['balanced', 'outline']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'ui stable and clear',
            ui: ['stable', 'clear']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'Settings',
            icon: 'gear-a',
            iconPos: 'left'
        }
    }, '<br><br>', {
        xtype: 'controlgroup',
        items: [{
            xtype: 'button',
            config: {
                text: '1'
            }
        }, {
            xtype: 'button',
            config: {
                active: true,
                text: '2'
            }
        }, {
            xtype: 'button',
            config: {
                text: '3'
            }
        }]
    }, '<br><br>', {
        xtype: 'list',
        items: [{
            label: 'One'
        }, {
            label: 'Two'
        }, {
            label: 'Three'
        }]
    }, {
        xtype: 'list',
        itemTpl: 'full',
        items: [{
            label: 'Zero',
            type: 'divider'
        }, {
            label: 'One',
            icon: 'gear-a',
            iconPos: 'right'
        }, {
            label: 'Two',
            type: 'divider'
        }, {
            label: 'Three',
            bubble: '1'
        }, {
            label: 'Four',
            icon: 'chatbubble-working'
        }, {
            label: 'Five',
            note: 'This is a note'
        }, {
            label: 'Six',
            arrow: false,
            button: {
                position: 'left',
                ui: 'positive',
                icon: 'ios7-telephone',
                text: 'Salut'
            }
        }]
    }, {
        xtype: 'abstract',
        xtpl: 'container',
        config: {
            id: 'demo-bubbles'
        },
        items: [{
            xtype: 'abstract',
            xtpl: 'bubble',
            config: {
                ui: 'positive',
                label: '0'
            }
        }, {
            xtype: 'abstract',
            xtpl: 'bubble',
            config: {
                ui: 'assertive',
                label: '1'
            }
        }, {
            xtype: 'abstract',
            xtpl: 'bubble',
            config: {
                ui: 'dark',
                label: '2'
            }
        }]
    }, '<br><br>', {
        xtype: 'list',
        itemTpl: 'avatar',
        items: [{
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }]
    }, '<br><br>', {
        xtype: 'list',
        itemTpl: 'thumbnail',
        items: [{
            imagePosition: 'left',
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            imagePosition: 'right',
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }]
    }];

    var app = new _ns.App({
        engine: 'ionic',
        onready: function() {
            var page = new _ns.views.Page({
                listeners: {
                    afterrender: onafterrender
                },
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'dark'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            icon: 'navicon'
                        }
                    }, '<h1 class="title">DEMO</h1>']
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    config: {
                        hasHeader: true,
                        hasFooter: true,
                        padding: true,
                        id: 'contentview'
                    },
                    items: items
                    // items: {
                    //     xtype: 'abstract',
                    //     xtpl: 'container',
                    //     config: {
                    //         id: 'contentview'
                    //     },
                    //     items: items
                    // }
                }, {
                    xtype: 'tabs',
                    config: {
                        ui: 'icon-top'
                    },
                    items: [{
                        xtype: 'tabbutton',
                        config: {
                            active: true,
                            text: 'Home',
                            icon: 'home'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Favorites',
                            icon: 'star'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Settings',
                            icon: 'gear-a'
                        }
                    }]
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());

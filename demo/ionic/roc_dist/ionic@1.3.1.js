/*
 * Scroller
 * http://github.com/zynga/scroller
 *
 * Copyright 2011, Zynga Inc.
 * Licensed under the MIT License.
 * https://raw.github.com/zynga/scroller/master/MIT-LICENSE.txt
 *
 * Based on the work of: Unify Project (unify-project.org)
 * http://unify-project.org
 * Copyright 2011, Deutsche Telekom AG
 * License: MIT + Apache (V2)
 */

/**
 * Generic animation class with support for dropped frames both optional easing and duration.
 *
 * Optional duration is useful when the lifetime is defined by another condition than time
 * e.g. speed of an animating object, etc.
 *
 * Dropped frame logic allows to keep using the same updater logic independent from the actual
 * rendering. This eases a lot of cases where it might be pretty complex to break down a state
 * based on the pure time difference.
 */
(function(global) {
	var time = Date.now || function() {
		return +new Date();
	};
	var desiredFrames = 60;
	var millisecondsPerSecond = 1000;
	var running = {};
	var counter = 1;

	// Create namespaces
	if (!global.core) {
		global.core = { effect : {} };

	} else if (!core.effect) {
		core.effect = {};
	}

	core.effect.Animate = {

		/**
		 * A requestAnimationFrame wrapper / polyfill.
		 *
		 * @param callback {Function} The callback to be invoked before the next repaint.
		 * @param root {HTMLElement} The root element for the repaint
		 */
		requestAnimationFrame: (function() {

			// Check for request animation Frame support
			var requestFrame = global.requestAnimationFrame || global.webkitRequestAnimationFrame || global.mozRequestAnimationFrame || global.oRequestAnimationFrame;
			var isNative = !!requestFrame;

			if (requestFrame && !/requestAnimationFrame\(\)\s*\{\s*\[native code\]\s*\}/i.test(requestFrame.toString())) {
				isNative = false;
			}

			if (isNative) {
				return function(callback, root) {
					requestFrame(callback, root)
				};
			}

			var TARGET_FPS = 60;
			var requests = {};
			var requestCount = 0;
			var rafHandle = 1;
			var intervalHandle = null;
			var lastActive = +new Date();

			return function(callback, root) {
				var callbackHandle = rafHandle++;

				// Store callback
				requests[callbackHandle] = callback;
				requestCount++;

				// Create timeout at first request
				if (intervalHandle === null) {

					intervalHandle = setInterval(function() {

						var time = +new Date();
						var currentRequests = requests;

						// Reset data structure before executing callbacks
						requests = {};
						requestCount = 0;

						for(var key in currentRequests) {
							if (currentRequests.hasOwnProperty(key)) {
								currentRequests[key](time);
								lastActive = time;
							}
						}

						// Disable the timeout when nothing happens for a certain
						// period of time
						if (time - lastActive > 2500) {
							clearInterval(intervalHandle);
							intervalHandle = null;
						}

					}, 1000 / TARGET_FPS);
				}

				return callbackHandle;
			};

		})(),


		/**
		 * Stops the given animation.
		 *
		 * @param id {Integer} Unique animation ID
		 * @return {Boolean} Whether the animation was stopped (aka, was running before)
		 */
		stop: function(id) {
			var cleared = running[id] != null;
			if (cleared) {
				running[id] = null;
			}

			return cleared;
		},


		/**
		 * Whether the given animation is still running.
		 *
		 * @param id {Integer} Unique animation ID
		 * @return {Boolean} Whether the animation is still running
		 */
		isRunning: function(id) {
			return running[id] != null;
		},


		/**
		 * Start the animation.
		 *
		 * @param stepCallback {Function} Pointer to function which is executed on every step.
		 *   Signature of the method should be `function(percent, now, virtual) { return continueWithAnimation; }`
		 * @param verifyCallback {Function} Executed before every animation step.
		 *   Signature of the method should be `function() { return continueWithAnimation; }`
		 * @param completedCallback {Function}
		 *   Signature of the method should be `function(droppedFrames, finishedAnimation) {}`
		 * @param duration {Integer} Milliseconds to run the animation
		 * @param easingMethod {Function} Pointer to easing function
		 *   Signature of the method should be `function(percent) { return modifiedValue; }`
		 * @param root {Element ? document.body} Render root, when available. Used for internal
		 *   usage of requestAnimationFrame.
		 * @return {Integer} Identifier of animation. Can be used to stop it any time.
		 */
		start: function(stepCallback, verifyCallback, completedCallback, duration, easingMethod, root) {

			var start = time();
			var lastFrame = start;
			var percent = 0;
			var dropCounter = 0;
			var id = counter++;

			if (!root) {
				root = document.body;
			}

			// Compacting running db automatically every few new animations
			if (id % 20 === 0) {
				var newRunning = {};
				for (var usedId in running) {
					newRunning[usedId] = true;
				}
				running = newRunning;
			}

			// This is the internal step method which is called every few milliseconds
			var step = function(virtual) {

				// Normalize virtual value
				var render = virtual !== true;

				// Get current time
				var now = time();

				// Verification is executed before next animation step
				if (!running[id] || (verifyCallback && !verifyCallback(id))) {

					running[id] = null;
					completedCallback && completedCallback(desiredFrames - (dropCounter / ((now - start) / millisecondsPerSecond)), id, false);
					return;

				}

				// For the current rendering to apply let's update omitted steps in memory.
				// This is important to bring internal state variables up-to-date with progress in time.
				if (render) {

					var droppedFrames = Math.round((now - lastFrame) / (millisecondsPerSecond / desiredFrames)) - 1;
					for (var j = 0; j < Math.min(droppedFrames, 4); j++) {
						step(true);
						dropCounter++;
					}

				}

				// Compute percent value
				if (duration) {
					percent = (now - start) / duration;
					if (percent > 1) {
						percent = 1;
					}
				}

				// Execute step callback, then...
				var value = easingMethod ? easingMethod(percent) : percent;
				if ((stepCallback(value, now, render) === false || percent === 1) && render) {
					running[id] = null;
					completedCallback && completedCallback(desiredFrames - (dropCounter / ((now - start) / millisecondsPerSecond)), id, percent === 1 || duration == null);
				} else if (render) {
					lastFrame = now;
					core.effect.Animate.requestAnimationFrame(step, root);
				}
			};

			// Mark as running
			running[id] = true;

			// Init first step
			core.effect.Animate.requestAnimationFrame(step, root);

			// Return unique animation ID
			return id;
		}
	};
})(this);


/*
 * Scroller
 * http://github.com/zynga/scroller
 *
 * Copyright 2011, Zynga Inc.
 * Licensed under the MIT License.
 * https://raw.github.com/zynga/scroller/master/MIT-LICENSE.txt
 *
 * Based on the work of: Unify Project (unify-project.org)
 * http://unify-project.org
 * Copyright 2011, Deutsche Telekom AG
 * License: MIT + Apache (V2)
 */

var Scroller;

(function() {
	var NOOP = function(){};

	/**
	 * A pure logic 'component' for 'virtual' scrolling/zooming.
	 */
	Scroller = function(callback, options) {

		this.__callback = callback;

		this.options = {

			/** Enable scrolling on x-axis */
			scrollingX: true,

			/** Enable scrolling on y-axis */
			scrollingY: true,

			/** Enable animations for deceleration, snap back, zooming and scrolling */
			animating: true,

			/** duration for animations triggered by scrollTo/zoomTo */
			animationDuration: 250,

			/** Enable bouncing (content can be slowly moved outside and jumps back after releasing) */
			bouncing: true,

			/** Enable locking to the main axis if user moves only slightly on one of them at start */
			locking: true,

			/** Enable pagination mode (switching between full page content panes) */
			paging: false,

			/** Enable snapping of content to a configured pixel grid */
			snapping: false,

			/** Enable zooming of content via API, fingers and mouse wheel */
			zooming: false,

			/** Minimum zoom level */
			minZoom: 0.5,

			/** Maximum zoom level */
			maxZoom: 3,

			/** Multiply or decrease scrolling speed **/
			speedMultiplier: 1,

			/** Callback that is fired on the later of touch end or deceleration end,
				provided that another scrolling action has not begun. Used to know
				when to fade out a scrollbar. */
			scrollingComplete: NOOP,
			
			/** This configures the amount of change applied to deceleration when reaching boundaries  **/
            penetrationDeceleration : 0.03,

            /** This configures the amount of change applied to acceleration when reaching boundaries  **/
            penetrationAcceleration : 0.08

		};

		for (var key in options) {
			this.options[key] = options[key];
		}

	};


	// Easing Equations (c) 2003 Robert Penner, all rights reserved.
	// Open source under the BSD License.

	/**
	 * @param pos {Number} position between 0 (start of effect) and 1 (end of effect)
	**/
	var easeOutCubic = function(pos) {
		return (Math.pow((pos - 1), 3) + 1);
	};

	/**
	 * @param pos {Number} position between 0 (start of effect) and 1 (end of effect)
	**/
	var easeInOutCubic = function(pos) {
		if ((pos /= 0.5) < 1) {
			return 0.5 * Math.pow(pos, 3);
		}

		return 0.5 * (Math.pow((pos - 2), 3) + 2);
	};


	var members = {

		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: STATUS
		---------------------------------------------------------------------------
		*/

		/** {Boolean} Whether only a single finger is used in touch handling */
		__isSingleTouch: false,

		/** {Boolean} Whether a touch event sequence is in progress */
		__isTracking: false,

		/** {Boolean} Whether a deceleration animation went to completion. */
		__didDecelerationComplete: false,

		/**
		 * {Boolean} Whether a gesture zoom/rotate event is in progress. Activates when
		 * a gesturestart event happens. This has higher priority than dragging.
		 */
		__isGesturing: false,

		/**
		 * {Boolean} Whether the user has moved by such a distance that we have enabled
		 * dragging mode. Hint: It's only enabled after some pixels of movement to
		 * not interrupt with clicks etc.
		 */
		__isDragging: false,

		/**
		 * {Boolean} Not touching and dragging anymore, and smoothly animating the
		 * touch sequence using deceleration.
		 */
		__isDecelerating: false,

		/**
		 * {Boolean} Smoothly animating the currently configured change
		 */
		__isAnimating: false,



		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: DIMENSIONS
		---------------------------------------------------------------------------
		*/

		/** {Integer} Available outer left position (from document perspective) */
		__clientLeft: 0,

		/** {Integer} Available outer top position (from document perspective) */
		__clientTop: 0,

		/** {Integer} Available outer width */
		__clientWidth: 0,

		/** {Integer} Available outer height */
		__clientHeight: 0,

		/** {Integer} Outer width of content */
		__contentWidth: 0,

		/** {Integer} Outer height of content */
		__contentHeight: 0,

		/** {Integer} Snapping width for content */
		__snapWidth: 100,

		/** {Integer} Snapping height for content */
		__snapHeight: 100,

		/** {Integer} Height to assign to refresh area */
		__refreshHeight: null,

		/** {Boolean} Whether the refresh process is enabled when the event is released now */
		__refreshActive: false,

		/** {Function} Callback to execute on activation. This is for signalling the user about a refresh is about to happen when he release */
		__refreshActivate: null,

		/** {Function} Callback to execute on deactivation. This is for signalling the user about the refresh being cancelled */
		__refreshDeactivate: null,

		/** {Function} Callback to execute to start the actual refresh. Call {@link #refreshFinish} when done */
		__refreshStart: null,

		/** {Number} Zoom level */
		__zoomLevel: 1,

		/** {Number} Scroll position on x-axis */
		__scrollLeft: 0,

		/** {Number} Scroll position on y-axis */
		__scrollTop: 0,

		/** {Integer} Maximum allowed scroll position on x-axis */
		__maxScrollLeft: 0,

		/** {Integer} Maximum allowed scroll position on y-axis */
		__maxScrollTop: 0,

		/* {Number} Scheduled left position (final position when animating) */
		__scheduledLeft: 0,

		/* {Number} Scheduled top position (final position when animating) */
		__scheduledTop: 0,

		/* {Number} Scheduled zoom level (final scale when animating) */
		__scheduledZoom: 0,



		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: LAST POSITIONS
		---------------------------------------------------------------------------
		*/

		/** {Number} Left position of finger at start */
		__lastTouchLeft: null,

		/** {Number} Top position of finger at start */
		__lastTouchTop: null,

		/** {Date} Timestamp of last move of finger. Used to limit tracking range for deceleration speed. */
		__lastTouchMove: null,

		/** {Array} List of positions, uses three indexes for each state: left, top, timestamp */
		__positions: null,



		/*
		---------------------------------------------------------------------------
			INTERNAL FIELDS :: DECELERATION SUPPORT
		---------------------------------------------------------------------------
		*/

		/** {Integer} Minimum left scroll position during deceleration */
		__minDecelerationScrollLeft: null,

		/** {Integer} Minimum top scroll position during deceleration */
		__minDecelerationScrollTop: null,

		/** {Integer} Maximum left scroll position during deceleration */
		__maxDecelerationScrollLeft: null,

		/** {Integer} Maximum top scroll position during deceleration */
		__maxDecelerationScrollTop: null,

		/** {Number} Current factor to modify horizontal scroll position with on every step */
		__decelerationVelocityX: null,

		/** {Number} Current factor to modify vertical scroll position with on every step */
		__decelerationVelocityY: null,



		/*
		---------------------------------------------------------------------------
			PUBLIC API
		---------------------------------------------------------------------------
		*/

		/**
		 * Configures the dimensions of the client (outer) and content (inner) elements.
		 * Requires the available space for the outer element and the outer size of the inner element.
		 * All values which are falsy (null or zero etc.) are ignored and the old value is kept.
		 *
		 * @param clientWidth {Integer ? null} Inner width of outer element
		 * @param clientHeight {Integer ? null} Inner height of outer element
		 * @param contentWidth {Integer ? null} Outer width of inner element
		 * @param contentHeight {Integer ? null} Outer height of inner element
		 */
		setDimensions: function(clientWidth, clientHeight, contentWidth, contentHeight) {

			var self = this;

			// Only update values which are defined
			if (clientWidth === +clientWidth) {
				self.__clientWidth = clientWidth;
			}

			if (clientHeight === +clientHeight) {
				self.__clientHeight = clientHeight;
			}

			if (contentWidth === +contentWidth) {
				self.__contentWidth = contentWidth;
			}

			if (contentHeight === +contentHeight) {
				self.__contentHeight = contentHeight;
			}

			// Refresh maximums
			self.__computeScrollMax();

			// Refresh scroll position
			self.scrollTo(self.__scrollLeft, self.__scrollTop, true);

		},


		/**
		 * Sets the client coordinates in relation to the document.
		 *
		 * @param left {Integer ? 0} Left position of outer element
		 * @param top {Integer ? 0} Top position of outer element
		 */
		setPosition: function(left, top) {

			var self = this;

			self.__clientLeft = left || 0;
			self.__clientTop = top || 0;

		},


		/**
		 * Configures the snapping (when snapping is active)
		 *
		 * @param width {Integer} Snapping width
		 * @param height {Integer} Snapping height
		 */
		setSnapSize: function(width, height) {

			var self = this;

			self.__snapWidth = width;
			self.__snapHeight = height;

		},


		/**
		 * Activates pull-to-refresh. A special zone on the top of the list to start a list refresh whenever
		 * the user event is released during visibility of this zone. This was introduced by some apps on iOS like
		 * the official Twitter client.
		 *
		 * @param height {Integer} Height of pull-to-refresh zone on top of rendered list
		 * @param activateCallback {Function} Callback to execute on activation. This is for signalling the user about a refresh is about to happen when he release.
		 * @param deactivateCallback {Function} Callback to execute on deactivation. This is for signalling the user about the refresh being cancelled.
		 * @param startCallback {Function} Callback to execute to start the real async refresh action. Call {@link #finishPullToRefresh} after finish of refresh.
		 */
		activatePullToRefresh: function(height, activateCallback, deactivateCallback, startCallback) {

			var self = this;

			self.__refreshHeight = height;
			self.__refreshActivate = activateCallback;
			self.__refreshDeactivate = deactivateCallback;
			self.__refreshStart = startCallback;

		},


		/**
		 * Starts pull-to-refresh manually.
		 */
		triggerPullToRefresh: function() {
			// Use publish instead of scrollTo to allow scrolling to out of boundary position
			// We don't need to normalize scrollLeft, zoomLevel, etc. here because we only y-scrolling when pull-to-refresh is enabled
			this.__publish(this.__scrollLeft, -this.__refreshHeight, this.__zoomLevel, true);

			if (this.__refreshStart) {
				this.__refreshStart();
			}
		},


		/**
		 * Signalizes that pull-to-refresh is finished.
		 */
		finishPullToRefresh: function() {

			var self = this;

			self.__refreshActive = false;
			if (self.__refreshDeactivate) {
				self.__refreshDeactivate();
			}

			self.scrollTo(self.__scrollLeft, self.__scrollTop, true);

		},


		/**
		 * Returns the scroll position and zooming values
		 *
		 * @return {Map} `left` and `top` scroll position and `zoom` level
		 */
		getValues: function() {

			var self = this;

			return {
				left: self.__scrollLeft,
				top: self.__scrollTop,
				zoom: self.__zoomLevel
			};

		},


		/**
		 * Returns the maximum scroll values
		 *
		 * @return {Map} `left` and `top` maximum scroll values
		 */
		getScrollMax: function() {

			var self = this;

			return {
				left: self.__maxScrollLeft,
				top: self.__maxScrollTop
			};

		},


		/**
		 * Zooms to the given level. Supports optional animation. Zooms
		 * the center when no coordinates are given.
		 *
		 * @param level {Number} Level to zoom to
		 * @param animate {Boolean ? false} Whether to use animation
		 * @param originLeft {Number ? null} Zoom in at given left coordinate
		 * @param originTop {Number ? null} Zoom in at given top coordinate
		 * @param callback {Function ? null} A callback that gets fired when the zoom is complete.
		 */
		zoomTo: function(level, animate, originLeft, originTop, callback) {

			var self = this;

			if (!self.options.zooming) {
				throw new Error("Zooming is not enabled!");
			}

			// Add callback if exists
			if(callback) {
				self.__zoomComplete = callback;
			}

			// Stop deceleration
			if (self.__isDecelerating) {
				core.effect.Animate.stop(self.__isDecelerating);
				self.__isDecelerating = false;
			}

			var oldLevel = self.__zoomLevel;

			// Normalize input origin to center of viewport if not defined
			if (originLeft == null) {
				originLeft = self.__clientWidth / 2;
			}

			if (originTop == null) {
				originTop = self.__clientHeight / 2;
			}

			// Limit level according to configuration
			level = Math.max(Math.min(level, self.options.maxZoom), self.options.minZoom);

			// Recompute maximum values while temporary tweaking maximum scroll ranges
			self.__computeScrollMax(level);

			// Recompute left and top coordinates based on new zoom level
			var left = ((originLeft + self.__scrollLeft) * level / oldLevel) - originLeft;
			var top = ((originTop + self.__scrollTop) * level / oldLevel) - originTop;

			// Limit x-axis
			if (left > self.__maxScrollLeft) {
				left = self.__maxScrollLeft;
			} else if (left < 0) {
				left = 0;
			}

			// Limit y-axis
			if (top > self.__maxScrollTop) {
				top = self.__maxScrollTop;
			} else if (top < 0) {
				top = 0;
			}

			// Push values out
			self.__publish(left, top, level, animate);

		},


		/**
		 * Zooms the content by the given factor.
		 *
		 * @param factor {Number} Zoom by given factor
		 * @param animate {Boolean ? false} Whether to use animation
		 * @param originLeft {Number ? 0} Zoom in at given left coordinate
		 * @param originTop {Number ? 0} Zoom in at given top coordinate
		 * @param callback {Function ? null} A callback that gets fired when the zoom is complete.
		 */
		zoomBy: function(factor, animate, originLeft, originTop, callback) {

			var self = this;

			self.zoomTo(self.__zoomLevel * factor, animate, originLeft, originTop, callback);

		},


		/**
		 * Scrolls to the given position. Respect limitations and snapping automatically.
		 *
		 * @param left {Number?null} Horizontal scroll position, keeps current if value is <code>null</code>
		 * @param top {Number?null} Vertical scroll position, keeps current if value is <code>null</code>
		 * @param animate {Boolean?false} Whether the scrolling should happen using an animation
		 * @param zoom {Number?null} Zoom level to go to
		 */
		scrollTo: function(left, top, animate, zoom) {

			var self = this;

			// Stop deceleration
			if (self.__isDecelerating) {
				core.effect.Animate.stop(self.__isDecelerating);
				self.__isDecelerating = false;
			}

			// Correct coordinates based on new zoom level
			if (zoom != null && zoom !== self.__zoomLevel) {

				if (!self.options.zooming) {
					throw new Error("Zooming is not enabled!");
				}

				left *= zoom;
				top *= zoom;

				// Recompute maximum values while temporary tweaking maximum scroll ranges
				self.__computeScrollMax(zoom);

			} else {

				// Keep zoom when not defined
				zoom = self.__zoomLevel;

			}

			if (!self.options.scrollingX) {

				left = self.__scrollLeft;

			} else {

				if (self.options.paging) {
					left = Math.round(left / self.__clientWidth) * self.__clientWidth;
				} else if (self.options.snapping) {
					left = Math.round(left / self.__snapWidth) * self.__snapWidth;
				}

			}

			if (!self.options.scrollingY) {

				top = self.__scrollTop;

			} else {

				if (self.options.paging) {
					top = Math.round(top / self.__clientHeight) * self.__clientHeight;
				} else if (self.options.snapping) {
					top = Math.round(top / self.__snapHeight) * self.__snapHeight;
				}

			}

			// Limit for allowed ranges
			left = Math.max(Math.min(self.__maxScrollLeft, left), 0);
			top = Math.max(Math.min(self.__maxScrollTop, top), 0);

			// Don't animate when no change detected, still call publish to make sure
			// that rendered position is really in-sync with internal data
			if (left === self.__scrollLeft && top === self.__scrollTop) {
				animate = false;
			}

			// Publish new values
			self.__publish(left, top, zoom, animate);

		},


		/**
		 * Scroll by the given offset
		 *
		 * @param left {Number ? 0} Scroll x-axis by given offset
		 * @param top {Number ? 0} Scroll x-axis by given offset
		 * @param animate {Boolean ? false} Whether to animate the given change
		 */
		scrollBy: function(left, top, animate) {

			var self = this;

			var startLeft = self.__isAnimating ? self.__scheduledLeft : self.__scrollLeft;
			var startTop = self.__isAnimating ? self.__scheduledTop : self.__scrollTop;

			self.scrollTo(startLeft + (left || 0), startTop + (top || 0), animate);

		},



		/*
		---------------------------------------------------------------------------
			EVENT CALLBACKS
		---------------------------------------------------------------------------
		*/

		/**
		 * Mouse wheel handler for zooming support
		 */
		doMouseZoom: function(wheelDelta, timeStamp, pageX, pageY) {

			var self = this;
			var change = wheelDelta > 0 ? 0.97 : 1.03;

			return self.zoomTo(self.__zoomLevel * change, false, pageX - self.__clientLeft, pageY - self.__clientTop);

		},


		/**
		 * Touch start handler for scrolling support
		 */
		doTouchStart: function(touches, timeStamp) {

			// Array-like check is enough here
			if (touches.length == null) {
				throw new Error("Invalid touch list: " + touches);
			}

			if (timeStamp instanceof Date) {
				timeStamp = timeStamp.valueOf();
			}
			if (typeof timeStamp !== "number") {
				throw new Error("Invalid timestamp value: " + timeStamp);
			}

			var self = this;

			// Reset interruptedAnimation flag
			self.__interruptedAnimation = true;

			// Stop deceleration
			if (self.__isDecelerating) {
				core.effect.Animate.stop(self.__isDecelerating);
				self.__isDecelerating = false;
				self.__interruptedAnimation = true;
			}

			// Stop animation
			if (self.__isAnimating) {
				core.effect.Animate.stop(self.__isAnimating);
				self.__isAnimating = false;
				self.__interruptedAnimation = true;
			}

			// Use center point when dealing with two fingers
			var currentTouchLeft, currentTouchTop;
			var isSingleTouch = touches.length === 1;
			if (isSingleTouch) {
				currentTouchLeft = touches[0].pageX;
				currentTouchTop = touches[0].pageY;
			} else {
				currentTouchLeft = Math.abs(touches[0].pageX + touches[1].pageX) / 2;
				currentTouchTop = Math.abs(touches[0].pageY + touches[1].pageY) / 2;
			}

			// Store initial positions
			self.__initialTouchLeft = currentTouchLeft;
			self.__initialTouchTop = currentTouchTop;

			// Store current zoom level
			self.__zoomLevelStart = self.__zoomLevel;

			// Store initial touch positions
			self.__lastTouchLeft = currentTouchLeft;
			self.__lastTouchTop = currentTouchTop;

			// Store initial move time stamp
			self.__lastTouchMove = timeStamp;

			// Reset initial scale
			self.__lastScale = 1;

			// Reset locking flags
			self.__enableScrollX = !isSingleTouch && self.options.scrollingX;
			self.__enableScrollY = !isSingleTouch && self.options.scrollingY;

			// Reset tracking flag
			self.__isTracking = true;

			// Reset deceleration complete flag
			self.__didDecelerationComplete = false;

			// Dragging starts directly with two fingers, otherwise lazy with an offset
			self.__isDragging = !isSingleTouch;

			// Some features are disabled in multi touch scenarios
			self.__isSingleTouch = isSingleTouch;

			// Clearing data structure
			self.__positions = [];

		},


		/**
		 * Touch move handler for scrolling support
		 */
		doTouchMove: function(touches, timeStamp, scale) {

			// Array-like check is enough here
			if (touches.length == null) {
				throw new Error("Invalid touch list: " + touches);
			}

			if (timeStamp instanceof Date) {
				timeStamp = timeStamp.valueOf();
			}
			if (typeof timeStamp !== "number") {
				throw new Error("Invalid timestamp value: " + timeStamp);
			}

			var self = this;

			// Ignore event when tracking is not enabled (event might be outside of element)
			if (!self.__isTracking) {
				return;
			}


			var currentTouchLeft, currentTouchTop;

			// Compute move based around of center of fingers
			if (touches.length === 2) {
				currentTouchLeft = Math.abs(touches[0].pageX + touches[1].pageX) / 2;
				currentTouchTop = Math.abs(touches[0].pageY + touches[1].pageY) / 2;
			} else {
				currentTouchLeft = touches[0].pageX;
				currentTouchTop = touches[0].pageY;
			}

			var positions = self.__positions;

			// Are we already is dragging mode?
			if (self.__isDragging) {

				// Compute move distance
				var moveX = currentTouchLeft - self.__lastTouchLeft;
				var moveY = currentTouchTop - self.__lastTouchTop;

				// Read previous scroll position and zooming
				var scrollLeft = self.__scrollLeft;
				var scrollTop = self.__scrollTop;
				var level = self.__zoomLevel;

				// Work with scaling
				if (scale != null && self.options.zooming) {

					var oldLevel = level;

					// Recompute level based on previous scale and new scale
					level = level / self.__lastScale * scale;

					// Limit level according to configuration
					level = Math.max(Math.min(level, self.options.maxZoom), self.options.minZoom);

					// Only do further compution when change happened
					if (oldLevel !== level) {

						// Compute relative event position to container
						var currentTouchLeftRel = currentTouchLeft - self.__clientLeft;
						var currentTouchTopRel = currentTouchTop - self.__clientTop;

						// Recompute left and top coordinates based on new zoom level
						scrollLeft = ((currentTouchLeftRel + scrollLeft) * level / oldLevel) - currentTouchLeftRel;
						scrollTop = ((currentTouchTopRel + scrollTop) * level / oldLevel) - currentTouchTopRel;

						// Recompute max scroll values
						self.__computeScrollMax(level);

					}
				}

				if (self.__enableScrollX) {

					scrollLeft -= moveX * this.options.speedMultiplier;
					var maxScrollLeft = self.__maxScrollLeft;

					if (scrollLeft > maxScrollLeft || scrollLeft < 0) {

						// Slow down on the edges
						if (self.options.bouncing) {

							scrollLeft += (moveX / 2  * this.options.speedMultiplier);

						} else if (scrollLeft > maxScrollLeft) {

							scrollLeft = maxScrollLeft;

						} else {

							scrollLeft = 0;

						}
					}
				}

				// Compute new vertical scroll position
				if (self.__enableScrollY) {

					scrollTop -= moveY * this.options.speedMultiplier;
					var maxScrollTop = self.__maxScrollTop;

					if (scrollTop > maxScrollTop || scrollTop < 0) {

						// Slow down on the edges
						if (self.options.bouncing) {

							scrollTop += (moveY / 2 * this.options.speedMultiplier);

							// Support pull-to-refresh (only when only y is scrollable)
							if (!self.__enableScrollX && self.__refreshHeight != null) {

								if (!self.__refreshActive && scrollTop <= -self.__refreshHeight) {

									self.__refreshActive = true;
									if (self.__refreshActivate) {
										self.__refreshActivate();
									}

								} else if (self.__refreshActive && scrollTop > -self.__refreshHeight) {

									self.__refreshActive = false;
									if (self.__refreshDeactivate) {
										self.__refreshDeactivate();
									}

								}
							}

						} else if (scrollTop > maxScrollTop) {

							scrollTop = maxScrollTop;

						} else {

							scrollTop = 0;

						}
					}
				}

				// Keep list from growing infinitely (holding min 10, max 20 measure points)
				if (positions.length > 60) {
					positions.splice(0, 30);
				}

				// Track scroll movement for decleration
				positions.push(scrollLeft, scrollTop, timeStamp);

				// Sync scroll position
				self.__publish(scrollLeft, scrollTop, level);

			// Otherwise figure out whether we are switching into dragging mode now.
			} else {

				var minimumTrackingForScroll = self.options.locking ? 3 : 0;
				var minimumTrackingForDrag = 5;

				var distanceX = Math.abs(currentTouchLeft - self.__initialTouchLeft);
				var distanceY = Math.abs(currentTouchTop - self.__initialTouchTop);

				self.__enableScrollX = self.options.scrollingX && distanceX >= minimumTrackingForScroll;
				self.__enableScrollY = self.options.scrollingY && distanceY >= minimumTrackingForScroll;

				positions.push(self.__scrollLeft, self.__scrollTop, timeStamp);

				self.__isDragging = (self.__enableScrollX || self.__enableScrollY) && (distanceX >= minimumTrackingForDrag || distanceY >= minimumTrackingForDrag);
				if (self.__isDragging) {
					self.__interruptedAnimation = false;
				}

			}

			// Update last touch positions and time stamp for next event
			self.__lastTouchLeft = currentTouchLeft;
			self.__lastTouchTop = currentTouchTop;
			self.__lastTouchMove = timeStamp;
			self.__lastScale = scale;

		},


		/**
		 * Touch end handler for scrolling support
		 */
		doTouchEnd: function(timeStamp) {

			if (timeStamp instanceof Date) {
				timeStamp = timeStamp.valueOf();
			}
			if (typeof timeStamp !== "number") {
				throw new Error("Invalid timestamp value: " + timeStamp);
			}

			var self = this;

			// Ignore event when tracking is not enabled (no touchstart event on element)
			// This is required as this listener ('touchmove') sits on the document and not on the element itself.
			if (!self.__isTracking) {
				return;
			}

			// Not touching anymore (when two finger hit the screen there are two touch end events)
			self.__isTracking = false;

			// Be sure to reset the dragging flag now. Here we also detect whether
			// the finger has moved fast enough to switch into a deceleration animation.
			if (self.__isDragging) {

				// Reset dragging flag
				self.__isDragging = false;

				// Start deceleration
				// Verify that the last move detected was in some relevant time frame
				if (self.__isSingleTouch && self.options.animating && (timeStamp - self.__lastTouchMove) <= 100) {

					// Then figure out what the scroll position was about 100ms ago
					var positions = self.__positions;
					var endPos = positions.length - 1;
					var startPos = endPos;

					// Move pointer to position measured 100ms ago
					for (var i = endPos; i > 0 && positions[i] > (self.__lastTouchMove - 100); i -= 3) {
						startPos = i;
					}

					// If start and stop position is identical in a 100ms timeframe,
					// we cannot compute any useful deceleration.
					if (startPos !== endPos) {

						// Compute relative movement between these two points
						var timeOffset = positions[endPos] - positions[startPos];
						var movedLeft = self.__scrollLeft - positions[startPos - 2];
						var movedTop = self.__scrollTop - positions[startPos - 1];

						// Based on 50ms compute the movement to apply for each render step
						self.__decelerationVelocityX = movedLeft / timeOffset * (1000 / 60);
						self.__decelerationVelocityY = movedTop / timeOffset * (1000 / 60);

						// How much velocity is required to start the deceleration
						var minVelocityToStartDeceleration = self.options.paging || self.options.snapping ? 4 : 1;

						// Verify that we have enough velocity to start deceleration
						if (Math.abs(self.__decelerationVelocityX) > minVelocityToStartDeceleration || Math.abs(self.__decelerationVelocityY) > minVelocityToStartDeceleration) {

							// Deactivate pull-to-refresh when decelerating
							if (!self.__refreshActive) {
								self.__startDeceleration(timeStamp);
							}
						}
					} else {
						self.options.scrollingComplete();
					}
				} else if ((timeStamp - self.__lastTouchMove) > 100) {
					self.options.scrollingComplete();
	 			}
			}

			// If this was a slower move it is per default non decelerated, but this
			// still means that we want snap back to the bounds which is done here.
			// This is placed outside the condition above to improve edge case stability
			// e.g. touchend fired without enabled dragging. This should normally do not
			// have modified the scroll positions or even showed the scrollbars though.
			if (!self.__isDecelerating) {

				if (self.__refreshActive && self.__refreshStart) {

					// Use publish instead of scrollTo to allow scrolling to out of boundary position
					// We don't need to normalize scrollLeft, zoomLevel, etc. here because we only y-scrolling when pull-to-refresh is enabled
					self.__publish(self.__scrollLeft, -self.__refreshHeight, self.__zoomLevel, true);

					if (self.__refreshStart) {
						self.__refreshStart();
					}

				} else {

					if (self.__interruptedAnimation || self.__isDragging) {
						self.options.scrollingComplete();
					}
					self.scrollTo(self.__scrollLeft, self.__scrollTop, true, self.__zoomLevel);

					// Directly signalize deactivation (nothing todo on refresh?)
					if (self.__refreshActive) {

						self.__refreshActive = false;
						if (self.__refreshDeactivate) {
							self.__refreshDeactivate();
						}

					}
				}
			}

			// Fully cleanup list
			self.__positions.length = 0;

		},



		/*
		---------------------------------------------------------------------------
			PRIVATE API
		---------------------------------------------------------------------------
		*/

		/**
		 * Applies the scroll position to the content element
		 *
		 * @param left {Number} Left scroll position
		 * @param top {Number} Top scroll position
		 * @param animate {Boolean?false} Whether animation should be used to move to the new coordinates
		 */
		__publish: function(left, top, zoom, animate) {

			var self = this;

			// Remember whether we had an animation, then we try to continue based on the current "drive" of the animation
			var wasAnimating = self.__isAnimating;
			if (wasAnimating) {
				core.effect.Animate.stop(wasAnimating);
				self.__isAnimating = false;
			}

			if (animate && self.options.animating) {

				// Keep scheduled positions for scrollBy/zoomBy functionality
				self.__scheduledLeft = left;
				self.__scheduledTop = top;
				self.__scheduledZoom = zoom;

				var oldLeft = self.__scrollLeft;
				var oldTop = self.__scrollTop;
				var oldZoom = self.__zoomLevel;

				var diffLeft = left - oldLeft;
				var diffTop = top - oldTop;
				var diffZoom = zoom - oldZoom;

				var step = function(percent, now, render) {

					if (render) {

						self.__scrollLeft = oldLeft + (diffLeft * percent);
						self.__scrollTop = oldTop + (diffTop * percent);
						self.__zoomLevel = oldZoom + (diffZoom * percent);

						// Push values out
						if (self.__callback) {
							self.__callback(self.__scrollLeft, self.__scrollTop, self.__zoomLevel);
						}

					}
				};

				var verify = function(id) {
					return self.__isAnimating === id;
				};

				var completed = function(renderedFramesPerSecond, animationId, wasFinished) {
					if (animationId === self.__isAnimating) {
						self.__isAnimating = false;
					}
					if (self.__didDecelerationComplete || wasFinished) {
						self.options.scrollingComplete();
					}

					if (self.options.zooming) {
						self.__computeScrollMax();
						if(self.__zoomComplete) {
							self.__zoomComplete();
							self.__zoomComplete = null;
						}
					}
				};

				// When continuing based on previous animation we choose an ease-out animation instead of ease-in-out
				self.__isAnimating = core.effect.Animate.start(step, verify, completed, self.options.animationDuration, wasAnimating ? easeOutCubic : easeInOutCubic);

			} else {

				self.__scheduledLeft = self.__scrollLeft = left;
				self.__scheduledTop = self.__scrollTop = top;
				self.__scheduledZoom = self.__zoomLevel = zoom;

				// Push values out
				if (self.__callback) {
					self.__callback(left, top, zoom);
				}

				// Fix max scroll ranges
				if (self.options.zooming) {
					self.__computeScrollMax();
					if(self.__zoomComplete) {
						self.__zoomComplete();
						self.__zoomComplete = null;
					}
				}
			}
		},


		/**
		 * Recomputes scroll minimum values based on client dimensions and content dimensions.
		 */
		__computeScrollMax: function(zoomLevel) {

			var self = this;

			if (zoomLevel == null) {
				zoomLevel = self.__zoomLevel;
			}

			self.__maxScrollLeft = Math.max((self.__contentWidth * zoomLevel) - self.__clientWidth, 0);
			self.__maxScrollTop = Math.max((self.__contentHeight * zoomLevel) - self.__clientHeight, 0);

		},



		/*
		---------------------------------------------------------------------------
			ANIMATION (DECELERATION) SUPPORT
		---------------------------------------------------------------------------
		*/

		/**
		 * Called when a touch sequence end and the speed of the finger was high enough
		 * to switch into deceleration mode.
		 */
		__startDeceleration: function(timeStamp) {

			var self = this;

			if (self.options.paging) {

				var scrollLeft = Math.max(Math.min(self.__scrollLeft, self.__maxScrollLeft), 0);
				var scrollTop = Math.max(Math.min(self.__scrollTop, self.__maxScrollTop), 0);
				var clientWidth = self.__clientWidth;
				var clientHeight = self.__clientHeight;

				// We limit deceleration not to the min/max values of the allowed range, but to the size of the visible client area.
				// Each page should have exactly the size of the client area.
				self.__minDecelerationScrollLeft = Math.floor(scrollLeft / clientWidth) * clientWidth;
				self.__minDecelerationScrollTop = Math.floor(scrollTop / clientHeight) * clientHeight;
				self.__maxDecelerationScrollLeft = Math.ceil(scrollLeft / clientWidth) * clientWidth;
				self.__maxDecelerationScrollTop = Math.ceil(scrollTop / clientHeight) * clientHeight;

			} else {

				self.__minDecelerationScrollLeft = 0;
				self.__minDecelerationScrollTop = 0;
				self.__maxDecelerationScrollLeft = self.__maxScrollLeft;
				self.__maxDecelerationScrollTop = self.__maxScrollTop;

			}

			// Wrap class method
			var step = function(percent, now, render) {
				self.__stepThroughDeceleration(render);
			};

			// How much velocity is required to keep the deceleration running
			var minVelocityToKeepDecelerating = self.options.snapping ? 4 : 0.1;

			// Detect whether it's still worth to continue animating steps
			// If we are already slow enough to not being user perceivable anymore, we stop the whole process here.
			var verify = function() {
				var shouldContinue = Math.abs(self.__decelerationVelocityX) >= minVelocityToKeepDecelerating || Math.abs(self.__decelerationVelocityY) >= minVelocityToKeepDecelerating;
				if (!shouldContinue) {
					self.__didDecelerationComplete = true;
				}
				return shouldContinue;
			};

			var completed = function(renderedFramesPerSecond, animationId, wasFinished) {
				self.__isDecelerating = false;
				if (self.__didDecelerationComplete) {
					self.options.scrollingComplete();
				}

				// Animate to grid when snapping is active, otherwise just fix out-of-boundary positions
				self.scrollTo(self.__scrollLeft, self.__scrollTop, self.options.snapping);
			};

			// Start animation and switch on flag
			self.__isDecelerating = core.effect.Animate.start(step, verify, completed);

		},


		/**
		 * Called on every step of the animation
		 *
		 * @param inMemory {Boolean?false} Whether to not render the current step, but keep it in memory only. Used internally only!
		 */
		__stepThroughDeceleration: function(render) {

			var self = this;


			//
			// COMPUTE NEXT SCROLL POSITION
			//

			// Add deceleration to scroll position
			var scrollLeft = self.__scrollLeft + self.__decelerationVelocityX;
			var scrollTop = self.__scrollTop + self.__decelerationVelocityY;


			//
			// HARD LIMIT SCROLL POSITION FOR NON BOUNCING MODE
			//

			if (!self.options.bouncing) {

				var scrollLeftFixed = Math.max(Math.min(self.__maxDecelerationScrollLeft, scrollLeft), self.__minDecelerationScrollLeft);
				if (scrollLeftFixed !== scrollLeft) {
					scrollLeft = scrollLeftFixed;
					self.__decelerationVelocityX = 0;
				}

				var scrollTopFixed = Math.max(Math.min(self.__maxDecelerationScrollTop, scrollTop), self.__minDecelerationScrollTop);
				if (scrollTopFixed !== scrollTop) {
					scrollTop = scrollTopFixed;
					self.__decelerationVelocityY = 0;
				}

			}


			//
			// UPDATE SCROLL POSITION
			//

			if (render) {

				self.__publish(scrollLeft, scrollTop, self.__zoomLevel);

			} else {

				self.__scrollLeft = scrollLeft;
				self.__scrollTop = scrollTop;

			}


			//
			// SLOW DOWN
			//

			// Slow down velocity on every iteration
			if (!self.options.paging) {

				// This is the factor applied to every iteration of the animation
				// to slow down the process. This should emulate natural behavior where
				// objects slow down when the initiator of the movement is removed
				var frictionFactor = 0.95;

				self.__decelerationVelocityX *= frictionFactor;
				self.__decelerationVelocityY *= frictionFactor;

			}


			//
			// BOUNCING SUPPORT
			//

			if (self.options.bouncing) {

				var scrollOutsideX = 0;
				var scrollOutsideY = 0;

				// This configures the amount of change applied to deceleration/acceleration when reaching boundaries
				var penetrationDeceleration = self.options.penetrationDeceleration; 
				var penetrationAcceleration = self.options.penetrationAcceleration; 

				// Check limits
				if (scrollLeft < self.__minDecelerationScrollLeft) {
					scrollOutsideX = self.__minDecelerationScrollLeft - scrollLeft;
				} else if (scrollLeft > self.__maxDecelerationScrollLeft) {
					scrollOutsideX = self.__maxDecelerationScrollLeft - scrollLeft;
				}

				if (scrollTop < self.__minDecelerationScrollTop) {
					scrollOutsideY = self.__minDecelerationScrollTop - scrollTop;
				} else if (scrollTop > self.__maxDecelerationScrollTop) {
					scrollOutsideY = self.__maxDecelerationScrollTop - scrollTop;
				}

				// Slow down until slow enough, then flip back to snap position
				if (scrollOutsideX !== 0) {
					if (scrollOutsideX * self.__decelerationVelocityX <= 0) {
						self.__decelerationVelocityX += scrollOutsideX * penetrationDeceleration;
					} else {
						self.__decelerationVelocityX = scrollOutsideX * penetrationAcceleration;
					}
				}

				if (scrollOutsideY !== 0) {
					if (scrollOutsideY * self.__decelerationVelocityY <= 0) {
						self.__decelerationVelocityY += scrollOutsideY * penetrationDeceleration;
					} else {
						self.__decelerationVelocityY = scrollOutsideY * penetrationAcceleration;
					}
				}
			}
		}
	};

	// Copy over members to prototype
	for (var key in members) {
		Scroller.prototype[key] = members[key];
	}

})();

var EasyScroller = function(content, options) {

	this.content = content;
	this.container = content.parentNode;
	this.options = options || {};

	// create Scroller instance
	var that = this;
	this.scroller = new Scroller(function(left, top, zoom) {
		that.render(left, top, zoom);
	}, options);

	// bind events
	this.bindEvents();

	// the content element needs a correct transform origin for zooming
	this.content.style[EasyScroller.vendorPrefix + 'TransformOrigin'] = "left top";

	// reflow for the first time
	this.reflow();

};

EasyScroller.prototype.render = (function() {

	var docStyle = document.documentElement.style;

	var engine;

	if (window.opera && Object.prototype.toString.call(opera) === '[object Opera]') {
		engine = 'presto';
	} else if ('MozAppearance' in docStyle) {
		engine = 'gecko';
	} else if ('WebkitAppearance' in docStyle) {
		engine = 'webkit';
	} else if (typeof navigator.cpuClass === 'string') {
		engine = 'trident';
	}

	var vendorPrefix = EasyScroller.vendorPrefix = {
		trident: 'ms',
		gecko: 'Moz',
		webkit: 'Webkit',
		presto: 'O'
	}[engine];

	var helperElem = document.createElement("div");
	var undef;

	var perspectiveProperty = vendorPrefix + "Perspective";
	var transformProperty = vendorPrefix + "Transform";

	if (helperElem.style[perspectiveProperty] !== undef) {

		return function(left, top, zoom) {
			this.content.style[transformProperty] = 'translate3d(' + (-left) + 'px,' + (-top) + 'px,0) scale(' + zoom + ')';
		};

	} else if (helperElem.style[transformProperty] !== undef) {

		return function(left, top, zoom) {
			this.content.style[transformProperty] = 'translate(' + (-left) + 'px,' + (-top) + 'px) scale(' + zoom + ')';
		};

	} else {

		return function(left, top, zoom) {
			this.content.style.marginLeft = left ? (-left/zoom) + 'px' : '';
			this.content.style.marginTop = top ? (-top/zoom) + 'px' : '';
			this.content.style.zoom = zoom || '';
		};

	}
})();

EasyScroller.prototype.reflow = function() {

	// set the right scroller dimensions
	this.scroller.setDimensions(this.container.clientWidth, this.container.clientHeight, this.content.offsetWidth, this.content.offsetHeight);

	// refresh the position for zooming purposes
	var rect = this.container.getBoundingClientRect();
	this.scroller.setPosition(rect.left + this.container.clientLeft, rect.top + this.container.clientTop);

};

EasyScroller.prototype.bindEvents = function() {

	var that = this;

	// reflow handling
	window.addEventListener("resize", function() {
		that.reflow();
	}, false);

	// touch devices bind touch events
	if ('ontouchstart' in window) {

		this.container.addEventListener("touchstart", function(e) {

			// Don't react if initial down happens on a form element
			if (e.touches[0] && e.touches[0].target && e.touches[0].target.tagName.match(/input|textarea|select/i)) {
				return;
			}

			that.scroller.doTouchStart(e.touches, e.timeStamp);
			e.preventDefault();

		}, false);

		document.addEventListener("touchmove", function(e) {
			that.scroller.doTouchMove(e.touches, e.timeStamp, e.scale);
		}, false);

		document.addEventListener("touchend", function(e) {
			that.scroller.doTouchEnd(e.timeStamp);
		}, false);

		document.addEventListener("touchcancel", function(e) {
			that.scroller.doTouchEnd(e.timeStamp);
		}, false);

	// non-touch bind mouse events
	} else {

		var mousedown = false;

		this.container.addEventListener("mousedown", function(e) {

			if (e.target.tagName.match(/input|textarea|select/i)) {
				return;
			}

			that.scroller.doTouchStart([{
				pageX: e.pageX,
				pageY: e.pageY
			}], e.timeStamp);

			mousedown = true;
			e.preventDefault();

		}, false);

		document.addEventListener("mousemove", function(e) {

			if (!mousedown) {
				return;
			}

			that.scroller.doTouchMove([{
				pageX: e.pageX,
				pageY: e.pageY
			}], e.timeStamp);

			mousedown = true;

		}, false);

		document.addEventListener("mouseup", function(e) {

			if (!mousedown) {
				return;
			}

			that.scroller.doTouchEnd(e.timeStamp);

			mousedown = false;

		}, false);

		this.container.addEventListener("mousewheel", function(e) {
			if(that.options.zooming) {
				that.scroller.doMouseZoom(e.wheelDelta, e.timeStamp, e.pageX, e.pageY);
				e.preventDefault();
			}
		}, false);

	}

};

// automatically attach an EasyScroller to elements found with the right data attributes
// document.addEventListener("DOMContentLoaded", function() {

// 	var elements = document.querySelectorAll('[data-scrollable],[data-zoomable]'), element;
// 	for (var i = 0; i < elements.length; i++) {

// 		element = elements[i];
// 		var scrollable = element.dataset.scrollable;
// 		var zoomable = element.dataset.zoomable || '';
// 		var zoomOptions = zoomable.split('-');
// 		var minZoom = zoomOptions.length > 1 && parseFloat(zoomOptions[0]);
// 		var maxZoom = zoomOptions.length > 1 && parseFloat(zoomOptions[1]);

// 		new EasyScroller(element, {
// 			scrollingX: scrollable === 'true' || scrollable === 'x',
// 			scrollingY: scrollable === 'true' || scrollable === 'y',
// 			zooming: zoomable === 'true' || zoomOptions.length > 1,
// 			minZoom: minZoom,
// 			maxZoom: maxZoom
// 		});

// 	};

// }, false);

/**
 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
 *
 * @version 0.6.9
 * @codingstandard ftlabs-jsv2
 * @copyright The Financial Times Limited [All Rights Reserved]
 * @license MIT License (see LICENSE.txt)
 */

/*jslint browser:true, node:true*/
/*global define, Event, Node*/


/**
 * Instantiate fast-clicking listeners on the specificed layer.
 *
 * @constructor
 * @param {Element} layer The layer to listen on
 */
function FastClick(layer) {
    'use strict';
    var oldOnClick, self = this;


    /**
     * Whether a click is currently being tracked.
     *
     * @type boolean
     */
    this.trackingClick = false;


    /**
     * Timestamp for when when click tracking started.
     *
     * @type number
     */
    this.trackingClickStart = 0;


    /**
     * The element being tracked for a click.
     *
     * @type EventTarget
     */
    this.targetElement = null;


    /**
     * X-coordinate of touch start event.
     *
     * @type number
     */
    this.touchStartX = 0;


    /**
     * Y-coordinate of touch start event.
     *
     * @type number
     */
    this.touchStartY = 0;


    /**
     * ID of the last touch, retrieved from Touch.identifier.
     *
     * @type number
     */
    this.lastTouchIdentifier = 0;


    /**
     * Touchmove boundary, beyond which a click will be cancelled.
     *
     * @type number
     */
    this.touchBoundary = 10;


    /**
     * The FastClick layer.
     *
     * @type Element
     */
    this.layer = layer;

    if (!layer || !layer.nodeType) {
        throw new TypeError('Layer must be a document node');
    }

    /** @type function() */
    this.onClick = function() { return FastClick.prototype.onClick.apply(self, arguments); };

    /** @type function() */
    this.onMouse = function() { return FastClick.prototype.onMouse.apply(self, arguments); };

    /** @type function() */
    this.onTouchStart = function() { return FastClick.prototype.onTouchStart.apply(self, arguments); };

    /** @type function() */
    this.onTouchEnd = function() { return FastClick.prototype.onTouchEnd.apply(self, arguments); };

    /** @type function() */
    this.onTouchCancel = function() { return FastClick.prototype.onTouchCancel.apply(self, arguments); };

    if (FastClick.notNeeded(layer)) {
        return;
    }

    // Set up event handlers as required
    if (this.deviceIsAndroid) {
        layer.addEventListener('mouseover', this.onMouse, true);
        layer.addEventListener('mousedown', this.onMouse, true);
        layer.addEventListener('mouseup', this.onMouse, true);
    }

    layer.addEventListener('click', this.onClick, true);
    layer.addEventListener('touchstart', this.onTouchStart, false);
    layer.addEventListener('touchend', this.onTouchEnd, false);
    layer.addEventListener('touchcancel', this.onTouchCancel, false);

    // Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
    // which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
    // layer when they are cancelled.
    if (!Event.prototype.stopImmediatePropagation) {
        layer.removeEventListener = function(type, callback, capture) {
            var rmv = Node.prototype.removeEventListener;
            if (type === 'click') {
                rmv.call(layer, type, callback.hijacked || callback, capture);
            } else {
                rmv.call(layer, type, callback, capture);
            }
        };

        layer.addEventListener = function(type, callback, capture) {
            var adv = Node.prototype.addEventListener;
            if (type === 'click') {
                adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
                    if (!event.propagationStopped) {
                        callback(event);
                    }
                }), capture);
            } else {
                adv.call(layer, type, callback, capture);
            }
        };
    }

    // If a handler is already declared in the element's onclick attribute, it will be fired before
    // FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
    // adding it as listener.
    if (typeof layer.onclick === 'function') {

        // Android browser on at least 3.2 requires a new reference to the function in layer.onclick
        // - the old one won't work if passed to addEventListener directly.
        oldOnClick = layer.onclick;
        layer.addEventListener('click', function(event) {
            oldOnClick(event);
        }, false);
        layer.onclick = null;
    }
}


/**
 * Android requires exceptions.
 *
 * @type boolean
 */
FastClick.prototype.deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;


/**
 * iOS requires exceptions.
 *
 * @type boolean
 */
FastClick.prototype.deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent);


/**
 * iOS 4 requires an exception for select elements.
 *
 * @type boolean
 */
FastClick.prototype.deviceIsIOS4 = FastClick.prototype.deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


/**
 * iOS 6.0(+?) requires the target element to be manually derived
 *
 * @type boolean
 */
FastClick.prototype.deviceIsIOSWithBadTarget = FastClick.prototype.deviceIsIOS && (/OS ([6-9]|\d{2})_\d/).test(navigator.userAgent);


/**
 * Determine whether a given element requires a native click.
 *
 * @param {EventTarget|Element} target Target DOM element
 * @returns {boolean} Returns true if the element needs a native click
 */
FastClick.prototype.needsClick = function(target) {
    'use strict';
    switch (target.nodeName.toLowerCase()) {

    // Don't send a synthetic click to disabled inputs (issue #62)
    case 'button':
    case 'select':
    case 'textarea':
        if (target.disabled) {
            return true;
        }

        break;
    case 'input':

        // File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
        if ((this.deviceIsIOS && target.type === 'file') || target.disabled) {
            return true;
        }

        break;
    case 'label':
    case 'video':
        return true;
    }

    return (/\bneedsclick\b/).test(target.className);
};


/**
 * Determine whether a given element requires a call to focus to simulate click into element.
 *
 * @param {EventTarget|Element} target Target DOM element
 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
 */
FastClick.prototype.needsFocus = function(target) {
    'use strict';
    switch (target.nodeName.toLowerCase()) {
    case 'textarea':
    case 'select':
        return true;
    case 'input':
        switch (target.type) {
        case 'button':
        case 'checkbox':
        case 'file':
        case 'image':
        case 'radio':
        case 'submit':
            return false;
        }

        // No point in attempting to focus disabled inputs
        return !target.disabled && !target.readOnly;
    default:
        return (/\bneedsfocus\b/).test(target.className);
    }
};


/**
 * Send a click event to the specified element.
 *
 * @param {EventTarget|Element} targetElement
 * @param {Event} event
 */
FastClick.prototype.sendClick = function(targetElement, event) {
    'use strict';
    var clickEvent, touch;

    // On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
    if (document.activeElement && document.activeElement !== targetElement) {
        document.activeElement.blur();
    }

    touch = event.changedTouches[0];

    // Synthesise a click event, with an extra attribute so it can be tracked
    clickEvent = document.createEvent('MouseEvents');
    clickEvent.initMouseEvent('click', true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
    clickEvent.forwardedTouchEvent = true;
    targetElement.dispatchEvent(clickEvent);
};


/**
 * @param {EventTarget|Element} targetElement
 */
FastClick.prototype.focus = function(targetElement) {
    'use strict';
    var length;

    if (this.deviceIsIOS && targetElement.setSelectionRange) {
        length = targetElement.value.length;
        targetElement.setSelectionRange(length, length);
    } else {
        targetElement.focus();
    }
};


/**
 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
 *
 * @param {EventTarget|Element} targetElement
 */
FastClick.prototype.updateScrollParent = function(targetElement) {
    'use strict';
    var scrollParent, parentElement;

    scrollParent = targetElement.fastClickScrollParent;

    // Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
    // target element was moved to another parent.
    if (!scrollParent || !scrollParent.contains(targetElement)) {
        parentElement = targetElement;
        do {
            if (parentElement.scrollHeight > parentElement.offsetHeight) {
                scrollParent = parentElement;
                targetElement.fastClickScrollParent = parentElement;
                break;
            }

            parentElement = parentElement.parentElement;
        } while (parentElement);
    }

    // Always update the scroll top tracker if possible.
    if (scrollParent) {
        scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
    }
};


/**
 * @param {EventTarget} targetElement
 * @returns {Element|EventTarget}
 */
FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
    'use strict';

    // On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
    if (eventTarget.nodeType === Node.TEXT_NODE) {
        return eventTarget.parentNode;
    }

    return eventTarget;
};


/**
 * On touch start, record the position and scroll offset.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onTouchStart = function(event) {
    'use strict';
    var targetElement, touch, selection;

    // Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
    if (event.targetTouches.length > 1) {
        return true;
    }

    targetElement = this.getTargetElementFromEventTarget(event.target);
    touch = event.targetTouches[0];

    if (this.deviceIsIOS) {

        // Only trusted events will deselect text on iOS (issue #49)
        selection = window.getSelection();
        if (selection.rangeCount && !selection.isCollapsed) {
            return true;
        }

        if (!this.deviceIsIOS4) {

            // Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
            // when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
            // with the same identifier as the touch event that previously triggered the click that triggered the alert.
            // Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
            // immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
            if (touch.identifier === this.lastTouchIdentifier) {
                event.preventDefault();
                return false;
            }

            this.lastTouchIdentifier = touch.identifier;

            // If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
            // 1) the user does a fling scroll on the scrollable layer
            // 2) the user stops the fling scroll with another tap
            // then the event.target of the last 'touchend' event will be the element that was under the user's finger
            // when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
            // is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
            this.updateScrollParent(targetElement);
        }
    }

    this.trackingClick = true;
    this.trackingClickStart = event.timeStamp;
    this.targetElement = targetElement;

    this.touchStartX = touch.pageX;
    this.touchStartY = touch.pageY;

    // Prevent phantom clicks on fast double-tap (issue #36)
    if ((event.timeStamp - this.lastClickTime) < 200) {
        event.preventDefault();
    }

    return true;
};


/**
 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.touchHasMoved = function(event) {
    'use strict';
    var touch = event.changedTouches[0], boundary = this.touchBoundary;

    if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
        return true;
    }

    return false;
};


/**
 * Attempt to find the labelled control for the given label element.
 *
 * @param {EventTarget|HTMLLabelElement} labelElement
 * @returns {Element|null}
 */
FastClick.prototype.findControl = function(labelElement) {
    'use strict';

    // Fast path for newer browsers supporting the HTML5 control attribute
    if (labelElement.control !== undefined) {
        return labelElement.control;
    }

    // All browsers under test that support touch events also support the HTML5 htmlFor attribute
    if (labelElement.htmlFor) {
        return document.getElementById(labelElement.htmlFor);
    }

    // If no for attribute exists, attempt to retrieve the first labellable descendant element
    // the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
    return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
};


/**
 * On touch end, determine whether to send a click event at once.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onTouchEnd = function(event) {
    'use strict';
    var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

    // If the touch has moved, cancel the click tracking
    if (this.touchHasMoved(event)) {
        this.trackingClick = false;
        this.targetElement = null;
    }

    if (!this.trackingClick) {
        return true;
    }

    // Prevent phantom clicks on fast double-tap (issue #36)
    if ((event.timeStamp - this.lastClickTime) < 200) {
        this.cancelNextClick = true;
        return true;
    }

    this.lastClickTime = event.timeStamp;

    trackingClickStart = this.trackingClickStart;
    this.trackingClick = false;
    this.trackingClickStart = 0;

    // On some iOS devices, the targetElement supplied with the event is invalid if the layer
    // is performing a transition or scroll, and has to be re-detected manually. Note that
    // for this to function correctly, it must be called *after* the event target is checked!
    // See issue #57; also filed as rdar://13048589 .
    if (this.deviceIsIOSWithBadTarget) {
        touch = event.changedTouches[0];

        // In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
        targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
        targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
    }

    targetTagName = targetElement.tagName.toLowerCase();
    if (targetTagName === 'label') {
        forElement = this.findControl(targetElement);
        if (forElement) {
            this.focus(targetElement);
            if (this.deviceIsAndroid) {
                return false;
            }

            targetElement = forElement;
        }
    } else if (this.needsFocus(targetElement)) {

        // Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
        // Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
        if ((event.timeStamp - trackingClickStart) > 100 || (this.deviceIsIOS && window.top !== window && targetTagName === 'input')) {
            this.targetElement = null;
            return false;
        }

        this.focus(targetElement);

        // Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
        if (!this.deviceIsIOS4 || targetTagName !== 'select') {
            this.targetElement = null;
            event.preventDefault();
        }

        return false;
    }

    if (this.deviceIsIOS && !this.deviceIsIOS4) {

        // Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
        // and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
        scrollParent = targetElement.fastClickScrollParent;
        if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
            return true;
        }
    }

    // Prevent the actual click from going though - unless the target node is marked as requiring
    // real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
    if (!this.needsClick(targetElement)) {
        event.preventDefault();
        this.sendClick(targetElement, event);
    }

    return false;
};


/**
 * On touch cancel, stop tracking the click.
 *
 * @returns {void}
 */
FastClick.prototype.onTouchCancel = function() {
    'use strict';
    this.trackingClick = false;
    this.targetElement = null;
};


/**
 * Determine mouse events which should be permitted.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onMouse = function(event) {
    'use strict';

    // If a target element was never set (because a touch event was never fired) allow the event
    if (!this.targetElement) {
        return true;
    }

    if (event.forwardedTouchEvent) {
        return true;
    }

    // Programmatically generated events targeting a specific element should be permitted
    if (!event.cancelable) {
        return true;
    }

    // Derive and check the target element to see whether the mouse event needs to be permitted;
    // unless explicitly enabled, prevent non-touch click events from triggering actions,
    // to prevent ghost/doubleclicks.
    if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

        // Prevent any user-added listeners declared on FastClick element from being fired.
        if (event.stopImmediatePropagation) {
            event.stopImmediatePropagation();
        } else {

            // Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
            event.propagationStopped = true;
        }

        // Cancel the event
        event.stopPropagation();
        event.preventDefault();

        return false;
    }

    // If the mouse event is permitted, return true for the action to go through.
    return true;
};


/**
 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
 * an actual click which should be permitted.
 *
 * @param {Event} event
 * @returns {boolean}
 */
FastClick.prototype.onClick = function(event) {
    'use strict';
    var permitted;

    // It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
    if (this.trackingClick) {
        this.targetElement = null;
        this.trackingClick = false;
        return true;
    }

    // Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
    if (event.target.type === 'submit' && event.detail === 0) {
        return true;
    }

    permitted = this.onMouse(event);

    // Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
    if (!permitted) {
        this.targetElement = null;
    }

    // If clicks are permitted, return true for the action to go through.
    return permitted;
};


/**
 * Remove all FastClick's event listeners.
 *
 * @returns {void}
 */
FastClick.prototype.destroy = function() {
    'use strict';
    var layer = this.layer;

    if (this.deviceIsAndroid) {
        layer.removeEventListener('mouseover', this.onMouse, true);
        layer.removeEventListener('mousedown', this.onMouse, true);
        layer.removeEventListener('mouseup', this.onMouse, true);
    }

    layer.removeEventListener('click', this.onClick, true);
    layer.removeEventListener('touchstart', this.onTouchStart, false);
    layer.removeEventListener('touchend', this.onTouchEnd, false);
    layer.removeEventListener('touchcancel', this.onTouchCancel, false);
};


/**
 * Check whether FastClick is needed.
 *
 * @param {Element} layer The layer to listen on
 */
FastClick.notNeeded = function(layer) {
    'use strict';
    var metaViewport;

    // Devices that don't support touch don't need FastClick
    if (typeof window.ontouchstart === 'undefined') {
        return true;
    }

    if ((/Chrome\/[0-9]+/).test(navigator.userAgent)) {

        // Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
        if (FastClick.prototype.deviceIsAndroid) {
            metaViewport = document.querySelector('meta[name=viewport]');
            if (metaViewport && metaViewport.content.indexOf('user-scalable=no') !== -1) {
                return true;
            }

        // Chrome desktop doesn't need FastClick (issue #15)
        } else {
            return true;
        }
    }

    // IE10 with -ms-touch-action: none, which disables double-tap-to-zoom (issue #97)
    if (layer.style.msTouchAction === 'none') {
        return true;
    }

    return false;
};


/**
 * Factory method for creating a FastClick object
 *
 * @param {Element} layer The layer to listen on
 */
FastClick.attach = function(layer) {
    'use strict';
    return new FastClick(layer);
};


if (typeof define !== 'undefined' && define.amd) {

    // AMD. Register as an anonymous module.
    define(function() {
        'use strict';
        return FastClick;
    });
} else if (typeof module !== 'undefined' && module.exports) {
    module.exports = FastClick.attach;
    module.exports.FastClick = FastClick;
} else {
    window.FastClick = FastClick;
}

/*!

 handlebars v1.3.0

Copyright (C) 2011 by Yehuda Katz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@license
*/
var Handlebars = (function() {
// handlebars/safe-string.js
var __module3__ = (function() {
  "use strict";
  var __exports__;
  // Build out our basic SafeString type
  function SafeString(string) {
    this.string = string;
  }

  SafeString.prototype.toString = function() {
    return "" + this.string;
  };

  __exports__ = SafeString;
  return __exports__;
})();

// handlebars/utils.js
var __module2__ = (function(__dependency1__) {
  "use strict";
  var __exports__ = {};
  /*jshint -W004 */
  var SafeString = __dependency1__;

  var escape = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#x27;",
    "`": "&#x60;"
  };

  var badChars = /[&<>"'`]/g;
  var possible = /[&<>"'`]/;

  function escapeChar(chr) {
    return escape[chr] || "&amp;";
  }

  function extend(obj, value) {
    for(var key in value) {
      if(Object.prototype.hasOwnProperty.call(value, key)) {
        obj[key] = value[key];
      }
    }
  }

  __exports__.extend = extend;var toString = Object.prototype.toString;
  __exports__.toString = toString;
  // Sourced from lodash
  // https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
  var isFunction = function(value) {
    return typeof value === 'function';
  };
  // fallback for older versions of Chrome and Safari
  if (isFunction(/x/)) {
    isFunction = function(value) {
      return typeof value === 'function' && toString.call(value) === '[object Function]';
    };
  }
  var isFunction;
  __exports__.isFunction = isFunction;
  var isArray = Array.isArray || function(value) {
    return (value && typeof value === 'object') ? toString.call(value) === '[object Array]' : false;
  };
  __exports__.isArray = isArray;

  function escapeExpression(string) {
    // don't escape SafeStrings, since they're already safe
    if (string instanceof SafeString) {
      return string.toString();
    } else if (!string && string !== 0) {
      return "";
    }

    // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.
    string = "" + string;

    if(!possible.test(string)) { return string; }
    return string.replace(badChars, escapeChar);
  }

  __exports__.escapeExpression = escapeExpression;function isEmpty(value) {
    if (!value && value !== 0) {
      return true;
    } else if (isArray(value) && value.length === 0) {
      return true;
    } else {
      return false;
    }
  }

  __exports__.isEmpty = isEmpty;
  return __exports__;
})(__module3__);

// handlebars/exception.js
var __module4__ = (function() {
  "use strict";
  var __exports__;

  var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

  function Exception(message, node) {
    var line;
    if (node && node.firstLine) {
      line = node.firstLine;

      message += ' - ' + line + ':' + node.firstColumn;
    }

    var tmp = Error.prototype.constructor.call(this, message);

    // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
    for (var idx = 0; idx < errorProps.length; idx++) {
      this[errorProps[idx]] = tmp[errorProps[idx]];
    }

    if (line) {
      this.lineNumber = line;
      this.column = node.firstColumn;
    }
  }

  Exception.prototype = new Error();

  __exports__ = Exception;
  return __exports__;
})();

// handlebars/base.js
var __module1__ = (function(__dependency1__, __dependency2__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;

  var VERSION = "1.3.0";
  __exports__.VERSION = VERSION;var COMPILER_REVISION = 4;
  __exports__.COMPILER_REVISION = COMPILER_REVISION;
  var REVISION_CHANGES = {
    1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
    2: '== 1.0.0-rc.3',
    3: '== 1.0.0-rc.4',
    4: '>= 1.0.0'
  };
  __exports__.REVISION_CHANGES = REVISION_CHANGES;
  var isArray = Utils.isArray,
      isFunction = Utils.isFunction,
      toString = Utils.toString,
      objectType = '[object Object]';

  function HandlebarsEnvironment(helpers, partials) {
    this.helpers = helpers || {};
    this.partials = partials || {};

    registerDefaultHelpers(this);
  }

  __exports__.HandlebarsEnvironment = HandlebarsEnvironment;HandlebarsEnvironment.prototype = {
    constructor: HandlebarsEnvironment,

    logger: logger,
    log: log,

    registerHelper: function(name, fn, inverse) {
      if (toString.call(name) === objectType) {
        if (inverse || fn) { throw new Exception('Arg not supported with multiple helpers'); }
        Utils.extend(this.helpers, name);
      } else {
        if (inverse) { fn.not = inverse; }
        this.helpers[name] = fn;
      }
    },

    registerPartial: function(name, str) {
      if (toString.call(name) === objectType) {
        Utils.extend(this.partials,  name);
      } else {
        this.partials[name] = str;
      }
    }
  };

  function registerDefaultHelpers(instance) {
    instance.registerHelper('helperMissing', function(arg) {
      if(arguments.length === 2) {
        return undefined;
      } else {
        throw new Exception("Missing helper: '" + arg + "'");
      }
    });

    instance.registerHelper('blockHelperMissing', function(context, options) {
      var inverse = options.inverse || function() {}, fn = options.fn;

      if (isFunction(context)) { context = context.call(this); }

      if(context === true) {
        return fn(this);
      } else if(context === false || context == null) {
        return inverse(this);
      } else if (isArray(context)) {
        if(context.length > 0) {
          return instance.helpers.each(context, options);
        } else {
          return inverse(this);
        }
      } else {
        return fn(context);
      }
    });

    instance.registerHelper('each', function(context, options) {
      var fn = options.fn, inverse = options.inverse;
      var i = 0, ret = "", data;

      if (isFunction(context)) { context = context.call(this); }

      if (options.data) {
        data = createFrame(options.data);
      }

      if(context && typeof context === 'object') {
        if (isArray(context)) {
          for(var j = context.length; i<j; i++) {
            if (data) {
              data.index = i;
              data.first = (i === 0);
              data.last  = (i === (context.length-1));
            }
            ret = ret + fn(context[i], { data: data });
          }
        } else {
          for(var key in context) {
            if(context.hasOwnProperty(key)) {
              if(data) { 
                data.key = key; 
                data.index = i;
                data.first = (i === 0);
              }
              ret = ret + fn(context[key], {data: data});
              i++;
            }
          }
        }
      }

      if(i === 0){
        ret = inverse(this);
      }

      return ret;
    });

    instance.registerHelper('if', function(conditional, options) {
      if (isFunction(conditional)) { conditional = conditional.call(this); }

      // Default behavior is to render the positive path if the value is truthy and not empty.
      // The `includeZero` option may be set to treat the condtional as purely not empty based on the
      // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
      if ((!options.hash.includeZero && !conditional) || Utils.isEmpty(conditional)) {
        return options.inverse(this);
      } else {
        return options.fn(this);
      }
    });

    instance.registerHelper('unless', function(conditional, options) {
      return instance.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn, hash: options.hash});
    });

    instance.registerHelper('with', function(context, options) {
      if (isFunction(context)) { context = context.call(this); }

      if (!Utils.isEmpty(context)) return options.fn(context);
    });

    instance.registerHelper('log', function(context, options) {
      var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
      instance.log(level, context);
    });
  }

  var logger = {
    methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },

    // State enum
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3,
    level: 3,

    // can be overridden in the host environment
    log: function(level, obj) {
      if (logger.level <= level) {
        var method = logger.methodMap[level];
        if (typeof console !== 'undefined' && console[method]) {
          console[method].call(console, obj);
        }
      }
    }
  };
  __exports__.logger = logger;
  function log(level, obj) { logger.log(level, obj); }

  __exports__.log = log;var createFrame = function(object) {
    var obj = {};
    Utils.extend(obj, object);
    return obj;
  };
  __exports__.createFrame = createFrame;
  return __exports__;
})(__module2__, __module4__);

// handlebars/runtime.js
var __module5__ = (function(__dependency1__, __dependency2__, __dependency3__) {
  "use strict";
  var __exports__ = {};
  var Utils = __dependency1__;
  var Exception = __dependency2__;
  var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
  var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;

  function checkRevision(compilerInfo) {
    var compilerRevision = compilerInfo && compilerInfo[0] || 1,
        currentRevision = COMPILER_REVISION;

    if (compilerRevision !== currentRevision) {
      if (compilerRevision < currentRevision) {
        var runtimeVersions = REVISION_CHANGES[currentRevision],
            compilerVersions = REVISION_CHANGES[compilerRevision];
        throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. "+
              "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").");
      } else {
        // Use the embedded version info since the runtime doesn't know about this revision yet
        throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. "+
              "Please update your runtime to a newer version ("+compilerInfo[1]+").");
      }
    }
  }

  __exports__.checkRevision = checkRevision;// TODO: Remove this line and break up compilePartial

  function template(templateSpec, env) {
    if (!env) {
      throw new Exception("No environment passed to template");
    }

    // Note: Using env.VM references rather than local var references throughout this section to allow
    // for external users to override these as psuedo-supported APIs.
    var invokePartialWrapper = function(partial, name, context, helpers, partials, data) {
      var result = env.VM.invokePartial.apply(this, arguments);
      if (result != null) { return result; }

      if (env.compile) {
        var options = { helpers: helpers, partials: partials, data: data };
        partials[name] = env.compile(partial, { data: data !== undefined }, env);
        return partials[name](context, options);
      } else {
        throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
      }
    };

    // Just add water
    var container = {
      escapeExpression: Utils.escapeExpression,
      invokePartial: invokePartialWrapper,
      programs: [],
      program: function(i, fn, data) {
        var programWrapper = this.programs[i];
        if(data) {
          programWrapper = program(i, fn, data);
        } else if (!programWrapper) {
          programWrapper = this.programs[i] = program(i, fn);
        }
        return programWrapper;
      },
      merge: function(param, common) {
        var ret = param || common;

        if (param && common && (param !== common)) {
          ret = {};
          Utils.extend(ret, common);
          Utils.extend(ret, param);
        }
        return ret;
      },
      programWithDepth: env.VM.programWithDepth,
      noop: env.VM.noop,
      compilerInfo: null
    };

    return function(context, options) {
      options = options || {};
      var namespace = options.partial ? options : env,
          helpers,
          partials;

      if (!options.partial) {
        helpers = options.helpers;
        partials = options.partials;
      }
      var result = templateSpec.call(
            container,
            namespace, context,
            helpers,
            partials,
            options.data);

      if (!options.partial) {
        env.VM.checkRevision(container.compilerInfo);
      }

      return result;
    };
  }

  __exports__.template = template;function programWithDepth(i, fn, data /*, $depth */) {
    var args = Array.prototype.slice.call(arguments, 3);

    var prog = function(context, options) {
      options = options || {};

      return fn.apply(this, [context, options.data || data].concat(args));
    };
    prog.program = i;
    prog.depth = args.length;
    return prog;
  }

  __exports__.programWithDepth = programWithDepth;function program(i, fn, data) {
    var prog = function(context, options) {
      options = options || {};

      return fn(context, options.data || data);
    };
    prog.program = i;
    prog.depth = 0;
    return prog;
  }

  __exports__.program = program;function invokePartial(partial, name, context, helpers, partials, data) {
    var options = { partial: true, helpers: helpers, partials: partials, data: data };

    if(partial === undefined) {
      throw new Exception("The partial " + name + " could not be found");
    } else if(partial instanceof Function) {
      return partial(context, options);
    }
  }

  __exports__.invokePartial = invokePartial;function noop() { return ""; }

  __exports__.noop = noop;
  return __exports__;
})(__module2__, __module4__, __module1__);

// handlebars.runtime.js
var __module0__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
  "use strict";
  var __exports__;
  /*globals Handlebars: true */
  var base = __dependency1__;

  // Each of these augment the Handlebars object. No need to setup here.
  // (This is done to easily share code between commonjs and browse envs)
  var SafeString = __dependency2__;
  var Exception = __dependency3__;
  var Utils = __dependency4__;
  var runtime = __dependency5__;

  // For compatibility and usage outside of module systems, make the Handlebars object a namespace
  var create = function() {
    var hb = new base.HandlebarsEnvironment();

    Utils.extend(hb, base);
    hb.SafeString = SafeString;
    hb.Exception = Exception;
    hb.Utils = Utils;

    hb.VM = runtime;
    hb.template = function(spec) {
      return runtime.template(spec, hb);
    };

    return hb;
  };

  var Handlebars = create();
  Handlebars.create = create;

  __exports__ = Handlebars;
  return __exports__;
})(__module1__, __module3__, __module4__, __module2__, __module5__);

  return __module0__;
})();

(function() {

    if (!Function.prototype.bind) {

        Function.prototype.bind = function(oThis) {

            if (typeof this !== 'function') {
                // closest thing possible to the ECMAScript 5 internal IsCallable function
                throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
            }

            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                EmptyFn = function() {},
                fBound = function() {
                    return fToBind.apply(this instanceof EmptyFn ? this : oThis || window,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            EmptyFn.prototype = this.prototype;
            fBound.prototype = new EmptyFn();

            return fBound;
        };

    }

}());
/**
 * @class Roc
 * @singleton
 *
 * Framework global namespace.
 *
 * ### Class definition example

    App.MyClass = (function() {

        // private instances shared variable declaration
        var _parent = Roc;

        return _parent.subclass({

            constructor: function(config) {
                // Class constructor
                _parent.prototype.constructor.call(this, config);
            }

        });

    }());
 */

(function() {

    //'use strict';

    var slice = Array.prototype.slice,
        _xtypes = {};

    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {
            return (_xtypes[xtype] ||
                console.warn('[core] xtype "' + xtype +
                    '" does not exists.'));
        }
    };

    var _createSubclass = function(props) {

        props = props || {};

        var key,
            realConstructor,
            superclass = props.superclass.prototype;

        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }

        function constructor() {

            var self = this;

            if (!(self instanceof constructor)) {
                throw new Error('[core] Please use "new" when initializing  classes');
            }
            realConstructor.apply(self, arguments);
        }

        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;

        _extend(constructor, {

            parent: superclass,

            subclass: function(obj) {

                var sclass;

                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);

                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });

        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }

        return constructor;
    };

    var _extend = function() {

        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();

        while (++i < l) {

            var key,
                props = args[i];

            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };

    var core = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        /**
         * @readonly
         * @property {String} version
         * Current framework version.
         */
        version: '1.21.0',

        /**
         * Allows to extend from class.
         * @method
         * @param {Object} obj
         * @return {Object}
         */
        subclass: function(obj) {
            var sclass;

            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);

            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = core;
        }
        exports.Roc = core;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return core;
        });
    } else {
        window.Roc = core;
    }

})();

Roc.debug = new (function() {

    var _profiles = {},
        _debug = false,
        _whitelist = [/*
            'router',
            'news',
            'store',
            'request',
            'newsdetail',
            'button',
            'list',
            //'click',
            //'clickbuster',
            'listbuffered',
            'loadmask',
            'scroll',
            'request',
            'bounce'
        */];

    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };

    return Roc.subclass({

        /*profile: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.time(key);
            //_profiles[key] = this.memory();
            return this;
        },*/

        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },

        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },

        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },

        /*profileEnd: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.timeEnd(key);
            //console.debug(_profiles[key], '->', this.memory());
            //console.debug(key, 'Before: ', _profiles[key]);
            //console.debug(key, 'Now: ', (window.performance.memory.usedJSHeapSize / 1024.0) + 'ko');
            return this;
        },

        memory: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            if (window.performance && window.performance.memory) {
                return ((window.performance.memory.totalJSHeapSize / 1024.0) + 'ko');
            }
            return 0;
        },*/

        set: function(debug) {
            _debug = (debug === true);
        }
    });

}())();

/**
 * @class Roc.utils
 * @singleton
 */
Roc.utils = (function() {

    'use strict';

    var _table = '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';

    return {
        /**
         * Apply base[key] to obj[key] if obj[key] is undefined.
         *
         * ### Sample usage
         *
         *     var obj = { a: true, b: "toto", c: undefined },
         *         base = { a: false, b: "tata", c: [1, 2], d: 1 };
         *
         *     var results = Roc.utils.applyIf(obj, base, ['a', 'c', 'd']);
         *
         *     // results = { a: true, b: "toto", c: [1, 2], d: 1 };
         *
         * @param {Object} obj The receiver of the properties.
         * @param {Object} base The source of the properties.
         * @param {Array} keys List of keys to copy.
         * @return {Object} returns obj
         */
        applyIf: function(obj, base, keys) {

            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof obj[key] === 'undefined' &&
                    typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of base to obj.
         * @param {Object} obj The receiver of properties.
         * @param {Object} base The source of properties.
         * @return {Object} returns obj
         */
        applyIfAuto: function(obj, base) {

            var key;

            for (key in base) {
                if (typeof obj[key] === 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of base to obj for given keys.
         * @param {Object} obj The receiver of properties.
         * @param {Object} base The source of properties.
         * @param {Array} keys Keys to apply.
         * @return {Object} Updated obj.
         */
        apply: function(obj, base, keys) {

            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        /**
         * Apply every values of both objects to new object.
         * It means we merge obj1 with obj2 into new object.
         * If same key is in both objects, obj2 values will be taken.
         * @param {Object} obj1
         * @param {Object} obj2
         * @return {Object} Merged obj1 and obj2 objects.
         */
        applyAuto: function(obj1, obj2) {

            var attrname, obj3 = {};

            for (attrname in obj1) {
                obj3[attrname] = obj1[attrname];
            }
            for (attrname in obj2) {
                obj3[attrname] = obj2[attrname];
            }
            return obj3;
        },

        /**
         * Copy all array values from given start index to given end index.
         * @param {Array} array Array to copy.
         * @param {Number} from (optional) Index to copy data from. Defaults to start.
         * @param {Number} to (optional) Index to copy data to. Defaults to end.
         * @return {Array} Copied array.
         */
        arraySlice: function(array, from, to) {

            var result = [],
                i = (from ? (from - 1) : 0),
                length = (to ? to : array.length);

            while (++i < length) {
                result.push(array[i]);
            }
            return result;
        },

        /**
         * Capitalize given string and return it.
         * @param {String} str String to capitalize.
         * @return {String} Capitalized string.
         */
        capitalize: function(str) {
            return (str.charAt(0).toUpperCase() + str.slice(1));
        },

        /**
         * Returns the crc32 for the given string.
         * @param {String} str String.
         * @param {Number} crc (optional) CRC, defaults to 0.
         * @return {Number} CRC32 for given string.
         */
        crc32: function(str, crc) {
            if (crc == window.undefined) {
                crc = 0;
            }

            var i = 0,
                iTop = str.length,
                n = 0, //a number between 0 and 255
                x = 0; //an hex number

            crc = crc ^ (-1);
            for (; i < iTop; i++ ) {
                n = ( crc ^ str.charCodeAt(i)) & 0xFF;
                x = '0x' + _table.substr(n * 9, 8);
                crc = (crc >>> 8) ^ x;
            }
            return crc ^ (-1);
        }
    };

}());

/**
 * @class Roc.Storage
 * @extends Roc
 * @singleton
 *
 * ### Example usage
 *
    var s = Roc.Storage;

    // (optional) set a prefix for our application
    s.setPrefix('myapp');

    // you can store item by name
    s.setItem('news', [{
        id: 1,
        title: 'News 1',
        content: '...'
    }, {
        id: 2,
        title: 'News 2',
        content: '...'
    }]);

    // you can retrieve those items by name too
    s.getItem('news');

    // or remove them by name
    s.removeItem('news');

    // or completly erase all data for given prefix
    s.empty('myapp');
 *
 */
Roc.Storage = new (function() {

    'use strict';

    var _store = localStorage,
        _prefix = '';

    /**
     * Set a default prefix for all store items key.
     * @method setPrefix
     * @param {String} prefix Prefix.
     * @return {Boolean} true if success else false.
     */
    function _setPrefix(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    }

    /**
     * Get the default prefix.
     * @method getPrefix
     * @param {String} prefix Prefix.
     * @return {String} Return given prefix if correct else default prefix.
     */
    function _getPrefix(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    }

    /**
     * Store value for given name.
     * @method setItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _setItem(name, value, prefix) {
        return (_store.setItem(_getPrefix(prefix) + name,
            JSON.stringify(value)));
    }

    /**
     * Get value stored for given name.
     * @method getItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _getItem(name, prefix) {

        var value = _store.getItem(_getPrefix(prefix) + name);

        // @bugfix for android 2.3 when JSON.parse null
        if (typeof value === 'string') {
            return JSON.parse(value);
        }
        return false;
    }

    /**
     * Remove value at given name.
     * @method removeItem
     * @param {String} name Name.
     * @param {String} prefix (optional) Prefix.
     * @return {Boolean} True if success else false.
     */
    function _removeItem(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    }

    /**
     * Empty all stored data for given prefix or all if no prefix.
     * @method empty
     * @param {String} prefix (optional) Prefix.
     */
    function _empty(prefix) {
        prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    }

    return Roc.subclass({

        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty

    });

}())();

/**
 * @singleton
 * @class Roc.Selector
 * @extends Roc
 *
 * DOM manipulation and utilities.
 */
Roc.Selector = new (function() {

    //'use strict';

    // @private
    var _scope,
        _elUid = 0,
        _win = window;

    // public
    return Roc.subclass({

        /**
         * Create new selector.
         * @param {Object} gscope The global scope. Defaults to `window`.
         */
        constructor: function(gscope) {
            _scope = gscope;
        },

        /**
         * Generate new DOM uniq identifier.
         * @param {String} prefix (optional) Prefix.
         * @return {String} id.
         */
        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'fs';
            return (prefix + _elUid);
        },

        /**
         * Get a DOM Node from query selector.
         *
         * @param {String} selector Selector
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {HTMLElement} result.
         */
        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },

        /**
         * Get all DOM nodes from query selector.
         * @param {String} selector Selector.
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {Array} Array of HTMLElement matches.
         */
        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },

        /**
         * Execute callback recursivly on element parent until
         * callback returns false.
         * @param {HTMLElement} el DOM node.
         * @param {Function} callback Callback.
         * @return {Mixed}
         */
        findParent: function(el, callback) {
            if (callback(el) === false && el.parentNode) {
                return this.findParent(el.parentNode, callback);
            }
        },

        /**
         * Apply given css properties to element.
         * @param {HTMLElement} el DOM node.
         * @param {Object} props CSS properties.
         * @return {Boolean} success
         */
        css: function(el, props) {
            Object.keys(props).forEach(function(name) {
                el.style[name] = props[name];
            });
            return true;
        },

        /**
         * Checks if element has CSS class.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean}
         */
        hasClass: function(el, cls) {
            // <debug>
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el);//, arguments.callee.caller.caller);
                return false;
            }
            // </debug>
            return (el.classList && el.classList.contains(cls));
            //return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') !== -1);
        },

        /**
         * Adds CSS class to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                //console.log('addClass ====== ', el.id, cls);
                // el.className += ' ' + cls;
                // el.className = el.className.replace(/^\s+|\s+$/g, '');
                el.classList.add(cls);
                return true;
            }
            return false;
        },

        /**
         * Add CSS classes from given element.
         * Use this method instead of {@link #addClass} when you are not sure if one or several of those classes are already applied on element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        addClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.addClass(el, c[len]);
            }
            return true;

        },

        /**
         * Removes CSS class from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                // var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                // el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                el.classList.remove(cls);
                return true;
            }
            return false;
        },

        /**
         * Remove CSS classes from given element.
         * Use this method instead of {@link #removeClass} when you are not sure if one or several of those classes are already applied or removed from element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        removeClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.removeClass(el, c[len]);
            }
            return true;
        },

        /**
         * Force browser to redraw given element.
         * @param {HTMLElement} el DOM node.
         */
        redraw: function(el) {
            el.style.display = 'none';
            var h = el.offsetHeight;
            el.style.display = 'block';
        },

        /**
         * Removes given element from DOM.
         * @param {HTMLElement} el DOM node.
         */
        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },

        /**
         * Removes all HTML from given element.
         * @param {HTMLElement} el DOM node.
         */
        removeHtml: function(el) {
            //el.innerHTML = '';
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },

        // !TODO doc
        findParentByTag: function(el, tagName) {
            while (el) {
                if (el.tagName === tagName) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        // !TODO doc
        findParentByClass: function(el, className) {
            while (el) {
                if (this.hasClass(el, className)) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        /**
         * Adds HTML to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },

        /**
         * Updates HTML from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },

        /**
         * Scrolls to given offsets for given element.
         * @param {HTMLElement} el DOM node.
         * @param {Number} offsetX (optional) Offset x. Defaults to 0.
         * @param {Number} offsetY (optional) Offset y. Defaults to 0.
         */
        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });

}())(document);

/**
 * @class Roc.Request
 * @extends Roc
 * @singleton
 *
 * Request class to perform remote request.
 * Used by {@link Roc.data.Store#proxy store proxy}.
 *
 * ### Example
 *
 *      Roc.Request.ajax('/login', {
 *          jsonParams: {
 *              username: 'mrsmith',
 *              password: 'wanna die?'
 *          },
 *          scope: this,
 *          callback: function(requestSuccess, data) {
 *
 *              if (requestSuccess === true) {
 *
 *                  if (typeof data === 'object' &&
 *                      data.success === true) {
 *                      // welcome !
 *                  } else
 *                      // login error
 *                  }
 *
 *              } else {
 *                  // server error
 *              }
 *
 *           }
 *      });
 *
 */
Roc.Request = new (function() {

    'use strict';

    // @private
    var jsonp_id = 0,
        _debug = Roc.debug;

    var _jsonToString = function(json, prefix) {

        var key,
            val,
            result = '',
            first = true;

        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }
        _debug.log('request', 'jsonToString result', result);
        return (prefix ? prefix : '') + result;
    };

    // public
    return Roc.subclass({
        /**
         * Makes an AJAX request.
         *
         * ## Example
         *
         *      Roc.Request.ajax('/getdata', {
         *          callback: function(success, data) {},
         *          scope: this,
         *          method: 'PUT', // GET, POST, PUT, DELETE
         *          data: 'name1=value1&name2=value2',
         *          params: 'name1=value1&name2=value2', // (for GET)
         *          jsonParams: { name1: 'value1', name2: 'value2' } // (for GET)
         *      });
         *
         * @param {String} url URL.
         * @param {Object} options Options.
         * @method
         */
        ajax: function(url, options) {

            var request,
                callback = (typeof options.callback === 'function' ?
                    options.callback.bind(options.scope || window) : null);
                options.method = options.method || 'GET';
                options.data = options.data || '';

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }

            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },

        /**
         * Makes a JSONP request.
         *
         * ## Example
         *
         *     Roc.Request.jsonp('/getdata?callback={callback}', {
         *         callback: function(success, data) {},
         *         scope: this,
         *         params: 'sortby=date&dir=desc',
         *         jsonParams: {
         *             sortby: 'date',
         *             dir: 'desc'
         *         }
         *     });
         *
         * @param {String} url URL.
         * @param {Object} options An object which may contain the following properties. Note that options will
         * take priority over any defaults that are specified in the class.
         * <ul>
         * <li><b>params</b> : String (Optional)<div class="sub-desc">A string containing additional parameters.</div></li>
         * <li><b>jsonParams</b> : Object (Optional)<div class="sub-desc">An object containing a series of
         * key value pairs that will be sent along with the request.</div></li>
         * <li><b>callback</b> : Function (Optional) <div class="sub-desc">A function to execute when the request
         * completes, whether it is a success or failure.</div></li>
         * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope in
         * which to execute the callbacks: The "this" object for the callback function. Defaults to the browser window.</div></li>
         * </ul>
         * @method
         */
        jsonp: function(url, options) {

            var callback_name = ['jsonp', ++jsonp_id].join(''),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    _debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        },

        /**
         * Store memory proxy.
         *
         * @method
         * @param {Array} data Data.
         * @param {Object} options Options.
         * @param {@link Roc.data.Store} store Store.
         */
        memory: function(data, options, store) {

            var i, length, callback,
                result = {},
                idProp = store.getReaderId(),
                totalProp = store.getReaderTotal(),
                successProp = store.getReaderSuccess(),
                rootProp = store.getReaderRoot();

            callback = options.callback.bind(options.scope || window);
            if (!data) {
                data = store.config.data || [];
            }
            length = data.length;
            if (data[0] && typeof data[0][idProp] === 'undefined') {
                for (i = 0; i < length; ) {
                    data[i][idProp] = ++i;
                }
            }
            result[totalProp] = length;
            result[successProp] = true;
            result[rootProp] = data;
            if (callback) {
                callback(true, result);
            }
        }
    });
}())();

/**
 * @class Roc.History
 * @extends Roc
 * @singleton
 * @requires Roc.Storage
 *
 * History support class. Use with {@link Roc.Router router}.
 *
 * ### Example
 *
 *      var history = Roc.History;
 *
 *      history.start();
 *      history.navigate('/home');
 *
 *      var currentUrl = history.here();
 *
 */
Roc.History = new (function() {

    'use strict';

    // @private
    var _ns = Roc,
        _win = window,
        scope = _win,
        _storage = _ns.Storage,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [],
        _history = [],
        _maxLength = 20,
        _curLength = 0;

    function runCallbacks(self, location) {
        /*if (self.curhash === location) {
            return false;
        }*/
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }

        var regexp,
            route,
            i = -1,
            length = routes.length;

        self.curhash = location;

        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                route.callback(location);
            }
        }

        return true;
    }

    var onHashChange = function(event) {

        var here = _win.location.hash.substr(1);

        _curLength = _history.push(here);

        if (_curLength > _maxLength) {
            _history.shift();
        }
        runCallbacks(this, here);
    };

    // public
    return _ns.subclass({

        /**
         * Create new history.
         * @param {Object} gscope The global scope. Defaults to `window`.
         */
        constructor: function(gscope) {
            /**
             * @property {String} defaultRoute The default route.
             */
            this.defaultRoute = '';
            scope = gscope || _win;
            onHashChange = onHashChange.bind(this);
            scope.addEventListener('hashchange', onHashChange);
        },

        /**
         * Set the default route.
         * @param {String} path Path.
         */
        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },

        /**
         * Start history.
         * If browser is standalone it will recover to last visited url or {@link #defaultRoute defaultRoute}.
         */
        start: function() {

            var hash = curhash;

            curhash = '';

            // if added to homescreen, try to restore previous location
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks(this, hash);
        },

        /**
         * Stop history.
         */
        stop: function() {
            scope.removeEventListener('hashchange', onHashChange);
        },

        /**
         * Get the current location.
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Current location.
         */
        here: function(encoded) {

            var hash = _win.location.hash.substr(1);

            return (encoded ? encodeURIComponent(hash) : hash);
        },

        /**
         * Get history item from given index.
         * @param {Number} index
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Location for given index.
         */
        get: function(index, encoded) {

            var pos = _curLength - 1 + index;

            if (pos >= _curLength) {
                return this.here(encoded);
            } else if (pos < 0) {
                pos = 0;
            }
            return (encoded ? encodeURIComponent(_history[pos]) : _history[pos]);
        },

        /**
         * Go back in history.
         * @param {Number} index (optional)
         */
        back: function(index) {
            index = (typeof index !== 'number' ? -1 : index);
            _win.history.go(index);
        },

        /**
         * Navigate to given location.
         * @param {String} location.
         */
        navigate: function(location) {
            _win.location.hash = '#' + location;
        },

        /**
         * Add given route.
         * @param {String} route Route
         * @param {Function} callback Callback.
         */
        route: function(route, callback) {
            routes.push({
                regexp: route,
                callback: callback
            });
        }
    });
}())(window);

/**
 * @class Roc.Router
 * @extends Roc
 *
 * ### Example
 *

 *
 */
Roc.Router = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _history = _ns.History,
        optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;

    function _extractParameters(route, fragment) {
        return route.exec(fragment).slice(1);
    }

    function _routeToRegExp(route) {

        route = route.replace(escapeRegExp, '\\$&')
            .replace(optionalParam, '(?:$1)?')
            .replace(namedParam, function(match, optional) {
                return optional ? match : '([^\/]+)';
            })
            .replace(splatParam, '(.*?)');

        return new RegExp('^' + route + '$');
    }

    function _prepareBefore(self) {

        if (self.before) {

            var pattern;

            self.beforeRegexp = {};
            for (pattern in self.before) {
                /* @ignore !TODO permettre de prendre en compte un array de callback
                if (typeof self.before[i] === 'string') {
                    self.before[i] = [self.before[i]];
                }
                */
                self.beforeRegexp[pattern] = new RegExp(pattern);
            }
        }
    }

    function _getBefore(self, funcname) {

        if (self.before) {

            var pattern;

            for (pattern in self.before) {
                if (self.beforeRegexp[pattern].test(funcname) === true) {
                //if (funcname.match(self.beforeRegexp[i]) !== null) {
                    var func = self[self.before[pattern]];

                    return (func ? func : false);
                }
            }
        }
        return false;
    }

    // public
    return _ns.subclass({
        /**
         * @cfg {Object} routes
         */
        /**
         * @cfg {Object} before
         */
        /**
         * Create new router.
         */
        constructor: function() {

            var self = this;

            _prepareBefore(self);
            if (self.routes) {

                var pattern;

                for (pattern in self.routes) {
                    self.route(pattern, self.routes[pattern]);
                }
            }
        },

        /**
         * Calls given path with callback.
         *
         * @param {String} path Path.
         * @param {Function} foo Route callback.
         * @method
         */
        route: function(path, foo) {

            var before,
                self = this,
                callback = self[foo],
                history = _history;

            if (callback) {
                path = _routeToRegExp(path);
                before = _getBefore(self, foo);

                var cb = function(fragment) {

                    var args = _extractParameters(path, fragment);

                    callback.apply(self, args);
                },
                beforeCb = function(fragment) {
                    before.call(self, function() {
                        return cb(fragment);
                    });
                };

                if (before) {
                    history.route(path, beforeCb);
                } else {
                    history.route(path, cb);
                }
            }

            return self;
        }
    });
}());

/**
 * @ignore !TODO for debugging: Global event handler like Roc.EventHandler.listenAllEvents('afterrender');
 */
/**
 * @class Roc.Event
 * @extends Roc
 * @requires Roc.utils
 *
 * Event manager class.
 *
 * ## Example

    var event = new Roc.Event({
        listeners: {
            scope: this,
            afterload: function(success, data) {
                // do some stuff here...
            },
            beforeload: function(request) {
                if (typeof request.data === 'undefined') {
                    return false; // stop all next events from being firing.
                }
            }
        }
    });

    // Registering listener for `error` event.
    var eid = event.on('error', function(code, message) {
        alert('Error (' + code + ') occured: \n' + message);
    }, this);

    // Firing `error` event.
    event.fire('error', 401, 'Unauthorized !');

    // Unregistering listener.
    event.off(eid);

 */
Roc.Event = (function() {

    'use strict';

    var _ns = Roc,
        _utils = _ns.utils,
        _parent = _ns;

    function _setupEvents(self) {
        if (typeof self.events !== 'object') {
            // events queue
            /*

            e1 = event_id

            {
                "eventname1": {
                    "998": [e1, e2],
                    "1000": [e3]
                },
                ...
            }

            */
            self.events = {};
            self.uids = 0;
            // priority queue @TODO
            /*

            {
                "eventname1": [998, 1000],
                "eventname2": [1000],
                "eventname3": [100, 1000, 9999],
                ...
            }

            */
            self.pqueue = {};
            // callbacks queue
            /*

            {
                "event_id": [event_name, priority, callback, scope],
                ...
            }

            */
            self.equeue = {};
        }
    }

    function _numSort(a, b) {
        return (b - a);
    }

    function _count_listeners(self, name) {

        var e = self.events[name];

        if (e) {
            var total = 0,
                keys = Object.keys(self.events[name]),
                i = keys.length;

            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    }

    /**
     * @cfg {Object} listeners Listeners.
     */
    return _parent.subclass({

        /**
         * @property {Number} priority. Prior to higher.
         * <p><ul>
            <li><b>CORE:</b> Core event priority (high)</li>
            <li><b>VIEWS:</b> Views event priority (medium)</li>
            <li><b>DEFAULT:</b> Default event priority (low)</li>
         * </ul></p>
         */
        priority: {
            CORE: 1000,
            VIEWS: 900,
            DEFAULT: 800
        },

        /**
         * @property {Number} defaultPriority Default priority.
         */
        defaultPriority: 800,

        /**
         * Create new event manager.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {
            // <inline>

            var self = this;

            if (opts && opts.listeners) {
                self.listeners = opts.listeners;
                self.wasListened = {};
            }
            _setupEvents(self);
            // </inline>
        },

        /**
         * Initialize listeners for given event and renderer.
         * @method
         * @param {Array} eventNames List of event names.
         * @param {@link Roc.views.Template} renderer (optional) Renderer. Defaults to current class instance.
         */
        setupListeners: function(eventNames, renderer) {

            var self = this;

            if (self.listeners) {

                var name,
                    scope = self.listeners.scope || self,
                    i = eventNames.length;

                renderer = renderer || self;

                while (i--) {
                    name = eventNames[i];
                    if (self.listeners[name] &&
                        !self.wasListened[name]) {
                        self.wasListened[name] = true;
                        renderer.on(name, self.listeners[name], scope);
                    }
                }
            }
        },

        /**
         * Checks to see if this object has any listeners for a specified event.
         * @method
         * @param {String} name Event name.
         * @return {Boolean} True if the event is being listened for, else false.
         */
        hasListener: function(name) {
            if (typeof this.listeners === 'object' &&
                typeof this.listeners[name] === 'function') {
                return true;
            }
            return false;
        },

        /**
         * Return listeners for given event name.
         * @method
         * @param {String} name Event name.
         * @return {Function} Listener callback for given event name.
         */
        getListener: function(name) {
            return this.listeners[name];
        },

        /**
         * Fires given event with given extra parameters.
         *
         * ## Example
         *
         *     // Registering event listener
         *     var eid = this.on('afterload', function(success, data) {
         *         console.log('Received data !', success, data);
         *     }, this);
         *
         *     // Fireing event
         *     this.fire('afterload', true, 'Some data');
         *
         *     // Unregistering event
         *     this.off(eid);
         *
         * @method
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fire: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1);

                while (++i < plen) {
                    events = self.events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function');//, arguments.callee.callee, e);
                        } else if (e[2].apply(e[3], args) === false) {
                            return false;
                        }
                    }

                }
                return true;
            }
        },

        fireScope: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1);

                while (++i < plen) {
                    events = self.events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function');//, arguments.callee.callee, e);
                        } else if (e[2].apply(e[3], [e[3]].concat(args)) === false) {
                            return false;
                        }
                    }

                }
                return true;
            }
        },

        /**
         * Register listener for given eventname.
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this, this.priority.VIEWS);
         *
         * @method
         * @param {String} name Event name.
         * @param {Function} cb Listener callback.
         * @param {Object} scope (optional) Callback scope. Defaults to `window`.
         * @param {Number} priority (optional) Callback priority. Defaults to `Roc.Event.priority.DEFAULT`.
         * @return {Number} Returns listener uniq identifier `uid`.
         */
        on: function(name, cb, scope, priority) {

            var self = this;

            priority = priority || self.defaultPriority;

            if (typeof self.events[name] === 'undefined') {
                self.events[name] = {};
            }

            if (typeof self.events[name]['p' + priority] === 'undefined') {
                self.events[name]['p' + priority] = [];
            }

            if (typeof self.pqueue[name] === 'undefined') {
                self.pqueue[name] = [];
            }

            if (self.pqueue[name].indexOf(priority) === -1) {
                self.pqueue[name].push(priority);
                self.pqueue[name].sort(_numSort);
            }
            scope = scope || window;
            self.equeue['u' + (++self.uids)] = [name, priority, cb, scope];
            self.events[name]['p' + priority].push(self.uids);
            return self.uids;
        },

        /**
         * Unregister listener for given listener uniq identifier (returns by `on` method).
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this);
         *      // ...
         *      this.off(eid);
         *
         * @method
         * @param {Number} uid Event listener uniq identifier (returns by `on` method).
         * @return {Mixed} Returns false if error else return event listener count.
         */
        off: function(uid) {

            var self = this,
                e = self.equeue['u' + uid];

            if (e) {

                var cb,
                    length,
                    name = e[0],
                    priority = e[1],
                    listeners = self.events[name]['p' + priority];

                listeners.splice(listeners.indexOf(uid), 1);
                length = listeners.length;

                delete self.equeue['u' + uid];

                if (!length) {
                    // cleaning hash tables
                    delete self.events[name]['p' + priority];
                    self.pqueue[name].splice(self.pqueue[name].indexOf(priority), 1);
                    if (!self.pqueue[name].length) {
                        delete self.pqueue[name];
                        delete self.events[name];
                        return 0;
                    }
                }
                return _count_listeners(self, name);
            }
            return false;
        }
    });

}());

/**
 * @class Roc.Templates
 * @extends Roc
 * @requires Handlebars
 */
Roc.Templates = new (function() {

    'use strict';

    // private
    var _ns = Roc,
        _handlebars = Handlebars,
        _request = _ns.Request;

    _handlebars.currentParent = [];

    /**
     * Render given configuration element.
     *
     * ### Sample usage
     * 
     * Where items are for example:
     * items: [{ xtype: 'button', config: { text: 'Hello' }}]
     */
    // <unstable> Use it carefully. Still testing performance memory/cpu.
    _handlebars.registerHelper('render', function(config) {

        var html,
            parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = new (_ns.xtype(config.xtype))(config),
            renderer = ((parent.renderer && parent.renderer.el) ? parent.abstractRenderer : parent.renderer);

        /*
            abstractRenderer is used for calling afterrender event for item compile after
            the parent renderer has been rendered.
            For example: the list renders her children after render itself so we need to use
            the abstract renderer.
            !TODO We should store the item inside parents collections to be able to delete it or retrieve it.
        */
        if (!renderer) {
            renderer = parent.abstractRenderer = new _ns.Event();
        }

        html = item.compile(renderer.config, renderer);
        return new _handlebars.SafeString(html);
    });
    // </unstable>

    /**
     * Get current config from given item children index.
     *
     * ### Sample usage
     * 
     */
    _handlebars.registerHelper('config', function(context, options) {

        var parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = (parent.items ? parent.items[context] : undefined);

        if (typeof item === 'object' && item.config) {
            return options.fn(item.config);
        }
        return options.fn(this);

    });

    // @ignore !TODO permettre d'avoir la config d'un item a partir de son index et/ou id
    /*_handlebars.registerHelper('getconfig', function() {

    });*/

    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });

    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });

    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });

    /*
     * If Not Equals
     * if_neq this compare=that
     * Example: 
     */
    _handlebars.registerHelper('if_neq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context != options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    /*
     * If Greather than Equals
     * if_gteq this compare=that
     * Example: 
     */
    _handlebars.registerHelper('if_gteq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context >= options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    function loadtpl(self, name, cb) {

        if (_handlebars.templates[name]) {
            cb(_handlebars.templates[name]);
        } else {

            var url = self.path + name + '.handlebars';

            _request.ajax(url, {
                scope: self,
                callback: function(success, data) {
                    if (success) {
                        _handlebars.templates[name] = self.compile(data);
                        return cb(_handlebars.templates[name]);
                    }
                    console.warn('Error while loading template "' + url + '".');
                    return cb(false);
                }
            });
        }
    }

    function loadtpls(self, names, cb) {

        var results = {},
            i = names.length,
            remaining = names.length;

        function loaded(tplname) {
            return function(tpl) {
                results[tplname] = tpl;
                if (!(--remaining)) {
                    cb(results);
                }
            };
        }

        while (i--) {
            loadtpl(self, names[i], loaded(names[i]));
        }
    }

    return _ns.subclass({

        /**
         * Create new templates class.
         */
        constructor: function() {
            this.path = '';
        },

        /**
         * Set path for loading external template files.
         * Used by {@link Roc.App#require application} class.
         * @param {String} path Path.
         */
        setPath: function(path) {
            this.path = path;
        },

        /**
         * Get template by given name.
         * @param {String} name Template name.
         * @return {Function} Template function.
         */
        get: function(name) {
            return _handlebars.templates[name];
        },

        /**
         * Load given template names and call callback when finished.
         * @param {Array} names Template names.
         * @param {Function} callback Callback.
         */
        load: function(names, cb) {
            if (typeof names === 'object') {
                return loadtpls(this, names, cb);
            }
            return loadtpl(this, names, cb);
        },

        /**
         * Compile given source or retrieve the template.
         * @param {Mixed} source The source.
         * @return {Function} Function corresponding to the given source.
         */
        compile: function(source) {

            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }

            return function() {
                return source;
            };
        }
    });
}())();

/**
 * @class Roc.Settings
 * @extends Roc.Event
 * @singleton
 *
 * ### Usage example

    var session = new Roc.Settings({
        username: 'mrsmith',
        logged: true
    });

    session.on('loggedchanged', function(s, n, value, oldValue) {
        if (value === false) {
            // show login
        } else {
            // hide login
        }
    }, this);

    session.set('logged', false);

 *
 */
Roc.Settings = (function() {

    'use strict';

    var _ns = Roc,
        _parent = _ns.Event;

    return _parent.subclass({

        /**
         * New Settings.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(config) {
            this.vars = config || {};
            _parent.prototype.constructor.apply(this, arguments);
        },

        /**
         * Set value for given name.
         * @param {String} name Name.
         * @param {Mixed} value Value.
         * @fires settingschanged
         * @fires `name`changed
         */
        set: function(name, value) {

            var oldValue = this.get(name);

            this.vars[name] = value;
            this.fire('settingchanged', this, name, value, oldValue);
            this.fire(name + 'changed', this, name, value, oldValue);
        },

        /**
         * Get value from given name.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        get: function(name, defaultValue) {
            if (this.vars[name]) {
                return this.vars[name];
            }
            return defaultValue;
        },

        /**
         * Gets value from given names.
         *
         * ## Sample usage
         *
         *      var loginUrl = settings.gets('serverUrl', 'loginPath').join('');
         *
         * @param {String...} names Variable names.
         * @return {Array} Values for given names put into array.
         */
        gets: function() {

            var result = [],
                i = -1,
                length = arguments.length;

            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        },

        /**
         * @fires nameempty
         * Empty given variable name.
         * @param {String} name Variable name.
         */
        empty: function(name) {
            if (typeof this.vars[name] !== 'undefined') {
                delete this.vars[name];
                this.fire(name + 'empty', this, name);
            }
        }
    });

}());

/**
 * @class Roc.App
 * @extends Roc
 *
 * ### Sample usage:
 *
    var app = new Roc.App({
        onready: function() {
            var main = new App.controllers.Main();
        },
        defaultRoute: '/home',
        engine: 'JqueryMobile',
        ui: 'c',
        debug: false,
        require: {
            templates: [
                'home',
                'news'
            ]
        }
    });
 *
 */
Roc.App = (function() {

    'use strict';

    var _ns = Roc,
        _debug = _ns.debug,
        _storage = _ns.Storage,
        _engines = _ns.engines;

    function _get_engine(name, defaultEngine) {

        var engine = _engines[name];

        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    }

    function _register_listener(self) {

        function ready() {
            if (!self.domReady) {
                document.removeEventListener('DOMContentLoaded', ready);
            }

            self.domReady = true;
            if (self.templateLoaded === true) {
                self.config.onready();
            }
        }

        document.addEventListener('DOMContentLoaded', ready);
    }

    /**
     * @cfg {Function} onready Callback when application ready to start.
     */

    /**
     * @cfg {String} engine Engine name. Defaults to 'Homemade'.
     */

    /**
     * @cfg {String} defaultRoute (optional) Defaults application route.
     */

    /**
     * @cfg {Boolean} debug (optional) Enables or disables debug mode.
     */

    /**
     * @cfg {Object} require (optional) Required components.
     *
     * ## Sample usage:
     *
     *      require: {
     *          templates: [
     *              'home',
     *              'menu',
     *              ...
     *          ]
     *      }
     *
     */

    /**
     * @cfg {String} ui (optional) Defaults ui for all components (can be override by component configuration).
     */
    return _ns.subclass({

        defaultEngine: 'Homemade',
        templateLoaded: true,
        domReady: false,

        /**
         * Create new application.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _ns.History.setDefaultRoute(self.config.defaultRoute);
            }
            if (self.config.require && self.config.require.templates) {
                self.templateLoaded = false;
                _ns.Templates.load(self.config.require.templates, function() {
                    self.templateLoaded = true;

                    if (self.domReady === true &&
                        self.config.onready) {
                        self.config.onready();
                    }
                });
            }
            if (self.config.onready) {
                _register_listener(self);
            }
            // Setting default engine to global configuration object.
            _ns.config.engine = _get_engine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _ns.config.ui = self.config.ui;
            }
        }
    });

})();

/**
 * @private
 * @class Roc.events.Abstract
 * @extends Roc
 * @xtype events.abstract
 *
 * Abstract event class. Allows DOM events to be merged together or standalone.
 *
 * ## Extending abstract class
 *
 *        Roc.events.Keypress = (function() {
 *
 *            var _parent = Roc.events.Abstract;
 *
 *            return _parent.subclass({
 *
 *                xtype: 'events.keypress',
 *                eventName: 'keypress',
 *                globalFwd: window,
 *
 *                constructor: _parent.prototype.constructor
 *
 *            });
 *
 *        }());
 *
 */
Roc.events.Abstract = (function() {

    'use strict';

    var _ns = Roc,
        _debug = _ns.debug,
        _eventsAttached = {},
        _events = {},
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    function _global_handler(name) {
        return function(e) {

            var handler = _events[name][e.target.id];

            if (handler) {
                handler(e);
            }
        };
    }

    function _global_attach(self) {

        if (!_eventsAttached[self.eventName]) {
            _eventsAttached[self.eventName] = true;
            self.globalFwd.addEventListener(self.eventName,
                _global_handler(self.eventName), self.useCapture);
        }
        if (!self.el.id || !self.el.id.length) {
            self.el.id = _ns.Selector.generateId();
        }
        if (!_events[self.eventName]) {
            _events[self.eventName] = {};
        }
        _events[self.eventName][self.el.id] = self.handler;

    }

    function _global_detach(self) {

        delete _events[self.eventName][self.el.id];
        if (_eventsAttached[self.eventName] &&
            !_events[self.eventName].length) {
            _eventsAttached[self.eventName] = false;
            self.globalFwd.removeEventListener(self.eventName,
                _global_handler, self.useCapture);
        }

    }

    return _ns.subclass({

        xtype: 'events.abstract',

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        useCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {String} eventName Browser event name.
         * @property {String} eventName Browser event name.
         * Should be override by super class or when constructor called.
         */
        eventName: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * @property {Object} globalFwd Global forward scope.
         * Used when we want to listen event on window, and
         * forward to target.
         */
        globalFwd: false,

        /**
         * New abstract event instance.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach',
                'globalFwd'
            ]);
            if (!self.eventName && opts.eventName) {
                self.eventName = opts.eventName;
            }
            self.defaultEl = opts.el || window;
            self.defaultScope = (opts.scope ? opts.scope : self);
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_attach(self);
                } else {
                    self.el.addEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (self.eventName) {
                if (self.globalFwd) {
                    _global_detach(self);
                } else {
                    self.el.removeEventListener(self.eventName,
                        self.handler, self.useCapture);
                }
            }
        }
    });

})();

/**
 * @class Roc.events.Click
 * @extends Roc
 * @requires FastClick
 * @xtype events.click
 *
 * Click event class. Also remove 300ms delay for touch device.
 *
 * ## Example
 *
 *       var clickEvent = new Roc.events.Click({
 *           autoAttach: true,
 *           el: document.getElementById('mybutton'),
 *           scope: this,
 *           handler: showPopup
 *       });
 *
 */
Roc.events.Click = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _win = window,
        _fastclick = _win.FastClick;
        //_isMobile = ('ontouchstart' in document.documentElement)

    function _no_handler() {
        console.warn('click', 'No handler is define !');
    }

    _win.addEventListener('load', function() {
        _fastclick.attach(document.body);
    }, false);

    function _stop_scroll(e) {
        e.preventDefault();
    }

    // public
    return _ns.subclass({

        xtype: 'events.click',

        /**
         * @cfg {Boolean} disableScrolling (optional) Enable or disable scrolling on attached element.
         * Defaults to `false`.
         */
        defaultDisableScrolling: false,

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        defaultUseCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * New click event.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.disableScrolling = opts.disableScrolling || self.defaultDisableScrolling;
            self.useCapture = opts.useCapture || self.defaultUseCapture;
            _ns.utils.apply(self, opts, [
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);

            /**
             * @property {Boolean} attached True if event is attached.
             */
            self.attached = false;

            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);

            self.el.addEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.addEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }

            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;

            self.el.removeEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.removeEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        }
    });
}());

/**
 * @class Roc.events.Scroll
 * @extends Roc
 * @xtype events.scroll
 *
 * Scroll event class.
 *
 * ## Example
 *
 *       var scrollEvent = new Roc.events.Scroll({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: loadMoreItem
 *       });
 *
 */
Roc.events.Scroll = (function() {

    'use strict';

    // private
    var _lastEvent,
        _intervalId,
        _ns = Roc,
        _debug = _ns.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,//_window.screen.availHeight,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _ns.Event();

    function _noHandler() {
        _debug.error('scroll', '[Roc.events.Scroll] No handler is define !');
    }

    function _poolHandler() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    }

    function _windowScrollListener() {
        _scrolled = true;
        _lastEvent = event;
    }

    function _windowResizeListener() {
        _screenHeight = _doc.height;//_window.screen.availHeight;
    }



    // public
    return _ns.subclass({

        xtype: 'events.scroll',
        defaultInterval: 167, // 60fps
        useCapture: false,
        autoAttach: false,
        // defaults
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.interval = opts.interval || self.defaultInterval;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _intervalId = _window.setInterval(_poolHandler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('scroll', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;

            var length = _events.off(self.euid);

            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();

/**
 * @class Roc.events.Resize
 * @extends Roc
 * @xtype events.resize
 *
 * Resize event class.
 *
 * ## Example
 *
 *       var resizeEvent = new Roc.events.Resize({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: updateLayout
 *       });
 *
 */
Roc.events.Resize = (function() {

    'use strict';

    // private
    var _lastEvent,
        _intervalId,
        _window = window,
        _eventsAttached = false,
        _resized = false,
        _ns = Roc,
        _debug = _ns.debug,
        _events = new _ns.Event();

    function _no_handler() {
        _debug.warn('events.resize', 'No handler is define !');
    }

    function _pool_handler() {
        if (_resized === true) {
            _resized = false;
            _events.fire('resize', _lastEvent);
        }
    }

    function _window_resize_listener(event) {
        _resized = true;
        _lastEvent = event;
    }

    // public
    return _ns.subclass({

        xtype: 'events.resize',
        defaultInterval: 500,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.interval = opts.interval || self.defaultInterval;
            self.attached = false;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _window_resize_listener, false);
                _intervalId = _window.setInterval(_pool_handler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('resize', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;

            var length = _events.off(self.euid);

            delete self.handler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _window_resize_listener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();

/**
 * var nobounce = new Roc.events.Nobounce({
     className: 'nobounce', // optional
     autoAttach: true, // optional
     el: document // optional
   });
 * nobounce.attach(document);
 * nobounce.detach(document);
 */
Roc.events.Nobounce = (function() {

    // private
    var _lastEvent,
        _ns = Roc,
        _selector = _ns.Selector,
        _debug = _ns.debug,
        _doc = document,
        _parent = _ns;

    var _isParentNoBounce = function(el, className) {
        while (el) {
            if (_selector.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _disableBounce = function(e) {
        _debug.log('nobounce', 'disable bounce ?');
        if (_isParentNoBounce(e.target, this.className)) {
            _debug.log('nobounce', 'disabling bounce !');
            e.preventDefault();
        }
    };

    // public
    return _parent.subclass({

        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,

        constructor: function(opts) {
            opts = opts || {};
            this.autoAttach = opts.autoAttach || this.defaultAutoAttach;
            this.className = opts.className || this.defaultClassName;
            this.el = opts.el || this.defaultEl;
            this.attached = false;
            this.handler = _disableBounce.bind(this);
            if (this.autoAttach === true) {
                this.attach(this.el);
            }
        },

        attach: function(cmp) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            this.el = cmp || this.el;
            this.el.addEventListener('touchmove', this.handler, false);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el;
            this.el.removeEventListener('touchmove', this.handler, false);
        }
    });
})();

/**
 * @class Roc.events.Focus
 * @extends Roc.events.Abstract
 * @xtype events.focus
 *
 * Input focus event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var focusEvent = new Roc.events.Focus({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on focus called
 *             Roc.Selector.addClass(focusEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
Roc.events.Focus = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.focus',

        /**
         * @readonly
         * @property {String} eventName Browser event name.
         */
        eventName: 'focusin',

        /**
         * @readonly
         * @property {Object} globalFwd Global forward scope.
         */
        globalFwd: window,

        /**
         * New focus event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class Roc.events.Blur
 * @extends Roc.events.Abstract
 * @xtype events.blur
 *
 * Input blur event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var blurEvent = new Roc.events.Blur({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on blur called
 *             Roc.Selector.removeClass(blurEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
Roc.events.Blur = (function() {

    // private
    var _ns = Roc,
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.blur',

        /**
         * @readonly
         */
        eventName: 'focusout',

        /**
         * @readonly
         */
        globalFwd: window,

        /**
         * New blur event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class Roc.events.Keypress
 * @extends Roc.events.Abstract
 * @xtype events.keypress
 *
 * Input keypress event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var keypressEvent = new Roc.events.Keypress({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on keypress called
 *             if (e.keyCode === 13) {
 *                 this.form.submit();
 *             }
 *         }
 *     });
 *
 */
Roc.events.Keypress = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.keypress',

        /**
         * @readonly
         */
        eventName: 'keypress',

        /**
         * @readonly
         */
        globalFwd: window,

        /**
         * New keypress event.
         */
        constructor: _parent.prototype.constructor
    });
}());

(function() {

    var _ns = window.Roc;

    if (!_ns.physics) {
        _ns.physics = {};
    }

}());

Roc.physics.Scroll = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.Event,
        $ = _ns.Selector,
        ua = navigator.userAgent.toLowerCase(),
        isAndroid = (ua.indexOf('android') > -1);

    // public
    return _parent.subclass({

        className: 'Roc.physics.Scroll',
        xtype: 'scroll',

        /**
         *
         */
        constructor: function(element, options) {

            var self = this;

            if (isAndroid === false) {

                $.addClass(element.parentNode, 'scroll-content');
                $.addClass(element, 'scroll');

                self.options = options = options || {};

                self.scroller = new EasyScroller(element, {
                    scrollingX: options.scrollable === true || options.scrollable === 'x',
                    scrollingY: options.scrollable === true || options.scrollable === 'y',
                    zooming: options.zoomable === true,
                    minZoom: parseFloat(options.minZoom),
                    maxZoom: parseFloat(options.maxZoom)
                });

            } else {

                $.css($.get('html'), {
                    height: '100%'
                });
                $.css(element, {
                    overflow: 'auto',
                    position: 'absolute',
                    width: '100%',
                    height: '100%'
                });
                $.css($.get('body'), {
                    position: 'relative',
                    minHeight: '100%',
                    top: '0px'
                });
                $.findParent(element, function(el) {
                    if ($.hasClass(el, 'view')) {
                        $.removeClass(el, 'view');
                        return true;
                    }
                    return false;
                });

            }

            _parent.prototype.constructor.call(self);

        }

    });
})();

/**
 * @class Roc.views.Template
 * @extends Roc.Event
 * @xtype template
 *
 * Base view template class.
 */
Roc.views.Template = (function() {

    'use strict';

    var _ns = Roc,
        _gconf = _ns.config,
        _parent = _ns.Event,
        _selector = _ns.Selector,
        _handlebars = Handlebars;
        //_observer = _win.MutationObserver || _win.WebKitMutationObserver

    function _registerEngineEvents(self) {

        if (!self.renderer ||
            self.engineEventsRegistered) {
            return false;
        }
        self.engineEventsRegistered = true;

        var i, e, events = self.engine.getEvents(self.xtype);

        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    self.renderer.on(i, events[i], self, self.priority.VIEWS);
                } else {
                    self.on(i, events[i], self, self.priority.VIEWS);
                }
            }
        }
        return true;
    }

    /*
    function _registerEngineOverrides() {
        if (this.engineOverridesRegistered) {
            return false;
        }
        this.engineOverridesRegistered = true;

        var i, o, overrides = this.engine.getOverrides(this.xtype);

        for (i in overrides) {
            o = overrides[i];

            if (typeof o === 'function') {
                this[i] = o;
            }
        }
        return true;
    };
    */

    return _parent.subclass({

        className: 'Roc.views.Template',
        xtype: 'template',

        /**
         * New template.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {
                template: '',
                config: {},
                items: []
            };

            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtpl = opts.xtpl;
            }

            /**
             * @cfg {Object} config (optional) Template configuration.
             * This variable will be the scope while executing template.
             */
            self.config = opts.config || {};
            if (!self.config.id) {
                self.config.id = _selector.generateId();
            }

            // <testing>
            //delete opts.config;
            //self.config = opts;
            // </testing>

            if (opts.defaults) {
                self.defaults = opts.defaults;
            }

            /**
             * @cfg {Mixed} items (optional) Template children items.
             *
             * ## Different possibilities:
             *
             *     var mypage = new Roc.views.Page({
             *         items: [{
             *             xtype: 'header',
             *             config: {
             *                 title: 'My page'
             *             }
             *         }, {
             *             xtype: 'content',
             *             items: '<h1>My content</h1>'
             *         }, {
             *             xtype: 'footer',
             *             items: [new Roc.views.TabButton({
             *                 ...
             *             }), {
             *                 xtype: 'tabbutton',
             *                 ...
             *             }, 'Raw HTML', ...]
             *         }]
             *     })
             *
             */
            self.items = opts.items || [];
            if (!self.items.indexOf) {
                self.items = [self.items];
            }
            self.data = self.config;
            self.data.items = [];
            //self.data.items = self.items;
            if (!opts.template) {
                opts.template = self.setEngine(opts, _gconf).getTpl(self.xtpl || self.xtype);
            } else {
                self.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }
            if (opts.ref) {
                self.ref = opts.ref;
            }
            if (opts.refScope) {
                self.refScope = opts.refScope;
            }
            self.tpl = opts.template;
            self.html = '';
            self.renderToEl = '';
            self.el = '';
            self.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(self, arguments);
        },

        getEngine: function(config, gconfig) {
            if (config && config.engine) {
                return config.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }

            return this.engine;
        },

        setEngine: function(config, gconfig) {

            var self = this;

            self.engine = self.getEngine(config, gconfig);

            self.engine = new self.engine();

            return self.engine;
        },

        /**
         * @fires aftercompile
         */
        compile: function(parentConfig, renderer) {

            var item, tpl,
                self = this,
                i = -1,
                items = self.items,//.slice(0),
                length = self.items.length;

            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _ns.utils.applyIf(self.config, parentConfig, ['ui']);
            }

            while (++i < length) {

                item = items[i];

                if (self.defaults && typeof item === 'object') {
                    _ns.utils.applyIfAuto(item, self.defaults);
                }

                if (typeof item === 'object' &&
                    typeof item.xtype === 'string' &&
                    !(item instanceof _parent)) {
                    //typeof item.__proto__.xtype === 'undefined') {
                    //typeof Object.getPrototypeOf(item).xtype === 'undefined') {
                    item = new (_ns.xtype(item.xtype))(item);
                    item.parent = self;
                }
                if (typeof item === 'string') {
                    tpl = _ns.Templates.compile(item);
                    self.data.items[i] = tpl(self.config);
                } else if (typeof item.compile === 'function') {
                    item.parent = self;
                    self.data.items[i] = item.compile(self.config, renderer || self);
                } else {
                    self.data.items[i] = item;
                }
            }

            _handlebars.currentParent.push(self);

            self.html = self.tpl(self.data);

            _handlebars.currentParent.pop();

            if (!self.renderer) {
                self.renderer = renderer || self;
                if (self.ref && !self.refScope) {
                    self.refScope = self.renderer;
                }
            }
            // @todo Check if this kind of thing is not too much perf killer
            // and double check memory leak.
            // <unstable>
            if (self.ref &&
                !self.refScope[self.ref]) {
                self.refScope[self.ref] = self;
            }
            // </unstable>

            self.__arID = self.renderer.on('afterrender', function() {
                self.el = document.getElementById(self.config.id);
                //self.parentEl = self.el.parentNode;
                self.renderer.off(self.__arID);
                delete self.__arID;
            }, self, self.priority.CORE);

            self.setupListeners([

                /**
                 * @event aftercompile
                 */
                'aftercompile',

                /**
                 * @event afterrender
                 */
                'afterrender'
            ], self.renderer);

            self.fire('aftercompile', self, self.renderer);
            _registerEngineEvents(self);

            return self.html;
        },

        /**
         * position: <!-- beforebegin --><p><!-- afterbegin -->foo<!-- beforeend --></p><!-- afterend -->
         * @fires afterrender
         */
        render: function(renderToEl, position) {

            var self = this;

            position = position || 'beforeEnd';

            if (typeof renderToEl === 'string') {
                //renderToEl = _ns.Selector.get(renderToEl);
                self.renderToEl = _ns.Selector.get(renderToEl);
            } else {
                self.renderToEl = renderToEl;
            }

            self.renderToEl.insertAdjacentHTML(position, self.html);
            self.fireScope('afterrender', self);

            if (!self.el) {
                self.el = document.getElementById(self.config.id);
            }
        },

        remove: function() {
            if (this.el) {
                this.el.parentNode.removeChild(this.el);
                delete this.el;
                // @ignore !TODO remove all events automatically this.fire('remove', this);
                // should store all events (like click, resize etc.) inside instance array var
            } else {
                console.warn('Element cannot been removed because it has not been rendered !', this);
            }
        },

        /* @ignore !TODO update: function(config, items) {
            var self = this;

            self.data = config;
            self.data.items = items;

            // /!\ Be aware of cleaning event before calling update method.
            self.parentEl.innerHTML = '';
            self.compile();
            self.render(self.parentEl);
        },*/

        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        }
    });

}());

Roc.views.Tabs = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.eventClick = new _ns.events.Click({
            autoAttach: true,
            el: self.el,
            scope: self,
            handler: self.handler
        });
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.Tabs',
        xtype: 'tabs',

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            if (opts.handler) {
                self.handler = opts.handler;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        handler: function(event) {

            var self = this,
                route = self.config.route;

            self.fire('select', self, event);
            if (route) {
                _ns.History.navigate(route);
            }
        }

    });
})();

(function() {

    var handlebars = Handlebars;

    handlebars.registerHelper('safe', function(str) {
        return new handlebars.SafeString(str);
    });

}());

this["Handlebars"] = this["Handlebars"] || {};

Handlebars.registerPartial("items", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, self=this, helperMissing=helpers.helperMissing, functionType="function", blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var stack1, helper, options;
  stack1 = (helper = helpers.safe || (depth0 && depth0.safe),options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, depth0, options) : helperMissing.call(depth0, "safe", depth0, options));
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program2(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.items) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.items); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.items) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
this["Handlebars"] = this["Handlebars"] || {};

Handlebars.registerPartial("id", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
this["Handlebars"] = this["Handlebars"] || {};

Handlebars.registerPartial("style", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " style=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.style) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.style); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.style) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
this["Handlebars"] = this["Handlebars"] || {};

Handlebars.registerPartial("css_class", Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.css_class) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.css_class); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.css_class) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }));
// this is deprecated should use css_class
Handlebars.registerPartial('cssClass', Handlebars.partials.css_class);

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["tabs"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " tabs-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  buffer += "<div class=\"tabs";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
    {
        config: { ... }, // Depends on your tpl.
        engine: 'JqueryMobile', // (Optional) 'Homemade', 'JqueryMobile', ...
        tpl: '<div id="mytpl">Hello</div>', // Inline string template.
        xtpl: 'content' // String with engine xtype template name.
    }
*/
Roc.views.Abstract = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _templates = _ns.Templates,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Abstract',
        xtype: 'abstract',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtype = opts.xtpl;
            }
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["bubble"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " badge-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  buffer += "<span class=\"badge";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  if (helper = helpers.label) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n";
  return buffer;
  });
Handlebars.registerHelper('foreach', function(arr, options) {

    var length = arr.length;

    if (options.inverse && !length) {
        return options.inverse(this);
    }

    return arr.map(function(item, index) {
        if (typeof item !== 'object') {
            item = {
                original: item
            };
        }
        item.$index = index;
        item.$first = index === 0;
        item.$last  = index === (length - 1);
        return options.fn(item);
    }).join('');

});

(function() {

    var _handlebars = Handlebars;

    /*
     * {{!-- {{get iconPos default="left" choices="left|right"}} --}}
     */
    _handlebars.registerHelper('get', function(context, options) {

        var hash = options.hash;

        if (typeof context === 'undefined' ||
            (hash.choices &&
            hash.choices.indexOf(context) === -1)) {
            return hash['default'];
        }
        return context;
    });

}());

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["listitemthumbnail"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<a class=\"item item-thumbnail-"
    + escapeExpression((helper = helpers.get || (depth0 && depth0.get),options={hash:{
    'default': ("left"),
    'choices': ("left|right")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.imagePosition), options) : helperMissing.call(depth0, "get", (depth0 && depth0.imagePosition), options)))
    + "\" href=\"javascript:;\">";
  options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}
  if (helper = helpers.image) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.image); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.image) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data}
  if (helper = helpers.title) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data}
  if (helper = helpers.desc) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.desc); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.desc) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  buffer += "<img src=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\">";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "";
  buffer += "<h2>"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h2>";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "";
  buffer += "<p>"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</p>";
  return buffer;
  }

  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["listitemavatar"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<a class=\"item item-avatar\" href=\"javascript:;\">";
  options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}
  if (helper = helpers.image) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.image); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.image) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data}
  if (helper = helpers.title) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data}
  if (helper = helpers.desc) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.desc); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.desc) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  buffer += "<img src=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\">";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "";
  buffer += "<h2>"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h2>";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "";
  buffer += "<p>"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</p>";
  return buffer;
  }

  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
/*
 * If Equals
 * if_eq this compare=that
 * Example:
   {{!--
    {{#if_eq disabled compare=true default=false}} ui-disabled{{/if_eq}}
    --}}
 */
Handlebars.registerHelper('if_eq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context == options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["listitemfull"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': ("divider")
  },inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.type), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.type), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += "<div class=\"item item-divider\">";
  if (helper = helpers.label) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</div>";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.arrow), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.arrow), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"item";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.button), {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  options={hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data}
  if (helper = helpers.note) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.note); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.note) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.programWithDepth(19, program19, data, depth0),data:data}
  if (helper = helpers.bubble) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.bubble); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.bubble) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.programWithDepth(19, program19, data, depth0),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data}
  if (helper = helpers.button) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.button); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.button) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.arrow), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.arrow), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "";
  buffer += "a";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  buffer += "div";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += " item-icon-"
    + escapeExpression((helper = helpers.get || (depth0 && depth0.get),options={hash:{
    'default': ("left")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.iconPos), options) : helperMissing.call(depth0, "get", (depth0 && depth0.iconPos), options)));
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += " item-button-"
    + escapeExpression((helper = helpers.get || (depth0 && depth0.get),options={hash:{
    'default': ("right")
  },data:data},helper ? helper.call(depth0, ((stack1 = (depth0 && depth0.button)),stack1 == null || stack1 === false ? stack1 : stack1.position), options) : helperMissing.call(depth0, "get", ((stack1 = (depth0 && depth0.button)),stack1 == null || stack1 === false ? stack1 : stack1.position), options)));
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon ion-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"></i>";
  return buffer;
  }

function program15(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

function program17(depth0,data) {
  
  var buffer = "";
  buffer += "<span class=\"item-note\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }

function program19(depth0,data,depth1) {
  
  var buffer = "", stack1;
  buffer += "<span class=\"badge badge-";
  stack1 = helpers['if'].call(depth0, (depth1 && depth1.ui), {hash:{},inverse:self.program(20, program20, data),fn:self.program(15, program15, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</span>";
  return buffer;
  }
function program20(depth0,data) {
  
  
  return "assertive";
  }

function program22(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<button class=\"button";
  options={hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><i class=\"icon ion-";
  if (helper = helpers.icon) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\"></i></button>";
  return buffer;
  }
function program23(depth0,data) {
  
  var buffer = "";
  buffer += " button-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["listitembasic"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "<a class=\"item\">";
  options={hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}
  if (helper = helpers.label) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.label); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.label) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

  stack1 = (helper = helpers.foreach || (depth0 && depth0.foreach),options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.items), options) : helperMissing.call(depth0, "foreach", (depth0 && depth0.items), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
/**
    {
        config: {
            icon: 'arrow-r', // (Optional). Depends on the itemTpl used. Defaults to undefined.
            inset: true, // (Optional). true, false. Defaults to false.
        },
        store = new Roc.data.Store({...}), // (Optional) Store. Defaults to undefined.
        itemTpl: 'basic', // (Optional). 'Your handlebars custom template' | 'basic|button'. Defaults to 'basic'.
        items: [{
            type: 'divider',
            label: 'My first divider'
        }, {
            label: 'My first list item'
        }, {
            label: 'My second list item'
        }, {
            type: 'divider',
            label: 'My second divider'
        }, {
            label: 'Rest of items',
            bubble: '99+',
            icon: 'info'
        }]
    }
 */
Roc.views.List = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _handlebars = Handlebars,
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    /**
    Meilleur moyen pour loader les images:

    SetInterval(50, loadImagesAfterImageLoad);
    On detecte quand le user arrete de scroller. La on
    */

    function _afterrender(self) {

        var id = self.config.id;

        self.loadingEl = document.getElementById(id + '-loader');
        if (self.hasListener('itemselect')) {
            self.clickEvent = new _ns.events.Click({
                autoAttach: true,
                el: self.el,
                scope: (self.listeners.scope ? self.listeners.scope : undefined),
                handler: self.getListener('itemselect')
            });
        }
        if (self.autoload) {
            self.load();
        }
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.List',
        xtype: 'list',

        defaultAutoload: true,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.store = opts.store;
            if (opts.listeners) {
                self.listeners = opts.listeners;
            }
            self.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : self.defaultAutoload);
            if (opts.config) {
                opts.config.isLoading = self.autoload;
            }
            self.emptyTpl = opts.emptyTpl || false;
            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);

            self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = self.engine.getTpl(self.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        getItemFromEvent: function(evt) {
            return _selector.findParentByClass(evt.target, 'list-item');
        },

        reload: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            self.el.innerHTML = '';

            /*while (self.el.firstChild) {
                self.el.removeChild(self.el.firstChild);
            }*/

            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },

        showLoading: function() {

            var self = this;

            self.isLoading = true;
            _selector.removeClass(self.loadingEl, 'list-loader-hide');
        },

        hideLoading: function() {

            var self = this;

            self.isLoading = false;
            _selector.addClass(self.loadingEl, 'list-loader-hide');
        },

        load: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },

        loadItems: function(items) {

            var self = this,
                itemTpl = self.itemTpl,
                config = _ns.utils.applyAuto(self.config, {});

            config.items = items;

            _handlebars.currentParent.push(self);
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            _handlebars.currentParent.pop();

            if (self.abstractRenderer) {
                self.abstractRenderer.fireScope('afterrender', self.renderer);
            }

            self.hideLoading();
        },

        storeLoaded: function(success, store, newRecords) {

            var self = this;

            self.loadItems(newRecords);

            if (!store.totalRecords && self.emptyTpl) {
                _selector.updateHtml(self.el, self.emptyTpl);
            }
        }
    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["list"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " id=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "-loader\"";
  return buffer;
  }

function program3(depth0,data) {
  
  
  return " list-loader-hide";
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += " list-loader-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

  buffer += "<div class=\"list";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "></div><div ";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.id) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.id) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " class=\"list-loader";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (false),
    'defaults': (true)
  },inverse:self.noop,fn:self.program(3, program3, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.isLoading), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.isLoading), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><a class=\"list-loader-text\">Loading...</a></div>\n";
  return buffer;
  });
/**
    {
        config: {
            size: 'medium', // (Optional) 'small', 'medium', 'large'. Defaults to 'medium'.
            icon: 'vcard', // (Optional) See `Roc.Templates`. Defaults to undefined.
            route: '/your/route', // (Optional). Defaults to undefined.
            iconPos: 'left', // (Optional) 'left', 'right', 'bottom', 'top'. Defaults to 'left'.
            inline: true // (Optional) true, false. Defaults to true.
        },
        handler: function(event) { ... } // (Optional). Defaults to `Roc.views.Button.handler`.
    }
    {
        config: {
            position: 'horizontal', // (Optional) 'horizontal', 'vertical'. Defaults to 'horizontal'.
            size: 'large', // (Optional) 'small', 'medium', 'large'. Defaults to 'large'.
            buttons: [{
                text: 'First', // (Optional) String for text button. Defaults to undefined.
                icon: 'vcard', // (Optional) String for button icon. Defaults to undefined.
                iconPos: 'right' // (Optional) 'left', 'right'. Defaults to undefined.
            }, {
                text: 'Second',
                icon: 'list'
            }, {
                text: 'Third',
                icon: 'location'
            }]
        }
*/
Roc.views.Controlgroup = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.eventClick = new _ns.events.Click({
            autoAttach: true,
            scope: self,
            el: self.el,
            handler: self.handler
        });
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.Controlgroup',
        xtype: 'controlgroup',

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            if (opts.handler) {
                self.handler = opts.handler;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        handler: function(event) {

            var self = this,
                route = self.config.route;

            self.fire('select', self, event);
            if (route) {
                _ns.History.navigate(route);
            }
        }
    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["controlgroup"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<div class=\"button-bar";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
Roc.views.Container = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Container',
        xtype: 'container'

    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["container"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, self=this, functionType="function", blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  
  return " hidden";
  }

  buffer += "<div class=\"container";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.hidden) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.hidden); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.hidden) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["content"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  return " has-header";
  }

function program3(depth0,data) {
  
  
  return " has-footer";
  }

function program5(depth0,data) {
  
  
  return " padding";
  }

  buffer += "<div class=\"content";
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hasHeader), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hasHeader), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(3, program3, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.hasFooter), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.hasFooter), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (true)
  },inverse:self.noop,fn:self.program(5, program5, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.padding), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.padding), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
/**
    {
        config: {
            text: 'My button', // (Optional) String for button label. Defaults to undefined.
            size: 'medium', // (Optional) 'small', 'medium', 'large'. Defaults to 'medium'.
            icon: 'vcard', // (Optional) See `Roc.Templates`. Defaults to undefined.
            route: '/your/route', // (Optional). Defaults to undefined.
            iconPos: 'left', // (Optional) 'left', 'right', 'bottom', 'top'. Defaults to 'left'.
            inline: true, // (Optional) true, false. Defaults to true.
            roundCorners: true // (Optional) true, false. Defaults to true.
        },
        handler: function(event) { ... } // (Optional). Defaults to `Roc.views.Button.handler`.
    }
*/
Roc.views.Button = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.eventClick = new _ns.events.Click({
            autoAttach: true,
            el: self.el,
            scope: self,
            handler: self.handler
        });
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: 'Roc.views.Button',
        xtype: 'button',

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            if (opts.handler) {
                self.handler = (opts.scope ?
                    opts.handler.bind(opts.scope) : opts.handler);
            }

            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        handler: function(evt) {

            var self = this,
                route = self.config.route;

            self.fire('click', self, evt);

            if (route) {
                _ns.History.navigate(route);
            }
        },

        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        }
    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["button"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " button-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, helper;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.iconPos), {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ion-";
  if (helper = helpers.icon) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += " icon-";
  if (helper = helpers.iconPos) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.iconPos); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1);
  return buffer;
  }

function program6(depth0,data) {
  
  
  return " icon";
  }

function program8(depth0,data) {
  
  
  return " active";
  }

function program10(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

  buffer += "<button class=\"button";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.icon), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(8, program8, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.active), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.active), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data}
  if (helper = helpers.text) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.text); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.text) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</button>\n";
  return buffer;
  });
Roc.views.Page = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Page',
        xtype: 'page'

    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["page"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, self=this;


  buffer += "<div class=\"view";
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</div>\n";
  return buffer;
  });
Roc.views.Header = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: 'Roc.views.Header',
        xtype: 'header'

    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["header"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " bar-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "";
  buffer += "<h1 class=\"title\">"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "</h1>";
  return buffer;
  }

  buffer += "<header class=\"bar bar-header";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}
  if (helper = helpers.title) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.title); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.title) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.items, 'items', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</header>\n";
  return buffer;
  });
/**
    {
        config: {
            size: 'medium', // (Optional) 'small', 'medium', 'large'. Defaults to 'medium'.
            icon: 'vcard', // (Optional) See `Roc.Templates`. Defaults to undefined.
            route: '/your/route', // (Optional). Defaults to undefined.
        },
        handler: function(event) { ... } // (Optional). Defaults to `Roc.views.Button.handler`.
    }
*/
Roc.views.Tabbutton = (function() {

    'use strict';

    // private
    var _ns = Roc,
        _parent = _ns.views.Button;

    // public
    return _parent.subclass({

        className: 'Roc.views.Tabbutton',
        xtype: 'tabbutton',

        handler: _parent.prototype.handler,

        activate: function() {
            this.active = true;
            this.fire('activate', this);
        },

        deactivate: function() {
            this.active = false;
            this.fire('deactivate', this);
        }

    });
})();

this["Handlebars"] = this["Handlebars"] || {};
this["Handlebars"]["templates"] = this["Handlebars"]["templates"] || {};

this["Handlebars"]["templates"]["tabbutton"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); partials = this.merge(partials, Handlebars.partials); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, blockHelperMissing=helpers.blockHelperMissing, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += " "
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  return buffer;
  }

function program3(depth0,data) {
  
  
  return " active";
  }

function program5(depth0,data) {
  
  var buffer = "";
  buffer += " href=\"#"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "";
  buffer += "<i class=\"icon ion-"
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\"></i>";
  return buffer;
  }

function program9(depth0,data) {
  
  
  return escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0));
  }

  buffer += "<a class=\"tab-item";
  options={hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}
  if (helper = helpers.ui) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.ui); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.ui) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = (helper = helpers.if_eq || (depth0 && depth0.if_eq),options={hash:{
    'compare': (true),
    'default': (false)
  },inverse:self.noop,fn:self.program(3, program3, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.active), options) : helperMissing.call(depth0, "if_eq", (depth0 && depth0.active), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.cssClass, 'cssClass', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"";
  options={hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}
  if (helper = helpers.route) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.route); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.route) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.style, 'style', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = self.invokePartial(partials.id, 'id', depth0, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options={hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}
  if (helper = helpers.icon) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.icon); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.icon) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  options={hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data}
  if (helper = helpers.text) { stack1 = helper.call(depth0, options); }
  else { helper = (depth0 && depth0.text); stack1 = typeof helper === functionType ? helper.call(depth0, options) : helper; }
  if (!helpers.text) { stack1 = blockHelperMissing.call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data}); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a>\n";
  return buffer;
  });
Roc.engines.ionic = (function() {

    'use strict';

    /* jshint -W030 */
    var _ns = Roc,
        _views = _ns.views,
        _event = _ns.events,
        _parent = _ns,
        _selector = _ns.Selector,
        _templates = _ns.Templates,
        _handlebars = Handlebars,
        _tpls = _handlebars.templates,
        _events = {};
    /* jshint +W030 */

    function findParent(el, callback) {
        if (callback(el) === false && el.parentNode) {
            return findParent(el.parentNode, callback);
        }
    }

    function activateButton(self, event) {

        var activeEl = self.el.querySelector('.active');

        if (activeEl) {
            _selector.removeClass(activeEl, 'active');
        }

        findParent(event.target, function(el) {
            if (el.tagName === 'BUTTON') {
                _selector.addClass(el, 'active');
                return true;
            }
            return false;
        });
    }

    function activateTab(self, event) {

        var activeEl = self.el.querySelector('.active');

        if (activeEl) {
            _selector.removeClass(activeEl, 'active');
        }

        findParent(event.target, function(el) {
            if (_selector.hasClass(el, 'tab-item')) {
                _selector.addClass(el, 'active');
                return true;
            }
            return false;
        });
    }

    var activeListItem = function(event) {

        var activeEl = this.el.querySelector('.active');

        if (activeEl) {
            _selector.removeClass(activeEl, 'active');
        }

        findParent(event.target, function(el) {
            if (_selector.hasClass(el, 'item')) {
                _selector.addClass(el, 'active');
                var sid = setTimeout(function() {
                    _selector.removeClass(el, 'active');
                    clearTimeout(sid);
                }, 500);
                return true;
            }
            return false;
        });

    };

    _events.list = {
        afterrender: function(self) {
            self.activeItem = new _event.Click({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: activeListItem
            });
        }
    };

    _events.controlgroup = {
        select: activateButton
    };

    _events.tabs = {
        select: activateTab
    };

    return _parent.subclass({

        xtype: 'ionic',
        className: 'Roc.engines.ionic',
        version: '0.9.14',

        constructor: function() {},

        getTpl: function(name) {
            return (_tpls[name] ||
                console.error(this.className + ' engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

(function() {

    var _ns = Roc,
        $ = _ns.Selector,
        scroll;

    function onafterrender(self) {
        scroll = new _ns.physics.Scroll($.get('#contentview'), {
            scrollable: 'y'
            // zoomable: true,
            // minZoom: 0.5,
            // maxZoom: 1.5
        });
    }

    var items = [{
        xtype: 'button',
        config: {
            text: 'ui positive and block',
            ui: ['positive', 'block']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'ui energized and full',
            ui: ['energized', 'full']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'ui balanced and outline',
            ui: ['balanced', 'outline']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'ui stable and clear',
            ui: ['stable', 'clear']
        }
    }, {
        xtype: 'button',
        config: {
            text: 'Settings',
            icon: 'gear-a',
            iconPos: 'left'
        }
    }, '<br><br>', {
        xtype: 'controlgroup',
        items: [{
            xtype: 'button',
            config: {
                text: '1'
            }
        }, {
            xtype: 'button',
            config: {
                active: true,
                text: '2'
            }
        }, {
            xtype: 'button',
            config: {
                text: '3'
            }
        }]
    }, '<br><br>', {
        xtype: 'list',
        items: [{
            label: 'One'
        }, {
            label: 'Two'
        }, {
            label: 'Three'
        }]
    }, {
        xtype: 'list',
        itemTpl: 'full',
        items: [{
            label: 'Zero',
            type: 'divider'
        }, {
            label: 'One',
            icon: 'gear-a',
            iconPos: 'right'
        }, {
            label: 'Two',
            type: 'divider'
        }, {
            label: 'Three',
            bubble: '1'
        }, {
            label: 'Four',
            icon: 'chatbubble-working'
        }, {
            label: 'Five',
            note: 'This is a note'
        }, {
            label: 'Six',
            arrow: false,
            button: {
                position: 'left',
                ui: 'positive',
                icon: 'ios7-telephone',
                text: 'Salut'
            }
        }]
    }, {
        xtype: 'abstract',
        xtpl: 'container',
        config: {
            id: 'demo-bubbles'
        },
        items: [{
            xtype: 'abstract',
            xtpl: 'bubble',
            config: {
                ui: 'positive',
                label: '0'
            }
        }, {
            xtype: 'abstract',
            xtpl: 'bubble',
            config: {
                ui: 'assertive',
                label: '1'
            }
        }, {
            xtype: 'abstract',
            xtpl: 'bubble',
            config: {
                ui: 'dark',
                label: '2'
            }
        }]
    }, '<br><br>', {
        xtype: 'list',
        itemTpl: 'avatar',
        items: [{
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }]
    }, '<br><br>', {
        xtype: 'list',
        itemTpl: 'thumbnail',
        items: [{
            imagePosition: 'left',
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            imagePosition: 'right',
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }, {
            image: 'images/mcfly.jpg',
            title: 'Mcfly',
            desc: 'This is the coolest guy in the world !'
        }]
    }];

    var app = new _ns.App({
        engine: 'ionic',
        onready: function() {
            var page = new _ns.views.Page({
                listeners: {
                    afterrender: onafterrender
                },
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'dark'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            icon: 'navicon'
                        }
                    }, '<h1 class="title">DEMO</h1>']
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    config: {
                        hasHeader: true,
                        hasFooter: true,
                        padding: true,
                        id: 'contentview'
                    },
                    items: items
                    // items: {
                    //     xtype: 'abstract',
                    //     xtpl: 'container',
                    //     config: {
                    //         id: 'contentview'
                    //     },
                    //     items: items
                    // }
                }, {
                    xtype: 'tabs',
                    config: {
                        ui: 'icon-top'
                    },
                    items: [{
                        xtype: 'tabbutton',
                        config: {
                            active: true,
                            text: 'Home',
                            icon: 'home'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Favorites',
                            icon: 'star'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Settings',
                            icon: 'gear-a'
                        }
                    }]
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());

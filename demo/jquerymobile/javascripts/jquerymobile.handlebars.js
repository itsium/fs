(function() {

    var app = new {{framework.barename}}.App({
        engine: 'jquerymobile',
        onready: function() {

            var formItems = [{
                xtype: 'textfield',
                config: {
                    type: 'search',
                    placeHolder: 'Search !'
                }
            }, {
                xtype: 'textfield',
                config: {
                    placeHolder: 'Username'
                }
            }, {
                xtype: 'textarea'
            }, {
                xtype: 'button',
                config: {
                    text: 'Submit',
                    bubble: 'bubble'
                }
            }, {
                xtype: 'controlgroup',
                config: {
                    buttons: [{
                        text: 'One'
                    }, {
                        text: 'Two',
                        active: true
                    }, {
                        text: 'Three'
                    }]
                }
            }, {
                xtype: 'switch',
                config: {
                    value: 'on'
                }
            }, {
                xtype: 'switch'
            }];

            var listItems = [{
                xtype: 'list',
                config: {
                    inset: true
                },
                items: [{
                    label: 'One'
                }, {
                    label: 'Two'
                }, {
                    label: 'Three'
                }, {
                    label: 'Four'
                }, {
                    label: 'Five'
                }]
            }, {
                xtype: 'list',
                itemTpl: 'button',
                config: {
                    style: 'margin: 10px -10px;'
                },
                items: [{
                    label: 'One',
                    bubble: 'bubble !'
                }, {
                    label: 'Two',
                    type: 'divider'
                }, {
                    label: 'Three',
                    icon: 'arrow'
                }, {
                    label: 'Four',
                    icon: 'arrow',
                    bubble: '3'
                }, {
                    label: 'Five',
                    items: [{
                        xtype: 'button',
                        config: {
                            ui: 'positive',
                            text: 'Add'
                        }
                    }]
                }, {
                    label: 'Six',
                    items: [{
                        xtype: 'flipswitch',
                        ref: 'testouz'
                    }]
                }]
            }];

            var tabs = {
                xtype: 'footer',
                config: {
                    position: 'fixed'
                },
                items: {
                    xtype: 'tabs',
                    items: [{
                        xtype: 'tabbutton',
                        config: {
                            active: true,
                            text: 'Button 1',
                            route: '1'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Button 2',
                            route: '2'
                        }
                    }, {
                        xtype: 'tabbutton',
                        config: {
                            text: 'Button 3',
                            route: '3'
                        }
                    }]
                }
            };

            var items = formItems.concat(listItems);
            items.push(tabs);

            var page = new {{framework.barename}}.views.Page({
                config: {
                    ui: 'c'
                },
                items: [{
                    xtype: 'header',
                    config: {
                        title: 'DEMO'
                    }
                }, {
                    xtype: 'toolbar',
                    config: {
                        cssClass: 'bar-header-secondary'
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            text: 'Button block'
                        }
                    }]
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    items: items
                }]
            });

            page.compile();
            page.render('body');
        }
    });

}());

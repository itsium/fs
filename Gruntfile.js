module.exports = function(grunt) {

	var timer = require('grunt-timer');

	timer.init(grunt);

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadTasks('cli/tasks');

};

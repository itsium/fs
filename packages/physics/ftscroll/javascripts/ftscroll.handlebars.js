{{classname}} = (function() {

    'use strict';

    // private
    var _api = FTScroller,
        _ns = {{ns}},
        _parent = _ns.Event,
        $ = _ns.Selector;
        // ua = navigator.userAgent.toLowerCase(),
        // isAndroid = (ua.indexOf('android') > -1);

    // public
    return _parent.subclass({

        className: '{{classname}}',
        xtype: '{{xtype}}',

        /**
         * !TODO why not pass object instead of element
         * and listen update remove afterrender events ?
         */
        constructor: function(options) {

            var element,
                cmp,
                self = this;

            self.options = options || {};

            self.el = element = options.el;
            self.cmp = cmp = options.cmp;
            // self.selector @TODO si pas de el alors on afterrender et on
            // self.cmp.querySelector(self.selector)

            // if (typeof options.parentClass === 'string') {
            //     $.addClass(element.parentNode, options.parentClass);
            // } else if (options.parentClass !== false) {
            //     $.addClass(element.parentNode, 'scroll-content');
            // }
            // $.addClass(element, 'scroll');

            var config = {
                scrollingX: options.scrollable === true || options.scrollable === 'x',
                scrollingY: options.scrollable === true || options.scrollable === 'y',
                scrollbars: !!options.scrollbars,
                updateOnChanges: true
            };

            if (typeof options.beforestart === 'function') {
                config.beforestart = options.beforestart;
            }

            if (typeof options.afterscroll === 'function') {
                config.afterscroll = options.afterscroll;
            }

            // if (options.zoomable === true) {
            //     config.zooming = true;
            //     config.minZoom = parseFloat(options.minZoom);
            //     config.maxZoom = parseFloat(options.maxZoom);
            // }

            self.api = new _api(element, config);
            self.attached = true;

            self.seID = cmp.renderer.on('show', function() {
                self.attach();
            });

            self.heID = cmp.renderer.on('hide', function() {
                self.detach();
            });

            _parent.prototype.constructor.call(self);

        },

        getSizes: function() {

            var api = this.api;

            return {
                width: api.scrollWidth,
                height: api.scrollHeight,
                top: api.scrollTop,
                left: api.scrollLeft
            };

        },

        getPositions: function() {

            var api = this.api;

            return {
                y: api.scrollTop,
                x: api.scrollLeft
            };
        },

        getDimensions: function() {

            var api = this.api;

            return {
                x: api.scrollWidth,
                y: api.scrollHeight
            };

        },

        attach: function() {

            var self = this;

            if (self.attached === false) {
                self.api.toggleEventHandlers(true);
                self.attached = true;
            }
        },

        detach: function() {

            var self = this;

            if (self.attached === true) {
                self.api.toggleEventHandlers(false);
                self.attached = false;
            }
        },

        resize: function() {
            // No need to resize with updateOnChanges true by default
        }

    });
})();

/**
 * @class {{ns}}.physics.Scroll
 * @extends {{ns}}.Event
 * @xtype {{xtype}}
 *
 * Javascript scrolling engine.
 *
 * ### Sample usage:
 *
    afterrender: function(self) {

        self.events.add('scroller', {
            xtype: 'physics.scroll',
            el: self.content.el,
            cmp: self,
            scrollable: 'y'
        });

        self.events.get('scroller').resize();

    }
 *
 */
{{classname}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _win = window,
        _doc = document,
        isTouch = ('ontouchstart' in _win),
        isAndroid = (_win.navigator.userAgent.indexOf('Android') >= 0),
        _parent = _ns.Event,
        $ = _ns.Selector;

    var docStyle = _doc.documentElement.style;

    var engine;

    if (_win.opera &&
        Object.prototype.toString.call(opera) === '[object Opera]') {
        engine = 'presto';
    } else if ('MozAppearance' in docStyle) {
        engine = 'gecko';
    } else if ('WebkitAppearance' in docStyle) {
        engine = 'webkit';
    } else if (typeof navigator.cpuClass === 'string') {
        engine = 'trident';
    }

    var vendorPrefix = {
        trident: 'ms',
        gecko: 'Moz',
        webkit: 'Webkit',
        presto: 'O'
    }[engine];

    var vendorStylePropertyPrefix = {
        trident: 'ms',
        gecko: 'Moz',
        webkit: 'webkit',
        presto: 'O'
    }[engine];

    var helperElem = _doc.createElement("div");
    var undef;

    var perspectiveProperty = vendorPrefix + "Perspective";
    var transformProperty = vendorPrefix + "Transform";

    function render(self) {

        if (helperElem.style[perspectiveProperty] !== undef) {

            return function(left, top, zoom) {
                if (self.callback && self.callback(left, top, zoom) === false) return false;
                self.content.style[transformProperty] = 'translate3d(' + (-left) + 'px,' + (-top) + 'px,0) scale(' + zoom + ')';
            };

        } else if (helperElem.style[transformProperty] !== undef) {

            return function(left, top, zoom) {
                if (self.callback && self.callback(left, top, zoom) === false) return false;
                self.content.style[transformProperty] = 'translate(' + (-left) + 'px,' + (-top) + 'px) scale(' + zoom + ')';
            };

        } else {

            return function(left, top, zoom) {
                if (self.callback && self.callback(left, top, zoom) === false) return false;
                self.content.style.marginLeft = left ? (-left/zoom) + 'px' : '';
                self.content.style.marginTop = top ? (-top/zoom) + 'px' : '';
                self.content.style.zoom = zoom || '';
            };

        }
    }

    function init_events(self) {

        if (!self.events) {

            var is_keyboard = false;
            var is_landscape = false;
            var initial_screen_size = window.innerHeight;
            var inputs = ['INPUT', 'TEXTAREA', 'SELECT'];
            var android = (window.navigator.userAgent.indexOf('Android') >= 0);

            self.events = {};

            if (isTouch) {

                var isFocusable = function(target) {
                    if (android && inputs.indexOf(target.tagName) !== -1) {
                        return true;
                    }
                    return false;
                };

                // fix bug for android when focus input we must scroll
                // to focused input
                if (android === true) {

                    var mustFocus = false;

                    self.events.click = function(evt) {

                        var target = evt.target;

                        if (['INPUT', 'TEXTAREA'].indexOf(target.tagName) !== -1) {
                            mustFocus = target;
                        } else {
                            mustFocus = false;
                        }

                    };

                    self.events.resize = function() {

                        self.resize();

                        if (mustFocus !== false) {

                            var sizes = mustFocus.getBoundingClientRect(),
                                pos = self.getPositions(),
                                offsetTop = (self.options.offsetTop ?
                                    self.options.offsetTop : 0),
                                total = (sizes.top - sizes.height - offsetTop + pos.y);

                            self.scrollTo(0, total, true);
                            mustFocus = false;
                        }

                    };

                } else {
                    self.events.focusin = function() {
                        self.disabled = true;
                    };
                    self.events.focusout = function() {
                        self.disabled = false;
                    };
                    self.events.resize = function() {
                        self.resize();
                    };
                }

                self.events.touchstart = function(e) {

                    if (self.disabled === true) {
                        return;
                    } else if (self.options.beforestart &&
                        self.options.beforestart(e, e.touches[0].target) === false) {
                        return;
                    }

                    // android inputs twice focus bug resolve
                    if (isFocusable(e.target)) {
                        self.__hasStarted = false;
                        return;
                    }

                    self.__hasStarted = true;

                    self.api.doTouchStart(e.touches, e.timeStamp);
                    e.preventDefault();

                };

                self.events.touchmove = function(e) {

                    if (!self.__hasStarted && isFocusable(e.target)) {
                        self.__hasStarted = true;
                        self.api.doTouchStart(e.touches, e.timeStamp);
                        e.preventDefault();
                        return;
                    } else if (!self.__hasStarted) return;

                    self.api.doTouchMove(e.touches, e.timeStamp, e.scale);
                };

                self.events.touchend = function(e) {
                    if (!self.__hasStarted && isFocusable(e.target)) {
                        return;
                    }
                    self.__hasStarted = false;
                    self.api.doTouchEnd(e.timeStamp);
                };

            } else {

                var mousedown = false;

                self.events.mousedown = function(e) {

                    if (e.target.tagName.match(/input|textarea|select/i)) {
                        return;
                    }

                    self.api.doTouchStart([{
                        pageX: e.pageX,
                        pageY: e.pageY
                    }], e.timeStamp);

                    mousedown = true;
                    e.preventDefault();

                };

                self.events.mousemove = function(e) {

                    if (!mousedown) {
                        return;
                    }

                    self.api.doTouchMove([{
                        pageX: e.pageX,
                        pageY: e.pageY
                    }], e.timeStamp);

                    mousedown = true;

                };

                self.events.mouseup = function(e) {

                    if (!mousedown) {
                        return;
                    }

                    self.api.doTouchEnd(e.timeStamp);

                    mousedown = false;

                };

                self.events.mousewheel = function(e) {
                    if (self.options.zooming) {
                        self.api.doMouseZoom(e.wheelDelta, e.timeStamp, e.pageX, e.pageY);
                        e.preventDefault();
                    }
                };
            }

        }
    }

    function auto_resize(self) {

        self.__autoResize = function() {
            self.resize();
        };

        self.__autoResizeAttached = false;

        // Try and reuse the old, disconnected observer instance if available
        // Otherwise, check for support before proceeding
        if (!self.mutationObserver) {
            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window[vendorStylePropertyPrefix + 'MutationObserver'];
            if (MutationObserver) {
                self.mutationObserver = new MutationObserver(self.__autoResize);
            } else {
                self.__autoResizeDOM = function (e) {
                    // Ignore changes to nested FT Scrollers - even updating a transform style
                    // can trigger a DOMSubtreeModified in IE, causing nested scrollers to always
                    // favour the deepest scroller as parent scrollers 'resize'/end scrolling.
                    if (e && (e.srcElement === self.container)) {
                        return;
                    }

                    self.resize();
                };
            }
        }

    }

    function auto_resize_attach(self) {

        if (self.__autoResizeAttached !== false) {
            return false;
        }

        self.__autoResizeAttached = true;

        if (self.mutationObserver) {
            self.mutationObserver.observe(self.container, {
                childList: true,
                characterData: true,
                subtree: true
            });
        } else {
            self.container.addEventListener('DOMSubtreeModified', self.__autoResizeDOM, true);
        }
        self.container.addEventListener('load', self.__autoResize, true);
    }

    function auto_resize_detach(self) {

        if (self.__autoResizeAttached !== true) {
            return false;
        }

        self.__autoResizeAttached = false;

        if (self.mutationObserver) {
            self.mutationObserver.disconnect();
        } else {
            self.container.removeEventListener('DOMSubtreeModified', self.__autoResizeDOM, true);
        }
        self.container.removeEventListener('load', self.__autoResize, true);
    }

    // public
    return _parent.subclass({

        className: '{{classname}}',
        xtype: '{{xtype}}',

        /**
         * @cfg {Object} [api={}] Passed to low level API for advanced custom configuration.
         */

        /**
         * @cfg {Mixed} [parentClass='scroll-content'] Container CSS class string or `false` to disable it.
         */

        /**
         * @cfg {Function} [callback=undefined] Scroll callback. Called each time scrolling is happening.
         */

        /**
         * @cfg {Mixed} scrollable True to activate x and y scrolling else 'x' or 'y' strings only. Defaults to undefined.
         */

        /**
         * @cfg {Boolean} [pullToRefresh=false] True to activate pull to refresh feature.
         */

        /**
         * @cfg {HTMLElement} el Given HTML element to activate scroll on.
         */

        /**
         * @cfg {HTMLElement} [container=el.parentNode] Given HTML element container.
         */

        /**
         * @cfg {Object} cmp Component (view) instance.
         */

        /**
         * @cfg {Boolean} [zoomable=false] Defines if scroll area is zoomable or not.
         */

        /**
         * @cfg {Number} [minZoom=undefined] Defines minimum zoom float. (0 to 100+). `zoomable` must be `true`.
         */

        /**
         * @cfg {Number} [maxZoom=undefined] Defines maximum zoom float. (0 to 100+). `zoomable` must be `true`.
         */

        /**
         * @cfg {Function} [beforestart=undefined] Before start callback.
         * You can return false to avoid scrolling continue.
         *
         *      function(event, target) { event.preventDefault(); return false; }
         */

        /**
         * @cfg {Function} [afterscroll=undefined] After scroll callback.
         */

        /**
         * @cfg {Function} [onrefresh=undefined] On refresh callback. `pullToRefresh` must be `true`.
         */

        /**
         * @cfg {Number} [offsetHeight=0] Add an offset height for container height.
         */

        /**
         * @cfg {Number} [refreshHeight=50] Set refresh height area. `pullToRefresh` must be `true`.
         */

        constructor: function(options) {

            var element,
                cmp,
                self = this;

            /**
             * @property {Object} options Scroller options given from constructor.
             */
            self.options = options || {};

            /**
             * @property {Boolean} attached Is scroller attached or not.
             */
            self.attached = false;

            /**
             * @property {HTMLElement} content Scroller content.
             */
            self.content = element = options.el;

            /**
             * @property {HTMLElement} container Scroller container. Defaults to content parent node.
             */
            self.container = (options.container ? options.container : element.parentNode);

            /**
             * @property {Object} cmp {{ns}} component (view) instance.
             */
            self.cmp = cmp = options.cmp;
            // self.selector @TODO si pas de el alors on afterrender et on
            // self.cmp.querySelector(self.selector)

            if (typeof options.parentClass === 'string') {
                $.addClass(self.container, options.parentClass);
            } else if (options.parentClass !== false) {
                $.addClass(self.container, 'scroll-content');
            }
            $.addClass(element, 'scroll');

            if (options.callback) {
                self.callback = options.callback;
            }

            var config = (options.api ? options.api : {});

            config.scrollingX = (options.scrollable === true ||
                options.scrollable === 'x');
            config.scrollingY = (options.scrollable === true ||
                options.scrollable === 'y');

            if (options.zoomable === true) {
                config.zooming = true;
                config.minZoom = parseFloat(options.minZoom);
                config.maxZoom = parseFloat(options.maxZoom);
            }

            if (options.paging === true) {
                config.paging = true;
            }

            if (options.snapping === true) {
                config.snapping = true;
            }

            if (typeof options.beforestart === 'function') {
                config.beforestart = options.beforestart;
            }

            if (typeof options.afterscroll === 'function') {
                config.scrollingComplete = options.afterscroll;
            }

            // self.api = new EasyScroller(element, config);
            // self.engine = self.api.scroller;
            /**
             * @property {Scroller} api Scroller API instance. You should avoid to use it.
             * @private
             */
            self.api = new Scroller(render(self), config);

            if (options.pullToRefresh === true) {
                self.refreshHeight = options.refreshHeight || 50;
                self.content.insertAdjacentHTML('afterbegin',
                    '<div class="refresh"><i class="ion ion-ios7-arrow-thin-down"></i><span></span></div>');//<span>' + t('Pull to Refresh') + '</span></div>');
                self.refreshEl = self.content.querySelector('.refresh');
                self.refreshIconEl = self.refreshEl.querySelector('.ion');
                //self.refreshTextEl = self.refreshEl.querySelector('span');
                self.api.activatePullToRefresh(self.refreshHeight, function() {
                    $.addClass(self.refreshEl, 'active');
                }, function() {
                    $.removeClass(self.refreshEl, 'active');
                }, function() {
                    $.removeClass(self.refreshIconEl, 'ion-ios7-arrow-thin-down');
                    $.addClass(self.refreshIconEl, 'ion-loading-d');
                    $.addClass(self.refreshEl, 'running');
                    if (typeof options.onrefresh === 'function') {
                        options.onrefresh(function() {
                            $.removeClass(self.refreshEl, 'running');
                            $.removeClass(self.refreshIconEl, 'ion-loading-d');
                            $.addClass(self.refreshIconEl, 'ion-ios7-arrow-thin-down');
                            var rect = self.container.getBoundingClientRect();
                            self.api.setPosition(rect.left + self.container.clientLeft,
                                rect.top + self.container.clientTop - self.refreshHeight);
                            self.api.finishPullToRefresh();
                        });
                    }
                });
            }

            _parent.prototype.constructor.call(self, options);

            self.setupListeners(['apiready']);

            self.fire('apiready', self, self.api);

            self.seID = cmp.renderer.on('show', self.attach, self);
            self.heID = cmp.renderer.on('hide', self.detach, self);

            init_events(self);

            self.content.style[vendorPrefix + 'TransformOrigin'] = 'left top';

            if (options.autoResize === true) {
                auto_resize(self);
            }

            self.attach();

        },

        /**
         * Get width, height, top and left from scrolling area.
         * @return {Object} `{ width: 100, height: 100, top: 10, left: 10 }`
         */
        getSizes: function() {

            var api = this.api;

            return ({
                width: api.__maxScrollLeft,
                height: api.__maxScrollTop,
                top: api.__scrollTop,
                left: api.__scrollLeft
            });

        },

        /**
         * Get current scrolling positions.
         * @return {Object} `{ x: 10, y: 0 }`
         */
        getPositions: function() {

            var api = this.api;

            return ({
                y: api.__scrollTop,
                x: api.__scrollLeft
            });
        },

        /**
         * Get scrolling dimensions.
         * @return {Object} `{ x: 100, y: 400 }`
         */
        getDimensions: function() {

            var api = this.api;

            return {
                x: api.__maxScrollTop,
                y: api.__maxScrollLeft
            };

        },

        /**
         * Scroll to given positions with given options.
         * @param  {Number} left    Left position.
         * @param  {Number} top     Top position.
         * @param  {Boolean} [animate] True to animate scrolling. Defaults to false.
         * @param  {Number} [zoom]    Float zoom. Defaults to 1.
         */
        scrollTo: function(left, top, animate, zoom) {
            this.api.scrollTo(left, top, animate, zoom);
        },

        /**
         * Attach scroller listeners.
         * @param  {Object} [self] Current instance.
         */
        attach: function(self) {

            self = self || this;

            var container = self.container,
                events = self.events;

            if (self.attached !== false) {
                return false;
            }

            //console.log('scroll ++ ATtach from ', self.cmp.el.id, new Date());

            self.attached = true;

            if (self.options.autoResize === true) {
                auto_resize_attach(self);
            }

            if (isTouch) {

                if (isAndroid) {
                    container.addEventListener('click', events.click, false);
                } else {
                    _win.addEventListener('focusin', events.focusin, false);
                    _win.addEventListener('focusout', events.focusout, false);
                }

                _win.addEventListener('resize', events.resize, false);

                container.addEventListener('touchstart', events.touchstart, false);

                // all of those were document. based
                container.addEventListener('touchmove', events.touchmove, false);
                container.addEventListener('touchend', events.touchend, false);
                container.addEventListener('touchcancel', events.touchend, false);
            } else {
                container.addEventListener('mousedown', events.mousedown, false);
                container.addEventListener('mousewheel', events.mousewheel, false);

                // all of those were document. based
                container.addEventListener('mousemove', events.mousemove, false);
                container.addEventListener('mouseup', events.mouseup, false);
            }
        },

        /**
         * Detach scroller listeners.
         * @param  {Object} [self] Current instance.
         */
        detach: function(self) {

            self = self || this;

            var container = self.container,
                events = self.events;

            if (self.attached !== true) {
                return false;
            }

            //console.log('scroll -- DEtach from ', self.cmp.el.id, new Date());

            self.attached = false;

            if (self.options.autoResize === true) {
                auto_resize_detach(self);
            }

            // reflow handling

            // touch devices bind touch events
            if (isTouch) {

                _win.removeEventListener("resize", events.resize, false);

                if (isAndroid) {
                    container.removeEventListener('click', events.click, false);
                } else {
                    _win.removeEventListener('focusin', events.focusin, false);
                    _win.removeEventListener('focusout', events.focusout, false);
                }

                container.removeEventListener("touchstart", events.touchstart, false);

                // all of those were document. based
                container.removeEventListener("touchmove", events.touchmove, false);
                container.removeEventListener("touchend", events.touchend, false);
                container.removeEventListener("touchcancel", events.touchend, false);

            // non-touch bind mouse events
            } else {

                container.removeEventListener("mousedown", events.mousedown, false);
                container.removeEventListener("mousewheel", events.mousewheel, false);

                // all of those were document. based
                container.removeEventListener("mousemove", events.mousemove, false);
                container.removeEventListener("mouseup", events.mouseup, false);

            }
        },

        /**
         * Force scroller to resize.
         */
        resize: function() {

            var self = this,
                // refresh the position for zooming purposes
                rect = self.container.getBoundingClientRect();

            self.api.setDimensions(self.container.clientWidth,
                self.container.clientHeight,
                self.content.offsetWidth,
                (self.content.offsetHeight + 95 + // TODO bugfix with ionic
                    (self.options.offsetHeight ? self.options.offsetHeight : 0)
                )
            );

            self.api.setPosition(rect.left + self.container.clientLeft, rect.top + self.container.clientTop);
        }

    });
})();

/**
 * Simple CRUD form.
 * @class {{classname initialize=false}}
 * @extends {{ns}}.views.Template
 * @xtype {{xtype}}
 *
 * ### Example
 *
 *      var parent = {{classname initialize=false}};
 *
 *      var my_form = parent.subclass({
 *          proxy: 'jsonp',
 *          listeners: {
 *              aftersubmit: function(form, success, data) {
 *                  if (!success) return alert('Server unreachable');
 *                  else if (!data.success) return alert('Data not correct');
 *              }
 *          },
 *          read_my_field_name: function(value) {
 *              if (value === false) return 'NO';
 *              else return 'YES';
 *          },
 *
 *          constructor: function() {
 *
 *              var self = this;
 *
 *              var form = [{
 *                  xtype: 'textfield',
 *                  config: {
 *                      label: 'Name',
 *                      name: 'name',
 *                      required: true,
 *                      placeHolder: 'Your name'
 *                  }
 *              }, {
 *                  xtype: 'button',
 *                  config: {
 *                      text: 'Submit'
 *                  },
 *                  handler: function() {
 *                      self.submit();
 *                  }
 *              }];
 *
 *              var options = {
 *                  items: form
 *              };
 *
 *              parent.prototype.constructor.call(self, options);
 *          }
 *      });
 */
{{classname}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _request = _ns.Request,
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    var reader = function(self, field_name, field_value) {

        var method_name = 'read_' + field_name;

        if (typeof self[method_name] === 'function') {
            return self[method_name](field_value);
        }
        return field_value;
    };

    var _fieldsToJson = function(self, selector, root) {

        var field,
            result = {},
            fields = $.gets(selector, root),
            i = fields.length;

        while (i--) {
            field = fields[i];
            // if (field.getAttribute('required') !== null &&
            //     !field.value.length) {
            //     return false;
            // } else if (field.validity.valid === false) {
            //     return false;
            // }

            var name = field.getAttribute('name');
            var value = field.value;
            var ignore = false;

            if (field.type === 'checkbox' &&
                field.hasAttribute('value') === false) {
                value = field.hasAttribute('checked');
            } else if (field.type === 'radio' &&
                field.hasAttribute('checked') === false) {
                ignore = true;
            }

            if (ignore === false) {
                result[name] = reader(self, name, value);
            }
        }
        return result;
    };

    var _validateFields = function(selector, root) {

        var field,
            result = true,
            fields = $.gets(selector, root),
            i = fields.length;

        while (i--) {
            field = fields[i];
            if (field.getAttribute('required') !== null &&
                !field.value.length) {
                $.addClass(field.parentNode, 'fielderror');
                result = false;
            } else if (field.validity.valid === false) {
                $.addClass(field.parentNode, 'fielderror');
                result = false;
            } else {
                $.removeClass(field.parentNode, 'fielderror');
            }
        }

        return result;

    };

    var _jsonToFields = function(self, json, fields) {
        if (json) {

            var field, name, i = fields.length;

            while (i--) {
                field = fields[i];
                name = field.getAttribute('name');
                if (typeof json[name] !== 'undefined') {

                    if (field.type === 'checkbox') {

                        var value = reader(self, name, json[name]);

                        if (value === true) {
                            field.setAttribute('checked', 'checked');
                        } else {
                            field.removeAttribute('checked');
                        }
                    } else if (field.type === 'radio') {

                        var value = reader(self, name, json[name]);

                        if (value === field.value) {
                            field.setAttribute('checked', 'checked');
                        } else {
                            field.removeAttribute('checked');
                        }

                    } else {
                        field.value = reader(self, name, json[name]);
                    }

                } else {
                    field.value = '';
                }
            }
        }
    };

    var _replaceByValues = function(str, fields, encode) {

        var i;

        for (i in fields) {

            var value = fields[i];

            if (typeof encode === 'function') {
                value = encode(value);
            }
            str = str.replace('{' + i + '}', value);
        }
        return str;
    };

    // public
    return _parent.subclass({

        className: '{{classname}}',
        xtype: '{{xtype}}',
        hasListeners: ['aftersubmit'],
        listeners: {
            priority: 'VIEWS',
            afterrender: function(self) {
                [].forEach.call(self.el.querySelectorAll('input[name]'), function(el) {
                    self.events.add('submit-on-enter-' + el.getAttribute('name'), {
                        xtype: 'events.keypress',
                        el: el,
                        handler: function(evt) {
                            if (evt.keyCode === 13) {
                                evt.target.blur();
                                self.submit();
                            }
                        }
                    });
                });
            }
        },
        implements: ['events'],

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            /**
             * @cfg {Object} [cmp=this] Component containing fields.
             */
            if (opts.cmp) {
                self.cmp = opts.cmp;
            } else {
                self.cmp = self;
            }

            /**
             * @cfg {String} [url] Submit form URL
             */
            if (opts.url) {
                self.url = opts.url;
            }

            /**
             * @cfg {Object} [urls] Form URLs
             * @cfg {String} [urls.read] Read URL
             * @cfg {String} [urls.update] Update URL
             * @cfg {String} [urls.remove] Remove URL
             */
            if (opts.urls) {
                self.urls = opts.urls;
            }

            /**
             * @cfg {Object} [loader] Form loader component.
             */
            if (opts.loader) {
                self.loader = opts.loader;
            }

            /**
             * @cfg {String} [proxy="jsonp"] Proxy type:
             * <ul>
             *  <li>ajax</li>
             *  <li>jsonp</li>
             *  <li>See {{ns}}.Request for other proxy type.</li>
             * </ul>
             */
            if (opts.proxy) {
                self.proxy = opts.proxy;
            }

            if (typeof self.proxy === 'string') {
                if (!_request[self.proxy]) {
                    console.warn('{{classname}}: Proxy "', self.proxy, '" does not exists.');
                }
                self.proxy = _request[self.proxy];
            }

            if (typeof self.proxy !== 'function') {
                self.proxy = _request.jsonp;
            }

            _parent.prototype.constructor.call(self, opts);

            self.setupListeners([
                'afterupdate',
                'afterremove',
                'aftersubmit',
                'afterread'
            ]);
        },

        /**
         * Check if form fields are valid or not.
         * @return {Boolean} True if all fields valid, else false.
         */
        isValid: function() {
            if (_validateFields('[name]', this.cmp.el) === false) {
                return false;
            }
            return true;
        },

        /**
         * Reset form by removing all values from fields.
         */
        reset: function() {

            var field,
                fields = $.gets('[name]', this.cmp.el),
                i = fields.length;

            while (i--) {
                field = fields[i];

                if (typeof field.value !== 'undefined') {
                    field.value = '';
                }
            }

        },

        /**
         * Read form data from server with given options.
         * @fires afterread
         * @param  {Object} [opts] Options
         * @param {Object} [opts.args] URL arguments dictionnary. Will add those values to URL.
         * @param {Function} [opts.callback] Callback function.
         */
        read: function(opts) {

            var self = this,
                url = self.urls.read;

            if (opts.args) {
                url = _replaceByValues(url, opts.args, encodeURIComponent);
            }

            if (self.loader) {
                self.loader.show();
            }

            self.proxy(url, {
                scope: self,
                callback: function(success, data) {

                    if (self.loader) {
                        self.loader.hide();
                    }

                    if (success && typeof data === 'string') {
                        data = JSON.parse(data);
                    }

                    if (success && data.success) {
                        self.json(data.data);
                    }

                    if (opts.callback) {
                        opts.callback(success, data);
                    }

                    self.fire('afterread', self, success, data);

                }
            });

        },

        /**
         * Submit updated field values to server.
         * @fires afterupdate
         * @param  {Object} [opts] Options
         * @param {Function} [opts.callback] After submit callback
         */
        update: function(opts) {

            var self = this;

            opts = opts || {};

            opts.url = self.urls.update;
            opts.operation = 'update';
            self.submit(opts);

        },

        /**
         * Remove values from server.
         * @fires afterremove
         * @param  {Object} opts Options
         */
        remove: function(opts) {

            var self = this;

            opts = opts || {};

            opts.url = self.urls.remove;
            opts.operation = 'remove';
            self.submit(opts);

        },

        /**
         * Apply given data to fields.
         * @param  {Object} data Dictionnary values
         */
        json: function(data) {

            var self = this;

            if (data) {
                self.data = data;
                return (_jsonToFields(self, data,
                    self.cmp.el.querySelectorAll('[name]')));
            }
            return _fieldsToJson(self, '[name]', self.cmp.el);
        },

        /**
         * Submit data to server if form is valid.
         * @fires aftersubmit
         * @param  {Object} [opts] Options
         * @param {String} [opts.operation="submit"] Operation name.
         * <ul>
         *  <li>submit</li>
         *  <li>remove</li>
         *  <li>update</li>
         *  <li>read</li>
         * </ul>
         * @param {Function} [opts.callback] Finish callback.
         * @param {Object} [opts.args] URL dictionnary arguments.
         * @return {Mixed} False if form is not valid.
         */
        submit: function(opts) {

            opts = opts || {};

            var self = this,
                url = (opts.url ? opts.url : self.url),
                operation = (opts.operation ? opts.operation : 'submit'),
                callback = opts.callback;

            if (self.isValid() === false) {
                if (callback) {
                    callback(false);
                }
                return false;
            }

            var fields = self.json();

            if (opts.args) {
                url = _replaceByValues(url, opts.args, encodeURIComponent);
            }

            if (fields !== false) {
                url = _replaceByValues(url, fields, encodeURIComponent);

                if (self.loader) {
                    self.loader.show();
                }

                self.proxy(url, {
                    scope: self,
                    callback: function(success, data) {

                        if (self.loader) {
                            self.loader.hide();
                        }

                        if (callback) {
                            callback(success, data);
                        }
                        self.fire(('after' + operation), self, success, data);
                    }
                });
            }
        }

    });

})();

(function() {

    var win = window;

    // Custom event polyfill
    if(!win.CustomEvent) {

        var CustomEvent = function(event, params) {

            var evt;

            params = params || {
                bubbles: false,
                cancelable: false,
                detail: undefined
            };

            try {
                evt = document.createEvent("CustomEvent");
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            } catch (error) {
                evt = document.createEvent("Event");
                for (var param in params) {
                    evt[param] = params[param];
                }
                evt.initEvent(event, params.bubbles, params.cancelable);
            }

            return evt;
        };

        CustomEvent.prototype = win.Event.prototype;

        win.CustomEvent = CustomEvent;

    }

}());
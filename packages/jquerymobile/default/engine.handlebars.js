{{framework.barename}}.engines.jquerymobile = (function() {

    'use strict';

    /* jshint -W030 */
    var _handlebars = Handlebars,
        _ns = {{framework.barename}},
        _events = {},
        //_overrides = {},
        _views = _ns.views,
        _event = _ns.events,
        _parent = _ns,
        _selector = _ns.Selector;
    /* jshint +W030 */

    // <@BUGFIX> orientation change header footer position fixed problem
    var _win = window,
        _body = document.body;

    function findParent(el, callback) {
        if (callback(el) === false && el.parentNode) {
            return findParent(el.parentNode, callback);
        }
    }

    _win.addEventListener('orientationchange', function() {

        var sid = setTimeout(function() {
            _win.scrollTo(_body.scrollLeft, _body.scrollTop);
            clearTimeout(sid);
        }, 0);

    }, false);
    // </@BUGFIX>

    // Template helpers
    _handlebars.registerHelper('getIndexLetter', function(index) {
        return String.fromCharCode(97 + (index % 26));
    });

    _handlebars.registerHelper('getLengthLetter', function(array) {
        var length = array.length;

        if (length < 2) {
            return 'a';
        }
        return String.fromCharCode(95 + length);
    });

    // helpers for setUI methods
    function _removeUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            _selector.removeClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    }

    function _addUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            _selector.addClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    }

    function updateMarginTop(self) {

        if (self.el) {
            return;
        }

        var el = self.el,
            height = el.offsetHeight / 2;

        el.style.marginTop = '-' + height + 'px';
    }

    _events.alert = {
        show: function(self) {
            if (self.el) {
                updateMarginTop(self);
            } else {

                var sid = setTimeout(function() {
                    updateMarginTop(self);
                    clearTimeout(sid);
                }, 150);

            }
        }
    };

    function activateTab(self, event) {

        var activeEl = self.el.querySelector('.ui-btn-active');

        if (activeEl) {
            _selector.removeClass(activeEl, 'ui-btn-active');
        }

        findParent(event.target, function(el) {
            if (el.tagName === 'A') {
                _selector.addClass(el, 'ui-btn-active');
                return true;
            }
            return false;
        });
    }

    _events.controlgroup = {
        select: activateTab
    };

    _events.tabs = {
        select: activateTab
    };

    _events.tabbuton = {
        activate: function() {
            // !TODO
        },
        deactivate: function() {
            // !TODO
        }
    };

    _events.collapsible = {
        afterrender: function(self) {
            self.iconEl = _selector.get('.ui-icon', self.el);
            self.iconCollapsed = 'ui-icon-' + (self.config.iconCollapsed || 'plus');
            self.iconExpanded = 'ui-icon-' + (self.config.iconExpanded || 'minus');
        },
        collapse: function(self) {
            _selector.addClass(self.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.addClass(self.contentEl, 'ui-collapsible-content-collapsed');
            _selector.addClass(self.el, 'ui-collapsible-collapsed');
            // icon
            _selector.removeClass(self.iconEl, self.iconExpanded);
            _selector.addClass(self.iconEl, self.iconCollapsed);
        },
        expand: function(self) {
            _selector.removeClass(self.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.removeClass(self.contentEl, 'ui-collapsible-content-collapsed');
            _selector.removeClass(self.el, 'ui-collapsible-collapsed');
            // icon
            _selector.removeClass(self.iconEl, self.iconCollapsed);
            _selector.addClass(self.iconEl, self.iconExpanded);
        }
    };

    _handlebars.registerHelper('getLabelFromValue', function(value, values) {

        var item,
            i = values.length;

        while (i--) {
            item = values[i];
            if (item.value === value) {
                return item.label;
            }
        }
        return value;
    });

    _events.select = {
        change: function(self, item) {

            var textEl = _selector.get('.ui-btn-text', self.uiEl);

            if (textEl) {
                textEl.innerHTML = item.label;
            }
        },
        loading: function(cmp, isLoading) {

            var textEl = _selector.get('.ui-btn-text', cmp.uiEl);

            if (isLoading) {
                cmp.setDisabled(true);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = 'Loading';
                }
            } else {
                cmp.setDisabled(false);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = cmp.config.emptyText;
                }
            }
        },
        disabled: function(cmp, isDisabled) {

            var btnEl = _selector.get('.ui-btn', cmp.uiEl);

            if (isDisabled) {
                _selector.addClass(btnEl, 'ui-disabled');
            } else {
                _selector.removeClass(btnEl, 'ui-disabled');
            }
        }
    };

    _events.button = {
        disabled: function(cmp, isDisabled) {
            if (isDisabled) {
                _selector.addClass(cmp.el, 'ui-disabled');
            } else {
                _selector.removeClass(cmp.el, 'ui-disabled');
            }
        }
    };

    var _setUIBtn = function(ui) {
        _removeUI('ui-btn-up-{{ui}}', this.config.ui, this.el);
        _addUI('ui-btn-up-{{ui}}', ui, this.el);

        this.config.ui = ui;
    };

    if (_views.Button) {
        _views.Button.prototype.setUI = _setUIBtn;
    }
    if (_views.TabButton) {
        _views.TabButton.prototype.setUI = _setUIBtn;
    }

    var _setUIBar = function(ui) {
        _removeUI('ui-bar-{{ui}}', this.config.ui, this.el);
        _addUI('ui-bar-{{ui}}', ui, this.el);

        this.config.ui = ui;
    };

    if (_views.Header) {
        _views.Header.prototype.setUI = _setUIBar;
    }
    if (_views.Footer) {
        _views.Footer.prototype.setUI = _setUIBar;
    }
    if (_views.Toolbar) {
        _views.Toolbar.prototype.setUI = _setUIBar;
    }

    _events.radiobutton = {
        check: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-off');
            _selector.addClass(this.iconEl, 'ui-icon-radio-on');
            _selector.removeClass(this.uiEl, 'ui-radio-off');
            _selector.addClass(this.uiEl, 'ui-radio-on');
        },
        uncheck: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-on');
            _selector.addClass(this.iconEl, 'ui-icon-radio-off');
            _selector.removeClass(this.uiEl, 'ui-radio-on');
            _selector.addClass(this.uiEl, 'ui-radio-off');
        }
    };

    _events.panel = {
        afterrender: function() {
            overthrow.set();
        },
        expand: function() {
            var page = _selector.get('.ui-page-active');

            _selector.addClass(page, 'active');
            _selector.addClass(this.el, 'active');
        },
        collapse: function() {
            var page = _selector.get('.ui-page-active');

            _selector.removeClass(page, 'active');
            _selector.removeClass(this.el, 'active');
        }
    };

    _handlebars.registerHelper('isFlipSwitchOptionActive', function(values, index, value, options) {
        value = value || values[0].value;
        var current = values[index].value;

        if (current === value) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    _events.switch = {
        afterrender: function(self) {

            var on = _selector.get('.ui-btn-active', self.uiEl),
                off = _selector.get('.ui-btn-down-c', self.uiEl),
                handle = _selector.get('.ui-slider-handle', self.uiEl);

            self.on('check', function() {
                on.style.width = '100%';
                off.style.width = '0%';
                handle.style.left = '100%';
            }, self, self.priority.VIEWS);
            self.on('uncheck', function() {
                on.style.width = '0%';
                off.style.width = '100%';
                handle.style.left = '0%';
            }, self, self.priority.VIEWS);
        }
    };

    _events.textarea = {
        afterrender: function(self) {
            self.eventFocus = new _event.Focus({
                autoAttach: true,
                el: self.el,
                handler: function() {
                    _selector.addClass(self.el, 'ui-focus');
                }
            });
            self.eventBlur = new _event.Blur({
                autoAttach: true,
                el: self.el,
                handler: function() {
                    _selector.removeClass(self.el, 'ui-focus');
                }
            });
        }
    };

    _events.checkbox = {
        afterrender: function(self) {

            var iconEl = _selector.get('.ui-icon', self.uiEl);

            self.on('check', function() {
                _selector.removeClass(self.uiEl, 'ui-checkbox-off');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-off');
                _selector.addClass(self.uiEl, 'ui-checkbox-on');
                _selector.addClass(iconEl, 'ui-icon-checkbox-on');
            }, self, self.priority.VIEWS);

            self.on('uncheck', function() {
                _selector.removeClass(self.uiEl, 'ui-checkbox-on');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-on');
                _selector.addClass(self.uiEl, 'ui-checkbox-off');
                _selector.addClass(iconEl, 'ui-icon-checkbox-off');
            }, self, self.priority.VIEWS);

        }
    };

    _events.textfield = {
        afterrender: function(self) {

            var parentNode = self.el.parentNode;

            self.eventFocus = new _event.Focus({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.addClass(parentNode, 'ui-focus');
                }
            });
            self.eventBlur = new _event.Blur({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.removeClass(parentNode, 'ui-focus');
                }
            });
            if (self.config.clearBtn) {
                self.clearBtnEl = document.getElementById(self.config.id + '-clearbtn');

                var keyupHandler = function() {
                    if (self.config.clearBtn <= self.el.value.length) {
                        _selector.removeClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    } else {
                        _selector.addClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    }
                };

                self.eventKeyup = new _event.Abstract({
                    eventName: 'keyup',
                    autoAttach: true,
                    el: self.el,
                    globalFwd: window,
                    handler: keyupHandler
                });
                self.eventClick = new _event.Click();
                self.eventClick.attach(self.clearBtnEl, function() {
                    if (_selector.hasClass(self.clearBtnEl, 'ui-disabled')) {
                        return false;
                    }
                    self.el.value = '';
                    keyupHandler.call(self);
                }, self);
            }
        },
        disabled: function(cmp, isDisabled) {

            var s = _selector;

            if (isDisabled) {
                s.addClass(cmp.el, 'ui-disabled');
                if (cmp.clearBtnEl) {
                    s.addClass(cmp.clearBtnEl, 'ui-disabled');
                }
            } else {
                s.removeClass(cmp.el, 'ui-disabled');
                if (cmp.clearBtnEl) {
                    s.removeClass(cmp.clearBtnEl, 'ui-disabled');
                }
            }
        }
    };

    return _parent.subclass({

        xtype: 'jquerymobile',
        className: '{{framework.barename}}.engines.jquerymobile',
        version: '1.3.1',

        constructor: function() {

            var s = _ns.Selector,
                html = s.get('html'),
                body = s.get('body');

            s.addClass(html, 'ui-mobile');
            s.addClass(body, 'ui-mobile-viewport');
        },

        getTpl: function(name) {
            return (_handlebars.templates[name] ||
                console.error(this.className + ' engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

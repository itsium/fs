{{!

// switch

}}{{#label}}<label class="ui-slider"{{> for_id}}>{{.}}</label>{{/label}}{{!
}}<select{{!
    }}{{#name}} name="{{.}}"{{/name}}{{> id}} class="ui-slider-switch">{{!
    }}{{#each options}}{{!
        }}<option value="{{value}}">{{label}}</option>{{!
    }}{{/each}}{{!
}}</select>{{!

}}<div {{#id}} id="{{.}}-ui"{{/id}}{{!
    }}class="ui-slider ui-slider-switch{{!
    }}{{#ui}} ui-btn-down-{{.}}{{/ui}}{{!
    }}{{#if_eq size compare="small" default="medium"}} ui-mini{{/if_eq}}{{!
    }} ui-btn-corner-all">{{!

    }}{{#foreach options}}{{!
        }}<span class="ui-slider-label{{!
            }} ui-slider-label-{{#if_eq $index compare=0}}b{{else}}a{{/if_eq}}{{!
            }}{{#if_eq $index compare=1}}{{!
                }} ui-btn-active{{!
            }}{{else}}{{!
                }} ui-btn-down-c{{!
            }}{{/if_eq}} ui-btn-corner-all"{{! ui-btn-active = blue background
            }} style="width: {{!
            }}{{#isFlipSwitchOptionActive ../options $index ../value}}{{!
                }}100%{{!
            }}{{else}}{{!
                }}0%{{!
            }}{{/isFlipSwitchOptionActive}};">{{!
            }}{{label}}{{!
        }}</span>{{!
    }}{{/foreach}}{{!

    }}<div class="ui-slider-inneroffset">{{!
        }}<a href="javascript:;" class="{{!
            }}ui-slider-handle ui-slider-handle-snapping ui-btn{{!
            }}{{#ui}} ui-btn-up-{{.}}{{/ui}}{{!
            }}ui-shadow ui-btn-corner-all"{{!
            }}{{#id}} id="{{.}}-btn"{{/id}}{{!
            }}style="left: {{!
            }}{{#isFlipSwitchOptionActive options 0 value}}{{!
                }}0%{{!
            }}{{else}}{{!
                }}100%{{!
            }}{{/isFlipSwitchOptionActive}};">{{!
            }}<span class="ui-btn-inner">{{!
                }}<span class="ui-btn-text"></span>{{!
            }}</span>{{!
        }}</a>{{!
    }}</div>{{!
}}</div>

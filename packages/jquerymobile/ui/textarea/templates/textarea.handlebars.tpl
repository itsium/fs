{{!

// textarea

}}{{#label}}{{!
    }}<label class="ui-input-text"{{> for_id}}>{{.}}</label>{{!
}}{{/label}}{{!

}}<textarea class="ui-input-text ui-shadow-inset ui-corner-all {{!
    }}{{#ui}} ui-body-{{.}}{{/ui}}{{!

    }}{{#if_eq disabled compare=true default=false}}{{!
        }} ui-disabled{{!
    }}{{/if_eq}}"{{!

    }}{{#name}} name="{{.}}"{{/name}}{{!

    }}{{#placeHolder}} placeholder="{{.}}"{{/placeHolder}}{{!

    }}{{#if_eq disabled compare=true default=false}}{{!
        }} disabled="disabled"{{!
    }}{{/if_eq}}{{!

    }}{{#if_eq required compare=true default=false}}{{!
        }} required{{!
    }}{{/if_eq}}{{!

    }} cols="{{#if cols}}{{cols}}{{else}}40{{/if}}"{{!

    }} rows="{{#if rows}}{{rows}}{{else}}8{{/if}}"{{!

    }}{{> id}}>{{!

    }}{{#value}}{{.}}{{/value}}{{!

}}</textarea>

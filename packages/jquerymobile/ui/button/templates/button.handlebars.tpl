{{!

// button

}}<a class="ui-btn{{!
    }}{{#ui}} ui-btn-up-{{.}}{{/ui}}{{!
    }}{{#shadow}} ui-shadow{{/shadow}}{{!
    }}{{#hidden}} hidden{{/hidden}}{{!
    }}{{#if_eq disabled compare=true default=false}} ui-disabled{{/if_eq}}{{!
    }}{{#if_eq roundCorners compare=true default=true}} ui-btn-corner-all{{/if_eq}}{{!
    }}{{#if_eq size compare="medium"}} ui-mini{{/if_eq}}{{!
    }}{{#if_eq size compare="small"}} ui-mini ui-btn-icon-notext{{/if_eq}}{{!
    }}{{#if_eq inline compare=true default=true}} ui-btn-inline{{/if_eq}}{{!
    }}{{#if icon}}{{#if iconPos}} ui-btn-icon-{{iconPos}}{{else}} ui-btn-icon-left{{/if}}{{/if}}{{!
    }}{{> cssClass}}"{{!
    }}{{> style}}{{!
    }}{{#target}} target="{{.}}"{{/target}}{{!
    }}{{#href}} href="{{.}}"{{/href}}{{!
    }}{{> id}}>{{!
    }}<span class="ui-btn-inner">{{!
        }}<span class="ui-btn-text">{{#if text}}{{text}}{{else}}&nbsp;{{/if}}</span>{{!
        }}{{#if icon}}<span class="{{!
        }}{{#if iconCssClass}}{{iconCssClass}}{{else}}icon{{/if}}{{!
        }} override-inline-btn-icon{{!
        }}">{{! ui-icon ui-icon-custom

            }}{{#geticon icon}}{{/geticon}}</span>{{!

        }}{{/if}}{{!
    }}</span>{{!
}}</a>

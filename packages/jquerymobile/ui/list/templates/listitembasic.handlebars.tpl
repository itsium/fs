{{!

// listitembasic

}}{{#foreach items}}{{!
    }}<li class="list-item ui-li ui-li-static{{!
        }}{{#if_eq $first compare=true}} ui-first-child{{/if_eq}}{{!
        }}{{#if_eq $last compare=true}} ui-last-child{{/if_eq}}{{!
        }}{{#../ui}} ui-btn-up-{{.}}{{/../ui}}">{{!

        }}{{#label}}{{.}}{{/label}}{{!

    }}</li>{{!
}}{{/foreach}}

{{!

// list

}}<ul class="ui-listview{{!
    }}{{#inset}} ui-listview-inset{{/inset}}{{!
    }}{{#corner}} ui-corner-{{.}}{{/corner}}{{!
    }}{{#shadow}} ui-shadow{{/shadow}}{{!
    }}{{> cssClass}}"{{!
    }}{{> style}}{{!
    }}{{> id}}>{{!
}}</ul>{{!

}}<div {{#id}} id="{{.}}-loader"{{/id}}{{!
    }} class="list-loader{{!
    }}{{#if_eq isLoading compare=false defaults=true}} list-loader-hide{{/if_eq}}{{!
    }}{{#ui}} list-loader-{{.}}{{/ui}}">{{!
    }}<a class="list-loader-text">{{!
        }}Loading...{{!
    }}</a>{{!
}}</div>

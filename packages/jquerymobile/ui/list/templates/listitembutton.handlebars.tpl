{{!

// listitembutton

}}{{#foreach items}}{{!

    }}{{#if_eq type compare="divider"}}{{!

        }}<li class="ui-li ui-li-divider{{!
            }}{{#../../ui}} ui-bar-{{get ../ui default=.}}{{/../../ui}}{{!
            }}{{#bubble}} ui-li-has-count{{/bubble}}{{!
            }}{{#if_eq $first compare=true}} ui-first-child{{/if_eq}}{{!
            }}{{#if_eq $last compare=true}} ui-last-child{{/if_eq}}{{!
            }}">{{!
            }}{{#label}}{{.}}{{/label}}{{!
            }}{{#if bubble}}{{!
                }}<span class="ui-li-count{{!
                    }}{{#../../../ui}} ui-btn-up-{{get ../ui default=.}}{{/../../../ui}}{{!
                    }} ui-btn-corner-all">{{bubble}}</span>{{!
            }}{{/if}}{{!
        }}</li>{{!

    }}{{else}}{{!

        }}<li class="list-item ui-btn{{!
            }}{{#../../ui}} ui-btn-up-{{get ../ui default=.}}{{/../../ui}}{{!
            }} ui-btn-icon-right ui-li-has-arrow ui-li{{!
            }}{{#bubble}} ui-li-has-count{{/bubble}}{{!
            }}{{#if_eq $first compare=true}} ui-first-child{{/if_eq}}{{!
            }}{{#if_eq $last compare=true}} ui-last-child{{/if_eq}}{{!
            }}">{{!
            }}<div class="ui-btn-inner ui-li">{{!
                }}<div class="ui-btn-text">{{!
                    }}<a href="javascript:;" class="ui-link-inherit">{{!
                        }}{{#label}}{{.}}{{/label}}{{!
                        }}{{#if bubble}}{{!
                            }}<span class="ui-li-count{{!
                                }}{{#../../../ui}} ui-btn-up-{{get ../ui default=.}}{{/../../../ui}}{{!
                                }} ui-btn-corner-all">{{bubble}}</span>{{!
                        }}{{/if}}{{!
                    }}</a>{{!
                }}</div>{{!
                }}<span class="ui-icon{{!
                    }}{{#../../icon}} ui-icon-{{get ../icon default=.}}{{/../../icon}}{{!
                    }} ui-icon-shadow">{{!
                    }}&nbsp;{{!
                }}</span>{{!
            }}</div>{{!
        }}</li>{{!

    }}{{/if_eq}}{{!

}}{{/foreach}}

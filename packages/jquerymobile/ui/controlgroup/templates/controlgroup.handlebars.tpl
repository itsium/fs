{{!

// controlgroup

}}<div class="ui-corner-all ui-controlgroup {{!
    }}ui-controlgroup-{{#if position}}{{position}}{{else}}horizontal{{/if}}{{!
    }}{{#if_eq size compare="medium"}} ui-mini{{/if_eq}}{{!
    }}{{#if_eq size compare="small"}} ui-mini{{/if_eq}}{{!
    }}"{{!
    }}{{> id}}>{{!

    }}<div class="ui-controlgroup-controls">{{!
        }}{{#foreach buttons}}{{!

            }}<a class="ui-btn {{!
                }}{{#../ui}}ui-btn-up-{{.}}{{/../ui}}{{!
                }} ui-shadow ui-btn-corner-all{{!
                }}{{#if icon}}{{#if iconPos}} ui-btn-icon-{{iconPos}}{{/if}}{{/if}}{{!
                }}{{#if_eq ../size compare="small"}} ui-btn-icon-notext{{/if_eq}}{{!
                }}{{#if $first}} ui-first-child{{/if}}{{#if $last}} ui-last-child{{/if}}">{{!

                }}<span class="ui-btn-inner">{{!
                    }}{{#text}}<span class="ui-btn-text">{{.}}</span>{{/text}}{{!
                    }}{{#icon}}<span class="ui-icon override-btn-icon icon">{{#geticon .}}{{/geticon}}</span>{{/icon}}{{! ui-icon ui-icon-shadow
                }}</span>{{!

            }}</a>{{!

        }}{{/foreach}}{{!
    }}</div>{{!

}}</div>

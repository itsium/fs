{{!

// tabbutton

}}<a class="ui-btn ui-btn-inline{{!
    }}{{#icon}} ui-btn-icon-{{get ../iconPos default="top" choices="top|bottom|left|right"}}{{/icon}}{{!
    }}{{#if_eq active compare=true}} ui-btn-active{{/if_eq}}{{!
    }}{{#ui}} ui-btn-up-{{.}}{{/ui}}"{{!
    }}{{> style}}{{!
    }}{{> id}}>{{!

    }}<span class="ui-btn-inner">{{!

        }}{{#text}}{{!
            }}<span class="ui-btn-text">{{.}}</span>{{!
        }}{{/text}}{{!

        }}{{#icon}}{{!
            }}<span class="icon override-tab-icon">{{! ui-icon ui-icon-custom
            }}{{#geticon .}}{{/geticon}}</span>{{!
        }}{{/icon}}{{!

    }}</span>{{!

}}</a>

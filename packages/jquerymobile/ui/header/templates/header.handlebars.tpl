{{!

// header

}}<div class="ui-header{{!
    }}{{#ui}} ui-bar-{{.}}{{/ui}}{{!
    }}{{> cssClass}}{{!
    }}{{#position}} ui-header-{{.}}{{/position}}"{{!
    }}{{> style}}{{!
    }}{{> id}}>{{!
    }}{{#title}}<h1 class="ui-title">{{.}}</h1>{{/title}}{{!

    }}{{> items}}{{!

}}</div>

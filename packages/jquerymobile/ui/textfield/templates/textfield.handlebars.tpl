{{!

// textfield

}}{{#labelInline}}<div class="ui-field-contain ui-body ui-br">{{/labelInline}}{{!
}}{{#label}}<label class="ui-input-text"{{> for_id}}>{{.}}</label>{{/label}}{{!
}}<div class="{{!
    }}{{#if icon}}ui-input-search{{else}}ui-input-text{{/if}}{{!
    }}{{#shadow}} ui-shadow-inset{{/shadow}}{{!
    }} ui-{{#icon}}btn-{{/icon}}corner-all ui-btn-shadow{{!
    }}{{#ui}} ui-body-{{.}}{{/ui}}{{!
    }}{{> cssClass}}{{!
    }}{{#icon}} ui-icon-{{.}}field{{/icon}}{{!
    }}{{#if_eq size compare="small" default="medium"}} ui-mini{{/if_eq}}{{!
    }}{{#if_eq disabled compare=true default=false}} ui-disabled{{/if_eq}}{{!
    }}">{{!

// input

    }}<input {{!
        }}type="{{#if type}}{{type}}{{else}}text{{/if}}"{{!
        }}{{#autocomplete}} autocomplete="{{.}}"{{/autocomplete}}{{!
        }}{{#name}} name="{{.}}"{{/name}}{{!
        }}{{#pattern}} pattern="{{.}}"{{/pattern}}{{!
        }}{{#value}} value="{{.}}"{{/value}}{{!
        }}{{#placeHolder}} placeholder="{{.}}"{{/placeHolder}}{{!
        }}class="ui-input-text{{!
        }}{{#if_eq disabled compare=true default=false}}{{!
            }} mobile-textinput-disabled ui-state-disabled{{!
        }}{{/if_eq}}{{!
        }}{{#ui}} ui-body-{{.}}{{/ui}}"{{!
        }}{{#if_eq disabled compare=true default=false}} disabled="disabled"{{/if_eq}}{{!
        }}{{#if_eq required compare=true default=false}} required{{/if_eq}}{{!
        }}{{#if_eq readonly compare=true default=false}} readonly{{/if_eq}}{{!
        }}{{> id}}>{{!

// clearBtn

    }}{{#if_neq clearBtn compare=false default=false}}{{!
        }}<a href="javascript:;" class="ui-input-clear ui-btn{{!
            }}{{#ui}} ui-btn-up-{{.}}{{/ui}}{{!
            }} ui-shadow{{!
            }} ui-btn-corner-all{{!
            }} ui-fullsize ui-btn-icon-notext{{!
            }}{{#if value}}{{!
                }}{{#if_gteq clearBtn compare=value.length default=1}}{{!
                    }} ui-input-clear-hidden{{!
                }}{{/if_gteq}}{{!
            }}{{else}}{{!
                }} ui-input-clear-hidden{{!
            }}{{/if}}"{{!
            }}{{#id}} id="{{.}}-clearbtn"{{/id}}>{{!

            }}<span class="ui-btn-inner">{{!
                }}<span class="ui-btn-text">clear text</span>{{!
                }}<span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span>{{!
            }}</span>{{!

        }}</a>{{!
    }}{{/if_neq}}{{!

}}</div>{{!
}}{{#labelInline}}</div>{{/labelInline}}

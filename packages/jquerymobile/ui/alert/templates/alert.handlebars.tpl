{{!

// alert

}}<div class="floatingpanel-overlay{{!
    }}{{#if overlay}} floatingpanel-overlay-{{overlay}}{{!
    }}{{else}} floatingpanel-overlay-default{{/if}}"{{!
    }}{{#id}} id="{{.}}-overlay"{{/id}}>{{!
}}</div>{{!

}}<div class="ui-popup-container ui-popup-active"{{!
    }}{{> style}}{{!
    }}{{> id}}>{{!
    }}<div class="{{!
        }}{{#corner}}ui-corner-{{.}}{{/corner}}{{!
        }} ui-popup{{!
        }}{{#ui}} ui-body-{{.}}{{/ui}}{{!
        }}{{#shadow}} ui-overlay-shadow{{/shadow}}"{{!
        }} style="border: 0px !important;">{{!

// close

    }}{{#if close}}{{!
        }}<div class="ui-btn-{{close}} ui-btn{{!
            }}{{#../ui}} ui-btn-up-{{get ../header.ui default=.}}{{/../ui}}{{!
            }}{{#../shadow}} ui-shadow{{/../shadow}}{{!
            }} ui-btn-corner-all ui-btn-icon-notext"{{!
            }}{{#../id}} id="{{.}}-close"{{/../id}}>{{!
            }}<span class="ui-btn-inner">{{!
                }}<span class="ui-btn-text">Close</span>{{!
                }}<span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span>{{!
            }}</span>{{!
        }}</div>{{!
    }}{{/if}}{{!

// header

    }}{{#if header}}{{!
        }}<div class="ui-corner-top ui-header{{!
            }}{{#ui}} ui-bar-{{get ../header.ui default=.}}{{/ui}}">{{!
            }}{{#header.title}}<h1 class="ui-title">{{.}}</h1>{{/header.title}}{{!
        }}</div>{{!
    }}{{/if}}{{!

// content

    }}<div class="ui-corner-bottom ui-content{{!
        }}{{#ui}} ui-body-{{.}}{{/ui}}">{{!

        }}{{> items}}{{!

    }}</div>{{!
    }}</div>{{!
}}</div>

/**
 * @class {{ns}}.Router
 * @extends {{ns}}
 * @xtype router
 *
 * ### Example
 *

 *
 */
{{ns}}.Router = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _history = _ns.History,
        optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;

    function _extractParameters(route, fragment) {
        return route.exec(fragment).slice(1);
    }

    function _routeToRegExp(route) {
        route = route.replace(escapeRegExp, '\\$&')
            .replace(optionalParam, '(?:$1)?')
            .replace(namedParam, function(match, optional) {
                return optional ? match : '([^\/]+)';
            })
            .replace(splatParam, '(.*?)');

        return new RegExp('^' + route + '$');
    }

    function _prepareBefore(self) {

        if (self.before) {

            var pattern;

            self.beforeRegexp = {};
            for (pattern in self.before) {
                /* @ignore !TODO permettre de prendre en compte un array de callback
                if (typeof self.before[i] === 'string') {
                    self.before[i] = [self.before[i]];
                }
                */
                self.beforeRegexp[pattern] = new RegExp(pattern);
            }
        }
    }

    function _getBefore(self, funcname) {

        if (self.before) {

            var pattern;

            for (pattern in self.before) {
                if (self.beforeRegexp[pattern].test(funcname) === true) {
                //if (funcname.match(self.beforeRegexp[i]) !== null) {
                    var func = self[self.before[pattern]];

                    return (func ? func : false);
                }
            }
        }
        return false;
    }

    // public
    return _ns.subclass({
        xtype: 'router',
        /**
         * @cfg {Object} routes
         */
        /**
         * @cfg {Object} before
         */
        /**
         * @cfg {String} routeNoMatch
         */
        /**
         * Create new router.
         */
        constructor: function() {

            var self = this;

            _prepareBefore(self);
            if (self.routes) {

                var pattern;

                for (pattern in self.routes) {
                    self.route(pattern, self.routes[pattern]);
                }
            }

            if (self.routeNoMatch) {
                self.matched = false;
                _history.route('', /^(.*?)+$/ig, function() {
                    if (self.matched === false) {
                        self[self.routeNoMatch]();
                    } else {
                        self.matched = false;
                    }
                });
            }
        },

        /**
         * Calls given path with callback.
         *
         * @param {String} path Path.
         * @param {Function} foo Route callback.
         * @method
         */
        route: function(base, foo) {

            var path,
                before,
                self = this,
                callback = self[foo],
                history = _history;

            if (callback) {
                if (self.routeEscape !== false) {
                    path = _routeToRegExp(base);
                } else {
                    path = new RegExp(base);
                }
                before = _getBefore(self, foo);

                var cb = function(fragment) {

                    var args = _extractParameters(path, fragment);

                    callback.apply(self, args);
                    self.matched = true;
                },
                beforeCb = function(fragment) {
                    var canContinue = before.call(self, function() {
                        return cb(fragment);
                    });
                    if (canContinue === false) return false;
                };

                if (before) {
                    history.route(base, path, beforeCb);
                } else {
                    history.route(base, path, cb);
                }
            }

            return self;
        }
    });
}());

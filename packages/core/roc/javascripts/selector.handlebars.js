/**
 * @singleton
 * @class {{ns}}.Selector
 * @extends {{ns}}
 *
 * DOM manipulation and utilities.
 */
{{ns}}.Selector = new (function() {

    //'use strict';

    // @private
    var _ns = {{ns}},
        _scope,
        _elUid = 0,
        _win = window,
        _prefix,
        _utils = _ns.utils;

    _prefix = (function () {

        var styles = _win.getComputedStyle(document.documentElement, ''),
            pre = (Array.prototype.slice
                .call(styles)
                .join('')
                .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
            )[1],
            dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1],
            cssStyle = _utils.capitalize(pre);

        var events = {
            'moz': undefined,
            'webkit': 'webkit',
            'ms': undefined,
            'o': 'o'
        };

        if (cssStyle === 'Webkit') {
            cssStyle = 'webkit';
        }

        return {
            dom: dom,
            lowercase: pre,
            css: '-' + pre + '-',
            cssStyle: cssStyle,
            jsEvent: function(eventName) {

                var before = events[pre];

                if (!before) {
                    return eventName.toLowerCase();
                }

                return (before + _utils.capitalize(eventName));
            },
            js: pre[0].toUpperCase() + pre.substr(1)
        };

    })();

    // public
    return {{ns}}.subclass({

        prefix: _prefix,

        /**
         * Create new selector.
         * @param {Object} gscope The global scope. Defaults to `window`.
         */
        constructor: function(gscope) {
            _scope = gscope;
        },

        /**
         * Generate new DOM uniq identifier.
         * @param {String} prefix (optional) Prefix.
         * @return {String} id.
         */
        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'r';
            return (prefix + _elUid);
        },

        /**
         * Get a DOM Node from query selector.
         *
         * @param {String} selector Selector
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {HTMLElement} result.
         */
        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },

        /**
         * Get all DOM nodes from query selector.
         * @param {String} selector Selector.
         * @param {HTMLElement} root (optional) Root DOM node. Defaults to global scope.
         * @return {Array} Array of HTMLElement matches.
         */
        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },

        /**
         * Execute callback recursivly on element parent until
         * callback returns false.
         * @param {HTMLElement} el DOM node.
         * @param {Function} callback Callback.
         * @return {Mixed}
         */
        findParent: function(el, callback) {

            var parentEl = callback(el);

            if (parentEl === false && el.parentNode) {
                return this.findParent(el.parentNode, callback);
            }
            return parentEl;
        },

        /**
         * Apply given css properties to element.
         * This method will automatically check for prefix properties.
         * So it is highly recommended to use for example:
         *  `transform` instead of `webkitTransform`.
         *
         * @method css
         * @param {HTMLElement} el DOM node.
         * @param {Object} props CSS properties.
         * @return {Boolean} success
         */
        css: function(el, props) {

            var self = this,
                styles = document.body.style;

            Object.keys(props).forEach(function(name) {

                var compliant = self.prefix.cssStyle + _utils.capitalize(name);

                if (compliant in styles) {
                    el.style[compliant] = props[name];
                }
                el.style[name] = props[name];
            });
            return true;
        },

        /**
         * Checks if element has CSS class.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean}
         */
        hasClass: function(el, cls) {
            // <debug>
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el);
                console.trace('{{ns}}.Selector.hasClass');
                return false;
            }
            // </debug>
            //return (el.classList && el.classList.contains(cls));
            return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') !== -1);
        },

        /**
         * Checks if element has given CSS classes.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        hasClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                if (!this.hasClass(el, c[len])) {
                    return false;
                }
            }
            return true;

        },

        /**
         * Adds CSS class to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                //console.log('addClass ====== ', el.id, cls);
                el.className += ' ' + cls;
                el.className = el.className.replace(/^\s+|\s+$/g, '');
                //el.classList.add(cls);
                return true;
            }
            return false;
        },

        /**
         * Add CSS classes from given element.
         * Use this method instead of {@link #addClass} when you are not sure if one or several of those classes are already applied on element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        addClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.addClass(el, c[len]);
            }
            return true;

        },

        /**
         * Removes CSS class from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS class.
         * @return {Boolean} success
         */
        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                //el.classList.remove(cls);
                return true;
            }
            return false;
        },

        /**
         * Remove CSS classes from given element.
         * Use this method instead of {@link #removeClass} when you are not sure if one or several of those classes are already applied or removed from element.
         * @param {HTMLElement} el DOM node.
         * @param {String} cls CSS classes (seperate by space).
         * @return {Boolean} success.
         */
        removeClasses: function(el, cls) {

            var c = cls.split(' '),
                len = c.length;

            while (len--) {
                this.removeClass(el, c[len]);
            }
            return true;
        },

        /**
         * Force browser to redraw given element.
         * @param {HTMLElement} el DOM node.
         */
        redraw: function(el) {
            el.style.display = 'none';
            var h = el.offsetHeight;
            el.style.display = 'block';
        },

        /**
         * Removes given element from DOM.
         * @param {HTMLElement} el DOM node.
         */
        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },

        /**
         * Removes all HTML from given element.
         * @param {HTMLElement} el DOM node.
         */
        removeHtml: function(el) {
            //el.innerHTML = '';
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },

        // !TODO doc
        findParentByTag: function(el, tagName) {
            while (el) {
                if (el.tagName.toLowerCase() === tagName.toLowerCase()) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        // !TODO doc
        findParentByClass: function(el, className) {
            while (el) {
                if (this.hasClass(el, className)) {
                    return el;
                }
                if (el.parentElement) {
                    el = el.parentElement;
                } else {
                    return false;
                }
            }
            return false;
        },

        /**
         * Adds HTML to given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },

        /**
         * Updates HTML from given element.
         * @param {HTMLElement} el DOM node.
         * @param {String} html HTML.
         */
        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },

        /**
         * Scrolls to given offsets for given element.
         * @param {HTMLElement} el DOM node.
         * @param {Number} offsetX (optional) Offset x. Defaults to 0.
         * @param {Number} offsetY (optional) Offset y. Defaults to 0.
         */
        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });

}())(document);

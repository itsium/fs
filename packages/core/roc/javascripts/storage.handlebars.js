/**
 * @class {{framework.barename}}.Storage
 * @extends {{framework.barename}}
 * @singleton
 *
 * ### Example usage
 *
    var s = {{framework.barename}}.Storage;

    // (optional) set a prefix for our application
    s.setPrefix('myapp');

    // you can store item by name
    s.setItem('news', [{
        id: 1,
        title: 'News 1',
        content: '...'
    }, {
        id: 2,
        title: 'News 2',
        content: '...'
    }]);

    // you can retrieve those items by name too
    s.getItem('news');

    // or remove them by name
    s.removeItem('news');

    // or completly erase all data for given prefix
    s.empty('myapp');
 *
 */
{{ns}}.Storage = new (function() {

    'use strict';

    var _store = localStorage,
        _prefix = '';

    var _privateStore = {};
    var _backupStore = {
        setItem: function(name, value) {
            _privateStore[name] = value;
        },
        getItem: function(name) {
            return _privateStore[name];
        },
        removeItem: function(name) {
            delete _privateStore[name];
        }
    };

    /**
     * Set a default prefix for all store items key.
     * @method setPrefix
     * @param {String} prefix Prefix.
     * @return {Boolean} true if success else false.
     */
    function _setPrefix(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    }

    /**
     * Get the default prefix.
     * @method getPrefix
     * @param {String} prefix Prefix.
     * @return {String} Return given prefix if correct else default prefix.
     */
    function _getPrefix(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    }

    /**
     * Store value for given name.
     * @method setItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _setItem(name, value, prefix, retry) {

        var varname = _getPrefix(prefix) + name,
            varvalue = JSON.stringify(value);

        try {
            _store.setItem(varname, varvalue);
        } catch (e) {
            if (e.name === 'QUOTA_EXCEEDED_ERR') {
                _reset(!retry);
                if (retry !== false) {
                    setTimeout(function() {
                        _setItem(name, value, prefix, false);
                    }, 1);
                } else {
                    _store = _backupStore;
                    _store.setItem(varname, varvalue);
                }
            }
        }
    }

    function _reset(keep_with_prefix) {

        var prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (keep_with_prefix !== true ||
                (prefix && name.indexOf(prefix))) {
                _removeItem(name, '');
            }
        }

    }

    /**
     * Get value stored for given name.
     * @method getItem
     * @param {String} name Name.
     * @param {Mixed} value Value to store.
     * @param {String} prefix (optional) Prefix. Defaults to default prefix.
     * @return {Boolean} True if success else false.
     */
    function _getItem(name, prefix) {

        var value = _store.getItem(_getPrefix(prefix) + name);

        // @bugfix for android 2.3 when JSON.parse null
        if (typeof value === 'string') {
            return JSON.parse(value);
        }
        return false;
    }

    /**
     * Remove value at given name.
     * @method removeItem
     * @param {String} name Name.
     * @param {String} prefix (optional) Prefix.
     * @return {Boolean} True if success else false.
     */
    function _removeItem(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    }

    /**
     * Empty all stored data for given prefix or all if no prefix.
     * @method empty
     * @param {String} prefix (optional) Prefix.
     */
    function _empty(prefix) {
        prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    }

    return {{ns}}.subclass({

        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty,
        reset: _reset

    });

}())();

/**
 * @class {{ns}}.App
 * @extends {{ns}}
 *
 * ### Sample usage:
 *
    var app = new {{ns}}.App({
        onready: function() {
            var main = new App.controllers.Main();
        },
        defaultRoute: '/home',
        engine: 'JqueryMobile',
        ui: 'c'
    });
 *
 */
{{ns}}.App = (function() {

    'use strict';

    var _ns = {{ns}},
        _debug = _ns.debug,
        _storage = _ns.Storage,
        _engines = _ns.engines;

    _ns.global = new _ns.Event();

    _ns.global.on = function(eventName, callback, scope) {

        if (['beforeready', 'ready'].indexOf(eventName) !== -1 &&
            ['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
            scope = scope || this;
            setTimeout(function() {
                callback.call(scope);
            }, 1);
        } else {
            return _ns.Event.prototype.on.apply(this, arguments);
        }

    };

    function _get_engine(name, defaultEngine) {

        var engine = _engines[name];

        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    }

    function _register_listener(self) {

        function ready() {
            if (!self.domReady) {
                document.removeEventListener('DOMContentLoaded', ready, false);
            }

            self.domReady = true;
            _ns.global.fire('beforeready');
            if (self.config && self.config.onready) self.config.onready();
            _ns.global.fire('ready');
            _ns.History.start();
        }

        if (['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
            self.domReady = true;
            setTimeout(ready, 10);
        } else {
            document.addEventListener('DOMContentLoaded', ready, false);
        }
    }

    /**
     * @cfg {Function} onready Callback when application ready to start.
     */

    /**
     * @cfg {String} engine Engine name. Defaults to 'Homemade'.
     */

    /**
     * @cfg {String} defaultRoute (optional) Defaults application route.
     */

    /**
     * @cfg {Boolean} debug (optional) Enables or disables debug mode.
     */

    /**
     * @cfg {Object} require (optional) Required components.
     *
     * ## Sample usage:
     *
     *      require: {
     *          templates: [
     *              'home',
     *              'menu',
     *              ...
     *          ]
     *      }
     *
     */

    /**
     * @cfg {String} ui (optional) Defaults ui for all components (can be override by component configuration).
     */
    return _ns.subclass({

        defaultEngine: 'Homemade',
        domReady: false,

        /**
         * Create new application.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _ns.History.setDefaultRoute(self.config.defaultRoute);
            }
            _register_listener(self);
            // Setting default engine to global configuration object.
            _ns.config.engine = _get_engine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _ns.config.ui = self.config.ui;
            }
        }
    });

})();

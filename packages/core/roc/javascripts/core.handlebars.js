/**
 * @class {{ns}}
 * @singleton
 *
 * Framework global namespace.
 *
 * ### Class definition example

    App.MyClass = (function() {

        // private instances shared variable declaration
        var _parent = {{ns}};

        return _parent.subclass({

            constructor: function(config) {
                // Class constructor
                _parent.prototype.constructor.call(this, config);
            }

        });

    }());
 */

(function() {

    //'use strict';

    var slice = Array.prototype.slice,
        _xtypes = {};

    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {

            if (_xtypes[xtype]) return _xtypes[xtype];
            console.warn('[core] xtype "' + xtype + '" does not exists.');
            return false;

        }
    };

    var _createSubclass = function(props) {

        props = props || {};

        var key,
            realConstructor,
            superclass = props.superclass.prototype;

        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }

        function constructor() {

            var self = this;

            if (!(self instanceof constructor)) {
                throw new Error('[core] Please use "new" when initializing {{ns}} classes');
            }
            realConstructor.apply(self, arguments);
        }

        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;

        _extend(constructor, {

            parent: superclass,

            subclass: function(obj) {

                var sclass;

                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);

                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });

        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }

        return constructor;
    };

    var _extend = function() {

        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();

        while (++i < l) {

            var key,
                props = args[i];

            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };

    var core = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        /**
         * @readonly
         * @property {String} version
         * Current framework version.
         */
        version: '{{framework.version}}',

        /**
         * Create instance of object from given xtype
         * and configuration.
         * @param {String} xtype Xtype.
         * @param {Object} [config] Configuration.
         * @return {Mixed} Instanciated object.
         */
        create: function(xtype, config) {

            var obj = _xtype(xtype);

            return (new (obj)(config));

        },

        /**
         * Extend given xtype or class with given configuration and return the new class.
         * This method will override constructor method of subclass, so please use `subclass` if you wanna override constructor instead.
         * @param  {Mixed} xtype  Xtype string or Parent object class to extend.
         * @param  {Object} config Object to extend.
         * @return {Mixed} New generated extend class.
         *
         * For example:
         *
         *      // this:
         *      var mypage = {{ns}}.extend('page', {
         *
         *          xtype: 'mypage',
         *
         *          config: {
         *              items: [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }]
         *          }
         *
         *      });
         *
         *      // is same as:
         *      var mypage2 = {{ns}}.views.Page.subclass({
         *
         *          xtype: 'mypage',
         *
         *          constructor: function(opts) {
         *
         *              opts = opts || {};
         *
         *              opts.items = [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }];
         *
         *              {{ns}}.views.Page.prototype.constructor.call(this, opts);
         *
         *          }
         *      });
         *
         *      // and same as:
         *      var mypage3 = {{ns}}.extend({{ns}}.views.Page, {
         *
         *          xtype: 'mypage',
         *
         *          config: {
         *              items: [{
         *                  xtype: 'header',
         *                  config: {
         *                      title: 'My page'
         *                  }
         *              }]
         *          }
         *
         *      });
         */
        extend: function(xtype, config) {

            if (typeof xtype === 'string') xtype = _xtype(xtype);

            if (!xtype || typeof xtype.subclass !== 'function') {
                throw new Error('Class or xtype does not exists.');
                return false;
            }

            if (config.config) {
                config.constructor = function(opts) {
                    opts = {{ns}}.utils.applyAuto(config.config, opts);
                    xtype.prototype.constructor.call(this, opts);
                };
            }

            return xtype.subclass(config);
        },

        /**
         * Allows to extend from class.
         * @method
         * @param {Object} obj
         * @return {Object}
         */
        subclass: function(obj) {
            var sclass;

            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);

            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = core;
        }
        exports.{{ns}} = core;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return core;
        });
    } else {
        window.{{ns}} = core;
    }

})();

/**
 * @class {{ns}}.History
 * @extends {{ns}}
 * @singleton
 * @requires {{ns}}.Storage
 *
 * History support class. Use with {@link {{ns}}.Router router}.
 *
 * ### Example
 *
 *      var history = {{ns}}.History;
 *
 *      history.start();
 *      history.navigate('/home');
 *
 *      var currentUrl = history.here();
 *
 */
{{ns}}.History = new (function() {

    'use strict';

    // @private
    var _ns = {{ns}},
        _win = window,
        _storage = _ns.Storage,
        _self,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [],
        _history = [],
        _callbacks = {},
        _maxLength = 20,
        _curLength = 0;

    function runCallbacks(self, location) {
        /*if (self.curhash === location) {
            return false;
        }*/
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }

        var regexp,
            route,
            i = -1,
            length = routes.length;

        self.curhash = location;

        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                if (route.callback(location) === false) {
                    return false;
                }
            }
        }

        if (typeof _callbacks[location] !== 'undefined' &&
            _callbacks[location].length > 0) {

            var c = _callbacks[location],
                len = c.length;

            while (len--) {
                (c.pop())();
            }

        }

        return true;
    }

    var onHashChange = function(event) {

        var here = _win.location.hash.substr(1);

        if (_history[_curLength - 1] !== here) {
            _curLength = _history.push(here);
        }

        if (_curLength > _maxLength) {
            _history.shift();
        }
        runCallbacks(_self, here);
    };

    // public
    return _ns.subclass({

        /**
         * Create new history.
         */
        constructor: function() {
            /**
             * @property {Boolean} started True if history has started else false.
             * @readOnly
             */
            this.started = false;
            /**
             * @property {String} defaultRoute The default route.
             * @readOnly
             */
            this.defaultRoute = '';
            _self = this;
        },

        /**
         * Set the default route.
         * @param {String} path Path.
         */
        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },

        /**
         * Start history.
         * If browser is standalone it will recover to last visited url or {@link #defaultRoute defaultRoute}.
         */
        start: function() {

            if (this.started === true) return false;

            var hash = curhash;

            this.started = true;
            curhash = '';
            _win.addEventListener('hashchange', onHashChange, false);

            // if added to homescreen, try to restore previous location
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks(this, hash);

            return true;
        },

        /**
         * Reset history
         * @param {Boolean} [restart=false] True to restart listener.
         */
        reset: function(restart) {
            _history = [];
            _curLength = 0;

            if (restart === true) {
                this.stop();
                _win.location.hash = '';
                this.start();
            }
        },

        /**
         * Stop history.
         */
        stop: function() {

            if (this.started === false) return false;

            this.started = false;
            _win.removeEventListener('hashchange', onHashChange, false);
        },

        /**
         * Get the current location.
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Current location.
         */
        here: function(encoded) {

            var hash = _win.location.hash.substr(1);

            return (encoded ? encodeURIComponent(hash) : hash);
        },

        /**
         * Get history item from given index.
         * @param {Number} index
         * @param {Boolean} encoded (optional) Return location urlencoded.
         * @return {String} Location for given index.
         */
        get: function(index, encoded) {

            var pos = _curLength - 1 + index;

            if (pos >= _curLength) {
                return this.here(encoded);
            } else if (pos < 0) {
                pos = 0;
            }
            return (encoded ? encodeURIComponent(_history[pos]) : _history[pos]);
        },

        /**
         * Go back in history.
         * @param {Number} index (optional)
         */
        back: function(index) {
            index = (typeof index !== 'number' ? -1 : index);
            this.go(this.get(index));
        },

        // @TODO backToLastRefused
        /*
        Stocker lors du go avec genre {pending: true}
        et lors de l'appel du callback on delete history.pending
        */

        /**
         * Navigate to given location.
         * @param {String} location.
         */
        navigate: function() {
            return this.go.apply(this, arguments);
        },

        go: function(location, callback) {
            _win.location.hash = '#' + location;

            if (typeof callback === 'function') {
                if (typeof _callbacks[location] === 'undefined') {
                    _callbacks[location] = [];
                }
                _callbacks[location].push(callback);
            }
        },

        /**
         * Add given route.
         * @param {String} route Route
         * @param {Function} callback Callback.
         */
        route: function(base, route, callback) {
            routes.push({
                base: base,
                regexp: route,
                callback: callback
            });
        }
    });
}())();

/**
 * @class {{ns}}.Request
 * @extends {{ns}}
 * @singleton
 *
 * Request class to perform remote request.
 * Used by {@link {{ns}}.data.Store#proxy store proxy}.
 *
 * ### Example
 *
 *      {{ns}}.Request.ajax('/login', {
 *          jsonParams: {
 *              username: 'mrsmith',
 *              password: 'wanna die?'
 *          },
 *          scope: this,
 *          callback: function(requestSuccess, data) {
 *
 *              if (requestSuccess === true) {
 *
 *                  if (typeof data === 'object' &&
 *                      data.success === true) {
 *                      // welcome !
 *                  } else
 *                      // login error
 *                  }
 *
 *              } else {
 *                  // server error
 *              }
 *
 *           }
 *      });
 *
 */
{{ns}}.Request = new (function() {

    'use strict';

    // @private
    var jsonp_id = 0;

    var _jsonToString = function(json, prefix) {

        var key,
            val,
            result = '',
            first = true;

        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }

        return (prefix ? prefix : '') + result;
    };

    {{region "request.privates"}}

    function url_delimiter(url) {
        return (url.indexOf('?') === -1 ? '?': '&');
    }

    // public
    return {{ns}}.subclass({

        {{region "request.public_methods" join=","}}

        /**
         * Makes an AJAX request.
         *
         * ## Example
         *
         *      {{ns}}.Request.ajax('/getdata', {
         *          callback: function(success, data) {},
         *          scope: this,
         *          method: 'PUT', // GET, POST, PUT, DELETE
         *          data: 'name1=value1&name2=value2',
         *          params: 'name1=value1&name2=value2', // (for GET)
         *          jsonParams: { name1: 'value1', name2: 'value2' } // (for GET)
         *      });
         *
         * @param {String} url URL.
         * @param {Object} options Options.
         * @method
         */
        ajax: function(url, options) {

            var request,
                callback = (typeof options.callback === 'function' ?
                    options.callback.bind(options.scope || window) : null);
                options.method = options.method || 'GET';
                options.data = options.data || '';

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }

            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },

        /**
         * Makes a JSONP request.
         *
         * ## Example
         *
         *     {{ns}}.Request.jsonp('/getdata?callback={callback}', {
         *         callback: function(success, data) {},
         *         scope: this,
         *         params: 'sortby=date&dir=desc',
         *         jsonParams: {
         *             sortby: 'date',
         *             dir: 'desc'
         *         }
         *     });
         *
         * @param {String} url URL.
         * @param {Object} options An object which may contain the following properties. Note that options will
         * take priority over any defaults that are specified in the class.
         * <ul>
         * <li><b>params</b> : String (Optional)<div class="sub-desc">A string containing additional parameters.</div></li>
         * <li><b>jsonParams</b> : Object (Optional)<div class="sub-desc">An object containing a series of
         * key value pairs that will be sent along with the request.</div></li>
         * <li><b>callback</b> : Function (Optional) <div class="sub-desc">A function to execute when the request
         * completes, whether it is a success or failure.</div></li>
         * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope in
         * which to execute the callbacks: The "this" object for the callback function. Defaults to the browser window.</div></li>
         * </ul>
         * @method
         */
        jsonp: function(url, options) {

            var callback_name = ['jsonp', ++jsonp_id].join(''),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    //_debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams, url_delimiter(url));
            }
            if (options.params) {
                url += (url_delimiter(url) + options.params);
            }

            if (url.indexOf('{callback}') === -1) {
                url += (url_delimiter(url) + 'callback={callback}');
            }

            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        }
    });
}())();

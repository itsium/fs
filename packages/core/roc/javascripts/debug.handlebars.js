{{framework.barename}}.debug = new (function() {

    var _profiles = {},
        _debug = false,
        _whitelist = [/*
            'router',
            'news',
            'store',
            'request',
            'newsdetail',
            'button',
            'list',
            //'click',
            //'clickbuster',
            'listbuffered',
            'loadmask',
            'scroll',
            'request',
            'bounce'
        */];

    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };

    return {{framework.barename}}.subclass({

        /*profile: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.time(key);
            //_profiles[key] = this.memory();
            return this;
        },*/

        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },

        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },

        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },

        /*profileEnd: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.timeEnd(key);
            //console.debug(_profiles[key], '->', this.memory());
            //console.debug(key, 'Before: ', _profiles[key]);
            //console.debug(key, 'Now: ', (window.performance.memory.usedJSHeapSize / 1024.0) + 'ko');
            return this;
        },

        memory: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            if (window.performance && window.performance.memory) {
                return ((window.performance.memory.totalJSHeapSize / 1024.0) + 'ko');
            }
            return 0;
        },*/

        set: function(debug) {
            _debug = (debug === true);
        }
    });

}())();

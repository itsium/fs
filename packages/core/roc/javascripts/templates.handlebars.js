/**
 * @class {{ns}}.Templates
 * @extends {{ns}}
 * @requires Handlebars
 */
{{ns}}.Templates = new (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _handlebars = Handlebars,
        _request = _ns.Request;

    _handlebars.currentParent = [];

    /**
     * @ignore
     * Render given configuration element.
     *
     * ### Sample usage
     * {{!--
        {{#items}}
            {{render .}}
        {{/items}}
     * --}}
     * Where items are for example:
     * items: [{ xtype: 'button', config: { text: 'Hello' }}]
     */
    // <unstable> Use it carefully. Still testing performance memory/cpu.
    _handlebars.registerHelper('render', function(config) {

        var html,
            parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = new (_ns.xtype(config.xtype))(config),
            renderer = ((parent.renderer && parent.renderer.el) ? parent.abstractRenderer : parent.renderer);

        /*
            abstractRenderer is used for calling afterrender event for item compile after
            the parent renderer has been rendered.
            For example: the list renders her children after render itself so we need to use
            the abstract renderer.
            !TODO We should store the item inside parents collections to be able to delete it or retrieve it.
        */
        if (!renderer) {
            renderer = parent.abstractRenderer = new _ns.Event();
        }

        html = item.compile(renderer.config, renderer);
        return new _handlebars.SafeString(html);
    });
    // </unstable>

    /**
     * @ignore
     * Get current config from given item children index.
     *
     * ### Sample usage
     * {{!--
        {{#foreach items}}

            {{#config $index}}
                {{! current context is now config of given item index }}
                {{label}}
            {{else}}
                Failed to find config.
                Optional behavior.
            {{/config}}

        {{/foreach}}
     * --}}
     */
    _handlebars.registerHelper('config', function(context, options) {

        var parent = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            item = (parent.items ? parent.items[context] : undefined);

        if (typeof item === 'object' && item.config) {
            return options.fn(item.config);
        }
        return options.fn(this);

    });

    // @ignore !TODO permettre d'avoir la config d'un item a partir de son index et/ou id
    /*_handlebars.registerHelper('getconfig', function() {

    });*/

    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });

    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });

    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });

    return _ns.subclass({

        /**
         * Create new templates class.
         */
        constructor: function() {
            this.path = '';
        },

        /**
         * Set path for loading external template files.
         * Used by {@link {{ns}}.App#require application} class.
         * @param {String} path Path.
         */
        setPath: function(path) {
            this.path = path;
        },

        /**
         * Get template by given name.
         * @param {String} name Template name.
         * @return {Function} Template function.
         */
        get: function(name) {
            return _handlebars.templates[name];
        },

        /**
         * Compile given source or retrieve the template.
         * @param {Mixed} source The source.
         * @return {Function} Function corresponding to the given source.
         */
        compile: function(source) {

            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }

            return function() {
                return source;
            };
        }
    });
}())();

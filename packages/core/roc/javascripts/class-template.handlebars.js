{{#template name="roc_class"}}

{{copyrights}}

{{classname}} = (function() {

    {{region name="private"}}

    return {{extends}}.subclass({

        {{region name="public"}}

        {{yield}}

    });

}());

{{/template}}
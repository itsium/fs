{{#content_for "request.public_methods"}}

    /**
     * Store memory proxy.
     *
     * @method
     * @param {Array} data Data.
     * @param {Object} options Options.
     * @param {@link {{ns}}.data.Store} store Store.
     */
    memory: function(url, options, store) {

        var i, length, callback,
            data = options.data,
            result = {},
            idProp = store.getReaderId(),
            totalProp = store.getReaderTotal(),
            successProp = store.getReaderSuccess(),
            rootProp = store.getReaderRoot();

        callback = options.callback.bind(options.scope || window);
        if (!data) {
            data = store.config.data || [];
        }
        length = data.length;
        if (data[0] && typeof data[0][idProp] === 'undefined') {
            for (i = 0; i < length; ) {
                data[i][idProp] = ++i;
            }
        }
        result[totalProp] = length;
        result[successProp] = true;
        result[rootProp] = data;
        if (callback) {
            callback(true, result);
        }
    }

{{/content_for}}
/**
 * @ignore !TODO for debugging: Global event handler like {{ns}}.EventHandler.listenAllEvents('afterrender');
 */
/**
 * @class {{ns}}.Event
 * @extends {{ns}}
 * @requires {{ns}}.utils
 *
 * Event manager class.
 *
 * ## Example

    var event = new {{ns}}.Event({
        listeners: {
            scope: this,
            afterload: function(success, data) {
                // do some stuff here...
            },
            beforeload: function(request) {
                if (typeof request.data === 'undefined') {
                    return false; // stop all next events from being firing.
                }
            }
        }
    });

    // Registering listener for `error` event.
    var eid = event.on('error', function(code, message) {
        alert('Error (' + code + ') occured: \n' + message);
    }, this);

    // Firing `error` event.
    event.fire('error', 401, 'Unauthorized !');

    // Unregistering listener.
    event.off(eid);

 */
{{ns}}.Event = (function() {

    'use strict';

    var _ns = {{ns}},
        _utils = _ns.utils,
        _parent = _ns,
        _priorities = {
            BEFORECORE: 1025,
            CORE: 1000,
            AFTERCORE: 975,
            BEFOREVIEWS: 925,
            VIEWS: 900,
            AFTERVIEWS: 875,
            DEFAULT: 800
        };

    function _setupEvents(self) {
        if (typeof self._events !== 'object') {
            // events queue
            /*

            e1 = event_id

            {
                "eventname1": {
                    "998": [e1, e2],
                    "1000": [e3]
                },
                ...
            }

            */
            self._events = {};
            self.uids = 0;
            // priority queue @TODO
            /*

            {
                "eventname1": [998, 1000],
                "eventname2": [1000],
                "eventname3": [100, 1000, 9999],
                ...
            }

            */
            self.pqueue = {};
            // callbacks queue
            /*

            {
                "event_id": [event_name, priority, callback, scope],
                ...
            }

            */
            self.equeue = {};
        }
    }

    function _numSort(a, b) {
        return (b - a);
    }

    function _count_listeners(self, name) {

        var e = self._events[name];

        if (e) {
            var total = 0,
                keys = Object.keys(self._events[name]),
                i = keys.length;

            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    }

    function _register_listeners(self, listeners) {

        var key, priority,
            scope = self,
            l = self._listeners = self._listeners || {},
            keys = Object.keys(listeners),
            len = keys.length;

        var scopeIndex = keys.indexOf('scope');
        if (scopeIndex !== -1) {
            keys.splice(scopeIndex, 1);
            scope = listeners.scope;
        }

        var priorityIndex = keys.indexOf('priority');
        if (priorityIndex !== -1) {
            keys.splice(priorityIndex, 1);
            priority = listeners.priority;
        }

        if (typeof priority === 'string' &&
            typeof _priorities[priority] !== 'undefined') {
            priority = _priorities[priority];
        } else {
            priority = _priorities.DEFAULT;
        }

        while (len--) {
            key = keys[len];

            if (!self._listeners[key]) {
                self._listeners[key] = [];
            }
            self._listeners[key].push([listeners[key], scope, priority]);
        }

    }

    /**
     * @cfg {Object} listeners Listeners.
     */
    return _parent.subclass({

        /**
         * @property {Number} priority. Prior to higher.
         * <p><ul>
            <li><b>CORE:</b> Core event priority (high)</li>
            <li><b>VIEWS:</b> Views event priority (medium)</li>
            <li><b>DEFAULT:</b> Default event priority (low)</li>
         * </ul></p>
         */
        priority: _priorities,

        /**
         * @property {Number} defaultPriority Default priority.
         */
        defaultPriority: 800,

        /**
         * Create new event manager.
         * @constructor
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {
            // <inline>

            var self = this;

            opts = opts || {};

            if (opts && opts.listeners) {
                //self.listeners = opts.listeners;
                _register_listeners(self, opts.listeners);
            }
            if (self.listeners) {
                _register_listeners(self, self.listeners);
            }
            self.wasListened = {};
            _setupEvents(self);

            if (opts.hasListeners) {
                self.hasListeners = (self.hasListeners || [])
                    .concat(opts.hasListeners);
            }

            if (self.hasListeners) {
                self.setupListeners(self.hasListeners, self);
            }
            // </inline>
        },

        addListeners: function(listeners) {
            _register_listeners(this, listeners);
        },

        {{#comment}}
        /**
         * Initialize listeners for given event and renderer.
         *
         * {{comment_method "setupListeners"}}
         * @param {Array} eventNames List of event names.
         * @param { {{ns}}.views.Template } renderer (optional) Renderer. Defaults to current class instance.
         */
        {{/comment}}
        setupListeners: function(eventNames, renderer) {

            var self = this;

            if (self._listeners) {

                var name,
                    scope = self._listeners.scope || self,
                    i = eventNames.length;

                renderer = renderer || self;

                while (i--) {
                    name = eventNames[i];

                    if (self._listeners[name] &&
                        !self.wasListened[name]) {

                        self.wasListened[name] = true;
                        self._listeners[name].forEach(function(listener) {
                            renderer.on(name, listener[0],
                                (listener[1] || scope), listener[2]);
                        });

                    }

                }
            }
        },

        /**
         * Checks to see if this object has any listeners for a specified event.
         * @param {String} name Event name.
         * @return {Boolean} True if the event is being listened for, else false.
         */
        hasListener: function(name) {
            if (typeof this._listeners === 'object') {
                return (this._listeners[name] instanceof Array);
            }
            return false;
        },

        /**
         * Return listeners for given event name.
         *
         * @param {String} name Event name.
         * @return {Function} Listener callback for given event name.
         */
        getListener: function(name) {
            return this._listeners[name];
        },

        /**
         * Fires given event with given extra parameters.
         *
         * ## Example
         *
         *     // Registering event listener
         *     var eid = this.on('afterload', function(success, data) {
         *         console.log('Received data !', success, data);
         *     }, this);
         *
         *     // Fireing event
         *     this.fire('afterload', true, 'Some data');
         *
         *     // Unregistering event
         *     this.off(eid);
         *
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fire: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1),
                    toOff = [];

                while (++i < plen) {
                    events = self._events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function !');//, arguments.callee.callee, e);
                            toOff.push(events[j]);
                        } else if (e[2].apply(e[3], args) === false) {
                            return false;
                        }
                    }

                }

                toOff.forEach(function(uid) {
                    self.off(uid);
                });
                return true;
            }
        },

        /**
         * Same as fire but put `scope` as first arguments instead of scoping it when calling listeners.
         * @param {String} name Event name to fire.
         * @param {Object...} args Variable number of parameters are passed to handlers.
         * @return {Boolean} Returns `false` if any of the handlers return `false` else return `true`.
         */
        fireScope: function(name) {

            var self = this,
                p = self.pqueue[name];

            if (p) {
                p = self.pqueue[name].slice(0);

                var e, events, elen, j, i = -1, plen = p.length,
                    args = _utils.arraySlice(arguments, 1),
                    toOff = [];

                while (++i < plen) {
                    events = self._events[name]['p' + p[i]].slice(0);
                    elen = events.length;
                    j = -1;

                    while (++j < elen) {

                        e = self.equeue['u' + events[j]];

                        if (typeof e[2] !== 'function') {
                            console.warn('fire "', name, '" not a function !');//, arguments.callee.callee, e);
                            toOff.push(events[j]);
                        } else if (e[2].apply(e[3], [e[3]].concat(args)) === false) {
                            return false;
                        }
                    }

                }

                toOff.forEach(function(uid) {
                    self.off(uid);
                });
                return true;
            }
        },

        /**
         * Register listener for given eventname.
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this, this.priority.VIEWS);
         *
         * @param {String} name Event name.
         * @param {Function} cb Listener callback.
         * @param {Object} [scope=null] Callback scope.
         * @param {Number} [priority={{ns}}.Event.DEFAULT] Callback priority.
         * @return {Number} Returns listener uniq identifier `uid`.
         */
        on: function(name, cb, scope, priority) {

            var self = this;

            if (typeof priority === 'string') {
                priority = _priorities[priority];
            }

            priority = priority || self.defaultPriority;

            if (typeof self._events[name] === 'undefined') {
                self._events[name] = {};
            }

            if (typeof self._events[name]['p' + priority] === 'undefined') {
                self._events[name]['p' + priority] = [];
            }

            if (typeof self.pqueue[name] === 'undefined') {
                self.pqueue[name] = [];
            }

            if (self.pqueue[name].indexOf(priority) === -1) {
                self.pqueue[name].push(priority);
                self.pqueue[name].sort(_numSort);
            }
            scope = scope || window;
            self.equeue['u' + (++self.uids)] = [name, priority, cb, scope];
            self._events[name]['p' + priority].push(self.uids);
            return self.uids;
        },

        has: function(name) {
            return _count_listeners(this, name);
        },

        /**
         * Unregister listener for given listener uniq identifier (returns by `on` method).
         *
         * ## Example
         *
         *      var eid = this.on('ready', launchapp, this);
         *      // ...
         *      this.off(eid);
         *
         * @param {Number} uid Event listener uniq identifier (returns by `on` method).
         * @return {Mixed} Returns false if error else return event listener count.
         */
        off: function(uid) {

            var self = this,
                e = self.equeue['u' + uid];

            if (e) {

                var cb,
                    length,
                    name = e[0],
                    priority = e[1],
                    listeners = self._events[name]['p' + priority];

                listeners.splice(listeners.indexOf(uid), 1);
                length = listeners.length;

                delete self.equeue['u' + uid];

                if (!length) {
                    // cleaning hash tables
                    delete self._events[name]['p' + priority];
                    self.pqueue[name].splice(self.pqueue[name].indexOf(priority), 1);
                    if (!self.pqueue[name].length) {
                        delete self.pqueue[name];
                        delete self._events[name];
                        return 0;
                    }
                }
                return _count_listeners(self, name);
            }
            return false;
        }
    });

}());

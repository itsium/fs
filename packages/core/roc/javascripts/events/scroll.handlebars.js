/**
 * @class {{ns}}.events.Scroll
 * @extends {{ns}}
 * @xtype events.scroll
 *
 * Scroll event class.
 *
 * ## Example
 *
 *       var scrollEvent = new {{ns}}.events.Scroll({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: loadMoreItem
 *       });
 *
 * @deprecated If you use {{ns}}.physics.Scroll, this event won't be fire.
 */
{{ns}}.events.Scroll = (function() {

    'use strict';

    // private
    var _lastEvent,
        _intervalId,
        _ns = {{ns}},
        _debug = _ns.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,//_window.screen.availHeight,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _ns.Event();

    function _noHandler() {
        _debug.error('scroll', '[{{ns}}.events.Scroll] No handler is define !');
    }

    function _poolHandler() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    }

    function _windowScrollListener() {
        _scrolled = true;
        _lastEvent = event;
    }

    function _windowResizeListener() {
        _screenHeight = _doc.height;//_window.screen.availHeight;
    }



    // public
    return _ns.subclass({

        xtype: 'events.scroll',
        defaultInterval: 167, // 60fps
        useCapture: false,
        autoAttach: false,
        // defaults
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.interval = opts.interval || self.defaultInterval;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _intervalId = _window.setInterval(_poolHandler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);
            self.euid = _events.on('scroll', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;

            var length = _events.off(self.euid);

            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_intervalId);
        }
    });
})();

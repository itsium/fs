/**
 * @class {{ns}}.events.Blur
 * @extends {{ns}}.events.Abstract
 * @xtype events.blur
 *
 * Input blur event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var blurEvent = new {{ns}}.events.Blur({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on blur called
 *             {{ns}}.Selector.removeClass(blurEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
{{ns}}.events.Blur = (function() {

    // private
    var _ns = {{ns}},
        //isAndroid = (navigator.userAgent.indexOf('Android ') !== -1),
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.blur',

        /**
         * @readonly
         */
        eventName: 'focusout',//(isAndroid === true ? 'blur' : 'focusout'),

        /**
         * @readonly
         */
        globalFwd: window,//(isAndroid === true ? undefined : window),

        /**
         * New blur event.
         */
        constructor: _parent.prototype.constructor
    });
}());

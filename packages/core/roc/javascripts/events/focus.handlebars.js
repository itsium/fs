/**
 * @class {{ns}}.events.Focus
 * @extends {{ns}}.events.Abstract
 * @xtype events.focus
 *
 * Input focus event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var focusEvent = new {{ns}}.events.Focus({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on focus called
 *             {{ns}}.Selector.addClass(focusEvent.el, 'input-focus');
 *         }
 *     });
 *
 */
{{ns}}.events.Focus = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        //isAndroid = (navigator.userAgent.indexOf('Android ') !== -1),
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.focus',

        /**
         * @readonly
         * @property {String} eventName Browser event name.
         */
        eventName: 'focusin',//(isAndroid === true ? 'focus' : 'focusin'),

        /**
         * @readonly
         * @property {Object} globalFwd Global forward scope.
         */
        globalFwd: window,//(isAndroid === true ? undefined : window),

        /**
         * New focus event.
         */
        constructor: _parent.prototype.constructor
    });
}());

/**
 * @class {{ns}}.events.Click
 * @extends {{ns}}
 * @xtype events.click
 *
 * Click event class. Also remove 300ms delay for touch device.
 *
 * ## Example
 *
 *       var clickEvent = new {{ns}}.events.Click({
 *           autoAttach: true,
 *           el: document.getElementById('mybutton'),
 *           scope: this,
 *           handler: showPopup
 *       });
 *
 */
{{ns}}.events.Click = (function() {

    'use strict';

    // private
    var _ns = {{ns}};
        //_isMobile = ('ontouchstart' in document.documentElement)


    function _no_handler() {
        console.warn('click', 'No handler is define !');
    }

    function _stop_scroll(e) {
        e.preventDefault();
    }

    // public
    return _ns.subclass({

        xtype: 'events.click',

        /**
         * @cfg {Boolean} disableScrolling (optional) Enable or disable scrolling on attached element.
         * Defaults to `false`.
         */
        defaultDisableScrolling: false,

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        defaultUseCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        /**
         * New click event.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.disableScrolling = opts.disableScrolling || self.defaultDisableScrolling;
            self.useCapture = opts.useCapture || self.defaultUseCapture;
            _ns.utils.apply(self, opts, [
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);

            /**
             * @property {Boolean} attached True if event is attached.
             */
            self.attached = false;

            if (self.autoAttach) {
                self.attach();
            }
        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler.bind(scope) : self.defaultHandler);

            self.el.addEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.addEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }

            self.attached = false;
            self.el = cmp || self.el || self.defaultEl;
            self.handler = self.handler || self.defaultHandler;

            self.el.removeEventListener('click', self.handler, self.useCapture);
            if (self.disableScrolling) {
                self.el.removeEventListener('touchmove', _stop_scroll, self.useCapture);
            }
        }
    });
}());

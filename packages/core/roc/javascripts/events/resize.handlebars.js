/**
 * @class {{ns}}.events.Resize
 * @extends {{ns}}
 * @xtype events.resize
 *
 * Resize event class.
 *
 * ## Example
 *
 *       var resizeEvent = new {{ns}}.events.Resize({
 *           autoAttach: true,
 *           el: document.body,
 *           scope: this,
 *           handler: updateLayout
 *       });
 *
 */
{{ns}}.events.Resize = (function() {

    'use strict';

    // private
    var //_lastEvent,
        //_intervalId,
        _window = window,
        _eventsAttached = false,
        //_resized = false,
        _ns = {{ns}},
        _debug = _ns.debug,
        _events = new _ns.Event();

    function _no_handler() {
        _debug.warn('events.resize', 'No handler is define !');
    }

    // function _pool_handler() {
    //     if (_resized === true) {
    //         _resized = false;
    //         _events.fire('resize', _lastEvent);
    //     }
    // }

    function _window_resize_listener(e) {
        // _resized = true;
        // _lastEvent = e;
        _events.fire('resize', e);
    }

    // public
    return _ns.subclass({

        xtype: 'events.resize',
        //defaultInterval: 500,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            //self.interval = opts.interval || self.defaultInterval;
            self.attached = false;
            self.defaultEl = opts.el || window;
            self.defaultScope = opts.scope || self;
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _no_handler);
        },

        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }

            self.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _window_resize_listener, false);
                //_intervalId = _window.setInterval(_pool_handler, self.interval);
            }
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ? handler : self.defaultHandler);
            self.euid = _events.on('resize', self.handler, scope);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;

            var length = _events.off(self.euid);

            delete self.handler;
            if (length === 0) {
                self.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _window_resize_listener, false);
            //_window.clearInterval(_intervalId);
        }
    });
})();

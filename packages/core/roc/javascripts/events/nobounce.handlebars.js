/**
 * var nobounce = new {{ns}}.events.Nobounce({
     className: 'nobounce', // optional
     autoAttach: true, // optional
     el: document // optional
   });
 * nobounce.attach(document);
 * nobounce.detach(document);
 */
{{ns}}.events.Nobounce = (function() {

    // private
    var _lastEvent,
        _ns = {{ns}},
        $ = _ns.Selector,
        _doc = document,
        _parent = _ns;

    var _isParentNoBounce = function(el, className) {
        while (el) {
            if ($.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _disableBounce = function(e) {
        if (_isParentNoBounce(e.target, this.className)) {
            e.preventDefault();
        }
    };

    // public
    return _parent.subclass({

        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            self.autoAttach = opts.autoAttach || self.defaultAutoAttach;
            self.className = opts.className || self.defaultClassName;
            self.el = opts.el || self.defaultEl;
            self.attached = false;
            self.handler = _disableBounce.bind(self);
            if (self.autoAttach === true) {
                self.attach(self.el);
            }
        },

        attach: function(cmp) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.el;
            self.el.addEventListener('touchmove', self.handler, false);
        },

        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;
            self.el = cmp || self.el;
            self.el.removeEventListener('touchmove', self.handler, false);
        }
    });
})();

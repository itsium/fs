/**
 * @class {{ns}}.events.Keypress
 * @extends {{ns}}.events.Abstract
 * @xtype events.keypress
 *
 * Input keypress event class.
 *
 * ## Example
 *
 *     // <input type="text" name="username" value="mrsmith" id="usernameField" />
 *
 *     var field = document.getElementById('usernameField');
 *
 *     var keypressEvent = new {{ns}}.events.Keypress({
 *         el: field,
 *         scope: this,
 *         handler: function(e) {
 *             // on keypress called
 *             if (e.keyCode === 13) {
 *                 this.form.submit();
 *             }
 *         }
 *     });
 *
 */
{{ns}}.events.Keypress = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.events.Abstract;

    // public
    return _parent.subclass({

        xtype: 'events.keypress',

        /**
         * @readonly
         */
        eventName: 'keypress',

        /**
         * @readonly
         */
        globalFwd: window,

        /**
         * New keypress event.
         */
        constructor: function(opts) {

            var self = this;


            if (opts.el) {

                var tagname = opts.el.tagName;

                if (tagname !== 'INPUT' && tagname !== 'TEXTAREA') {
                    opts.el.setAttribute('tabindex', '1');
                }
            }

            _parent.prototype.constructor.call(self, opts);

        }
    });
}());

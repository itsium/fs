/**
 * @class {{ns}}.Settings
 * @extends {{ns}}.Event
 * @singleton
 *
 * ### Usage example

    var session = new {{ns}}.Settings({
        username: 'mrsmith',
        logged: true,
        city: 'Paris',
        country: 'France'
    }, {
        storage: {{ns}}.Storage,
        storageKeys: ['city', 'country']
    });

    session.on('loggedchanged', function(s, n, value, oldValue) {
        if (value === false) {
            // show login
        } else {
            // hide login
        }
    }, this);

    session.set('logged', false);

 *
 */
{{ns}}.Settings = (function() {

    'use strict';

    var _ns = {{ns}},
        _parent = _ns.Event;

    function with_storage(self, name) {
        if (self.storage) {
            if (self.storageKeys) {
                return (self.storageKeys.indexOf(name) !== -1);
            }
            return true;
        }
        return false;
    }

    return _parent.subclass({

        /**
         * New Settings.
         * @param {Object} config (optional) Configuration.
         */
        constructor: function(config, opts) {

            var self = this;

            self.vars = (config ? config : {});

            if (opts && opts.storage) {
                self.storage = opts.storage;
                if (opts.storageKeys) {
                    self.storageKeys = opts.storageKeys;
                    self.storageKeys.forEach(function(key) {
                        if (typeof self.vars[key] !== 'undefined') {
                            self.storage.setItem(key, self.vars[key]);
                            delete self.vars[key];
                        }
                    });
                } else {
                    delete self.vars;
                }
            }

            _parent.prototype.constructor.apply(self, arguments);
        },

        /**
         * Set value for given name.
         * @param {String} name Name.
         * @param {Mixed} value Value.
         * @param {Boolean} silent True to disable fire events. Defaults to false.
         * @fires settingschanged
         * @fires `name`changed
         */
        set: function(name, value, silent) {

            var self = this,
                oldValue = this.get(name);

            if (with_storage(self, name)) {
                self.storage.setItem(name, value);
            } else {
                self.vars[name] = value;
            }

            if (silent !== true) {
                self.fire('settingchanged', self, name, value, oldValue);
                self.fire(name + 'changed', self, name, value, oldValue);
            }
        },

        /**
         * Get value from given name.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        get: function(name, defaultValue) {

            var self = this;

            if (with_storage(self, name)) {
                return self.storage.getItem(name, defaultValue);
            } else if (typeof self.vars[name] !== 'undefined') {
                return self.vars[name];
            }

            return defaultValue;
        },

        /**
         * Get value from given name and delete it if exists.
         * @param {String} name Name.
         * @param {Mixed} defaultValue Default value if variable does not exists.
         * @return {Mixed} Value for given name or given default value if `undefined`.
         */
        pop: function(name, defaultValue) {

            var self = this,
                value = self.get(name, defaultValue);

            self.empty(name);

            return value;

        },

        /**
         * Gets value from given names.
         *
         * ## Sample usage
         *
         *      var loginUrl = settings.gets('serverUrl', 'loginPath').join('');
         *
         * @param {String...} names Variable names.
         * @return {Array} Values for given names put into array.
         */
        gets: function() {

            var result = [],
                i = -1,
                length = arguments.length;

            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        },

        /**
         * @fires nameempty
         * Empty given variable name.
         * @param {String} name Variable name.
         * @param {Boolean} silent True to disable fire events. Defaults to false.
         */
        empty: function(name, silent) {

            var self = this;

            if (with_storage(self, name)) {
                self.storage.removeItem(name);
            }

            if (typeof self.vars[name] !== 'undefined') {

                var oldValue = self.vars[name];

                delete self.vars[name];
                if (silent !== true) {
                    self.fire(name + 'empty', self, name, oldValue);
                    self.fire(name + 'changed', self, name, undefined, oldValue);
                }
            }
        }
    });

}());

(function() {

    /**
     * @ignore
     * FastClick is an override shim which maps event pairs of
     *   'touchstart' and 'touchend' which differ by less than a certain
     *   threshold to the 'click' event.
     *   This is used to speed up clicks on some browsers.
     */
    var clickThreshold = 300;
    var clickWindow = 500;
    var potentialClicks = {};
    var recentlyDispatched = {};
    var win = window;
    var inputs = ['INPUT', 'TEXTAREA', 'SELECT'];
    var android = (win.navigator.userAgent.indexOf('Android') >= 0);
    var mustBlur = false;

    win.addEventListener('touchstart', function(event) {

        if (mustBlur) {
            //if (document.activeElement) document.activeElement.blur();
            if (typeof mustBlur.blur === 'function') {
                mustBlur.blur();
            }
            mustBlur = false;
        }

        var timestamp = Date.now(),
            len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            potentialClicks[touch.identifier] = timestamp;
        }
    });
    win.addEventListener('touchmove', function(event) {

        var len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            delete potentialClicks[touch.identifier];
        }
    });
    win.addEventListener('touchend', function(event) {

        var currTime = Date.now(),
            len = event.changedTouches.length;

        while (len--) {
            var touch = event.changedTouches[len];
            var startTime = potentialClicks[touch.identifier];
            if (startTime && currTime - startTime < clickThreshold) {
                var clickEvt = new win.CustomEvent('click', {
                    'bubbles': true,
                    'details': touch
                });
                recentlyDispatched[currTime] = event;
                event.target.dispatchEvent(clickEvt);
                // we prevent here to avoid ghost click on ios 7 with input click (2nd time)
                // tested on ios
                event.preventDefault(); // if (ios) ? must test on android
                if (inputs.indexOf(event.target.tagName) !== -1) {
                    if (!android && event.target.focus) event.target.focus();
                    else mustBlur = event.target;
                }
            }
            delete potentialClicks[touch.identifier];
        }
    });
    win.addEventListener('click', function(event) {

        var currTime = Date.now();
        for (var i in recentlyDispatched) {
            var previousEvent = recentlyDispatched[i];
            if (currTime - i < clickWindow) {
                if (event instanceof win.MouseEvent) {
                    event.stopPropagation();
                    // prevent ghost click
                    if (event.target !== previousEvent.target) event.preventDefault();
                }
                // if (event instanceof win.MouseEvent && event.target === previousEvent.target) event.stopPropagation();
                // else if (event instanceof win.MouseEvent && event.target !== previousEvent.target) {
                //     // @TODO @PATCH prevent ghost click
                //     event.stopPropagation();
                //     event.preventDefault();
                // }
            }
            else delete recentlyDispatched[i];
        }
    }, true);

}());

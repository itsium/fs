function processor(roc) {

    'use strict';

    function run(grunt, pkg, file, done) {

        var coffee = grunt.config.getRaw('coffee') || {};

        coffee[pkg.id + file] = {
            src: file,
            dest: file
        };

        grunt.config('coffee', coffee);
        grunt.task.run('coffee:' + pkg.id + file);

        process.nextTick(done);

    }

    roc.processor({
        name: 'coffee',
        description: 'Coffee script.',
        extensions: ['coffee'],
        filetypes: ['javascripts'],
        run: run
    });

}

module.exports = processor;

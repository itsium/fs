function processor(roc) {

    'use strict';

    function run(grunt, pkg, file, done) {

        var less = grunt.config.getRaw('less') || {};

        less[pkg.id + file] = {
            src: file,
            dest: file
        };

        grunt.config('less', less);
        grunt.task.run('less:' + pkg.id + file);

        process.nextTick(done);

    }

    roc.processor({
        name: 'less',
        description: 'LESS processor for CSS files.',
        extensions: ['less'],
        filetypes: ['stylesheets'],
        run: run
    });

}

module.exports = processor;

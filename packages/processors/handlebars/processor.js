function processor(roc) {

    'use strict';

    var utils = roc.require('utils'),
        precompiler = roc.require('precompiler');

    function run(grunt, pkg, file, done) {

        var handlebars = grunt.config.getRaw('roc_handlebars') || {};

        // @TODO add pkg.id to roc_handlebars configuration
        // console.log(pkg, file);
        // process.exit(1);

        var roc = grunt.config.get('roc');

        if (roc.current.config) {
            roc = utils.extend(roc, roc.current.config);
        }

        handlebars[pkg.id + file] = {
            helpers: precompiler.roc_helpers,
            template: file,
            templateData: utils.extend({
                me: pkg.config
                // we should extend this config with the current config
                // from the given config file !
            }, roc),
            output: file
        };

        grunt.config('roc_handlebars', handlebars);
        grunt.task.run('roc_handlebars:' + pkg.id + file);

        process.nextTick(done);

    }

    roc.processor({
        name: 'handlebars',
        extensions: ['handlebars', 'hbs'],
        filetypes: ['javascripts', 'stylesheets', 'htmls'],
        run: run
    });

}

module.exports = processor;
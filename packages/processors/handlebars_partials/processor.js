function processor(roc) {

    'use strict';

    var path = require('path');

    // This method takes the filepath of an Handlebars templates
    // and create the template name from it.
    function templates_process_name(filepath) {

        var ext,
            filename = path.basename(filepath);

        ext = (filename.indexOf('.handlebars') !== -1 ?
            filename.indexOf('.handlebars') :
            filename.indexOf('.hbs'));

        if (ext > -1) {
            filename = filename.substr(0, ext);
        }

        return filename;
    }

    function run(grunt, pkg, file, done) {

        var handlebars = grunt.config.getRaw('handlebars') || {};

        handlebars[pkg.id + file] = {
            options: {
                namespace: 'Handlebars',
                partialsPathRegexp: /[^\r]*/,
                partialRegex: /[^\r]*/,
                processPartialName: templates_process_name,
                processName: templates_process_name
            },
            files: {}
        };

        handlebars[pkg.id + file].files[file] = file;

        grunt.config('handlebars', handlebars);
        grunt.task.run('handlebars:' + pkg.id + file);

        process.nextTick(done);

    }

    roc.processor({
        name: 'handlebars for partial',
        extensions: ['handlebars', 'hbs'],
        filetypes: ['handlebars_partials'],
        run: run
    });

}

module.exports = processor;
/**
 * Direction event.
 * @class {{classname initialize=false}}
 * @extends {{ns}}
 * @xtype {{xtype}}
 *
 * ### Example
 *
 *      afterrender: function(self) {
 *
 *          self.events.add('hideonscroll', {
 *              xtype: 'events.direction',
 *              el: self.el,
 *              handler: function(direction) {
 *                  if (direction === 'up') {
 *                      self.setVisible(true);
 *                  } else {
 *                      self.setVisible(false);
 *                  }
 *              }
 *          });
 *
 *      }
 */
{{classname}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns,
        _events = new _ns.Event(),
        _attached = false,
        _tid = false,
        _timerInterval = 300, // 300ms
        _previous = false,
        _current = false,
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    function timer() {
        _events.fire('move', ((_previous - _current) > 0 ? 'down' : 'up'));
    }

    function touchstart(event) {
        if (event.touches.length === 1) {
            _previous = event.touches[0].clientY;
            _tid = setInterval(timer, _timerInterval);
        }
    }

    function touchmove(event) {
        if (event.touches.length === 1) {

            if (_current !== false) {
                _previous = _current;
            }
            _current = event.touches[0].clientY;
        }
    }

    function touchend(event) {
        if (_tid !== false) {
            if ((_previous !== _current) &&
                _previous !== false &&
                _current !== false) {
                timer();
            }
            _previous = false;
            _current = false;
            clearInterval(_tid);
            _tid = false;
        }
    }

    function touchcancel(event) {
        if (_tid !== false) {
            clearInterval(_tid);
            _tid = false;
        }
    }

    function mousewheel(event) {
        if (event.wheelDelta >= 0) {
            _events.fire('move', 'up');
        } else {
            _events.fire('move', 'down');
        }
    }

    function _attach() {

        _attached = true;

        if ('ontouchstart' in window) {
            document.addEventListener('touchstart', touchstart, false);
            document.addEventListener('touchmove', touchmove, false);
            document.addEventListener('touchend', touchend, false);
            document.addEventListener('touchcancel', touchcancel, false);
        } else {
            document.addEventListener('mousewheel', mousewheel, false);
        }

    }

    function _detach() {

        _attached = false;

        if ('ontouchstart' in window) {
            document.removeEventListener('touchstart', touchstart, false);
            document.removeEventListener('touchmove', touchmove, false);
            document.removeEventListener('touchend', touchend, false);
            document.removeEventListener('touchcancel', touchcancel, false);
        } else {
            document.removeEventListener('mousewheel', mousewheel, false);
        }

    }

    // public
    return _parent.subclass({

        className: '{{classname}}',
        xtype: '{{xtype}}',

        /**
         * @cfg {Boolean} useCapture (optional) Enable or disable capture for DOM event listener.
         */
        useCapture: false,

        /**
         * @cfg {Boolean} autoAttach (optional) Automaticaly attach event to element.
         */
        autoAttach: false,

        /**
         * @cfg {Object} scope (optional) Callback handler scope.
         * Defaults to event class instance.
         */
        defaultScope: false,

        /**
         * @cfg {Function} handler Callback handler.
         */
        defaultHandler: false,

        /**
         * @cfg {HTMLElement} el (optional) DOM element to attach this event.
         * Defaults to `window`.
         */
        defaultEl: false,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            _ns.utils.apply(self, opts, [
                'useCapture',
                'autoAttach'
            ]);
            self.defaultEl = opts.el || window;
            self.defaultScope = (opts.scope ? opts.scope : self);
            self.defaultHandler = (opts.handler ?
                opts.handler.bind(self.defaultScope) : _noHandler);
            self.attached = false;
            if (self.autoAttach) {
                self.attach();
            }

        },

        /**
         * Attach callback to element event.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration.
         * @param {Function} callback (optional) Callback handler.
         * Defaults to {@link #cfg-handler handler} from configuration.
         * @param {Object} scope (optional) Callback scope.
         * Defaults to {@link #cfg-scope scope} from configuration.
         */
        attach: function(cmp, handler, scope) {

            var self = this;

            if (self.attached === true) {
                return false;
            }
            self.attached = true;
            self.el = cmp || self.defaultEl;
            scope = scope || self.defaultScope;
            self.handler = (handler ?
                handler : self.defaultHandler);
            if (_attached === false) {
                _attach();
            }
            self.eid = _events.on('move', self.handler, scope);
        },

        /**
         * Detach event from element.
         *
         * @param {HTMLElement} el (optional) DOM element.
         * Defaults to {@link #cfg-el el} from configuration or previously attach method call element parameter.
         */
        detach: function(cmp) {

            var self = this;

            if (self.attached === false) {
                return false;
            }
            self.attached = false;

            var count = _events.off(self.eid);

            if (!count) {
                _detach();
            }
        }

    });
})();

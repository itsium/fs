{{ns}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _api,
        _ns = {{ns}},
        _parent = _ns.views.Template,
        _utils = _ns.utils,
        _loaded = false,
        _loading = false,
        _injector = _ns.helpers.Injector,
        _loader = new _ns.Event();

    window.GMAPInit = function() {
        _loaded = true;
        _loading = false;
        _api = google.maps;
        _loader.fireScope('init');
    };

    function init_map(self) {
        self.map = new _api.Map(self.el, self.mapOptions);

        if (self.opts.markers) {

            var len = self.opts.markers.length;

            while (len--) {
                self.addMarker(self.opts.markers[len]);
            }
        }
    }

    function _afterrender(self) {
        if (_loaded === false) {
            _loader.on('init', init_map, self);
        } else {
            init_map(self);
        }
    }

    // public
    return _parent.subclass({

        className: '{{ns}}.{{lowercase me.roc_type}}.{{ucfirst me.name}}',
        xtype: '{{lowercase me.roc_type}}.{{lowercase me.name}}',
        xtpl: 'gmap',
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            self.opts = _utils.applyIf((opts || {}), {
                api: '3.15',
                ak: '8d177402540bc09601c14f705a68c795',
                sensor: 'false'
            }, ['api', 'ak', 'sensor']);

            self.mapOptions = _utils.applyIf((self.opts.mapOptions || {}), {
                zoom: 8
            }, ['zoom']);
            self.markers = [];

            if (_loaded === false && _loading === false) {
                _loading = true;
                _injector.script({
                    src: ('https://maps.googleapis.com/maps/api/js' +
                        '?v=' + self.opts.api +
                        '&sensor=' + self.opts.sensor +
                        //'&ak=' + self.opts.ak +
                        '&callback=GMAPInit'),
                    id: '{{lowercase me.roc_type}}.{{lowercase me.name}}'
                });
            }

            _parent.prototype.constructor.call(self, self.opts);

        },

        addMarker: function(config) {

            var marker,
                point = new _api.LatLng(config.latitude, config.longitude),
                self = this;

            marker = new _api.Marker({
                position: point,
                map: self.map
            });
            self.markers.push(marker);

            // if (config.popup) {
            //     var popup = config.popup,
            //         infosWin = new _api.InfoWindow(popup.html, popup);
            // }

            self.map.setCenter(point);

            return marker;

        }

    });
})();

{{ns}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _api,
        _ns = {{ns}},
        _parent = _ns.views.Template,
        _utils = _ns.utils,
        _loaded = false,
        _injector = _ns.helpers.Injector,
        _loader = new _ns.Event();

    window.baiduMapInit = function() {
        _loaded = true;
        _api = BMap;
        _loader.fireScope('init');
    };

    function init_map(self) {
        self.map = new _api.Map(self.config.id);

        if (self.opts.markers) {

            var len = self.opts.markers.length;

            while (len--) {
                self.addMarker(self.opts.markers[len]);
            }
        }
    }

    function _afterrender(self) {
        if (_loaded === false) {
            _loader.on('init', init_map, self);
        } else {
            init_map(self);
        }
    }

    // public
    return _parent.subclass({

        className: '{{ns}}.{{lowercase me.roc_type}}.{{ucfirst me.name}}',
        xtype: '{{lowercase me.roc_type}}.{{lowercase me.name}}',
        xtpl: 'baidumap',
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            self.opts = _utils.applyIf((opts || {}), {
                api: '2.0',
                ak: '8d177402540bc09601c14f705a68c795'
                //type: 'simple'
            }, ['api', 'ak', 'type']);

            self.markers = [];

            if (_loaded === false) {
                _injector.script({
                    src: ('http://api.map.baidu.com/api' +
                        '?v=' + self.opts.api +
                        '&ak=' + self.opts.ak +
                        (self.opts.type ? ('&type=' + self.opts.type) : '') +
                        '&callback=baiduMapInit'),
                    id: '{{lowercase me.roc_type}}.{{lowercase me.name}}'
                });
            }

            _parent.prototype.constructor.call(self, self.opts);

        },

        addMarker: function(config) {

            var marker,
                point = new _api.Point(config.longitude, config.latitude),
                self = this;

            marker = new _api.Marker(point);
            self.markers.push(marker);

            // if (config.popup) {
            //     var popup = config.popup,
            //         infosWin = new _api.InfoWindow(popup.html, popup);
            // }

            self.map.addOverlay(self.markers[self.markers.length - 1]);
            self.map.centerAndZoom(point, 12);

            return marker;

        }

    });
})();

/**
 * Phonegap exit application on back button pressed.
 */
{{framework.barename}}.helpers.{{ucfirst me.name}} = (function() {

    'use strict';

    var _doc = document,
        _nav = window.navigator;

    function exit() {
        _nav.app.exitApp();
    }

    function ready() {
        _doc.removeEventListener('deviceready', ready, false);
        _doc.addEventListener('backbutton', exit, false);
    }

    _doc.addEventListener('deviceready', ready, false);

    return true;

}());

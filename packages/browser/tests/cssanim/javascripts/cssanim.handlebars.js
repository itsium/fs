{{classname "Browser.tests.Cssanim"}} = (function() {

    'use strict';

    // private
    var _working,
        _ns = {{ns}},
        $ = _ns.Selector,
        _doc = document,
        _animationEnd = $.prefix.jsEvent('animationEnd'),
        _event = new _ns.Event();

    function test_animation(done) {

        var style = _doc.createElement('style'),
            cssPrefix = $.prefix.css;

        style.id = '{{xtype}}-style';

        /* @TODO les animations avec juste un from ne fonctionne pas sur android 4.1 ... */
        style.innerHTML = [
            '@', cssPrefix, 'keyframes cssanim {',
                'from {',
                    cssPrefix, 'transform: translateX(100%);',
                '}',
            '}',

            '@keyframes cssanim {',
                'from {',
                    'transform: translateX(100%);',
                '}',
            '}',

            '.cssanim-test {',
                cssPrefix, 'animation: cssanim 1ms ease both;',
                'animation: cssanim 1ms ease both;',
            '}'
        ].join('');

        // style.innerHTML = [
        //     '@', cssPrefix, 'keyframes cssanim { ',
        //         '0% { ',
        //             cssPrefix, 'transform: translateX(0px);',
        //             'transform: translateX(0px);',
        //         '} ',
        //         '100% { ',
        //             cssPrefix, 'transform: translateX(100px);',
        //             'transform: translateX(100px);',
        //         '} ',
        //     '} ',

        //     '@keyframes cssanim { ',
        //         '0% { ',
        //             cssPrefix, 'transform: translateX(0px);',
        //             'transform: translateX(0px);',
        //         '} ',
        //         '100% { ',
        //             cssPrefix, 'transform: translateX(100px);',
        //             'transform: translateX(100px);',
        //         '} ',
        //     '} ',

        //     '.cssanim-test { ',
        //         cssPrefix, 'animation-name: cssanim;',
        //         cssPrefix, 'animation-timing-function: linear;',
        //         cssPrefix, 'animation-duration: 1ms;',
        //         'animation-name: cssanim;',
        //         'animation-timing-function: linear;',
        //         'animation-duration: 1ms;',
        //     '}'
        // ].join('');

        _doc.head.appendChild(style);

        var el = _doc.createElement('div');
        el.id = '{{xtype}}-el';

        $.css(el, {
            display: 'block',
            background: 'transparent',
            position: 'absolute',
            width: '1px',
            height: '1px',
            bottom: 0,
            left: 0
        });

        _doc.body.appendChild(el);

        var clean = function() {
            if (typeof _working === 'undefined') {
                _working = false;
                _event.fire('working', false);
            }
            el.parentNode.removeChild(el);
            style.parentNode.removeChild(style);
        };

        var success = function() {
            _working = true;
            _event.fire('working', true);
            el.removeEventListener(_animationEnd, success, false);
        };

        el.addEventListener(_animationEnd, success, false);
        el.addEventListener('animationEnd', success, false);
        el.className = 'cssanim-test';
        setTimeout(clean, 750);

    }

    _ns.global.on('ready', test_animation);

    // public
    return function(done) {
        if (typeof _working === 'boolean') {
            return done(_working);
        } else {
            _event.on('working', done);
        }
    };

})();

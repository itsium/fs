/**
 * @class {{ns}}.handlebars.partials.for_id
 *
 * Handlebars `for_id` partial.
 *
 * This partial is used while `label` HTML tag needs to be link to an `id`.
 *
 * ## Sample usage
 *
 * 		\{{> for_id}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *				config: {
 *					label: 'Username',
 *					id: 'username'
 *				}
 *			}
 *		};
 *
 * #### Handlebars template
 *
 *		<label\{{> for_id}}>
 *			\{{label}}:
 *			<input type="text"\{{> id}} />
 *		</label>
 *
 * #### Output
 *
 * 		<label for="username">
 * 			Username:
 * 			<input type="text" id="username" />
 * 		</label>
 *
 */

/**
 * @class {{ns}}.handlebars.partials.css_class
 *
 * Handlebars `css_class` partial.
 *
 * This partial allows you to pass any CSS classes as string from `cssClass` or `css_class` variable inside component configuration.
 *
 * ## Sample usage
 *
 * 		\{{> css_class}}
 * 		\{{! or }}
 * 		\{{> cssClass}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			css_class: "raw css classes"
 *		};
 *
 * #### Handlebars template
 *
 *		<div class="\{{> css_class}}"></div>
 *
 * #### Output
 *
 * 		<div class="raw css classes"></div>
 *
 */
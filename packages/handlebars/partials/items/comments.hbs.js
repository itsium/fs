/**
 * @class {{ns}}.handlebars.partials.items
 *
 * Handlebars `items` partial.
 *
 * This partial is used to push given items from components configuration into your template as HTML.
 *
 * ## Sample usage
 *
 * 		\{{> items}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			config: {
 *				title: 'Profile'
 *			},
 *			items: [{
 *				xtype: 'button',
 *				config: {
 *					text: 'Back',
 *					route: '/home'
 *				}
 *			}]
 *		};
 *
 * #### Handlebars template
 *
 *		<div\{{> id}}>
 *			\{{> items}}
 *			\{{#title}}<h2>\{{.}}</h2>\{{/title}}
 *		</div>
 *
 * #### Output
 *
 * 		<div id="fs1">
 * 			<button id="fs2" href="#/home">Back</button>
 * 			<h2>Profile</h2>
 * 		</div>
 *
 */

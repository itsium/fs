/**
 * @class {{ns}}.handlebars.partials.id
 *
 * Handlebars `id` partial.
 *
 * This partial allows you to pass any id as string from `id` variable inside component configuration.
 *
 * _Note_: You must use this partial in any templates which is link to a component to allow {{ns}} retreive the HTMLElement by its own.
 *
 * ## Sample usage
 *
 *		\{{> id}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			id: "html-element-id"
 *		};
 *
 * #### Handlebars template
 *
 *		<div\{{> id}}></div>
 *
 * #### Output
 *
 * 		<div id="html-element-id"></div>
 *
 */
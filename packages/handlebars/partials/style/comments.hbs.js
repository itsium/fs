/**
 * @class {{ns}}.handlebars.partials.style
 *
 * Handlebars `style` partial.
 *
 * This partial is used to pass inline CSS style for HTMLElement.
 *
 * ## Sample usage
 *
 * 		\{{> style}}
 *
 * ### Example
 *
 * #### Given template configuration (from javascript)
 *
 *		var config = {
 *			config: {
 *				title: 'Profile',
 *				style: 'background: #000; color: #fff;'
 *			}
 *		};
 *
 * #### Handlebars template
 *
 *		<div\{{> id}}\{{> style}}>
 *			\{{#title}}<h2>\{{.}}</h2>\{{/title}}
 *		</div>
 *
 * #### Output
 *
 * 		<div id="fs1" style="background: #000; color: #fff;">
 * 			<h2>Profile</h2>
 * 		</div>
 *
 */

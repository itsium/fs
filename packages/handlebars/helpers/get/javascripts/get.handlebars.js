/**
 * @class {{ns}}.handlebars.helpers.get
 *
 * Handlebars `get` helper.
 *
 * ## Sample usage:
 *
 *      \{{get position default="left" choices="left|right"}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 *      var config = {
 *          hidden: true,
 *          required: undefined,
 *          disabled: false,
 *          iconPos: "right"
 *      };
 *
 * #### Handlebars template
 *
 *      \{{get hidden default=false}}
 *      \{{get required default=false}}
 *      \{{get disabled default=true}}
 *      \{{get iconPos default="left" choices="left|right"}}
 *
 * #### Output
 *
 *      true
 *      false
 *      false
 *      right
 *
 */
(function() {

    var _handlebars = Handlebars;

    var is_choice = function(choices, value) {

        if (typeof choices === 'string') {
            choices = choices.split('|');
            return (choices.indexOf(value) !== -1);
        }
        return false;

    };

    _handlebars.registerHelper('get', function(context, options) {

        var hash = options.hash;

        if (typeof context === 'undefined' ||
            is_choice(hash.choices, context)) {
            return hash['default'];
        }
        return context;
    });

}());

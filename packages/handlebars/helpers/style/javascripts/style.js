(function() {

    var _handlebars = Handlebars;

    /*
     * {{!-- {{style inputStyle}} --}}
     */
    _handlebars.registerHelper('style', function(context, options) {

        if (typeof context !== 'string') {
            if (typeof this.style === 'string') {
                return new _handlebars.SafeString(' style="' + this.style + '"');
            }
            return '';
        }
        return new _handlebars.SafeString(' style="' + context + '"');
    });

}());

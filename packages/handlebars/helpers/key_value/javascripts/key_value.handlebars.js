/**
 * @class {{ns}}.handlebars.helpers.key_value
 *
 * Handlebars `key_value` helper.
 *
 * ## Sample usage:
 *
 *      \{{#key_value people}}
 *         Key: \{{key}} \{{! This is the key of current iteration }}
 *         Value: \{{value}} \{{! This is the value of current iteration }}
 *      \{{/key_value}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 *      var config = {
 *          attrs = {
 *              required: true,
 *              disabled: false,
 *              value: 'test'
 *          }
 *      };
 *
 * #### Handlebars template
 *
 *      \{{#key_value attrs}}
 *          \{{key}}="\{{value}}"
 *      \{{/key_value}}
 *
 * #### Output
 *
 *      required="true"
 *      disabled="false"
 *      value="test"
 *
 */
Handlebars.registerHelper('key_value', function(context, options) {

    var buffer = '',
        key;

    for (key in context) {
        if (context.hasOwnProperty(key)) {
            buffer += options.fn({
                key: key,
                value: context[key]
            });
        }
    }

    return buffer;
});
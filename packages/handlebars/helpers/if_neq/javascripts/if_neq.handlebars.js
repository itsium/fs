/**
 * @class {{ns}}.handlebars.helpers.if_neq
 *
 * Handlebars `if_neq` (if not equals) helper.
 *
 * _Note_: Given `default` value is only used when `value` is undefined.
 *
 * ## Sample usage:
 *
 *      \{{#if_neq value compare=true default=false}}
 *         Value is not true
 *      \{{else}}
 *         Value is true
 *      \{{/if_neq}}
 */
Handlebars.registerHelper('if_neq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context != options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});
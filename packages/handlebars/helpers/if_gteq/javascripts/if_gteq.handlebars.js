/**
 * @class {{ns}}.handlebars.helpers.if_gteq
 *
 * Handlebars `if_gteq` (if greater than or equals) helper.
 *
 * _Note_: Given `default` value is only used when `value` is undefined.
 *
 * ## Sample usage:
 *
 *      \{{#if_gteq value compare=10 default=1}}
 *         Value >= 10
 *      \{{else}}
 *         Value < 10
 *      \{{/if_gteq}}
 */
Handlebars.registerHelper('if_gteq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context >= options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});
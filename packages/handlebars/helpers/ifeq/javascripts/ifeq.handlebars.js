/**
 * @class {{ns}}.handlebars.helpers.ifeq
 *
 * Handlebars `if_eq` (if equals) helper.
 *
 * _Note_: Given `default` value is only used when `value` is undefined.
 *
 * ## Sample usage:
 *
 *      \{{#if_eq value compare=true default=false}}
 *         Value is true
 *      \{{else}}
 *         Value is false or undefined (default)
 *      \{{/if_eq}}
 */
Handlebars.registerHelper('if_eq', function(context, options) {
    if (typeof context === 'undefined') {
        context = options.hash['default'];
    }
    if (context == options.hash.compare) {
        return options.fn(this);
    }
    return options.inverse(this);
});

/**
 * @class {{ns}}.handlebars.helpers.safe
 * @alternateClassName {{ns}}.handlebars.helpers.raw
 *
 * Handlebars `safe`, `raw` helper.
 * By default Handlebars will sanetize HTML from all output.
 *
 * This helper allows you to force Handlebars to output this string as raw.
 *
 * ## Sample usage:
 *
 *		\{{#safe items}}
 *         \{{.}}
 *      \{{/safe}}
 *      \{{! OR }}
 *      \{{safe item}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 * 		var config = {
 * 			html: '<p>test</p>'
 * 		};
 *
 * #### Handlebars template
 *
 * 		\{{html}}
 * 		\{{raw html}}
 * 		\{{safe html}}
 *
 * #### Output
 *
 *      &lt;p&gt;test&lt;/p&gt;
 *      <p>test</p>
 *      <p>test</p>
 *
 */
(function() {

    var handlebars = Handlebars;

    handlebars.registerHelper('safe', function(str) {
        return new handlebars.SafeString(str);
    });

    handlebars.registerHelper('raw', function(str) {
        return new handlebars.SafeString(str);
    });

}());

/**
 * @class {{ns}}.handlebars.helpers.strftime
 *
 * Handlebars `strftime` helper.
 * Allows to easily format date and time.
 *
 * ## Sample usage:
 *
 *      \{{strftime format="%d-%m-%Y" date=date_string_or_javascript_date}}
 *      \{{strftime tformat="short_date" date=date_string_or_javascript_date}}
 *
 * _Note_: `tformat` option requires `{{ns}}.helpers.i18n` package.
 *
 * ### With i18n package:
 *
 *      App.i18n.addLang('cn', {
 *          //...
 *          // date format for strftime
 *          strftime: {
 *
 *              formats: {
 *                  short_date: '%Y年%b%e%a',
 *                  short_datetime: '%Y年%b%e%a %I:%M%p'
 *              },
 *
 *              days: [ '星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六' ],
 *              shortDays: [ '日', '一', '二', '三', '四', '五', '六' ],
 *
 *              months: [ '一月', '二月', '三月', '四月', '五月', '六月', '七月',
 *                '八月', '九月', '十月', '十一月', '十二月' ],
 *
 *              shortMonths: [ '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月',
 *                '9月', '10月', '11月', '12月' ],
 *              AM: '上午',
 *              PM: '下午'
 *          }
 *      });
 *
 *      And then you can use:
 *
 *      \{{strftime date=my_date tformat="short_date"}}
 *
 */
Handlebars.registerHelper('strftime', function(context, options) {

    if (!options) options = context;

    var date = options.hash.date,
        format = options.hash.format;

    if (typeof format === 'undefined' &&
        options.hash.tformat) {
        format = t('strftime').formats[options.hash.tformat];
    }

    if (typeof date === 'string' ||
        typeof date === 'number') {
        date = new Date(date);
    }

    if (typeof t === 'function' &&
        typeof t('strftime') === 'object') {

        var timezone = ((new Date()).getTimezoneOffset()).toString();

        return strftimeTZ(format, date, t('strftime'), timezone);
    } else {
        return strftime(format, date);
    }

});

/**
 * @class {{ns}}.handlebars.helpers.foreach
 *
 * Handlebars `foreach` helper.
 *
 * ## Sample usage:
 *
 *      \{{#foreach people}}
 *         Name: \{{name}} \{{! This show the name of each person inside people }}
 *         Index: \{{$index}} \{{! This is current index (Number) }}
 *         First: \{{$first}} \{{! true when first iteration else false }}
 *         Last: \{{$last}} \{{! true when last iteration else false }}
 *      \{{/foreach}}
 *
 * ### Example:
 *
 * #### Given template configuration (from javascript)
 *
 *      var config = {
 *          people = [{
 *              name: 'John Smith'
 *          }, {
 *              name: 'Hello World'
 *          }, {
 *              name: 'Jessica Alba'
 *          }]
 *      };
 *
 * #### Handlebars template
 *
 *      \{{#foreach people}}
 *          My name is \{{name}}.
 *          I am the index: \{{$index}}
 *          \{{#$first}}
 *              I am the first item !
 *          \{{else}}
 *              \{{#$last}}I am the last item !\{{/$last}}
 *          \{{/$first}}
 *      \{{/foreach}}
 *
 * You can also access to `\{{original}}` object,
 * which contains `people[$index]` data without the `$index`, `$first`, `$last` of foreach
 * (instead of `\{{.}}`).
 *
 * #### Output
 *
 *      My name is John Smith.
 *      I am the index: 0
 *      I am the first item !
 *
 *      My name is Hello World
 *      I am the index: 1
 *
 *      My name is Jessica Alba
 *      I am the index: 2
 *      I am the last item !
 *
 */
Handlebars.registerHelper('foreach', function(arr, options) {

    var length = arr.length;

    if (options.inverse && !length) {
        return options.inverse(this);
    }

    return arr.map(function(item, index) {
        if (typeof item !== 'object') {
            item = {
                original: item
            };
        }
        item.$index = index;
        item.$first = index === 0;
        item.$last  = index === (length - 1);
        return options.fn(item);
    }).join('');

});

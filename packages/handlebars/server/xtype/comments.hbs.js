/**
 * @class {{ns}}.handlebars.server.xtype
 *
 * Handlebars `xtype` server-side helper.
 *
 * You can use this helper directly in {{ns}} source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      \{{xtype}}
 *      \{{xtype "alert"}}
 *      \{{xtype type=false}}
 *      \{{xtype type="views"}}
 *
 * ### Example:
 *
 * #### Given {{ns uppercase=true}} JSON configuration (from package `.{{ns lowercase=true}}.json` file)
 *
 *      {
 *          "name": "alert",
 *          "version": "0.1.0",
 *          "roc_type": "components"
 *      }
 *
 * #### Javascript source file
 *
 *      \{{xtype}}
 *      \{{xtype "alert"}}
 *      \{{xtype type=false}}
 *      \{{xtype type="views"}}
 *
 * #### Output
 *
 *      components.alert
 *      alert
 *      alert
 *      views.alert
 *
 */

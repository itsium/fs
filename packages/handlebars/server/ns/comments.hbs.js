/**
 * @class {{ns}}.handlebars.server.ns
 *
 * Handlebars `ns` (namespace) server-side helper.
 *
 * The namespace is based on the {{ns}} `package.json` file (`name` configuration key value).
 *
 * You can use this helper directly in {{ns}} source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      \{{ns}}
 *      \{{ns lowercase=true}}
 *      \{{ns uppercase=true}}
 *
 * ### Example:
 *
 * #### Javascript source file
 *
 *      \{{ns}}
 *      \{{ns lowercase=true}}
 *      \{{ns uppercase=true}}
 *
 * #### Output
 *
 *      {{ns}}
 *      {{ns lowercase=true}}
 *      {{ns uppercase=true}}
 *
 */

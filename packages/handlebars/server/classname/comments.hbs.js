/**
 * @class {{ns}}.handlebars.server.classname
 *
 * Handlebars `classname` server-side helper.
 *
 * You can use this helper directly in {{ns}} source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      \{{classname}}
 *      \{{classname type=false}}
 *      \{{classname type="views"}}
 *      \{{classname type="views" base="App"}}
 *      \{{classname type="views" base="App" initialize=true}}
 *      \{{classname "App.views.Home"}}
 *
 * ### Example:
 *
 * #### Given {{ns uppercase=true}} JSON configuration (from package `.{{ns lowercase=true}}.json` file)
 *
 *      {
 *          "name": "home",
 *          "version": "0.1.0",
 *          "roc_type": "app"
 *      }
 *
 * #### Javascript source file
 *
 *      \{{classname}}
 *      \{{classname type=false}}
 *      \{{classname type="views"}}
 *      \{{classname type="views" base="App"}}
 *      \{{classname type="views" base="App" initialize=true}}
 *      \{{classname "App.views.Home"}}
 *
 * #### Output
 *
 *      {{ns}}.app.Home
 *      {{ns}}.Home
 *      {{ns}}.views.Home
 *      App.views.Home
 *      if (!window.App) window.App = {}; if (!window.App.views) window.App.views = {}; App.views.Home
 *      App.views.Home
 *
 */

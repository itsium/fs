/**
 * @class {{ns}}.handlebars.server.lowercase
 *
 * Handlebars `lowercase` server-side helper.
 *
 * You can use this helper directly in {{ns}} source code.
 * But before, you have to add `.hbs` or `.handlebars` preprocessor extensions to the source file.
 *
 * Example: 'source.js' -> 'source.hbs.js'
 *
 * ## Sample usage:
 *
 *      \{{lowercase "Hello World"}}
 *
 * ### Example:
 *
 * #### Javascript source file
 *
 *      \{{lowercase "Hello World"}}
 *
 * #### Output
 *
 *      hello world
 *
 */

describe("{{ns}}.views.Template", function() {

	var templateClass = {{ns}}.views.Template;
	var template;

	beforeEach(function() {
		template = undefined;
	});

	afterEach(function() {
		if (template.el) template.remove();
	});

	describe("events implements", function() {

		it("should implements events", function() {
			template = new templateClass({
				implements: ['events']
			});
			expect(template.events).toBeDefined();
		});

		it("should add event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});
			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			expect(evt === template.events.get('test')).toBe(true);
		});

		it("should remove event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});
			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			expect(evt === template.events.get('test')).toBe(true);
			template.events.remove('test');
			expect(template.events.get('test')).toBe(undefined);
		});

		it("should listen event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			spyOn(template.events, 'listen').and.callThrough();

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			expect(template.events.listen).toHaveBeenCalled();
		});

		it("should unlisten event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			spyOn(template.events, 'unlisten').and.callThrough();

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			template.events.remove('test');
			expect(template.events.unlisten).toHaveBeenCalled();
		});

		it("should attach event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			spyOn(evt, 'attach').and.callThrough();
			template.events.attach();
			expect(evt.attach).toHaveBeenCalled();
		});

		it("should detach event", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['events'],
				renderTo: 'body'
			});

			var evt = template.events.add('test', {
				xtype: 'events.click',
				el: template.el,
				handler: function() {}
			});
			spyOn(evt, 'detach').and.callThrough();
			template.events.attach();
			template.events.detach();
			expect(evt.detach).toHaveBeenCalled();
		});

	});

	describe("update implements", function() {

		it("should implements update", function() {
			template = new templateClass({
				implements: ['update']
			});
			expect(typeof template.update).toBe('function');
		});

		it("should update HTML", function() {
			template = new templateClass({
				xtpl: 'test',
				implements: ['update'],
				items: 'toto'
			});

			template.compile();
			template.render('body');
			expect(template.html).toContain('toto');

			template.update({}, ['tata']);
			expect(template.html).toContain('tata');
			expect(template.html).not.toContain('toto');
		});

	});

	describe("templating", function() {

		it("should compile template", function() {

			template = new templateClass({
				xtpl: 'test'
			});

			var html = template.compile();

			expect(typeof html).toBe('string');
			expect(html.length).toBeGreaterThan(25);
			expect(html).toEqual(template.html);
		});

		it("should not compile if no template given", function() {
			template = new templateClass();

			var compile = function() {
				template.compile();
			};

			expect(compile).toThrowError("no template given");
		});

		it("should render given template", function() {
			template = new templateClass({
				xtpl: 'test',
				config: {
					id: 'test-body-rendering'
				},
				renderTo: 'body'
			});
			expect(document.getElementById('test-body-rendering')).toBe(template.el);
		});

		it("should remove element", function() {
			template = new templateClass({
				xtpl: 'test',
				config: {
					id: 'test-remove-element'
				},
				renderTo: 'body'
			});
			expect(document.getElementById('test-remove-element')).toBe(template.el);
			template.remove();
			expect(document.getElementById('test-remove-element')).toBe(null);
			expect(template.el).toBe(undefined);
		});

	});

	describe("configuration", function() {

		it("items should contains default", function() {

			template = new templateClass({
				xtpl: 'test',
				defaults: {
					items: 'default items value'
				},
				items: [{
					xtpl: 'test'
				}]
			});

			var html = template.compile();

			expect(html).toContain('default items value');

		});

		it("should have access to reference", function() {

			template = new templateClass({
				xtpl: 'test',
				items: {
					xtpl: 'test',
					ref: 'referenceTest'
				}
			});

			template.compile();

			expect(template.referenceTest).toBeDefined();

		});

		it("should have reference in scope", function() {

			var scope = {};

			template = new templateClass({
				xtpl: 'test',
				items: {
					xtpl: 'test',
					refScope: scope,
					ref: 'referenceTest'
				}
			});

			template.compile();

			expect(scope.referenceTest).toBeDefined();
			expect(template.referenceTest).not.toBeDefined();

		});

		it("should render to given (string) element", function() {
			template = new templateClass({
				xtpl: 'test',
				renderTo: 'body'
			});
			expect(template.el).toBeDefined();
		});

		it("should render to given (HTML) element", function() {
			template = new templateClass({
				xtpl: 'test',
				renderTo: document.body
			});
			expect(template.el).toBeDefined();
		});

	});

});
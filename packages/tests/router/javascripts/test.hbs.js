describe("{{ns}}.Router", function() {

	var j = jasmine,
		routerClass,
		withArgs,
		withoutArgs,
		noMatchDefault,
		withBefore,
		isBefore,
		mustFailBefore,
		withBeforeButFail,
		//default_interval = jasmine.DEFAULT_TIMEOUT_INTERVAL,
		history = {{ns}}.History;

	beforeEach(function() {

		history.reset(true);

		withArgs = j.createSpy('withArgs');
		withoutArgs = j.createSpy('withoutArgs');
		noMatchDefault = j.createSpy('noMatchDefault');
		withBefore = j.createSpy('withBefore');
		isBefore = j.createSpy('isBefore');
		mustFailBefore = j.createSpy('mustFailBefore');
		withBeforeButFail = j.createSpy('withBeforeButFail');

		routerClass = {{ns}}.extend('router', {
			routeNoMatch: 'routeNoMatchDefault',
			before: {
				'^routeWithBefore$': 'mustCalledBefore',
				'^routeWithBeforeButFail$': 'mustFailInThisBefore'
			},
			routes: {
				'/without_args': 'routeWithoutArgs',
				'/with_args/:first/:second': 'routeWithArgs',
				'/before': 'routeWithBefore',
				'/fail_before': 'routeWithBeforeButFail'
			},
			routeWithBeforeButFail: function() {
				withBeforeButFail();
			},
			mustFailInThisBefore: function(ok) {
				mustFailBefore();
				return false;
			},
			mustCalledBefore: function(ok) {
				isBefore();
				ok();
			},
			routeWithBefore: function() {
				withBefore();
			},
			routeNoMatchDefault: function() {
				noMatchDefault();
			},
			routeWithoutArgs: function() {
				withoutArgs();
			},
			routeWithArgs: function(first, second) {
				withArgs(first, second);
			}
		});

	});

	it("should call no match default route", function(done) {

		var router = new routerClass();

		history.go('/unknown', function() {
			expect(noMatchDefault).toHaveBeenCalled();
			done();
		});

	});

	it("should call route without arguments", function(done) {

		var router = new routerClass();

		history.go('/without_args', function() {
			expect(withoutArgs).toHaveBeenCalled();
			done();
		});
	});

	it("should call route with arguments", function(done) {

		var router = new routerClass();

		history.go('/with_args/abcd/1234', function() {
			expect(withArgs).toHaveBeenCalledWith('abcd', '1234');
			done();
		});
	});

	it("should call before with route", function(done) {

		var router = new routerClass();

		history.go('/before', function() {
			expect(withBefore).toHaveBeenCalled();
			expect(isBefore).toHaveBeenCalled();
			done();
		});

	});

	xit("should call before without route", function(done) {

		// test fail i dont know why

		var router = new routerClass();

		history.go('/fail_before', function() {
			expect(mustFailBefore).toHaveBeenCalled();
			expect(withBeforeButFail).not.toHaveBeenCalled();
			done();
		});

	});

});

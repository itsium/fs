describe("{{ns}}.Request", function() {

	var request = {{ns}}.Request;

	describe("ajax", function() {

		it("should fetch given url", function(done) {
			request.ajax('resources/ajax.json', {
				callback: function(success, response) {
					expect(success).toBe(true);
					expect(typeof response).toEqual('string');

					var data = JSON.parse(response);

					expect(data.success).toEqual(true);
					done();
				}
			});
		});

		it("should return success false", function(done) {
			request.ajax('resources/unknown', {
				callback: function(success) {
					expect(success).toBe(false);
					done();
				}
			});
		});

	});

	describe("jsonp", function() {

		it("should fetch given url", function(done) {
			request.jsonp('resources/jsonp.json', {
				callbackName: 'jsonpTest',
				callback: function(success, data) {
					expect(success).toBe(true);
					expect(typeof data).toEqual('object');
					expect(data.success).toEqual(true);
					done();
				}
			});
		});

		it("should return success false", function(done) {
			request.jsonp('resources/unknown', {
				callback: function(success) {
					expect(success).toBe(false);
					done();
				}
			});
		});

	});

});

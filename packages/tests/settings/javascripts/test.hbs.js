describe("{{ns}}.Settings", function() {

	var settingsClass = {{ns}}.Settings;

	describe("without storage", function() {

		var settings;

		beforeEach(function() {
			settings = undefined;
		});

		it("should get value from known name", function() {
			settings = new settingsClass({
				is_correct: true
			});
			expect(settings.get('is_correct')).toBe(true);
			expect(settings.get('is_correct', 'default')).toBe(true);
		});

		it("should get default value from unknown name", function() {
			settings = new settingsClass({
				is_not_correct: false
			});
			expect(settings.get('is_correct', 'default')).toBe('default');
			expect(settings.get('is_correct', true)).toBe(true);
		});

		it("should get undefined from unknown name without default value", function() {
			settings = new settingsClass({
				is_not_correct: false
			});
			expect(settings.get('is_correct')).not.toBeDefined();
		});

		it("should get multiple known values", function() {
			settings = new settingsClass({
				one: 'One',
				two: 'Two',
				three: 'Three'
			});
			expect(settings.gets('one', 'two', 'three').join(', ')).toBe('One, Two, Three');
		});

		it("should pop value from name", function() {
			settings = new settingsClass({
				is_still_here: true
			});
			expect(settings.pop('is_still_here')).toBe(true);
			expect(settings.pop('is_still_here')).not.toBe(true);
			expect(settings.pop('is_still_here')).not.toBeDefined();
		});

		it("should pop value from name with default value", function() {
			settings = new settingsClass({
				is_still_here: true
			});
			expect(settings.pop('is_still_here', 'default')).toBe(true);
			expect(settings.pop('is_still_here', 'default')).toBe('default');
		});

		it("should empty given name value", function() {
			settings = new settingsClass({
				one: true,
				two: true
			});
			expect(settings.get('one')).toBe(true);
			expect(settings.get('two')).toBe(true);
			settings.empty('one');
			expect(settings.get('one')).not.toBe(true);
			expect(settings.get('one')).not.toBeDefined();
			expect(settings.get('two')).toBe(true);
		});

		it("should fire empty events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('oneempty', function(_, name, oldValue) {
				console.log('empty: ', arguments);
				expect(name).toBe('one');
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.on('onechanged', function(_, name, value, oldValue) {
				console.log('changed: ', arguments);
				expect(name).toBe('one');
				expect(value).not.toBeDefined();
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.empty('one');
			expect(calls).toEqual(2);
		});

		it("should not fire empty events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('oneempty', function(_, name, oldValue) {
				console.log('empty: ', arguments);
				++calls;
			});
			settings.on('onechanged', function(_, name, value, oldValue) {
				console.log('changed: ', arguments);
				++calls;
			});
			settings.empty('one', true);
			expect(calls).toEqual(0);
		});

		it("should set value to given name", function() {
			settings = new settingsClass({
				one: true
			});
			expect(settings.get('one')).toBe(true);
			settings.set('one', false);
			expect(settings.get('one')).toBe(false);
		});

		it("should fire changed events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('settingchanged', function(_, name, value, oldValue) {
				expect(name).toBe('one');
				expect(value).toBe(false);
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.on('onechanged', function(_, name, value, oldValue) {
				expect(name).toBe('one');
				expect(value).toBe(false);
				expect(oldValue).toBe(true);
				++calls;
			});
			settings.set('one', false);
			expect(calls).toEqual(2);
		});

		it("should not fire changed events", function() {

			var calls = 0;

			settings = new settingsClass({
				one: true
			});
			settings.on('settingchanged', function() {
				++calls;
			});
			settings.on('onechanged', function() {
				++calls;
			});
			settings.set('one', false, true);
			expect(calls).toEqual(0);
		});

	});

});

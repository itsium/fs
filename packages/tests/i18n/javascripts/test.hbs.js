describe("{{ns}}.helpers.i18n", function() {

	var i18nClass = {{ns}}.helpers.i18n,
		i18n;

	beforeEach(function() {
		i18n = undefined;
	});

	it("should translate given words", function() {
		i18n = new i18nClass();
		i18n.addLang('test', {
			'OK': 'Alright',
			'1': true
		}, true);
		expect(t('OK')).toEqual('Alright');
		expect(t('1')).toBe(true);
	});

	it("should change current language", function() {
		i18n = new i18nClass();
		i18n.addLang('test2', {
			'OK': 'Alright'
		});
		i18n.addLang('test3', {
			'OK': 'Good'
		});
		i18n.setLang('test2');
		expect(t('OK')).toEqual('Alright');
		i18n.setLang('test3');
		expect(t('OK')).toEqual('Good');
	});

});

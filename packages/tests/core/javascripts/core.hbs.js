describe("{{ns}}", function() {

	var coreClass = {{ns}};
	var cls;

	beforeEach(function() {
		cls = undefined;
	});

	describe("subclass", function() {

		it("should has subclass method", function() {

			var classA = coreClass.subclass();

			expect(typeof classA.subclass).toBe('function');
		});

		it("should herits from attributes", function() {

			var classA = coreClass.subclass({
				exists: true,
				will_override: false,
				not_override: true
			});
			var classB = classA.subclass({
				will_override: true
			});
			var instance = new classB();

			expect(classA.prototype.will_override).toBe(false);
			expect(classB.prototype.will_override).toBe(true);
			expect(instance.will_override).toBe(true);
			expect(instance.exists).toBe(true);
			expect(instance.not_override).toBe(true);

		});

		it("should call parent constructor", function() {

			var classA = coreClass.subclass({

				parent_called: false,

				constructor: function() {
					this.parent_called = true;
				}
			});
			var classB = classA.subclass({
				constructor: function() {
					classA.prototype.constructor.apply(this, arguments);
				}
			});
			var instance = new classB();

			expect(instance.parent_called).toBe(true);

		});

		it("should be able to have multiple herits", function() {

			var classA = coreClass.subclass({
				still_there: true,
				from_who: function() {
					return 'A';
				}
			});
			var classB = classA.subclass({
				from_who: function() {
					return 'B';
				}
			});
			var classC = classB.subclass({
				from_who: function() {
					return 'C';
				}
			});
			var instance = new classC();

			expect(instance.still_there).toBe(true);
			expect(instance.from_who()).toBe('C');

		});

		it("should register with xtype", function() {

			var classA = coreClass.subclass({
				xtype: 'test-xtype',
				my_super_class: true
			});
			var _classA = coreClass.xtype('test-xtype');

			expect(classA).toBe(_classA);
			expect(_classA.prototype.my_super_class).toBe(true);

		});

		it("should create from xtype", function() {

			var classA = coreClass.subclass({
				xtype: 'test-xtype2',
				my_super_class: true
			});
			var instance = coreClass.create('test-xtype2');

			expect(instance.my_super_class).toBe(true);

		});

		it("should herits with extend", function() {

			var classA = coreClass.extend(coreClass, {
				xtype: 'herits-with-extend',
				has_extend: false
			});
			var classB = coreClass.extend('herits-with-extend', {
				xtype: 'herits-with-extend2',
				has_extend: true
			});
			var instance = coreClass.create('herits-with-extend2');

			expect(instance.has_extend).toBe(true);
		});

		it("should not extend from unknown xtype/class", function() {

			expect(function() {
				coreClass.extend('unknown');
			}).toThrowError('Class or xtype does not exists.');

			expect(function() {
				coreClass.extend({});
			}).toThrowError('Class or xtype does not exists.');

		});

		it("should have parent attribute", function() {

			var classA = coreClass.extend(coreClass, {
				is_base_class: true
			});

			var classB = coreClass.extend(classA, {
				is_base_class: false
			});

			expect(classB.parent.is_base_class).toBe(true);

		});

	});

});

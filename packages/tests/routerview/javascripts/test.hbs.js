describe("{{ns}}.RouterView", function() {

	var j = jasmine,
		routerClass,
		router,
		view1,
		view2,
		history = {{ns}}.History;

	{{ns}}.extend('template', {
		xtype: 'view1',
		config: {
			xtpl: 'container',
			config: {
				style: 'display: none'
			},
			items: 'view 1'
		}
	});

	{{ns}}.extend('template', {
		xtype: 'view2',
		config: {
			xtpl: 'container',
			config: {
				style: 'display: none'
			},
			items: 'view 2'
		}
	});

	beforeEach(function() {

		history.reset(true);

		view1 = j.createSpy('view1');
		view2 = j.createSpy('view2');

		routerClass = {{ns}}.extend('routerview', {
			routes: {
				'/set_current_view/view1': 'routeView1',
				'/set_current_view/view2': 'routeView2'
			},
			routeView1: function() {
				view1();
				this.setCurrentView('view1');
			},
			routeView2: function() {
				view2();
				this.setCurrentView('view2');
			}
		});

		router = new routerClass();

	});

	afterEach(function() {
		history.reset(true);
	});

	it("should set view", function() {
		router.setCurrentView('view1');
		expect(router.currentView.xtype).toEqual('view1');
	});

	it("should hide current view", function() {
		router.setCurrentView('view1');
		expect(router.currentView.xtype).toEqual('view1');
		router.hideCurrentView();
		expect(router.currentView.hidden).toBe(true);
	});

	it("should hide previous view", function() {
		router.setCurrentView('view1');
		expect(router.currentView.xtype).toEqual('view1');

		var previousView = router.getPreviousView();

		router.setCurrentView('view2');
		expect(router.currentView.xtype).toEqual('view2');
		router.hidePreviousView();
		expect(router.currentView.hidden).toBe(false);
		expect(previousView.hidden).toBe(true);
	});

	it("should get view from given xtype", function() {
		expect(router.getView('view1').xtype).toEqual('view1');
		expect(router.getView('view2').xtype).toEqual('view2');
	});

});

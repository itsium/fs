describe("{{ns}}.Event", function() {

	var eventClass = {{ns}}.Event;
	var evt;

	beforeEach(function() {
		evt = undefined;
	});

	describe("events", function() {

		it("should listen event", function() {

			var called = false;

			evt = new eventClass();
			evt.on('test', function() {
				called = true;
			});
			evt.fire('test');
			expect(called).toBe(true);
		});

		it("should unlisten event", function() {

			var nb_call = 0;

			evt = new eventClass();

			var eid = evt.on('test', function() {
				++nb_call;
			});

			evt.fire('test');
			evt.off(eid);
			evt.fire('test');
			expect(nb_call).toEqual(1);

		});

		it("should count listeners", function() {
			evt = new eventClass();
			expect(evt.has('test')).toEqual(0);
			evt.on('test', function() {
				called = true;
			});
			expect(evt.has('test')).toBe(1);
		});

		it("should prevent next listener to be fired", function() {

			var nb_call = 0;

			evt = new eventClass();
			evt.on('test', function() {
				++nb_call;
				return false;
			});
			evt.on('test', function() {
				++nb_call;
			});
			evt.fire('test');
			expect(nb_call).toEqual(1);
		});

		it("should remove listener inside listener", function() {

			var nb_call = 0;

			evt = new eventClass();
			var uid = evt.on('test', function() {
				++nb_call;
				evt.off(uid);
			});
			evt.fire('test');
			evt.fire('test');
			expect(nb_call).toEqual(1);
		});

		it("should fire with priority", function() {

			var first = true;

			evt = new eventClass();
			evt.on('test', function() {
				expect(first).toBe(false);
			});
			evt.on('test', function() {
				expect(first).toBe(true);
				first = false;
			}, this, 'CORE');
			evt.fire('test');
		});

	});

	describe("listeners", function() {

		it("should fire event", function() {

			var nb_call = 0;

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					test: function() {
						++nb_call;
					}
				}
			});
			evt.fire('test');
			expect(nb_call).toEqual(1);
		});

		it("should not listen event without hasListeners", function(done) {

			var called = false;

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					test2: function() {
						called = true;
					},
					test: function() {
						expect(called).toBe(false);
						done();
					}
				}
			});
			evt.fire('test2');
			evt.fire('test');
		});

		it("should fire with scope as first argument", function(done) {

			var test_scope = {
				test: true
			};

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					scope: test_scope,
					test: function(scope, value) {
						expect(scope.test).toBe(true);
						done();
					}
				}
			});
			evt.fireScope('test', true);
		});

		it("should fire with given scope", function(done) {

			var test_scope = {
				test: true
			};

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					scope: test_scope,
					test: function(scope, value) {
						expect(this.test).toBe(true);
						done();
					}
				}
			});
			evt.fireScope('test', true);
		});

		it("should fire with priority", function(done) {

			var first = true;

			evt = new eventClass({
				hasListeners: ['test'],
				listeners: {
					priority: 'VIEWS',
					test: function() {
						expect(first).toBe(false);
						done();
					}
				}
			});
			evt.on('test', function() {
				expect(first).toBe(true);
				first = false;
			}, this, 'CORE');
			evt.fire('test');
		});

		it("should has or not listener", function() {
			evt = new eventClass({
				hasListeners: ['exists'],
				listeners: {
					exists: function() {}
				}
			});
			expect(evt.hasListener('exists')).toBe(true);
			expect(evt.hasListener('does_not_exist')).toBe(false);
		});

		it("should return listener", function() {
			evt = new eventClass({
				hasListeners: ['exists'],
				listeners: {
					exists: function() {}
				}
			});
			expect(evt.getListener('exists')).toBeDefined();
		});

	});

});
describe("{{ns}}.Selector", function() {

	var $ = {{ns}}.Selector;

	describe("DOM", function() {

		var el;

		beforeEach(function() {
			el = document.createElement('div');
			el.id = 'tests-selector-el';
			document.body.appendChild(el);
		});

		afterEach(function() {
			el.parentNode.removeChild(el);
		});

		it("should add class", function() {
			$.addClass(el, 'test');
			expect(el.className).toContain('test');
		});

		it("should remove class", function() {
			$.addClass(el, 'test');
			expect(el.className).toContain('test');
			$.removeClass(el, 'test');
			expect(el.className).not.toContain('test');
		});

		it("should have css class", function() {
			$.addClass(el, 'test');
			expect($.hasClass(el, 'test')).toBe(true);
		});

		it("should add multiple css classes", function() {
			$.addClasses(el, 'test1 test2 test3');
			expect(el.className).toContain('test1');
			expect(el.className).toContain('test2');
			expect(el.className).toContain('test3');
		});

		it("should remove multiple css classes", function() {
			$.addClasses(el, 'test1 test2 test3');
			expect(el.className).toContain('test1');
			expect(el.className).toContain('test2');
			expect(el.className).toContain('test3');
			$.removeClasses(el, 'test1 test2 test3');
			expect(el.className).not.toContain('test1');
			expect(el.className).not.toContain('test2');
			expect(el.className).not.toContain('test3');
		});

		it("should have multiple css classes", function() {
			$.addClasses(el, 'test1 test2 test3');
			expect($.hasClasses(el, 'test1 test2 test3')).toBe(true);
		});

		it("should get element from selector", function() {
			expect($.get('#tests-selector-el')).toBe(el);
		});

		it("should get element from selector with root", function() {
			expect($.get('#tests-selector-el', document.body)).toBe(el);
		});

		it("should set css properties to element", function() {
			$.css(el, {
				'background-image': 'none',
				backgroundColor: 'white',
				color: 'black',
				transform: 'rotateX(40deg)'
			});
			expect(el.style.color).toEqual('black');
			expect(el.style.backgroundColor).toEqual('white');
			expect(el.style.backgroundImage).toEqual('none');
			expect(el.style[$.prefix.cssStyle + 'Transform']).toEqual('rotateX(40deg)');
		});

		it("should remove element", function() {

			var el2 = document.createElement('div');

			el2.id = 'tests-selector-el2';
			document.body.appendChild(el2);
			expect($.get('#tests-selector-el2')).not.toBe(null);
			$.removeEl(el2);
			expect($.get('#tests-selector-el2')).toBe(null);
		});

		it("should add HTML to element", function() {
			expect(el.innerText).toBe('');
			$.addHtml(el, '<i>test</i>');
			expect(el.innerText).toBe('test');
			expect(el.innerHTML).toBe('<i>test</i>');
		});

		it("should update HTML from element", function() {
			expect(el.innerText).toBe('');
			$.updateHtml(el, '<i>test</i>');
			expect(el.innerText).toBe('test');
			expect(el.innerHTML).toBe('<i>test</i>');
		});

		it("should remove HTML from element", function() {
			el.innerHTML = 'test';
			expect(el.innerText).toBe('test');
			$.removeHtml(el);
			expect(el.innerText).toBe('');
		});

		it("should find parent from callback", function() {

			el.innerHTML = '<div class="parent-we-wanna-find">' +
				'<ul><li></li><li><a href="" class="start-from-here">test</a></li></ul></div>';

			var startEl = $.get('.start-from-here', el);
			var parentEl = $.findParent(startEl, function(el) {
				if ($.hasClass(el, 'parent-we-wanna-find')) return el;
				return false;
			});

			expect($.hasClass(parentEl, 'parent-we-wanna-find')).toBe(true);
		});

		it("should find parent from tag", function() {

			el.innerHTML = '<div class="parent-we-wanna-find">' +
				'<ul><li></li><li><a href="" class="start-from-here">test</a></li></ul></div>';

			var startEl = $.get('.start-from-here', el);
			var parentEl = $.findParentByTag(startEl, 'div');

			expect($.hasClass(parentEl, 'parent-we-wanna-find')).toBe(true);
		});

		it("should find parent from css class", function() {

			el.innerHTML = '<div class="parent-we-wanna-find">' +
				'<ul><li></li><li><a href="" class="start-from-here">test</a></li></ul></div>';

			var startEl = $.get('.start-from-here', el);
			var parentEl = $.findParentByClass(startEl, 'parent-we-wanna-find');

			expect($.hasClass(parentEl, 'parent-we-wanna-find')).toBe(true);
		});

	});

});

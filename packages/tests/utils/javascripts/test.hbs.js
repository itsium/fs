describe("{{ns}}.utils", function() {

	var utils = {{ns}}.utils;

	describe("apply if", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should not apply if key exists but value is defined", function() {

			var result = utils.applyIf(to_override, values, ['a']);

			expect(result.a).toBe(true);
		});

		it("should apply if key exists and value undefined", function() {

			var result = utils.applyIf(to_override, values, ['c']);

			expect(result.c).toEqual('ok');
		});

		it("should keep untouched values", function() {

			var result = utils.applyIf(to_override, values, ['a', 'b']);

			expect(result.not_in_keys).toEqual(true);
		});

		it("should not apply if key not given", function() {

			var result = utils.applyIf(to_override, values, ['a']);

			expect(result.c).not.toBeDefined();
		});

	});

	describe("apply if auto", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should not apply if value is defined", function() {

			var result = utils.applyIfAuto(to_override, values);

			expect(result.a).toBe(true);
		});

		it("should apply if new value", function() {

			var result = utils.applyIfAuto(to_override, values);

			expect(result.b).toEqual(false);
		});

		it("should keep untouched values", function() {

			var result = utils.applyIfAuto(to_override, values);

			expect(result.not_in_keys).toEqual(true);
		});

	});

	describe("apply", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should apply even if value is defined", function() {

			var result = utils.apply(to_override, values, ['a']);

			expect(result.a).toBe(false);
		});

		it("should apply if new value", function() {

			var result = utils.apply(to_override, values, ['c']);

			expect(result.c).toEqual('ok');
		});

		it("should keep untouched values", function() {

			var result = utils.apply(to_override, values, ['a', 'b']);

			expect(result.not_in_keys).toEqual(true);
		});

		it("should not apply if keys not here", function() {

			var result = utils.apply(to_override, values, ['a']);

			expect(result.c).not.toBeDefined();
		});

	});

	describe("applyAuto", function() {

		var to_override, values;

		beforeEach(function() {
			to_override = {
				a: true,
				not_in_keys: true,
				b: false
			};
			values = {
				a: false,
				c: 'ok'
			};
		});

		it("should apply even if value is defined", function() {

			var result = utils.applyAuto(to_override, values);

			expect(result.a).toBe(false);
		});

		it("should apply if new value", function() {

			var result = utils.applyAuto(to_override, values);

			expect(result.c).toEqual('ok');
		});

		it("should keep untouched values", function() {

			var result = utils.applyAuto(to_override, values);

			expect(result.not_in_keys).toEqual(true);
		});

	});

	describe("arraySlice", function() {

		var array;

		beforeEach(function() {
			array = [1, 2, 3, 4, 5];
		});

		it("should copy array", function() {

			var result = utils.arraySlice(array);

			array[0] = 1000;
			expect(result[0]).toEqual(1);
			expect(result[1]).toEqual(2);
			expect(result[2]).toEqual(3);
			expect(result[3]).toEqual(4);
			expect(result[4]).toEqual(5);
		});

		it("should copy array from second element", function() {

			var result = utils.arraySlice(array, 1);

			array[0] = 1000;
			expect(result[0]).toEqual(2);
			expect(result[1]).toEqual(3);
			expect(result[2]).toEqual(4);
			expect(result[3]).toEqual(5);
		});

		it("should copy array from third element to fourth element", function() {

			var result = utils.arraySlice(array, 2, 3);

			array[0] = 1000;
			expect(result[0]).toEqual(3);
			expect(result[1]).toEqual(4);
		});

		it("should copy from 0 with < 0 start", function() {

			var result = utils.arraySlice(array, -10);

			expect(result[0]).toEqual(1);
			expect(result[1]).toEqual(2);
			expect(result[2]).toEqual(3);
			expect(result[3]).toEqual(4);
			expect(result[4]).toEqual(5);
		});

		it("should copy from length with > length start", function() {

			var result = utils.arraySlice(array, 10);

			expect(result.length).toEqual(0);
		});

		it("should copy to 0 with < 0 end", function() {

			var result = utils.arraySlice(array, 0, -10);

			expect(result.length).toEqual(0);
		});

		it("should copy until length with > length end", function() {

			var result = utils.arraySlice(array, 0, 10);

			expect(result[0]).toEqual(1);
			expect(result[1]).toEqual(2);
			expect(result[2]).toEqual(3);
			expect(result[3]).toEqual(4);
			expect(result[4]).toEqual(5);
		});

	});

	it("should capitalize", function() {
		expect(utils.capitalize('test')).toEqual('Test');
		expect(utils.capitalize('0123')).toEqual('0123');
	});

	it("should not capitalize non string", function() {

		var test = false;

		expect(utils.capitalize(test)).toBe(false);
	});

	it("should crc32", function() {
		expect(utils.crc32('test')).toEqual(-662733300);
		expect(utils.crc32('test', 1000)).toEqual(-906843774);
	});

});

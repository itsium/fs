describe("{{ns}}.History", function() {

	var history = {{ns}}.History;

	beforeEach(function() {
		window.location.hash = '';
		history.reset(true);
	});

	it("should set default route", function() {
		history.setDefaultRoute('/test/default/route');
		expect(history.defaultRoute).toBe('/test/default/route');
	});

	it("should stop history", function() {
		history.stop();
		expect(history.started).toBe(false);
	});

	it("should start history", function() {
		history.start();
		expect(history.started).toBe(true);
	});

	it("should go to location", function() {
		history.go('/test/route');
		expect(window.location.hash).toContain('/test/route');
	});

	it("should get current location", function() {
		history.go('/test/route');
		expect(history.here()).toContain('/test/route');
	});

	it("should get urlencoded current location", function() {
		history.go('/test/route');
		expect(history.here(true)).toContain('%2Ftest%2Froute');
	});

	it("should get given index url", function(done) {
		history.go('/another/url');
		setTimeout(function() {
			history.go('/again/route');
			setTimeout(function() {
				expect(history.get(-1)).toContain('/another/url');
				expect(history.get(0)).toContain('/again/route');
				done();
			}, 10);
		}, 10);
	});

	it("should back to previous url", function(done) {
		history.go('/back1');
		setTimeout(function() {
			expect(history.here()).toContain('/back1');
			history.go('/back2');
			setTimeout(function() {
				expect(history.here()).toContain('/back2');
				history.back(-1);
				setTimeout(function() {
					expect(history.here()).toContain('/back1');
					done();
				}, 10);
			}, 10);
		}, 10);
	});

	it("should call route callback", function(done) {

		var nb_call = 0;

		history.route('', new RegExp('^/the/route$'), function() {
			++nb_call;
		});
		history.go('/the/route');
		setTimeout(function() {
			expect(nb_call).toBeGreaterThan(0);
			done();
		}, 10);

	});

});

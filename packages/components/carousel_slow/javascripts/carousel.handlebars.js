{{ns}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _win = window,
        _touchSlider = _win.touchSlider,
        _parent = _ns.views.Template;

    function _afterrender(self) {

        var opts = {
            el: self.el
            // afterslide: function() {}
        };

        if (typeof self.height !== 'object') {
            opts.height = self.height;
        }

        if (self.animation) {
            opts.duration = self.animation;
            opts.afterslide = function() {
                if (self.stopAuto !== true && self.hidden === false) {
                    setTimeout(function() {
                        if (self.stopAuto !== true && self.hidden === false) {
                            self.api.next();
                        }
                    }, 2000);
                }
            };
            opts.userslide = function() {
                self.stopAuto = true;
            };
        }

        self.api = _touchSlider(opts);

    }

    function _onshow(self) {

        self.hidden = false;
        self.api.resume();

        if (self.stopAuto !== true && self.animation) {
            var sid = setTimeout(function() {
                self.api.next();
                clearTimeout(sid);
            }, 2500);
        }

    }

    function _onhide(self) {
        self.hidden = true;
        self.api.pause();
    }

    function _aftercompile(self, renderer) {

        if (self.showEID) {
            renderer.off(self.showEID);
        }
        if (self.hideEID) {
            renderer.off(self.hideEID);
        }

        self.off(self.eid);

        self.arEID = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);

        self.showEID = renderer.on('show', _onshow,
            self, self.priority.VIEWS);
        self.hideEID = renderer.on('hide', _onhide,
            self, self.priority.VIEWS);

    }

    // public
    return _parent.subclass({

        className: '{{ns}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            opts = opts || { config: {}};
            opts.config = opts.config || {};

            if (opts.config.animation) {
                self.animation = opts.config.animation;
            }

            if (opts.config.canFullscreen) {
                self.canFullscreen = opts.config.canFullscreen;
            }

            if (opts.config.height) {
                self.height = opts.config.height;
            }

            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        }
    });
}());
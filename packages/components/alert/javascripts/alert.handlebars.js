/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Alert message box component.
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new {{ns}}.views.Alert({
 *          config: {
 *              title: 'Error',
 *              message: 'Something went wrong'
 *          }
 *      });
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    function _afterrender(self) {

        var id = self.config.id;

        self.overlayEl = document.getElementById(id + '-overlay');
        self.closeEl = document.getElementById(id + '-close');

        if (self.config.overlayHide !== false) {
            self.events.add('overlay-click-hide', {
                xtype: 'events.click',
                el: self.overlayEl,
                scope: self,
                handler: self.hide
            });
        }

        if (self.closeEl) {
            self.events.add('close-click', {
                xtype: 'events.click',
                disabledScrolling: true,
                el: self.closeEl,
                scope: self,
                handler: self.hide
            });
        }
    }

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype type=false}}',
        hasListeners: [
            'show',
            'hide'
        ],
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        /**
         * Show alert.
         * @fires show
         */
        show: function() {

            var self = this;

            if (self.el) {
                _selector.removeClass(self.el, 'hidden');
                _selector.removeClass(self.overlayEl, 'hidden');
            }
            self.fire('show', self);
        },

        /**
         * Hide alert.
         * @fires hide
         */
        hide: function() {

            var self = this;

            if (self.el) {
                _selector.addClass(self.el, 'hidden');
                _selector.addClass(self.overlayEl, 'hidden');
            }
            self.fire('hide', self);
        }

    });
})();

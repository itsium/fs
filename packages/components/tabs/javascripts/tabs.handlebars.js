/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Tabs component.
 *
 * This class implements {@link {{ns}}.implements.events events}.
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.views.Template;

    function _afterrender(self) {
        if (typeof self.handler === 'function') {
            self.events.add('click', {
                xtype: 'events.click',
                el: self.el,
                scope: self,
                handler: self.handler
            });
        }
    }

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype type=false}}',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            if (typeof opts.handler !== 'undefined') {
                /**
                 * @cfg {Function} [handler=this.handler] Click event handler.
                 */
                self.handler = opts.handler;
            }
            _parent.prototype.constructor.call(self, opts);
        },

        /**
         * Handler attached to click event.
         * @param  {Event} event Click event.
         * @fires select
         */
        handler: function(event) {

            var self = this;

            self.fire('select', self, event);
        },

        /**
         * Set given tabbutton name as active.
         * @param {String} name   Tab button name.
         * @param {Boolean} [silent=false] If true it will not fire activate event.
         */
        setActive: function(name, silent) {

            var self = this;

            if (silent === true) return;

            self.fire('activate', self, name);
        }

    });
})();

{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    // private
    var _ns = {{framework.barename}},
        _selector = _ns.Selector,
        _win = window,
        _events = _ns.events,
        _parent = _ns.views.Template;

    function _afterrender(self) {

        self.setupListeners(['show', 'hide'], self);
        self.overlayEl = document.getElementById(self.config.id + '-overlay');

        self.clickEvent = new _events.Click({
            disableScrolling: true,
            el: self.overlayEl,
            scope: self,
            handler: self.hide
        });

        self.resizeEvent = new _events.Resize({
            el: self.el,
            scope: self,
            handler: self.resize
        });

        if (self.hidden === false) {
            self.clickEvent.attach();
            self.resizeEvent.attach();
            self.resize();
        }

        self.on('show', _onshow, self, self.priority.VIEWS);
        self.on('hide', _onhide, self, self.priority.VIEWS);
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    function _onshow(self) {
        self.clickEvent.attach();
        self.resizeEvent.attach();
    }

    function _onhide(self) {
        self.clickEvent.detach();
        self.resizeEvent.detach();
    }

    function _find_parent_item(el) {
        while (el) {
            if (_selector.hasClass(el, 'floatingpanel')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    }

    function _stop_window_scroll(e) {
        if (_find_parent_item(e.target) === false) {

            if (e.stopPropagation) {
                e.stopPropagation();
            } else {
                e.cancelBubble = true;
            }

            if (e.preventDefault) {
                e.preventDefault();
            } else {
                e.returnValue = false;
            }

            return false;
        }
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            self.hidden = (opts && opts.config && opts.config.hidden === true ? true : false);
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        show: function() {

            var self = this;

            self.hidden = false;
            self.resize();
            _selector.removeClass(self.el, 'hidden');
            _selector.removeClass(self.overlayEl, 'hidden');
            _selector.addClass(document.body, 'scroll-stop');
            _win.addEventListener('touchstart', _stop_window_scroll, false);
            self.fire('show', self);
        },

        hide: function() {

            var self = this;

            self.hidden = true;
            _selector.addClass(self.el, 'hidden');
            _selector.addClass(self.overlayEl, 'hidden');
            _selector.removeClass(document.body, 'scroll-stop');
            _win.removeEventListener('touchstart', _stop_window_scroll, false);
            self.fire('hide', self);
        },

        resize: function() {

            var self = this;

            if (self.hidden === false && !self.el.offsetWidth) {

                var sid = setTimeout(function() {
                    self.resize();
                    clearTimeout(sid);
                }, 100);

                return false;

            }

            var leftMargin,
                elWidth = self.el.offsetWidth,
                containerWidth = document.body.offsetWidth;

            leftMargin = (containerWidth - elWidth) / 2;
            self.el.style.left = leftMargin + 'px';
            return true;
        }
    });
})();

/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Select input for form.
 * Examples are done with ionic UI.
 *
 * This class implements {@link {{ns}}.implements.events events}.
 *
 * ### Sample usage
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'select',
 *          config: {
 *              label: 'Gender',
 *              value: 'm',
 *              options: [{
 *                  label: 'Male',
 *                  value: 'm'
 *              }, {
 *                  label: 'Female',
 *                  value: 'f'
 *              }]
 *          }
 *      };
 *
 * ### Link to {{ns}}.data.Store
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'select',
 *          config: {
 *              label: 'Countries',
 *              name: 'country',
 *              required: true,
 *              placeHolder: 'Select your country'
 *          },
 *          store: countries_store,
 *          listeners: {
 *              change: function(select, item) {
 *                  // do something with item.value
 *              }
 *          }
 *      };
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.uiEl = document.getElementById(self.config.id + '-ui');
        self.setupListeners(['change']);
        self.events.add('change', {
            xtype: 'events.abstract',
            autoAttach: true,
            eventName: 'change',
            el: self.el,
            scope: self,
            handler: function() {

                var item;

                if (self.store) {
                    item = self.store.getAt(self.el.selectedIndex);
                } else {
                    item = self.config.options[self.el.selectedIndex];
                }
                self.fire('change', self, item);
            }
        });

        if (self.store && self.autoload) {
            self.load();
        }
    }

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype type=false}}',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        defaultAutoload: true,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            /**
             * @cfg { {{ns}}.data.Store } [store] Data store.
             */
            self.store = opts.store;
            opts.config = opts.config || {};

            /**
             * @cfg {Boolean} [autoload=true] Autoload the data.
             */
            self.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : self.defaultAutoload);

            _parent.prototype.constructor.call(self, opts);

            if (!self.itemTpl) {
                /**
                 * @cfg {Mixed} [itemTpl="itembasic"] List item template name or function directly. You can see availables `itemTpl` in `{{ns}}.*.ui.Selectitem*`.
                 */
                self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            }

            if (typeof self.itemTpl === 'string') {

                var engineItemTpl = self.engine.getTpl(self.itemTpl);

                if (typeof engineItemTpl !== 'undefined') {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        /**
         * Set loading to given value.
         * @param {Boolean} [loading=false] Loading flag. True is for loading, false is for hide loading.
         * @fires loading
         */
        setLoading: function(loading) {

            var self = this;

            loading = (loading === true);
            self.isLoading = loading;
            self.fire('loading', self, loading);
        },

        /**
         * Set disabled to given value.
         * @param {Boolean} disabled True to disable this component, else false.
         * @fires disabled
         */
        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        },

        /**
         * Empty the select items and reload data from reloading store, or given items.
         * @param  {Array} [items] Items for itemTpl.
         */
        reload: function(items) {

            var self = this;

            while (self.el.firstChild) {
                self.el.removeChild(self.el.firstChild);
            }
            if (self.store) {
                self.setLoading(true);
                self.store.reset();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.config.options);
            }
        },

        /**
         * Clear selected item.
         * @fires change
         */
        clearSelection: function() {

            var self = this;

            if (self.el) {

                var emptyItem = {};

                if (self.config.emptyText) {
                    emptyItem.label = self.config.emptyText;
                }
                self.el.selectedIndex = 0;
                self.fire('change', self, emptyItem);
            }
        },

        /**
         * Load store or given items.
         * It will append to existing items.
         * @param  {Array} [items] Items to load.
         */
        load: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            if (self.store) {
                self.setLoading(true);
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.config.options);
            }
        },

        /**
         * Load given items to select.
         * @param  {Array} items Items to load.
         */
        loadItems: function(items) {

            var self = this,
                itemTpl = self.itemTpl,
                config = _ns.utils.applyAuto(self.config, {});

            config.options = items;
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            self.setLoading(false);
        },

        /**
         * @private
         * Callback after store has been loaded.
         */
        storeLoaded: function() {

            var self = this;

            if (self.config.emptyText) {
                self.store.addRecord({
                    label: self.config.emptyText
                }, true);
            }
            self.loadItems(self.store.records);
        }
    });
})();

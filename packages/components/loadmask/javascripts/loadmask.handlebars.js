/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Loadmask component.
 *
 * This class implements {@link {{ns}}.implements.events events}.
 *
 * ### Sample usage
 *
 *      @example
 *      var loader = new {{ns}}.views.Loadmask({
 *          lock: true
 *      });
 *      loader.compile();
 *      loader.render('body');
 *
 */
{{classname type="views"}} = (function() {

    'use strict';

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        if (self.lock === true) {
            self.events.add('lock-screen', {
                xtype: 'events.abstract',
                eventName: 'touchstart',
                el: window,
                autoAttach: true,
                handler: function(evt) {
                    evt.stopPropagation();
                    evt.preventDefault();
                    return false;
                }
            });
        }
    }

    return _parent.subclass({

        className: '{{classname type="views" initialize=false}}',
        xtype: '{{xtype type=false}}',
        implements: ['events'],
        hasListeners: ['afterrender'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            if (opts.lock === true) {
                /**
                 * @cfg {Boolean} [lock=false] True to lock click/touch events while loading is active.
                 */
                self.lock = true;
                self.bodyEl = document.body;
            }

            _parent.prototype.constructor.call(self, opts);
        },

        /**
         * Show the loader.
         * @fires show
         */
        show: function() {

            var self = this;

            if (self.lock) {
                self.bodyEl.style.pointerEvents = 'none';
            }

            $.removeClass(self.el, 'hidden');
            self.fireScope('show', self);
        },

        /**
         * Hide the loader.
         * @fires hide
         */
        hide: function() {

            var self = this;

            if (self.lock) {
                self.bodyEl.style.pointerEvents = '';
            }

            $.addClass(this.el, 'hidden');
            self.fireScope('hide', self);
        }
    });

}());

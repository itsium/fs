/**
    {
        config: {
            header: {
                title: 'My supa popup', // (Optional). String for title. Defaults to undefined.
                close: 'left' // (Optional). 'left', 'right'. Defaults to undefined (none).
            },
            // @TODO actionSheet JQMobile bottom not OK.
            actionSheet: 'top', (Optional). 'top', 'bottom'. Defaults to undefined (none).
            shadows: true, // (Optional). true, false. Defaults to undefined (false).
            corner: 'all', // (Optional). 'all', 'left', 'left right top bottom', ... Defaults to undefined (none).
            overlay: 'darklight', // (Optional). 'default', 'darklight'. Defaults to 'default'.
            overlayHide: true, // (Optional). true, false. Defaults to true. Hide popup when click on overlay.
            items: [ ... ] // (Recommended). Popup content. {{framework.barename}}.views|'<p>My content</p>'
        }
    }
*/
{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    function _afterrender(self) {

        var id = self.config.id;

        self.overlayEl = document.getElementById(id + '-overlay');
        self.closeEl = document.getElementById(id + '-close');

        if (self.config.overlayHide !== false) {
            self.clickEvent = new _ns.events.Click({
                disableScrolling: true,
                autoAttach: true,
                el: self.overlayEl,
                scope: self,
                handler: self.hide
            });
        }
        if (self.closeEl) {
            self.closeEvent = new _ns.events.Click({
                disableScrolling: true,
                autoAttach: true,
                el: self.closeEl,
                scope: self,
                handler: self.hide
            });
        }
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        show: function() {

            var self = this;

            if (self.el) {
                _selector.removeClass(self.el, 'hidden');
                _selector.removeClass(self.overlayEl, 'hidden');
            }
            self.fire('show', self);
        },

        hide: function() {

            var self = this;

            if (self.el) {
                _selector.addClass(self.el, 'hidden');
                _selector.addClass(self.overlayEl, 'hidden');
            }
            self.fire('hide', self);
        }

    });
})();

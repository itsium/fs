/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Checkbox component.
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new {{ns}}.views.Checkbox({
 *          config: {
 *
 *          }
 *      });
 */
/**
    {
        config: {
            size: 'medium', // (Optional) 'small', 'medium', 'large'. Defaults to 'medium'.
            name: 'mycheckboxname', // Recommended. String. Defaults to undefined.
            label: 'My label', // Recommended. String. Defaults to undefined.
            value: false // Optional. Boolean. Defaults to undefined (unchecked).
        }
    }
*/
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.uiEl = document.getElementById(self.config.id + '-ui');
        self.eventClick = new _ns.events.Click();
        self.eventClick.attach(self.uiEl, self.handler, self);
        if (self.config.value) {
            self.check();
        } else {
            self.uncheck();
        }
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        toggle: function() {

            var self = this;

            if (self.checked) {
                return self.uncheck();
            }
            return self.check();
        },

        check: function() {

            var self = this;

            self.fire('check', self);
            self.checked = true;
            if (self.el) {
                self.el.checked = true;
            }
        },

        uncheck: function() {

            var self = this;

            self.fire('uncheck', self);
            self.checked = false;
            if (self.el) {
                self.el.checked = false;
            }
        },

        handler: function() {
            this.toggle();
        }
    });
})();

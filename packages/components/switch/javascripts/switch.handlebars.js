/**
    {
        config: {
            options: [{ // Optional. Defaults to on/off.
                value: 'off',
                label: 'Off'
            }, {
                value: 'on',
                label: 'On'
            }],
            size: 'medium', // (Optional) 'small', 'medium', 'large'. Defaults to 'medium'.
            name: 'myslidername', // Recommended. String. Defaults to undefined.
            label: 'My label', // Optional. String. Defaults to undefined.
            value: 'off' // Optional. String. Defaults to first options value.
        }
    }
*/
{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _parent = _ns.views.Template;

    function _afterrender(self) {

        self.uiEl = document.getElementById(self.config.id + '-ui');
        self.eventClick = new _ns.events.Click();
        self.eventClick.attach(self.uiEl, self.handler, self);

        if (self.config.value === true ||
            self.config.value === self.config.options[1].value) {
            self.check();
        } else {
            self.uncheck();
        }
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            opts.config = opts.config || {};
            if (!opts.config.options) {
                opts.config.options = [{
                    value: 'off',
                    label: 'Off'
                }, {
                    value: 'on',
                    label: 'On'
                }];
            }
            if (!opts.config.value) {
                opts.config.value = opts.config.options[0].value;
            }
            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        toggle: function() {

            var self = this;

            if (self.checked) {
                return self.uncheck();
            }
            return self.check();
        },

        check: function() {

            var self = this;

            self.fire('check', self);
            self.checked = true;
            if (self.el) {
                self.el.selectedIndex = 1;
            }
        },

        uncheck: function() {

            var self = this;

            self.fire('uncheck', self);
            self.checked = false;
            if (self.el) {
                self.el.selectedIndex = 0;
            }
        },

        handler: function() {
            this.toggle();
        }
    });
})();

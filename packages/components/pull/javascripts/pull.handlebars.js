/*

config.height || 50;
config.sensibility || 0.75;
config.offset || 30;
config.releaseColor || '#ddd';
config.loadingIcon || '&#128260;';
config.pullIcon || '&#59224;';

*/

{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    var _ns = {{framework.barename}},
        _parent = _ns.views.Template;

    function _tpl(config) {

        var tpl = [

            '<div id="{{id}}" class="pull">',
                '<div class="pull-text">',
                    '<span class="icon pull-icon pull-show">{{pullIcon}}</span>',
                    '<span class="icon loading-icon">{{loadingIcon}}</span>',
                '</div>',
            '</div>'

        ].join('');

        tpl = tpl.replace('{{id}}', config.id);
        tpl = tpl.replace('{{pullIcon}}', config.pullIcon);
        tpl = tpl.replace('{{loadingIcon}}', config.loadingIcon);

        return tpl;

    }

    function _onshow(self) {
        if (!self.eventAdded) {

            var parent = self.el.parentNode;

            parent.addEventListener('touchend', self.etouchend, false);
            parent.addEventListener('touchstart', self.etouchstart, false);
            parent.addEventListener('touchmove', self.etouchmove, false);
            self.eventAdded = true;
        }
    }

    function _onhide(self) {
        if (self.eventAdded) {

            var parent = self.el.parentNode;

            parent.removeEventListener('touchend', self.etouchend, false);
            parent.removeEventListener('touchstart', self.etouchstart, false);
            parent.removeEventListener('touchmove', self.etouchmove, false);
            self.eventAdded = false;
        }
    }

    function _afterrender(self) {

        var win = window,
            initialStart = null,
            previousY = null;

        self.el.parentNode.style['-webkit-transform'] = 'translate(0, -' +
            self.height + 'px) translateZ(0)';
        // prevent from flickering (blinking)
        //self.el.parentNode.style['-webkit-backface-visibility'] = 'hidden';
        //self.el.parentNode.style['-webkit-perspective'] = '1000';
        self.iconEl = self.el.querySelector('.pull-icon');
        self.loadingIconEl = self.el.querySelector('.loading-icon');
        self.currentState = 0;
        self.defaultColor = self.iconEl.style.color;

        self.etouchend = function() {

            if (self.isLoading === true) {
                return;
            }

            self.iconEl.style.color = self.defaultColor;
            self.setLoading(self.currentState >= 1 ? true : false);

        }.bind(self);

        self.etouchstart = function(e) {

            previousY = e.touches[0].screenY;
            initialStart = previousY;
            self.currentState = 0;

        }.bind(self);

        self.etouchmove = function(e) {
            var y = e.touches[0].screenY;

            if (self.isLoading === true) {
                return;
            }

            if (win.scrollY < 10) {

                var pos = Math.abs(initialStart - y);

                if (y < initialStart) {
                    pos = 0;
                } else if (win.scrollY < 5) {
                    // stopping scrolling
                    e.preventDefault();
                    e.stopPropagation();
                }

                pos = pos * self.sensibility;

                var state = (pos - self.offset) / self.height,
                    deg = (180 / self.height) * (pos - self.offset);

                // state <= 0 is close
                // state > 0 && state < 1 is opening
                // state >= 1 is open
                if (state > 1) {
                    state = 1;
                    deg = 180;
                } else if (state < 0) {
                    state = 0;
                    deg = 0;
                }

                if (self.currentState < 1 && state >= 1) {

                    // panel is fully open now == release text
                    self.iconEl.style.color = self.releaseColor;

                } else if (self.currentState >= 1 && state < 1) {

                    // panel is not full opened yet == pull text
                    self.iconEl.style.color = self.defaultColor;

                }

                self.currentState = state;

                self.iconEl.style['-webkit-transform'] = 'rotate(' + deg + 'deg)';

                self.el.parentNode.style['-webkit-transform'] = 'translate(0, ' + (-self.height + pos) + 'px) translateZ(0)';

            }
        }.bind(self);

    }

    function _aftercompile(self, renderer) {

        self.off(self.eid);
        renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);

        renderer.on('show', _onshow,
            self, self.priority.VIEWS);
        renderer.on('hide', _onhide,
            self, self.priority.VIEWS);

    }

    return _parent.subclass({

        xtype: '{{lowercase me.name}}',
        className: '{{framework.barename}}.views.{{ucfirst me.name}}',

        constructor: function(opts) {

            var self = this;

            opts.template = _tpl;
            opts.config = opts.config || {};

            if (typeof opts.config.loadingIcon === 'undefined') {
                opts.config.loadingIcon = '&#128260;';
            }

            if (typeof opts.config.pullIcon === 'undefined') {
                opts.config.pullIcon = '&#59224;';
            }

            self.height = opts.config.height || 50;
            self.sensibility = opts.config.sensibility || 0.75;
            self.offset = opts.config.offset || 30;
            self.releaseColor = opts.config.releaseColor || '#ddd';
            self.loadingIcon = opts.config.loadingIcon;
            self.pullIcon = opts.config.pullIcon;

            _parent.prototype.constructor.call(self, opts);

            self.setupListeners(['loading']);

            self.eid = self.on('aftercompile', _aftercompile,
                    self, self.priority.VIEWS);
        },

        setLoading: function(loading) {

            var self = this,
                selector = _ns.Selector;

            self.isLoading = (loading === true ? true : false);

            if (self.isLoading === true) {

                self.el.parentNode.style['-webkit-transform'] = 'translate(0, 0) translateZ(0)';
                selector.removeClass(self.iconEl, 'pull-show');
                selector.addClass(self.loadingIconEl, 'pull-loading');
                self.fire('loading');

            } else {

                selector.addClass(self.iconEl, 'pull-show');
                selector.removeClass(self.loadingIconEl, 'pull-loading');
                self.currentState = 0;
                self.el.parentNode.style['-webkit-transform'] = 'translate(0, -' +
                    self.height + 'px) translateZ(0)';
                self.iconEl.style['-webkit-transform'] = 'rotate(0deg)';

            }
        }

    });

})();

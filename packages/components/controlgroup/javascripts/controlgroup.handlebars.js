/**
    {
        config: {
            size: 'medium', // (Optional) 'small', 'medium', 'large'. Defaults to 'medium'.
            icon: 'vcard', // (Optional) See `{{framework.barename}}.Templates`. Defaults to undefined.
            route: '/your/route', // (Optional). Defaults to undefined.
            iconPos: 'left', // (Optional) 'left', 'right', 'bottom', 'top'. Defaults to 'left'.
            inline: true // (Optional) true, false. Defaults to true.
        },
        handler: function(event) { ... } // (Optional). Defaults to `{{framework.barename}}.views.Button.handler`.
    }
    {
        config: {
            position: 'horizontal', // (Optional) 'horizontal', 'vertical'. Defaults to 'horizontal'.
            size: 'large', // (Optional) 'small', 'medium', 'large'. Defaults to 'large'.
            buttons: [{
                text: 'First', // (Optional) String for text button. Defaults to undefined.
                icon: 'vcard', // (Optional) String for button icon. Defaults to undefined.
                iconPos: 'right' // (Optional) 'left', 'right'. Defaults to undefined.
            }, {
                text: 'Second',
                icon: 'list'
            }, {
                text: 'Third',
                icon: 'location'
            }]
        }
*/
{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.eventClick = new _ns.events.Click({
            autoAttach: true,
            scope: self,
            el: self.el,
            handler: self.handler
        });
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            if (opts.handler) {
                self.handler = opts.handler;
            }
            _parent.prototype.constructor.call(self, opts);
            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        handler: function(event) {

            var self = this,
                route = self.config.route;

            self.fire('select', self, event);
            if (route) {
                _ns.History.navigate(route);
            }
        }
    });
})();

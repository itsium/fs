{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _utils = _ns.utils,
        _win = window,
        _parent = _ns.views.Template;

    function _afterrender(self) {

        if (self.height) {
            self.el.style.height = self.height;
        }

        self.width = self.el.getBoundingClientRect().width;
        self.length = self.el.querySelectorAll('.slide').length;

        var container = self.el.querySelector('.slides');

        container.style.width = (self.length * self.width) + 'px';

        var slides = self.el.querySelectorAll('.slide');

        [].forEach.call(slides, function(slide) {
            slide.style.width = self.width + 'px';
        });

        self.current = 0;
        self.running = false;

        self.api = new _ns.physics.Scroll({
            api: self.apiConfig,
            cmp: self,
            el: container,
            scrollable: 'x',
            paging: true,
            beforestart: function() {
                //console.log('beforestart', new Date());
                self.started = true;
                self.pause();
            },
            afterscroll: function() {
                if (self.started) {

                    var pos = self.api.getPositions();

                    self.started = false;
                    self.current = parseInt(pos.x / self.width);
                    //console.log('afterscroll: ', pos, self.current);
                    setTimeout(function() {
                        self.resume();
                    }, 2000);

                }
            }
        });

        self.eids = [];

        self.eids.push(self.renderer.on('show', function() {
            self.resume();
        }, self));

        self.eids.push(self.renderer.on('hide', function() {
            self.pause();
        }, self));

        self.api.resize();

    }

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype}}',

        constructor: function(opts) {

            var self = this;

            opts = opts || { config: {}};
            opts.config = opts.config || {};

            if (opts.config.animation) {
                self.animation = opts.config.animation;
            }

            self.apiConfig = (opts.api ? opts.api : {});

            if (opts.config.canFullscreen) {
                self.canFullscreen = opts.config.canFullscreen;
            }

            if (opts.config.height) {
                self.height = opts.config.height;
            }

            _parent.prototype.constructor.call(self, opts);

            self.addListeners({
                priority: 'VIEWS',
                scope: self,
                afterrender: _afterrender
            })
        },

        destroy: function() {

            var self = this;

            self.api.detach();
            self.el.parentNode.removeChild(self.el);
            self.eids.forEach(function(eid) {
                self.renderer.off(eid);
            })
        },

        pause: function() {

            var self = this;

            if (self.running) {

                //console.log('pause !', self, new Date());
                self.running = false;
                clearInterval(self.tid);
            }
        },

        resume: function() {

            var self = this;

            if (self.animation && !self.running) {
                self.running = true;

                //console.log('resume !', self, new Date());

                self.tid = setInterval(function() {
                    ++self.current;

                    if (self.current >= self.length) {
                        self.current = 0;
                    }

                    //console.log('interval: ', self.current, self.length, self.width * self.current);

                    self.api.scrollTo(self.width * self.current, 0, true);
                }, self.animation);
            }
        }
    });
}());
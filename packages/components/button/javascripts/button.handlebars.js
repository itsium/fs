/**
 * @class {{classname initialize=false type="views"}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new {{ns}}.views.Button({
 *          config: {
 *              label: 'Submit'
 *          },
 *          handler: function() {
 *              // form.submit();
 *          }
 *      });
 */
(function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _views = _ns.views,
        _parent = _ns.views.Template;

    function _afterrender(self) {
        self.events.add('click', {
            xtype: 'events.click',
            autoAttach: true,
            el: self.el,
            scope: self,
            handler: self.handler
        });
    }

    // public
    _views.{{ucfirst me.name}} = _parent.subclass({

        className: '{{ns}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            /**
             * @cfg {Function} [handler] Click handler function.
             */
            /**
             * @cfg {Object} [scope] Handler function scope.
             */
            /**
             * @cfg {String} [route] Button default route. If handler has been override, this will not work by default.
             */

            if (opts.handler) {
                self.handler = (opts.scope ?
                    opts.handler.bind(opts.scope) : opts.handler);
            }

            _parent.prototype.constructor.call(self, opts);

        },

        /**
         * Default button handler.
         * If route has been defined, it will go to route when clicked.
         * @fires click
         * @param  {Event} evt Click or tap event
         */
        handler: function(evt) {

            var self = this,
                route = self.config.route;

            self.fire('click', self, evt);

            if (route) {
                _ns.History.go(route);
            }
        },

        /**
         * Set disabled state to button
         * @fires disabled
         * @param {Boolean} [disabled=false] Disabled state
         */
        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        },

        /**
         * Set icon
         * @fires seticon
         * @param {Mixed} icon Icon value, depends of which UI used.
         */
        setIcon: function(icon) {

            var self = this;

            self.fire('seticon', self, icon);
        }
    });

    _ns.global.fire('button_ready', true);

})();

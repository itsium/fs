/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Button
 *
 * Tab button component.
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.views.Button;

    // public
    return _parent.subclass({

        className: '{{classname type="views" initialize=false}}',
        xtype: '{{xtype type=false}}',

        /**
         * Click event handler.
         * Can be override.
         */
        handler: _parent.prototype.handler,

        /**
         * Active this tab button.
         * @fires activate
         */
        activate: function() {
            this.active = true;
            this.fire('activate', this);
        },

        /**
         * Deactivate this tab button.
         * @fires deactivate
         */
        deactivate: function() {
            this.active = false;
            this.fire('deactivate', this);
        }

    });
})();

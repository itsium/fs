/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Textarea for form.
 * Examples are done with ionic UI.
 *
 * ### Simple textarea
 *
 *      @example ionic_page
        var example = {
            xtype: 'textarea',
            config: {
                label: 'Description',
                placeHolder: 'Lot of content...',
                attrs: {
                    rows: 10
                }
            }
        };
 *
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.views.Template;

    function _afterrender(self) {
        if (self.clearBtn) {
            self.clearBtnEl = document.getElementById(self.config.id + '-clearBtn');
            // @TODO ? self.eid2 = self.on('keydown', _onKeyDown, self, self.priority.VIEWS);
        }
    }

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype type=false}}',
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        }

    });

})();

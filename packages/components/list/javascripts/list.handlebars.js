/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * List component.
 * Can be use with or without store.
 *
 * This class implements {@link {{ns}}.implements.events events}.
 *
 * ### Most simple example
 *
 *      @example ionic_page
 *      var example = new {{ns}}.views.List({
 *          items: [{
 *              label: 'One'
 *          }, {
 *              label: 'Two'
 *          }, {
 *              label: 'Three'
 *          }]
 *      });
 *
 * ### With listitemfull itemTpl
 *
 *      @example ionic_page
 *      var example = new {{ns}}.views.List({
 *          itemTpl: 'listitemfull',
 *          items: [{
 *              label: 'Zero',
 *              type: 'divider'
 *          }, {
 *              label: 'One',
 *              icon: 'gear-a',
 *              iconPos: 'right'
 *          }, {
 *              label: 'Two',
 *              type: 'divider'
 *          }, {
 *              label: 'Three',
 *              bubble: '1'
 *          }, {
 *              label: 'Four',
 *              icon: 'chatbubble-working'
 *          }, {
 *              label: 'Five',
 *              note: 'This is a note'
 *          }, {
 *              label: 'Six',
 *              arrow: false,
 *              button: {
 *                  position: 'left',
 *                  ui: 'positive',
 *                  icon: 'ios7-telephone',
 *                  text: 'Salut'
 *              }
 *          }]
 *      });
 *
 * ### With store
 *
 *      var store = new {{ns}}.data.Store({
 *          enableHashTable: true,
 *          proxy: {
 *              type: 'jsonp',
 *              cache: false,
 *              url: 'http://...'
 *          }
 *      });
 *      var example = new {{ns}}.views.List({
 *          itemTpl: 'test',
 *          store: store
 *      });
 *
 * ### Example of custom itemTpl handlebars template
 *
    \{{#foreach items}}

        <a class="item list-item">
            \{{#label}}\{{.}}\{{/label}}
        </a>

    \{{/foreach}}
 *
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _handlebars = Handlebars,
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    /**
     * @ignore
     * Meilleur moyen pour loader les images:
     *
     * SetInterval(50, loadImagesAfterImageLoad);
     * On detecte quand le user arrete de scroller. La on
    */

    function _afterrender(self) {

        var id = self.config.id;

        self.loadingEl = document.getElementById(id + '-loader');

        if (self.hasListener('itemselect') || self.has('itemselect')) {
            self.events.add('itemselect', {
                xtype: 'events.click',
                autoAttach: false,
                el: self.el,
                handler: function(e) {
                    // handler is called twice :( but registered just once
                    // e.stopImmediatePropagation();
                    // e.stopPropagation();
                    // e.preventDefault();
                    self.fire('itemselect', self, e);
                }
            });
        }

        if (self.autoload) {
            self.load();
        }
    }

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype type=false}}',
        implements: ['events'],
        hasListeners: ['itemselect', 'updated'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        defaultAutoload: true,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            if (!self.store) {
                /**
                 * @cfg {Object} [store=undefined] Store instance to load data through.
                 */
                self.store = opts.store;
            }

            /**
             * @cfg {Boolean} [autoload=true] Automaticaly load list after render.
             */
            self.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : self.defaultAutoload);
            if (opts.config) {
                opts.config.isLoading = self.autoload;
            }

            /**
             * @cfg {String} [emptyTpl=false] Empty data HTML markup. This will replace the list content in case data is empty (on load or after).
             */
            self.emptyTpl = opts.emptyTpl || false;

            _parent.prototype.constructor.call(self, opts);

            if (!self.itemTpl) {
                /**
                 * @cfg {Mixed} [itemTpl="itembasic"] List item template name or function directly. You can see availables `itemTpl` in `{{ns}}.*.ui.ListItem*`.
                 */
                self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            }

            if (typeof self.itemTpl === 'string') {

                var engineItemTpl = self.engine.getTpl(self.itemTpl);

                if (typeof engineItemTpl !== 'undefined') {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        /**
         * Retreive item HTMLElement from given event.
         * @param  {Event} evt Click or Touch event.
         * @return {HTMLElement} Item element if found.
         */
        getItemFromEvent: function(evt) {
            return _selector.findParentByClass(evt.target, 'list-item');
        },

        /**
         * Empty the list and then reload it.
         * If store is bind to list, use store and not given items.
         * If no items given, then just empty the list.
         * If list is already loading, then just return false.
         * @param  {Array} [items] List items.
         */
        reload: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            self.el.innerHTML = '';

            /*while (self.el.firstChild) {
                self.el.removeChild(self.el.firstChild);
            }*/

            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },

        /**
         * Show loading.
         */
        showLoading: function() {

            var self = this;

            self.isLoading = true;
            _selector.removeClass(self.loadingEl, 'hidden');
        },

        /**
         * Hide loading.
         */
        hideLoading: function() {

            var self = this;

            self.isLoading = false;
            _selector.addClass(self.loadingEl, 'hidden');
        },

        /**
         * Load list with given items.
         * If store is bind to the list, then just use store instead of items.
         * @param  {Array} [items] List items to load.
         */
        load: function(items) {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            if (self.store) {
                self.showLoading();
                self.store.load({
                    callback: self.storeLoaded.bind(self)
                });
            } else {
                self.loadItems(items || self.items);
            }
        },

        /**
         * Append given items to list.
         * @param  {Array} items Items to append.
         */
        loadItems: function(items) {

            var self = this,
                itemTpl = self.itemTpl,
                config = _ns.utils.applyAuto(self.config, {});

            config.items = items;

            _handlebars.currentParent.push(self);
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            _handlebars.currentParent.pop();

            if (self.abstractRenderer) {
                self.abstractRenderer.fireScope('afterrender', self.renderer);
            }

            self.hideLoading();
        },

        /**
         * Callback called by store after it has been loaded.
         * @param  {Boolean} success    Success flag.
         * @param  {Object} store Store.
         * @param  {Array} newRecords Records
         */
        storeLoaded: function(success, store, newRecords) {

            var self = this;

            self.loadItems(newRecords);

            if (!store.totalRecords && self.emptyTpl) {
                _selector.updateHtml(self.el, self.emptyTpl);
            }
            self.fire('updated', self, success, store, newRecords);
        }
    });
})();

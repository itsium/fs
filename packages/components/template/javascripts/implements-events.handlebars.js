/**
 * @class {{ns}}.implements.events
 * @extends {Object}
 *
 * Usage:
 *
 * 		var example = {
 * 			xtpl: 'container',
 * 			implements: ['events'],
 * 			hasListeners: ['afterrender'],
 * 			listeners: {
 * 				afterrender: function(self) {
 * 					self.events.add('scroll', {
 * 						xtype: 'physics.scroll',
 * 						cmp: this,
 * 						el: this.el,
 * 						scrollable: 'y'
 * 					});
 * 					self.events.get('scroll').resize();
 * 				}
 * 			}
 * 		};
 *
 */

/**
 * Attach all event listeners.
 * @method listen
 */

/**
 * Detach all event listeners.
 * @method unlisten
 */

/**
 * Add new event to component.
 * Example:
 *
 *      this.events.add('scroll', {
 *          xtype: 'physics.scroll',
 *          cmp: this,
 *          el: this.el,
 *          scrollable: 'y'
 *      });
 *
 * @method add
 * @param {String} name Event name. Must be unique else it will not override.
 * @param {Object} opts Event constructor options.
 * @param {String} opts.xtype Event xtype.
 */

/**
 * Remove event from component.
 * Example:
 *
 *     this.events.remove('scroll');
 *
 * @method remove
 * @param  {String} name Event name to remove.
 * @return {Boolean} True if success else false.
 */

/**
 * Get event corresponding to given name.
 * @method get
 * @param  {String} name Event name.
 * @return {Object} Event instance.
 */

/**
 * Attach all events.
 * @method attach
 */

/**
 * Detach all events.
 * @method detach
 */

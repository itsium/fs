/**
 * @class {{ns}}.views.Template
 * @extends {{ns}}.Event
 * @xtype template
 *
 * Base view template class.
 *
 *      var tpl = new {{ns}}.views.Template({
 *          xtpl: 'my-compiled-handlebars-template',
 *          config: {
 *              title: 'Hi there !'
 *          }
 *      });
 *
 *      tpl.compile();
 *      tpl.render('body');
 */
{{ns}}.views.Template = (function() {

    'use strict';

    var _ns = {{ns}},
        _gconf = _ns.config,
        _parent = _ns.Event,
        _selector = _ns.Selector,
        _handlebars = Handlebars;
        //_observer = _win.MutationObserver || _win.WebKitMutationObserver

    function _registerEngineEvents(self) {

        if (!self.renderer ||
            self.engineEventsRegistered) {
            return false;
        }
        self.engineEventsRegistered = true;

        var parent = self;

        while (parent) {
            if (parent.xtype) {
                _registerEngineListeners(self, parent.xtype);
                parent = _ns.xtype(parent.xtype).parent;
            } else {
                parent = false;
            }
        }
        return true;
    }

    function _registerEngineListeners(self, xtype) {

        var i, e, events = self.engine.getEvents(xtype);

        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    self.renderer.on(i, events[i], self, self.priority.VIEWS);
                } else {
                    self.on(i, events[i], self, self.priority.VIEWS);
                }
            }
        }
    }

    var events = function(parent) {

        return {

            listen: function() {

                var self = this;

                if (!self._eshowid) {
                    self._eshowid = parent.renderer.on('show', self.attach, self);
                    self._ehideid = parent.renderer.on('hide', self.detach, self);
                    self._eremoveid = parent.renderer.on('remove', self.remove, self);
                }
            },

            unlisten: function() {

                var self = this;

                if (self._eshowid) {
                    parent.renderer.off(self._eshowid);
                    parent.renderer.off(self._ehideid);
                    parent.renderer.off(self._eremoveid);

                    delete self._eshowid;
                    delete self._ehideid;
                    delete self._eremoveid;
                }
            },

            add: function(name, opts) {

                var self = this;

                if (!self._events) {
                    self._events = {};
                    self._eventKeys = [];
                    self._elen = 0;
                }
                if (!self._events[name]) {
                    self._events[name] = new (_ns.xtype(opts.xtype))(opts);
                    self._eventKeys.push(name);
                    ++self._elen;
                }
                if (self._elen === 1) {
                    self.listen();
                }
                return self._events[name];
            },

            remove: function(name) {

                var self = this;

                if (self._events && self._events[name]) {
                    self._events[name].detach();
                    delete self._events[name];
                    --self._elen;

                    if (!self._elen) {
                        self.unlisten();
                    }

                    return true;
                }
                return false;
            },

            get: function(name) {
                return this._events[name];
            },

            attach: function() {

                var key,
                    self = this,
                    len = self._elen;

                while (len--) {
                    key = self._eventKeys[len];
                    self._events[key].attach();
                }

                return self;

            },

            detach: function() {

                var key,
                    self = this,
                    len = self._elen;

                while (len--) {
                    key = self._eventKeys[len];
                    self._events[key].detach();
                }

                return self;

            }

        };

    };

    /*
    function _registerEngineOverrides() {
        if (this.engineOverridesRegistered) {
            return false;
        }
        this.engineOverridesRegistered = true;

        var i, o, overrides = this.engine.getOverrides(this.xtype);

        for (i in overrides) {
            o = overrides[i];

            if (typeof o === 'function') {
                this[i] = o;
            }
        }
        return true;
    };
    */

    function tpl_update(self) {

        return function(config, items) {

            if (!self.el) {
                return false;
            }

            self.parentEl = self.el.parentNode;
            self.nextEl = self.parentEl.querySelector('#' + self.el.id + ' + *');
            self.remove();

            if (!config.id) {
                config.id = self.config.id;
            }

            self.data = config;
            self.data.items = [];
            if (items) {
                self.items = items;
            } else {
                self.items = [];
            }

            self.compile();

            if (self.nextEl) {
                self.render(self.nextEl, 'beforebegin');
            } else {
                self.render(self.parentEl, 'beforeend');
            }
            self.fireScope('update', self);
        };

    }

    return _parent.subclass({

        className: '{{ns}}.views.Template',
        xtype: 'template',

        /**
         * @cfg {String} [xtpl=undefined] Abstract template name.
         */

        /**
         * @cfg {Boolean} [hidden=false] Hidden component.
         */

        /**
         * New template.
         * @param {Object} config Configuration.
         */
        constructor: function(opts) {

            var self = this;

            opts = opts || {
                template: '',
                config: {},
                items: []
            };

            if (opts.renderTo) {
                /**
                 * @cfg {Mixed} [renderTo] Render to element.
                 * Can be a selector string or a HTMLElement.
                 *
                 *     @example
                 *     renderTo: 'body'
                 *     renderTo: '#myelementid'
                 *     renderTo: document.body
                 */
                self.renderTo = opts.renderTo;
            }

            if (opts.implements) {
                self.implements = self.implements || [];
                self.implements = self.implements.concat(opts.implements);
            }

            /**
             * @cfg {Array} [implements=[]] Additional features implementations.
             * You can implement:
             * <ul>
             *  <li>{@link {{ns}}.implements.events events}: Allows to use event API to manage component events.</li>
             *  <li>{@link {{ns}}.implements.update update}: Add update method to component to update template data and items without creating new component.</li>
             * </ul>
             */
            if (self.implements) {
                if (self.implements.indexOf('events') !== -1) {
                    self.events = events(self);
                }
                if (self.implements.indexOf('update') !== -1) {
                    self.update = tpl_update(self);
                }
            }

            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtpl = opts.xtpl;
            }

            /**
             * @cfg {Object} [config={}] Template configuration.
             * This variable will be the scope while executing template.
             */
            self.config = opts.config || {};
            if (!self.config.id) {
                self.config.id = _selector.generateId();
            }

            if (self.config.hidden === true) {
                self.hidden = true;
            } else {
                self.hidden = false;
            }

            // <testing>
            //delete opts.config;
            //self.config = opts;
            // </testing>

            /**
             * @cfg {Object} [defaults={}] Defaults values for first child level items.
             */
            if (opts.defaults) {
                self.defaults = opts.defaults;
            }

            /**
             * @cfg {Mixed} items (optional) Template children items.
             *
             * ## Different possibilities:
             *
             *     var mypage = new {{ns}}.views.Page({
             *         items: [{
             *             xtype: 'header',
             *             config: {
             *                 title: 'My page'
             *             }
             *         }, {
             *             xtype: 'content',
             *             items: '<h1>My content</h1>'
             *         }, {
             *             xtype: 'footer',
             *             items: [new {{ns}}.views.TabButton({
             *                 ...
             *             }), {
             *                 xtype: 'tabbutton',
             *                 ...
             *             }, 'Raw HTML', ...]
             *         }]
             *     })
             *
             */
            self.items = opts.items || [];
            if (!self.items.indexOf) {
                self.items = [self.items];
            }
            self.data = self.config;
            self.data.items = [];
            //self.data.items = self.items;
            if (!opts.template) {
                opts.template = self.setEngine(opts, _gconf).getTpl(self.xtpl || self.xtype);
            } else {
                self.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }

            /**
             * @cfg {String} ref Reference name to given `refScope`.
             */
            if (opts.ref) {
                self.ref = opts.ref;
            }

            /**
             * @cfg {Object} refScope Reference scope.
             */
            if (opts.refScope) {
                self.refScope = opts.refScope;
            }

            /**
             * @property {Function} tpl Template function.
             * @readonly
             */
            self.tpl = opts.template;

            /**
             * @property {String} html Compiled HTML.
             * @readonly
             */
            self.html = '';
            /**
             * @property {String} renderToEl Render to element
             * @readonly
             */
            self.renderToEl = '';

            /**
             * @property {HTMLElement} el HTML element corresponding to this component.
             * @readonly
             */
            self.el = '';
            self.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(self, arguments);

            if (typeof self.renderTo !== 'undefined') {
                self.compile();
                self.render(self.renderTo);
            }
        },

        getEngine: function(config, gconfig) {

            var self = this;

            if (config && config.engine) {
                return config.engine;
            } else if (self.engine) {
                return self.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }

            return self.engine;
        },

        setEngine: function(config, gconfig) {

            var self = this;

            self.engine = self.getEngine(config, gconfig);

            if (typeof self.engine === 'string') {
                self.engine = new (_ns.xtype(self.engine))();
            } else if (self.engine) {
                self.engine = new self.engine();
            } else {
                console.warn('No engine specified. Use default engine.', self.engine);
                self.engine = {
                    getEvents: function() {},
                    getTpl: function(tplname) {
                        var tpl = _handlebars.templates[tplname];
                        if (!tpl) console.warn('Template "' + tplname + '" does not exists.');
                        return tpl;
                    }
                };
            }

            return self.engine;
        },

        /**
         * Compile template and generate HTML.
         * @fires aftercompile
         */
        compile: function(parentConfig, renderer) {

            var item, tpl,
                self = this,
                i = -1,
                items = self.items,//.slice(0),
                length = self.items.length;

            if (typeof self.tpl !== 'function') {
                throw new Error('no template given');
                return false;
            }

            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _ns.utils.applyIf(self.config, parentConfig, ['ui']);
            }

            while (++i < length) {

                item = items[i];

                if (typeof item === 'object') {
                    if (self.defaults) {
                        _ns.utils.applyIfAuto(item, self.defaults);
                    }
                    if (!item.engine) {
                        item.engine = self.engine.xtype;
                    }
                }

                if (typeof item === 'object' &&
                    (typeof item.xtype === 'string' ||
                    typeof item.xtpl === 'string') &&
                    !(item instanceof _parent)) {
                    if (!item.xtype) {
                        item.xtype = 'template';
                    }
                    //typeof item.__proto__.xtype === 'undefined') {
                    //typeof Object.getPrototypeOf(item).xtype === 'undefined') {
                    item = new (_ns.xtype(item.xtype))(item);
                    item.parent = self;
                }
                if (typeof item === 'string') {
                    tpl = _ns.Templates.compile(item);
                    self.data.items[i] = tpl(self.config);
                } else if (typeof item.compile === 'function') {
                    item.parent = self;
                    self.data.items[i] = item.compile(self.config, renderer || self);
                } else {
                    self.data.items[i] = item;
                }
            }

            _handlebars.currentParent.push(self);

            self.html = self.tpl(self.data);

            _handlebars.currentParent.pop();

            if (!self.renderer) {
                self.renderer = renderer || self;
                if (self.ref && !self.refScope) {
                    self.refScope = self.renderer;
                }
            }
            // @todo Check if this kind of thing is not too much perf killer
            // and double check memory leak.
            // <unstable>
            if (self.ref &&
                !self.refScope[self.ref]) {
                self.refScope[self.ref] = self;
            }
            // </unstable>

            self.__arID = self.renderer.on('afterrender', function() {
                self.el = document.getElementById(self.config.id);
                //self.parentEl = self.el.parentNode;
                self.renderer.off(self.__arID);
                delete self.__arID;
            }, self, self.priority.CORE);

            self.setupListeners([

                /**
                 * @event aftercompile
                 * Fire after component has been compiled.
                 * @param { {{ns}}.views.Template } self The component instance
                 * @param {Mixed} renderer The renderer component instance.
                 */
                'aftercompile',

                /**
                 * @event afterrender
                 * Fire after component has been rendered.
                 * @param {Mixed} scope Given scope
                 * @param { {{ns}}.views.Template } self The component instance
                 * @param {Mixed} renderer The renderer component instance.
                 * This is the instance of the component from which the `render` method has been called.
                 */
                'afterrender'
            ], self.renderer);

            self.fire('aftercompile', self, self.renderer);
            _registerEngineEvents(self);

            return self.html;
        },

        /**
         * Render template to given element and position.
         * position: <!-- beforebegin --><p><!-- afterbegin -->foo<!-- beforeend --></p><!-- afterend -->
         * @fires afterrender
         * @param {Mixed} renderToEl Selector string or HTML element.
         * @param {String} [position='beforeend'] Position to render element at.
         * @chainable
         */
        render: function(renderToEl, position) {

            var self = this;

            position = position || 'beforeend';

            if (typeof renderToEl === 'string') {
                //renderToEl = _ns.Selector.get(renderToEl);
                self.renderToEl = _ns.Selector.get(renderToEl);
            } else {
                self.renderToEl = renderToEl;
            }

            self.renderToEl.insertAdjacentHTML(position, self.html);
            self.fireScope('afterrender', self);

            if (!self.el) {
                self.el = document.getElementById(self.config.id);
            }

            return self;
        },

        /**
         * Remove element from DOM if rendered.
         * @fires remove
         * @chainable
         */
        remove: function() {

            var self = this;

            if (self.el) {
                self.fireScope('remove', self);
                self.el.parentNode.removeChild(self.el);
                delete self.el;
                // @ignore !TODO remove all events automatically self.fire('remove', self);
                // should store all events (like click, resize etc.) inside instance array var
            } else {
                console.warn('Element cannot been removed because it has not been rendered !', this);
            }

            return self;
        },

        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        },

        /**
         * Show element.
         * @param  {Mixed} args Arguments for listeners.
         * @fires show
         * @chainable
         */
        show: function(args) {

            var self = this;

            self.hidden = false;
            /**
             * @event show
             * Fire after component has been showed.
             * @param {Mixed} scope Given scope from listener
             * @param { {{ns}}.views.Template } self Component instance.
             * @param {Mixed} args Given arguments to show (from router for example)
             */
            self.fireScope('show', self, args);
            return self;
        },

        /**
         * Hide element.
         * @param  {Mixed} args Arguments for listeners.
         * @fires hide
         * @chainable
         */
        hide: function() {

            var self = this;

            self.hidden = true;
            /**
             * @event hide
             * Fire after component has been hidden.
             * @param {Mixed} scope Given scope from listener
             * @param { {{ns}}.views.Template } self Component instance.
             */
            self.fireScope('hide', self);
            return self;
        }
    });

}());

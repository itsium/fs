/**
 * @class {{ns}}.implements.update
 * @extends {Object}
 *
 * Usage:
 *
 * 		var example = {
 * 			xtpl: 'container',
 * 			implements: ['update'],
 * 			hasListeners: ['afterrender'],
 * 			listeners: {
 * 				afterrender: function(self) {
 * 					self.update({}, 'New container items content.');
 * 				}
 * 			}
 * 		};
 *
 */

/**
 * Update component with given configuration and items.
 * Example:
 *
 *      this.update({
 *      	data: data
 *      }, [{
 *      	xtype: 'button',
 *      	config: {
 *      		text: 'OK'
 *      	}
 *      }, '-', {
 *      	xtype: 'button',
 *      	config: {
 *      		text: 'Cancel'
 *      	}
 *      }]);
 *
 * @method update
 * @param {Object} config New component configuration.
 * @param {Mixed} [items] New component items.
 */

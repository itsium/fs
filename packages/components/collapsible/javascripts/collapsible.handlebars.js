/* views/collapsible.js */

/**
    {
        config: {
            header: {
                title: 'My section', // (Recommended) String. Defaults to undefined.
                ui: 'a' // (Optional) String. Defaults to inherit from parent config.
            },
            content: {
                ui: 'c' // (Optional) String. Defaults to inherit from parent config.
            },
            iconPos: 'left', // (Optional). String 'left', 'right'. Defaults to 'left'.
            iconCollapsed: 'plus', // (Optional). String. Defaults to engine default.
            iconExpanded: 'minus', // (Optional). String. Defaults to engine default.
            size: 'small', // (Optional). String 'small', 'medium'. Defaults to 'medium'.
            canCollapse: true, // (Optional). Boolean. Defaults to true.
        },
        items: [ ... ] // Items for collapsible content. Array of strings or {{framework.barename}} views object/tpl.
    }
*/
{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _parent = _ns.views.Template;

    function _afterrender(self) {

        var id = self.config.id;

        self.headerEl = document.getElementById(id + '-header');
        self.contentEl = document.getElementById(id + '-content');
        if (self.canCollapse) {
            self.eventClick = new {{framework.barename}}.events.Click({
                autoAttach: true,
                el: self.headerEl,
                scope: self,
                handler: self.handler
            });
        }
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            opts.config = opts.config || {};
            self.collapsed = opts.config.collapsed || false;
            if (typeof opts.config.canCollapse !== 'undefined') {
                self.canCollapse = opts.config.canCollapse;
            } else {
                self.canCollapse = true;
            }
            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        toggle: function() {

            var self = this;

            if (self.collapsed) {
                return self.expand();
            }
            return self.collapse();
        },

        collapse: function() {

            var self = this;

            self.collapsed = true;
            self.fire('collapse', self);
        },

        expand: function() {

            var self = this;

            self.collapsed = false;
            self.fire('expand', self);
        },

        handler: function() {
            this.toggle();
        }
    });
})();

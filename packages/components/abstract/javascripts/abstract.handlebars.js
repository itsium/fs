/**
 * Abstract view.
 *
 * This view is now deprecated. Please use {{ns}}.views.Template with `xtpl` option instead.
 *
 * @deprecated
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 */
{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _templates = _ns.Templates,
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtype = opts.xtpl;
            }
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();

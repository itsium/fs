/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * List buffered component.
 * Must be use with PagingStore instead of Store.
 *
 * This class implements {@link {{ns}}.implements.events events}.
 *
 * ### Example
 *
 *      var store = new _ns.data.Pagingstore({
            enableHashTable: true,
            recordPerPage: 10,
            proxy: {
                type: 'jsonp',
                cache: false,
                url: 'http://...'
            }
        });

        var example = {
            xtype: 'listbuffered',
            xtpl: 'tickets-list',
            itemTpl: 'user-orderitem',
            emptyTpl: [
                '<div class="empty-list">',
                    '<i class="icon ion-pricetags"></i>',
                    '<span class="message">',
                        t('No orders.'),
                    '</span>',
                '</div>'
            ].join(''),
            ref: 'list',
            refScope: this,
            store: store
        };
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _window = window,
        _ns = {{ns}},
        _events = _ns.events,
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    /**
    Meilleur moyen pour loader les images:

    SetInterval(50, loadImagesAfterImageLoad);
    On detecte quand le user arrete de scroller. La on
    */

    function _afterrender(self) {

        self.loadingEl = document.getElementById(self.config.id + '-loader');

        if (self.hasListener('itemselect') || self.has('itemselect')) {
            self.setupListeners(['itemselect'], self);
            self.events.add('itemselect', {
                xtype: 'events.click',
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function(e) {
                    self.fire('itemselect', self, e);
                }
            });
        }

        if (self.disableScrollEvent !== true) {
            self.events.add('scroll', {
                xtype: 'events.scroll',
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: self.scrollHandler
            });
        }

        self.loadNext();

    }

    // public
    return _parent.subclass({

        // What range of pixel to load before scrolling hit the reload point
        deltaPixels: 100,
        className: '{{classname type="views" initialize=false}}',
        xtype: '{{xtype type=false}}',
        implements: ['events'],
        listeners: {
            priority: 'VIEWS',
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};
            /**
             * @cfg { {{ns}}.data.PagingStore } store Store.
             */
            self.store = opts.store;
            /**
             * @cfg {String} [emptyTpl=false] Empty data HTML markup. This will replace the list content in case data is empty (on load or after).
             */
            self.emptyTpl = opts.emptyTpl || false;
            /**
             * @property {Boolean} [isLoading=false] True if list is loading, else false.
             */
            self.isLoading = false;
            /**
             * @cfg {Boolean} [disableScrollEvent=false] True to disable default scroll event, else false.
             */
            self.disableScrollEvent = opts.disableScrollEvent || false;

            /**
             * @cfg {Boolean} [loading=true] Loading indicator.
             */
            if (typeof opts.loading === 'boolean') {
                self.loading = opts.loading;
            } else {
                self.loading = true;
            }

            /**
             * @cfg {Boolean} [emptyBeforeLoad=true] Empty the list before store load.
             * Only occurs on store load, not on store next page.
             */
            if (typeof opts.emptyBeforeLoad === 'boolean') {
                self.emptyBeforeLoad = opts.emptyBeforeLoad;
            } else {
                self.emptyBeforeLoad = true;
            }

            _parent.prototype.constructor.call(self, opts);

            self.setupListeners(['showloading', 'hideloading', 'afterload']);

            if (!self.itemTpl) {
                /**
                 * @cfg {Mixed} [itemTpl="itembasic"] List item template name or function directly. You can see availables `itemTpl` in `{{ns}}.*.ui.ListItem*`.
                 */
                self.itemTpl = opts.itemTpl || self.engine.getTpl(self.xtype + 'itembasic');
            }

            if (typeof self.itemTpl === 'string') {

                var engineItemTpl = self.engine.getTpl(self.itemTpl);

                if (typeof engineItemTpl !== 'undefined') {
                    self.itemTpl = engineItemTpl;
                }
            }
        },

        /**
         * Retreive item HTMLElement from given event.
         * @param  {Event} evt Click or Touch event.
         * @return {HTMLElement} Item element if found.
         */
        getItemFromEvent: function(evt) {
            return $.findParentByClass(evt.target, 'list-item');
        },

        scrollHandler: function() {

            var p = document.body,
                //scrollTop = p.scrollTop,
                screenHeight = _window.screen.availHeight,
                scrollHeight = p.scrollHeight - this.deltaPixels,
                totalScroll = p.scrollTop + screenHeight;

            if (totalScroll >= scrollHeight) {
                this.loadNext();
            }
        },

        /**
         * Empty the list and then reload it.
         * If list is already loading, then just return false.
         */
        reload: function() {

            var self = this;

            if (self.isLoading === true) {
                return false;
            }
            self.isLoading = true;
            if (self.emptyBeforeLoad === true) {
                self.el.innerHTML = '';
            } else {
                self.needEmpty = true;
            }
            if (self.loading === true) {
                self.showLoading();
            } else {
                self.hideLoading();
            }
            self.store.reset();
            self.store.nextPage({
                callback: self.getNext.bind(self)
            });
        },

        /**
         * Load next items from paging store.
         * If store already loading, returns false.
         */
        loadNext: function() {

            var self = this;

            if (self.isLoading === true) {
                return false;
            } else if (self.store.loadedRecords &&
                self.store.loadedRecords === self.store.totalRecords) {
                self.hideLoading();
                return false;
            }
            self.isLoading = true;
            self.showLoading();
            self.store.nextPage({
                callback: self.getNext.bind(self)
            });
        },

        /**
         * @private
         * Store nextPage callback.
         * @param  {Boolean} success Success flag
         * @param  { {{ns}}.data.PagingStore } store Paging store
         * @param  {Array} newRecords New records
         * @fires afterload
         */
        getNext: function(success, store, newRecords) {

            var conf,
                self = this,
                itemTpl = this.itemTpl || _itemTpl;

            if (self.needEmpty === true) {
                self.el.innerHTML = '';
                delete self.needEmpty;
            }
            conf = self.config;
            conf.items = newRecords;
            self.el.insertAdjacentHTML('beforeEnd', itemTpl(conf));/*{
                items: newRecords
            }));*/
            // <forcing redraw> BUG on iOS 6.1.3 Safari mobile
            // when reloading list from 4 elems to 1 elem.
            // The 1 elem not shown if don't force redraw
            $.redraw(self.el);
            // </forcing redraw>
            if (store.loadedRecords === store.totalRecords) {
                self.hideLoading();
            } else if (store.totalRecords && self.loading === false) {
                self.showLoading();
            }

            self.isLoading = false;

            if (!store.totalRecords && self.emptyTpl) {
                $.updateHtml(self.el, self.emptyTpl);
            }

            self.fireScope('afterload', self);
        },

        /**
         * Show loading
         * @fires showloading
         */
        showLoading: function() {

            var self = this;

            $.removeClass(self.loadingEl, 'list-loader-hide');
            self.fireScope('showloading', self);
        },

        /**
         * Hide loading
         * @fires hideloading
         */
        hideLoading: function() {

            var self = this;

            $.addClass(this.loadingEl, 'list-loader-hide');
            self.fireScope('hideloading', self);
        }
    });
})();

/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 */
{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}'

    });
})();

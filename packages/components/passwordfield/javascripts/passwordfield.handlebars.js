{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _parent = _ns.views.Textfield;

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',
        xtpl: 'textfield',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            opts.config.type = 'password';
            opts.config.autocomplete = 'off';
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();

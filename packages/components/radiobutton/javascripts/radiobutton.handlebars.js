{{framework.barename}}.views.{{ucfirst me.name}} = (function() {

    'use strict';

    // private
    var _radioUncheckEvent,
        _window = window,
        _ns = {{framework.barename}},
        _selector = _ns.Selector,
        _parent = _ns.views.Template;

    /**
     * @ignore
     * @event radioUncheck
     * Fired on input radio when it need to be unchecked
     */
     if (_window.CustomEvent) {
        _radioUncheckEvent = new _window.CustomEvent('radioUncheck', {
            /*detail: {
                message: 'Fired when radio need to be unchecked.'
            },*/
            bubbles: true,
            cancelable: true
        });
    }

    var _getOtherRadios = function(formEl, name) {
        return _selector.gets('input[name="' + name + '"]', formEl);
    };

    function _afterrender(self) {

        var id = self.config.id;

        self.uiEl = document.getElementById(id + '-ui');
        self.clickEvent = new _ns.events.Click();
        self.clickEvent.attach(self.uiEl, self.handler, self);
        self.changeEvent = new _ns.events.Abstract({
            autoAttach: true,
            eventName: 'radioUncheck',
            el: self.el,
            scope: self,
            handler: self.uncheck
        });
        if (self.config.checked) {
            self.check();
        }
    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    // public
    return _parent.subclass({

        className: '{{framework.barename}}.views.{{ucfirst me.name}}',
        xtype: '{{lowercase me.name}}',

        constructor: function(opts) {

            var self = this;

            _parent.prototype.constructor.call(self, opts);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);
        },

        check: function() {

            var self = this;

            if (self.checked) {
                return false;
            }

            self.fire('check', self);
            self.checked = true;

            if (self.el) {

                var item,
                    form = _selector.findParentByTag(self.el, 'FORM'),
                    radios = _getOtherRadios(form, self.config.name),
                    i = radios.length;

                while (i--) {
                    item = radios[i];
                    if (item.checked && _radioUncheckEvent) {
                        item.dispatchEvent(_radioUncheckEvent);
                    }
                }
                self.el.checked = true;
            }
        },

        uncheck: function() {

            var self = self;

            if (!self.checked) {
                return false;
            }
            self.fire('uncheck', self);
            self.checked = false;
            if (self.el) {
                self.el.checked = false;
            }
        },

        handler: function() {

            var self = self;

            if (!self.checked) {
                self.check();
            }
        }
    });
})();

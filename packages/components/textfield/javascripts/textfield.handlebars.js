/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * Textfield for form.
 * Examples are done with ionic UI.
 *
 * ### Simple textfield
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'textfield',
 *          config: {
 *              label: 'Username',
 *              icon: 'ion-user',
 *              placeHolder: 'remi'
 *          }
 *      };
 *
 * ### Multiple fields type
 *
 *      @example ionic_page
 *      var example = [{
 *          xtype: 'textfield',
 *          config: {
 *              labelInline: false,
 *              required: true,
 *              label: 'Username',
 *              placeHolder: 'Your name...'
 *          }
 *      }, {
 *          xtype: 'textfield',
 *          config: {
 *              required: true,
 *              type: 'password',
 *              labelInline: false,
 *              label: 'Password',
 *              placeHolder: 'Password...'
 *          }
 *      }, {
 *          xtype: 'textfield',
 *          config: {
 *              type: 'date',
 *              labelInline: false,
 *              label: 'Birthday date',
 *              placeHolder: 'DD-MM-YYYY...'
 *          }
 *      }];
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: '{{classname type="views"}}',
        xtype: '{{xtype type=false}}',

        /**
         * Disable or enable textfield.
         * @param {Boolean} disabled Disable flag. True to disable, false to enable.
         * @fires disabled
         */
        setDisabled: function(disabled) {

            var self = this;

            disabled = (disabled === true);
            self.disabled = disabled;
            if (self.el) {
                if (disabled) {
                    self.el.setAttribute('disabled', 'disabled');
                } else {
                    self.el.removeAttribute('disabled');
                }
            }
            self.fire('disabled', self, disabled);
        }
    });
})();

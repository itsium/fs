/**
 * @class {{classname type="views" initialize=false}}
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Template
 *
 * ### Example
 *
 *		@example ionic_container
 *      var example = {
 *      	xtype: 'page',
 *          items: [{
 *          	xtype: 'header',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Header title'
 *          	}
 *          }, {
 *          	xtype: 'footer',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Footer title'
 *          	}
 *          }]
 *      };
 */
{{classname type="views"}} = (function() {

    'use strict';

    // private
    var _ns = {{ns}},
        _parent = _ns.views.Template;

    // public
    return _parent.subclass({

        className: '{{classname type="views" initialize=false}}',
        xtype: '{{xtype type=false}}'

    });
})();

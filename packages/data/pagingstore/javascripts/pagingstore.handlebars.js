/**
 * @class {{framework.barename}}.data.{{ucfirst me.name}}
 * @extends {{framework.barename}}.data.Store
 * @xtype {{lowercase me.name}}
 *
 * Paging store. Contains easy setup methods for retrieve records with paginator.
 *
 * ## Example
 *
 *     this.newsStore = new {{framework.barename}}.data.{{ucfirst me.name}}({
 *         enableHashTable: true,
 *         recordPerPage: 10,
 *         storageCache: true,
 *         reader: {
 *             idProperty: 'id',
 *             rootProperty: 'data',
 *             successProperty: 'success',
 *             totalProperty: 'total'
 *         },
 *         proxy: {
 *             type: 'jsonp',
 *             cache: false,
 *             url: 'http://example.com/news/list.json?callback={callback}'
 *         },
 *         listeners: {
 *             scope: this,
 *             loaderror: onStoreError
 *         }
 *     });
 *
 *     this.newsStore.loadPage(1, {
 *         scope: this,
 *         callback: onDataLoaded
 *     });
 *
 *     this.newsStore.nextPage();
 *
 */
{{framework.barename}}.data.{{ucfirst me.name}} = (function() {

    'use strict';

    var _ns = {{framework.barename}},
        _parent = _ns.data.Store;

    return _parent.subclass({

        xtype: '{{lowercase me.name}}',

        /**
         * Create new paging store.
         * @param {Object} config Configuration.
         */
        constructor: function(config) {
            config = config || {};

            /**
             * @cfg {Number} currentPage (optional) Current page.
             */
            this.currentPage = config.currentPage || 0;

            /**
             * @cfg {Number} recordPerPage (optional) Number of record per page.
             */
            this.recordPerPage = config.recordPerPage || 10;
            _parent.prototype.constructor.call(this, config);
        },

        /**
         * Load the given page.
         *
         * @param {Number} page Page number.
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        loadPage: function(page, opts) {

            opts = opts || {};
            opts.url = opts.url || this.getProxyUrl();

            var nb = this.recordPerPage,
                paging = 'start=' + (page * nb) +
                    '&limit=' + nb;

            if (opts.url.indexOf('?') !== -1) {
                opts.url += '&' + paging;
            } else {
                opts.url += '?' + paging;
            }
            return this.load(opts);
        },

        /**
         * Load the next page.
         *
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        nextPage: function(opts) {
            return this.loadPage(this.currentPage++, opts);
        },

        /**
         * Load the previous page.
         *
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        previousPage: function(opts) {
            return this.loadPage(this.currentPage--, opts);
        },

        /**
         * Reset/Empty all data. TODO: Need to rename to `empty` ?
         */
        reset: function() {
            this.currentPage = 0;
            _parent.prototype.reset.apply(this, arguments);
        }
    });

}());

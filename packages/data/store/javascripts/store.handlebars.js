/**
 * @class {{ns}}.data.{{ucfirst me.name}}
 * @extends {{ns}}.Event
 * @requires {{ns}}.Storage
 * @requires {{ns}}.Request
 * @xtype store
 *
 * Store class. Used for retrieve data from given proxy configuration and link them to
 * view.
 *
 * ## Example
 *
 *     this.newsStore = new {{ns}}.data.{{ucfirst me.name}}({
 *         enableHashTable: true,
 *         storageCache: true,
 *         reader: {
 *             idProperty: 'id',
 *             rootProperty: 'data',
 *             successProperty: 'success',
 *             totalProperty: 'total'
 *         },
 *         proxy: {
 *             type: 'jsonp',
 *             cache: false,
 *             url: 'http://example.com/news/list.json?callback={callback}'
 *         },
 *         listeners: {
 *             scope: this,
 *             loaderror: onStoreError
 *         }
 *     });
 *
 *     this.newsStore.load({
 *         jsonParams: { page: 1, dir: 'ASC', name: 'test%' },
 *         scope: this,
 *         callback: onDataLoaded
 *     });
 *
 */
(function() {

    'use strict';

    var _ns = {{ns}},
        _storage = _ns.Storage,
        _debug = _ns.debug,
        _request = _ns.Request,
        _parent = _ns.Event,
        _win = window,
        _online = _win.navigator.onLine;

    _win.addEventListener('online', function() {
        _online = true;
    }, false);

    _win.addEventListener('offline', function() {
        _online = false;
    }, false);

    /**
     * @cfg {Object} proxy Proxy configuration.
     *
     * ## Example
     *
     *     proxy: {
     *         url: 'http://example.com/data/list.json',
     *         type: 'ajax' // or `json` or `memory`
     *     }
     *
     */

    /**
     * @cfg {Object} reader Store reader configuration.
     *
     * ## Example (with default values)
     *
     *     reader {
     *         idProperty: 'id',
     *         rootProperty: 'data',
     *         successProperty: 'success',
     *         totalProperty: 'total'
     *     }
     *
     */
    _ns.data.{{ucfirst me.name}} = _parent.subclass({

        xtype: '{{lowercase me.name}}',
        defaultEnableHashTable: false,
        defaultReaderRoot: 'data',
        defaultReaderTotal: 'total',
        defaultReaderSuccess: 'success',
        defaultReaderId: 'id',
        defaultStorageCache: false,

        /**
         * Create new store.
         * @param {Object} config Configuration.
         */
        constructor: function(config) {

            var self = this;

            self.config = config || {};
            self.totalRecords = config.totalRecords || 0;
            self.loadedRecords = 0;
            self.records = [];

            /**
             * @property {Boolean} isLoading Current loading status.
             * @readonly
             */
            self.isLoading = false;

            /**
             * @cfg {Boolean} enableHashTable Enable hash table for store records.
             * Allows developer to use {@link #getRecord getRecord} method to quickly find record.
             * Defaults to `false`.
             */
            self.enableHashTable = config.enableHashTable || self.defaultEnableHashTable;

            /**
             * @cfg {Boolean} storageCache Enable or disable local storage cache for store remote results.
             * Defaults to `false`.
             */
            self.storageCache = config.storageCache || self.defaultStorageCache;

            if (self.enableHashTable) {
                self.hashRecords = {};
            }

            self.rootProperty = self.getReaderRoot(self.defaultReaderRoot);
            self.totalProperty = self.getReaderTotal(self.defaultReaderTotal);
            self.successProperty = self.getReaderSuccess(self.defaultReaderSuccess);
            self.idProperty = self.getReaderId(self.defaultReaderId);

            _parent.prototype.constructor.apply(self, arguments);

            self.setupListeners([
                /**
                 * @event load
                 * Fired after data from load request from {@link #method-load load} method call but before `callback` call.
                 * @param {Boolean} success Request success.
                 * @param {@link {{ns}}.data.Store} store Store.
                 * @param {Array} newRecords New records.
                 */
                'load',

                /**
                 * @event afterload
                 * Fired after data from load request from {@link #method-load load} method call and after `callback` call.
                 * @param {Boolean} success Request success.
                 * @param {@link {{ns}}.data.Store} store Store.
                 * @param {Array} newRecords New records.
                 */
                'afterload',

                /**
                 * @event beforeload
                 * Fired before load request from {@link #method-load load} method called.
                 * @param {@link {{ns}}.data.Store} store Store.
                 * @param {Object} options Load request options.
                 */
                'beforeload',

                /**
                 * @event loaderror
                 * Fired when error occured from {@link #method-load load} method call.
                 * @param {Boolean} success Request success.
                 * @param {@link {{ns}}.data.Store} store Store.
                 * @param {String} data Data from request result.
                 */
                'loaderror'
            ]);

        },

        /**
         * Load data from proxy configuration to store.
         *
         * ## Example
         *
         *     store.load({
         *         url: 'http://example.com/news/list.json',
         *         // ... other proxy request configuration goes here.
         *         scope: store,
         *         callback: onLoadComplete
         *     });
         *
         * @method
         * @fires beforeload
         * @fires load
         * @fires afterload
         * @fires loaderror
         * @param {Object} options (optional) Proxy request options.
         * @return {Boolean} True if request sending else false.
         */
        load: function(opts) {
            opts = _ns.utils.applyIfAuto(opts || {}, this.getProxyOpts());
            this.isLoading = true;

            var request,
                self = this,
                type = self.getProxyType(),
                finalCb = opts.callback;

            self.fire('beforeload', self, opts);
            var cb = function(success, data) {
                var total,
                    newRecords = [];

                if (typeof data === 'string') {
                    data = JSON.parse(data);
                }

                if (success && data[self.successProperty]) {
                    total = data[self.totalProperty];
                    if (typeof total !== 'undefined') {
                        self.totalRecords = total;
                    }
                    newRecords = data[self.rootProperty];
                    if (self.enableHashTable) {
                        var recordId, length = newRecords.length;
                        while (length--) {
                            recordId = newRecords[length][self.idProperty];
                            self.hashRecords['r' + recordId] = self.loadedRecords + length;
                        }
                    }
                    self.loadedRecords += newRecords.length;
                    self.records = self.records.concat(newRecords);
                } else {
                    self.fire('loaderror', success, self, data);
                }
                self.fire('load', success, self, newRecords);
                if (finalCb) {
                    finalCb(success, self, newRecords);
                }
                self.fire('afterload', success, self, newRecords);
                self.isLoading = false;
            };

            opts.url = opts.url || self.getProxyUrl();

            if (self.storageCache) {
                var key = JSON.stringify(_ns.utils.crc32(JSON.stringify(opts))),
                    results = _storage.getItem(key);
                if (_online === false && results) {
                    setTimeout(function() {
                        cb(true, results);
                    }, 10);
                    return true;
                } else {
                    var tmp = cb;
                    cb = function(success, data) {
                        if (success && data[self.successProperty]) {
                            _storage.setItem(key, data);
                        }
                        tmp(success, data);
                    };
                }
            }

            // if (self.getProxyCache() === false) {
            //     self.addUrlTimestamp(opts.url);
            // }
            opts.callback = cb;
            if (typeof _request[type] === 'function') {
                request = _request[type];
            } else {
                request = _request.ajax;
            }
            return request(opts.url, opts, self);
        },

        /**
         * Add given record to the end of list or start of list.
         *
         * @method
         * @param {Object} record Record to add.
         * @param {Boolean} fromStart Add record from start of current records list.
         * Defaults to `false`.
         */
        addRecord: function(record, fromStart) {

            var self = this,
                recordId = record[self.idProperty];

            if (self.enableHashTable) {
                self.hashRecords['r' + recordId] = self.loadedRecords;
            }
            if (fromStart === true) {
                self.records = [record].concat(self.records);
                if (self.enableHashTable) {
                    var rec,
                        i = self.loadedRecords + 1;
                    while (i--) {
                        rec = self.records[i];
                        self.hashRecords['r' + rec[self.idProperty]] = i;
                    }
                }
            } else {
                self.records = self.records.concat([record]);
            }
            self.loadedRecords += 1;
            self.totalRecords += 1;
        },

        /**
         * Reset/Empty all data. TODO: Need to rename to `empty` ?
         *
         * @method
         */
        reset: function() {

            var self = this;

            self.records = [];
            self.totalRecords = 0;
            self.loadedRecords = 0;
            if (self.enableHashTable) {
                self.hashRecords = {};
            }
        },

        // addUrlTimestamp: function(url) {
        //     // @ignore TODO!
        //     //var date = new Date();
        // },

        /**
         * Get record from its `idProperty` value.
         * Configuration option {@link #enableHashTable enableHashTable}
         * needs to be set to `true`.
         *
         * @method
         * @param {Mixed} recordId Record id.
         * @return {Mixed} Return record if found else `undefined`.
         */
        getRecord: function(recId) {
            // <debug>
            if (!this.enableHashTable) {
                _debug.error('store', 'You must enable hash table for using store.getRecord by setting enableHashTable: true in store config.');
                return;
            }
            // </debug>
            return this.records[this.hashRecords['r' + recId]];
        },

        /**
         * Get record at given index.
         *
         * @method
         * @param {Number} index Index.
         * @return {Mixed} Return record if found else `undefined`.
         */
        getAt: function(index) {
            return this.records[index];
        },

        setProxyOpts: function(opts) {
            if (!this.config.proxy) {
                this.config.proxy = {};
            }
            this.config.proxy.opts = opts;
            return true;
        },

        getProxyOpts: function(defaultValue) {
            defaultValue = defaultValue || {};
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.opts || defaultValue;
            }
            return defaultValue;
        },

        getProxyUrl: function(defaultValue) {
            defaultValue = defaultValue || '';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.url || defaultValue;
            }
            return defaultValue;
        },

        getProxyCache: function(defaultValue) {
            defaultValue = defaultValue || true;
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return (p.cache === false ? false : defaultValue);
            }
            return defaultValue;
        },

        getProxyType: function(defaultValue) {
            defaultValue = defaultValue || 'ajax';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.type || defaultValue;
            }
            return defaultValue;
        },

        getReaderRoot: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderRoot;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.rootProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderTotal: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderTotal;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.totalProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderSuccess: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderSuccess;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.successProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderId: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderId;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.idProperty || defaultValue;
            }
            return defaultValue;
        }
    });
}());

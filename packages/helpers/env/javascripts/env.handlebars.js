{{framework.barename}}.helpers.{{ucfirst me.name}} = (function() {

    var _env = platform,
        _win = window;

    // document.addEventListener('deviceready', function() {
    //     document.removeEventListener('deviceready', arguments.callee, false);
    //     _env.phonegap = true;
    // }, false);

    if (_win.cordova || _win.PhoneGap || _win.phonegap) {
        _env.phonegap = true;
    } else {
        _env.phonegap = false;
    }

    return _env;

}());

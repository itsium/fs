/**
 * @class {{ns}}.helpers.i18n
 * @extends {{ns}}
 *
 * I18N helper.
 *
 * ### Example
 *
 *      var i18n = new {{ns}}.helpers.i18n();
 *
 *      i18n.addLang('cn', {
 *          'Highlights': '推荐',
 *          'Discover': '分类',
 *          //...
 *      });
 *
 *      i18n.setLang('cn');
 *
 *      // Then you can use global `t` function
 *      var title = t('Highlights');
 *
 *      // And `t` helper in handlebars templates:
 *      \{{ t "Highlights" }}
 */
{{ns}}.helpers.{{me.name}} = (function(gscope) {

    'use strict';

    var _ns = {{ns}},
        // stores all languages available (after load)
        _langs = {},
        // stores current language
        _currentLang = {},
        // activated language
        _lang = 'en',
        _defaultScope = gscope,
        _handlebars = Handlebars,
        _parent = _ns;

    /**
     * Translate function.
     * @private
     * @param  {String} str String to translate with current language.
     * @return {String} Translated string.
     */
    function _t(str) {
        return (_currentLang[str] || str);
    }

    if (_handlebars) {
        _handlebars.registerHelper('t', _t);
    }

    _defaultScope.t = _t;

    return _parent.subclass({

        /**
         * Set language to given name.
         * @param {String} name Language name.
         */
        setLang: function(name) {
            _lang = name;
            _currentLang = _langs[_lang];
        },

        /**
         * Add language.
         * @param {String} name       Language name
         * @param {Object} values     Translation dictionnary.
         * @param {Boolean} [setDefault=false] True to set this language as default.
         */
        addLang: function(name, values, setDefault) {
            _langs[name] = values;
            if (setDefault) {
                this.setLang(name);
            }
        }
    });

}(this));

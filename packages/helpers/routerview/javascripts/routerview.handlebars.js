/**
 * @class  {{ns}}.RouterView
 * @extends {{ns}}.Router
 * @xtype routerview
 *
 * Router helper to easily manage view by xtype.
 *
 * ### Example
 *
 *      var history = {{ns}}.History,
 *          session = App.Session,
 *          parent = {{ns}}.RouterView;
 *
 *      var routerview = new (parent.subclass({
 *
 *          xtype: 'user.router',
 *
 *          before: {
 *              'routeProfile': 'isAuthenticated'
 *          },
 *
 *          routes: {
 *              \{{ route "/user/login" as="user.login" method="routeLogin" }},
 *              \{{ route "/user/profile" as="user.profile" method="routeProfile" }}
 *          },
 *
 *          isAuthenticated: function(done) {
 *
 *              if (session.logged === false) {
 *                  history.go(\{{ url "user.login" }});
 *                  return false;
 *              }
 *
 *              // user is logged already so we continue
 *              return done();
 *          },
 *
 *          routeLogin: function() {
 *              this.setCurrentView('user.login');
 *          },
 *
 *          routeProfile: function() {
 *              this.setCurrentView('user.profile');
 *          }
 *      }));
 *
 */
{{ns}}.RouterView = (function() {

    var _ns = {{ns}},
        _history = _ns.History,
        _parent = _ns.Router,
        _events = new _ns.Event(),
        _animating = false,
        _queue = [],
        _vqueue = [];

    _events.on('queuePop', function() {

        if (!_queue.length) {
            return false;
        }

        var item = _queue.pop();

        item.router.setCurrentView(item.xtype, item.config, item.args);
    });

    function has_fx(self, xtype) {
        if (self.fx && self.fx[xtype]) {
            return true;
        }
        return false;
    }

    function get_fx(self, xtype) {
        return (self.fx[xtype]);
    }

    function fx(self, xtype, done) {

        var config = get_fx(self, xtype),
            show = config.show,
            effect = new (_ns.xtype(config.xtype))(config),
            previousView = self.getPreviousView();

        if (previousView && config[previousView.xtype]) {
            show = config[previousView.xtype];
        }

        var opts = {
            speed: show.speed,
            effect: show.me,
            el: self.currentView.el,
            callback: function() {
                self.currentView.fireScope('afterfx', self.currentView);
                done();
            }
        };

        if (previousView) {
            if (show.other) {
                opts.fromEl = previousView.el;
                opts.fromEffect = show.other;
            } else {
                previousView.hide();
            }
        }

        effect.run(opts);

    }

    return _parent.subclass({

        xtype: 'routerview',

        /**
         * Create a new router view.
         *
         * @constructor
         */
        constructor: function() {

            var self = this;

            self.views = {};
            _parent.prototype.constructor.apply(self, arguments);

        },

        /**
         * Remove view from given xtype.
         * It will simply hide the view and keep the instance.
         *
         * @param  {String} xtype View xtype
         */
        removeView: function(xtype) {

            var self = this;

            if (self.views[xtype]) {
                // self.views[xtype].remove();
                // delete self.views[xtype];
                self.views[xtype].hide();
            }

        },

        /**
         * Remove current view.
         * @fires hideCurrentView if global is not false.
         * @param  {Boolean} [global=true] Global flag to let other router view catch this event.
         */
        hideCurrentView: function(global) {

            var self = this;

            if (self.currentView) {
                self.removeView(self.currentView.xtype);
                if (self.rvid) {
                    _events.off(self.rvid);
                    self.rvid = false;
                }
            }
            if (global !== false) {
                _events.fire('hideCurrentView', false);
            }

        },

        /**
         * Add previous view to queue.
         * @param {Object} view View instance.
         */
        addPreviousView: function(view) {
            _vqueue.push(view);
        },

        /**
         * Get previous view.
         * @return {Mixed} View instance if found else false.
         */
        getPreviousView: function() {
            if (_vqueue.length > 0) {
                return _vqueue[_vqueue.length - 1];
            }
            return false;
        },

        /**
         * Hide previous view.
         * If current view is same as previous view, it will do nothing.
         * @param  {Object} currentView Current view.
         */
        hidePreviousView: function(currentView) {

            var self = this;

            currentView = currentView || self.currentView;

            if (_vqueue.length) {
                var view = _vqueue.pop();
                console.log('<<< Remove previous view: ' + view.xtype);
                if (view.config.id !== currentView.config.id) {
                    view.hide();
                }
            }

        },

        /**
         * Add given parameter to queue.
         * It is called by setCurrentView when animating is already running.
         * @param  {String} xtype  View xtype
         * @param  {Object} [config] View configuration
         * @param  {Mixed} [args]   View arguments
         */
        queue: function(xtype, config, args) {
            _queue = [];
            _queue.push({
                router: this,
                xtype: xtype,
                config: config,
                args: args
            });
        },

        /**
         * Set current view.
         * @param {String} xtype  View xtype
         * @param {Object} config Configuration for view constructor.
         * @param {Mixed} args   Arguments passed to view show event listeners.
         */
        setCurrentView: function(xtype, config, args) {

            var self = this;

            if (_animating === true) {
                return self.queue(xtype, config, args);
            }

            self.currentView = self.getView(xtype);

            var previous = self.getPreviousView();

            if (previous !== false &&
                previous.config.id === self.currentView.config.id) {
                console.log('=== Current view is same as previous view: ' + xtype);
                return self.currentView;
            }

            console.log('>>> Set current view to: ' + xtype);

            // fx stuff
            if (has_fx(self, xtype)) {
                _animating = true;
                self.currentView.show(args);
                fx(self, xtype, function() {
                    self.hidePreviousView(self.currentView);
                    self.addPreviousView(self.currentView);
                    _animating = false;
                    _events.fire('queuePop', self);
                });
            } else {
                _animating = false;
                self.currentView.show(args);
                self.hidePreviousView(self.currentView);
                self.addPreviousView(self.currentView);
            }

            return self.currentView;
        },

        /**
         * Get view from given xtype.
         * If the view does not exists, it will create new instance.
         * @param  {String} xtype View xtype
         * @return {Object}       View instance.
         */
        getView: function(xtype) {

            var self = this;

            if (!self.views[xtype]) {

                var view = self.views[xtype] = new (_ns.xtype(xtype))();

                view.compile();
                view.render('body', 'beforeend');
            }
            return self.views[xtype];
        }

    });

}());
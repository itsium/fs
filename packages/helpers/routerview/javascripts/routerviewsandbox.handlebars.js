/**
 * @class  {{ns}}.RouterViewSandbox
 * @extends {{ns}}.Router
 * @xtype routerviewsandbox
 *
 * Sandbox router helper to easily manage view by xtype.
 *
 * ### Example
 *
 *      var history = {{ns}}.History,
 *          session = App.Session,
 *          parent = {{ns}}.RouterViewSandbox;
 *
 *      var routerview = new (parent.subclass({
 *
 *          xtype: 'user.router',
 *
 *          before: {
 *              'routeProfile': 'isAuthenticated'
 *          },
 *
 *          routes: {
 *              \{{ route "/user/login" as="user.login" method="routeLogin" }},
 *              \{{ route "/user/profile" as="user.profile" method="routeProfile" }}
 *          },
 *
 *          isAuthenticated: function(done) {
 *
 *              if (session.logged === false) {
 *                  history.go(\{{ url "user.login" }});
 *                  return false;
 *              }
 *
 *              // user is logged already so we continue
 *              return done();
 *          },
 *
 *          routeLogin: function() {
 *              this.setCurrentView('user.login');
 *          },
 *
 *          routeProfile: function() {
 *              this.setCurrentView('user.profile');
 *          }
 *      }));
 *
 */
{{ns}}.RouterViewSandbox = (function() {

    var _ns = {{ns}},
        _history = _ns.History,
        _parent = _ns.Router;

    function has_fx(self, xtype) {
        if (self.fx && self.fx[xtype]) {
            return true;
        }
        return false;
    }

    function get_fx(self, xtype) {
        return (self.fx[xtype]);
    }

    function fx(self, xtype, done) {

        var config = get_fx(self, xtype),
            show = config.show,
            effect = new (_ns.xtype(config.xtype))(config),
            previousView = self.getPreviousView();

        if (previousView && config[previousView.xtype]) {
            show = config[previousView.xtype];
        }

        var opts = {
            speed: show.speed,
            effect: show.me,
            el: self.currentView.el,
            callback: done
        };

        if (previousView) {
            if (show.other) {
                opts.fromEl = previousView.el;
                opts.fromEffect = show.other;
            } else {
                previousView.hide();
            }
        }

        effect.run(opts);

    }

    return _parent.subclass({

        xtype: 'routerviewsandbox',
        hasListeners: ['queuePop'],
        listeners: {
            priority: 'VIEWS',
            queuePop: function(self) {

                if (!self.queue.length) {
                    return false;
                }

                var item = self.queue.pop();

                self.setCurrentView(item.xtype);
            }
        },

        /**
         * Create a new router view.
         *
         * @constructor
         */
        constructor: function() {

            var self = this;

            self.views = {};
            self.animating = false;
            self.vqueue = [];
            self.queue = [];

            _parent.prototype.constructor.apply(self, arguments);

        },

        /**
         * Remove view from given xtype.
         * It will simply hide the view and keep the instance.
         *
         * @param  {String} xtype View xtype
         */
        removeView: function(xtype) {

            var self = this;

            if (self.views[xtype]) {
                // self.views[xtype].remove();
                // delete self.views[xtype];
                self.views[xtype].hide();
            }

        },

        /**
         * Remove current view.
         * @fires removeCurrentView if global is not false.
         * @param  {Boolean} [global=true] Global flag to let other router view catch this event.
         */
        removeCurrentView: function(global) {

            var self = this;

            if (self.currentView) {
                self.removeView(self.currentView.xtype);
                if (self.rvid) {
                    self.off(self.rvid);
                    self.rvid = false;
                }
            }
            if (global !== false) {
                self.fire('removeCurrentView', false);
            }

        },

        /**
         * Add previous view to queue.
         * @param {Object} view View instance.
         */
        addPreviousView: function(view) {
            this.vqueue.push(view);
        },

        /**
         * Get previous view.
         * @return {Mixed} View instance if found else false.
         */
        getPreviousView: function() {

            var self = this;

            if (self.vqueue.length > 0) {
                return self.vqueue[self.vqueue.length - 1];
            }
            return false;
        },

        /**
         * Remove previous view. It will hide it.
         * If current view is same as previous view, it will do nothing.
         * @param  {Object} currentView Current view.
         */
        removePreviousView: function(currentView) {

            var self = this;

            if (self.vqueue.length) {

                var view = self.vqueue.pop();
                console.log('<<< Remove previous sandbox view: ' + view.xtype);
                if (view.config.id !== currentView.config.id) {
                    view.hide();
                }
            }

        },

        /**
         * Add given parameter to queue.
         * It is called by setCurrentView when animating is already running.
         * @param  {String} xtype  View xtype
         * @param  {Object} [config] View configuration
         * @param  {Mixed} [args]   View arguments
         */
        queue: function(xtype, config, args) {

            var self = this;

            self.queue = [];
            self.queue.push({
                router: this,
                xtype: xtype,
                config: config,
                args: args
            });
        },

        /**
         * Set current view.
         * @param {String} xtype  View xtype
         * @param {Object} config Configuration for view constructor.
         * @param {Mixed} args   Arguments passed to view show event listeners.
         */
        setCurrentView: function(xtype, config, args) {

            var self = this;

            if (self.animating === true) {
                return self.queue(xtype, config, args);
            }

            self.currentView = self.getView(xtype, config);

            var previous = self.getPreviousView();

            if (previous !== false &&
                previous.config.id === self.currentView.config.id) {
                console.log('=== Current sandbox view is same as previous view: ' + xtype);
                return self.currentView;
            }

            console.log('>>> Set current sandbox view to: ' + xtype);

            // fx stuff
            if (has_fx(self, xtype)) {
                self.animating = true;
                self.currentView.show(args);
                fx(self, xtype, function() {
                    self.removePreviousView(self.currentView);
                    self.addPreviousView(self.currentView);
                    self.animating = false;
                    _events.fire('queuePop', self);
                });
            } else {
                self.animating = false;
                self.currentView.show(args);
                self.removePreviousView(self.currentView);
                self.addPreviousView(self.currentView);
            }

            return self.currentView;
        },

        /**
         * Get view from given xtype.
         * If the view does not exists, it will create new instance.
         * @param  {String} xtype View xtype
         * @return {Object}       View instance.
         */
        getView: function(xtype, config) {

            var self = this;

            if (!self.views[xtype]) {

                var view = self.views[xtype] = new (_ns.xtype(xtype))(config);

                view.compile();
                if (!self.view) {
                    view.render('body', 'beforeend');
                } else {
                    if (self.selector) {
                        view.render(self.view.el.querySelector(self.selector), 'beforeend');
                    } else {
                        view.render(self.view.el, 'beforeend');
                    }
                }
            }
            return self.views[xtype];
        }

    });

}());
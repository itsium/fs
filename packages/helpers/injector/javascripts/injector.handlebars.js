{{framework.barename}}.helpers.{{ucfirst me.name}} = new (function() {

    'use strict';

    // private
    var _ns = {{framework.barename}},
        _doc = document,
        _ready = false,
        $ = _ns.Selector,
        _queue = [];

    function ready() {

        if (_ready === false) {
            _doc.removeEventListener('DOMContentLoaded', ready, false);
        }

        _ready = true;

        var self = _ns.helpers.{{ucfirst me.name}},
            i = 0,
            l = _queue.length;

        while (i < l) {
            self[_queue[i][0]](_queue[i][1]);
            ++i;
        }

    }

    if (['complete', 'ready', 'interactive'].indexOf(document.readyState) !== -1) {
        _ready = true;
        ready();
    } else {
        _doc.addEventListener('DOMContentLoaded', ready, false);
    }

    // public
    return _ns.subclass({

        script: function(opts) {

            if (_ready === false) {
                _queue.push(['script', arguments]);
                return false;
            }

            // var script = '<script src="' + opts.src + '"';

            // if (opts.id) {
            //     script += ' id="' + opts.id + '"';
            // }
            // script += '></script>';
            // $.addHtml($.get('body'), script);

            var script = _doc.createElement('script');

            if (opts.id) {
                script.id = opts.id;
            }
            //script.type = 'text/javascript';
            script.src = opts.src;
            _doc.body.appendChild(script);
            return true;
        },

        javascript: function(opts) {

            if (_ready === false) {
                _queue.push(['javascript', arguments]);
                return false;
            }

            var script = _doc.createElement('script');

            if (opts.id) {
                script.id = opts.id;
            }
            //script.type = 'text/javascript';
            script.innerHTML = '\n<!--\n' + opts.code + '\n// -->\n';
            _doc.body.appendChild(script);
            return true;

        },

        style: function(opts) {
            if (_ready === false) {
                _queue.push(['style', arguments]);
                return false;
            }

            var style = _doc.createElement('style');

            if (opts.id) {
                style.id = opts.id;
            }
            style.src = opts.src;
            _doc.head.appendChild(style);
            return true;
        },

        stylesheet: function(opts) {
            if (_ready === false) {
                _queue.push(['stylesheet', arguments]);
                return false;
            }

            var link = _doc.createElement('link');

            link.rel = 'stylesheet';
            link.media = 'screen';
            if (opts.id) {
                link.id = opts.id;
            }
            link.src = opts.src;
            _doc.head.appendChild(link);
            return true;
        }

    });

}())();

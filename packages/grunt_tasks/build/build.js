function task(grunt) {

    var notify = grunt.config.getRaw('notify') || {};

    notify.pack =  {
        options: {
            title: 'Task Complete',
            message: 'Pack finished !'
        }
    };

    grunt.config('notify', notify);

    var tasks = [
        'dependencies',
        'pack_generate_output_paths',
        'precompile',
        'assemble',
        'resources',
        'minify',
        'live_reload',
        'notify:pack'
    ];

    if (grunt.option('watch')  === true) {
        tasks.push('watch:pack');
    }

    grunt.registerTask('pack', 'Pack package from given configuration file.', tasks);

}

module.exports = task;
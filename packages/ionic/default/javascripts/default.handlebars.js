{{ns}}.engines.ionic = (function() {

    //'use strict';

    /* jshint -W030 */
    var _ns = {{ns}},
        _views = _ns.views,
        _event = _ns.events,
        _parent = _ns,
        $ = _ns.Selector,
        _templates = _ns.Templates,
        _handlebars = Handlebars,
        _tpls = _handlebars.templates,
        _events = {};
    /* jshint +W030 */

    _ns.global.on('ready', function() {

        var win = window,
            nav = win.navigator,
            body = document.body,
            ua = nav.userAgent;

        [
            ua.match(/iPhone OS (\d+)/i),
            ua.match(/CPU iPhone OS (\d+)/i),
            ua.match(/CPU OS (\d+)/i)
        ].forEach(function (m) {
            if (m && m.length > 1 && parseInt(m[1]) > 6) {
                $.addClass(body, 'platform-ios7');
            }
        });

        if (ua.indexOf('Android ') !== -1) {

            var android = ua.match(/Android [\d\.]+/ig),
                version = parseFloat(android[0].split(' ')[1]);

            if (version < 4.4) {
                $.addClass(body, 'grade-' + (version < 4 ? 'c' : 'b'));
            }

            $.addClass(body, 'platform-android');
        }
        if (nav.standalone === true ||
            typeof win.cordova !== 'undefined' ||
            typeof win.phonegap !== 'undefined') {
            $.addClass(body, 'platform-cordova');
        } else {
            win.addEventListener('deviceready', function() {
                $.addClass(body, 'platform-cordova');
            }, false);
        }

        var focusin = function() {
            $.addClass(body, 'keyboard-open');
        };

        var focusout = function() {
            $.removeClass(body, 'keyboard-open');
        };

        if (ua.indexOf('Android ') !== -1) {

            var mustHide = false;

            win.addEventListener('click', function(evt) {

                if (['SELECT', 'INPUT', 'TEXTAREA'].indexOf(evt.target.tagName) > -1) {
                    mustHide = true;
                    focusin();
                } else if (mustHide === true) {
                    mustHide = false;
                    focusout();
                }

            }, false);
        } else {
            win.addEventListener('focusin', focusin, false);
            win.addEventListener('focusout', focusout, false);
        }

    });

    function findParent(el, callback) {
        if (callback(el) === false) {
            if (el.parentNode) {
                return findParent(el.parentNode, callback);
            }
            return false;
        }
        return el;
    }

    function activateButton(self, event) {

        var activeEl = self.el.querySelector('.active');

        if (activeEl) {
            $.removeClass(activeEl, 'active');
        }

        findParent(event.target, function(el) {
            if (el.tagName === 'BUTTON') {
                $.addClass(el, 'active');
                return true;
            }
            return false;
        });
    }

    function activateTab(self, event) {

        var activeEl = self.el.querySelector('.active');

        var parent = findParent(event.target, function(el) {
            if ($.hasClass(el, 'tab-item')) {
                return true;
            }
            return false;
        });

        if (parent && parent !== activeEl) {
            if (activeEl) {
                $.removeClass(activeEl, 'active');
            }
            $.addClass(parent, 'active');
        }
    }

    var activeListItem = function(self, event) {

        findParent(event.target, function(el) {
            if ($.hasClass(el, 'item')) {
                $.addClass(el, 'active');

                var activeEl = self.el.querySelector('.active');

                if (activeEl) {
                    $.removeClass(activeEl, 'active');
                }

                var sid = setTimeout(function() {
                    $.removeClass(el, 'active');
                    clearTimeout(sid);
                }, 500);

                return true;
            }
            return false;
        });

    };

    _events.list = _events.listbuffered = {
        itemselect: activeListItem
    };

    _events.controlgroup = {
        select: activateButton
    };

    _events.toggle = {
        change: function(scope, component, checked) {

            var el = component.el.querySelector('input[type]');

            if (checked === true) {
                el.setAttribute('checked', '');
            } else {
                el.removeAttribute('checked');
            }
        }
    };

    _events.tabs = {
        select: activateTab,
        activate: function(self, name) {

            var el = self.el.querySelector('a[data-name="' + name + '"]');

            if (el) {
                activateTab(self, {
                    target: el
                });
            }

        }
    };

    _events.button = {
        seticon: function(self, icon) {
            if (icon === self.config.icon) {
                return false;
            }
            $.removeClass(self.el, 'ion-' + self.config.icon);
            $.addClass(self.el, 'ion-' + icon);
            self.config.icon = icon;
        }
    };

    _events.template = {
        show: function(self) {
            if (self.el) {
                $.removeClass(self.el, 'hidden');
            }
        },
        hide: function(self) {
            if (self.el) {
                $.addClass(self.el, 'hidden');
            }
        }
    };

    // helpers for setUI methods
    function _removeUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            $.removeClass(el, pattern.replace('#{ui}', ui[len]));
        }
    }

    function _addUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            $.addClass(el, pattern.replace('#{ui}', ui[len]));
        }
    }

    var _setUIBtn = function(ui) {

        var self = this;

        _removeUI('button-#{ui}', self.config.ui, self.el);
        _addUI('button-#{ui}', ui, self.el);

        self.config.ui = ui;
    };

    _ns.global.on('button_ready', function() {

        if (_views.Button) {
            _views.Button.prototype.setUI = _setUIBtn;
        }

    });

    return _parent.subclass({

        xtype: 'ionic',
        className: '{{ns}}.engines.ionic',
        version: '0.9.14',

        getTpl: function(name) {
            if (_tpls[name]) {
                return _tpls[name];
            }
            console.warn(this.className +
                ' engine: Template "%s" does not exists.', name);
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

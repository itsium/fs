/**
 * @class {{ns}}.ionic.ui.Textfield
 * @xtype textfield
 * @extends {{ns}}.views.Textfield
 *
 * ### Example
 *
 *      @example ionic_page
        var example = {
            xtype: 'textfield',
            config: {
                label: 'Username',
                icon: 'ion-user',
                placeHolder: 'remi'
            }
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {String} [config.type="text"] Input type:
 *  <ul>
 *   <li>text</li>
 *   <li>password</li>
 *   <li>color</li>
 *   <li>date</li>
 *   <li>datetime</li>
 *   <li>datetime-local</li>
 *   <li>email</li>
 *   <li>month</li>
 *   <li>number</li>
 *   <li>range</li>
 *   <li>search</li>
 *   <li>tel</li>
 *   <li>time</li>
 *   <li>url</li>
 *   <li>week</li>
 *  </ul>
 * @cfg {String} [config.label] Field label.
 * @cfg {Boolean} [config.labelInline=true] Put label inline. False to disable it.
 * @cfg {Boolean} [config.disabled=false] Set field as disabled.
 * @cfg {Boolean} [config.required=false] Set field as required.
 * @cfg {Boolean} [config.readonly=false] Set field as readonly.
 * @cfg {String} [config.inputStyle] Input inline style.
 * @cfg {String} [config.autocomplete] Autocomplete attribute value.
 * @cfg {String} [config.name] Input name attribute value.
 * @cfg {String} [config.pattern] Input pattern attribute value.
 * @cfg {String} [config.value] Input value attribute value.
 * @cfg {String} [config.placeHolder] Input placeHolder attribute value.
 * @cfg {Object} [config.attrs] Input attributes object.
 * Example:
 *
 * 		attrs: {
 * 			required: "required",
 * 			onclick: "javascript:alert(1);"
 * 		}
 *
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 */
/**
 * @class {{ns}}.ionic.ui.Row
 * @extends {{ns}}.views.Template
 *
 * Ionic row template.
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = {
 *      	xtpl: 'row',
 *          items: [{
 *          	xtpl: 'container',
 *          	config: {
 *           		col: 75
 *          	},
 *          	items: 'This is a 75% width template'
 *          }, {
 *          	xtpl: 'container',
 *          	items: 'This is a 25% width container'
 *          }]
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

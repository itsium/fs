/**
 * @class {{ns}}.ionic.ui.HeaderTitle
 * @extends {{ns}}.views.Template
 *
 * Header title template.
 * Used throught `xtpl`.
 *
 * ### Example
 *
 *      var component = new {{ns}}.views.Header({
 *          items: [{
 *          	xtype: 'button',
 *          	config: {
 *          		text: 'Just a button'
 *          	}
 *          }, {
 *          	xtpl: 'headertitle',
 *          	items: 'My title or other components'
 *          }]
 *      });
 *
 * 		// or
 * 		items: [{
 * 			xtype: 'header',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: [{
 * 				xtpl: 'headertitle',
 *     			items: [{
 *     				xtpl: 'button',
 *     				config: {
 *     					text: 'Title is a button'
 *     				}
 *     			}]
 * 			}]
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

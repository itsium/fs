/**
 * @class {{ns}}.ionic.ui.Textarea
 * @xtype textarea
 * @extends {{ns}}.views.Textarea
 *
 * ### Example
 *
 *      @example ionic_page
        var example = {
            xtype: 'textarea',
            config: {
                label: 'Description',
                placeHolder: 'Lot of content...',
                attrs: {
					rows: 10
                }
            }
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {String} [config.label] Field label.
 * @cfg {Boolean} [config.labelInline=true] Put label inline. False to disable it.
 * @cfg {Boolean} [config.disabled=false] Set field as disabled.
 * @cfg {Boolean} [config.required=false] Set field as required.
 * @cfg {Boolean} [config.readonly=false] Set field as readonly.
 * @cfg {String} [config.inputStyle] Textarea inline style.
 * @cfg {String} [config.autocomplete] Autocomplete attribute value.
 * @cfg {String} [config.name] Textarea name attribute value.
 * @cfg {String} [config.pattern] Textarea pattern attribute value.
 * @cfg {String} [config.value] Textarea value.
 * @cfg {String} [config.placeHolder] Textarea placeHolder attribute value.
 * @cfg {Object} [config.attrs] Textarea attributes object.
 * Example:
 *
 * 		attrs: {
 * 			required: "required",
 * 			onclick: "javascript:alert(1);"
 * 		}
 *
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 */
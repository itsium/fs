/**
 * @class {{ns}}.ionic.ui.Page
 * @extends {{ns}}.views.Page
 *
 * ### Example
 *
 * 		@example ionic_container
 *      var example = {
 *      	xtype: 'page',
 *          items: [{
 *          	xtype: 'header',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Header title'
 *          	}
 *          }, {
 *          	xtype: 'footer',
 *          	config: {
 *          		ui: 'positive',
 *          		title: 'Footer title'
 *          	}
 *          }]
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

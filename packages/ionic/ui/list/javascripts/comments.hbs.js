/**
 * @class {{ns}}.ionic.ui.List
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.List
 *
 * ### Example
 *
 * 		items: [{
 * 			xtpl: 'list'
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 */
/**
 * @class {{ns}}.ionic.ui.Listitemavatar
 *
 * ### Example
 *
 * 		@example ionic_page
 * 		var example = {
 * 			xtype: 'list',
 * 			itemTpl: 'listitemavatar',
 * 			items: [{
 * 				image: "http://ionicframework.com/img/docs/venkman.jpg",
 * 				title: "The guy",
 * 				desc: "The guy description and all"
 * 			}, {
 * 				image: "http://ionicframework.com/img/docs/barrett.jpg",
 * 				title: "The girl",
 * 				desc: "The girl description and all"
 * 			}]
 * 		};
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 */
/**
 * @class {{ns}}.ionic.ui.Button
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Button
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = new {{ns}}.views.Button({
 *          config: {
 *              ui: 'calm',
 *              icon: 'loop',
 *              iconPos: 'right',
 *              text: 'Reload'
 *          },
 *          handler: function() {
 *              // form.submit();
 *          }
 *      });
 *
 * ### Different buttons
 *
 * 		@example ionic_page
 *      var example = [{
 *      	xtype: 'button',
 *      	config: {
 *      		ui: ['block', 'balanced'],
 *      		text: 'Block button'
 *      	}
 *      }, {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: ['small', 'stable'],
 *      		icon: 'ios7-gear-outline'
 *      	}
 *      }, '&nbsp;&nbsp;', {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: 'assertive',
 *      		active: true,
 *      		icon: 'ios7-cart',
 *      		text: 'Buy it'
 *      	}
 *      }, '&nbsp;&nbsp;', {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: ['outline', 'large'],
 *      		text: 'Outline & large button'
 *      	}
 *      }, '&nbsp;&nbsp;', {
 *      	xtype: 'button',
 *      	config: {
 *      		ui: 'clear',
 *      		text: 'Clear button'
 *      	}
 *      }];
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String|Array} [config.ui] UI theme:
 * <ul>
 *  <li>light</li>
 *  <li>stable</li>
 *  <li>positive</li>
 *  <li>calm</li>
 *  <li>balanced</li>
 *  <li>energized</li>
 *  <li>assertive</li>
 *  <li>royal</li>
 *  <li>dark</li>
 * </ul>
 * But also:
 * <ul>
 *  <li>small (for small size button)</li>
 *  <li>large (for large size button)</li>
 *  <li>outline</li>
 *  <li>clear</li>
 * </ul>
 * @cfg {String} [config.icon] Icon name (without ion-) (from http://ionicons.com)
 * @cfg {String} [config.iconPos="left"] Icon position:
 * <ul>
 *  <li>left</li>
 *  <li>right</li>
 * </ul>
 * @cfg {Boolean} [config.active=false] Set button as active
 * @cfg {Boolean} [config.hidden=false] Hide the button
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {String} [config.text] Button text
 */
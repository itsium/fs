/**
 * @class {{ns}}.ionic.ui.Alert
 * @extends {{ns}}.views.Alert
 * @xtype alert
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = new {{ns}}.views.Alert({
 *          config: {
 *              title: 'Error',
 *              message: 'Something went wrong'
 *          }
 *      });
 */
(function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _alert = _ns.views.Alert.prototype;

    /**
     * @cfg {Object} [config] Template configuration
     * @cfg {String} [config.cssClass] Additional CSS classes
     * @cfg {String} [config.style] Inline CSS style
     * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
     * @cfg {String} [config.title] Alert header title
     * @cfg {String} [config.message] Alert message.
     * @cfg {Mixed} [config.items] Alert body items.
     * @cfg {String} [config.overlay] Alert backdrop overlay
     * @cfg {Boolean} [config.overlayHide] Hide alert when clicking/tapping on overlay layer.
     */

    _alert.show = function() {

        var self = this;

        $.removeClass(self.overlayEl, 'hidden');
        $.removeClass(self.el, 'hidden');

        var top = (self.el.offsetHeight / 2),
            left = (self.el.offsetWidth / 2);

        self.el.setAttribute('style', ('margin-top: -' + top + 'px;' +
            'margin-left: -' + left + 'px;'));

        $.addClasses(self.el, 'popup-showing active');

        self.fire('show', self);
    };

    _alert.hide = function() {

        var self = this;

        $.removeClasses(self.el, 'active');
        $.addClass(self.el, 'popup-hidden');
        $.addClass(self.overlayEl, 'hidden');

        var tid = setTimeout(function() {
            $.addClass(self.el, 'hidden');
            $.removeClass(self.el, 'popup-hidden');
            clearTimeout(tid);

            self.fire('hide', self);
        }, 150);
    };

}());
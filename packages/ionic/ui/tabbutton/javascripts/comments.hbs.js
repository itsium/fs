/**
 * @class {{ns}}.ionic.ui.Tabbutton
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Tabbuton
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    active: true,
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {Boolean} [config.active=false] True to set button as active.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 * @cfg {String} [config.name] Tab button name. Usefull for activate tab from name.
 * @cfg {String} [config.route] Tab route without the "#".
 * @cfg {String} [config.text] Tab button text.
 */
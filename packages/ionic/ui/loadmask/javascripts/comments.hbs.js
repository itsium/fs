/**
 * @class {{ns}}.ionic.ui.Loadmask
 * @extends {{ns}}.views.Loadmask
 *
 * Loadmask component.
 *
 * This class implements {@link {{ns}}.implements.events events}.
 *
 * ### Sample usage
 *
 *      @example
 *      var loader = new {{ns}}.views.Loadmask({
 *          lock: true
 *      });
 *      loader.compile();
 *      loader.render('body');
 *
 */

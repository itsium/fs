/**
 * @class {{ns}}.ionic.ui.Card
 * @extends {{ns}}.views.Template
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = {
 *          xtpl: 'card',
 *          config: {
 *          	header: 'Header',
 *          	footer: 'Footer'
 *          },
 *          items: [{
 *          	xtpl: 'container',
 *          	config: {
 *          		title: 'Card 1'
 *          	},
 *          	items: '<p>Card 1 content</p>'
 *          }, {
 *          	xtpl: 'container',
 *          	config: {
 *          		title: 'Card 2'
 *          	},
 *          	items: '<p>Card 2 content</p>'
 *          }]
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {String} [config.header] Card header.
 * @cfg {String} [config.footer] Card footer.
 */

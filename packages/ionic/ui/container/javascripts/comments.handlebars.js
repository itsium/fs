/**
 * @class {{ns}}.ionic.ui.Container
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Container
 *
 * ### Example
 *
 *      var container = new {{ns}}.views.Container({
 *          config: {
 *          	style: 'background-color: black;'
 *          },
 *          items: '<p>Hello</p>'
 *      });
 *
 * 		// should better use this way when just for template:
 * 		items: [{
 * 			xtpl: 'container'
 * 		}]
 * 		// or
 * 		var container = new {{ns}}.views.Template({
 * 			xtpl: 'container',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: '...'
 * 		});
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 */
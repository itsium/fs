/**
 * @class {{ns}}.ionic.ui.Select
 * @xtype select
 * @extends {{ns}}.views.Select
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = {
 *          xtype: 'select',
 *          config: {
 *              label: 'Gender',
 *              value: 'm',
 *              options: [{
 *                  label: 'Male',
 *                  value: 'm'
 *              }, {
 *                  label: 'Female',
 *                  value: 'f'
 *              }]
 *          }
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {String} [config.label] Field label.
 * @cfg {Boolean} [config.labelInline=true] Put label inline. False to disable it.
 * @cfg {Boolean} [config.disabled=false] Set field as disabled.
 * @cfg {Boolean} [config.required=false] Set field as required.
 * @cfg {Boolean} [config.readonly=false] Set field as readonly.
 * @cfg {String} [config.inputStyle] Textarea inline style.
 * @cfg {String} [config.autocomplete] Autocomplete attribute value.
 * @cfg {String} [config.name] Textarea name attribute value.
 * @cfg {String} [config.pattern] Textarea pattern attribute value.
 * @cfg {String} [config.value] Textarea value.
 * @cfg {String} [config.placeHolder] Textarea placeHolder attribute value.
 * @cfg { {{ns}}.data.Store } [config.store] Data store.
 * @cfg {Object} [config.attrs] Textarea attributes object.
 * Example:
 *
 * 		attrs: {
 * 			required: "required",
 * 			onclick: "javascript:alert(1);"
 * 		}
 *
 * @cfg {Array} [config.options] Options for select. Example:
 *
 *      [{
 *          label: 'Option one',
 *          value: 1
 *      }, {
 *          label: 'Option two',
 *          value: 2
 *      }, ...]
 *
 * or only values (in this case, label will be the value)
 *
 *      [1, 2]
 *
 * @cfg {String} [config.icon] Raw icon name ("ion-" must be include in the given name).
 */
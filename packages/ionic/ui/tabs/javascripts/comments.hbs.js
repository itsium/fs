/**
 * @class {{ns}}.ionic.ui.Tabs
 * @xtype {{xtype type=false}}
 * @extends {{ns}}.views.Tabs
 *
 * ### Example
 *
 *      @example ionic_container
        var example = {
            xtype: 'tabs',
            handler: false,
            config: {
                iconPos: 'left'
            },
            items: [{
                xtype: 'tabbutton',
                config: {
                    text: 'Home',
                    name: 'home',
                    icon: 'ion-home',
                    route: '/home'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Star',
                    name: 'favorites',
                    icon: 'ion-star',
                    route: '/favorites'
                }
            }, {
                xtype: 'tabbutton',
                config: {
                    text: 'Settings',
                    name: 'settings',
                    icon: 'ion-gear-a',
                    route: '/settings'
                }
            }]
        };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {Mixed} [config.ui] UI theme string or array.
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {String} [config.iconPos] Icon position:
 * <ul><li>left: icons on left of text.</li><li>top: icons on top of text.</li><li>only: only icons.</li></ul>
 */
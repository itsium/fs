/**
 * @class {{ns}}.ionic.ui.Content
 * @extends {{ns}}.views.Template
 *
 * ### Example
 *
 *      @example ionic_page
 *      var example = {
 *      	xtpl: 'content',
 *          config: {
 *          	style: 'background-color: black;'
 *          },
 *          items: '<p style="color: white;">Hello</p>'
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.hidden=false] Hide the component on render.
 * @cfg {Boolean} [config.padding=true] Add padding to content.
 * @cfg {Boolean} [config.hasHeader=false] Set to true if page has header.
 * @cfg {Boolean} [config.hasFooter=false] Set to true if page has footer.
 * @cfg {Boolean} [config.hasSubHeader=false] Set to true if page has sub-header.
 */

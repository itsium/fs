/**
 * @class {{ns}}.ionic.ui.Footer
 * @xtype footer
 * @extends {{ns}}.views.Footer
 *
 * ### Example
 *
 * 		@example ionic_container
 *      var example = {
 *      	xtype: 'footer',
 *          config: {
 *          	ui: 'positive',
 *          	title: 'Hello'
 *          }
 *      };
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Boolean} [config.ui] Bar UI theme:
 * <ul>
 *  <li>positive</li>
 *  <li>calm</li>
 *  <li>balanced</li>
 *  <li>energized</li>
 *  <li>assertive</li>
 *  <li>royal</li>
 *  <li>dark</li>
 * </ul>
 * @cfg {String} [config.title] Bar title.
 */

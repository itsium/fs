/**
 * @class {{ns}}.ionic.ui.Bubble
 * @extends {{ns}}.views.Template
 *
 * Ionic bubble template.
 *
 * ### Example
 *
 * 		@example ionic_page
 *      var example = [{
 *      	xtpl: 'bubble',
 *      	config: {
 *      		ui: 'assertive',
 *      		label: '99+'
 *      	}
 *      }, '<br><br>', {
 *      	xtpl: 'bubble',
 *      	config: {
 *      		ui: 'positive',
 *      		label: 'important'
 *      	}
 *      }];
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 * @cfg {Mixed} [config.ui] UI theme for bubble. Can be string or array of string.
 * @cfg {String} [config.label] Label for bubble.
 */

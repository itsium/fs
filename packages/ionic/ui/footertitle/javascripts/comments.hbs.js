/**
 * @class {{ns}}.ionic.ui.FooterTitle
 * @extends {{ns}}.views.Template
 *
 * Footer title template.
 * Used throught `xtpl`.
 *
 * ### Example
 *
 *      var component = new {{ns}}.views.Footer({
 *          items: [{
 *          	xtype: 'button',
 *          	config: {
 *          		text: 'Just a button'
 *          	}
 *          }, {
 *          	xtpl: 'footertitle',
 *          	items: 'My title or other components'
 *          }]
 *      });
 *
 * 		// or
 * 		items: [{
 * 			xtype: 'footer',
 * 			config: {
 * 				// ...
 * 			},
 * 			items: [{
 * 				xtpl: 'footertitle',
 *     			items: [{
 *     				xtpl: 'button',
 *     				config: {
 *     					text: 'Title is a button'
 *     				}
 *     			}]
 * 			}]
 * 		}]
 */

/**
 * @cfg {Object} [config] UI configuration
 * @cfg {String} [config.cssClass] Additional CSS classes
 * @cfg {String} [config.style] Inline CSS style
 * @cfg {String} [config.id] View id. If none, {{ns}} will automatically generate one.
 * @cfg {Mixed} [config.items] Child items.
 */

{{classname}} = (function() {

    'use strict';

    // private
    var _working = false,
        _ns = {{ns}},
        _parent = _ns.Event,
        $ = _ns.Selector,
        _animationEnd = $.prefix.jsEvent('animationEnd'),
        _doc = document;

    Browser.tests.Cssanim(function(success) {
        _working = success;
    });

    function add_style(speed) {

        var id = '{{xtype}}-' + speed,
            exists = _doc.getElementById(id);

        if (exists) {
            return exists;
        }

        var style = _doc.createElement('style');

        style.id = id;

        return _doc.head.appendChild(style);

    }

    function removeAnimation(self, config) {

        var speed = config.speed,
            cssClass = (typeof speed !== 'undefined' ?
                ('custom_animated' + speed.replace('.', '_')) : 'animated');

        var foo = function() {

            if (config.fromEl) {
                $.removeClasses(config.fromEl,
                    [cssClass, config.fromEffect].join(' '));
            }

            $.removeClasses(config.el,
                [cssClass, config.effect].join(' '));

            $.removeClass($.get('body'), 'startanim');

            this.removeEventListener(_animationEnd, foo, false);

            if (speed) {
                clear_speed(speed);
            }

            self.animating = false;

            if (typeof config.callback === 'function') {
                config.callback();
            }
        };

        return foo;

    }

    function add_speed(speed) {

        var style = add_style(speed);

        style.innerHTML = [
            '.startanim .custom_animated', speed.replace('.', '_'), ' {',
                $.prefix.css, 'animation-duration:', speed, ';',
                'animation-duration:', speed, ';',
                $.prefix.css, 'animation-fill-mode:both;',
                'animation-fill-mode:both;',
            '}'
        ].join('');
    }

    function clear_speed(speed) {

        var style = _doc.getElementById('{{xtype}}-' + speed);

        if (style) {
            style.parentNode.removeChild(style);
        }

    }

    // public
    return _parent.subclass({

        className: '{{classname}}',
        xtype: '{{xtype}}',

        constructor: function(opts) {

            var self = this;

            self.opts = opts;
            _parent.prototype.constructor.apply(self, arguments);
        },

        /**
        fromEffect (optional) {String} effect name
        fromEl (optional) {HtmlElement}
        effect {String}
        el {HtmlElement}
        callback {Function}
        */
        run: function(config) {

            if (_working === false) {
                return (typeof config.callback === 'function' ?
                    config.callback() : false);
            }

            var self = this;

            if (self.animating === true) {
                return false;
            }

            self.animating = true;

            var speed = config.speed,
                cssClass = (typeof speed !== 'undefined' ?
                    ('custom_animated' + speed.replace('.', '_')) :
                    'animated');

            if (speed) {
                add_speed(speed);
            }

            config.el.addEventListener(_animationEnd,
                removeAnimation(self, config), false);

            if (config.fromEl) {
                $.addClasses(config.fromEl,
                    [cssClass, config.fromEffect].join(' '));
            }

            $.addClasses(config.el,
                [cssClass, config.effect].join(' '));
            $.addClass($.get('body'), 'startanim');

        }

    });
})();

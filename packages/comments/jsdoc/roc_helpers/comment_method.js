(function() {

    var api = module.exports = {

        handlebars: undefined,
        grunt: undefined,

        name: 'comment_method',

        /**
         * {{#comment}}
         * /**
         *  * {{comment_method "my_method"}}
         *  *-/
         * {{/comment}}
         */
        helper: function(context, options) {
            return new api.handlebars.SafeString('@method ' + context);
        }

    };

}());

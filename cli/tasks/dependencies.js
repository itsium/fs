'use strict';

function task(grunt) {

    grunt.registerTask('dependencies', 'Get all given config dependencies.', function() {

        var roc = require('./../config').parse(grunt);

        grunt.config('roc', roc);

        if (roc.current.roc_dependencies) {

            var package_api = require('../api/package'),
                current = package_api.get_name_from_path(grunt.option('config'));

            roc.current.roc_dependencies[current] = roc.current.version;
            grunt.config('roc_dependencies', roc.current.roc_dependencies);
            grunt.task.run('roc_dependencies');
        }

    });

}

module.exports = task;
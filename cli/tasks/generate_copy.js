function task(grunt) {

    var path = require('path'),
        async = require('async'),
        package_api = require('./../api/package'),
        utils = require('./../api/utils');

    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('generate_copy', 'Copy all files by their types.', function() {

        var done = this.async(),
            extensions = {
            javascripts: '.js',
            stylesheets: '.css',
            htmls: '.html',
            templates: '.js',
            images: '',
            resources: '',
            handlebars_partials: '.js'
        },
        packages = package_api.store,
        files = packages.files;

        var copy = grunt.config.getRaw('copy') || {};

        async.each(Object.keys(files), function(pkg_name, next) {

            var pkg_files = files[pkg_name],
                pkg = packages.includes[pkg_name],
                base = '<%= roc.paths.output %>' + pkg_name + path.sep;

            Object.keys(extensions).forEach(function(ext) {

                if (pkg_files[ext]) {

                    var copy = grunt.config.getRaw('copy') || {};

                    copy[pkg_name + '-' + ext] = {
                        nonull: true,
                        expand: true,
                        cwd: pkg.paths.cache,
                        src: pkg_files[ext],
                        dest: base
                    };

                    //console.log(copy);

                    grunt.config('copy', copy);
                    grunt.task.run('copy:' + pkg_name + '-' + ext);

                }

            });

            next();

        }, function(err) {
            //grunt.config('copy', copy);
            process.nextTick(done);
        });

    });

}

module.exports = task;
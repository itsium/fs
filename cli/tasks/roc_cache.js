'use strict';

module.exports = function(grunt) {

    var cache = require('../api/cache'),
        path = require('path');

    grunt.registerMultiTask('roc_cache', 'Cache given files.', function() {

        var data = this.data,
            done = this.async(),
            processed_file = path.resolve(data.processed);

        cache.add_package_file(data.pkg, data.file, processed_file, done);

    });

};

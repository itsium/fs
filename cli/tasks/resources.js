function task(grunt) {

    grunt.registerTask('resources', 'Copy all resources and images to destination.', function() {

        var async = require('async'),
            done = this.async(),
            package_api = require('../api/package'),
            packages = package_api.store,
            path = require('path'),
            utils = require('./../api/utils');

        var filetypes = [
            'images',
            'resources'
        ], files = packages.files;

        // copy options
        // options: {
        // // exclude binary format from the processContent function
        //  processContentExclude: [
        //      '**/*.{png,gif,jpg,ico,psd,ttf,otf,woff,svg}'
        //  ]
        // },

        var copy = grunt.config.getRaw('copy') || {};

        async.each(packages.orders, function(pkg_name, next) {

            var pkg_files = files[pkg_name],
                pkg = packages.includes[pkg_name];

            filetypes.forEach(function(ft) {

                if (pkg_files[ft]) {

                    copy[ft + '-' + pkg.id] = {
                        nonull: true,
                        expand: true,
                        //flatten: true,
                        cwd: pkg.paths.cwd,
                        src: pkg_files[ft],
                        dest: '<%= roc.paths.output %>'
                    };

                    //grunt.config('copy', copy);
                    grunt.task.run('copy:' + ft + '-' + pkg.id);

                }

            });

            next();

        }, function(err) {
            grunt.config('copy', copy);
            process.nextTick(done);
        });

    });

}

module.exports = task;
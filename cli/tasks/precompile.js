var utils = require('../api/utils'),
    path = require('path');

function add_package_file_to_watch(pkg, files, to_watch) {

    Object.keys(files).forEach(function(ft) {

        var pkg_files = utils.prepend(pkg.paths.cwd, files[ft]);

        to_watch = to_watch.concat(pkg_files);

    });

    to_watch.push(path.resolve(pkg.paths.root, pkg.paths.config));

    return to_watch;

}


function task(grunt) {

    grunt.registerTask('precompile', 'Precompile all files by their extensions.', function() {

        var done = this.async(),
            async = require('async'),
            package_api = require('../api/package'),
            packages = package_api.store,
            precompiler = require('../api/precompiler'),
            cache = require('../api/cache'),
            config_dir = path.dirname(grunt.option('config')),
            to_watch = [];

        var empty_cache = (grunt.option('cache') === false);

        cache.change_directory(path.resolve(config_dir, cache.dirname + '/'), {
            empty: empty_cache,
            callback: function() {

                if (!packages.orders.length) {
                    return process.nextTick(done);
                }

                precompiler.register_roc_utilities(function() {

                    async.eachLimit(packages.orders, 1, function(pkgname, next) {

                        var pkg = packages.includes[pkgname];

                        package_api.resolve_files(pkg, function(success, files) {

                            to_watch = add_package_file_to_watch(pkg, files, to_watch);

                            if (!Object.keys(files)) {
                                // no files founds could be strange
                                // it means maybe packages only have resources or images files.
                                return next();
                            }

                            precompiler.run(grunt, pkg, function() {
                                next();
                            });

                        });

                    }, function() {

                        if (grunt.option('watch') === true) {

                            var watch = grunt.config.getRaw('watch') || {};

                            watch.pack.files = to_watch;
                            watch.generate.files = to_watch;
                            grunt.config('watch', watch);
                        }

                        process.nextTick(done);
                    });

                });

            }
        });

    });

}

module.exports = task;
function task(grunt) {

    var notify = grunt.config.getRaw('notify') || {};

    notify.generate =  {
        options: {
            title: 'Task Complete',
            message: 'Generate finished !'
        }
    };

    grunt.config('notify', notify);

    var tasks = [
        'dependencies',
        //'pack_generate_output_paths',
        'precompile',
        'generate_copy',
        'notify:generate'
    ];

    if (grunt.option('watch')  === true) {
        tasks.push('watch:generate');
    }

    grunt.registerTask('generate', 'Generate package from given configuration file.', tasks);

}

module.exports = task;
/*
* grunt-roc-dependencies
*
*
* Copyright (c) 2014 bydooweedoo
* Licensed under the MIT license.
*/

'use strict';

module.exports = function(grunt) {

    var path = require('path'),
        api_path = path.resolve(process.cwd(), 'cli/api'),
        api = require(api_path + '/package'),
        description = 'ROC dependencies management.',
        config_file = path.resolve(grunt.option('config'));

    api.add_from_config(config_file, function() {});

    grunt.registerMultiTask('roc_dependencies', description, function() {

        var done = this.async(),
            roc = grunt.config.getRaw('roc'),
            options = this.options({
                recursive_paths: true,
                paths: [
                    path.resolve(roc.paths.framework, 'packages/'),
                    roc.paths.config_file
                ]
            }),
            pkg_name = this.target,
            pkg_version = this.data;

        api.logger(grunt.log);

        api.list(options.paths, function(success, pkgs) {
            console.log('#' + pkg_name + '$');
            api.include(pkg_name, pkg_version);
            process.nextTick(done);

        }, options);

    });

};

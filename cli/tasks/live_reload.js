function task(grunt) {

    // @TODO shengjie specific !!!!!!!!
    // must put inside package !
    grunt.registerTask('live_reload', 'Live reload.', function() {

        var must_reload = !(grunt.option('live_reload') === undefined);

        if (must_reload === true) {

            var path = require('path'),
                fs = require('fs'),
                ts = (new Date()).getTime();

            fs.writeFile(path.resolve(grunt.option('live_reload')), ts, function(err) {
                grunt.log.ok('Live reload : Chaned '+ grunt.option('live_reload'));
            });
        }

    });

}

module.exports = task;
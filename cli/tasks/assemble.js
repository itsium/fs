function task(grunt) {

    var path = require('path'),
        async = require('async'),
        package_api = require('./../api/package'),
        utils = require('./../api/utils');

    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('assemble', 'Assemble all files by their types.', function() {

        var done = this.async(),
            extensions = {
            javascripts: '.js',
            stylesheets: '.css',
            htmls: '.html'
        }, filenames = {
            base: [
                '<%= roc.paths.output %>',
                '<%= roc.current.name',
                ' + "@" + ',
                ' roc.current.version %>'
            ].join(''),
            htmls: [
                '<%= roc.paths.output %>',
                'index',
            ].join(''),
        }, all_files = {
            javascripts: [],
            stylesheets: [],
            htmls: []
        },
        packages = package_api.store,
        files = packages.files;

        var concat = grunt.config.getRaw('concat') || {};

        async.each(Object.keys(files), function(pkg_name, next) {

            var pkg_files = files[pkg_name],
                pkg = packages.includes[pkg_name];

            if (pkg_files.templates) {
                all_files.javascripts = all_files.javascripts.concat(
                    utils.prepend(pkg.paths.cache + path.sep, pkg_files.templates)
                );
            }

            if (pkg_files.handlebars_partials) {
                all_files.javascripts = all_files.javascripts.concat(
                    utils.prepend(pkg.paths.cache + path.sep, pkg_files.handlebars_partials)
                );
            }

            if (pkg_files.javascripts) {
                all_files.javascripts = all_files.javascripts.concat(
                    utils.prepend(pkg.paths.cache + path.sep, pkg_files.javascripts)
                );
            }

            if (pkg_files.stylesheets) {
                all_files.stylesheets = all_files.stylesheets.concat(
                    utils.prepend(pkg.paths.cache + path.sep, pkg_files.stylesheets)
                );
            }

            if (pkg_files.htmls) {
                all_files.htmls = all_files.htmls.concat(
                    utils.prepend(pkg.paths.cache + path.sep, pkg_files.htmls)
                );
            }

            next();

        }, function(err) {

            packages.assemble = {};

            async.each(Object.keys(all_files), function(filetype, nextFiletype) {

                var filename = filenames[filetype] || filenames.base;

                packages.assemble[filetype] = filename + extensions[filetype];

                concat[filetype] = {
                    src: all_files[filetype],
                    dest: packages.assemble[filetype]
                };

                //grunt.config('concat', concat);
                grunt.task.run('concat:' + filetype);

                nextFiletype();

            }, function(err) {
                // finish concat
                grunt.config('concat', concat);
                process.nextTick(done);
            });

        });

    });

}

module.exports = task;
function initialize(grunt) {

    grunt.loadNpmTasks('grunt-contrib-watch');

    var watch = grunt.config.getRaw('watch') || {};

    watch.pack = {
        tasks: ['pack']
    };

    watch.generate = {
        tasks: ['generate']
    };

    if (!watch.options) {
        watch.options = {
            spawn: false
        };
    } else {
        watch.options.spawn = false;
    }

    grunt.config('watch', watch);

}

function task(grunt) {

    initialize(grunt);

    var path = require('path'),
        store = require('./../api/store'),
        namespace = require('./../api/namespace');

    grunt.event.on('watch', function(action, filepath) {

        if (path.basename(filepath).indexOf('.roc.json') > 0) {
            grunt.option('cache', false);
            store.reset();
        } else {
            grunt.option('cache', true);
            namespace.reset();
        }

    });

}

module.exports = task;
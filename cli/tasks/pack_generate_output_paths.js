function task(grunt) {

    grunt.registerTask('pack_generate_output_paths', 'Generate output paths before pack.', function() {

        var done = this.async(),
            roc = grunt.config.getRaw('roc'),
            package_api = require('../api/package'),
            packages = package_api.store,
            path = require('path');

        roc.files = {
            resources: false,
            htmls: false
        };

        roc.files.javascripts = [
            roc.current.name + '@' + roc.current.version + '.js'
        ];

        roc.files.stylesheets = [
            roc.current.name + '@' + roc.current.version + '.css'
        ];

        grunt.config('roc', roc);

        process.nextTick(done);

    });

}

module.exports = task;
function task(grunt) {

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('minify', 'Minify js/css files.', function() {

        var roc = grunt.config.getRaw('roc'),
            package_api = require('./../api/package'),
            packages = package_api.store,
            minify = roc.current.minify || grunt.option('minify');

        if (minify !== true) {
            return;
        }

        var filetypes = [
            'javascripts',
            'stylesheets'
        ],
            minifier = {
            javascripts: 'uglify',
            stylesheets: 'cssmin'
        };

        var tpl = grunt.template.process(
            '<%= roc.paths.output %>roc_dist/' +
            '<%= roc.current.name + ' +
            '"@" + roc.current.version %>', {
            roc: roc
        });

        filetypes.forEach(function(ft) {

            if (packages.assemble[ft]) {

                var engine = grunt.config.getRaw(minifier[ft]) || {},
                    file = packages.assemble[ft];

                engine[ft] = {
                    files: {}
                };
                engine[ft].files[file] = file;

                grunt.config(minifier[ft], engine);
                grunt.task.run(minifier[ft] + ':' + ft);

            }

        });

    });

}

module.exports = task;
'use strict';

var store = {
    /**
     * Stores all included package informations by package name.
     *
     * Example:
        {
            core: { name: 'core', paths: {...}, config: {...}, id: 'core@0.1.0'},
            ...
        }
     */
    includes: {},
    /**
     * Stores all included package names in the RIGHT order.
     *
     * Example:
        [ 'core', 'page', 'app' ]
     */
    orders: [],
    /**
     * Stores all resolved files path for each package names.
     * The files are stored in the RIGHT order.
     *
     * Example:
        {
            core: { javascripts: [...]},
            page: { templates: [...], javascripts: [...], stylesheets: [...] },
            app: { javascripts: [...] }
        }
     */
    files: {}
};

module.exports = store;
'use strict';

var glob = require('glob'),
    async = require('async'),
    packages = [],
    paths_cache = {};

/**
 * Format package object from given configuration file.
 *
 * @private
 * @method format_from_configuration_file
 * @param {String} cwd Current working directory for the listing.
 * @param {String} file Configuration file from cwd.
 * @return {Object} pkgs Package listing informations.
 */
function format_from_configuration_file(cwd, file) {

    // var name,
    //     namelen,
    var path = require('path'),
        pkgs = {
            length: 1,
            root: path.dirname(cwd)
        };

    // name = file.replace('.roc.json', '').split('/');
    // namelen = name.length;
    // name = [name[namelen - 1], name[namelen - 2]].join('.');
    var c = require(file);
    var name = c.roc_type + '.' + c.name;
    pkgs[name] = path.basename(file);

    return pkgs;

}

/**
 * Format packages object from given configuration files list.
 *
 * @private
 * @method format_from_configuration_files
 * @param {String} cwd Current working directory for the listing.
 * @param {Array} files List of configuration files from cwd.
 * @return {Object} pkgs Package listing informations.
 */
function format_from_configuration_files(cwd, files) {

    var name,
        namelen,
        pkgs = {
            length: files.length,
            root: cwd
        };

    files.forEach(function(file) {
        name = file.replace('.roc.json', '').split('/');
        namelen = name.length;
        name = [name[namelen - 1], name[namelen - 2]].join('.');
        pkgs[name] = file;
    });

    return pkgs;

}

/**
 * Add given path to cache.
 *
 * @private
 * @method add_path_to_cache
 * @param {String} path Path.
 */
function add_path_to_cache(path) {
    paths_cache[path] = true;
}

/**
 * Check if given path has already been walked.
 *
 * @private
 * @method is_path_cache
 * @param {String} path Path.
 * @return {Boolean} True if path is already here, else false.
 */
function is_path_cache(path) {
    return (paths_cache[path] === true);
}

/**
 * List all available packages from given paths.
 *
 * @async If cache is empty
 * @sync If cache if not empty
 *
 * @static
 * @public
 * @method from_paths
 * @alias list
 * @param {Array} paths List of absolute paths.
 * @param {Function} callback Callback to execute when list is finish.
 * @param {Object} opts (optional) Options dictionnary.
 * Available options:
 *  - recursive_paths {Boolean} (optional) Defaults to false.
 */
function list(paths, callback, opts) {

    var pkgs = [],
        pattern = '*/*\.roc\.json';

    if (typeof opts === 'object') {
        if (opts.recursive_paths === true) {
            pattern = '*' + pattern;
        }
    }

    async.eachLimit(paths, 1, function(p, next) {

        // path has already been done
        if (is_path_cache(p)) return next();

        glob(pattern, {
            cwd: p,
            nosort: true
        }, function(err, files) {

            if (err) throw err;

            // we add this path to the cache to avoid doing it again
            add_path_to_cache(p);

            pkgs.push(format_from_configuration_files(p, files));
            next();

        });

    }, function(err) {

        if (err) throw err;

        if (pkgs.length) {
            packages = packages.concat(pkgs);
        }

        callback(true, pkgs);
    });

}

/**
 * Add package to the list from given configuration path.
 *
 * @public
 * @method add_from_config_file
 * @alias from_config_file
 * @param {String} config_path Configuration path
 * @param {Function} callback Callback function(success) { ... }
 */
function add_from_config_file(config_path, callback) {

    var path = require('path'),
        fs = require('fs'),
        config_path = path.resolve(config_path);

    if (is_path_cache(config_path)) return (callback ? callback(true) : true);

    fs.exists(config_path, function(exists) {

        if (exists) {

            // we add this path to the cache to avoid doing it again
            add_path_to_cache(config_path);

            packages.push(
                format_from_configuration_file(config_path, config_path)
            );
            return (callback ? callback(true) : true);
        }

        return (callback ? callback(false) : false);

    });

}

module.exports = {
    from_config_file: add_from_config_file,
    from_paths: list,

    /**
     * Return all available packages.
     *
     * @method availables
     * @return {Object} packages.
     */
    availables: function() {
        return packages;
    },

    /**
     * Flush the cache.
     *
     * @static
     * @method flush
     */
    flush: function() {
        paths_cache = {};
        packages = [];
    }
};
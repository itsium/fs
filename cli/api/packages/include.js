'use strict';

var glob = require('glob'),
    async = require('async'),
    log = require('./../log'),
    store = require('./store'),
    list = require('./list'),
    path = require('path'),
    semver = require('semver');

function err_package_not_found(pkg_name, pkg_version) {
    log.pkg({
        pkg: {
            name: pkg_name,
            version: pkg_version
        },
        foo: log.logger.writeln,
        level: 'ERR',
        msg: ('Package "' + pkg_name + '" not found.')
    });
    return false;
}

function err_need_list_packages_first(pkg_name, pkg_version) {
    log.pkg({
        pkg: {
            name: pkg_name,
            version: pkg_version
        },
        foo: log.logger.writeln,
        level: 'ERR',
        msg: ('Please run package.list before package.include.')
    });
    return false;
}

function skip_package(pkg_name, pkg_version) {
    log.pkg({
        pkg: {
            name: pkg_name,
            version: pkg_version
        },
        foo: log.logger.writeln,
        level: 'WARN',
        msg: ('Skip "' + pkg_name + '".')
    });
    return false;
}

function success_add_package(pkg_name, pkg_version) {
    log.pkg({
        pkg: {
            name: pkg_name,
            version: pkg_version
        },
        level: 'DONE',
        msg: ('Add ROC package "' + pkg_name + '".')
    });
    return true;
}

function cannot_find_package(pkg_name, pkg_version) {
    log.pkg({
        pkg: {
            name: pkg_name,
            version: pkg_version
        },
        exit: true,
        foo: log.logger.writeln,
        level: 'ERR',
        msg: ('Cannot find package "' +
            pkg_name + '@' + pkg_version + '".')
    });
    return false;
}

function invalid_version(name, version) {
    log.pkg({
        pkg: {
            name: name,
            version: version
        },
        exit: true,
        foo: log.logger.writeln,
        level: 'ERR',
        msg: ('Invalid version for package "' +
            name + ' ' + version + '".')
    });
    return false;
}

function satisfied_failed(name, version, asked_version) {
    log.pkg({
        pkg: {
            name: name,
            version: version
        },
        exit: true,
        foo: log.logger.writeln,
        level: 'ERR',
        msg: ('"' + version + '" does not matched "' +
            asked_version + '".')
    });
    return false;
}

function check_version(pkgs, name, version) {

    var config = require(path.resolve(pkgs.root, pkgs[name]));

    if (semver.valid(config.version) === null) {
        return invalid_version(name, config.version);
    }

    if (semver.satisfies(config.version, version) === false) {
        return satisfied_failed(name, config.version, version);
    }

    return true;

}

function index(pkgs, name, version) {

    var len = pkgs.length,
        i = -1;

    while (++i < len) {
        if (typeof pkgs[i][name] !== 'undefined' &&
            check_version(pkgs[i], name, version) === true) {
            return i;
        }
    }

    return -1;

}

function get(pkg_name, pkg_version) {

    var pkgs = list.availables();

    if (!pkgs.length) return err_need_list_packages_first(pkg_name, pkg_version);

    var i = index(pkgs, pkg_name, pkg_version);

    if (i === -1) {
        return cannot_find_package(pkg_name, pkg_version);
    }

    var infos,
        pkg = pkgs[i];

    infos = {
        name: pkg_name,
        paths: {
            root: pkg.root,
            cwd: path.dirname(path.resolve(pkg.root, pkg[pkg_name])) + path.sep,
            config: pkg[pkg_name]
        },
        config: require(path.resolve(pkg.root, pkg[pkg_name]))
    };

    infos.id = infos.config.roc_type + '.' +
        infos.config.name +
        '@' + infos.config.version;

    return infos;

}

/**
 * Include all dependencies to store from given package name and version.
 *
 * @sync
 *
 * @method from_name
 * @param pkg_name {String} Package name.
 * @param pkg_version {String} Package version.
 */
function include(pkg_name, pkg_version) {

    if (pkg_version === "false") return skip_package(pkg_name, pkg_version);

    var pkg = get(pkg_name, pkg_version);

    if (!pkg) return err_package_not_found(pkg_name, pkg_version);

    store.includes[pkg_name] = pkg;

    if (typeof pkg.config.roc_dependencies === 'object') {

        var excludes = ['options'],
            includes = Object.keys(store.includes),
            deps = pkg.config.roc_dependencies,
            dep_names = Object.keys(pkg.config.roc_dependencies);

        dep_names.forEach(function(dep_name){
            if (includes.indexOf(dep_name) === -1 &&
                excludes.indexOf(dep_name) === -1) {
                include(dep_name, deps[dep_name]);
            }
        });

    }

    // we check if package is already included inside orders
    // @TODO it is strange I had bug with handlebars.partials.items
    // package included twice here.
    if (store.orders.indexOf(pkg_name) === -1) {
        store.orders.push(pkg_name);
    }

    return success_add_package(pkg_name, pkg_version);

}

module.exports = {
    from_name: include
};
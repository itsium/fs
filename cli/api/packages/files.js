'use strict';

var glob = require('glob'),
    async = require('async'),
    list = require('./list'),
    log = require('./../log'),
    store = require('./store'),
    precompiler = require('./../precompiler'),
    path = require('path');

function warn_no_match_for(pkg, pattern, extension) {
    log.pkg({
        pkg: {
            name: pkg.name,
            version: pkg.version
        },
        level: 'WARN',
        msg: ('No match for pattern "' + pattern + '" (' + extension + ').')
    });
    return false;
}

/**
 * Find package file from given extension and call callback when finish.
 *
 * @param {Object} pkg Package.
 * @param {String} extension Extension
 * @param {Function} callback
 */
function __find_files(pkg, extension, callback) {

    var patterns = store.includes[pkg.name].config[extension];

    if (!patterns) {
        return callback(false);
    }

    var files = [];

    async.eachLimit(patterns, 1, function(pattern, next) {

        glob(pattern, {
            cwd: pkg.paths.cwd
        }, function(err, results) {

            if (!results.length) {
                warn_no_match_for(pkg, pattern, extension);
            } else {
                files = files.concat(results);
            }

            next();
        });

    }, function(err) {

        callback(true, files);

    });

}

/**
 * Resolve filesystem pattern from given package and store results in
 * package store.
 *
 * @static
 * @async
 *
 * @method resolve_from_package
 * @param {Object} pkg Package.
 * @param {Function} callback Callback.
 */
function resolve_from_package(pkg, callback) {

    var pkg_name = pkg.name;

    if (store.files[pkg_name]) {
        return callback(true, store.files[pkg_name]);
    } else {
        store.files[pkg_name] = {};
    }

    var extensions = precompiler.available_filetypes();

    async.eachLimit(extensions, 2, function(ext, next) {

        __find_files(pkg, ext, function(success, files) {

            if (files && files.length) {
                store.files[pkg_name][ext] = files;
            }

            next();
        });

    }, function(err) {

        callback(true, store.files[pkg_name]);

    });

}


module.exports = {
    resolve_from_package: resolve_from_package
};
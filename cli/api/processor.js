'use strict';

var path = require('path'),
    async = require('async'),
    glob = require('glob'),
    extensions = false,
    publicApi = {
        require: function(p) {
            return require(path.resolve(__dirname, p));
        },
        processor: function(options, proc) {
            if (proc) options.run = proc;
            return register_processor(options);
        }
    };

function err_no_processor_name(p) {
    console.warn('No processor name', p);
    return false;
}

function err_no_processor_extensions(p) {
    console.warn('No processor extensions', p);
    return false;
}

function err_no_processor_filetypes(p) {
    console.warn('No processor filetypes', p);
    return false;
}

/**
 * Validates given processor.
 *
 * @method is_valid_processor
 * @param {Object} p Processor object.
 * @return {Boolean} True if processor valid, else false.
 */
function is_valid_processor(p) {

    if (!p.name) {
        return err_no_processor_name(p);
    } else if (!p.extensions) {
        return err_no_processor_extensions(p);
    } else if (!p.filetypes) {
        return err_no_processor_filetypes(p);
    }

    return true;

}

/**
 * Register given processor object.
 *
 * @method register
 * @param {Object} p Processor object. Generaly it is a previously required
 * javascript file.
 * @return {Boolean} If anything wrong returns false, else true.
 */
function register_processor(p) {

    var success = false;

    if (is_valid_processor(p)) {

        success = true;

        p.filetypes.forEach(function(ft) {

            p.extensions.forEach(function(ext) {

                if (!extensions[ft]) {
                    extensions[ft] = {};
                }

                if (!extensions[ft][ext]) {
                    extensions[ft][ext] = p;
                } else {
                    success = false;
                    console.warn('Processor conflict for filetype "' +
                        ft + '" and extension "' + ext + '"');
                    console.log('Skip processor "' + p.name +
                        '" for "' + ft + ':' + ext + '"');
                }

            });

        });

    }

    return success;

}

/**
 * Will get all available processors and register them.
 * For now only processors from the processors directory are parsed.
 * In the future, package would be able to add their own processor.
 *
 * @method reload
 * @param {Function} callback Callback when load finish.
 */
function reload(callback) {

    var base = __dirname;

    extensions = {};

    async.each(api.paths, function(pattern, next) {

        if (pattern.substr(0, 1) !== '/') {
            pattern = path.resolve(__dirname, pattern);
        }

        glob(pattern, {
            nosort: true
        }, function(error, files) {

            async.each(files, function(file, nextFile) {

                try {
                    require(file)(publicApi);
                } catch (err) {
                    console.error('Processor "' + file +
                        '" is not valid javascript file.');
                    console.error(err);
                }

                nextFile();

            }, function(err) {
                next();
            });

        });

    }, function(err) {
        if (callback) callback();
    });

}

/**
 * Returns correct processor for given filetype and extension.
 * If no processor found, return false.
 *
 * @method get
 * @param {String} filetype File type. (javascripts, htmls, stylesheets, ...)
 * @param {String} extension File extension (handlebars, less, coffee)
 * @return {Mixed} Return processor if found match else return false.
 */
function get(filetype, extension) {

    var type = extensions[filetype];

    if (type && type[extension]) {
        return type[extension];
    }

    return false;

}

/**
 * Run processor for given package file.
 *
 * @method run_on_file
 * @param {Object} grunt Grunt instance.
 * @param {Object} pkg Package.
 * @param {String} file Relative path to file.
 * @param {String} filetype Filetype.
 * @param {Function} callback Callback.
 */
function run_on_file(grunt, pkg, file, filetype, callback) {

    var file_exts = path.basename(file).split('.').slice(1),
        file_fullpath = path.resolve(pkg.paths.cache, file);

    // <@TODO>
    // We do that to avoid the dummy
    // .handlebars.hbs bug I created by avoiding consider last extension as
    // a processor.
    // we need to rename every template which is
    // template_name.handlebars.hbs by just
    // template_name.handlebars
    if (file_exts.length < 2) {
        return callback();
    }

    file_exts.pop();
    // </@TODO>

    async.eachLimit(file_exts, 1, function(ext, next) {

        var p = get(filetype, ext);

        if (p && p.run) {
            p.run(grunt, pkg, file_fullpath, next);
        } else {
            next();
        }

    }, function(err) {
        callback();
    });

}

var api = module.exports = {
    paths: [],
    reload: reload,
    register: register_processor,
    run_on_file: run_on_file,
    get: get
};

if (!extensions) reload();
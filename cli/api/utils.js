(function() {

    function prepend(str, values) {

        var res = values.slice(0),
            i = res.length;

        while (i--) {
            res[i] = str + res[i];
        }

        return res;

    }

    function extend(obj1, obj2) {

        if (typeof obj2 !== 'object') {
            return obj1;
        }

        var k,
            keys = Object.keys(obj2),
            len = keys.length;

        while (len--) {
            k = keys[len];
            obj1[k] = obj2[k];
        }

        return obj1;

    }

    module.exports = {
        prepend: prepend,
        extend: extend
    };

}());

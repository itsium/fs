(function() {

    /**
     * {{#comment}}
     *   My comment !
     * {{/comment}}
     * {{comment "My comment !"}}
     */
    var api = module.exports = {

        handlebars: undefined,
        grunt: undefined,

        helper: function(context, options) {

            if (!options) {
                options = context;
            }

            if (api.grunt.option('minify') === true ||
                api.grunt.option('comments') === false) {
                return options.inverse(this);
            }
            return options.fn(this);

        }

    };

}());

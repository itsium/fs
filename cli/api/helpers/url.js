(function() {

    var store = require('./../store');

    var api = module.exports = {

        handlebars: undefined,

        helper: function(context, options) {

            var url = store.get('urls.' + context, context),
                keys = Object.keys(options.hash);

            keys.forEach(function(key) {
                url = url.replace(':' + key, "'+" + options.hash[key] + "+'");
            });

            return new api.handlebars.SafeString("'" + url + "'");

        }

    };

}());
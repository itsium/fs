(function() {

    var ns = require('./../namespace');

    /**
     * {{classname}}
     * {{classname type="views"}}
     * {{classname type="views" base="App"}}
     * {{classname "App.views.MyView"}}
     */
    module.exports = function(context) {

        ns.init(this);

        if (typeof context === 'string') {
            return (ns.use(context));
        }

        var base = ns.ucfirst(this.framework.barename),
            type = this.me.roc_type,
            must_initialize = true,
            name = ns.ucfirst(ns.slug(this.me.name));

        if (context && context.hash) {
            // type
            if (context.hash.type === false) {
                type = false;
            } else if (typeof context.hash.type === 'string') {
                type = context.hash.type;
            }
            // base
            if (typeof context.hash.base === 'string') {
                base = ns.ucfirst(context.hash.base);
            }
            // initialize
            if (typeof context.hash.initialize === 'boolean') {
                must_initialize = !!(context.hash.initialize);
            }
        }

        if (must_initialize === true) {
            return (ns.use(base +
                (type !== false ? ('.' + type) : '') + '.' + name));
        }

        return (base + (type !== false ? ('.' + type) : '') + '.' + name);

    };

}());

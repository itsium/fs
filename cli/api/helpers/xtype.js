(function() {

    function slug(str) {
        return (str.replace(/-+/g, '')
            .replace(/\s+/g, '-')
            .replace(/[^a-z0-9-\.]/g, ''));
    }

    module.exports = function(context) {

        var type = this.me.roc_type,
            classname = this.me.name.toLowerCase();

        if (context && context.hash) {
            if (context.hash.type === false) {
                type = false;
            } else if (typeof context.hash.type === 'string') {
                type = context.hash.type;
            }
        } else if (typeof context === 'string') {
            type = false;
            classname = context;
        }

        return ((type !== false ? (type + '.') : '') + slug(classname));
    };

}());

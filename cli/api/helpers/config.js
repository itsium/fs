(function() {

    var store = require('./../store');

    var api = module.exports = {

        handlebars: undefined,
        grunt: undefined,

        helper: function(context, options) {

            var url = context,
                as = (options.hash ? options.hash.as : false);

            if (typeof as === 'string') {
                store.set('urls.' + as, url);
            } else {

                url = store.get('urls.' + url);

                if (!url) {
                    api.grunt.fail.fatal('[helpers] route: Route "' +
                        context + '" is unknown.');
                }
            }

            return new api.handlebars.SafeString("'" + url + "'");

        }

    };

}());
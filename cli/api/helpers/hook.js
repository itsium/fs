(function() {

    var storage = require('./storage');

    module.exports = function(context, options) {

        if (options.name) {
            return storage.get(options.name);
        } else if (options.file) {
            storage.set(options.file);
            // include file and compile it
        }

    };

}());

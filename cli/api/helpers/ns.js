(function() {

    module.exports = function(context, options) {

    	var ns = this.framework.barename;

    	if (context && context.hash) {
    		if (context.hash.lowercase === true) {
    			ns = ns.toLowerCase();
    		} else if (context.hash.uppercase === true) {
    			ns = ns.toUpperCase();
    		}
    	}

        return ns;
    };

}());
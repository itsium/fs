(function() {

    var store = {};

    module.exports = {
        add: function(name, value) {
            store[name] = value;
        },
        get: function(name) {
            return store[name];
        }
    };

}());

(function() {

    var cache,
        fs = require('fs'),
        async = require('async'),
        crypto = require('crypto'),
        glob = require('glob'),
        path = require('path'),
        mkdirp = require('mkdirp'),
        store = require('./store'),
        cache_default_name = 'roc_cache',
        cache_filename = 'roc_cache.json',
        cache_dir = path.resolve(__dirname, '../../' + cache_default_name),
        log = require('./log'),
        store = require('./store'),
        path = require('path');

    function change_directory(dir, opts) {

        cache_dir = dir;

        opts = opts || {};

        if (opts.empty === true) {

            var rmdir = require('rimraf');

            rmdir(cache_dir, function() {
                create_directory(opts.callback);
            });

        } else {
            create_directory(opts.callback);
        }
    }

    function create_directory(callback) {
        store.setFile(path.resolve(cache_dir, 'store.json'));
        mkdirp(cache_dir, function(err) {
            if (err) {
                console.log(err);
            }
            if (callback) callback();
        });
    }

    change_directory(cache_dir);

    function hash(str) {

        var shasum = crypto.createHash('sha1');

        shasum.update(str);
        return shasum.digest('hex');

    }

    function hash_pkg(pkg) {

        var p = pkg.paths,
            pkg_path = path.resolve(p.root, p.config);

        return hash(pkg_path);

    }

    function create_package_dir(pkg, file, done) {

        var pkg_cache = path.resolve(cache_dir, hash_pkg(pkg)),
            pkg_dir = path.resolve(pkg_cache, path.dirname(file));

        pkg.paths.cache = pkg_cache;

        fs.exists(pkg_dir, function(exists) {

            if (exists) {
                done();
            } else {
                mkdirp(pkg_dir, function(err) {
                    if (err) console.error(err)
                    else if (done) done()
                });
            }

        });

    }

    function register_package(pkg) {

        create_package_dir(pkg, function() {
            register_package_files(pkg);
        });

    }

    function get_cache_config(pkg, done) {

        var file = path.resolve(pkg.paths.cache, cache_filename);

        fs.exists(file, function(exists) {
            if (exists) done(require(file))
            else done({})
        });

    }

    function write_package_cache(pkg, file) {

    }

    function register_package_files(pkg) {

        var pkg_cache = pkg.paths.cache;

        glob('**', {
            cwd: pkg.paths.cwd
        }, function(err, files) {

            var config = {
                files: {}
            };

            async.each(files, function(file, done) {
                add_package_file_to_config(pkg, file, config, done);
            }, function(err) {
                write_package_cache(pkg, config);
            });

        });

    }

    function add_package_file_to_config(pkg, file, config, done) {

        var fullpath = path.resolve(pkg.paths.cwd, file);

        fs.stat(fullpath, function(err, stats) {
            if (err) console.error(err)
            else if (stats.isFile()) {
                config.files[file] = {
                    size: stats.size,
                    updated_at: stats.mtime.getTime()
                };
            }
            done();
        });

    }

    function copy_file(src, dest, done) {

        fs.readFile(src, function(err, data) {

            if (err) console.error(err);

            fs.writeFile(dest, data, function(err) {
                if (err) console.error(err);
                done();
            });

        });

    }

    function read_config(pkg, done) {

        var config_path = path.resolve(pkg.paths.cache, cache_filename);

        fs.exists(config_path, function(exists) {

            if (!exists) return done({ files: {}});

            fs.readFile(config_path, function(err, data) {
                done(JSON.parse(data));
            });

        });

    }

    function write_config(pkg, config, done) {

        var config_path = path.resolve(pkg.paths.cache, cache_filename);

        fs.writeFile(config_path, JSON.stringify(config, null, 4), function() {
            process.nextTick(done);
        });

    }

    function update_config_for_file(pkg, file, done) {

        read_config(pkg, function(config) {

            config.files[file] = {};

            fs.stat(path.resolve(pkg.paths.cwd, file), function(err, stats) {

                config.files[file] = {
                    size: stats.size,
                    updated_at: stats.mtime.getTime()
                };

                write_config(pkg, config, done);

            });

        });

    }

    /**
     * Add given package file to cache.
     * It will store stats informations about original filepath
     * and store the processed_file content to avoid recompilation
     * if original file was not modified.
     *
     * @method add_package_file
     * @param {Object} pkg Package.
     * @param {String} filepath The filepath to the original non-precompile
     * file
     * @param {String} processed_file The filepath to the precompiled file.
     * @param {Function} done Finish callback.
     */
    function add_package_file(pkg, filepath, processed_file, done) {

        var update_config = function() {
            update_config_for_file(pkg, filepath, done);
        };

        create_package_dir(pkg, filepath, function() {

            var file_fullpath = path.resolve(pkg.paths.cache, filepath);

            if (file_fullpath !== processed_file) {
                copy_file(processed_file, file_fullpath, update_config);
            } else {
                update_config();
            }

        });

    }

    /**
     * Check if given file needs to be update or not.
     *
     * @method is_package_file_outdated
     * @param {Object} pkg Package.
     * @param {String} file File path. Needs to be relative to package directory.
     * No absolute path allowed.
     * @param {Function} done Finish callback. function(success) { ... }
     */
    function is_package_file_outdated(pkg, file, done) {

        var pkg_cache = path.resolve(cache_dir, hash_pkg(pkg));

        pkg.paths.cache = pkg_cache;

        var cache_config_path = path.resolve(pkg.paths.cache, cache_filename);

        fs.stat(path.resolve(pkg.paths.cwd, file), function(err, stats) {

            if (err) {
                console.error(err);
                return done(false);
            } else if (!stats.isFile()) {
                // @TODO glob also returns folders entry (without any file)
                // so we need to check if given filepath is a file or not
                return done(false);
            }

            fs.exists(cache_config_path, function(exists) {

                // if cache_config does not exists
                // consider file as outdated
                if (!exists) return done(true);

                var config = require(cache_config_path),
                    cache_stats = config && config.files && config.files[file];

                if (!cache_stats) return done(true);

                if (cache_stats.size !== stats.size ||
                    cache_stats.updated_at !== stats.mtime.getTime()) {
                    return done(true);
                }

                return done(false);

            });

        });

    }

    function add_grunt_task(grunt, pkg, file, processed) {

        var roc_cache = grunt.config.getRaw('roc_cache') || {};

        roc_cache[processed] = {
            pkg: pkg,
            file: file,
            processed: processed
        };

        grunt.config('roc_cache', roc_cache);
        grunt.task.run('roc_cache:' + processed);

    }

    /**
     * Copy given package file to cache.
     * It will not update the cache config !
     * To update the cache config please use {this@add_package_file} method.
     *
     * @method copy_package_file
     * @param {Object} pkg Package.
     * @param {String} filepath Relative file path to copy.
     * @param {Function} callback Callback.
     */
    function copy_package_file(pkg, filepath, callback) {

        create_package_dir(pkg, filepath, function() {

            copy_file(
                path.resolve(pkg.paths.cwd, filepath),
                path.resolve(pkg.paths.cache, filepath),
                callback
            );

        });

    }

    var api = module.exports = {
        dirname: cache_default_name,
        copy_package_file: copy_package_file,
        is_package_file_outdated: is_package_file_outdated,
        add_package_file: add_package_file,
        add_grunt_task: add_grunt_task,
        register_package: register_package,
        change_directory: change_directory
    };

}());

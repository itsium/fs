(function() {

    var store = require('./store'),
        namespaces;

    store.on('reset', function() {
        namespaces = undefined;
    });

    function ucfirst(str) {
        return (str.charAt(0).toUpperCase() + str.slice(1));
    }

    function slug(str) {
        return (str.replace(/-+/g, '')
            .replace(/\s+/g, '_')
            .replace(/[^a-z0-9_]/g, ''));
    }

    function initialize_namespaces(self) {

        if (typeof namespaces !== 'undefined') {
            return false;
        }

        var n = store.get('ns', false);

        if (n) {
            namespaces = n;
        } else {
            namespaces = {};
            namespaces[ucfirst(self.framework.barename)] = {
                views: {},
                events: {},
                data: {},
                engines: {},
                helpers: {}
            };
            store.set('ns', namespaces);
        }

        return true;
    }

    function use(ns) {

        var declaration = '',
            names = ns.split('.'),
            i = 0,
            length = names.length - 1,
            base = namespaces;

        while (i < length) {

            var key = names[i];

            if (typeof base[key] !== 'object') {
                base[key] = {};
                var path = names.slice(0, i + 1).join('.');
                if (!i) {
                    declaration += ('if (!this.' + path +
                        ') ' + path + ' = {};\n');
                } else {
                    declaration += ('if (!' + path +
                        ') ' + path + ' = {};\n');
                }
            }
            base = base[key];
            ++i;
        }

        store.set('ns', namespaces);

        return (declaration + ns);

    }

    var api = module.exports = {
        init: initialize_namespaces,
        slug: slug,
        ucfirst: ucfirst,
        use: use,
        reset: function() {
            store.set('ns', false);
            namespaces = undefined;
        }
    };

}());
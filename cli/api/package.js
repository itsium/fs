'use strict';

var path = require('path'),
    log = require('./log'),
    list = require('./packages/list'),
    include = require('./packages/include'),
    files = require('./packages/files'),
    store = require('./packages/store');

function logger(obj) {
    log.logger = obj;
}

function get_name_from_path(config_path) {

    // var dir = path.dirname(config_path).split(path.sep).pop(),
    //     file = path.basename(config_path),
    //     name = file.replace('.roc.json', '') + '.' + dir;
    //
    // return name;

    var c = require(path.resolve(config_path))

    return c.roc_type + '.' + c.name;

}

function each(callback) {

    store.orders.forEach(function(pkgname) {
        callback(store.includes[pkgname]);
    });

}

/**
 * @TODO faire une methode .each qui fait comme async.each(packages.orders),
 * mais qui retourne directe a chaque iteration packages.includes[pkgname] et le
 * next aussi.
 */

module.exports = {
    clear_cache: function() {
        list.flush();
    },
    resolve_files: files.resolve_from_package,
    store: store,
    include: include.from_name,
    list: list.from_paths,
    add_from_config: list.from_config_file,
    availables: list.availables,
    get_name_from_path: get_name_from_path,
    logger: logger,
    each: each
};
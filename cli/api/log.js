(function() {

    var chalk = require('chalk');

    function col(str, size, sep, style) {

        str = str || '';
        sep = sep || ' ';

        var len = str.length;

        if (len < size) {
            while (len < size) {
                str += sep;
                ++len;
            }
        } else if (len > size) {
            str = str.substr(0, size - 1) + '…';
        }

        if (style) {
            str = str[style];
        }

        return str;

    }

    function rcol(str, size, sep, style) {

        str = str || '';
        sep = sep || ' ';

        var len = str.length;

        if (len < size) {
            while (len < size) {
                str = sep + str;
                ++len;
            }
        } else if (len > size) {
            str = str.substr(0, size - 1) + '…';
        }

        if (style) {
            str = str[style];
        }

        return str;

    }

    function pkg(opts) {

        var level = (opts.level ? opts.level : 'INFO'),
            foo = (opts.foo ? opts.foo : api.logger.verbose.writeln),
            pkg = (opts.pkg.config ? opts.pkg.config : opts.pkg),
            msg = opts.msg,
            type = pkg.roc_type,
            colors = {
                WARN: chalk.yellow,
                ERR: chalk.red,
                INFO: chalk.cyan,
                DONE: chalk.green
            },
            header = [
                rcol(pkg.name, 24), '@',
                col(pkg.version, 7), ' ',
                rcol(type, 6).bold
            ].join('');

        if (level) {
            header = [
                'roc',
                colors[level](rcol(level, 4)),
                header
            ].join(' ');
        } else {
            header = [
                'roc',
                rcol(' ', 4),
                header
            ].join(' ');
        }

        foo(header + ' ' + msg);

        if (opts.exit === true) process.exit(1);

    }

    var api = module.exports = {
        logger: console,
        col: col,
        rcol: rcol,
        pkg: pkg
    };

}());

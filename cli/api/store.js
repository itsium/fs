(function() {

    var store,
        utils = require('./utils'),
        util = require('util'),
        EventEmitter = require('events').EventEmitter,
        fs = require('fs'),
        path = require('path'),
        store_file = path.resolve(process.cwd(), 'store.json'),
        updated = false,
        updating = false;

    var interval = setInterval(function() {
        if (updated && !updating) {
            updated = false;
            save_store();
        }
    }, 1000);

    function save_store(callback) {

        updating = true;

        fs.writeFile(store_file, JSON.stringify(store, null, 4), function(err) {
            updating = false;
            if (err && callback) callback(false);
            else if (callback) callback(true);
        });
    }

    function load_store() {
        if (fs.existsSync(store_file)) {
            store = require(store_file);
        } else {
            store = {};
        }
    }

    var Api = function() {};

    util.inherits(Api, EventEmitter);

    utils.extend(Api.prototype, {

        setFile: function(file) {
            store_file = file;
        },

        set: function(name, value) {
            if (!store) load_store();
            store[name] = value;
            updated = true;
        },

        get: function(name, defaultValue) {
            if (!store) load_store();
            if (typeof store[name] === 'undefined') {
                return defaultValue;
            }
            return store[name];
        },

        reset: function() {
            store = {};
            updated = false;
            if (!updating) fs.writeFileSync(store_file, '{}');
            api.emit('reset');
        }

    });

    var api = module.exports = new Api();

}());
(function() {

    var previous = {},
        store = {},
        slow = 20;

    var api = module.exports = {

        tick: function(name, id) {

            var time = 0,
                now = (new Date()).getTime(),
                prev = previous[name];

            if (prev) {
                time = (now - prev.time);
                if (time > slow) {
                    console.log(name.bold.red + ': ', time, 'ms. (' + prev.id + ')');
                } else {
                    console.log(name + ': ', time, 'ms. (' + prev.id + ')');
                }
            }
            previous[name] = {
                id: id,
                time: now
            };

            return time;

        },

        on: function(name, id) {

            store[name] = {
                id: id,
                time: (new Date()).getTime()
            };

        },

        off: function(name, id) {

            var start = store[name],
                time = (new Date()).getTime() - start.time;

            if (time > slow) {
                console.log(name.bold.red + ': ', time, 'ms. (' + start.id + ')');
            } else {
                console.log(name + ': ', time, 'ms. (' + start.id + ')');
            }

        },

        slow: function(ms) {
            slow = ms;
        }

    };

}());
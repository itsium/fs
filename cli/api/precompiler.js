'use strict';

var path = require('path'),
    async = require('async'),
    processors = require('./processor'),
    cache = require('./cache'),

    /**
     * List all available filetypes.
     * Order can matter (take future hooks for example).
     */
    filetypes = [
        'javascripts',
        'handlebars_partials',
        'templates',
        'stylesheets',
        'htmls',
        'resources',
        'images'
    ]
    // extensions = {
    //     javascripts: '.js',
    //     stylesheets: '.css',
    //     templates: '.js',
    //     htmls: '.html',
    //     handlebars_partials: '.js',
    //     resources: false,
    //     images: false
    // };

/**
 * Precompile given file if cache is outdated.
 *
 * @method run_file
 * @param {Object} grunt Grunt instance.
 * @param {Object} pkg Package.
 * @param {String} file Relative file path.
 * @param {String} filetype File type.
 * @param {Function} callback Callback
 */
function run_file(grunt, pkg, file, filetype, callback) {

    cache.is_package_file_outdated(pkg, file, function(outdated) {

        if (outdated) {
            // file has been updated, we need to process it again !
            cache.copy_package_file(pkg, file, function(success) {
                processors.run_on_file(grunt, pkg, file, filetype, function() {

                    var file_fullpath = path.resolve(pkg.paths.cache, file);

                    // we refresh the cache information for this file
                    cache.add_grunt_task(grunt, pkg, file, file_fullpath);
                    callback();
                });
            });

        } else {
            // file already up to date
            callback();
        }

    });

}

/**
 * Precompile all files from given package.
 *
 * @method run
 * @param {Object} grunt Grunt instance
 * @param {Object} pkg Package
 * @param {Function} callback Callback
 */
function run(grunt, pkg, callback) {

    var packages = require('./package'),
        files = packages.store.files[pkg.name];

    async.eachLimit(Object.keys(files), 1, function(ft, nextFiletype) {

        async.eachLimit(files[ft], 1, function(file, nextFile) {

            run_file(grunt, pkg, file, ft, nextFile);

        }, function() {
            nextFiletype();
        });

    }, function() {
        callback();
    });

}

/**
 * This method register all included packages roc utilities such as:
 *
 *  . `roc_helpers`: This is ROC handlebars templates.
 *  . `roc_processors`: This is ROC processors (Ex: filename.processor_name.ext)
 *
 * @method register_roc_utilities
 * @param {Function} callback (optional) Callback.
 */
function register_roc_utilities(callback) {

    var packages = require('./package'),
        utils = require('./utils'),
        roc_helpers = [
            'cli/api/helpers/*.js'
        ], roc_processors = [];

    packages.each(function(pkg) {

        var config = pkg.config;

        if (config.roc_helpers) {
            roc_helpers = roc_helpers.concat(
                utils.prepend(pkg.paths.cwd, config.roc_helpers)
            );
        }

        if (config.roc_processors) {
            roc_processors = roc_processors.concat(
                utils.prepend(pkg.paths.cwd, config.roc_processors)
            );
        }

    });

    api.roc_helpers = roc_helpers;
    processors.paths = roc_processors;

    processors.reload(callback);

}

var api = module.exports = {
    roc_helpers: [],
    /**
     * Return available filetypes.
     *
     * @method available_filetypes
     */
    available_filetypes: function() {
        return filetypes;
    },
    register_roc_utilities: register_roc_utilities,
    run: run
};
function list(argv) {

    var path = require('path'),
        api = require('../api/package'),
        log = require('../api/log'),
        search = (argv._[1] ? argv._[1] : false),
        paths = [
            path.resolve(__dirname, '../../packages'),
            path.resolve(__dirname, '../../demo')
        ];

    var list_finish = function(success, folders) {

        folders.forEach(function(pkgs) {

            Object.keys(pkgs).forEach(function(pkgname) {

                if (['root', 'length'].indexOf(pkgname) !== -1) {
                    return false;
                }

                var pkg = require(path.resolve(pkgs.root, pkgs[pkgname]));

                pkg.id = pkg.roc_type + '.' +
                    pkg.name + '@' + pkg.version;

                if (search) {
                    process.stdout.write(' ' + log.col(pkg.id, 36)
                        .replace(search, search.bold.red));
                } else {
                    process.stdout.write(' ' + log.col(pkg.id, 36));
                }
                process.stdout.write(' ' + log.col(pkg.description, 50) + '\n');

            });

        });

        process.exit(0);

    };

    var pkgs = api.list(paths, list_finish, {
        recursive_paths: true,
        type: (argv._[1] ? argv._[1] : false)
    });

}

module.exports = list;
function cache(argv) {

    var path = require('path'),
        api = require('../api/package'),
        log = require('../api/log'),
        cache = require('../api/cache'),
        search = (argv._[1] ? argv._[1] : false),
        paths = [
            path.resolve(__dirname, '../../packages'),
            path.resolve(__dirname, '../../demo')
        ];


    var list_finish = function(success, folders) {

        folders.forEach(function(pkgs) {

            Object.keys(pkgs).forEach(function(pkgname) {

                if (['root', 'length'].indexOf(pkgname) !== -1) {
                    return false;
                }

                var pkg = api.get(folders, pkgname),
                    config = pkg.config;

                cache.register_package(pkg);

            });

        });

    };

    var pkgs = api.list(paths, list_finish, {
        recursive_paths: true,
        type: (argv._[1] ? argv._[1] : false)
    });

}

module.exports = cache;
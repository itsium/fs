/*
 * grunt-roc-config
 *
 *
 * Copyright (c) 2014 bydooweedoo
 * Licensed under the MIT license.
 */

 (function() {

    'use strict';

    var api = module.exports = {},
        path = require('path'),
        extend = require('./api/utils').extend;

    function get_config(grunt, filepath) {

        var dep_opts,
            json = grunt.file.readJSON(filepath),
            filedir = path.resolve(path.dirname(filepath)) + path.sep,
            output;// = (json.roc_config ? json.roc_config.output : false);

        if (typeof output !== 'string') {
            output = path.resolve(filedir, 'roc_dist') + path.sep;
        } else {
            output = path.resolve(filedir, output, 'roc_dist/') + path.sep;
        }

        if (typeof grunt.option('output') !== 'undefined') {
            output = path.resolve(grunt.option('output')) + path.sep;
        }

        return extend({
            current: json,
            paths: {
                framework: path.resolve('./') + path.sep,
                config: filedir,
                config_file: path.resolve(filepath),
                output: output
            },
            framework: grunt.file.readJSON('package.json')
        }, json.roc_config);

    }

    function template_keys(grunt, config) {

        if (config.current.roc_dependencies) {

            var deps = config.current.roc_dependencies,
                keys = Object.keys(deps),
                len = keys.length,
                opts = {
                    data: {
                        roc: config
                    }
                };

            while (len--) {

                var key = keys[len],
                    tpl_key = grunt.template.process(key, opts);

                if (key !== tpl_key) {
                    deps[tpl_key] = deps[key];
                    delete deps[key];
                }

            }

        }

        return config;

    }

    /**
     * Sample of <% roc %> template vars after calling this task:
     *
     *   {
     *       "paths": {
     *           "framework": "absolute path to framework base directory with ending slash",
     *           "config": "absolute path to user configuration file directory with ending slash",
     *           "output": "absolute path to output directory with ending slash"
     *       },
     *       "framework": {
     *           ... (Dictionnary of framework `package.json`)
     *       },
     *       "current": {
     *           ... (Dictionnary of given configuration json file)
     *       },
     *       ... (Dictionnary of given "roc_config" if specified)
     *   }
     *
     */
    api.parse = function(grunt) {

        var config,
            file = grunt.option('config'),
            path = require('path'),
            options = [
                'minify'
            ];

        if (!file) {
            grunt.log.error('You must use "--config ' +
                'path/to/config.json" for this task.');
            return grunt.fail.warn('Exiting');
        }

        config = get_config(grunt, file);

        options.forEach(function(opt) {

            var value = grunt.option(opt);

            if (typeof value !== 'undefined') {
                config[opt] = value;
            }
        });

        return template_keys(grunt, config);

    };

}());

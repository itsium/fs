# ROC
> Javascript framework

## Getting Started

### Install

```shell
npm install && npm install -g grunt-cli yo
```

### Use

```shell
grunt [command] --config /path/to/config.roc.json [options, ...]
```

#### Commands

 . `generate`: copy given project to destination.
 . `pack`: pack given project by assemble every files into one.

#### Options

 . `--minify`: `true` or `false`. Will minify javascripts and stylesheets files if `true`. Defaults to `false`.
 . `--cache`: `true` or `false`. Will use the cache to avoid precompile previous unmodified files. Defaults to `true`.
 . `--output`: Will write the output files inside the given folder. Note that
 it will *NOT* create `roc_dist` folder inside given path. Defaults to given configuration folder path + `roc_dist/`.
 . `-v`: Verbose mode.
 . `--watch`: `true` or `false`. Will watch every files and relaunch command when modification applied. Defaults to `false`.
 . `--comments`: `true` or `false`. Will keep or remove comments. Defaults to `true`.


 #### TODO

 * Change `--output` option by `--dest`.

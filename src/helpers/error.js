/* error */

{{framework.barename}}.helpers.Error = (function() {

    var _win = window,
        _nav = _win.navigator,
        _ns = {{framework.barename}},
        _storage = _ns.Storage,
        _request = _ns.Request,
        _parent = _ns,

        _infos = {
            browser: _nav.userAgent,
            vendor: _nav.vendor,
            os: _nav.platform,
            cookieEnabled: _nav.cookieEnabled,
            isOnLine: _nav.onLine,
            language: _nav.language
        };

    function onerror(message, url, linenumber) {
        var error = _infos;

        error.message = message;
        error.url = url;
        error.linenumber = linenumber;
        this.fire('error', this, error);

        if (this.path !== null) {
            _request.jsonp(this.path, {

            });
        }
        return true;
    };

    return _parent.subclass({

        infos: _infos,

        constructor: function(opts) {
            this.path = opts.path || null;
            window.onerror = onerror.bind(this);
        }

    });

}());

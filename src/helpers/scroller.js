{{framework.barename}}.helpers.Scroller = (function() {

    var _doc = document;

    _doc.addEventListener('DOMContentLoaded', function() {

        var style = _doc.createElement('style');

        if ('-webkit-overflow-scrolling' in _doc.body.style) {
            style.innerHTML = [
                '.scroller {',
                    'overflow: auto;',
                    '-webkit-overflow-scrolling: touch;',
                    'position: absolute;',
                    //'top: 69px;',
                    //'bottom: 0px;',
                    'left: 0px;',
                    'right: 0px;',
                '}'
            ].join('');
        } else {
            style.innerHTML = [
                '.scroller {overflow: auto;}'
            ].join('');
        }

        style.id = '{{lowerCase framework.barename}}-helpers-scroller';
        _doc.head.appendChild(style);
        _doc.removeEventListener('DOMContentLoaded', arguments.callee, false);

    }, false);

}());

{{framework.barename}}.helpers.Network = (function() {

    'use strict';

    var _ns = {{framework.barename}},
        _parent = _ns,
        _env = _ns.helpers.Env,
        _events = new _ns.Event(),
        _nav = navigator,
        _online = (_nav && typeof _nav.onLine === 'boolean' ? _nav.onLine : true),
        _scope = (_env.phonegap ? document : window);

    function offline() {
        _online = false;
        _events.fire('offline');
        _events.fire('status', false);
    }

    function online() {
        _online = true;
        _events.fire('online');
        _events.fire('status', true);
    }

    function ready() {
        _scope.removeEventListener('deviceready', ready, false);
        _online = (_nav && typeof _nav.onLine === 'boolean' ? _nav.onLine : true);
        _scope.addEventListener('offline', offline, false);
        _scope.addEventListener('online', online, false);
    }

    if (_env.phonegap) {
        _scope.addEventListener('deviceready', ready, false);
    } else {
        _scope.addEventListener('offline', offline, false);
        _scope.addEventListener('online', online, false);
    }

    var Network = _parent.subclass({

        constructor: function() {
        },

        isOnline: function() {
            return _online;
        },

        status: function(callback, scope, priority) {
            return _events.on('status', callback, scope, priority);
        },

        online: function(callback, scope, priority) {
            return _events.on('online', callback, scope, priority);
        },

        offline: function(callback, scope, priority) {
            return _events.on('offline', callback, scope, priority);
        },

        detach: function(uid) {
            return _events.off(uid);
        }

    });

    return (new Network());

}());

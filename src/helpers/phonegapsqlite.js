(function() {

	'use strict';

	var _ns = {{framework.barename}},
		_env = _ns.helpers.Env;

	/**
	 * SQLite proxy for store.
	 * https://github.com/lite4cordova/Cordova-SQLitePlugin
	 *
	 * http://stackoverflow.com/questions/7691238/confused-about-phonegap-and-ios-need-to-get-file-from-www-path-from-plugin
	 *
	 * ## Example
     *
		proxy: {
			type: 'phonegapsqlite',
			database: 'database_name',
			query: 'SELECT id, eng, chn, pingying FROM dicos WHERE LOWER(chn) LIKE LOWER(?) OR LOWER(eng) LIKE LOWER(?) OR LOWER(eng) LIKE LOWER(?)',
			total: 'SELECT COUNT(id) FROM dicos WHERE LOWER(chn) LIKE LOWER(?) OR LOWER(eng) LIKE LOWER(?) OR LOWER(eng) LIKE LOWER(?)'
		}
	 *
	 */
	_ns.Request.phonegapsqlite = function(url, options, store) {

		if (store) {

			var proxy = store.config.proxy;

			options.query = proxy.query;
			options.database = proxy.database;
		}

		var sql = window.sqlitePlugin,
			dbname = options.database,
			db = sql.openDatabase({
				name: (_env.os.family === 'Android' ? dbname.replace('.db', '') : dbname),
				bgType: 1
			}),
			callback = options.callback.bind(options.scope || window);

		db.transaction(function(tx) {

			tx.executeSql(options.query, options.queryValues, function(txq, res) {

				//console.log('results: ', Object.keys(res.rows), res.rows.length, typeof res.rows.item, arguments);

				var data = res;

				if (store) {
					data = {};
					data[store.successProperty] = true;
					data[store.rootProperty] = [];
					data[store.totalProperty] = res.rows.length;

					var i = -1,
						length = res.rows.length;

					while (++i < length) {
						data[store.rootProperty].push(res.rows.item(i));
					}
				}

				if (callback) {
					callback(true, data);
				}

			}, function(txe, error) {
				//console.log('ERROR phonegapsqlite executeSql: ', error.code, error.message);
				if (callback) {
					callback(false, error);
				}
			});

		}, function(error) {
			//console.log('ERROR phonegapsqlite transaction: ', error.code, error.message);
			if (callback) {
				callback(false, error);
			}
		});

	};

}());

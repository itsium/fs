/**
 * @class {{framework.barename}}.helpers.Updater
 * @extends {{framework.barename}}.Event
 *
 * Allows update checking from server update file.
 *
 * ### Example
 *
 *      var up = new {{framework.barename}}.helpers.Updater({
            proxy: 'jsonp',
            currentVersion: '0.1.0',
            url: 'http://app.example.com/updater.json?callback={callback}'
        });
 *
 */
{{framework.barename}}.helpers.Updater = (function() {

    var _alert,
        _ns = {{framework.barename}},
        _net = _ns.helpers.Network,
        _request = _ns.Request,
        _storage = _ns.Storage,
        _parent = _ns.Event,
        _env = _ns.helpers.Env,
        _family = _env.os.family.toLowerCase(),
        _android = (_family.indexOf('android') > -1),
        _ios = (_family.indexOf('ios') > -1);

    function get_alert(self) {

        if (!_alert) {

            var items = [];

            _alert = new _ns.views.Alert({
                config: {
                    header: {
                        title: 'Update',
                        ui: 'f'
                    },
                    style: 'position: fixed; top: 50%; left: 50%; margin-left: -140px; width: 280px; margin-top: -140px;'
                },
                listeners: {
                    scope: self,
                    show: function(self) {
                        self.el.style.marginTop = '-' + (self.el.offsetHeight / 2) + 'px';
                    }
                },
                items: [[
                    'An update is available for this application.'
                ].join(''), {
                    xtype: 'button',
                    config: {
                        ui: 'c',
                        text: 'Later',
                        inline: false
                    },
                    scope: self,
                    handler: function() {
                        _alert.hide();
                    }
                }]
            });
            _alert.compile();
            _alert.render('body', 'afterbegin');
        }

        return _alert;

    }

    function get_update(self) {

        self.request(self.url, {
            callback: function(success, data) {

                if (success) {

                    self.data = data;
                    _checkUpdate(self);

                    if (typeof self.sid === 'undefined') {
                        self.sid = setInterval(function() {
                            _checkUpdate(self);
                        }, self.interval);
                    }

                }

            }
        });

    }

    var _checkUpdate = function(self) {

        var data = self.data;

        self = self || this;

        if (self.currentVersion !== data.version) {
            get_alert(self).show();
        }

    };

    return _parent.subclass({

        defaultInverval: (86400 * 1000), // check every day

        constructor: function(opts) {

            var self = this;

            self.eids = {};

            self.currentVersion = opts.currentVersion;
            self.interval = opts.interval || self.defaultInverval;
            self.url = opts.url;
            self.request = _ns.Request[self.proxy] || _ns.Request.jsonp;

            _parent.prototype.constructor.call(self, opts);

            self.eids.online = _net.online(_checkUpdate, self);

            if (_net.isOnline()) {
                get_update(self);
            }

        }

    });

}());

{{framework.barename}}.layouts.Card = (function() {

    var _ns = {{framework.barename}},
        _parent = _ns.Event;

    function _afterrender(self) {



    }

    function _aftercompile(self, renderer) {
        self.off(self.eid);
        self.eid = renderer.on('afterrender', _afterrender,
            self, self.priority.VIEWS);
    }

    _parent.subclass({

        defaultMaxActive: 1,

        constructor: function(opts) {

            var self = this;

            if (opts.activeCard) {
                self.activeCard = opts.activeCard;
            } else {
                self.activeCard = 0;
            }

            self.items = opts.items || [];

            if (!self.items.indexOf) {
                self.items = [self.items];
            }

            _parent.prototype.constructor.apply(self, arguments);

            self.eid = self.on('aftercompile', _aftercompile,
                self, self.priority.VIEWS);

        },

        active: function(index) {

        },

        compile: function() {

        },

        render: function() {

        }

    });

}());

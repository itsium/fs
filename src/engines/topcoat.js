{{framework.barename}}.engines.Topcoat = (function() {

    'use strict';

    /* jshint -W030 */
    var _ns = {{framework.barename}},
        _views = _ns.views,
        _parent = _ns,
        _selector = _ns.Selector,
        _templates = _ns.Templates,
        _handlebars = Handlebars,
        _tpls = _handlebars.templates,
        _events = {};
    /* jshint +W030 */

    // used in listitemfull.handlebars
    _handlebars.registerHelper('if_prop', function(context, options) {

        var index = options.hash.index + options.hash.indexDiff,
            item = context[index];

        if (item && item[options.hash.prop] === options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    return _parent.subclass({

        xtype: 'topcoat',
        className: '{{framework.barename}}.engines.Topcoat',
        version: '0.8.0',

        constructor: function() {},

        getTpl: function(name) {
            return (_tpls[name] ||
                console.error(this.className + ' engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

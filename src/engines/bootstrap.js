{{framework.barename}}.engines.Bootstrap = (function() {

    'use strict';

    /* jshint -W030 */
    var _ns = {{framework.barename}},
        _views = _ns.views,
        _parent = _ns,
        _selector = _ns.Selector,
        _templates = _ns.Templates,
        _handlebars = Handlebars,
        _tpls = _handlebars.templates,
        _events = {};
    /* jshint +W030 */

    /**
     * Check if current item is in given parent xtype.
     */
    _handlebars.registerHelper('isInParent', function(xtype, options) {

        var self = _handlebars.currentParent[_handlebars.currentParent.length - 1],
            ifNo = (options.hash.ifNo ? options.hash.ifNo.split('|') : []);

        while ((self = self.parent)) {

            var pxtype = self.xtype,
                pxtpl = self.xtpl;

            if (ifNo.indexOf(pxtype) > -1 ||
                ifNo.indexOf(pxtpl) > -1) {
                return options.inverse(this);
            } else if (pxtype === xtype || pxtpl === xtype) {
                return options.fn(this);
            }

        }
        return options.inverse(this);
    });

    _events.menuitem = {
        click: function() {
            console.log('click: ', arguments);
        }
    };

    return _parent.subclass({

        xtype: 'bootstrap',
        className: '{{framework.barename}}.engines.Bootstrap',
        version: '3.0.2',

        constructor: function() {},

        getTpl: function(name) {
            return (_tpls[name] ||
                console.error(this.className + ' engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

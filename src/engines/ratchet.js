{{framework.barename}}.engines.Ratchet = (function() {

    'use strict';

    /* jshint -W030 */
    var _ns = {{framework.barename}},
        _views = _ns.views,
        _parent = _ns,
        _selector = _ns.Selector,
        _templates = _ns.Templates,
        _tpls = Handlebars.templates,
        _events = {};
    /* jshint +W030 */

    function findParent(el, callback) {
        if (callback(el) === false && el.parentNode) {
            return findParent(el.parentNode, callback);
        }
    }

    _events.flipswitch = {
        afterrender: function(self) {

            var el = self.uiEl;

            self.on('check', function() {
                _selector.addClass(el, 'active');
            }, self, self.priority.VIEWS);

            self.on('uncheck', function() {
                _selector.removeClass(el, 'active');
            }, self, self.priority.VIEWS);

        }
    };

    function activateTab(self, event) {

        var activeEl = self.el.querySelector('.active');

        if (activeEl) {
            _selector.removeClass(activeEl, 'active');
        }

        findParent(event.target, function(el) {
            if (el.tagName === 'LI') {
                _selector.addClass(el, 'active');
                return true;
            }
            return false;
        });
    }

    _events.controlgroup = {
        select: activateTab
    };

    _events.tabs = {
        select: activateTab
    };

    return _parent.subclass({

        xtype: 'ratchet',
        className: '{{framework.barename}}.engines.Ratchet',
        version: '1.0.2',

        constructor: function() {},

        getTpl: function(name) {
            return (_tpls[name] ||
                console.error(this.className + ' engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());

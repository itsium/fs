/* homemade */


{{framework.barename}}.engines.Homemade = (function() {

    var _ns = {{framework.barename}},
        _parent = _ns,
        _templates = _ns.Templates,
        _tpls = {};

    _tpls.page = _templates.load([
        '<div class="page',
            '{{#ui}} page-{{.}}{{/ui}}',
            '{{#cssClass}} {{.}}{{/cssClass}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#items}}',
                '{{#safe .}}{{/safe}}',
            '{{/items}}',
        '</div>'
    ].join(''));

    _tpls.header = _templates.load([
        '<div class="header{{#ui}} header-{{.}}{{/ui}}',
            '{{#cssClass}} {{.}}{{/cssClass}}"',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#title}}<span class="header-title',
                '{{#../ui}} header-title-{{.}}{{/../ui}}">{{.}}</span>',
            '{{/title}}',
            '{{#items}}',
                '{{#safe .}}{{/safe}}',
            '{{/items}}',
        '</div>'
    ].join(''));

    _tpls.footer = _templates.load([
        '<div class="footer{{#ui}} footer-{{.}}{{/ui}}',
            '{{#cssClass}} {{.}}{{/cssClass}}',
            '{{#position}} footer-position-{{.}}{{/position}}"',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#items}}',
                '{{#safe .}}{{/safe}}',
            '{{/items}}',
        '</div>'
    ].join(''));

    _tpls.tabs = _templates.load([
        '<ul class="tabs{{#ui}} tabs-{{.}}{{/ui}} ',
            'tabs-{{#length items}}{{/length}}-items"',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#items}}',
                '<li class="tabs-item{{#../ui}} tabs-{{.}}-item{{/../ui}}">',
                    '{{#safe .}}{{/safe}}',
                '</li>',
            '{{/items}}',
        '</ul>'
    ].join(''));

    _tpls.button = _templates.load([
        '<a{{#xroute}} href="#{{.}}"{{/xroute}} ',
            'class="button',
            '{{#ui}} button-{{.}}{{/ui}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            //'{{#css}} style="{{.}}"{{/css}}',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#icon}}<div class="icon">{{#geticon .}}{{/geticon}}</div>{{/icon}}',
            '{{#text}}{{.}}{{/text}}',
        '</a>'
    ].join(''));

    _tpls.list = _templates.load([
        '<ul class="list',
            '{{#ui}} list-{{.}}{{/ui}}',
            '{{#cssClass}} {{.}}{{/cssClass}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#each items}}',
                '<li class="list-item{{#../ui}} list-{{.}}-item{{/../ui}}">',
                    '{{#safe this}}{{/safe}}',
                '</li>',
            '{{/each}}',
        '</ul>',
        '<div class="list-loader',
            '{{#ui}} list-loader-{{.}}{{/ui}}">',
            '<a class="list-loader-text">',
                'Loading...',
            '</a>',
        '</div>'
    ].join(''));

    _tpls.listitem = _templates.load([
        '{{#each items}}',
            '<li class="list-item{{#../ui}} list-{{.}}-item{{/../ui}}">',
                '{{#safe this}}{{/safe}}',
            '</li>',
        '{{/each}}'
    ].join(''));

    _tpls.listbuffered = _templates.load([
        '<ul class="list',
            '{{#ui}} list-{{.}}{{/ui}}',
            '{{#cssClass}} {{.}}{{/cssClass}}"',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#each items}}',
                '<li class="list-item{{#../ui}} list-{{.}}-item{{/../ui}}">',
                    '{{#safe this}}{{/safe}}',
                '</li>',
            '{{/each}}',
        '</ul>',
        '<div class="list-loader',
            '{{#ui}} list-loader-{{.}}{{/ui}}">',
            '<a class="list-loader-text">',
                'Loading...',
            '</a>',
        '</div>'
    ].join(''));

    _tpls.listbuffereditem = _templates.load([
        '{{#each items}}',
            '<li class="list-item{{#../ui}} list-{{.}}-item{{/../ui}}">',
                '{{#safe this}}{{/safe}}',
            '</li>',
        '{{/each}}'
    ].join(''));

    _tpls.container = _templates.load([
        '<div class="container',
            '{{#hidden}} hidden{{/hidden}}',
            '{{#cssClass}} {{.}}{{/cssClass}}',
            '{{#ui}} container-{{.}}{{/ui}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#items}}',
                '{{#safe .}}{{/safe}}',
            '{{/items}}',
        '</div>'
    ].join(''));

    _tpls.floatingpanel = _templates.load([
        '<div class="floatingpanel-overlay',
            '{{#hidden}} hidden{{/hidden}}',
            '{{#if overlay}} floatingpanel-overlay-{{overlay}}',
            '{{else}} floatingpanel-overlay-default{{/if}}',
        '"',
        '{{#id}} id="{{.}}-overlay"{{/id}}>',
        '</div>',
        '<div class="floatingpanel',
            '{{#arrow}} arrow-{{.}}{{/arrow}}',
            '{{#hidden}} hidden{{/hidden}}',
            '{{#ui}} floatingpanel-{{.}}{{/ui}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            //'{{#hidden}} style="display:none;"{{/hidden}}',
            '{{#id}} id="{{.}}"{{/id}}>',
            '{{#items}}',
                '{{#safe .}}{{/safe}}',
            '{{/items}}',
        '</div>'
    ].join(''));

    _tpls.loadmask = _templates.load([
        '<div class="loadmask',
            '{{#ui}} loadmask-{{.}}{{/ui}}',
            '{{#if fullscreen}}',
                ' loadmask-fullscreen',
            '{{else}}',
                ' loadmask-no-fullscreen',
            '{{/if}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            '{{#id}} id="{{.}}"{{/id}}>',
            '<div class="loadmask-msg',
                '{{#ui}} loadmask-msg-{{.}}{{/ui}}">',
                'Loading...',
            '</div>',
        '</div>'
    ].join(''));

    // @TODO not finish I think
    _tpls.textfield = _templates.load([
        '{{#icon}}<div class="textfield-icon" style="position: absolute;">{{#geticon .}}{{/geticon}}</div>{{/icon}}',
        '<input type="textfield" class="textfield',
            '{{#ui}} textfield-{{.}}{{/ui}}',
            '{{#icon}} textfield-icon-left{{/icon}}"',
            '{{#style}} style="{{.}}"{{/style}}',
            '{{#placeHolder}} placeholder="{{.}}"{{/placeHolder}}',
            '{{#id}} id="{{.}}"{{/id}}',
            '{{#value}} value="{{.}}"{{/value}}',
        '>'
    ].join(''));

    return _parent.subclass({

        constructor: function() {
            var q = _ns.Selector,
                html = q.get('html'),
                body = q.get('body');
            q.addClass(html, 'mobile');
            q.addClass(body, 'viewport');
        },

        getTpl: function(name) {
            return _tpls[name];
        }

    });

}());

# Setup audio and picture in phonegap

## Command-line

### Pictures

    cordova plugin add org.apache.cordova.camera

### Debug console

    cordova plugin add org.apache.cordova.console

### Recording/playing audio

    cordova plugin add org.apache.cordova.media

## Example

    <!DOCTYPE html>
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <script src="cordova.js" type="text/javascript"></script>
    </head>
    <body>
      <script type="text/javascript">
        <!--
        navigator.splashscreen.show();
        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            navigator.splashscreen.hide();
        }

        function recordAudio() {

          var src = "myrecording.wav";
          window.mediaRec = new Media(src,

          // success callback
          function() {
            console.log("recordAudio():Audio Success");
          },

          // error callback
          function(err) {
            console.log("recordAudio():Audio Error: "+ err.code);
          });

          // Record audio
          window.mediaRec.startRecord();

          setTimeout(function() {
            window.mediaRec.stopRecord();
          }, 5000);

        }

        function stopRecordAudio() {
          window.mediaRec.stopRecord();
        }

        function playAudio() {
          // Play the audio file at url
          var my_media = new Media('myrecording.wav',
              // success callback
              function () {
                  console.log("playAudio():Audio Success");
              },
              // error callback
              function (err) {
                  console.log("playAudio():Audio Error: " + err);
              }
          );
          // Play audio
          my_media.play();
        }

        function takePicture() {

          function onSuccess(imageURI) {
            //var image = document.getElementById('myImage');
            //image.src = imageURI;
            console.log('success');
            document.body.querySelector('.ui-page').style.display = 'none';
            document.body.backgroundSize = 'cover;';
            document.body.background = 'url(data:image/jpeg;base64,' + imageURI + ') center center no-repeat !important;';
          }

          function onFail(message) {
            console.log('Failed because: ' + message);
          }

          navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            //destinationType: Camera.DestinationType.FILE_URI
            destinationType: Camera.DestinationType.DATA_URL
          });
        }

        // -->
      </script>
      <a href="javascript:recordAudio();" style="z-index: 20000000; position: absolute; top:100px; left: 100px; background: red; color: #fff;">RECORD</a>
      <a href="javascript:playAudio();" style="z-index: 20000000; position: absolute; top:150px; left: 100px; background: green; color: #fff;">PLAY</a>
      <a href="javascript:takePicture();" style="z-index: 20000000; position: absolute; top:200px; left: 100px; background: blue; color: #fff;">TAKE A PIC</a>
    </body>
    </html>

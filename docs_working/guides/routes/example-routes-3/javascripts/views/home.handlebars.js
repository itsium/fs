{{ns}}.extend('page', {
    xtype: 'home',
    xtpl: 'page',
    config: {
        items: [{
            xtype: 'header',
            config: {
                ui: 'positive',
                title: 'Home'
            }
        }, {
            xtpl: 'content',
            config: {
                hasHeader: true
            },
            items: ['Welcome home', {
                xtype: 'button',
                config: {
                    text: 'Click me to see your profile',
                    route: '/user/profile'
                }
            }]
        }]
    }
});

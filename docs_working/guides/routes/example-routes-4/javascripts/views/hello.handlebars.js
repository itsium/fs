{{ns}}.extend('page', {
    xtype: 'hello',
    xtpl: 'page',
    hasListeners: ['show'],
    listeners: {
        show: function(renderer, self, args) {
            // args is here the arguments passed
            // from the router third arguments of
            // setCurrentView method
            self.content.update({
                hasHeader: true
            }, 'Hello ' + args.name + '!');
        }
    },
    config: {
        items: [{
            xtype: 'header',
            config: {
                ui: 'royal',
                title: 'Hello'
            }
        }, {
            xtpl: 'content',
            config: {
                hasHeader: true
            },
            implements: ['update'],
            ref: 'content'
        }]
    }
});

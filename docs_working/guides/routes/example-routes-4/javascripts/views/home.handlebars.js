var minirouter = new ({{ns}}.extend('routerviewsandbox', {
    routes: {
        '/home/list': 'routeHomeList',
        '/home/card': 'routeHomeCard'
    },
    routeHomeList: function() {
        this.setCurrentView('home.list');
    },
    routeHomeCard: function() {
        this.setCurrentView('home.card');
    }
}))();

{{ns}}.extend('page', {
    xtype: 'home',
    xtpl: 'page',
    listeners: {
        afterrender: function(self) {
            // we need to let the sandbox router know
            // where it needs to insert elements
            // and delete elements
            // here is like:
            //   self.el.querySelector('.home-content').innerHTML
            minirouter.view = self;
            minirouter.selector = '.home-content';
        }
    },
    config: {
        items: [{
            xtype: 'header',
            config: {
                ui: 'positive'
            },
            items: [{
                xtype: 'button',
                config: {
                    text: 'List',
                    route: '/home/list'
                }
            }, {
                xtpl: 'headertitle',
                items: 'Home'
            }, {
                xtype: 'button',
                config: {
                    text: 'Card',
                    route: '/home/card'
                }
            }]
        }, {
            xtpl: 'content',
            config: {
                hasHeader: true
            },
            items: ['Welcome home', {
                xtype: 'button',
                config: {
                    text: 'Click me to see your profile',
                    route: '/user/profile'
                }
            }, {
                xtpl: 'container',
                config: {
                    cssClass: 'home-content'
                },
                items: 'Here will be inserted the dynamic content based on the routes'
            }]
        }]
    }
});

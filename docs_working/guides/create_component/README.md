# Create a new component

In this guide we will create a new component to allow you understand how ROC works on several aspects:

-   Events
-   Rendering
-   Templates
-   Engines

We will create a toogle component like this one:

![toggle component](guides/create_component/images/toggle.png)

There are the three main steps:

1. First step will be to herit from `Roc.views.Template` and bind a `click` event to an handler.
2. Then we will create the toggle HTML markup using `Handlebars` template.
3. Finaly we will use the `ionic` engine to update the UI when the toggle handler is called.

## Example code

Example code is available [here](guides/create_component/toggle.zip).
You also can see the result [here](guides/create_component/toggle/roc_dist/).

## Creating the class

Our component needs to herit from `Roc.views.Template` class in order to be renderable easily.

`Roc.views.Template` is the base component class in ROC. It implements the `compile` and `render` methods and will automatically look for a template with same name as the given `xtype` of your component.

Let's start with extending the template class like this:

    Roc.views.Toggle = (function() {

        var _ns = Roc,
            _parent = _ns.views.Template;

        return _parent.subclass({
            xtype: 'toggle',
            className: 'Roc.views.Toggle'
        });

    })();

This convention allows us to have some shared private variable through all `toggle` instances, like `_ns` and `_parent` variables.

We return the `toggle` class and put it in `Roc.views.Toggle`. We also provide an `xtype` (`toggle`) to allows Roc to be able to retreive this class while using `xtype` JSON instance definition:

    items: [{
        xtype: 'toggle',
        // ...
    }]

This will automaticaly create a new `toggle` instance from the given `xtype`.

Note there is another method to extend from a class in Roc:

    Roc.views.Toggle = (function() {

        var _ns = Roc;

        // 'template' is the xtype of _parent.
        // We can also give Roc.views.Template directly.
        return _ns.extend('template', {
            xtype: 'toggle',
            className: 'Roc.views.Toggle'
        });

    })();

## Creating the template

Now that we have our empty `toggle` class, we need to create a `Handlebars` template in order to make our class bind to an HTMLElement.

To do so, we will create a `toggle.handlebars.hbs` file like this:

    <label class="toggle
        {{#ui}} toggle-{{.}}{{/ui}}
        {{> cssClass}}"{{> style}}{{> id}}>
        <input type="checkbox">
        <div class="track">
            <div class="handle"></div>
        </div>
    </label>

The `id` helper allows the `Template` class component to retreive the HTMLElement automaticaly after render.

Then we will just have to bind click event on it.

## Binding the click event

Let's now add the `click` event to our `toggle` element. First we need to be able to access to our component HTML markup in order to bind the `click` event.

The `afterrender` event is called after our component will be render, then we can access to our markup here.

Let's create a listener on the `afterrender` event:

    Roc.views.Toggle = (function() {

        var _ns = Roc,
            _parent = _ns.views.Template;

        function _afterrender(self, renderer) {
            // self.el is our HTMLElement
        }

        return _parent.subclass({

            xtype: 'toggle',
            className: 'Roc.views.Toggle',
            listeners: {
                priority: 'VIEWS',
                afterrender: _afterrender
            }

        });

    })();

We are now able to retreive our HTMLElement from our template.

Let's now use the `events` implements to help us bind the `events.click` event to our HTMLElement.

    Roc.views.Toggle = (function() {

        var _ns = Roc,
            _parent = _ns.views.Template;

        function _afterrender(self, renderer) {
            self.events.add('click', {
                xtype: 'events.click',
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: self.handler
            });
        }

        return _parent.subclass({

            xtype: 'toggle',
            className: 'Roc.views.Toggle',

            implements: ['events'],
            listeners: {
                priority: 'VIEWS',
                afterrender: _afterrender
            },

            handler: function(evt) {
                // toggle method will be defined in next section
                evt.preventDefault();
                this.toggle();
            }

        });

    })();

Using `implements: ['events']` allows us to quickly add an event without caring about attaching or detaching it every time our component is `show` or `hide`.

## Implementing the logic inside the component

The toggle component has only two states:

1. checked
2. unchecked

We will now implements two new methods:

1. setState (set state to given value)
2. toggle (switch current state to the other one)

Let's create the `setState` method first:

    setState: function(state) {
        this.checked = !!state;
        this.fireScope('change', this, this.checked);
    }

We store the current state inside `this.checked` and just fire `change` event with our component instance and the new value.

The event will be usefull for `ionic` engine later to update to UI. It also allows user to put a listener on this event.

The `toggle` method (used by our `click` `handler` method) will use `setState`:

    toggle: function() {
        this.setState(!this.checked);
    }

Last thing to do is to initialize our `this.checked` variable to a default value inside the component constructor:

    constructor: function() {
        this.checked = false;
        _parent.prototype.constructor.apply(this, arguments);
    }

We are now ready to work with ionic engine !

## Let the ionic engine update the UI

ROC is designed to have reusable component without attaching it to a specific UI. That's why we need to update the `ionic` engine class, to let the `<input type="checkbox" />` HTMLElement to be updated every time the user click on it.

This separation between component logic and UI allows us to be able to plug another UI (another `Handlebars` template) or to override the default UI to implement our own.

_Note_: You can specify a `xtpl` configuration value to any component to override the default template by your own. But you will need to respect the current engine HTML markup specification in order to make your new template works correctly with the engine component.

First we need to open the package file:

    /path/to/roc/packages/ionic/default/javascripts/default.handlebars.js

The `_events` private variable allows us to put some listener to our `toggle` component.

We will add a `change` listener by using `_events` variable:

    _events.toggle = {
        change: function(scope, component, checked) {

            var el = component.el.querySelector('input[type]');

            if (checked === true) {
                el.setAttribute('checked', 'checked');
            } else {
                el.removeAttribute('checked');
            }
        }
    };

We get the same arguments passed from our component `this.fireScope('change', this, this.checked);`

That's it. Our component is now ready to use inside a `Roc.forms.Simple` class !

#!/bin/sh

if [ $# = 0 ]; then
    echo
    echo "\tUsage: sh $0 path/"
    echo
    echo "\tpath: Path to ROC framework main directory."
    echo
    exit
fi

if [ ! -d $1 ]; then
    echo
    echo "\tError: \"$1\" is not a directory."
    echo "\tUsage: sh $0 path/"
    echo
    exit
fi

cd $1

echo "Cleaning..."
rm -rf docs/*

echo "Packing..."
grunt pack --output dist/doc/ --cache false --config documentation-full.roc.json

echo "Generating documentation..."
jsduck --title="ROC Documentation" --output docs/ dist/doc/core@1.0.0.js --guides=utils/doc-guides.json --external=Handlebars,Scroller --eg-iframe=utils/doc-iframe/iframe.html

mkdir -p docs/roc-build/
cp -R dist/doc/fonts docs/roc-build/
cp dist/doc/core@1.0.0.* docs/roc-build/

echo "Done."
#!/bin/sh

if [ $# = 0 ]; then
    echo
    echo "\tUsage: sh $0 path/"
    echo
    echo "\tpath: Path to ROC framework main directory."
    echo
    exit
fi

if [ ! -d $1 ]; then
    echo
    echo "\tError: \"$1\" is not a directory."
    echo "\tUsage: sh $0 path/"
    echo
    exit
fi

cd $1

echo "Cleaning..."
rm -rf docs_working/*

echo "Packing..."
grunt pack --output dist/docs_working/ --cache false --config documentation.roc.json

echo "Generating documentation..."
jsduck --title="ROC Documentation" --output docs_working/ dist/docs_working/core@1.0.0.js --guides=utils/doc-guides.json --external=Handlebars,Scroller --eg-iframe=utils/doc-iframe/iframe.html

mkdir -p docs_working/roc-build/
cp -R dist/docs_working/fonts docs_working/roc-build/
cp dist/docs_working/core@1.0.0.* docs_working/roc-build/

echo "Done."
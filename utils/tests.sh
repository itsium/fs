#!/bin/sh

if [ $# = 0 ]; then
    echo
    echo "\tUsage: sh $0 path/"
    echo
    echo "\tpath: Path to ROC framework main directory."
    echo
    exit
fi

if [ ! -d $1 ]; then
    echo
    echo "\tError: \"$1\" is not a directory."
    echo "\tUsage: sh $0 path/"
    echo
    exit
fi

cd $1

echo "Cleaning..."
rm -rf tests/*

echo "Generating..."
grunt pack --output tests/ --cache false --config packages/tests/all/tests.roc.json

echo "Done."
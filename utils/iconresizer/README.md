INSTALL
-------

 * sudo apt-get install imagemagick
 * sudo apt-get install graphicsmagick
 * npm install

********

USAGE
-----

 * node index.js -i icon-144.png --ios --android

********

CHANGELOG
---------

## 1.1.0

_Date: 2014/02/14_

#### Features

  - **wechat:** Add `wechat` option.

********


var path = require('path'),
    gm = require('gm'),
    mkdirp = require('mkdirp'),
    colors = require('colors'),
    argv = require('optimist')
    .usage('Usage: $0 [--ios] [--android] [-s splash.png] [-i icon.png] [-d destination/]')
    .boolean('h', 'ios', 'android')
    .alias('i', 'icon')
    .alias('s', 'splash')
    .alias('h', 'help')
    .alias('d', 'destination')
    .describe('h', 'Show help')
    .describe('i', 'Icon image')
    .describe('s', 'Splash image')
    .describe('d', 'Destination folder')
    .describe('ios', 'Enable ios')
    .describe('android', 'Enable android')
    .describe('wechat', 'Enable wechat')
    .describe('androidiconname', 'Android icon name')
    .default('androidiconname', 'ic_launcher')
    .argv,
    inputIcon = (argv.icon ? path.resolve(argv.icon) : null),
    inputSplash = (argv.splash ? path.resolve(argv.splash) : null);

function start() {

    if (argv.h === true) {
        console.log(require('optimist').help());
        process.exit(1);
    }

    if (!argv.destination) {
        argv.destination = path.dirname((inputIcon ? inputIcon : inputSplash));
    }

    if (inputIcon) {
        checkSourceIcon(inputIcon, function(success) {
            if (success !== true) {
                process.exit(1);
            }
            if (argv.ios === true) {
                iosIcons();
            }
            if (argv.android === true) {
                androidIcons();
            }
            if (argv.wechat === true) {
                wechatIcons();
            }
        });
    }

    if (inputSplash) {
        checkSourceSplash(inputSplash, function(success) {
            if (success !== true) {
                process.exit(1);
            }
            if (argv.ios === true) {
                iosSplashs();
            }
            if (argv.android === true) {
                androidSplashs();
            }
        });
    }

}

function error() {

    var args = ['!'.red].concat(Array.prototype.slice.call(arguments, 0));

    console.error.apply(console, args);
}

function success() {

    var args = ['+'.green].concat(Array.prototype.slice.call(arguments, 0));

    console.log.apply(console, args);
}

function checkSourceIcon(srcImage, callback) {

    var i = gm(srcImage);

    i.size(function(err, value) {

        if (err) {
            error(err);
            callback(false);
        } else if (value.width !== value.height ||
            value.width < 144) {
            error('Image size should be >= 144x144');
            callback(false);
        } else {
            callback(true);
        }

    });
}

function checkSourceSplash(srcImage, callback) {

    var i = gm(srcImage);

    i.size(function(err, value) {

        if (err) {
            error(err);
            callback(false);
        } else if (value.width < 320 || value.height < 480) {
            error('Image size should be >= 320x480');
            callback(false);
        } else {
            callback(true);
        }

    });
}

function createIcon(srcImage, destImage, opts, callback) {

    var i = gm(srcImage);

    opts = opts || {};

    if (opts.width && !opts.height) {
        opts.height = opts.width;
    }

    i.resize(opts.width, opts.height);

    if (opts.bits) {
        i.bitdepth(opts.bits);
    }
    if (opts.grayscale === true) {
        i.type('Grayscale');
    }

    i.write(destImage, function(err) {
        if (err) {
            error('Failed to resize icon with given options: ', opts);
            error(err);
            callback(false);
        } else {
            callback(true);
        }
    });
}

function createSplash(srcImage, destImage, opts, callback) {

    var i = gm(srcImage);

    opts = opts || {};

    if (opts.source.width >= opts.width &&
        opts.source.height >= opts.height) {

        var x = (opts.source.width - opts.width) / 2,
            y = (opts.source.height - opts.height) / 2;

        // @ignore !TODO check if opts.width === opts.source.width / 2 if true so resize and don't crop

        i.crop(opts.width, opts.height, x, y);
    } else {
        i.resize(opts.width, opts.height, '!');
    }

    i.write(destImage, function(err) {
        if (err) {
            error('Failed to resize splashscreen with given options: ', opts);
            error(err);
            callback(false);
        } else {
            callback(true);
        }
    });

}

function iosSplashs() {

    var splashs = [{
        name: 'Default~iphone.png',
        width: 320,
        height: 480
    }, {
        name: 'Default@2x~iphone.png',
        width: 640,
        height: 960
    }, {
        name: 'Default-568h@2x~iphone.png',
        width: 640,
        height: 1136
    }, {
        name: 'Default-Portrait~ipad.png',
        width: 768,
        height: 1004
    }, {
        name: 'Default-Portrait@2x~ipad.png',
        width: 1536,
        height: 2008
    }, {
        name: 'Default-Landscape~ipad.png',
        width: 1024,
        height: 768
    }, {
        name: 'Default-Landscape@2x~ipad.png',
        width: 2048,
        height: 1496
    }],
        length = splashs.length,
        dest = path.resolve(argv.destination, 'ios/splashs/'),
        remaining = length,
        sourceSize = {
            width: 0,
            height: 0
        };

    function resize() {
        while (length--) {

            var splash = splashs[length];

            splash.source = sourceSize;

            createSplash(inputSplash, path.resolve(dest, splash.name), splash, function() {
                --remaining;
                if (!remaining) {
                    success('ios splashscreens.');
                }
            });
        }
    }

    var i = gm(inputSplash)
        .size(function(err, value) {
            if (err) {
                error('Failed to get splash size');
                error(err);
            } else {
                sourceSize = value;
                mkdirp(dest, function(err) {
                    if (err) {
                        error('Failed to create directory: ', dest);
                        error(err);
                    } else {
                        resize();
                    }
                });
            }
        });

}

function androidSplashs() {

    var splashs = [{
        name: 'drawable/screen.png',
        width: 720,
        height: 1280
    }, {
        name: 'drawable-ldpi/screen.png',
        width: 200,
        height: 320
    }, {
        name: 'drawable-mdpi/screen.png',
        width: 320,
        height: 480
    }, {
        name: 'drawable-hdpi/screen.png',
        width: 480,
        height: 800
    }, {
        name: 'drawable-xhdpi/screen.png',
        width: 720,
        height: 1280
    }, {
        name: 'drawable-xxhdpi/screen.png',
        width: 720,
        height: 1280
    }],
        length = splashs.length,
        dest = path.resolve(argv.destination, 'android/'),
        remaining = length,
        sourceSize = {
            width: 0,
            height: 0
        };

    function resize() {
        while (length--) {

            var splash = splashs[length];

            splash.source = sourceSize;

            createSplash(inputSplash, path.resolve(dest, splash.name), splash, function() {
                --remaining;
                if (!remaining) {
                    success('android splashscreens.');
                }
            });
        }
    }

    var i = gm(inputSplash)
        .size(function(err, value) {
            if (err) {
                error('Failed to get splash size');
                error(err);
            } else {
                sourceSize = value;
                mkdirp(dest, function(err) {
                    if (err) {
                        error('Failed to create directory: ', dest);
                        error(err);
                    } else {
                        resize();
                    }
                });
            }
        });

}

function androidIcons() {

    var iconName = argv.androidiconname,
        icons = [{
        name: 'drawable/' + iconName + '.png',
        width: 144
    }, {
        name: 'drawable-ldpi/' + iconName + '.png',
        width: 36
    }, {
        name: 'drawable-mdpi/' + iconName + '.png',
        width: 48
    }, {
        name: 'drawable-hdpi/' + iconName + '.png',
        width: 72
    }, {
        name: 'drawable-xhdpi/' + iconName + '.png',
        width: 96
    }, {
        name: 'drawable-xxhdpi/' + iconName + '.png',
        width: 144
    }],
        length = icons.length,
        dest = path.resolve(argv.destination, 'android/'),
        remaining = length;

    function resize() {
        while (length--) {

            var icon = icons[length],
                iconPath = path.resolve(dest, icon.name);

            mkdirp(path.dirname(iconPath), resizeIcon(icon, iconPath));

        }
    }

    function resizeIcon(icon, iconPath) {

        return function(err) {
            if (err) {
                error('Failed to create directory: ', dest);
                error(err);
            } else {
                createIcon(inputIcon, iconPath, icon, function() {
                    --remaining;
                    if (!remaining) {
                        success('android icons');
                    }
                });
            }
        };

    }

    mkdirp(dest, function(err) {
        if (err) {
            error('Failed to create directory: ', dest);
            error(err);
        } else {
            resize();
        }
    });

}

function wechatIcons() {

    var icons = [{
        name: 'Watermark-28x28-24bits.png',
        grayscale: true,
        width: 28,
        bits: 24
    }, {
        name: 'High-Resolution-108x108-24bits.png',
        width: 108,
        bits: 24
    }],
        length = icons.length,
        dest = path.resolve(argv.destination, 'wechat/'),
        remaining = length;

    function resize() {
        while (length--) {

            var icon = icons[length];

            createIcon(inputIcon, path.resolve(dest, icon.name), icon, function() {
                --remaining;
                if (!remaining) {
                    success('wechat icons');
                }
            });
        }
    }

    mkdirp(dest, function(err) {
        if (err) {
            error('Failed to create directory: ', dest);
            error(err);
        } else {
            resize();
        }
    });

}

function iosIcons() {

    var icons = [{
        name: 'Icon-29.png',
        width: 29
    }, {
        name: 'Icon-29@2x.png',
        width: 58
    }, {
        name: 'Icon-40.png',
        width: 40
    }, {
        name: 'Icon-40@2x.png',
        width: 80
    }, {
        name: 'Icon-50.png',
        width: 50
    }, {
        name: 'Icon-50@2x.png',
        width: 100
    }, {
        name: 'Icon-60.png',
        width: 60
    }, {
        name: 'Icon-60@2x.png',
        width: 120
    }, {
        name: 'Icon-72.png',
        width: 72
    }, {
        name: 'Icon-72@2x.png',
        width: 144
    }, {
        name: 'Icon-76.png',
        width: 76
    }, {
        name: 'Icon-76@2x.png',
        width: 152
    }, {
        name: 'Icon-Small.png',
        width: 29
    }, {
        name: 'Icon-Small@2x.png',
        width: 58
    }, {
        name: 'Icon.png',
        width: 57
    }, {
        name: 'Icon@2x.png',
        width: 114
    }],
        length = icons.length,
        dest = path.resolve(argv.destination, 'ios/'),
        remaining = length;

    function resize() {
        while (length--) {

            var icon = icons[length];

            createIcon(inputIcon, path.resolve(dest, icon.name), icon, function() {
                --remaining;
                if (!remaining) {
                    success('ios icons');
                }
            });
        }
    }

    mkdirp(dest, function(err) {
        if (err) {
            error('Failed to create directory: ', dest);
            error(err);
        } else {
            resize();
        }
    });

}

start();

/**
 * @class {{ns}}.views.Toggle
 *
 * Toggle component.
 *
 * Example:
 *
 * 		@example ionic_page
 * 		var example = [{
 * 			xtype: 'toggle',
 * 			config: {
 * 				ui: 'positive'
 * 			}
 * 		}];
 *
 */
{{ns}}.views.Toggle = (function() {

	var _ns = {{ns}},
		_parent = _ns.views.Template;

	function _afterrender(self, renderer) {
		self.events.add('click', {
			xtype: 'events.click',
			autoAttach: true,
			el: self.el,
			scope: self,
			handler: self.handler
		});
	}

	/**
	 * @event change
	 * Fires after state changed.
	 * @param {Mixed} scope Given listener scope
	 * @param { {{ns}}.views.Toggle } self Toggle component instance
	 * @param {Boolean} state True if checked else false.
	 */

	return _parent.subclass({

		xtype: 'toggle',
		className: '{{ns}}.views.Toggle',

		implements: ['events'],
		listeners: {
			priority: 'VIEWS',
			afterrender: _afterrender
		},

		constructor: function() {
			this.checked = false;
			_parent.prototype.constructor.apply(this, arguments);
		},

		/**
		 * Toggle state.
		 * @fires change
		 */
		toggle: function() {
			this.setState(!this.checked);
		},

		/**
		 * Set toggle state from given value.
		 * True to check, false to uncheck.
		 * @param {Boolean} state True to check, false to uncheck.
		 * @fires changes
		 */
		setState: function(state) {
			this.checked = !!state;
			this.fireScope('change', this, this.checked);
		},

		/**
		 * Click/touch handler.
		 * Defaults to toggle.
		 * @param  {TouchEvent/MouseEvent} evt Event
		 */
		handler: function(evt) {
			evt.preventDefault();
			this.toggle();
		}

	});

})();

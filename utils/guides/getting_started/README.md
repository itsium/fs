# Getting started

In order to create our first app with ROC, we will use the yoeman generator roc-app:

    yo roc-app

![yoeman generator roc app](guides/getting_started/images/yo-roc-app.png)

Once it is done, you can go to ROC root directory and launch the precompilation.

    cd /path/to/roc
    roc$ grunt pack --config /path/to/my-first-app/app.roc.json

Now you can open the generated html file directly from your browser:

    /path/to/my-first-app/roc_dist/index.html

![basic app screenshot](guides/getting_started/images/first-app-screenshot.png)

## How does it works

First let's see the `app.roc.json` file:

    {
        "name": "my-first-app",
        "version": "1.0.0",
        "roc_type": "app",
        "roc_tags": ["mobile", "app", "demo"],
        "minify": false,
        "roc_dependencies": {
            "physics.scroll": ">=0.1.0",
            "ionic.theme.default": ">=0.1.0",
            "ionic.theme.ios7": ">=0.1.0",
            "ionic.engine.default": ">=0.1.0",
            "component.abstract": ">=0.1.0",
            "ionic.ui.tabbutton": ">=0.1.0",
            "ionic.ui.header": ">=0.1.0",
            "ionic.ui.headertitle": ">=0.1.0",
            "ionic.ui.page": ">=0.1.0",
            "ionic.ui.footer": ">=0.1.0",
            "ionic.ui.button": ">=0.1.0",
            "ionic.ui.content": ">=0.1.0",
            "ionic.ui.container": ">=0.1.0",
            "ionic.ui.controlgroup": ">=0.1.0",
            "ionic.ui.bubble": ">=0.1.0",
            "ionic.ui.tabs": ">=0.1.0",
            "helpers.i18n": ">=0.1.0",
            "events.direction": "~0.1.0",
            "helpers.routerview": ">=0.1.0",
            "fx.animate.css": "~0.1.0",
            "ionic.ui.loadmask": "~0.1.0"
        },
        "resources": [
            "fonts/*"
        ],
        "stylesheets": [
            "stylesheets/*.css"
        ],
        "javascripts": [
            "javascripts/bootstrap.handlebars.js",
            "javascripts/locales/cn.js",
            "javascripts/footer.handlebars.js",
            "javascripts/routes.handlebars.js",
            "javascripts/views/home.handlebars.js",
            "javascripts/views/account.handlebars.js",
            "javascripts/views/discover.handlebars.js",
            "javascripts/views/more.handlebars.js",
            "javascripts/app.handlebars.js"
        ],
        "htmls": [
            "htmls/index.handlebars.html"
        ],
        "images": [
            "images/**"
        ]
    }

This file is used by ROC to know which files and which dependencies your package needs.
Then `grunt` will know which file to concat and copy, also retreive all dependencies files, and then precompile all files with their given preprocessor.
For example the file:

    javascripts/bootstrap.handlebars.js

will be precompile with `handlebars` to javascript. You can use more than one preprocessor if those are compatible.

All available packages are under:

    /path/to/roc/packages/

Or you can use this command in order to list all availables package:

    cd /path/to/roc/
    node bin/roc list


# Routes

Routes are available in order to bind views or actions inside your ROC application.

First you need to add the `helpers.routerview` package to your application.

## How to route two pages

### Example code

Example code is available [here](guides/routes/example-routes-1.zip).
You also can see the result [here](guides/routes/example-routes-1/roc_dist/).

### Guide

The most simple example is you want to redirect the user to another view when he clicks on a button.

To make this works, we will imagine we have those two views:

    // javascripts/app.handlebars.js
    var app = new Roc.App({
        engine: 'ionic',
        defaultRoute: '/home'
    });

    // javascripts/views/home.handlebars.js
    var homeview = Roc.extend('page', {
        xtype: 'home',
        config: {
            items: [{
                xtype: 'header',
                config: {
                    title: 'Home'
                }
            }, {
                xtpl: 'content',
                config: {
                    hasHeader: true
                },
                items: ['Welcome home', {
                    xtype: 'button',
                    config: {
                        text: 'Click me to see your profile',
                        route: '/home'
                    }
                }]
            }]
        }
    });

    // javascripts/views/profile.handlebars.js
    var profileview = Roc.extend('page', {
        xtype: 'user.profile',
        config: {
            items: [{
                xtype: 'header',
                config: {
                    title: 'My profile'
                }
            }, {
                xtpl: 'content',
                config: {
                    hasHeader: true
                },
                items: 'This is my profile page'
            }]
        }
    });

    // javascripts/routes.handlebars.js
    var router = new (Roc.extend('routerview', {
        routeNoMatch: 'routeHome',
        routes: {
            '/home': 'routeHome',
            '/user/profile': 'routeProfile'
        },
        routeHome: function() {
            this.setCurrentView('home');
        },
        routeProfile: function() {
            this.setCurrentView('user.profile');
        }
    }))();

This code will create a new `home` page by default. If we add this anchor to the URL:

    http://localhost/example.html#/user/profile

It will then create a new `user.profile` page, hide the `home` page and then show the `user.profile` page.

_Note_: views are only instanciated once.

## How to pass arguments from routes to view

### Example code

Example code is available [here](guides/routes/example-routes-2.zip).
You also can see the result [here](guides/routes/example-routes-2/roc_dist/).

### Guide

Now we have this working, we want to be able to take arguments from a route, and show it inside the view.

First we need to add a new route:

    // javascripts/routes.handlebars.js
    var router = new (Roc.extend('routerview', {
        routeNoMatch: 'routeHome',
        routes: {
            '/home': 'routeHome',
            '/user/profile': 'routeProfile',
            '/say/hello/to/:name': 'routeHello'
        },
        routeHome: function() {
            this.setCurrentView('home');
        },
        routeProfile: function() {
            this.setCurrentView('user.profile');
        },
        routeHello: function(name) {
            this.setCurrentView('hello', {}, {
                name: name
            });
        }
    }))();

To define arguments in a route, we use the synthax `:my_variable_name` as showed for the `routeHello`.

Then Roc will split the current route hash, and give every variables to the route method as arguments.

Now we need to add a new view which will take the name arguments in the `show` listeners. Everytime a view is called from the router, the `show` event will be fire with given dictionnary pass to `setCurrentView` method.

    // javascripts/views/hello.handlebars.js
    var helloview = Roc.extend('page', {
        xtype: 'hello',
        xtpl: 'page',
        hasListeners: ['show'],
        listeners: {
            show: function(renderer, self, args) {
                // args is here the arguments passed
                // from the router third arguments of
                // setCurrentView method
                self.content.update({
                    hasHeader: true
                }, 'Hello ' + args.name + '!');
            }
        },
        config: {
            items: [{
                xtype: 'header',
                config: {
                    ui: 'royal',
                    title: 'Hello'
                }
            }, {
                xtpl: 'content',
                config: {
                    hasHeader: true
                },
                implements: ['update'],
                ref: 'content'
            }]
        }
    });

## How to restrict access to a route

### Example code

Example code is available [here](guides/routes/example-routes-3.zip).
You also can see the result [here](guides/routes/example-routes-3/roc_dist/).

### Guide

How to check if a user has access to a route, or to restrict access to a route which need user to be logged in ?

To do so, we can add the `before` configuration in our router. See below for the `routeProfile`:

    // javascripts/routes.handlebars.js
    var router = new ({{ns}}.extend('routerview', {
        routeNoMatch: 'routeHome',
        before: {
            'routeProfile': 'isAuthenticated'
        },
        routes: {
            '/home': 'routeHome',
            '/user/profile': 'routeProfile',
            '/say/hello/to/:name': 'routeHello'
        },
        isAuthenticated: function(done) {

            // we are assuming you have created a session object
            // which stores user information
            if (session.logged === true) {
                // if user is logged in then we can call the done callback here to
                // go to the next before, or to the route if there is no next before.
                return done();
            }

            // else we redirect the user to the home view
            // or we can show an alert box here and then redirect
            alert('Sorry you need to be logged in order to access to this page.');
            Roc.History.go('/home');
            return false;

        },
        routeHome: function() {
            this.setCurrentView('home');
        },
        routeProfile: function() {
            this.setCurrentView('user.profile');
        },
        routeHello: function(name) {
            this.setCurrentView('hello', {}, {
                name: name
            });
        }
    }))();

## How to change content inside a page with a sandbox router ?

### Example code

Example code is available [here](guides/routes/example-routes-4.zip).
You also can see the result [here](guides/routes/example-routes-4/roc_dist/).

### Guide

We want now to keep our home header and content, but show a list or a card based on the current route.

First we need to create listview and cardview:

    // javascripts/views/homelist.handlebars.js
    Roc.extend('list', {
        xtype: 'home.list',
        xtpl: 'list',
        itemTpl: 'listitembasic',
        config: {
            items: [{
                label: 'First item'
            }, {
                label: 'Second item'
            }, {
                label: 'Third item'
            }]
        }
    });

    // javascripts/views/homecard.handlebars.js
    Roc.extend('template', {
        xtype: 'home.card',
        xtpl: 'card',
        config: {
            config: {
                header: 'Header',
                footer: 'Footer'
            },
            items: [{
                xtpl: 'container',
                config: {
                    title: 'Card 1'
                },
                items: '<p>Card 1 content</p>'
            }, {
                xtpl: 'container',
                config: {
                    title: 'Card 2'
                },
                items: '<p>Card 2 content</p>'
            }]
        }
    });

Then we can add the sandbox router at the top of home view:

    // javascripts/views/home.handlebars.js
    var minirouter = new (Roc.extend('routerviewsandbox', {
        routes: {
            '/home/list': 'routeHomeList',
            '/home/card': 'routeHomeCard'
        },
        routeHomeList: function() {
            this.setCurrentView('home.list');
        },
        routeHomeCard: function() {
            this.setCurrentView('home.card');
        }
    }))();

    Roc.extend('page', {
        xtype: 'home',
        xtpl: 'page',
        listeners: {
            afterrender: function(self) {
                // we need to let the sandbox router know
                // where it needs to insert elements
                // and delete elements
                // here is like:
                //   self.el.querySelector('.home-content').innerHTML
                minirouter.view = self;
                minirouter.selector = '.home-content';
            }
        },
        config: {
            items: [{
                xtype: 'header',
                config: {
                    ui: 'positive'
                },
                items: [{
                    xtype: 'button',
                    config: {
                        text: 'List',
                        route: '/home/list'
                    }
                }, {
                    xtpl: 'headertitle',
                    items: 'Home'
                }, {
                    xtype: 'button',
                    config: {
                        text: 'Card',
                        route: '/home/card'
                    }
                }]
            }, {
                xtpl: 'content',
                config: {
                    hasHeader: true
                },
                items: ['Welcome home', {
                    xtype: 'button',
                    config: {
                        text: 'Click me to see your profile',
                        route: '/user/profile'
                    }
                }, {
                    xtpl: 'container',
                    config: {
                        cssClass: 'home-content'
                    },
                    items: 'Here will be inserted the dynamic content based on the routes'
                }]
            }]
        }
    });

Last thing we need to do, is letting the global router knows we have two more routes now and tell it we want those routes to use the `home` view.
Add those two lines inside the `routes` dictionnary in file `javascripts/routes.handlebars.js`:

        '/home/list': 'routeHome',
        '/home/card': 'routeHome',

## What about add another router in another package ?

It often happens that two different packages (or more) have both routers but cannot interact with each other due to variable scope.

When this happen just know that the `Roc.RouterView` class will automaticaly hide the previous view, even if the previous view is from another router.

_Note_: Instead of `Roc.RouterView`, the `Roc.RouterViewSandbox` class cannot interact with other sandbox router. So be aware of this before creating a sandbox router.

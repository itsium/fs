{{ns}}.extend('list', {
    xtype: 'home.list',
    xtpl: 'list',
    itemTpl: 'listitembasic',
    config: {
        items: [{
            label: 'First item'
        }, {
            label: 'Second item'
        }, {
            label: 'Third item'
        }]
    }
});

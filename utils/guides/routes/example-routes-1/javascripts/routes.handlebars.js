var router = new ({{ns}}.extend('routerview', {
    routeNoMatch: 'routeHome',
    routes: {
        '/home': 'routeHome',
        '/user/profile': 'routeProfile'
    },
    routeHome: function() {
        this.setCurrentView('home');
    },
    routeProfile: function() {
        this.setCurrentView('user.profile');
    }
}))();
